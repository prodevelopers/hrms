<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <title>Parexons HRM</title>
    <link href="../img/favicon-img.png" rel="shortcut icon" />
    <link rel="stylesheet" type="text/css" href="../css/component.css" />
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/progress-bar.css">
    <link type="text/css" rel="stylesheet" href="../css/popModal.css">
    <link rel="stylesheet" type="text/css" href="../css/form.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../less/dropdown.less">
    <script src="js/jquery-1.11.1.min.js"></script>
</head>
<body>
<style type="text/css">
    .dropdown-menu{
    position: relative;
    left: 70%;
}
.margino{
        padding-left: 2em;
        font-family: "Myriad Pro";
    }
    .margino .header {
        width: 200px;
        background: #4D6684;
        color: #fff;
        border:none;
    }
    .marginol{
        position: relative;
        left: 600px;
    }
    .margino ul{
        float: left;
    }
    .margino .menu li{
        float: left;
        width: 200px;
        background: #fff;
        border:none;
    }
    .margino .menu h4{
        font-size: 14px;
        float: left;
    }
    .margino .menu small{
        float: right;
        padding: 0.2em 0;
    }
    .margino .menu a{
        padding: 0.2em 0.5em;
        background: green;
        color: #fff;
    }
    .margino .menu a:hover{
        color: #f8f8f8;
    }
    .margino .menu p{
        float: left;
        width: 100%;
        padding: 0.2em;
        font-size: 13px;
        font-weight: bold;;
    }
    .margino .menu p:hover{
        background: #f8f8f8;
    }
    .margino ul ul{
        position: relative;
        left: -2px;
        top: 27px;
    }
    .margino ul ul li {
        float: left;
    }
    .margino h3 {
        font-size: 12px;
        padding: 0.5em;
    }
</style>
<header>
    <h1>HR Management System</h1>
    <div class="right-icons">
        <ul>
            <li><a href="#">
                <span class="fa fa-user"></span>
                <h4>Admin</h4>
                <i class="fa fa-caret-down"></i>
            </a>
            <ul>
                <li><a href="#">My Account</a></li>
                <li><a href="#">Log Out</a></li>
            </ul>
            </li>
            <li class="margino">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-users"></i>
                <div class="notify-count">7</div>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 7 messages
                    <a href="../configuration.php" style="margin-left:10px; font-size:12px; color:#fff; ">View All</a> 
                </li>
                <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                </ul>
                </ul>
            </li>
            </ul>
    <!-- <div style="display:none">
        <div id="content">
            <div class="notify-actions">
                <input type="checkbox">
                <label>Stop Notifications</label>
                <h5>Mark As Read</h5>
            </div>
            <br class="clear">
            <div class="notifications">
                <h3>Leave Approval Request</h3>
                <h5>From: <i><a href="#">Employee Name</a></i></h5>
                <h5>26 june 2014 05:55pm</h5>
            </div>
            <div class="notifications">
                <h3>Leave Approval Request</h3>
                <h5>From: <i><a href="#">Employee Name</a></i></h5>
                <h5>26 june 2014 05:55pm</h5>
            </div>
            <div class="notifications">
                <h3>Leave Approval Request</h3>
                <h5>From: <i><a href="#">Employee Name</a></i></h5>
                <h5>26 june 2014 05:55pm</h5>
            </div>

            <div class="popModal_footer">
                View All
            </div>
        </div>
    </div> -->

    </div>
</header>

<div class="nav-container clearfix">
    <nav id="menu" class="nav">                 
        <ul>
            <li>
                <a href="../index.php">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-home"></i>
                    </span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="../all_employee_list.php">
                    <span class="icon"> 
                        <i aria-hidden="true" class="fa fa-users"></i>
                    </span>
                    <span>Human Resource</span>
                </a>
            </li>
            <li>
                <a href="../leave_attendance.php">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-edit"></i>
                    </span>
                    <span>Leave & Attendance</span>
                </a>
            </li>
            <li>
                <a href="../payroll_register.php">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-money"></i>
                    </span>
                    <span>PayRoll</span>
                </a>
            </li>
            
            <li>
                <a href="../users_management.php">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-user"></i>
                    </span>
                    <span>Users Management</span>
                </a>
            </li>
            <li>
                <a href="../configuration.php">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-cogs"></i>
                    </span>
                    <span>Configuration</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="icon">
                        <i aria-hidden="true" class="fa fa-hdd-o"></i>
                    </span>
                    <span>Backup & Restore</span>
                </a>
            </li>
        </ul>
    </nav>
</div>

<!-- header ends -->

<br style="clear:both;">
<!-- contents -->
<link rel="stylesheet" href="demo.css"/>
        <link rel="stylesheet" href="jquery.orgchart.css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="jquery.orgchart.js"></script>
        <script>
        $(function() {
            $("#organisation").orgChart({container: $("#main"), interactive: true, fade: true, speed: 'slow'});
        });
        </script>
        <style type="text/css">
            .btn{
                float: left;
                padding: 5px 8px 5px 8px;
                border: none;
                margin-right: 5px;
                cursor: pointer;
                /*width: 45%;*/
            }
            .green{
                background: #6AAD6A;
                color: #fff;
            }
        </style>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Organizational Chart</div> <!-- bredcrumb -->
<div class="left-nav">
        <ul>
            <li><a href="organization_chart.php">View Chart</a></li>
            <li><a href="../add_chart.php">Make Chart</a></li>
        </ul>
    </div>

	<div class="right-contents">
    <div class="head">Organiational Chart</div>
		 <div id="left">

            <ul id="organisation" style="display:none;">
                <li><em>Batman</em>
                    <ul>
                        <li>Batman Begins
                            <ul>
                                <li>Ra's Al Ghul</li>
                                <li>Carmine Falconi</li>
                            </ul>
                        </li>
                        <li>The Dark Knight
                            <ul>
                                <li>Joker</li>
                                <li>Harvey Dent
                                     <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                        <li>The Dark Knight Rises
                            <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul
                                    <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        
        </div>

        <div id="content">
            
            <div id="main">
            </div>

        </div>
	</div>
<!-- contents -->
<br class="clear">
<footer>
    
<script src="js/modernizr.custom.js"></script>
<script src="custom-jquery.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/popModal.js"></script>
<!-- notifications -->
<script>
$(function(){
    $('#popModal_ex1').click(function(){
        $('#popModal_ex1').popModal({
            html : $('#content'),
            placement : 'bottomLeft',
            showCloseBut : true,
            onDocumentClickClose : true,
            inline : true,
            overflowContent : false,
            onOkBut : function(){},
            onCancelBut : function(){},
            onLoad : function(){},
            onClose : function(){}
        });
    });
    
    $('#popModal_ex2').click(function(){
        $('#popModal_ex2').popModal({
            html : function(callback) {
                $.ajax({url:'ajax.html'}).done(function(content){
                    Ocallback(content);
                });
            }
        });
    });
    
    $('#notifyModal_ex1').click(function(){
        $('#content2').notifyModal({
            duration : 2500,
            placement : 'center',
            type : 'notify',
            overlay : true
        });
    });
    
    $('#dialogModal_ex1').click(function(){
        $('.dialog_content').dialogModal({
            onOkBut: function() {},
            onCancelBut: function() {},
            onLoad: function() {},
            onClose: function() {},
        });
    });
    
});
</script>

<!-- navigation  -->
<script>
    //  The function to change the class
    var changeClass = function (r,className1,className2) {
        var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
        if( regex.test(r.className) ) {
            r.className = r.className.replace(regex,' '+className2+' ');
        }
        else{
            r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"),' '+className1+' ');
        }
        return r.className;
    };  

    //  Creating our button in JS for smaller screens
    var menuElements = document.getElementById('menu');
    menuElements.insertAdjacentHTML('afterBegin','<button type="button" id="menutoggle" class="navtoogle" aria-hidden="true"><i aria-hidden="true" class="icon-menu"> </i> Menu</button>');

    //  Toggle the class on click to show / hide the menu
    document.getElementById('menutoggle').onclick = function() {
        changeClass(this, 'navtoogle active', 'navtoogle');
    }

    // http://tympanus.net/codrops/2013/05/08/responsive-retina-ready-menu/comment-page-2/#comment-438918
    document.onclick = function(e) {
        var mobileButton = document.getElementById('menutoggle'),
            buttonStyle =  mobileButton.currentStyle ? mobileButton.currentStyle.display : getComputedStyle(mobileButton, null).display;

        if(buttonStyle === 'block' && e.target !== mobileButton && new RegExp(' ' + 'active' + ' ').test(' ' + mobileButton.className + ' ')) {
            changeClass(mobileButton, 'navtoogle active', 'navtoogle');
        }
    }
</script>
<!--  navigation -->

<!-- scroll -->
<script>
  $(document).ready(function() {
  
    var nice = $("html").niceScroll();  // The document page (body)
    
    $("#div1").html($("#div1").html()+' '+nice.version);
    
    $("#boxscroll").niceScroll({cursorborder:"",cursorcolor:"#00F",boxzoom:true}); // First scrollable DIV

    $("#boxscroll2").niceScroll("#contentscroll2",{cursorcolor:"#F00",cursoropacitymax:0.7,boxzoom:true,touchbehavior:true});  // Second scrollable DIV
    $("#boxframe").niceScroll("#boxscroll3",{cursorcolor:"#0F0",cursoropacitymax:0.7,boxzoom:true,touchbehavior:true});  // This is an IFrame (iPad compatible)
    
    $("#boxscroll4").niceScroll("#boxscroll4 .wrapper",{boxzoom:true});  // hw acceleration enabled when using wrapper
    
  });
</script>
<!-- scroll -->
    
</footer>

                

</body>
</html>