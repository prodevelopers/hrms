/**
 * Created by Syed Haider Hassan on 12/5/2014.
 */
/**
 * Created by Syed Haider Hassan on 8/25/14.
 */
;(function(window) {
    var
// Is Modernizr defined on the global scope
        Modernizr = typeof Modernizr !== "undefined" ? Modernizr : false,
// whether or not is a touch device
        isTouchDevice = Modernizr ? Modernizr.touch : !!('ontouchstart' in window || 'onmsgesturechange' in window),
// Are we expecting a touch or a click?
        buttonPressedEvent = (isTouchDevice) ? 'touchstart' : 'click',
        Parexons = function() {
            this.init();
        };
// Initialization method
    Parexons.prototype.init = function() {
        this.isTouchDevice = isTouchDevice;
        this.buttonPressedEvent = buttonPressedEvent;
    };
    Parexons.prototype.getViewportHeight = function() {
        var docElement = document.documentElement,
            client = docElement.clientHeight,
            inner = window.innerHeight;
        if (client < inner)
            return inner;
        else
            return client;
    };
    Parexons.prototype.getViewportWidth = function() {
        var docElement = document.documentElement,
            client = docElement.clientWidth,
            inner = window.innerWidth;
        if (client < inner)
            return inner;
        else
            return client;
    };
// Creates a "Parexons" object.
    window.Parexons = new Parexons();
})(this);
;(function($){
    "use strict";
    Parexons.notification = function(text,notyficationType) {
        /*----------- BEGIN validationEngine CODE -------------------------*/
        noty({
            text: text,
            type: notyficationType,
            theme: 'defaultTheme',
            timeout:3600,
            closeWith: ['click']
        });
        /*----------- END validate CODE -------------------------*/
    };
    return Parexons;
})(jQuery);