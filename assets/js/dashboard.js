$(document).ready(function() {
  $("#zoomin").click(function(){
     $("canvas[id='district']").hide(200);
     $("canvas[id='district1']").show(3500);
     // $("canvas[id='projects']").parent('div[class="centerto"]').addClass('center');
     $("#target_district").addClass('chart-resize center',1500);
     $("#target_district").removeClass('chart',3000);

  });
   $("#zoomout").click(function(){
     $("canvas[id='district']").show(3000);
     $("canvas[id='district1']").hide(1500);
     $("#target_district").addClass('chart',3000);
     $("#target_district").removeClass('chart-resize center',1500);

  });
   //////////////////////////////////////////////////////////////////
   $("#zoomind").click(function(){
     $("canvas[id='projects']").hide(200);
     $("canvas[id='projects1']").show(3500);
     // $("canvas[id='projects']").parent('div[class="centerto"]').addClass('center');
     $("#target_project").addClass('chart-resize center',1500);
     $("#target_project").removeClass('chart',3000);
  });
   $("#zoomoutd").click(function(){
     $("canvas[id='projects']").show(3000);
     $("canvas[id='projects1']").hide(1500);
     $("#target_project").addClass('chart',3000);
     $("#target_project").removeClass('chart-resize center',1500);
  });
   ////////////////////////////////////////////////////////////////////////////
   $("#zooming").click(function(){
     $("canvas[id='gender']").hide(200);
     $("canvas[id='gender1']").show(3500);
     // $("canvas[id='projects']").parent('div[class="centerto"]').addClass('center');
     $("#target_gender").addClass('chart-resize center',1500);
     $("#target_gender").removeClass('chart',3000);
  });
   $("#zoomoutg").click(function(){
     $("canvas[id='gender']").show(3000);
     $("canvas[id='gender1']").hide(1500);
     $("#target_gender").addClass('chart',3000);
     $("#target_gender").removeClass('chart-resize center',1500);
  });
///////////////////////////////////////////////////////////////
   $("#zoominl").click(function(){
     $("canvas[id='leavers']").hide(200);
     $("canvas[id='leavers1']").show(3500);
     // $("canvas[id='projects']").parent('div[class="centerto"]').addClass('center');
     $("#target_leavers").addClass('chart-resize center',1500);
     $("#target_leavers").removeClass('chart',3000);
  });
   $("#zoomoutl").click(function(){
     $("canvas[id='leavers']").show(3000);
     $("canvas[id='leavers1']").hide(1500);
     $("#target_leavers").addClass('chart',3000);
     $("#target_leavers").removeClass('chart-resize center',1500);
  });
     ///////////////////////////////////////////////////////////////
   $("#zoominj").click(function(){
     $("canvas[id='joiners']").hide(200);
     $("canvas[id='joiners1']").show(3500);
     // $("canvas[id='projects']").parent('div[class="centerto"]').addClass('center');
     $("#target_joiner").addClass('chart-resize center',1500);
     $("#target_joiner").removeClass('chart',3000);
  });
   $("#zoomoutj").click(function(){
     $("canvas[id='joiners']").show(3000);
     $("canvas[id='joiners1']").hide(1500);
     $("#target_joiner").addClass('chart',3000);
     $("#target_joiner").removeClass('chart-resize center',1500);
  });
});