/**
 * Created by Syed Haider Hassan. on 12/5/2014.
 */
function commonDataTables(selector,url,aoColumns,sDom,HiddenColumnID,RowCallBack,DrawCallBack){
    oTable = selector.dataTable({
        "bProcessing": true,
        "bPaginate" :true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "bDestroy":true,
        "sServerMethod": "POST",
        "aaSorting": [[ 0, "asc" ]],
        "sDom" : sDom,
        "aoColumns":aoColumns,
        "sAjaxSource": url,
        "iDisplayLength": 10,
        'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': url,
                'data': aoData,
                'success': fnCallback
            }); //end of ajax
        },
        'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if(typeof HiddenColumnID !== "undefined"){
                $(nRow).attr("data-id",aData[HiddenColumnID]);
            }else{
                $(nRow).attr("data-id",aData[0]);
            }

            if(typeof RowCallBack !== "undefined"){
                eval(RowCallBack);
            }
            return nRow;
        },
        fnDrawCallback : function (oSettings) {
            if(typeof DrawCallBack !== "undefined"){
                eval(DrawCallBack);
            }
        }
    });
}
function commonDataTablesFiltered(selector,url,aoColumns,sDom,HiddenColumnID,filters,RowCallBack,DrawCallBack){
    oTable = selector.dataTable({
        "bProcessing": true,
        "bPaginate" :true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "bDestroy":true,
        "sServerMethod": "POST",
        "tableTools": {
            "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "os",
            "sRowSelector": 'td:first-child',
            // "aButtons": [ "copy", "csv", "xls","pdf","print","select_all", "select_none" ]
            "aButtons": [
                "copy",
                "print", {
                    "sExtends": "collection",
                    "sButtonText": "Save", // button name
                    // "aButtons":    [ "csv", "xls", "pdf" ]
                    "aButtons": [
                        "csv",
                        {
                            "sExtends": "xls",
                            "sButtonText": "Excel",
                            "sFileName": "*.xls"
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sPdfMessage": "List of product."
                        },
                        "print"
                    ]
                }
            ]
        },
        "aaSorting": [[ 0, "asc" ]],
        "sDom" : sDom,
        "aoColumns":aoColumns,
        "sAjaxSource": url,
        "iDisplayLength": 10,
        'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': url,
                'data': aoData,
                'success': fnCallback
            }); //end of ajax
        },
        'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if(typeof HiddenColumnID !== "undefined"){
                $(nRow).attr("data-id",aData[HiddenColumnID]);
            }else{
                $(nRow).attr("data-id",aData[0]);
            }
            if(typeof RowCallBack !== "undefined"){
                eval(RowCallBack);
            }
            return nRow;
        },
        fnDrawCallback : function (oSettings) {
            if(typeof DrawCallBack !== "undefined"){
                eval(DrawCallBack);
            }
        },
        "fnServerParams": function (aoData, fnCallBack){
            if(typeof filters !== "undefined"){
                eval(filters);
            }
        }
    });
}

function commonDataTablesTableTools(selector,url,aoColumns,sDom,HiddenColumnID,aButtons,RowCallBack,DrawCallBack){
    oTable = selector.dataTable({
        "bProcessing": true,
        "bPaginate" :true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "bDestroy":true,
        "sServerMethod": "POST",
        "tableTools": {
            "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "os",
            "sRowSelector": 'td:first-child',
            // "aButtons": [ "copy", "csv", "xls","pdf","print","select_all", "select_none" ]
            "aButtons": aButtons
        },
        "aaSorting": [[ 0, "asc" ]],
        "sDom" : sDom,
        "aoColumns":aoColumns,
        "sAjaxSource": url,
        "iDisplayLength": 10,
        'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': url,
                'data': aoData,
                'success': fnCallback
            }); //end of ajax
        },
        'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if(typeof HiddenColumnID !== "undefined"){
                $(nRow).attr("data-id",aData[HiddenColumnID]);
            }else{
                $(nRow).attr("data-id",aData[0]);
            }

            if(typeof RowCallBack !== "undefined"){
                eval(RowCallBack);
            }
            return nRow;
        },
        fnDrawCallback : function (oSettings) {
            if(typeof DrawCallBack !== "undefined"){
                eval(DrawCallBack);
            }
        }
    });
}

function commonDataTablesFilteredTableTools(selector,url,aoColumns,sDom,HiddenColumnID,aButtons,filters,RowCallBack,DrawCallBack){
    oTable = selector.dataTable({
        "bProcessing": true,
        "bPaginate" :true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "bDestroy":true,
        "sServerMethod": "POST",
        "tableTools": {
            "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "os",
            "sRowSelector": 'td:first-child',
            "aButtons": aButtons
        },
        "aaSorting": [[ 0, "asc" ]],
        "sDom" : sDom,
        "aoColumns":aoColumns,
        "sAjaxSource": url,
        "iDisplayLength": 25,
        'fnServerData' : function(sSource, aoData, fnCallback){
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': url,
                'data': aoData,
                'success': fnCallback
            }); //end of ajax
        },
        'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if(typeof HiddenColumnID !== "undefined"){
                $(nRow).attr("data-id",aData[HiddenColumnID]);
            }else{
                $(nRow).attr("data-id",aData[0]);
            }
            if(typeof RowCallBack !== "undefined"){
                eval(RowCallBack);
            }
            return nRow;
        },
        fnDrawCallback : function (oSettings) {
            if(typeof DrawCallBack !== "undefined"){
                eval(DrawCallBack);
            }
        },
        "fnServerParams": function (aoData, fnCallBack){
            if(typeof filters !== "undefined"){
                eval(filters);
            }
        }
    });
}
/*----------- Select2 DropDown Common Script -------------------------*/
//For ServerSide Script
function commonSelect2(selector,url,id,text,minInputLength,placeholder,multiple){
    selector.select2({
        minimumInputLength:minInputLength,
        placeholder:placeholder,
        multiple:multiple,
        ajax: {
            type:"post",
            url: url,
            dataType: 'json',
            quietMillis: 100,
            data: function(term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function(data, page) {
                var newData = [];
                $.each(data, function (index,value) {
                    newData.push({
                        id: value[id], //id part present in data
                        text: value[text] //string to be displayed
                    });
                });
                return { results: newData };
            }
        },
        initSelection : function (element, callback) {
            var data = element.val().split(',');
            var completeData = {id: data[0], text: data[1]};
//console.log(completeData);
            callback(completeData);
        }
    });
}
//For ServerSide Script
function commonSelect2Depend(selector,url,id,text,minInputLength,placeholder,multiple,dependentWhere){
    selector.select2({
        minimumInputLength:minInputLength,
        placeholder:placeholder,
        multiple:multiple,
        ajax: {
            type:"post",
            url: url,
            dataType: 'json',
            quietMillis: 100,
            data: function(term, page) {
                return {
                    term: term, //search term
                    dependent:dependentWhere,
                    page_limit: 10 // page size
                };
            },
            results: function(data, page) {
                var newData = [];
                $.each(data, function (index,value) {
                    newData.push({
                        id: value[id], //id part present in data
                        text: value[text] //string to be displayed
                    });
                });
                return { results: newData };
            }
        },
        initSelection : function (element, callback) {
            var data = element.val().split(',');
            var completeData = {id: data[0], text: data[1]};
//console.log(completeData);
            callback(completeData);
        }
    });
}
//For ServerSide with Templating
function commonSelect2Templating(selector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout){
    if(typeof templateLayout !== "undefined"){
        eval(templateLayout);
    }
    else{
        function format(state){
            return state.text;
        }
    }
//console.log(templateLayout);
    selector.select2({
        minimumInputLength:minInputLength,
        placeholder:placeholder,
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function(m) { return m; },
        ajax: {
            type:"post",
            url: url,
            dataType: 'json',
            quietMillis: 100,
            data: function(term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function(data, page) {
                if(typeof(data)!== 'string'){
                    var newData = [];
                    $.each(data, function (index,value) {
                        var obj = {};
                        for(var key in tDataValues) {
                            obj[key] = value[tDataValues[key]];
                        }
                        newData.push(obj);
                    });
                    return { results: newData };
                }else{
                    var newData = [];
                    var nData = data.split('::');
                    HRS.notification(nData[1],nData[2]);
                    return { results: newData };
                }
            }
        },
        initSelection : function (element, callback) {
            var data = element.val().split(',');
            var completeData = {id: data[0], text: data[1]};
//console.log(completeData);
            callback(completeData);
        }
    });
}
$(".commonGeneralSelect2").select2({
    placeholder: "Select a State",
    allowClear: true
});
