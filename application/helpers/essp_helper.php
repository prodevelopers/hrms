<?php
/*
*/
function getEmpPersonalInformation($employeeID = NULL)
{
    if ($employeeID !== NULL and $employeeID > 0) {
        $ci = $ci =& get_instance();
        $table = 'employee E';
        $data = array(
            'E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.full_name AS EmployeeName, E.father_name AS EmployeeFatherName,
            E.CNIC AS CNIC, E.date_of_birth AS EmployeeDOB,
            E.marital_status AS MaritalStatusID, MLMS.marital_status_title AS MaritalStatusText,
            E.nationality AS NationalityID, MLN.nationality AS NationalityText,
            E.religion AS ReligionID, MLG.religion_title AS ReligionText,
            CC.address AS EmployeeAddress,
            CC.city_village AS ccCityVillageID, MLccC.city_name AS ccCityVillageText,
            CC.district AS ccDistrictID, MLccD.district_name AS ccDistrictName,
            CC.home_phone AS EmployeeHomePhone,
            CC.office_phone AS EmployeeWorkPhone,
            CC.mob_num AS EmployeeMobilePhone,
            CC.email_address AS ccEmployeeEmail,
            CC.official_email AS ccOfficialEmail,
            PC.address AS pcEmployeeAddress,
            PC.city_village AS pcCityVillageID, MLpcC.city_name AS pcCityVillageText,
            PC.district AS pcDistrictID, MLpcD.district_name AS pcDistrictName,
            PC.province AS pcProvinceID, MLpcP.province_name AS pcProvinceName,
            PC.phone AS pcEmployeePhone,
            EC.home_phone AS ecHomePhone,
            EC.mobile_num AS ecMobilePhone,
            EC.cotact_person_name AS EmergencyContactPersonName,
            EC.work_phone AS ecWorkPhone,
            EC.relationship AS ecPersonRelationID, MLecR.relation_name AS ecPersonRelationName',
            false
        );
        $joins = array(
            array(
                'table' => 'ml_marital_status MLMS',
                'condition' => 'MLMS.marital_status_id = E.marital_status AND MLMS.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_nationality MLN',
                'condition' => 'MLN.nationality_id = E.nationality AND MLN.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_religion MLG',
                'condition' => 'MLG.religion_id = E.religion AND MLG.trashed = 0',
                'type' => 'LEFT'
            ),
            //These Below Extra Joins Are For Contacts.
            array(
                'table' => 'current_contacts CC',
                'condition' => 'CC.employee_id = E.employee_id AND CC.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'permanant_contacts PC',
                'condition' => 'PC.employee_id = E.employee_id AND PC.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'emergency_contacts EC',
                'condition' => 'EC.employee_id = E.employee_id AND EC.trashed = 0',
                'type' => 'LEFT'
            ),
            //Current Contacts Joins
            array(
                'table' => 'ml_city MLccC',
                'condition' => 'MLccC.city_id = CC.city_village AND MLccC.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_province MLccP',
                'condition' => 'MLccP.province_id = CC.province AND MLccP.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_district MLccD',
                'condition' => 'MLccD.district_id = CC.district AND MLccD.trashed = 0',
                'type' => 'LEFT'
            ),
            //Permanent Contacts Joins
            array(
                'table' => 'ml_city MLpcC',
                'condition' => 'MLpcC.city_id = PC.city_village AND MLpcC.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_province MLpcP',
                'condition' => 'MLpcP.province_id = PC.province AND MLpcP.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_district MLpcD',
                'condition' => 'MLpcD.district_id = PC.district AND MLpcD.trashed = 0',
                'type' => 'LEFT'
            ),
            //Emergency Contacts Joins
            array(
                'table' => 'ml_relation MLecR',
                'condition' => 'MLecR.relation_id = EC.relationship AND MLecR.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = array(
            'E.enrolled' => 1,
            'E.trashed' => 0,
            'E.employee_id' => $employeeID
        );
        /**
         * @property common_model $common_model
         */
        $ci->load->model('common_model');
        $result = $ci->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);
        return $result;
    }
}