<?php
function check_status($status = NULL)
{
	if($status == 1) 
	{
		return 'Enable'; 
	}else {
		return 'Disable';
	}
}

function approved($approved_by = NULL)
{

	$qry = mysql_query( "select full_name from employee where employee_id = $approved_by");
	$name = mysql_fetch_array($qry);
	if($name)
	{
	 return $name['full_name'];
	}
	else{
			return 'Not Approved';
		}
	//return $qry->result();
}

function delete_all($employee_id){
  return  "<input type='checkbox' value='$employee_id' name='mycheckbox[]' id='mycheckbox' /> ";
    
}

function breadcrumb(){
  $page_uri = $_SERVER['REQUEST_URI']; 
  $page_uri = explode('/', $page_uri);
  $page_uri = $page_uri[3];
  $page_uri = explode('_', $page_uri);
  $page = ucfirst($page_uri[1]).' / '.ucfirst($page_uri[0]).' '.ucfirst($page_uri[1]);
  return $page;
}


if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

//A Function To Sort A MultiDimensional Array
if (!function_exists('array_sort_by_column')) {
    function array_sort_by_column(&$array, $column, $direction = SORT_ASC)
    {
        $reference_array = array();

        foreach ($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
    }
}

if (!function_exists('array_order_by')){
    function array_order_by()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
}

if (!function_exists('createBarChartData')) {
    function createBarChartData($queryResultArray,$idForBar,$timeInterval){
        if(isset($queryResultArray) && isset($queryResultArray) && isset($queryResultArray) && (!empty($queryResultArray) && !empty($idForBar))){
            $pastYears = intval($timeInterval);
            $rangeYear = $pastYears - intval(1);
            $last5Years = range(date("Y"), date("Y",strtotime("-".$rangeYear." year")));

            //Now As We Got The Data and Duration. We Will Populate the Json Array for the Charts.js.
            $totalGendersAvailable = array_count_values(array_column(json_decode(json_encode($queryResultArray),true),$idForBar));
            $barColors = array(
                0 => array(
                    0 => 'rgba(215, 40, 40, 0.9)',
                    1 => 'rgba(0, 0, 0, 0.4)',
                    2 => 'rgba(220,220,220,0.75)',
                    3 => 'rgba(220,220,220,1)'
                ),
                1 => array(
                    0 => 'rgba(65, 116, 44, 0.9)',
                    1 => 'rgba(220,220,220,0.8)',
                    2 => 'rgba(220,220,220,0.75)',
                    3 => 'rgba(220,220,220,1)'
                ),
                2 => array(
                    0 => 'rgba(118, 155, 102, 1)',
                    1 => 'rgba(151,187,205,0.8)',
                    2 => 'rgba(151,187,205,0.75)',
                    3 => 'rgba(151,187,205,1)'
                ),
                3 => array(
                    0 => 'rgba(71, 205, 102, 1)',
                    1 => 'rgba(210,220,220,0.8)',
                    2 => 'rgba(220,220,220,0.75)',
                    3 => 'rgba(220,220,220,1)'
                )
            );
            $datasets = array();
            $i=0;
            foreach($totalGendersAvailable as $key=>$value){
                $label = $key;
                $fillColor =  $barColors[$i][0];
                $strokeColor = $barColors[$i][1];
                $highlightFill = $barColors[$i][2];
                $highlightStroke = $barColors[$i][3];
                $array = array(
                    'label' => $label,
                    'fillColor' => $fillColor,
                    'strokeColor' => $strokeColor,
                    'highlightFill' => $highlightFill,
                    'highlightStroke' => $highlightStroke,
                    'data' => array()
                );
                array_push($datasets,$array);
                foreach($last5Years as $yearKey=>$year){
                    foreach($queryResultArray as $yearData){
                        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $yearData->Date))
                        {
                            // one or more of the 'special characters' found in $string
                            $innerYear = date("Y",strtotime($yearData->Date));
                        }else{
                            $innerYear = $yearData->Date;
                        }
                        if($innerYear == $year && $key == $yearData->$idForBar){
                            $ifDataArray = array(
                                $yearKey => intval($yearData->Total)
                            );
                            if(!isset($datasets[$i]['data'][$yearKey])){
                                array_push($datasets[$i]['data'],intval($yearData->Total));
                            }else{
                                $datasets[$i]['data'] = array_replace($datasets[$i]['data'],$ifDataArray);
                            }
                        }else{
                            if(!isset($datasets[$i]['data'][$yearKey])){
                                array_push($datasets[$i]['data'],intval(0));
                            }
                        }
                    }
                }
                $i++;
            }
            $barChartData = array(
                'labels' => $last5Years,
                'datasets' => $datasets
            );

            return json_encode($barChartData);
        }else{
            //Return Nothing when If Statement is not True.
            return;
        }
    }
}

if(!function_exists('rand_color')){
    function rand_color() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }
}

if(!function_exists('stripAttributes')){
    function stripAttributes($s, $allowedattr = array()) {
        if (preg_match_all("/<[^>]*\\s([^>]*)\\/*>/msiU", $s, $res, PREG_SET_ORDER)) {
            foreach ($res as $r) {
                $tag = $r[0];
                $attrs = array();
                preg_match_all("/\\s.*=(['\"]).*\\1/msiU", " " . $r[1], $split, PREG_SET_ORDER);
                foreach ($split as $spl) {
                    $attrs[] = $spl[0];
                }
                $newattrs = array();
                foreach ($attrs as $a) {
                    $tmp = explode("=", $a);
                    if (trim($a) != "" && (!isset($tmp[1]) || (trim($tmp[0]) != "" && !in_array(strtolower(trim($tmp[0])), $allowedattr)))) {

                    } else {
                        $newattrs[] = $a;
                    }
                }
                $attrs = implode(" ", $newattrs);
                $rpl = str_replace($r[1], $attrs, $tag);
                $s = str_replace($tag, $rpl, $s);
            }
        }
        return $s;
    }
}
/**
 * @function previousURL return URL
 */
if(!function_exists('previousURL')){
    function previousURL(){
        if (isset($_SERVER['HTTP_REFERER']))
        {
            return $_SERVER['HTTP_REFERER'];
        }
        else
        {
           return base_url();
        }
    }
}

function date_format_helper($date)
{
    if(!empty($date)){
   echo  $new_date=date("d-M-Y",strtotime($date));
    }else{

    }
}

function date_format2_helper($date)
{
    if(!empty($date)){
        echo  $new_date=date("d-m-Y",strtotime($date));
    }else{

    }
}


//Get Active Employment For The Active Employee
function get_employment_from_employeeID($employeeID)
{
    $ci = $ci =& get_instance();
    $table = 'employment ET';
    $data = 'employment_id AS EmploymentID';
    $where = array(
        'current' => 1,
        'trashed' => 0,
        'employee_id' => $employeeID
    );
    $ci->load->model('common_model');
    $employmentInfo = $ci->common_model->select_fields_where($table, $data, $where, TRUE);
    if (isset($employmentInfo) && !empty($employmentInfo)) {
        $employmentID = $employmentInfo->EmploymentID;
        return $employmentID;
    } else {
        return FALSE;
    }
}

function get_employee_from_employmentID($employmentID)
{
    $ci = $ci =& get_instance();
    $table = 'employment ET';
    $data = 'employee_id AS EmployeeID';
    $where = array(
        'trashed' => 0,
        'employment_id' => $employmentID
    );
    $ci->load->model('common_model');
    $employmentInfo = $ci->common_model->select_fields_where($table, $data, $where, TRUE);
    if (isset($employmentInfo) && !empty($employmentInfo)) {
        $employeeID = $employmentInfo->EmployeeID;
        return $employeeID;
    } else {
        return FALSE;
    }
}

//Get Active Employee Name By Employment ID
function get_employment_Name_from_employmentID($employeeID)
{
    $ci = $ci =& get_instance();
  $table = 'employee E';
    $data = ('E.full_name AS EmployeeName');
    $join=array(
        array(
            'table'=>  'employment ET',
            'condition'=> 'ET.employee_id=E.employee_id',
            'type'=>       'INNER'
        )
    );
    $ci->load->model('common_model');
    $where = array('ET.current' => 1,
        'ET.trashed' => 0,
        'ET.employment_id' => $employeeID);
    $employmentInfo = $ci->common_model->select_fields_where_like_join($table,$data,$join,$where,TRUE);
    if (isset($employmentInfo) && !empty($employmentInfo)) {
        $employmentName = $employmentInfo->EmployeeName;
        return $employmentName;
    } else {
        return FALSE;
    }
}
function addQuotes($string) {
    return '"'. implode('","', explode(',', $string)) .'"';
}

//Get Active Employment For The Active Employee
function Active_currency($amount)
{
    $ci = $ci =& get_instance();
    $table = 'ml_currency MC';
    $data = 'currency_name AS currency';
    $where = array(
        'active' => 1,
        'trashed' => 0,
        'enabled' => 1
    );
    $ci->load->model('common_model');
    $CurrencyInfo = $ci->common_model->select_fields_where($table, $data, $where, TRUE);
    if (isset($CurrencyInfo) && !empty($CurrencyInfo)) {
        $Currency = $CurrencyInfo->currency;
        if(!empty($amount)){
        return '<b>'.$Currency.'</b>'.' '.$amount;}
    } else {
        return FALSE;
    }
}

//Get Active Employment For The Active Employee
function currency_label()
{
    $ci = $ci =& get_instance();
    $table = 'ml_currency MC';
    $data = 'currency_name AS currency';
    $where = array(
        'active' => 1,
        'trashed' => 0,
        'enabled' => 1
    );
    $ci->load->model('common_model');
    $CurrencyInfo = $ci->common_model->select_fields_where($table, $data, $where, TRUE);
    if (isset($CurrencyInfo) && !empty($CurrencyInfo)) {
        $Currency = $CurrencyInfo->currency;

        return $Currency;
    }else{
        return false;
    }
}

function createDateRangeArray($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}

  function yearRang()
    {
        $year=date("Y");
        $minusYear=$year - 50;
        $plusYear=$year + 10;
        $rang=$minusYear.":".$plusYear;
        return $rang;
    }
?>