<?php
/**
 * Created by PhpStorm.
 * User: HaiderHassan
 * Date: 1/11/2015
 * Time: 8:38 PM
 */

if (!function_exists('getApprovalsSideMenus')) {
    //Need This Functions To Get The Data for the Approvals Left SIde Menus..
    // Moving All The Code to One Place. So It would Be Easy in Future to Update Records Easily..
    function getApprovalsSideMenus()
    {
        $ci = $ci =& get_instance();
        $ci->load->model('common_model');
        $loggedInEmployeeID = $ci->session->userdata('employee_id');

        //Lets Get The Where Statements///
        if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
            $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
            $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
            $subOrdinatesEmploymentIDs = $ci->common_model->select_fields_where('employment ET','employment_id AS EmploymentID','trashed = 0 AND current = 1 AND employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')');
            $subOrdinatesEmploymentIDs = array_column(json_decode(json_encode($subOrdinatesEmploymentIDs),true),'EmploymentID');
            $subOrdinatesEmploymentIDsImploded = implode(',',$subOrdinatesEmploymentIDs);

            //Different Where Statements.
            $whereLeaveEntitlement = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereExpenseClaims = 'status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $wherePositionManagement = 'status = 1 AND current = 1 AND trashed = 0 AND employement_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereTransaction = 'status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereSeparation = 'status = 1 AND employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            $whereDeduction = 'status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereLoanAdvances = 'status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereBenefits = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereAllowance = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereIncrements = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $wherePosting = 'trashed = 0 AND status = 1 AND current = 1 AND employement_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereLeaveApproval = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereSalary = 'trashed = 0 AND status = 1 AND employement_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereContract = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereEOS = 'trashed = 0 AND status = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereTimeSheet = 'timesheet_status_id = 1 AND employment_id IN ('.$subOrdinatesEmploymentIDsImploded.')';
            $whereDisciplineReviews = 'status = 1 AND employmentID IN ('.$subOrdinatesEmploymentIDsImploded.')';
        }else{
            $whereLeaveEntitlement = 'trashed = 0 AND status = 1';
            $whereExpenseClaims = 'status = 1';
            $wherePositionManagement = 'status = 1 AND current = 1 AND trashed = 0';
            $whereTransaction = 'status = 1';
            $whereSeparation = 'status = 1';
            $whereDeduction = 'status = 1';
            $whereLoanAdvances = 'status = 1';
            $whereBenefits = 'trashed = 0 AND status = 1';
            $whereAllowance = 'trashed = 0 AND status = 1';
            $whereIncrements = 'trashed = 0 AND status = 1';
            $wherePosting = 'trashed = 0 AND status = 1 AND current = 1';
            $whereLeaveApproval = 'trashed = 0 AND status = 1';
            $whereSalary = 'trashed = 0 AND status = 1';
            $whereContract = 'trashed = 0 AND status = 1';
            $whereEOS = 'trashed = 0 AND status = 1';
            $whereTimeSheet = 'timesheet_status_id = 1';
            $whereDisciplineReviews = 'status = 1';
        }

        //C
        $data['separation'] = $ci->site_model->count_by_where('seperation_management', $whereSeparation);
        $data['deduction'] = $ci->site_model->count_by_where('deduction', $whereDeduction);
        $data['expense_claims'] = $ci->site_model->count_by_where('expense_claims', $whereExpenseClaims);
        $data['entitlement'] = $ci->site_model->count_by_where('leave_entitlement', $whereLeaveEntitlement);
        $data['position'] = $ci->site_model->count_by_where('position_management', $wherePositionManagement);
        $data['count'] = $ci->site_model->count_by_where('transaction', $whereTransaction);
        $data['loan'] = $ci->site_model->count_by_where('loan_advances', $whereLoanAdvances);
        $data['benefits'] = $ci->site_model->count_by_where('benefits', $whereBenefits);
        $data['allowance'] = $ci->site_model->count_by_where('allowance', $whereAllowance);
        $data['increments'] = $ci->site_model->count_by_where('increments', $whereIncrements);
        $data['posting'] = $ci->site_model->count_by_where('posting', $wherePosting);
        $data['leave_approval'] = $ci->site_model->count_by_where('leave_approval', $whereLeaveApproval);
        $data['salary'] = $ci->site_model->count_by_where('salary', $whereSalary);
        $data['contract'] = $ci->site_model->count_by_where('contract', $whereContract);
        $data['eos'] = $ci->site_model->count_by_where('eos',$whereEOS);
        $data['manualTimeSheet'] = $ci->site_model->count_by_where('timesheet', $whereTimeSheet);
        $data['disciplineReview'] = $ci->site_model->count_by_where('disciplinary_action_reports', $whereDisciplineReviews);
        //End C

        /////////No Need To Make Two Different Wheres For This As Line Manager is Not Supposed To Select Applicants For Employees.
        //Selected Applicants Approvals
        $ApplicantsReviews = array(
            'selected_approval' => 1
        );
        $data['ApplicantsReviews'] = $ci->site_model->count_by_where('shortlisted_interview', $ApplicantsReviews);
        return $data;
    }
}

if(!function_exists('DoNotLetNumeric')){
    function DoNotLetNumeric($inputString = "",$redirectPath, $msg= NULL){
        if(is_numeric($inputString))
        {
            $ci =& get_instance();
            if($msg !== NULL && is_string($msg)){
                $Alert=$msg;
            }else{
                $Alert="Please Enter Some Valid Data::warning";
            }
            $ci->session->set_flashdata('msg',$Alert);
            redirect($redirectPath);
            return NULL;
        }
    }
}
?>