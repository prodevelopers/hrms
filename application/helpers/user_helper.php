<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/6/2015
 * Time: 2:26 PM
 */

function LoggedIn(){
    $ci =& get_instance();
    if($ci->session->userdata('logged_in')){
        return TRUE;
    }
    else{
        return FALSE;
    }
}

if (!function_exists('is_admin')) {
    function is_admin($employeeID)
    {
        $ci =& get_instance();
        $ci->load->model('common_model');
        $table = 'user_account U';
        $data = ('U.user_group AS GroupID');
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'U.employee_id = E.employee_id AND E.trashed = 0',
                'type' => 'INNER'
            )
        );
        $where = array(
            'U.employee_id' => $employeeID
        );
        $result = $ci->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);
        if(isset($result) && !empty($result)){
            $groupID = $result->GroupID;
            if ($groupID == 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
}

if (!function_exists('is_module_Allowed')) {
    function is_module_Allowed($employeeID, $moduleController, $permissionType = NULL)
    {
        $ci =& get_instance();
        $ci->load->model('common_model');
        $table = 'user_account U';
        $data = ('COUNT(1) AS TotalRecords');
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'U.employee_id = E.employee_id AND E.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_user_groups MLUG',
                'condition' => 'MLUG.user_group_id = U.user_group AND MLUG.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'user_groups_privileges UGP',
                'condition' => 'UGP.user_group_id = MLUG.user_group_id AND UGP.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_module_list MLML',
                'condition' => 'MLML.ml_module_id = UGP.ml_module_list_id AND MLML.trashed = 0',
                'type' => 'INNER'
            )
        );
        $where = array(
            'U.employee_id' => $employeeID,
            'MLML.module_controllers' => $moduleController
        );
        if ($permissionType !== NULL) {
            switch ($permissionType) {
                case 'view':
                    $where['UGP.view_only'] = 1;
                    break;
                case 'print':
                    $where['UGP.print'] = 1;
                    break;
                case 'modify':
                    $where['UGP.modify'] = 1;
                    break;
                case 'create':
                    $where['UGP.create_new_record'] = 1;
                    break;
            }
        }
        $result = $ci->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);
        if (isset($result)) {
            if ($result->TotalRecords > 0) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
}
if (!function_exists('is_method_allowed')) {
    function is_method_allowed($employeeID, $viewMethod, $permissionType = NULL)
    {
        $ci =& get_instance();
        $ci->load->model('common_model');
        $table = 'user_account U';
        $data = ('COUNT(1) AS TotalRecords');
        $joins = array(
            array(
                'table' => 'ml_user_groups MLUG',
                'condition' => 'MLUG.user_group_id = U.user_group AND MLUG.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'user_groups_views_privileges UGVP',
                'condition' => 'UGVP.user_group_id = MLUG.user_group_id AND UGVP.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_views MLV',
                'condition' => 'MLV.view_id = UGVP.view_id AND MLV.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = U.employee_id AND E.enrolled = 1 AND E.trashed = 0',
                'type' => 'INNER'
            )
        );
        $where = array(
            'U.employee_id' => $employeeID,
            'MLV.view_method' => $viewMethod
        );

        if ($permissionType !== NULL) {
            switch ($permissionType) {
                case 'view':
                    $where['UGVP.view'] = 1;
                    break;
                case 'print':
                    $where['UGVP.print'] = 1;
                    break;
                case 'modify':
                    $where['UGVP.modify'] = 1;
                    break;
                case 'create':
                    $where['UGVP.create'] = 1;
                    break;
            }
        }

        $result = $ci->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);
        if (isset($result)) {
            if ($result->TotalRecords > 0) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('is_lineManager')) {
    function is_lineManager($employeeID)
    {
        $ci =& get_instance();
        $ci->load->model('common_model');
        $table = 'user_account U';
        $data = ('COUNT(1) AS TotalRecords');
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = U.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'reporting_heirarchy RH',
                'condition' => 'RH.for_employee = E.employee_id AND RH.trashed = 0',
                'type' => 'INNER'
            )
        );
        $where = 'U.employee_id ='.$employeeID.' AND RH.employeed_id IS NOT NULL';

        $result = $ci->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);
        if (isset($result)) {
            if ($result->TotalRecords > 0) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
}

/**
 * @return array SubOrdinates
 */
if (!function_exists('get_lineManager_SubOrdinatesEmployeeIDs')) {
    function get_lineManager_SubOrdinatesEmployeeIDs($lineManagerID)
    {
        if(!isset($lineManagerID) || empty($lineManagerID)){
            return FALSE;
        }
        $ci =& get_instance();
        $ci->load->model('common_model');
        $table = 'reporting_heirarchy RH';
        $data = ('RH.reporting_authority_id AS SubOrdinateID');
        $where = 'RH.for_employee ='.$lineManagerID.' AND RH.reporting_authority_id IS NOT NULL AND RH.trashed = 0';
        $result = $ci->common_model->select_fields_where($table, $data, $where, FALSE);
        if (isset($result) && !empty($result)) {
            //If Line Manager Got Some Sub Ordinates We Need To Get Those SubOrdinates.
            $employeeIDs = array();
            foreach($result as $key=>$row){
                $employeeIDs[$key] = $row->SubOrdinateID;
            }
            return $employeeIDs;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('loginCheckBool')) {
    function loginCheckBool()
    {
        $ci =& get_instance();
        $user_id = $ci->session->userdata('employee_id');
        if (strlen($user_id) <= 0) {
// No Login Information Found in the session Object
// So now we will check if we have in cookies
            if (get_cookie('Username') == true && get_cookie('Password') == true) {
                $ci->login->sign_in("USE_COOKIES");
                return TRUE; //Return True If Cookies Are Found.
            } else {
                return FALSE; // Return False if both cookies and session don't exist or expired.
            }
        } else {
            return TRUE; // Return True if Session Found for the User.
        }
    }
}

if (!function_exists('can_apply_for_job')) {
    function can_apply_for_job($applicantID,$jobAdvertisementID)
    {
        if ($applicantID > 0) {
            $ci =& get_instance();
            $ci->load->model('common_model');
           // $where = array(
            //    'applicantID' => $applicantID
           // );
//            $UserGroupID = $ci->Common_Model->get_by('GroupID', 'users_users', $where, TRUE);
           // $ApplicantType = $ci->common_model->select_fields_where('applicants','applicantType',$where,TRUE);
            //print_r($ApplicantType); die;
            //If No Result Found Then Return False.

                //If Is a Candidate, Then Need To Check If He is Applying Back Again For Same Job Or Some Other Job.
                $where = array(
                    'applicantID' => $applicantID,
                    'jobAdvertisementID' =>$jobAdvertisementID,
                    'trashed' => 0
                );
                $selectData = array('COUNT(1) AS TotalRecordsFound',false);
                $countResult = $ci->common_model->select_fields_where('applicant_applied_jobs',$selectData,$where,TRUE);
                if($countResult->TotalRecordsFound > 0){
                    //If User Already Has Applied For Certain Job, Then Also Needs To Send False.
                    return FALSE;
                }
                return TRUE;

//            return $UserGroupID['GroupID'];
        }
    }
}

/**
 * Need To Find Out If Employee Status Is Active Or Not.
 * @function employeeStatus return bool --TRUE/FALSE
 */
if(!function_exists('employeeStatus')){
    function employeeStatus($employeeID){
        $ci =& get_instance();
        $ci->load->model('common_model');
        $PTable = 'employee E';
        $data = ('COUNT(1) AS TotalRecordsFound');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                'type' => 'INNER'
            )
        );
        $where = array(
           'E.trashed' => 0,
            'E.enrolled' => 1,
            'E.employee_id' => $employeeID
        );
       $statusInfo = $ci->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, TRUE);
        if(isset($statusInfo) && $statusInfo->TotalRecordsFound > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}


if(!function_exists('getUserAvatar')){
    function get_user_avatar($employeeID, $avatarType = NULL){
        ///Need Link For User Avatar..
        $ci =& get_instance();
        $ci->load->model('common_model');
        //First Need To Check If User Has Any Avatar Set or Not.
        $empTable = 'employee E';
        $selectData = array('E.employee_id AS EmployeeID, E.photograph AS EmployeeAvatar, E.thumbnail AS EmployeeAvatarThumbnail');
        $where = array(
            'E.employee_id' => $employeeID
        );
        $employeeData = $ci->common_model->select_fields_where($empTable,$selectData,$where,TRUE);
        if(!is_numeric($employeeID)){
            return false;
        }
        if(isset($employeeData) && !empty($employeeData)){
            //If We Get the EmployeeData Then Need To Check If Avatar Is Set and That Avatar is Current Present In the System.
            if(empty($employeeData->EmployeeAvatar)){
                return base_url('assets/images/defaultAvatar.png');
            }
            //If We Reached up to this Point Means Employee Has Avatar In The Database. Only Now Need To Check if That Image/Avatar Exist in The System..
            if($avatarType === 'photograph'){
                $avatarPath = base_url('upload').'/'.$employeeData->EmployeeAvatar;
                $uploadPathWithoutBaseURL = FCPATH."upload/".$employeeData->EmployeeAvatar;
            }elseif($avatarType === 'thumb'){
                $avatarPath = base_url('upload/Thumb_Nails').'/'.$employeeData->EmployeeAvatarThumbnail;
                $uploadPathWithoutBaseURL = FCPATH."upload/Thumb_Nails/".$employeeData->EmployeeAvatar;
            }else{
                $avatarPath = base_url('upload/Thumb_Nails').'/'.$employeeData->EmployeeAvatarThumbnail;
                $uploadPathWithoutBaseURL = FCPATH."upload/Thumb_Nails/".$employeeData->EmployeeAvatar;
            }
            $allowedExt = array('jpeg','jpg','png','gif');
            if(!in_array(strtolower(end(explode('.',$employeeData->EmployeeAvatar))),$allowedExt)){
                //If There is Data but that data is not OK. Means Wrong Entry Done By Any User, Then This Section Should Execute.
                return base_url('assets/images/defaultAvatar.png');
            }
//             return base_url('upload/Thumb_Nails').'/'.$employeeData->EmployeeAvatarThumbnail;
            if(file_exists($uploadPathWithoutBaseURL)){
                return $avatarPath;
            }else{
                return base_url('assets/images/defaultAvatar.png');
            }
        }else{
            return base_url('assets/images/defaultAvatar.png');
        }
    }
}

if(!function_exists('empAvatarExist')) {
    function empAvatarExist($empAvatar){
        //Just Only Need To Check If Avatar Exist On Server Show, Else Don't Show..
        if(!isset($empAvatar) || empty($empAvatar)){
            return base_url('assets/images/defaultAvatar.png');
        }
        if(file_exists(FCPATH."upload/Thumb_Nails/".$empAvatar)){
            return TRUE;
        }elseif(file_exists(FCPATH."upload/".$empAvatar)){
            return TRUE;
        }else{
            return base_url('assets/images/defaultAvatar.png');
        }
    }
}