<?php 
function hr_status($status = NULL)
{
	if($status == 1) 
	{
		return 'Enable'; 
	}else {
		return 'Disable';
	}
	
}
function hr_retire($status = NULL)
{
	if($status == 1) 
	{
		return 'Not Retired'; 
	}else {
		return 'Retired';
	}
}

if(!function_exists('update_employee_tax_statement')){
	function update_employee_tax_statement($employmentID){
        //First Check If Employee Has His Salary and Contract Set.

        $ci =& get_instance();
        $ci->load->model('common_model');


        //Checking Up Salary
        $salaryTable = "salary S";
        $salarySelectData = "*";
        $salaryWhere = array(
            'S.employement_id' => $employmentID,
            'S.status' => 2,
            'S.trashed' => 0
        );

        $salaryInfo = $ci->common_model->select_fields_where($salaryTable,$salarySelectData,$salaryWhere);

        if(empty($salaryInfo)){
            return "Missing Salary, Please Set Current Employee's Employment Salary";
        }else{
            $employeeSalary = $salaryInfo->base_salary;
            $employeeSalaryDateApproved = $salaryInfo->date_approved;
            $employeeSalaryDateModified = $salaryInfo->date_modified;
        }

        //Checking Up Contract
        $contractTable = "contract C";
        $contractSelectData = "*";
        $contractJoins = array(
            array(
                'table' => 'employee_contract_extentions ECE',
                'condition' => 'ECE.contract_id = C.contract_id',
                'type' => 'INNER'
            )
        );
        $contractWhere = array(
            'C.employement_id' => $employmentID,
            'C.status' => 2,
            'C.trashed' => 0
        );

        $contractInfo = $ci->common_model->select_fields_where_like_join($contractTable,$contractSelectData,$contractJoins,$contractWhere);

        if(empty($contractInfo)){
            return "Missing Contract Information, Please Set Current Employee's Employment Contract";
        }else{
            //Contract Start Date
            //Contract End Date
        }

    }
}
?>