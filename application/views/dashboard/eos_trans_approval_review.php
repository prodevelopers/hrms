<?php
$joiningDate=$details->contract_start_date;
$toDate=$details->contract_expiry_date;
$lastMonth=date("m",strtotime($toDate));
$lastDays=date("d",strtotime($toDate));
$diff= abs(strtotime($joiningDate) - strtotime($toDate));
$years   = floor($diff / (365*60*60*24));
$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days  = floor(($diff - $months * 30*60*60*24) / (24*60*60));

if($lastDays > 0)
{$to_pay=round(($details->base_salary/30) * ($lastDays));}else
{$to_pay=round($$details->base_salary);}
?>
<!-- contents -->
<style>
    .beauty h4{
        font-size:13px;
        font-weight:normal;
    }
    .beauty td{
        font-size: 13px;
    }
    .beauty{
        width:23%;
        margin:1em 0.5em;
        float:left;
    }
    .head1{
        background: #4D6684;
        color: #fff;
        font-weight:500;
        font-size: 0.9em;
        line-height: 20px;
    }
    .bubble {
        position: relative;
        background-color:red;
        color:#000000;
        margin: 0;
        padding:10px;
        text-align:center;
        width:180px;
        -moz-border-radius:10px;
        -webkit-border-radius:10px;
        -webkit-box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        -moz-box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        left: 20%;
    }
    .bubble:after {
        position: relative;
        display: block;
        content: "";
        border-color: red transparent transparent transparent;
        border-style: solid;
        border-width: 10px;
        height:0;
        width:0;
        position:relative;
        bottom:-27px;
        left:1em;
    }
</style>
<script>
   $(document).ready(function(){
       $("#remarks").hide();

   });
</script>
<div class="contents-container">
    <div class="bredcrumb">Dashboard /Approvals /Transactions Approvals /Transactions Approval Action</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/aproval_menu'); ?>
    <script>
        var navigation = responsiveNav(".nav-collapse");
    </script>
    <div class="right-contents1">
        <div class="head" style="font-size:1em; font-weight:bold;">Transaction Details</div>

            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head1">
                    <th colspan="2" style="text-align:left;">Employee Infomation</th>
                </tr>
                <tr class="table-row">
                    <td>Name</td>
                    <td><h4><?php echo @$details->full_name;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>Employee Code</td>
                    <td><h4><?php echo @$details->employee_code;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>New Designation</td>
                    <td><h4><?php echo @$details->designation_name;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>Pay Grade</td>
                    <td><h4><?php echo @$details->pay_grade;?></h4></td>
                </tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Salary Info</th>
                </tr>
                <tr >
                    <td>Base Salary</td>
                    <td><h4><?php echo @$details->base_salary;?></h4></td>
                </tr>
                <tr>
                    <td>Refunding Deductions</td>

                    <td><h4><?php if(!empty($ded_refund->refund_ded_amount)){$ref_ded=$ded_refund->refund_ded_amount; if($months > 0){$total_ded=$ref_ded * $months; echo $total_ded;}}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Allowances</td>

                    <td><h4><?php
                            if(!empty($allw_trans->transaction_amount)){
                                echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Expenses</td>

                    <td><h4><?php
                            if(!empty($exp_trans->transaction_amount)){
                                echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Advances</td>

                    <td><h4><?php
                            if(!empty($advance_trans->transaction_amount))
                            {
                                echo @$advance_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Total Amount</td>

                    <td><h4><?php echo @$salary_info->transaction_amount;?></h4></td>
                </tr>


            </table>
        <table class="table beauty" cellpadding="0" cellspacing="0">

            <tr class="head">
                <th colspan="8" style="text-align:left;">Working days</th>
            </tr>
            <tr class="table-row">
                <td><?php if($lastDays> 0){echo "worked days in this month ";} ?></td>
                <td><h4>
                        <?php if($lastDays> 0){ echo $lastDays;}?> </h4></td>
            </tr>
            <tr class="table-row">
                <td><?php if($lastDays> 0){echo "salary of ".$lastDays." Days ";} ?></td>
                <td><h4>
                        <?php if($lastDays> 0){ echo Active_currency($to_pay);}?> </h4></td>

            </tr>
            <tr class="table-row">
                <td><?php echo "Total Duration of worked: ";?></td>
                <td><?php echo "Years:".$years;?></td>

            </tr>
            <tr class="table-row">
                <td><?php echo "";?></td>
                <td><?php echo "Months:".$months;?></td>
            </tr>
            <tr class="table-row">
                <td><?php echo "";?></td>
                <td><?php echo "Days:".$days;?></td>
            </tr>

        </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Arrears Details</th>
                </tr>
                <tr class="table-row">
                    <td>Arrears </td>
                    <td><h4>
                            <?php echo @$salary_info->arears;?> </h4></td>
                </tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8">Deductions</th>
                </tr>
                <tr class="table-row">
                    <?php
                    $ded_type=@$ded_refund->ded_rectp; $ded_amt=@$ded_refund->ded_recmnt;
                    ?>
                    <td>
                        <?php $explode_dedtyp=explode(",",$ded_type);
                        $explode_dedamt=explode(",",$ded_amt);
                        ?>

                        <?php foreach($explode_dedtyp as $ded_typ)
                        {
                            echo $ded_typ."<br />";

                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_dedamt as $ded_mt)
                            {
                                echo $ded_mt."<br />";

                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total <!--Deductions--> </td>
                    <td><h4><?php
                            $deduction_amount=@$ded_refund->refund_ded_amount;
                            echo $deduction_amount;?></h4></td></tr>
                <tr class="table-row">
                    <td></td>
                    <td></td>
                </tr>
            </table>

            <br class="clear"/>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Allowances</th>
                </tr>

                <tr>
                    <?php
                    $alw_rectp=@$allw_trans->alw_rectp; $alw_recm=@$allw_trans->alw_recm;
                    ?>
                    <td>
                        <?php $explode_alw_rectp=explode(",",$alw_rectp);
                        $explode_alw_recm=explode(",",$alw_recm);
                        ?>

                        <?php foreach($explode_alw_rectp as $alw_typ)
                        {
                            echo $alw_typ."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_alw_recm as $alw_mt)
                            {
                                echo $alw_mt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total</td>
                    <td><h4><?php
                            $allowance_amount=@$allw_trans->total_alw_recm;
                            echo $allowance_amount;?></h4></td></tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Expenses</th>
                </tr>

                <tr>
                    <?php
                    $exp_rectp=@$exp_trans->exp_rectp; $exp_recm=@$exp_trans->exp_recm;
                    ?>
                    <td>
                        <?php $explode_exp_rectp=explode(",",$exp_rectp);
                        $explode_exp_recm=explode(",",$exp_recm);
                        ?>

                        <?php foreach($explode_exp_rectp as $expense_type)
                        {
                            echo $expense_type."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_exp_recm as $expense_amt)
                            {
                                echo $expense_amt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total </td>
                    <td><h4><?php
                            $exp_amount=@$exp_trans->total_exp_recm;
                            echo $exp_amount;?></h4></td></tr>


            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Advances</th>
                </tr>
                <tr>
                    <?php
                    $advance_rectp=@$advance_trans->loan_rectp; $advance_recm=@$advance_trans->loan_recm;
                    ?>
                    <td>
                        <?php $explode_advance_rectp=explode(",",$advance_rectp);
                        $explode_advance_recm=explode(",",$advance_recm);
                        ?>

                        <?php foreach($explode_advance_rectp as $advance_type)
                        {
                            echo $advance_type."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_advance_recm as $advance_amt)
                            {
                                echo $advance_amt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total </td>
                    <td><h4>
                            <?php
                            $advance_amount=@$advance_trans->total_advamount;
                            echo $advance_amount;?></h4>
                    </td></tr>
            </table>

            <table class="table beauty" cellpadding="0" cellspacing="0" style="width:45%">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Total Amount of <?php echo @$details->month." ".$details->year; ?></th>
                </tr>
                <tr >
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><?php echo @$details->salary_date;?></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Net Payable</td>
                    <td><?php /*?><?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?><?php */?>
                        <h4><?php echo "RS: ".$salary_info->transaction_amount." /-";?></h4></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <br class="clear">
        <form action="dashboard_site/update_eos_transaction_status/<?php echo @$salary_info->transaction_id;?>" method="post" name="approve">
            <div id="msg" style="display: none;" class="bubble">Please select status !</div>
            <div class="row">
                <h4>Status</h4>

                <br>
                <?php echo form_dropdown('status',$status,"class='status'","id='status' onchange='check()'");?>
                <input type="hidden" name="alw_trans_id" value="<?php echo @$allw_trans->alw_trans_id; ?>" />
                <input type="hidden" name="ded_trans_id" value="<?php echo @$ded_trans->RefDed_trans_id; ?>" />
                <input type="hidden" name="exp_trans_id" value="<?php echo @$exp_trans->exp_trans_id; ?>" />
                <input type="hidden" name="adv_trans_id" value="<?php echo @$advance_trans->adv_trans_id; ?>" />
                <input type="hidden" name="employment_id" value="<?php echo @$employment_id; ?>" />

                <?php	$expense_ids=explode(",",@$exp_trans->exp_ids);
                foreach($expense_ids as $exp_id)
                {?>
                    <input type="hidden" name="exp_ids[]" value="<?php echo $exp_id;?>" />
                <?php
                }
                ?>

                <?php	$advance_ids=explode(",",@$advance_trans->adv_ids);
                foreach($advance_ids as $adv_ids)
                {?>
                    <input type="hidden" name="adv_ids[]" value="<?php echo $adv_ids;?>" />
                <?php
                }
                ?>
            </div>
            <br class="clear">
        <div class="row" id="remarks">
                <h4>Remarks</h4>
                    <textarea name="remarks" placeholder="Remarks here...." ></textarea>
                    <input type="hidden" name="salary_payment_id" value="<?php echo @$salary_info->salary_payment_id; ?>" />
            </div>
        </form>
            <br class="clear">
            <!-- button group -->
            <div class="row">
                <div class="button-group">
                    <!--<a href="add_employee_personal_info.php"><button class="btn green">Add</button></a>-->
                    <input type="button" name="add" value="Submit" class="btn green" onclick="submit();">
<!--                   <input type="button" name="remarks" class="btn gray"  value="Review Fail" onclick="remark();">-->
                </div>
            </div>

    </div>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript">
       function check()
       {
            var status1=$("#status").val();
            if(status1==3)
            {$("#remarks").show();}
            else{$("#remarks").hide();}
        }
        $( "#accordion" ).accordion();
        $( "#accordion1" ).accordion();
        function submit()
        {
            var status=$("#status").val();
            if(status)
            {document.approve.submit();}
            else
            {
                $("#msg").show();
                $("#msg").fadeOut(7000);
            }
             }

    </script>


