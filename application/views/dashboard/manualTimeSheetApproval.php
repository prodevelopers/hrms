<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/11/2015
 * Time: 1:20 PM
 */

$loggedInEmployee = $this->session->userdata('employee_id');
if(isset($monthYear) && !empty($monthYear)){
    $Year = $monthYear[1];
    $Month = $monthYear[0];
    $MonthNumber =  date('m', strtotime($Month));
    $date = $Year.'-'.$MonthNumber.'-01';
    $end  = $Year.'-'.$MonthNumber.'-' . date('t', strtotime($date));
}else{
    $date = date('Y-m-01');
    $end = date('Y-m-') . date('t', strtotime($date));
}
$monthYear = explode('-',$date);
?>
<?php
$projectData = array(
    'hoursWorkedInProject' => array()
);
$timeSheetData = array(
    'Date' => array(),
    'Day' => array()
);
if(isset($TimeSheetViewData) && !empty($TimeSheetViewData)){
    $totalProjectsInMonth = array_count_values(array_column(json_decode(json_encode($TimeSheetViewData), true), 'ProjectID'));
    while (strtotime($date) <= strtotime($end)) {
        $day_num = date('d', strtotime($date));
        $day_name = date('l', strtotime($date));
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        foreach ($TimeSheetViewData as $key => $value) {
            if (date('d', strtotime($value->WorkDate)) === $day_num) {
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = $value->WorkHours;
                if(!array_key_exists('Total',$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] = 00;
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] += intval($value->WorkHours);
            }else{
//            echo '<br />Date Not Matching'.$day_num;
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                if(isset($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    if(!array_key_exists($monthYear[0].'-'.$monthYear[1].'-'.$day_num,$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                        $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = '00';
                    }
                }
            }
            ksort($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]);
        }
        array_push($timeSheetData['Date'], $day_num);
        array_push($timeSheetData['Day'], $day_name);
    }
//Need to Add the Total Column in the End.
    $timeSheetData['Date']['Total'] = 'Total Hours Worked';
}else{
    $TimeSheetViewData = 'No Projects Assigned to You, So TimeSheet Can Not Be Enabled for You.';
}

//Now Need to Show the Employee Leaves In Time Sheet.
if(!empty($employeeTotalLeaves) && isset($employeeTotalLeaves)){
    $leaves_array = array_filter($employeeTotalLeaves, function($val){
        return (!empty($val->LeaveFrom) && !empty($val->LeaveTo));
    });
    foreach($leaves_array as $leave) {
        $range = array();
        foreach ($timeSheetData as $tsKey => $value) {
            if($tsKey != 'Day' && $tsKey != 'Date'){
                foreach($value as $vDates=>$vHours){
                    //Now Change the Index
                    try {
                        $new_date = new DateTime($vDates);
                        $from = new DateTime($leave->LeaveFrom);
                        $to = new DateTime($leave->LeaveTo);
                        if($new_date >= $from && $new_date <= $to) {
                            $range[] = $vDates . '-'.(empty($leave->HexColor) ? rand_color(): $leave->HexColor);
                            unset($timeSheetData[$tsKey][$vDates]);
                        }
                    } catch(Exception $e) {
                    }
                }
                // changes
                foreach($range as $range_date) {
                    if(!array_key_exists( $range_date , $timeSheetData[$tsKey])){
                        $timeSheetData[$tsKey][$range_date] = '00';
                    }
                }
                // resort
                ksort($timeSheetData[$tsKey]);
            }
        }
    }
}
?>
<style type="text/css">
#pagination
{
    float:left;
    padding:5px;
    margin-top:15px;
}
#pagination a
{
    padding:5px;
    background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
    color:#5D5D5E;
    font-size:14px;
    text-decoration:none;
    border-radius:5px;
    border:1px solid #CCC;
}
#pagination a:hover
{
    border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
    background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
    background-color:#F0F0E1;
}
.paginate_button
{
    padding: 6px;
    background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
    color: #5D5D5E;
    font-size: 14px;
    text-decoration: none;
    border-radius: 5px;

    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    border: 1px solid #CCC;
    margin: 1px;
    cursor:pointer;
}
.paging_full_numbers"
{
    margin-top:8px;
}
.dataTables_info
{
    color:#3474D0;
    font-size:14px;

    margin:6px;
}
.paginate_active
{
    padding: 6px;
    border: 1px solid #3474D0;
    border-radius: 5px;
    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    color: #3474D0;
    font-weight: bold;
}
.dataTables_filter
{
    float:right;
}

#autoSuggestionsList
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;

    display:none;
}
#autoSuggestionsList li
{
    list-style:none;
    cursor:pointer;
    padding:5px;
    font-size:14px;
    font-family:Arial;
    margin:2px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
#autoSuggestionsList li:hover
{
    border:1px solid #CCC;
}
.serch
{
    width: 200px;
    height: 25px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    color: black;
    padding-left: 5px;
    border: 1px solid #999;
}

.designing{
    top:200px;
    right:200px;
}
#employee_inf0
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;
    display:none;
    alignment-adjust:central;
}

#autoSuggestionsList
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;

    display:none;
}
#autoSuggestionsList li
{
    list-style:none;
    cursor:pointer;
    padding:5px;
    font-size:14px;
    font-family:Arial;
    margin:2px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
#autoSuggestionsList li:hover
{
    border:1px solid #CCC;
}
.serch
{
    width: 200px;
    height: 25px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    color: black;
    padding-left: 5px;
    border: 1px solid #999;
}

.designing{
    top:200px;
    right:200px;
}
#employee_inf0
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;
    display:none;
    alignment-adjust:central;
}
table {
    table-layout: fixed;
    width: 150%;
    *margin-left: -100px;/*ie7*/
}
td, th {
    vertical-align: top;
}
td.columnfirst, th.columnfirst {
    width: 100px;
    font-size:14px;
    font-weight: bold;
}
td.inner{
    font-size: 12pt;
    width: 20px;
}
.outer {position:relative;}
div.inner {
    overflow-x:scroll;
    overflow-y:visible;
}
.inputCopyCat{
    -moz-appearance: textfield;
    -webkit-appearance: textfield;
    background-color: white;
    background-color: -moz-field;
    border: 1px solid darkgray;
    box-shadow: 1px 1px 1px 0 lightgray inset;
    font: -moz-field;
    font: -webkit-small-control;
    margin-top: 5px;
    padding: 2px 3px;
    width: 398px;
}
</style>

<!--PLEASE Designer Dont Include Jquery inside View.. It Messes WIth other script Files. Jquery is already included in Head Section. No Need To Include Again.-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> -->
<script src="http://malsup.github.com/jquery.form.js"></script>
<div class="contents-container">
    <div class="bredcrumb">Dashboard / Leave&Attendance / Manual Time Sheet Approval</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/aproval_menu'); ?>
    <div class="right-contents1">
        <div class="head">Mannual Timesheet Approval</div>

        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:75%">
                <thead class="table-head">
                <td colspan="4">Actions</td>
                </thead>
                <tbody>
                <tr class="table-row">
                    <td><input type="button" class="green btn" value="Approve" id="approveBtn"></td>
                    <td><input type="button" class="red btn" value="Decline" id="declineBtn"></td>
                    <td><textarea name="approveDeclineComments" id="approveDeclineComments" placeholder=" Remarks For Related Approval Should Go Here."></textarea></td>
                </tr>
                </tbody>
            </table>
        </div>
        <br class="clear">
        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:70%">
                <thead class="table-head">
                <td>Color Code</td>
                </thead>
                <tbody>
                <tr class="table-row">
                    <td>
                        <ul class="color-legend">
                            <li style = "background: none repeat scroll 0 0 #f90; color:#ffffff;" ><span>Sunday</span></li>
                            <li style="background: none repeat scroll 0 0 #dd2200; color:#ffffff;"><span>Holiday</span></li>
                            <?php
                            if(isset($LeaveTypes) && !empty($LeaveTypes)){
                                foreach($LeaveTypes as $leaveType){
                                    echo "<li style='background: none repeat scroll 0 0 ".$leaveType->LeaveTypeColor."; color:#ffffff;'><span>".$leaveType->LeaveType."</span></li>";
                                }
                            }
                            ?>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>
<div class="right-contents" style="width:79.6%; margin-right:1.2em;">

    <table class="table" style="width:100%;">
        <tr>
            <th>Name:</th>
            <th><?php echo $employeeData->EmployeeName; ?></th>
            <th>Timesheet Month:</th>
            <th> <?php
                if(isset($filterInfo)){
                    echo $filterInfo[0]." ".$filterInfo[1];
                }else{
                    echo date("F Y");
                }

                ?>
            </th>
            <th>Location:</th>
            <th>Swat</th>
        <tr>
    </table>

    <!--    Automated TimeSheet Code-->
    <?php
    function ifColor($dates,$key){

        $attributes = '';
        if($dates === 'Total' && $key === 'Date'){
            $attributes = "rowspan='2' style = 'width:100px;'";
        }
        if($key !== 'Date' && $key !== 'Day' && $dates !== 'Total'){
            $leaveType = explode('-',$dates);
            if(date('l', strtotime($leaveType[0].'-'.$leaveType[1].'-'.$leaveType[2])) === 'Sunday'){
                $attributes = "style='background:none repeat scroll 0 0 #f90;color:#fff;'";
            }elseif(isset($leaveType[3]) && !empty($leaveType[3])){
                $attributes = "style='background:none repeat scroll 0 0 ".$leaveType[3].";color:#fff;'";
            }
        }
        return $attributes;
    }

    if(is_array($TimeSheetViewData) && !is_string($TimeSheetViewData)){
        echo "<div class='outer'><div class='inner'><table class=\"table-responsive1\">";
        echo "<tbody style=\"width:150%\">";

        foreach ($timeSheetData as $key => $value) {
            if($key == 'Day' || $key == 'Date'){
                echo "<tr style='background: none repeat scroll 0 0 #4d6684;color:#fff;'>";
            }else{
                echo "<tr class='tableRow'>";
            }
            if(strpos($key,"::")){
                $key = explode("::",$key);
                echo "<td class=\"columnfirst\" data-project='".$key[0]."'>".$key[1]."</td>";
            }else{
                echo "<td class=\"columnfirst\">".$key."</td>";
            }
            foreach ($value as $vKey=>$valueData){
                if($vKey !== 'Total'){
                    echo "<td ". ifColor($vKey,$key) ." class=\"inner\" td-data='".$vKey."'>";
                }else{
                    echo "<td ". ifColor($vKey,$key) ." class=\"totalTD\" td-data='".$vKey."'>";
                }
                if($key === 'Day'){
                    print_r(substr($valueData, 0, 2));
                }else{
                    print_r($valueData);
                }
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table></div></div>";
    }else{
        echo '<div class="messageBox">'.$TimeSheetViewData.'</div>';
    }
    ?>


    <table cellspacing="0" id="table_list" style="width:50%; float:left; margin-left:0.7em;">
        <thead class="table-head">
        <td colspan="5">Leave Details</td>
        </thead>
        <tr class="table-row">
            <th>Category</th>
            <th>Allocated</th>
            <th>Leaves Taken Yearly</th>
            <th>Leaves Taken Monthly</th>
            <th>Balance</th>
            <!-- <th><b>Total Hours</b></th> -->
        </tr>
        <?php
        //     Now Need to Populate Table of EmployeeLeaves
        foreach($employeeTotalMonthlyAndYearly as $LeaveTypes){
            echo '<tr class="table-row" style="text-align:center;">';
            echo '<td>'.$LeaveTypes->AvailableLeaveTypes.'</td><td>'.$LeaveTypes->TotalAllocatedLeaves.'</td><td>'.$LeaveTypes->YearTotal.'</td><td>'.$LeaveTypes->MonthTotal.'</td><td>'.$LeaveTypes->Balance.'</td>';
            //<td><b>88:00</b></td>
            echo '</tr>';
        }
        ?>
    </table>
</div>


<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#approveBtn').on('click',function(e){
            var employeeID = <?php echo isset($employeeData->EmployeeID)? $employeeData->EmployeeID : 0; ?>;
            if(employeeID == 0){
                Parexons.notification('Employee Details Are Not Set, Please Contact the IT Administrator For Current Problem','error');
                return;
            }
            var timesheet = <?php echo isset($TimeSheetViewData[0]->TimeSheetID)? $TimeSheetViewData[0]->TimeSheetID : 0; ?>;
            if(timesheet == 0){
                Parexons.notification('Time Sheet Details Are Not Set, Please Contact the IT Administrator For Current Problem','error');
                return;
            }
            var approvalRemarks = $('#approveDeclineComments').val();
            if(approvalRemarks.length < 10){
                Parexons.notification('Your Remarks Must Be At Least 10 Characters Long.','error');
                return;
            }
            var postData = {
                empID: employeeID,
                timeSheet: timesheet,
                approvalRemarks:approvalRemarks
            };
            $.ajax({
                url: "<?php  echo base_url(); ?>dashboard_site/approve_decline_manualTimeSheet",
                data: postData,
                type: "POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        Parexons.notification(data[1],data[2]);
                        window.location.href = '<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval';
                    } else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
$('tbody').addClass('table-row');
        });
        //Now the Function to Decline the Approval..
        $('#declineBtn').on('click', function () {
            var employeeID = <?php echo isset($employeeData->EmployeeID)? $employeeData->EmployeeID : 0; ?>;
            if(employeeID == 0){
                Parexons.notification('Employee Details Are Not Set, Please Contact the IT Administrator For Current Problem','error');
                return;
            }
            var timesheet = <?php echo isset($TimeSheetViewData[0]->TimeSheetID)? $TimeSheetViewData[0]->TimeSheetID : 0; ?>;
            if(timesheet == 0){
                Parexons.notification('Time Sheet Details Are Not Set, Please Contact the IT Administrator For Current Problem','error');
                return;
            }
            var declinedRemarks = $('#approveDeclineComments').val();
            if(declinedRemarks.length < 10){
                Parexons.notification('Your Remarks Must Be At Least 10 Characters Long.','error');
                return;
            }
            var postData = {
                empID: employeeID,
                timeSheet: timesheet,
                declinedRemarks:declinedRemarks
            };
            $.ajax({
                url: "<?php  echo base_url(); ?>dashboard_site/approve_decline_manualTimeSheet",
                data: postData,
                type: "POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        Parexons.notification(data[1],data[2]);
                        window.location.href = '<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval';
                    } else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>
