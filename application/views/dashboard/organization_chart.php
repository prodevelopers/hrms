<?php

function buildTree(array $elements, $parentId = 0) {
    $branch = array();
    foreach ($elements as $element) {
        if ($element['parent_organization_id'] == $parentId) {
            $children = buildTree($elements, $element['organization_id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}
if(isset($data) && !empty($data)){
    $tree = buildTree(json_decode(json_encode($data),true));
}

/*echo "<pre>";
print_r($tree);
echo "</pre>";*/
?>
<body>
<style type="text/css">
    .dropdown-menu{
    position: relative;
    left: 70%;
}
.margino{
        padding-left: 2em;
        font-family: "Myriad Pro";
    }
    .margino .header {
        width: 200px;
        background: #4D6684;
        color: #fff;
        border:none;
    }
    .marginol{
        position: relative;
        left: 600px;
    }
    .margino ul{
        float: left;
    }
    .margino .menu li{
        float: left;
        width: 200px;
        background: #fff;
        border:none;
    }
    .margino .menu h4{
        font-size: 14px;
        float: left;
    }
    .margino .menu small{
        float: right;
        padding: 0.2em 0;
    }
    .margino .menu a{
        padding: 0.2em 0.5em;
        background: green;
        color: #fff;
    }
    .margino .menu a:hover{
        color: #f8f8f8;
    }
    .margino .menu p{
        float: left;
        width: 100%;
        padding: 0.2em;
        font-size: 13px;
        font-weight: bold;;
    }
    .margino .menu p:hover{
        background: #f8f8f8;
    }
    .margino ul ul{
        position: relative;
        left: -2px;
        top: 27px;
    }
    .margino ul ul li {
        float: left;
    }
    .margino h3 {
        font-size: 12px;
        padding: 0.5em;
    }
</style>

<!-- header ends -->

<br style="clear:both;">
<!-- contents -->
<link rel="stylesheet" href="demo.css"/>
        <link rel="stylesheet" href="assets/js/chart/jquery.orgchart.css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="assets/js/chart/jquery.orgchart.js"></script>
        <script>
        $(function() {
            $("#organisation").orgChart({container: $("#main"), interactive: true, fade: true, speed: 'slow'});
        });
        </script>
        <style type="text/css">
            .btn{
                float: left;
                padding: 5px 8px 5px 8px;
                border: none;
                margin-right: 5px;
                cursor: pointer;
                /*width: 45%;*/
            }
            .green{
                background: #6AAD6A;
                color: #fff;
            }
        </style>

<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Organizational Chart</div> <!-- bredcrumb -->
<div class="left-nav">
        <ul>
            <li><a href="dashboard_site/organization_chart">View Chart</a></li>
            <li><a href="dashboard_site/add_chart">Make Chart</a></li>
        </ul>
    </div>

	<div class="right-contents1">
    <div class="head">Organizational Chart</div>
		 <div id="left">

<!--            <ul id="organisation" style="display:none;">
                <li><em>Batman</em>
                    <ul>
                        <li>Batman Begins
                            <ul>
                                <li>Ra's Al Ghul</li>
                                <li>Carmine Falconi</li>
                            </ul>
                        </li>
                        <li>The Dark Knight
                            <ul>
                                <li>Joker</li>
                                <li>Harvey Dent
                                     <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                        <li>The Dark Knight Rises
                            <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul
                                    <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>-->


             <?php
//             programatically generated list
             function printTree(Array $tree, $markup = '')
             {
                 foreach ($tree as $elm) {
                     echo '<li data-id="' . $elm['employee_id'] . '">' . $elm['full_name'] . '<a href="#' . $elm['employee_id'] . '"><span class="addChild" title="Add a child training"></span></a><span class="editTraining" title="Edit"></span>';
                     if (isset($elm['children'])) {
                         echo '<ul>';
                         printTree($elm['children'], $markup);
                         echo '</ul>';
                     }
                     else{
                         echo "</li>";
                     }
                 }
             }
             echo "<ul id='organisation' style='display: none;'>";
             printTree($tree);
             echo "</ul>";

             ?>
        </div>

        <div id="content">
            
            <div id="main">
            </div>

        </div>
	</div>
<!-- contents -->
