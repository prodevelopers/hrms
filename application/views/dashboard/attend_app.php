<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>


<div class="contents-container">
	<div class="bredcrumb">Dashboard / Attendence Application</div>
        <?php $this->load->view('includes/dl_left'); ?>
	<div class="right-contents1">
		<div class="head">Attendence Application</div>
            <table class="table"  style="width: 25%; float: left; padding-left: 1em;">
                <thead class="table-head">
                    <td>QR Code</td>
                </thead>
                <tbody>
                <tr>
                   <td>
                       <a href="assets/Apps/Parexons.apk">
                           <img src="assets/img/Parexon_apps.png" alt="" ><br>
                           <p style="color:#00F; padding-left: 1em;">Download Attendce App</p>
                       </a>
                   </td>
                </tr>
                </tbody>
            </table>
        <table class="table"  style="width: 73%; float: left; padding-left: 1em;">
            <thead class="table-head">
            <td colspan="2">Instruction</td>
            </thead>
            <tbody>
            <tr>
                <td width="60%">
                    <img src="assets/images/attendence.png" alt="" width="100%" height="360 !important">
                </td>
                <td>
                    <ul style="list-style-type: numbers; padding-left:1em; font-size:13px; line-height:26px;">
                        <li>From Your Android Mobile Select QR CODE Reader</li>
                        <li>Then View The QR CODE Trough Reader</li>
                        <li>The QR Reader Will Download You The Link Of The Application</li>
                        <li>Copy The Link And Paste It At In Your Favourite Browser</li>
                        <li>Application Will Be Downloaded</li>
                        <li>Install It On Your Device</li>
                        <li>Thank You</li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
		</div>
	</div>
<!-- contents -->