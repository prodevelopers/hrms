<!DOCTYPE HTML>

<html>
    <head>
        <title>Org Chart</title>
        <link rel="stylesheet" href="demo.css"/>
        <link rel="stylesheet" href="jquery.orgchart.css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="jquery.orgchart.js"></script>
        <script>
        $(function() {
            $("#organisation").orgChart({container: $("#main"), interactive: true, fade: true, speed: 'slow'});
        });
        </script>
        <style type="text/css">
            .btn{
                float: left;
                padding: 5px 8px 5px 8px;
                border: none;
                margin-right: 5px;
                cursor: pointer;
                /*width: 45%;*/
            }
            .green{
                background: #6AAD6A;
                color: #fff;
            }
        </style>
    </head>

    <body>

        <div id="left">

            <ul id="organisation" style="display:none;">
                <li><em>Batman</em>
                    <ul>
                        <li>Batman Begins
                            <ul>
                                <li>Ra's Al Ghul</li>
                                <li>Carmine Falconi</li>
                            </ul>
                        </li>
                        <li>The Dark Knight
                            <ul>
                                <li>Joker</li>
                                <li>Harvey Dent
                                     <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                        <li>The Dark Knight Rises
                            <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul
                                    <ul>
                                <li>Bane</li>
                                <li>Talia Al Ghul</li>
                            </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        
        </div>

        <div id="content">
            
            <a href="../index.php"><button class="btn green">Back Home</button></a>
            <br>
            <h1>Organiational Chart</h1>
            <div id="main">
            </div>

        </div>
            
  </body>

</html>
