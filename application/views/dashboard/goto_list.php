<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
	.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -0.2%;
	top: -20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
    float:right;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
    margin-top: -10px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"full_name","value":$('#full_name').val()});
			aoData.push({"name":"department_name","value":$('#department_name').val()});
					        
                
		}
                
	});
    $('#searchby').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>
<!-- contents -->



<div class="contents-container">

	<div class="bredcrumb">Dashboard / Go To List</div> <!-- bredcrumb -->


	<div class="right-contents">

		<div class="head">Go To List</div>

			<!-- filter -->
			<div class="filter">
				<?php echo form_open('');?>
                <input type="text" id="searchby" placeholder="Search Here" style="width: 30%;">
        <?php echo form_dropdown('full_name',$exp,$full_name,"class='resize' id='full_name' onchange='this.form.submit()'");?>
        
        <?php echo form_dropdown('department_name',$status,$department_name,"class='resize' id='department_name' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>
       
				</div>
			
 <a href="dashboard_site/add_goto_list" class=""><button class="btn green down ">Add</button></a>
			
			<!-- table -->
			 <table cellspacing="0" id="table_list" style="margin-top:45px;" class="marge">
				<thead class="table-head">
				<!-- 	<td><input type="checkbox"></td> -->
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Department</td>
					<td>Responsible For</td>
					<td>Email</td>
					<td>Phone</td>
					<td><span class="fa fa-pencil"></span>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="fa fa-trash-o"></span></td>
					
				</thead>
                <tbody class="table-row">
                </tbody>
				<!--<tr class="table-row">
				 	<td><input type="checkbox"></td> 
					<td>Ahmad</td>
					<td>Transportation</td>
					
				</tr>-->
				
			</table>
<!-- button group 
			<div class="row">
				<div class="button-group">
					<a href="add_employees.php"><button class="btn green">Add</button></a>
				</div>
			</div>-->
</div>
		</div>

	</div>
<!-- contents -->
<script>
$(document).ready(function(e){
$('#full_name,#department_name').select2();
});
</script>