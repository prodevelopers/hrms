<script type="application/javascript" src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css">
<script type="application/javascript" src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css">

<div class="contents-container">
    <div class="bredcrumb">Dashboard /Leave Apprvals /Leave Approval Action</div>
    <!-- bredcrumb -->
    <?php $this->load->view('includes/aproval_menu'); ?>
    <div class="right-contents1">
        <div class="head">Leave Approval Action</div>
        <div class="form-left">
            <?php echo form_open();?>
            <table class="table table-approved">
                <thead class="table-head">
                    <td colspan="2">Employee Information</td>
                </thead>
                <tbody>
                    <tr class="table-row">
                        <td> <h4>Employee Code</h4></td>
                        <td><?php echo isset($emp->employee_code) ? $emp->employee_code : ''; ?></td>
                    </tr>
                    <tr class="table-row">
                        <td><h4>Employee Name</h4></td>
                        <td><?php echo isset($emp->full_name) ? $emp->full_name : ''; ?></td>
                    </tr>
                    <tr class="table-row">
                        <td><h4>New Leave Type</h4></td>
                        <td><?php echo isset($emp->leave_type) ? $emp->leave_type : ''; ?></td>
                    </tr>
                    <tr class="table-row">
                        <td><h4>Requested From Date</h4></td>
                        <td id="requestFromDate"><?php echo date_format_helper(isset($emp->ApplicationFromDate) ? $emp->ApplicationFromDate : ''); ?>
                        <input type="hidden" name="fromDate" id="fromDate" value="<?php echo (isset($emp->ApplicationFromDate) ? $emp->ApplicationFromDate : '');?>">
                        </td>
                    </tr>
                    <tr class="table-row">
                        <td> <h4>Requested To Date</h4></td>
                        <td id="requestToDate"><?php echo date_format_helper(isset($emp->ApplicationToDate) ? $emp->ApplicationToDate : ''); ?>
                            <input type="hidden" name="toDate" id="toDate" value="<?php echo (isset($emp->ApplicationToDate) ? $emp->ApplicationToDate : '');?>"></td>
                    </tr>
                    <tr class="table-row">
                        <td> <h4>Total Leaves Requested</h4></td>
                        <td><?php echo isset($emp->TotalDays) ? $emp->TotalDays : '' ?>
                            <input type="hidden" name="totalDay" id="totalDay" value="<?php echo isset($emp->TotalDays) ? $emp->TotalDays : '' ?>"></td>
                    </tr>

                </tbody>
            </table>

            <!--Now the Work Needs To Be Done By the Approval Supervisor-->
            <br class="clear">
            <div class="row2">
                <h4>Grant Leave From</h4>
                <input type="text" name="approved_from" id="allocateFromDate" placeholder="From Date" required="required">
            </div>
            <br class="clear">
            <div class="row2">
                <h4>Grant Leave To</h4>
                <input type="text" name="approved_from" id="allocateToDate" placeholder="To Date" required="required">
            </div>
            <br class="clear">
            <div class="row2">
                <h4>Total Leave(s) Granted</h4>
                <input type="text" id="totalLeavesAllocated" readonly>
            </div>
            <br class="clear">
            <div class="row2">
                <h4>Total Unpaid Leaves</h4>
                <input type="text" id="totalUnPaidLeaves" onchange="handleChange(this);">
            </div>

            <br class="clear">

            <div class="row2">
                <h4>Total paid Leaves</h4>
                <input type="text" id="totalPaidLeaves" onchange="handleChange(this);">

            </div>

            <br class="clear">

            <div class="row2">
                <h4>Remarks</h4>
                <textarea rows="7" cols="30" name="remarks" id="approvedDeclinedRemarks"></textarea>
            </div>
            <br class="clear">
            <!-- button group -->
            <div class="row2">
                <div class="button-group">
                    <input type="button" id="approveBtn" value="Approve" class="green btn">
                    <input type="button" id="declinedBtn" value="Declined" class="red btn">
                </div>
            </div>
            </form>
        </div>
        <div class="form-right">
            <table cellspacing="0" id="table_list" style="width:50%; float:left; margin-left:0.7em;">
                <thead class="table-head">
                <td colspan="5">Leave Details</td>
                </thead>
                <tr class="table-row">
                    <th>Category</th>
                    <th>Allocated</th>
                    <th>Leaves Taken Yearly</th>
                    <th>Balance</th>
                    <!-- <th><b>Total Hours</b></th> -->
                </tr>
                <?php if(!empty($complete_profile)){
                //     Now Need to Populate Table of EmployeeLeaves
                foreach ($complete_profile as $LeaveTypes) {
                    echo '<tr class="table-row" style="text-align:center;">';
                    echo '<td>' . $LeaveTypes->AvailableLeaveTypes . '</td><td>' . $LeaveTypes->TotalAllocatedLeaves . '</td><td>' . $LeaveTypes->YearTotal . '</td><td>' . $LeaveTypes->Balance . '</td>';
                    //<td><b>88:00</b></td>
                    echo '</tr>';
                }}
                ?>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
    $("#accordion").accordion();
    $("#accordion1").accordion();
    $(document).ready(function () {
        $('#allocateFromTime').timepicker();
        $('#allocateToTime').timepicker();
        $("#allocateFromDate, #allocateToDate").datepicker({
            beforeShow: customRange,
            dateFormat: "dd-mm-yy",
            firstDay: 1
        });
        //Approve the Leaves
        $('#approveBtn').on('click', function (e) {
            var approvedRemarks = $('#approvedDeclinedRemarks').val();
            var approved = 1;
          /*  if(approvedRemarks.length < 10){
                Parexons.notification('Remarks Must Be 10 Characters Long','error');
                return;
            }*/
            <?php /*if($emp->ApplicationShortLeave == 2){*/?>
            var postData = {
                allowFromDate: $('#allocateFromDate').val(),
                allowToDate: $('#allocateToDate').val(),
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                totalDay: $('#totalDay').val(),
                paidDays: $('#totalPaidLeaves').val(),
                unPaidDays: $('#totalUnPaidLeaves').val(),
                approvedRemarks: approvedRemarks,
                approved:approved
            };
            <?php /*}*/?>

           /* <?php //if($emp->ApplicationShortLeave == 1){?>
            var postData = {
                allowFromTime: $('#allocateFromTime').val(),
                allowToTime: $('#allocateToTime').val(),
                approvedRemarks: approvedRemarks,
                short: $("#short_leave").val()
            };
            <?php //}?>*/
            var redirectURL = '<?php echo base_url(); ?>dashboard_site/action_leave_approval/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>';
            $.redirect(redirectURL,postData,'POST');
        });
        //Decline the Leaves
        $('#declinedBtn').on('click', function (e) {
            var declinedRemarks = $('#approvedDeclinedRemarks').val();
            if(declinedRemarks.length < 10){
                Parexons.notification('Remarks Must Be 10 Characters Long','error');
                return;
            }
            var postData = {
                declinedRemarks: declinedRemarks
            };
            var redirectURL = '<?php echo base_url(); ?>dashboard_site/action_leave_approval/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>';
            $.redirect(redirectURL,postData,'POST');
        });

        //Update Date Allocated On FromDate Change
        $('#allocateFromDate').on('change', function (e) {
            if ($('#allocateToDate').datepicker("getDate") != null && $(this).datepicker('getDate') != null) {
                var startDate = $(this).val();
                var endDate = $('#allocateToDate').val();
                var fromDateArray = startDate.split('-');
                var toDateArray = endDate.split('-');
                var diff = new Date(Date.parse(toDateArray[2]+'-'+toDateArray[1]+'-'+toDateArray[0]) - Date.parse(fromDateArray[2]+'-'+fromDateArray[1]+'-'+fromDateArray[0]));
               // var diff = new Date(Date.parse(endDate) - Date.parse(startDate));
                var totalDays = diff / 1000 / 60 / 60 / 24;
                $('#totalLeavesAllocated').val(totalDays + 1);
                $('#totalUnPaidLeaves').val(totalDays + 1);
                $('#totalPaidLeaves').val(0);
            }
        });
        //Update Date Allocated On ToDate Change
        $('#allocateToDate').on('change', function (e) {
            if ($('#allocateFromDate').datepicker("getDate") != null && $(this).datepicker('getDate') != null) {
                var startDate = $('#allocateFromDate').val();
                var endDate = $(this).val();

                var fromDateArray = startDate.split('-');
                var toDateArray = endDate.split('-');
                var diff = new Date(Date.parse(toDateArray[2]+'-'+toDateArray[1]+'-'+toDateArray[0]) - Date.parse(fromDateArray[2]+'-'+fromDateArray[1]+'-'+fromDateArray[0]));
                //var diff = new Date(Date.parse(endDate) - Date.parse(startDate));
                var totalDays = diff / 1000 / 60 / 60 / 24;
                $('#totalLeavesAllocated').val(totalDays + 1);
                $('#totalUnPaidLeaves').val(totalDays + 1);
                $('#totalPaidLeaves').val(0);
            }
        });

    });


    //Function for Restricting Date Range For Date Pickers..
    function customRange(input) {
        var min = new Date(2008, 11 - 1, 1), //Set this to your absolute minimum date
            dateMin = min,
            dateMax = null,
            dayRange = 6; // Set this to the range of days you want to restrict to

        if (input.id === "allocateFromDate") {
            var requestFromDate = $('#requestFromDate').text();
            requestFromDate = requestFromDate.split("-");
            requestFromDateObj = new Date(requestFromDate[2], requestFromDate[1] - 1, requestFromDate[0]);
            dateMin = requestFromDateObj;
            //Now We Should Go For The Max Date
            if ($('#allocateToDate').datepicker("getDate") != null) {
                dateMax = $('#allocateToDate').datepicker("getDate");
            } else {
                var requestToDate = $('#requestToDate').text();
                requestToDate = requestToDate.split("-");
                requestToDateObj = new Date(requestToDate[2], requestToDate[1] - 1, requestToDate[0]);
                dateMax = requestToDateObj;
            }
        }
        else if (input.id === "allocateToDate") {
            var requestFromDate = $('#requestFromDate').text();
            requestFromDate = requestFromDate.split("-");
            dateMin = new Date(requestFromDate[2], requestFromDate[1] - 1, requestFromDate[0]);

            var requestToDate = $('#requestToDate').text();
            requestToDate = requestToDate.split("-");
            dateMax = new Date(requestToDate[2], requestToDate[1] - 1, requestToDate[0]);

            if ($("#allocateFromDate").datepicker("getDate") != null) {
                var allocateFromDate = $('#allocateFromDate').val();
                allocateFromDate = allocateFromDate.split("-");
                dateMin = new Date(allocateFromDate[0], allocateFromDate[1] - 1, allocateFromDate[2]);
            }
        }
        return {
            minDate: dateMin,
            maxDate: dateMax
        };
    }
    //Restrict Inputs To There Respective Ranges...
    function handleChange(input) {
        var availableLeaves = $('#totalLeavesAllocated').val();
        if(availableLeaves.length === 0){
            input.value = 0;
            return;
        }

        //Restrict Inputs To only Numerics..
        if(isNaN(parseInt(input.value)) || !$.isNumeric(input.value)){
            Parexons.notification('Only Numeric Values Are Allowed In Paid And Unpaid Fields.','error');
            $('#totalUnPaidLeaves').val(availableLeaves);
            $('#totalPaidLeaves').val(0);
            return;
        }

        if(input.id === "totalUnPaidLeaves"){
            if(parseInt(input.value) > availableLeaves){
                input.value = availableLeaves;
                $('#totalPaidLeaves').val(0);
            }
            else if(parseInt(input.value) <= availableLeaves){
                $('#totalPaidLeaves').val(availableLeaves-input.value);
            }
        }else if(input.id === "totalPaidLeaves"){
            if(parseInt(input.value) > availableLeaves){
                console.log('itsGreater');
                input.value = availableLeaves;
                $('#totalUnPaidLeaves').val(0);
            }else if(parseInt(input.value) <= availableLeaves){
                $('#totalUnPaidLeaves').val(availableLeaves-input.value);
            }
        }
        return;
    }
</script>

