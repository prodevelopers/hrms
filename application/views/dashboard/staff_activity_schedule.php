<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
	.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -0.2%;
	top: -20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"project_title","value":$('#project_title').val()});
			aoData.push({"name":"department_name","value":$('#department_name').val()});
					        
                
		}
                
	});
    $('#searchby').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });

	$('tbody').addClass('table-row');
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Staff Activity Scheduler</div> <!-- bredcrumb -->


	<div class="right-contents">

		<div class="head">Staff Activity Scheduler</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
                <input type="text" id="searchby" placeholder="Search Here" style="width: 30%;">
        <?php echo form_dropdown('project_title',$exp,$project_title,"class='resize' id='project_title' onchange='this.form.submit()'");?>
        
        <?php echo form_dropdown('department_name',$status,$department_name,"class='resize' id='department_name' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>
				</div>
			

			
			<!-- table -->
			 <table cellspacing="0" id="table_list" class="marge">
				<thead class="table-head">
				<!-- 	<td><input type="checkbox"></td> -->
					<td>Employee Name</td>
					<td>Project</td>
					<td>Department </td>
					<td>Activity/Task </td>
					<td>Start Date</td>
					<td>Est. Completion Date</td>
					<td>Status</td>
					<!--<td>Status</td>-->
				</thead>
			
			</table>

</div>
		</div>

	</div>
<!-- contents -->
<script>
    $(document).ready(function(e){
        $('#project_title,#department_name').select2();
    });
</script>