<style type="text/css">
    .employee_info {
        width: 30%;
        float: left;
        margin-left: 1em;
    }
    .hide{
        visibility: hidden;
    }
</style>
<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>

<div class="contents-container">
    <div class="bredcrumb">Dashboard /Skills Inventory/ Assign Tasks</div>
    <!-- bredcrumb -->

    <div class="right-contents">
        <div class="head">Assign Task</div>
        <table cellspacing="0" id="table_employeeList" class="employee_info">
            <thead class="table-head">
            <td colspan="4">Employee's Info</td>
            </thead>
            <tbody>
            <?php if (isset($employeesData)) {
                foreach ($employeesData as $employeeData) {
                    echo '<tr data-id="'.$employeeData->employment_id.'"><td class="table-row">' . $employeeData->full_name . '</td><td class="table-row"><button class="remove">Remove</button></td></tr>';
                }
            } else {
                echo "<tr><td colspan='2'>No Employees Selected</td></tr>";
            } ?>
            </tbody>
        </table>
        <form action="" id="form1">
            <br class="clear">
            <hr>
            <br class="clear">
            <div class="row">
                <h4>Tasks</h4>
                <input type="hidden" name="selectCurrentTask" id="selectCurrentTask">
                <input type="hidden" name="project_id" id="project_id" value="<?php echo $project_id;?>">
            </div>
            <br class="clear">
            <div class="row">
                <h4>Start Date</h4>
                <input type="text" name="start_date" id="startDate" placeholder="Start Date">
            </div>
              <br class="clear">
            <div class="row">
                <h4>Completion Date</h4>
                <input type="text" name="comp_date" id="compDate" placeholder="Complete Date">
            </div>

            <br class="clear">

            <div class="row">
                <h4>Task Description</h4>
                <textarea name="taskDescription" id="taskDescription" cols="30" rows="10"></textarea>
            </div>
            <!--- Milestone -->
            <br class="clear">
            <br class="clear">
            <h2 class="head">MileStone</h2>
            <div class="row2 input_fields ">
                <div class="row2">
                    <h4 id="milestone">Mile Stone</h4>
                    <input type="text" name="milestones[]" style="width:235px">
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Estimate Completion Date</h4>
                    <input type="text" id="estimate" name="estimate_date[]" class="estimate_date" style="width:235px" placeholder="Estimate Date" >
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Actual Completion Date</h4>
                    <input type="text" id="actuals" name="actual_date[]" class="actual_date" style="width:235px" placeholder="Completion Date">
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Remarks</h4>
                    <textarea name="remarks[]" id="" cols="33" rows="7"></textarea>
                    <input type="hidden" name="employment" id="employment" />
                </div>
            </div>
            <br class="clear">
            <div class="row2">
                <h4>&nbsp;</h4>
                <button type="button" class="btn green add_field"><span class="fa fa-plus"></span></button>
                <button type="button" class="btn green rem_field"><span class="fa fa-minus"></span></button>
            </div>
            <!---- End----->
        </form>
        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <!--<a href="add_employee_personal_info.php"><button class="btn green">Add</button></a>-->
                <input type="button" name="add" id="saveButton" value="Save" class="btn green">
                <!-- <button class="btn red">Delete</button> -->
            </div>
        </div>

        </form>
    </div>

</div>

<!--menu approvals-------------------------->



<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function () {
        var EmployeeIDs = new Array();
        var array = $('#table_employeeList tbody tr');
        $.each(array, function (i, e) {
            EmployeeIDs.push($(e).attr('data-id'));
        });
        $("#employment").val(EmployeeIDs);


        /*The Selector for Selecting the Employee Skills*/
        var select2Selector = $('#selectCurrentTask');
        var url = "<?php echo base_url(); ?>dashboard_site/load_all_available_project_tasks/<?php echo $project_id;?>";
        var id = "TaskID";
        var text = "TaskName";
        var minInputLength = 0;
        var placeholder = "Select Tasks";
        var multiple = false;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css("width","223px");
        //End of the CommonSelect2 function

        //Function for Save Button
        $('#saveButton').on('click', function (e) {
            var formData=$("#form1").serialize();

            if(EmployeeIDs.length !== 0){
                var redirectURL = "<?php echo base_url(); ?>dashboard_site/assign_task_to_employees_action";

                $.ajax({
                    url: redirectURL,
                    data:formData,
                    type:"POST",
                    success: function (output) {
                        var data = output.split('::');
                        if(data[0] === "OK"){
                            Parexons.notification(data[1],data[2]);
                        }else{
                            Parexons.notification(data[1],data[2]);
                        }

                    }

                });
                return;
                if($('#selectCurrentTask').val() > 0){
                    if($('#selectCurrentTask').val() > 0){
                        var postData=formData+'&Employment_id='+EmployeeIDs;
                      /*  var postData = {
                            'employeeIDs':EmployeeIDs,
                            'projectID' : $('#selectCurrentTask').val(),
                            'taskName' : $('#taskName').val(),
                            'taskDesc' : $('#taskDescription').val()
                        }*/
                        //console.log(postData); return;
                        $.redirect(redirectURL,postData,'POST');
                    }else{
                        Parexons.notification('You Must Fill All the Forms','error');
                    }
                }else{
                    Parexons.notification('You Must Select The Project to Assign Task','error');
                }
            }
            else{
                Parexons.notification('No Employees Are Selected','error');
            }
        });

        //Function for Remove Button inside Table.
     $('#table_employeeList').on('click','.remove', function (e) {
            $(this).closest('tr').remove();
         Parexons.notification('Employee Removed From List','success');
         $("#employment").text('');
         var EmployeeIDs = new Array();
         var array = $('#table_employeeList tbody tr');
         $.each(array, function (i, e) {
             EmployeeIDs.push($(e).attr('data-id'));
         });
         $("#employment").val(EmployeeIDs);
     });

        $("#startDate" ).datepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:MM",
            changeMonth: true,
            changeYear: true});

           $("#compDate" ).datepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:MM",
            changeMonth: true,
            changeYear: true});

        /*$(function(){$("#startDate").datepicker({dateFormat:'yy-mm-dd'})});
*/
    });
</script>
<script>
    $(document).ready(function(){
        var max_fields = 20; //maximum input boxes allowed
        var wrapper    = $(".input_fields"); //Fields wrapper
        var add_button = $(".add_field"); //Add button ID
        var rem_button = $(".rem_field"); //Add button ID
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="clear"></div><div class="ciruler" id="'+(x-1)+'"><div class="row2">'
                +'<h4 id="milestone">Mile Stone</h4>'
                +'<input type="text" name="milestones[]" style="width:235px">'
                +'</div>'
                +'<br class="clear">'
                +'<div class="row2">'
                +'<h4>Estimate Completion Date</h4>'
                +'<input type="text" name="estimate_date[]" class="estimate_date" style="width:235px" placeholder="Estimate Date">'
                +'</div>'
                +'<br class="clear">'
                +'<div class="row2">'
                +'<h4>Actual Completion Date</h4>'
                +'<input type="text" name="actual_date[]" class="actual_date" style="width:235px" placeholder="Completion Date">'
                +'</div>'
                +'<br class="clear">'
                +'<div class="row2">'
                +'<h4>Remarks</h4>'
                +'<textarea name="remarks[]" id="" cols="33" rows="7"></textarea>'
                +'</div>');}

        });

        $(rem_button).click(function(e){ // on add input button click
            e.preventDefault();
            if(x < max_fields){ // max input box allowed
                // text box increment
                $(wrapper).find('div[id="'+(x-1)+'"]').remove(); //add input box
                x--;
            }
        });

    });
</script>
<script>
    //Little Fix for Dynamically Created Input Boxes..
    $('body').on('focus','.actual_date, .estimate_date',function(e){
        //console.log('Hello World');
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            timeFormat: "HH:MM",
            changeMonth: true,
            changeYear: true
        });
    });
</script>