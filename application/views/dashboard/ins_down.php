<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Add File To Downloads</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/dl_left'); ?>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents1">

		<div class="head">Add Download Files</div>

			<?php echo form_open_multipart();?>
				<!--<div class="row">
					<h4>File Name Tag</h4>
					<input type="text" placeholder="File Tag Name" name="file_tag_name">
				</div>-->
				<br class="clear">
				<div class="row">
					<h4>Select File</h4>
					<input type="file" name="attached_file" style="border:1px solid grey" required>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Remarks</h4>
					<input type="text" placeholder="Remarks" name="remarks" required>
				</div>

				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add File" class="btn green"  />
					<!--<a href="#"><button class="btn green">Add File</button></a>
					 <button class="btn red">Delete</button> -->
				</div>
			</div>
            <?php echo form_close();?>

		</div>

	</div>
<!-- contents -->