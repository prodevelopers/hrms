<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

</style>
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"year","value":$('#year').val()});
            aoData.push({"name":"month","value":$('#month').val()});    
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}


function lookup(inputString) {
	
    if(inputString.length == 0) {
        $('#suggestions').hide();
		 $('#employee_inf0').hide();
	       location.reload();
    } else {
        $.post("dashboard_site/autocomplete_goto_list/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,ml_designation_id,designation_name,department_id,department_name,employee_id) {
    $('#id_input').val(thisValue);
	//$('#name').val(department_name);
	$('#desg_id').val(ml_designation_id);
	$('#desg').val(designation_name);
	$('#emp_id').val(employee_id);
	$('#dept_id').val(department_id);
	$('#dept').val(department_name);
  /* $('#approval_types').append(approval_types);
	
	 $('#todate').append(from_date)
	$('#fromdate').append(to_date);*/
	setTimeout("$('#suggestions').hide();", 200);
      //$('#suggestions').hide();
   <!--$('#employee_inf0').show();-->
 
	//setTimeout("$('#autoSuggestionsList').hide()", 200);
}   



</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Go To List</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Go To List</div>
			
			<form method="post" action="dashboard_site/add_goto_list">
				<div class="row">
					<h4>Employee Name</h4>
                    <!--<select name="full_name" id="id_input" onchange="lookup(this.value)">
                    <option>-- Name--</option>
                    <?php //foreach($data as $data){?><option><?php //echo $data->full_name;?></option><?php //}?>
                    </select>-->
					<input type="text" name="name" id="id_input" class="serch" autocomplete="off" onkeyup="lookup(this.value)">
                    <input type="hidden" name="employee_id"  id="emp_id" />
				</div>
				<br class="clear">
				<div id="suggestions">
                        <div class="autoSuggestionsList_l" id="autoSuggestionsList">
                        </div>
                    </div>
				<br class="clear">
				<div class="row">
					<h4>Designation</h4>
                    <input type="text" name="designation_name" id="desg" readonly="readonly" />
                    <input type="hidden" name="desg_id"  id="desg_id" />
					<?php //echo @form_dropdown('designation_name',$Designations,$designation_name);?>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Department</h4>
					<input type="text" name="department_name" id="dept" readonly="readonly" />
                    <input type="hidden" name="dept_id"  id="dept_id" />
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Responsible For</h4>
					<input type="text" name="resposible">
				</div>
				<br class="clear">
                 <input type="submit" name="add" class="btn green" value="Add" />
				
			</form>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

			

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>
<!-- leftside menu end -->
