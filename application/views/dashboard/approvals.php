
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard /Approvals /Transactions Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>
	<div class="right-contents1">

		<div class="head">Transactions Approvals</div>

			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
        <?php echo form_dropdown('transaction_type',$transaction_type,$type,"class='select_47 filter1' id='transaction_type' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>	
			</div>

			  <table cellspacing="0" id="table_list">
				<thead class="table-head">
                	<td>Transaction ID</td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<!--<td>Payment Mode</td>-->
					<td>Transaction Type</td>
					<td>Transaction Amount</td>
					<td>Status</td>
					<td>Action</td>
				</thead>
				  <?php if(!empty($info)){ foreach($info as $rec){?>
				<tr>
					<td><?php echo $rec->trans_id;?></td>
					<td><?php echo $rec->employee_code;?></td>
					<td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<td><?php echo $rec->transaction_type;?></td>
					<td><?php echo $rec->transaction_amount;?></td>
					<td><?php echo $rec->status_title;?></td>
					<?php if($rec->id==5){?>
					<td><a href="dashboard_site/action_transaction_review/<?php echo $rec->employment_id;?>/<?php echo $rec->salary_month;?>/<?php echo $rec->year;?>"><button class= "btn green">Action</button></a></td>
					<?php }else{?>
					<td><a href="dashboard_site/action_transcation_approval/<?php echo $rec->transaction_id;?>/<?php echo $rec->employee_id;?>/<?php echo $rec->id;?>"><button class= "btn green">Action</button></a></td>
					<?php }?>
				  </tr>
				  <?php }}else {echo "No Records Founded";}?>
			</table>  	
	   </div>


	</div>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
    $(document).ready(function(e){
        $('#transaction_type').select2();
    });


</script>
