
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard /Approvals /Transactions Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>
	<div class="right-contents1">

		<div class="head">End of Service Approvals</div>

			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
        <?php echo form_dropdown('status_title',$status,$status_title,"class='select_47 filter1' id='status_title' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>	
			</div>

			  <table cellspacing="0" id="table_list">
				<thead class="table-head">
                	<td>Transaction ID</td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Transaction Type</td>
					<td>Transaction Amount</td>
					<td>Status</td>
					<td>Action</td>
				</thead>
				  <?php if(!empty($info_eos)){ foreach($info_eos as $rec){
                      if($rec->id==5){?>
				<tr>
					<td><?php echo $rec->trans_id;?></td>
					<td><?php echo $rec->employee_code;?></td>
					<td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<td><?php echo $rec->transaction_type;?></td>
					<td><?php echo $rec->transaction_amount;?></td>
					<td><?php echo $rec->status_title;?></td>
					<?php if($rec->id==5){?>
					<td><a href="dashboard_site/action_eos_transaction_review/<?php echo $rec->employment_id;?>/<?php echo $rec->salary_month.'/'.$rec->salary_year;?>"><button class= "btn green">Action</button></a></td>
					<?php }else{?>
					<td><a href="dashboard_site/action_transcation_approval/<?php echo $rec->employment_id;?>/<?php echo $rec->trans_id;?>"><button class= "btn green">Action</button></a></td>
					<?php }?>
				  </tr>
				  <?php }}}else {echo "No Records Founded";}?>
			</table>  	
	   </div>


	</div>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#status_title').select2();
    });
</script>
