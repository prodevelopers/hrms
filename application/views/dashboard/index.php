<style>
    .f td{
        border-left: thin solid #ccc;
        border-bottom: thin solid #ccc;
    }
    .f-head td {
        font-weight: bold;
        background: #fff;
        color: #000;
        padding-left: 1.2em;
    }
    .table-row td{
        text-align: center;
    }
</style>
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css"> -->

<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
function pieValueToArrayConversion($phpArray)
{
    $phpArray = json_decode(json_encode($phpArray), true);
    if (isset($phpArray) && is_array($phpArray)) {
        foreach ($phpArray as $key => $value) {
            $integerValue = $value['value'];
            $array = array(
                'value' => intval($integerValue)
            );
            $phpArray[$key] = array_replace($phpArray[$key], $array);
            //If Color is Empty Get a random Color
            if($value['color'] == ''|| $value['color'] == NULL){
                $arrayColor = array(
                    'color' => rand_color()
                );
                $phpArray[$key] = array_replace($phpArray[$key], $arrayColor);
            }
            //If highlight value is null get a random color value for highlight
            if($value['highlight'] == ''|| $value['highlight'] == NULL){
                $arrayColor = array(
                    'highlight' => rand_color()
                );
                $phpArray[$key] = array_replace($phpArray[$key], $arrayColor);
            }
        }
        return json_encode($phpArray);
    }
}
if(isset($EmpInDistricts)){
    $EmpInDistricts = pieValueToArrayConversion($EmpInDistricts);
    echo "<script> var districtsEmployees = ".$EmpInDistricts."; </script>";
}
if(isset($EmpInProjects)){
    $EmpInProjects = pieValueToArrayConversion($EmpInProjects);
    echo "<script> var projectEmployees = ".$EmpInProjects."; </script>";
}
if(isset($EmpGendersLast5Years)){
    echo "<script> var EmpGendersLast5Years = ".$EmpGendersLast5Years."; </script>";
}
if(isset($LeaversData)){
    echo "<script> var LeaversData = ".$LeaversData."; </script>";
}
if(isset($JoinersData)){
    echo "<script> var JoinersData = ".$JoinersData."; </script>";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />
<div class="contents-container">
	<!-- Contracts Alerts -->
	<div class="dashboard-left">
	<div class="chart1" id="target_district">
		<div class="chart-head">Employee's District Wise Distributions</div>
		 <div class="chart-nav">
			<ul>
				<li><button id="zoomin" class="btn green"><span class="fa fa-search-plus"></span></button></li>
				<li><button id="zoomout" class="btn green"><span class="fa fa-search-minus"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-print"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-save"></span></button></li>
			</ul>
		</div>
        <div  class="centerto">
            <canvas id="district" class="alignment"></canvas>
            <canvas id="district1" class="alignment" width="500" height="500" style="display:none; margin-left:4em;"/></canvas>

        </div>
        <div id="districtLegend"></div>

	</div>
	<div class="chart1" id="target_project">
		<div class="chart-head">Employee's Project Wise Distributions</div>
		 <div class="chart-nav">
			<ul>
				<li><button id="zoomind" class="btn green"><span class="fa fa-search-plus"></span></button></li>
				<li><button id="zoomoutd" class="btn green"><span class="fa fa-search-minus"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-print"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-save"></span></button></li>
			</ul>
		</div>
        <div class="centerto">
            <canvas id="projects" class="alignment"></canvas>
            <canvas id="projects1" class="alignment" width="500" height="500" style="display:none; margin-left:4em;"/></canvas>

        </div>
        <div id="projectsLegend"></div>
	</div>
	<div class="chart" id="target_gender">
		<div class="chart-head">Gender Wise Employee's Distribution</div>
		 <div class="chart-nav">
			<ul>
				<li><button id="zooming" class="btn green"><span class="fa fa-search-plus"></span></button></li>
				<li><button id="zoomoutg" class="btn green"><span class="fa fa-search-minus"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-print"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-save"></span></button></li>
			</ul>
		</div>
		<div  class="">
			<canvas id="gender" class="alignment"></canvas>
			<canvas id="gender1" class="alignment" width="500" height="500" style="display:none; margin-left:4em;"></canvas>
            <div id="employersLegend"></div>
		</div>
	</div>
	<div class="chart" id="target_leavers">
		<div class="chart-head">Leaver Statistics</div>
		 <div class="chart-nav">
			<ul>
				<li><button id="zoominl" class="btn green"><span class="fa fa-search-plus"></span></button></li>
				<li><button id="zoomoutl" class="btn green"><span class="fa fa-search-minus"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-print"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-save"></span></button></li>
			</ul>
		</div>
		<div  class="">
			<canvas id="leavers1" class="alignment" width="500" height="500" style="display:none; margin-left:4.5em;"></canvas>
			<canvas id="leavers" class="alignment"></canvas>
            <div id="leaversLegend"></div>
		</div>
	</div>
		<div class="chart" id="target_joiner">
		<div class="chart-head">Joiners Statistics</div>
		 <div class="chart-nav">
			<ul>
				<li><button id="zoominj" class="btn green"><span class="fa fa-search-plus"></span></button></li>
				<li><button id="zoomoutj" class="btn green"><span class="fa fa-search-minus"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-print"></span></button></li>
                <li><button id="zoomout" class="btn green"><span class="fa fa-save"></span></button></li>
			</ul>
		</div>
		<div  class="">
			<canvas id="joiners1" class="alignment" width="500" height="500" style="display:none; margin-left:4.5em;"></canvas>
			<canvas id="joiners" class="alignment"></canvas>
            <div id="joinersLegend"></div>
		</div>
	</div>
	</div>

	<!-- right side of dashboard -->

	<div class="dashboard-right">
    <h2 class="box-head">Contracts Expiry</h2>
	<div class="boxes" style="margin:0;">

        <table class="table f">
            <tr class="table-head f-head">
                <td>Name</td>
                <td>Date</td>
                <td><a title="Remove This"><span class="fa fa-times"></span></a></td>
            </tr>
            <?php if(isset($ContractExpiryTable) && is_array($ContractExpiryTable)){
                foreach($ContractExpiryTable as $key=>$value){
                    echo '<tr class="table-row" data-id="'.$value->ContractID.'"><td><a href="'.base_url().'human_resource/view_employee_profile/'.$value->EmployeeID.'">'.$value->EmployeeName.'</a></td><td>'?><?php echo date_format_helper($value->ContractExpiry).'</td><td><a style="cursor:pointer;" class="removeContractExpiry" title="Remove This"><span class="fa fa-times"></span></a></td></tr>';
                }
            }else{
                echo "<tr class='table-row'><td colspan='3'>No Current Projects Available In System</td></tr>";
            }
            ?>
        </table>
	 </div>
        <h2 class="box-head" >New Contracts / Extensions</h2>
        <div class="boxes"  style="margin:0;">
            <table class="table f">
                <tr class="table-head f-head">
                    <td>Name</td>
                    <td>Date</td>
                    <td><a title="Remove This"><span class="fa fa-times"></span></a></td>
                </tr>
                <?php if(isset($NewContractExtensions) && is_array($NewContractExtensions)){
                    foreach($NewContractExtensions as $key=>$value){
                        echo '<tr class="table-row" data-id="'.$value->EmploymentID.'"><td><a href="'.base_url().'human_resource/view_employee_profile/'.$value->EmployeeID.'">'.$value->EmployeeName.'</a></td><td>'?><?php echo date_format_helper($value->ContractExtensionDate).'</td><td><a style="cursor:pointer;" class="removeNewContractExtensions" title="Remove This"><span class="fa fa-times"></span></a></td></tr>';
                    }
                }else{
                    echo "<tr class='table-row'><td colspan='3'>No Current Projects Available In System</td></tr>";
                }
                ?>
            </table>
        </div>
        <h2 class="box-head">Projects</h2>
	 <div class="boxes" style="margin:0;">
         <table class="table f">
             <tr class="table-head f-head">
                 <td>Name</td>
                 <td>Status</td>
                 <td><span class="fa fa-times"></span></td>
             </tr>
             <?php if(isset($ProjectsTable) && is_array($ProjectsTable)){
                 foreach($ProjectsTable as $key=>$value){
                    echo '<tr class="table-row" data-id="'.$value->ProjectID.'"><td>'.$value->ProjectName.'</td><td>'.$value->ProjectStatus.'</td><td><a style="cursor:pointer;" class="removeProject" title="Remove This"><span class="fa fa-times"></span></a></td></tr>';
                 }
             }else{
                 echo "<tr class='table-row'><td colspan='3'>No Current Projects Available In System</td></tr>";
             }
             ?>
         </table>

	</div>
        <br class="clear">
        <h2 class="box-head">Exits</h2>
        <div class="boxes">
            <table class="table f">
                <tr class="table-head f-head">
                    <td>Name</td>
                    <td>Date</td>
                    <td><a title="Remove This"><span class="fa fa-times"></span></a></td>
                </tr>
                <?php if(isset($ExitsTable) && is_array($ExitsTable)){
                    foreach($ExitsTable as $key=>$value){
                        echo '<tr class="table-row" data-id="'.$value->SeparationID.'"><td><a href="'.base_url().'human_resource/view_employee_profile/'.$value->EmployeeID.'">'.$value->EmployeeName.'</a></td><td>'?><?php  echo date_format_helper($value->SeparationDate).'</td><td><a style="cursor:pointer;" class="removeExit" title="Remove This"><span class="fa fa-times"></span></a></td></tr>';
                    }
                }else{
                    echo "<tr class='table-row'><td colspan='3'>No Records Found For Leavers </td></tr>";
                }
                ?>
            </table>

        </div>
	<!-- Projects On Going -->
	<!-- Projects Completed -->

        <!--- for probition period alert--->
        <br class="clear">
        <h2 class="box-head">Probation</h2>
        <div class="boxes">
            <table class="table f">
                <tr class="table-head f-head">
                    <td>Name</td>
                    <td>Date</td>
                    <td><a title="Remove This"><span class="fa fa-times"></span></a></td>
                </tr>
                <?php if(isset($Probation) && is_array($Probation)){
                    foreach($Probation as $key=>$value){
                        echo '<tr class="table-row" data-id="'.$value->ContractID.'"><td><a href="'.base_url().'human_resource/view_employee_profile/'.$value->EmployeeID.'">'.$value->EmployeeName.'</a></td><td>'?><?php  echo date_format_helper($value->ProbationDate).'</td><td><a style="cursor:pointer;" class="removeProbation" title="Remove This"><span class="fa fa-times"></span></a></td></tr>';
                    }
                }else{
                    echo "<tr class='table-row'><td colspan='3'>No Records Found For Leavers </td></tr>";
                }
                ?>
            </table>

        </div>

	</div>
	<!-- Charts Ends Here -->
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger">
					<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
					<nav class="gn-menu-wrapper">
						<div class="gn-scroller">
							<ul class="gn-menu">
								<li>
									<a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
								</li>
								<li>
									<a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
								</li>
								<li>
									<a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
								</li>
								<li>
									<a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
								</li>
								<li>
								<a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
								</li>
								<li>
									<a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
								</li>
								<li>
									<a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
								</li>
								<li>
									<a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
								</li>
								<li>
									<a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
								</li>
							</ul>
						</div>
					</nav>
				</li>
			</ul>
</div>
<div id="dialog" title="Attendance" class="wel" style="display:none; width:400px;">
    <div class="data">
        <p>Today is: <b><?php echo date('D'); ?></b></p>
        <p>Current Time is: <b><?php echo date('h:i:s'); ?></b></p>
        <p>Current Date is: <b><?php echo date('d-m-Y'); ?></b></p><br>

        <?php
        if(isset($disable->in_date) && isset($disable->employee_id))
        {?>
    <?php //var_dump($disable);?>
        <p><a id="submit1"  style="color:#fff; background: grey;" class="btn green addedto disabled">DayIn Time</a>
            <?php } else {?>
                <a onclick="submit_form()" id="submit1"  style="color:#fff;" class="btn green addedto">DayIn Time</a>
            <?php }?>
            <?php if(isset($enable->out_date) && isset($enable->employee_id))
            {?>
                <a style="color:#fff; background: grey;" class="btn green addedto disabled">DayOut Time</a>
            <?php } else {?>
            <a onclick="submit_form1()" style="color:#fff;" class="btn green addedto">DayOut Time</a></p>
    <?php }?>

    </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/chart-plugin/Chart.js"></script>
<script src="<?php echo base_url();?>assets/js/dialog.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>
<script>
    function submit_form()
    {
        document.form1.submit();
    }
/*    $('#myCollapsible').collapse({
        toggle: false
    });*/

    function submit_form1()
    {
        document.form2.submit();
    }
/*    $('#myCollapsible').collapse({
        toggle: false
    });*/
</script>
<script>
	window.onload = function(){
        <?php
//If Leaves Data is Set
if(isset($LeaversData)){

 ?>
        var ctxLeaver = document.getElementById('leavers');
        var leaversBar = new Chart(ctxLeaver.getContext('2d')).Bar(LeaversData, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });
        var ctxLeaver1 = document.getElementById('leavers1');
        var leaversBar = new Chart(ctxLeaver1.getContext('2d')).Bar(LeaversData, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });

//Legend for Chart..
        var legendHolder = document.createElement('div');
        legendHolder.innerHTML = leaversBar.generateLegend();
        document.getElementById('leaversLegend').appendChild(legendHolder.firstChild);
<?php } ?>

        <?php if(isset($EmpGendersLast5Years)){ ?>


        //Genders Chart
        var ctxGender = document.getElementById('gender');
        var empBar = new Chart(ctxGender.getContext('2d')).Bar(EmpGendersLast5Years, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });
        var ctxGender1 = document.getElementById('gender1');
        var empBar = new Chart(ctxGender1.getContext('2d')).Bar(EmpGendersLast5Years, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });
        var legendHolder = document.createElement('div');
        legendHolder.innerHTML = empBar.generateLegend();
        document.getElementById('employersLegend').appendChild(legendHolder.firstChild);
        <?php  } ?>
        //Joiners Chart
        var canvasJoiners = document.getElementById('joiners');
        var joinerBar = new Chart(canvasJoiners.getContext('2d')).Bar(JoinersData, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });

        var canvasJoiners1 = document.getElementById('joiners1');
        var joinerBar = new Chart(canvasJoiners1.getContext('2d')).Bar(JoinersData, {
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employee's",
            animation: true,
            responsive :true
        });
//
        var joinersLegendHolder = document.createElement('div');
        joinersLegendHolder.innerHTML = joinerBar.generateLegend();
        document.getElementById('joinersLegend').appendChild(joinersLegendHolder.firstChild);

        //District Charts..
		var district = document.getElementById("district").getContext("2d");
        var empDistricts = new Chart(district).Pie(districtsEmployees,{
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employees",
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><!----><%=segments[i].label%> (<%=segments[i].value%>)<%}%></li><%}%></ul>",
            animation: true,
            responsive :true
        });
		var district = document.getElementById("district1").getContext("2d");
        empDistricts = new Chart(district).Pie(districtsEmployees,{
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employees",
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><!----><%=segments[i].label%> (<%=segments[i].value%>)<%}%></li><%}%></ul>",
            animation: true,
            responsive :true
        });

        var districtLegendHolder = document.createElement('div');
        districtLegendHolder.innerHTML = empDistricts.generateLegend();
        document.getElementById('districtLegend').appendChild(districtLegendHolder.firstChild);

        //Project Charts
		var projects = document.getElementById("projects").getContext("2d");
        var empProjects = new Chart(projects).Pie(projectEmployees,{
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employees",
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><!----><%=segments[i].label%> (<%=segments[i].value%>)<%}%></li><%}%></ul>",
            animation: true,
            responsive :true
        });
		var projects = document.getElementById("projects1").getContext("2d");
		var empProjects = new Chart(projects).Pie(projectEmployees,{
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Employees",
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><!----><%=segments[i].label%> (<%=segments[i].value%>)<%}%></li><%}%></ul>",
            animation: true,
            responsive :true
        });
        var projectsLegendHolder = document.createElement('div');
        projectsLegendHolder.innerHTML = empProjects.generateLegend();
        document.getElementById('projectsLegend').appendChild(projectsLegendHolder.firstChild);
	}

    //Need to Do some Work on Tables on Right Side of the Dashboard.
    $(document).ready(function(e){
        //Unsetting the Project Alert
        $('.removeProject').on('click', function (e) {
            var selectedRow = $(this).parents('tr');
            var ProjectID = selectedRow.attr('data-id');
            var data = {
                alertText: 'Project',
                alertID: ProjectID
            };
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard_site/suppress_alerts',
                data:data,
                type: 'POST',
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        selectedRow.remove();
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        //Unsetting the Project Alert
        $('.removeExit').on('click', function (e) {
            var selectedRow = $(this).parents('tr');
            var ExitID = selectedRow.attr('data-id');
            var data = {
                alertText: 'Exit',
                alertID: ExitID
            };
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard_site/suppress_alerts',
                data:data,
                type: 'POST',
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        selectedRow.remove();
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        //Un-Setting the Contract Expiry Alerts
        $('.removeContractExpiry').on('click', function (e) {
            var selectedRow = $(this).parents('tr');
            var ContractID = selectedRow.attr('data-id');
            var data = {
                alertText: 'ContractExpiry',
                alertID: ContractID
            };
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard_site/suppress_alerts',
                data:data,
                type: 'POST',
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        selectedRow.remove();
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        ////probation Alerts
        $('.removeProbation').on('click', function (e) {
            var selectedRow = $(this).parents('tr');
            var ContractID = selectedRow.attr('data-id');
            var data = {
                alertText: 'ProbationExpiry',
                alertID: ContractID
            };
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard_site/suppress_alerts',
                data:data,
                type: 'POST',
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        selectedRow.remove();
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        //Un-Setting the New Contract Extensions Alerts
        $('.removeNewContractExtensions').on('click', function (e) {
            var selectedRow = $(this).parents('tr');
            var ContractExtensionEmploymentID = selectedRow.attr('data-id');
            var data = {
                alertText: 'NewContractExtensions',
                alertID: ContractExtensionEmploymentID
            };
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard_site/suppress_alerts',
                data:data,
                type: 'POST',
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        selectedRow.remove();
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

<form name="form1" action="dashboard_site/Register_attendance" method="post" style="display:none">
    <input type="hidden" name="in_time" value="<?php echo date('H:i'); ?>"  />
    <input type="hidden" name="in_date" value="<?php echo date('Y-m-d'); ?>"  />
    <input type="hidden" name="year" value="<?php echo date("Y"); ?>"  />
</form>

<form name="form2" action="dashboard_site/dayout_attendance" method="post" style="display:none">
    <input type="hidden" name="out_time" value="<?php echo date('H:i'); ?>"  />
    <input type="hidden" name="out_date" value="<?php echo date('Y-m-d'); ?>"  />
</form>