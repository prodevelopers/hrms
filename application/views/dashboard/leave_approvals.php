<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<style>
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }
    h4 {
        font-size: 22px;
        font-weight: 400;
        line-height: 22px;
    }
    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        height:35px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
</style>
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}


 .dataTables_length {
        position: relative;
        left: -0.2%;
        top: -20px;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -21px;
        float:left;
    }

    .dataTables_filter input {
        width: 180px;
    }
.btn-right {
    float: right;
    height: 25px;
    margin-top: -6px;
    width: auto;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
$(document).ready(function() {


    //// Need To Work ON New Way Of DataTables..

    oTable ='';
    var leaveApplicationApprovalsTable = $('#table_list');
    var url_DT = "<?php echo base_url(); ?>dashboard_site/leave_approval/list";
    var aoColumns_DT = [
        /* ID */ {
            "mData": "ID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
        /* checkboxes */ {
            "mData": "checkboxes",
            "bVisible": true,
            "bSortable": false,
            "bSearchable": false
        },
        /* Employee Code */ {
            "mData" : "EmployeeCode"
        },
        /* Employee Full Name */ {
            "mData" : "EmployeeName"
        },
        /* Employee Designation Here */ {
            "mData" : "EmployeeDesignation"
        },
        /* Reported By (Employee Name) */ {
            "mData" : "LeaveType"
        },
        /* Leave From Date */ {
            "mData" : "LeaveFromDate"
        },
        /* Leave To Date */ {
            "mData" : "LeaveToDate"
        },
        /* Total Requested Leave Days */ {
            "mData" : "TotalDays"
        },
        /* Shows The Approval Status(Mostly It Would Be Pending.) */ {
            "mData" : "Status"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "leave_approval_id"
        }
    ];
    var HiddenColumnID_DT = 'ID';
    var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    var filters = 'aoData.push({"name":"status_title","value":$("#status_title").val()}); aoData.push({"name":"designation_name","value":$("#Designations").val()});';
    commonDataTablesFiltered(leaveApplicationApprovalsTable,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters)
	$('tbody').addClass('table-row');
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0],data[1]);
    <?php
    }
    }
}
?>

    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script><!-- contents -->

<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard / Approvals/ Leave Application Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>
	<div class="right-contents1">

		<div class="head">Leave Application Approvals
            <div class="btn-right">
                <a class="btn green" id="batchApproveBtn" style="cursor: hand;">Approve</a>
                <a class="btn red" id="batchDeclineBtn" style="cursor: hand;">Decline</a>
            </div>
        </div>

			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
                <input type="text" id="tableSearchBox" placeholder="Search Here" style="width: 30%;">
        <?php echo form_dropdown('status_title',$status,$status_title,"class='resize' id='status_title' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>	
			</div>
			  <table cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>ID</td>
					<td><input type="checkbox" id="selectAllCheckBoxes"> All</td>
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Leave Type</td>
					<td>From Date</td>
					<td>To Date</td>
					<td>Total Days</td>
					<td>Status</td>
					<td>Action</td>
				</thead>
				<tbody>
                </tbody>
			</table>  	
	   </div>
	</div>
<style>#width{}</style>
<!----------------------------Add Report Discipline Breach----------------------------->
<div id="declineReportModal" class="modal-demo" id="width"style="display: none;">
    <div class="top_header" style="width: 600px;">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Decline Report</h3>
    </div>
    <form class="form-horizontal" id="addNewBreachReport">
        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Description</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="declineRemarks" id="declineRemarks" placeholder="Reason To Decline Leave Requests"></textarea>
            </div>
        </div>

        <div class="btn-row">
            <input type="button" class="btn-pop red" id="submitRemarks" value="Decline">
        </div>
    </form>

</div>
<!-------------------------Edit Model Finish------------------------->

<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
    $(document).ready(function(e){
        $('#status_title').select2();
        $('.btn-right').hide();

        //
        $('#table_list').on('click','#selectAllCheckBoxes',function(e){
            if($(this).is(':checked')){
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',true);
                $('.btn-right').show();
            }else{
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',false);
                $('.btn-right').hide();
            }
        });

        $('#table_list').on('change', '.batchApprovalCheckBox', function (e) {
            if ($(this).is(':checked')) {
                $('.btn-right').show();
            } else {
                if ($('.batchApprovalCheckBox:checked').length > 0) {
                    $('.btn-right').show();
                } else {
                    $('.btn-right').hide();
                    $('#selectAllCheckBoxes').prop('checked',false);
                }
            }
        });

        $('#batchApproveBtn').on('click',function(e){
            //first we need to get all the checked checkboxes
            var approvalIDs = new Array();
            var array = $('#table_list .batchApprovalCheckBox:checked');
            $.each(array, function (i, e) {
                approvalIDs.push($(e).closest('tr').attr('data-id'));
            });

            var postData = {
                batchType:"approval",
                IDs:JSON.stringify(approvalIDs)
            };

            $.ajax({
                url:"<?=base_url();?>dashboard_site/leave_approval/batch",
                data:postData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        oTable.api().ajax.reload();
                        $('.btn-right').hide();
                        $('#selectAllCheckBoxes').prop('checked',false);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        $('#submitRemarks').on('click',function(e){
//         so As This Button Is Pressed We Need To Decline the Selected Application Requests
            var declineIDs = new Array();
            var array = $('#table_list .batchApprovalCheckBox:checked');
            $.each(array, function (i, e) {
                declineIDs.push($(e).closest('tr').attr('data-id'));
            });
            var declineReason = $('#declineRemarks').val();
            var postData = {
                batchType:"decline",
                IDs:JSON.stringify(declineIDs),
                remarks:declineReason
            };

            $.ajax({
                url:"<?=base_url();?>dashboard_site/leave_approval/batch",
                data:postData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        oTable.api().ajax.reload();
                        $('.btn-right').hide();
                        $('#selectAllCheckBoxes').prop('checked',false);
                        Custombox.close({
                           target: '#declineReportModal'
                        });
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });



    });

</script>
<script>
    //////////////////////////////////////////// for decline
    ///
    $('#batchDeclineBtn').on('click', function(e){
        Custombox.open({
            target: '#declineReportModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });
</script>
