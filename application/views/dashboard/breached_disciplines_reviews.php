
<!-- contents -->
<style>
    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }
    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers
    {
        margin-top: 8px;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;
        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 0px;
    }

    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }
    h4 {
        font-size: 22px;
        font-weight: 400;
        line-height: 22px;
    }
    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
        margin-right: 0px;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .right_txt_area_select{
        width:225px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        height:35px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
.drop_dwn{
    width: 240px;
    height: 35px;
    float:left;}
.approve_style{
    float:left;
    margin-left: 15px;
    margin-left: 15px;
    width:98%;
    margin-top: 20px;;
}

</style>
<style>
    <style>
    .left_block_coments{
        width:100%;
        height: auto;
        float:left;
    }
    .right_lbl_coments{
        width:470px;
        height: auto;
        float:left;
    }
    .left_lbl_coments{
        width: 24%;
        float: left;
        line-height: 35px;
        padding-left: 5px;
    }
    .coments_row{
        float: left;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
    }
</style>
<style>
    .tip {
        width: 0px;
        height: 0px;
        position: absolute;
        background: transparent;
        border: 10px solid #ccc;
    }

    .tip-up {
        top: -25px; /* Same as body margin top + bordere */
        left: 10px;
        border-right-color: transparent;
        border-left-color: transparent;
        border-top-color: transparent;
    }

    .tip-down {
        bottom: -25px;
        left: 10px;
        border-right-color: transparent;
        border-left-color: transparent;
        border-bottom-color: transparent;
    }

    .tip-left {
        top: 10px;
        left: -25px;
        border-top-color: transparent;
        border-left-color: transparent;
        border-bottom-color: transparent;
    }

    .tip-right {
        top: 10px;
        right: -25px;
        border-top-color: transparent;
        border-right-color: transparent;
        border-bottom-color: transparent;
    }

    .dialogbox .body {
        position: relative;
        max-width: 100%;
        height: auto;
        /* margin: 20px 10px;*/
        padding: 5px;
        background-color: #DADADA;
        border-radius: 3px;
        border: 5px solid #ccc;
    }

    .body .message {
        min-height: 30px;
        border-radius: 3px;
        font-family: Arial;
        font-size: 14px;
        line-height: 1.5;
        color: #797979;
    }
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }
</style>
<div class="contents-container">
    <div class="bredcrumb">Dashboard /Approvals /Breached Disciplines Reviews</div> <!-- bread crumbs -->
    <?php $this->load->view('includes/aproval_menu'); ?>
    <div class="right-contents1">

        <div class="head">Breached Discipline Reviews
            <div class="btn-right">
                <a class="btn green" id="batchApproveBtn" style="cursor: hand;">Review</a>
            </div>
        </div>

        <div class="filter">
            <h4>Filter By</h4>
            <?php echo form_open('');?>
            <input type="text" id="tableSearchBox" placeholder="Search Employees" style="width: 30%;">
            <?php echo form_close();?>
        </div>

        <table cellspacing="0" id="pendingBreachedDisciplineReviews" class="approve_style" >
            <thead class="table-head">
            <td>Report ID</td>
            <td><input type="checkbox" id="selectAllCheckBoxes"> All</td>
            <td>Accused Employee</td>
            <td>Breached Discipline</td>
            <td>Date Of Occurrence</td>
            <td>Reported By</td>
            <td>Investigated By</td>
            <td>Status</td>
            <td>Action</td>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<!---------------------------Review Modal---------------------------->


<!---Modified POP---------->
<div id="reviewBreachedDisciplineModal" class="modal-demo" style="display: none;margin-left: -190px;">
    <div class="top_header">
        <button type="button"  style="margin-right: -190px;" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Review Breached Discipline</h3>
    </div>
    <form class="form-horizontal" id="InvestigationReportForm">
        <div class="container_view" style="float:left;width:990px;height:auto;">
            <div class="left_photo">
                <img id="userAvatar" src="assets/images/user.png">
            </div>

            <div class="right_sec">
                <div class="left_block">
                    <div class="left_lbl">
                        <h5>Employee Code</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="employeeCode"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Accused Employee</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="employeeName"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Breach of discipline</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="BreachedDiscipline"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Date of occurrence</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="DateOfOccurrence"> - </h5>
                    </div>

                </div>

                <div class="left_block" >

                    <div class="left_lbl">
                        <h5>Reported By</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="ReportedBy"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Investigated By</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="InvestigatedBy"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Disciplinary Action</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="DisciplinaryAction"> - </h5>
                    </div>

                    <div class="left_lbl">
                        <h5>Status</h5>
                    </div>
                    <div class="right_lbl">
                        <h5 id="ReportStatus"> - </h5>
                    </div>
                </div>









<!----------End Popup------------------------->

                <!---for Dialogoe---->


                <div class="left_block_coments">
                    <div class="coments_row">
                        <div class="left_lbl_coments">
                            <h5>Employee Feedback</h5>
                        </div>
                        <div class="right_lbl_coments">
                            <div class="dialogbox">
                                <div class="body">
                                    <span class="tip tip-left"></span>
                                    <div class="message">
                                        <span id="employeeFeedBack"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pop_row_txtarea" style="margin-top: 5px;">
                        <div class="left_side">
                            <h5 style="padding-left: 5px;">Disciplinary Action</h5>
                        </div>
                        <div class="right_side">
                            <input type="hidden" id="selectDisciplinaryAction" style="margin-left: -125px;">
                        </div>
                    </div>
                    <div class="coments_row">

                        <input type="hidden" name="IReportID" id="IReportID" style="display: none;">
                        <div class="pop_row_txtarea">
                            <div class="left_side">
                                <h5 style="padding-left: 5px;">Reviewer Remarks</h5>
                            </div>
                            <div class="right_side">
                                <textarea  class="right_txt_area_select" style="margin-left: -125px;" name="employeeRemarks" id="reviewerRemarks" placeholder="Remarks"></textarea>
                            </div>
                        </div>



                    </div>



                </div>

            </div>
            <style>
                .btn_rights{float:right;width:auto;height: 35px;background-color: #6AAD6A;margin-right: 35px;
                    color:white;}
                .btn_rights:hover{cursor:pointer;}
            </style>

            <div class="btn_row">
                <input type="button" class="btn_rights" id="updateReview" value="Update Review">
            </div>

    </form>
</div>

<!---Breached ReviewModal---------->
<div id="batchApproveRelated" class="modal-demo" style="display: none;margin-left: -360px;">
    <div class="top_header">
        <button type="button"  style="margin-right: -340px;" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Review Breached Discipline</h3>
    </div>
    <form class="form-horizontal" id="InvestigationReportForm">
        <div class="container_view" style="float:left;width:700px;height:auto;">
            <div class="right_sec">

                <!----------End Popup------------------------->
                <!---for Dialogoe---->
                <div class="left_block_coments">

                    <div class="pop_row_txtarea" style="margin-top: 5px;">
                        <div class="left_side">
                            <h5 style="padding-left: 5px;">Disciplinary Action</h5>
                        </div>
                        <div class="right_side">
                            <input type="hidden" id="selectDisciplinaryActionBatch" style="margin-left: -125px;">
                        </div>
                    </div>
                    <div class="coments_row">

                        <input type="hidden" name="IReportID" id="IReportIDBatch" style="display: none;">
                        <div class="pop_row_txtarea">
                            <div class="left_side">
                                <h5 style="padding-left: 5px;">Reviewer Remarks</h5>
                            </div>
                            <div class="right_side">
                                <textarea  class="right_txt_area_select" style="margin-left: -125px;" name="employeeRemarks" id="reviewerRemarksBatch" placeholder="Remarks"></textarea>
                            </div>
                        </div>



                    </div>



                </div>

            </div>
            <style>
                .btn_rights{float:right;width:auto;height: 35px;background-color: #6AAD6A;margin-right: 35px;
                    color:white;}
                .btn_rights:hover{cursor:pointer;}
            </style>

            <div class="btn_row">
                <input type="button" class="btn_rights" id="updateBatchReview" value="Update Review">
            </div>

    </form>
</div>

<style>
.modal-demo {

float:left;
background-color: #FFF;
padding-bottom: 15px;
border: 1px solid #000;
border-radius: 10px;
box-shadow: 0 8px 6px -6px black;
display: none;
text-align: left;
width: 600px;


}
.title {
border-bottom: 1px solid #ccc;
font-size: 18px;
line-height: 18px;
padding: 10px 20px 15px;
}
h4 {
font-size: 22px;
font-weight: 400;
line-height: 22px;
}
h1, h2, h4 {
font-family: "Dosis",sans-serif;
}
.text{
padding: 0 20px 20px;
}
.text , .title{
color: #333;
font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
font-size: 14px;
line-height: 1.42857;
}
button.close {
background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
border: 0 none;
cursor: pointer;
padding: 0;
}
.close {
position: absolute;
right: 15px;
top: 15px;
}

.close {
color: #ffffff;
float: right;
font-size: 21px;
font-weight: 700;
line-height: 1;
text-shadow: 0 1px 0 #fff;
}

.pop_row{
float:left;
width:100%;
height:auto;
margin-bottom: 7px;
}
.pop_row_txtarea{
float:left;
width:100%;
height:auto;
margin-bottom: 7px;
}
.left_side{
float:left;
width:40%;
height:auto;
}
.left_side h5{
line-height: 35px;
padding-left: 20px;
}
.right_side{
float:left;
width:60%;
height:auto;
}
.right_txt{
width:240px important;
height:auto important;
}
.top_header{
float:left;
width:100%;
height:45px;
background-color: #4f94cf;
margin-bottom: 10px;
border-top-right-radius: 10px;
border-top-left-radius: 10px;
border-bottom: 2px solid rosybrown
}
.top_header h3{
color:#ffffff
}
.right_txt_area{
width:240px;
height:100px;
padding:7px;
}
.btn-row{
width:auto;
float:right;
margin-right:-15px;
}
.btn-pop{
float:right;
/*width:80px;*/
height:35px;
background-color: #6AAD6A;
color:white;
margin-right: 120px;
}
.btn-pop:hover{
cursor: pointer
}
.view_model{
width:1000px;
}
.left_photo{
float:left;
width:100px;
height:100px;
margin-right: 10px;
margin-top: 10px;
border: 1px solid #d3d3d3
}
.right_sec{
float:left;
width:820px;
height:auto;
margin-right: 10px;
margin-bottom: 10px;
}
.left_lbl h5{
line-height: 35px;
}
.left_lbl{
float:left;
width:190px;
height:35px;
margin-right: 5px;
margin-bottom:10px;
padding-left: 5px;
}
.right_lbl{
float:left;
width:190px;
height:35px;
margin-bottom:10px
}
.right_lbl{
line-height: 35px;
padding-left: 5px;
}
.left_block{
width:400px;
height:auto;
float:left;
}
div.text form{
display: inline-block;
}

.modal-footer{
border-top: @gray-lighter solid 1px;
text-align: right;
}

</style>

<!--<script type="text/javascript" src="js/jquery-ui.js"></script>-->
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
    $( "#accordion" ).accordion();
    $( "#accordion1" ).accordion();
    var oTable;
    $(document).ready(function(e){
//        $('#status_title').select2();
        //Need To Display the list of All The Reported/Violated Disciplines
        oTable ='';
        var tableSelector = $('#pendingBreachedDisciplineReviews');
        var url_DT = "<?php echo base_url(); ?>dashboard_site/review_breached_disciplines/list";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "ReportID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* checkboxes */ {
                "mData": "checkboxes",
                "bVisible": true,
                "bSortable": false,
                "bSearchable": false
            },
            /* Name of The Employee Who Broke/Violated Company Discipline/Rule */ {
                "mData" : "AccusedEmployee"
            },
            /* Discipline That Have Been Breached/Violated By Employee */ {
                "mData" : "BreachedDiscipline"
            },
            /* Breach Date/Date of Occurrence */ {
                "mData" : "DateOfBreach"
            },
            /* Reported By (Employee Name) */ {
                "mData" : "ReportedBy"
            },
            /* Disciplinary Action */ {
                "mData" : "InvestigatedBy"
            },
            /* Shows The Status Of The Report */ {
                "mData" : "ReportStatus"
            },
            /* Shows The Status Of The Report */ {
                "mData" : "ViewEditActionButtons"
            }
        ];
        var HiddenColumnID_DT = 'ReportID';
        var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
        //End Of DataTables

        tableSelector.on('click','.disciplinaryActionReview',function(e){
            var reportID = $(this).closest('tr').attr('data-id');
            $('#IReportID').val(reportID);
            //Need To Get Values/Data for Popup Modal.
            var postData = {
                reportID: reportID
            };
            $.ajax({
                url:"<?php echo base_url(); ?>human_resource/get_report_details/view",
                type: "POST",
                data: postData,
                success:function(output){
                    try
                    {
                        //If Result is In JSON then Show The Edit Fields In Modal.
                        var jsonViewData = JSON.parse(output);
                        $('#userAvatar').attr('src',jsonViewData.EmployeeAvatar);
                        $('#employeeName').text(jsonViewData.AccusedEmployeeName);
                        $('#employeeCode').text(jsonViewData.AccusedEmployeeCode);
                        $('#BreachedDiscipline').text(jsonViewData.BreachedDiscipline);
                        $('#DateOfOccurrence').text(jsonViewData.BreachedDate);
                        $('#InvestigatedBy').text(jsonViewData.InvestigatedByEmployeeName);
                        $('#ReportedBy').text(jsonViewData.ReportedByEmployeeName);
                        $('#DisciplinaryAction').text(jsonViewData.DisciplinaryActionType);
                        $('#ReportStatus').html(jsonViewData.ReportStatus);
                        $('#employeeFeedBack').text(jsonViewData.EmployeeFeedBack);
                        $('#investigatorRemarksSpan').text(jsonViewData.InvestigatorRemarks);
//                        $('#reviewerRemarks').text(jsonViewData.ReviewerRemarks);

                        //If Has Authority Of Investigation Then Assign ReportID
                        $('#IReportID').val(reportID);
                    }
                    catch(e)
                    {
                        var data = output.split('::');
                        if(data[0] === "OK"){
                            Parexons.notification(data[1],data[2]);
                        }else if(data[0] === "FAIL"){
                            Parexons.notification(data[1],data[2]);
                        }
                    }
                }
            });
            //Need To Redirect User To Its Right Path.
            Custombox.open({
                target: '#reviewBreachedDisciplineModal',
                effect: 'fadein'
            });
            e.preventDefault();
        });

        //Selector in Popup
        var disciplinaryActionSelector = $('#selectDisciplinaryAction');
        var url = '<?php base_url(); ?>dashboard_site/load_all_available_disciplinary_actions';
        var id = "ID";
        var text = "Text";
        var minInputLength = 0;
        var placeholder = "Select Disciplinary Action";
        var multiple = false;
        commonSelect2(disciplinaryActionSelector,url,id,text,minInputLength,placeholder,multiple);

        //Selector in Popup
        var disciplinaryActionSelectorBatch = $('#selectDisciplinaryActionBatch');
        var url = '<?php base_url(); ?>dashboard_site/load_all_available_disciplinary_actions';
        var id = "ID";
        var text = "Text";
        var minInputLength = 0;
        var placeholder = "Select Disciplinary Action";
        var multiple = false;
        commonSelect2(disciplinaryActionSelectorBatch,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css('width','80%');

        //Extra Page Scripts
        $('tbody').addClass("table-row");
        $('#tableSearchBox').keyup(function(){
            oTable.fnFilter( $(this).val() );
        });
        $('#updateReview').on('click',function(e){
            e.preventDefault();
            var reportID = $('#IReportID').val();
            var reviewerRemarks = $('#reviewerRemarks').val();
            var disciplinaryActionID = $('#selectDisciplinaryAction').val();

            //Getting Input Values.


            var postData = {
                reportID: reportID,
                reviewerRemarks: reviewerRemarks,
                disciplinaryActionID: disciplinaryActionID
            };
            //Need To POST Values Using AJAX
            $.ajax({
               url:'<?php echo base_url(); ?>dashboard_site/update_breached_discipline_review',
                data :postData,
                type : 'POST',
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        //Close the Modal If Successfully Reviewed...
                        Custombox.close();
                        //Destroy And Recreate The Table
                        oTable.fnDestroy();
                        commonSelect2(disciplinaryActionSelector,url,id,text,minInputLength,placeholder,multiple);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });


        ///Code For Batch Reviews.
        $('.btn-right').hide();

        //Custom Page Work..
        tableSelector.on('click','#selectAllCheckBoxes',function(e){
            if($(this).is(':checked')){
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',true);
                $('.btn-right').show();
            }else{
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',false);
                $('.btn-right').hide();
            }
        });


        tableSelector.on('change', '.batchApprovalCheckBox', function (e) {
            if ($(this).is(':checked')) {
                $('.btn-right').show();
            } else {
                if ($('.batchApprovalCheckBox:checked').length > 0) {
                    $('.btn-right').show();
                } else {
                    $('.btn-right').hide();
                    $('#selectAllCheckBoxes').prop('checked',false);
                }
            }
        });

        $('#batchApproveBtn').on('click',function(e){
            Custombox.open({
                target: '#batchApproveRelated',
                effect: 'fadein'
            });
        });


        $('#updateBatchReview').on('click',function(e){
            var approvalIDs = new Array();
            var array = $('#pendingBreachedDisciplineReviews .batchApprovalCheckBox:checked');
            $.each(array, function (i, e) {
                approvalIDs.push($(e).closest('tr').attr('data-id'));
            });
            var postData = {
                batchType:"review",
                IDs:JSON.stringify(approvalIDs),
                disciplinaryAction:disciplinaryActionSelectorBatch.val(),
                disciplinaryActionRemarks:$('#reviewerRemarksBatch').val()
            };

            $.ajax({
                url:"<?=base_url();?>dashboard_site/review_breached_disciplines/batch",
                data:postData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        oTable.api().ajax.reload();
                        $('.btn-right').hide();
                        $('#selectAllCheckBoxes').prop('checked',false);
                        Custombox.closeAll();
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });


</script>
