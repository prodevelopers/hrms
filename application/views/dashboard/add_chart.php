<div class="contents-container">
	<style type="text/css">
		h4{
			padding-left: 20px;
			padding-top: 20px;
		}
		.remove_field{
			float: left;position: relative;
			right: 40px;
			top: 15px;
		}
        #autoSuggestionsList
        {
            width: 205px;
            overflow: hidden;
            border:1px solid #CCC;
            -moz-border-radius: 5px;
            border-radius: 5px;

            display:none;
        }
        .designing{
            top:200px;
            right:200px;
        }
        #autoSuggestionsList li
        {
            list-style:none;
            cursor:pointer;
            padding:5px;
            font-size:14px;
            font-family:Arial;
            margin:2px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        #autoSuggestionsList li:hover
        {
            border:1px solid #CCC;
        }
	</style>
	<div class="bredcrumb">Dashboard / Organizational Chart / Add Chart</div> <!-- bredcrumb -->
	<div class="left-nav">
        <ul>
            <li><a href="dashboard_site/organization_chart">View Chart</a></li>
            <li><a href="dashboard_site/add_chart">Make Chart</a></li>
        </ul>
    </div>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>

    <!---------------------------For Left-Top-Approvals Menu------------------------------>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


    <ul id="gn-menu" class="gn-menu-main">
        <li class="gn-trigger">
            <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
            <nav class="gn-menu-wrapper">
                <div class="gn-scroller">
                    <ul class="gn-menu">
                        <li>
                            <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                        </li>
                        <li>
                            <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                        </li>
                        <li>
                            <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                        </li>
                        <li>
                            <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                        </li>
                        <li>
                            <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                        </li>
                        <li>
                            <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                        </li>
                        <li>
                            <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                        </li>
                        <li>
                            <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                        </li>
                        <li>
                            <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </li>
    </ul>
    <!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
    <script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
    <script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
    <script>
        new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
    <script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
    <script>
        $(function() {
            $( "#dialog" ).dialog({
                autoOpen: false,
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });

            $( "#attendence" ).click(function() {
                $( "#dialog" ).dialog( "open" );
            });
        });
    </script>

    <!------------------------------------Menu Dashboard Approvals End------------------------------>

    <div class="right-contents1">

		<div class="head">Top Name</div>


			<form>
                <input type="hidden" id="id_input">
                <div class="row">
                    <h4>Root Employee</h4>
                    <input name="topName" id="topName"  class="serch" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                        <div class="autoSuggestionsList_l" id="autoSuggestionsList">
                        </div>
                    </div>
                    </div>
                <div class="form-right" id="employee_inf0" style="float:right;">
                    <div class="head">Information</div>
                    <div class="row2">
                        <span class="headingfive">Employee Name</span>
                        <i class="italica" id="emp_name"></i>
                    </div>
                    <div class="row2">
                        <span class="headingfive">Employee Code</span>
                        <i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                        <span  class="headingfive">Department</span>
                        <i class="italica" id="department"></i>
                    </div>
                    <div class="row2">
                        <span  class="headingfive">position</span>
                        <i class="italica" id="position"></i>
                    </div>
                   <!--  <div class="row2">
                        <span  class="headingfive">Last Deduction</span>
                        <i class="italica" id="amount"><span id="ded_type"></span>&nbsp;&nbsp;&nbsp;&nbsp;</i>
                    </div> -->
                </div>
				<br class="clear">
			</form>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<button class="btn green" id="AddRoot">Add Name </button>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
		<!-- sub -->
	`		<div class="head" style="margin-top:50px; text-align:left; ">Sub Name</div>
			<form>
                <div class="row">
                    <h4>Sub parent</h4>
                    <input type="text" id="multiSelectEmployeesForParent">
                </div>
				<div class="row">
					<h4>Sub Child</h4>
					<input type="text" id="multiSelectEmployees">
				</div>
				<div class="input_fields">
				</div>
				<br class="clear">
			</form>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<button class="btn green" id="AddNodes">Add Field </button>
				</div>
			</div>


		</div>

    </div>
		

	</div>
<!-- contents -->
<script type="text/javascript">
    function lookup(inputString) {
        if(inputString.length == 0) {
            $('#suggestions').hide();
            $('#employee_inf0').hide();
            location.reload();
        } else {
            $.post("dashboard_site/get_employeeData/", {queryString: ""+inputString+""}, function(data){
                if(data.length > 0) {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').show();
                    $('#autoSuggestionsList').html(data);
                }
            });
        }
    }
    function fill(name,empID) {
        $('#id_input').val(empID);
        $('#topName').val(name);
        $('#employee_code').append(employee_code);
        $('#emp_name').append(name);
        setTimeout("$('#suggestions').hide();", 200);
        $('#employee_inf0').show();
    }


    $(document).ready(function() {
        $('#topNameAutoComplete').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "<?php base_url() ?> dashboard_site/get_employeeData",
                    dataType: "json",
                    data: {
                        q: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                log(ui.item ?
                "Selected: " + ui.item.label :
                "Nothing selected, input was " + this.value);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });

        //Select2 Script
        $('#multiSelectEmployees').select2({
            minimumInputLength:1,
            multiple:true,
            width:'resolve',
            ajax: {
                type:"post",
                url: '<?php echo base_url()?>dashboard_site/select2_employee_get',
                dataType: 'json',
                quietMillis: 100,
                data: function(term, page) {
                    return {
                        term: term, //search term
                        page_limit: 10 // page size
                    };
                },
                results: function(data, page) {
                    var newData = [];
                    $.each(data, function (index,value) {
                        newData.push({
                            id: value.ID, //id part present in data
                            text: value.Text //string to be displayed
                        });
                    });
                    return { results: newData };
                }
            },
            initSelection : function (element, callback) {
                var data = element.val().split(',');
                var completeData = {id: data[0], text: data[1]};
//console.log(completeData);
                callback(completeData);
            }
        });

        //Select2 Script
        $('#multiSelectEmployeesForParent').select2({
            minimumInputLength:0,
            placeholder: 'Select Parent Employee',
            width:'resolve',
            ajax: {
                type:"post",
                url: '<?php echo base_url()?>dashboard_site/select2_get_available_employees',
                dataType: 'json',
                quietMillis: 100,
                data: function(term, page) {
                    return {
                        term: term, //search term
                        page_limit: 10 // page size
                    };
                },
                results: function(data, page) {
                    var newData = [];
                    $.each(data, function (index,value) {
                        newData.push({
                            id: value.ID, //id part present in data
                            text: value.Text //string to be displayed
                        });
                    });
                    return { results: newData };
                }
            },
            initSelection : function (element, callback) {
                var data = element.val().split(',');
                var completeData = {id: data[0], text: data[1]};
//console.log(completeData);
                callback(completeData);
            }
        });
/*        $('.select2-input').css('width','47%');
        $("#multiSelectEmployees").select2({ width: 'resolve' });*/

        //function to insert
        $('#AddNodes').on('click',function(e){
            var selectedValues = $('#multiSelectEmployees').val();
            var data = {
               ParentID : $('#multiSelectEmployeesForParent').val(),
                childIDs : selectedValues
            };
            $.ajax({
               url:'<?php echo base_url() ?>dashboard_site/insert_organization_data',
                data:data,
                type: 'post',
                success: function (output) {
                    console.log(output);
                }
            });
        });
        $('#AddRoot').on('click', function (e) {
            var data = {
                rootEmployee: $('#id_input').val()
            };
        $.ajax({
            url:'<?php echo base_url() ?>dashboard_site/update_root_employee',
            data: data,
            type: 'post',
            success: function (output) {
                console.log(output);
            }
        });
        });
    });
</script>


