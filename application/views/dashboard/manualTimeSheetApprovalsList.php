<style>
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    /*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
    .newbtn{
        float: right;
        margin-top: -30px;
        margin-right: 130px;
    }
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }
</style>


<div class="contents-container">
    <div class="bredcrumb">Dashboard /Approvals/ Timesheet</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/aproval_menu'); ?>
    <div class="right-contents1">

        <div class="head">Timesheet Approvals
            <div class="btn-right">
                <a class="btn green" id="batchApproveBtn" style="cursor: hand;">Approve</a>
                <a class="btn red" id="batchDeclineBtn" style="cursor: hand;">Decline</a>
            </div>
        </div>

        <div class="filter">
            <h4>Filter By</h4>
            <input type="hidden" placeholder="select Employees" id="selectEmployees">
            <input type="hidden" id="selectYear">
            <input type="hidden" id="selectMonth">
            <div class="clear"></div>
            <input type="button" value="Filters" id="filterButton" class="btn green newbtn" >
        </div>

        <table cellspacing="0" id="listManualTimeSheet">
            <thead class="table-head">
            <td>Employee ID</td>
            <td><input type="checkbox" id="selectAllCheckBoxes"> All</td>
            <td>Employee Name</td>
            <td>Employee Code</td>
            <td>Time Sheet Month</td>
            <td>Action</td>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
    var oTable;
    var selectedMonth;
    var selectedYear;
    $(document).ready(function() {

        oTable = '';
        //Load List of Approvals in DataTables.
       var selectorTableManualTimeSheet =  $('#listManualTimeSheet');
       var url_DT = "<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval/listEmployeesWithTimeSheetApprovals";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "EmployeeID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* checkboxes */ {
                "mData": "checkboxes",
                "bVisible": true,
                "bSortable": false,
                "bSearchable": false
            },
            /* Employee Name */ {
                "mData" : "EmployeeName"
            },
            /* EmployeeCode */ {
                "mData" : "EmployeeCode"
            },
            /* Project */ {
                "mData" : "MonthOfTimeSheet"
            },
            /* Actions */ {
                "mData" : "Actions"
            }
        ];
        var HiddenColumnID_DT = 'EmployeeID';
        var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);

        /**
         * Select2 Selectors Start Here..
         */
        /*The Selector for Selecting the Employee Employees*/
        var select2Selector = $('#selectEmployees');
        var url = "<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval/listEmployeesWithTimeSheetApprovalsSelect2";
        var id = "EmployeeID";
        var text = "EmployeeName";
        var minInputLength = 0;
        var placeholder = "Select Employees";
        var multiple = true;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);

        /*The Selector for Selecting the TimeSheet Year*/
        var select2Selector = $('#selectYear');
        var url = "<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval/listYearsWithTimeSheetApprovalsSelect2";
        var id = "ManualTimeSheetYear";
        var text = "ManualTimeSheetYear";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);

        /*The Selector for Selecting the TimeSheet Year*/
        var select2Selector = $('#selectMonth');
        var url = "<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval/listMonthsWithTimeSheetApprovalsSelect2";
        var id = "ManualTimeSheetMonthNumeric";
        var text = "ManualTimeSheetMonth";
        var minInputLength = 0;
        var placeholder = "Select Month";
        var multiple = false;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);

        /** End Of Select2 Selectors */
        var filters = 'aoData.push({"name":"year","value":$("#selectYear").val()}); aoData.push({"name":"employees","value":$("#selectEmployees").val()});  aoData.push({"name":"month","value":$("#selectMonth").val()});';
        $('#filterButton').on('click', function () {
            oTable.fnDestroy();
            selectedMonth = $("#selectMonth").val();
            selectedYear = $("#selectYear").val();
            commonDataTablesFiltered(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters)
        });
        $('.select2-container').css("width","223px");

        $('#listManualTimeSheet').on('click','.viewEmployeeTimeSheet', function () {
            var employeeID = $(this).closest('tr').attr('data-id');
            var rowMonthYear = $(this).parent().prev().text();
            var monthYear = rowMonthYear.split(",");
            var selectedRowMonthName = monthYear[0].trim();
            var selectedRowMonthNumeric = getMonthFromString(selectedRowMonthName);
            var selectedRowYear = monthYear[1].trim();
            var postData = {
                employee: employeeID
            };
            if(typeof selectedMonth != 'undefined' && selectedMonth.length > 0){

                    postData.selectedMonth = selectedMonth;

            }else{
                postData.selectedMonth = selectedRowMonthNumeric;
            }

            if(typeof selectedYear != 'undefined' && selectedYear.length > 0 ){
                postData.selectedYear = selectedYear;
            }else{
                postData.selectedYear = selectedRowYear;
            }
            if(typeof employeeID == 'undefined'){
                Parexons.notification('Some Error in Table, Please Contact the System Administrator About This Error','error');
                return;
            }
            //Redirect To TimeSheet To Approve the TimeSheet.
            var redirectURL = "<?php echo base_url(); ?>dashboard_site/manual_timesheet_approval/showEmployeeTimeSheetToApprove";
            console.log(redirectURL);
            $.redirect(redirectURL,postData,'POST');
        });
    });
    function getMonthFromString(mon){

        var d = Date.parse(mon + "1, 2012");
        if(!isNaN(d)){
            return new Date(d).getMonth() + 1;
        }
        return -1;
    }


    /// Need To Work For Batch Approvals Now.
    $(document).ready(function(){
        $('.btn-right').hide();

        //Custom Page Work..
        $('#listManualTimeSheet').on('click','#selectAllCheckBoxes',function(e){
            if($(this).is(':checked')){
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',true);
                $('.btn-right').show();
            }else{
                $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',false);
                $('.btn-right').hide();
            }
        });

        $('#listManualTimeSheet').on('change', '.batchApprovalCheckBox', function (e) {
            if ($(this).is(':checked')) {
                $('.btn-right').show();
            } else {
                if ($('.batchApprovalCheckBox:checked').length > 0) {
                    $('.btn-right').show();
                } else {
                    $('.btn-right').hide();
                    $('#selectAllCheckBoxes').prop('checked',false);
                }
            }
        });



        $('#batchApproveBtn').on('click',function(e){
            //first we need to get all the checked checkboxes
            var approvalIDs = new Array();
            var array = $('#table_list .batchApprovalCheckBox:checked');
            $.each(array, function (i, e) {
                approvalIDs.push($(e).closest('tr').attr('data-id'));
            });

            var postData = {
                batchType:"approval",
                IDs:JSON.stringify(approvalIDs)
            };

            $.ajax({
                url:"<?=base_url();?>dashboard_site/manual_timesheet_approval/batch",
                data:postData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        oTable.api().ajax.reload();
                        $('.btn-right').hide();
                        $('#selectAllCheckBoxes').prop('checked',false);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        $('#batchDeclineBtn').on('click',function(e){
            //first we need to get all the checked checkboxes
            var approvalIDs = new Array();
            var array = $('#table_list .batchApprovalCheckBox:checked');
            $.each(array, function (i, e) {
                approvalIDs.push($(e).closest('tr').attr('data-id'));
            });

            var postData = {
                batchType:"decline",
                IDs:JSON.stringify(approvalIDs)
            };

            $.ajax({
                url:"<?=base_url();?>dashboard_site/manual_timesheet_approval/batch",
                data:postData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        oTable.api().ajax.reload();
                        $('.btn-right').hide();
                        $('#selectAllCheckBoxes').prop('checked',false);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

    });

</script>
<!-- contents -->
