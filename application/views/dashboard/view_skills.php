<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
<script type="text/javascript">
/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");		
	}
</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div>

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		
		<div class="head">Complete Details </div>
			
           <div class="form-right">
                    
                            <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$job_history->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Employee ID</h5>
                                <i class="italica"><?php echo @$job_history->employee_code;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$job_history->designation_name;?></i>
                        </div>
            </div>     
			<?php /*?><table>
            
			<thead class="table-head class">
					<td colspan="4">Experiance</td>
				</thead>
				<tbody>
                <?php if(empty($complete_profile)){echo "<td>No Data Founded</td>";}else{
					foreach($complete_profile as $complete_profile):
					?>
				<tr class="table-row">
					<td>Job Title</td>
					<td><?php echo @$complete_profile->designation_name; ?></td>
					<td>Organization</td>
					<td><?php echo @$complete_profile->organization; ?></td>
				</tr>
				<tr class="table-row">
					<td>Employement Type</td>
					<td><?php echo @$complete_profile->employment_type; ?></td>
					<td>Start Date</td>
					<td><?php echo @$complete_profile->from_date; ?></td>
				</tr>
				<tr class="table-row">
					<td>End Date</td>
					<td><?php echo @$complete_profile->to_date; ?></td>
				</tr>	
                <?php endforeach;}?>
				</tbody>	
			</table><?php */?>
			<table>
			<thead class="table-head class">
					<td colspan="4">Skills</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Skill Name</td>
					<td>Experiance in Years</td>
                    </tr>
                <?php if(empty($complete_profile)){echo "<td>No Data Founded</td>";}else{
					foreach($complete_profile as $skill):
					?>
				<tr class="table-row">
					<td><?php echo @$skill->skill_name; ?></td>
					<td><?php echo @$skill->experience_in_years; ?></td>
				</tr>
                <?php endforeach;}?>
				</tbody>	
			</table>

        <table>
            <thead class="table-head class">
            <td colspan="4">Assigned Tasks</td>
            </thead>
            <tbody>
            <tr class="table-row">
                <td><h2>Task</h2></td>
                <td><h2>Project</h2></td>
            </tr>
            <?php if(empty($job_history)){echo "<td>No Data Founded</td>";}else{
                foreach($job_history as $tasks):
                    ?>
                    <tr class="table-row">
                        <td><?php echo $tasks->task_name; ?></td>
                        <td><?php echo $tasks->project_title; ?></td>
                    </tr>
                <?php endforeach;}?>
            </tbody>
        </table>
			
			
	</div>
    <!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
        $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
        $('#close-panel-bt').click(function() {
            $.panelslider.close();
        });
        $(document).ready(function(e){
            $('#dept, #design').select2();
        });
    </script>
    <!-- leftside menu end -->
