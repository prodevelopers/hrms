
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard /Approvals /Applicants Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>

	<div class="right-contents1">

		<div class="head">Selected Applicants Approvals</div>

		<!--	<div class="filter">
				<h4>Filter By</h4>
				<?php /*echo form_open('');*/?>
        <?php /*echo form_dropdown('status_title',$status,"class='select_47 filter1' id='status_title' onchange='this.form.submit()'");*/?>
        
        <?php /*echo form_close();*/?>
			</div>-->

			  <table cellspacing="0" id="table_list">
				<thead class="table-head">
                	<!--<td>Transcation ID</td>-->
					<td>Job Reference#</td>
					<td>Applicant Name</td>
					<td>Job Applied</td>
					<!--<td>Payment Mode</td>-->
					<td>Applied Date</td>
					<td>Action</td>
				</thead>
				  <?php if(!empty($applicants)){ foreach($applicants as $rec){?>
				<tr class="table-row">
					<td><?php echo $rec->referenceNo;?></td>
					<td><?php echo $rec->fullName;?></td>
					<td><?php echo $rec->title;?></td>
					<td><?php echo date_format_helper($rec->dateAppliedOn);?></td>
					<td><a href="dashboard_site/action_applicants_approval/<?php echo $rec->shtintr_id;?>"><button class= "btn green">Action</button></a></td>
				  </tr>
				  <?php }}else {echo "No Records Founded;";}?>
			</table>  	
	   </div>
	</div>
