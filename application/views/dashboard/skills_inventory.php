<?php /**
 * Created by PhpStorm.
 * User: Syed Haider Hassan.
 * Date: 12/5/2014
 * Time: 9:45 PM
 */ ?>

<style>
#pagination
{
    float:left;
    padding:5px;
	margin-top:15px;
}
#pagination a
{
    padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
    border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
    padding: 6px;
	background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
    margin-top:8px;
}
.dataTables_info
{
    color:#3474D0;
    font-size:14px;
	margin:6px;
}
.paginate_active
{
    padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
    float:right;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        //////// disabled all fields before project selection
        $("#filters *").attr("disabled", "disabled").off('click');
        $("#table *").attr("disabled", "disabled").off('click');
        //$("#filters").attr('disabled="disabled"');
        ///////
    });
    function table_data(){
        var project=$("#project_id").val();
        oTable = '';
        //Data Tables Script Here.
        var selector = $('#skillsList');
        var url = "<?php echo base_url(); ?>dashboard_site/skills_inventory_DT/"+project;
        $("#filters *").removeAttr("disabled", "disabled").on('click');
        $("#table *").removeAttr("disabled", "disabled").on('click');
        var aoColumns = [
            /* ID */ {
                "mData": "employment_id",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /*Check Boxes*/
            {
                "mData" : "CheckBoxes",
                "bSearchable" : false,
                "bSortable" : false
            },
            /* Employee Name */ {
                "mData" : "full_name"
            },
            /* Designation */ {
                "mData" : "designation_name"
            },
            /* Project */ {
                "mData" : "Projects"
            },
            /* Skills List */ {
                "mData" : "EmployeeSkills"
            },
            /* Actions */ {
                "mData" : "Actions"
            }
        ];
        var HiddenColumnID = 'employment_id';
        var sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(selector,url,aoColumns,sDom,HiddenColumnID);
        /*Script for CheckBoxes inside the DataTables As DataTables it Self Don't Provide Any Support for CheckBoxes.*/
        $("#selectAllCheckBoxes").click(function () {
            $('#skillsList tbody input[type="checkbox"]').prop('checked', this.checked);
        });
        //Custom Search Box OutSide DataTables
        $('#generalSearch').keyup(function(){
            oTable.fnFilter( $(this).val() );
        });
        //End of DataTables
        //Now Need to Work on The Filters of The DataTables..
        /*The Selector for Selecting the Employee Skills*/
        var select2Selector = $('#selectSkill');
        var url = "<?php echo base_url(); ?>dashboard_site/load_all_available_skills/"+project;
        var id = "SkillID";
        var text = "SkillName";
        var minInputLength = 0;
        var placeholder = "Select Skills";
        var multiple = true;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css("width","223px");
//End of the CommonSelect2 function

        //Doing Some Function if select2 has selected some item
        $('#selectSkill').on("change", function(e) {
            var employeeID = e.val;
            url = "<?php echo base_url(); ?>dashboard_site/skills_inventory_DT/"+project;
            var filters = 'aoData.push({"name":"skill_name","value":'+JSON.stringify(employeeID)+'});  aoData.push({"name":"experience_in_years","value":$("#selectExperience").val()});';
//$('#ManageFormsInGroups').dataTable().api().ajax.url(url).load();
//oTable.fnReloadAjax('google.com');
            oTable.fnDestroy();
            commonDataTablesFiltered(selector,url,aoColumns,sDom,HiddenColumnID,filters);
        });

        //Doing Some Function if select2 has selected some item
        $('#selectExperience').on("change", function(e) {
            var employeeID = e.val;
            console.log(JSON.stringify(employeeID));
            url = "<?php echo base_url(); ?>dashboard_site/skills_inventory_DT/"+project;
            var skills = JSON.stringify($('#selectSkill').select2("val"));
            var filters = 'aoData.push({"name":"skill_name","value":'+skills+'});  aoData.push({"name":"experience_in_years","value":'+JSON.stringify(employeeID)+'});';
//$('#ManageFormsInGroups').dataTable().api().ajax.url(url).load();
//oTable.fnReloadAjax('google.com');
            oTable.fnDestroy();
            commonDataTablesFiltered(selector,url,aoColumns,sDom,HiddenColumnID,filters);
        });

//Function To Execute when Clicked On View Button
        $('#skillsList').on('click','.view_skills', function (e) {
            var EmployeeID = $(this).closest('tr').attr('data-id');
            var redirectURL = 'dashboard_site/view_skills/'+EmployeeID;
            window.location.href = redirectURL;
        });
        //Function To Execute When Assign Task is Clicked for Single or Multiple Employees.
        $('#assignTasksBtn').on('click', function (e) {
            var EmployeeIDs = new Array();
            var array = $('#skillsList input:checked');
            $.each(array, function (i, e) {
                if ($(e).attr("id") != 'selectAllCheckBoxes') {
                    EmployeeIDs.push($(e).closest('tr').attr('data-id'));
                }
            });
            if(EmployeeIDs.length >0){
                var redirectURL = "<?php echo base_url(); ?>dashboard_site/assign_task_to_employees/"+project;
                $.redirect(redirectURL,{'employeeIDs':EmployeeIDs},'POST');
            }else {
                Parexons.notification('No Employees Selected','error');
            }
        });
        $('tbody').addClass('table-row');
}

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function confirm()
{
    alert('Are You Sure to Delete Record ...?');
}
</script>

<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Skills Inventory</div> <!-- bredcrumb -->


<div class="right-contents">

    <div class="head">Skills Inventory</div>
<div class="filter">
    <h4>Select By Project</h4>
    <?php echo form_dropdown('project',$projects,'','class="commonGeneralSelect2" id="project_id" onchange="table_data();"');?>
    <!--<select class="commonGeneralSelect2">
        <option>Select Project</option>

    </select>-->
</div>
    <!-- filter -->
    <div class="filter" id="filters">
        <h4>Filter By</h4>
<form>
    <input type='hidden' name='selectSkill' id='selectSkill'/>


    <select id="selectExperience" name="selectExperience" class="commonGeneralSelect2">
        <option value="">Select Year of Experience</option>
        <option value="1">1 Year</option>
        <option value="2">2 Years</option>
        <option value="3">3 Years</option>
        <option value="4">4 Years</option>
        <option value="5">5 Years or Above</option>
    </select>
    <input type="text" id="generalSearch" placeholder="General Search Here" style="width: 30%;font-size: 100%;"/>
</form>

    </div>

<div id="table">
    <!-- table -->
    <table cellspacing="0" id="skillsList">
        <thead class="table-head">
        <tr>
        <td>Employee ID</td>
        <td><input type="checkbox" id="selectAllCheckBoxes" value="" />&nbsp;All</td>
        <td width="20%">Employee Name</td>
        <td width="20%">Designation</td>
        <td width="20%">Project</td>
        <td width="20%">Skills List</td>
        <td width="20%"><span class="fa fa-eye"></span></td>
        </tr>
        </thead>
    </table>
    <!--button group-->
                <div class="row">
                    <div class="button-group">
                        <button class="btn green" id="assignTasksBtn">Assign Tasks</button>
                    </div>
                </div>

</div>
</div>
</div>
<!-- contents -->
