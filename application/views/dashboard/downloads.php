<!---------------------------For Left-Top-Approvals Menu------------------------------>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/dashboard_menu/css/component.css" />


<ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger">
        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
        <nav class="gn-menu-wrapper">
            <div class="gn-scroller">
                <ul class="gn-menu">
                    <li>
                        <a  id="attendence" class="gn-icon gn-icon-download">Register Attendance</a>
                    </li>
                    <li>
                        <a href="dashboard_site/goto_list"><span class="fa fa-list space"></span>Go To List</a>
                    </li>
                    <li>
                        <a href="dashboard_site/download_app"><span class="fa fa-download space"></span>Downloads</a>
                    </li>
                    <li>
                        <a href="dashboard_site/organization_chart"><span class="fa fa-sitemap space"></span>Organization Chart</a>
                    </li>
                    <li>
                        <a href="dashboard_site/skills_inventory"><span class="fa fa-book space"></span>Skills Inventory</a>
                    </li>
                    <li>
                        <a href="dashboard_site/staff_activity_schedule"><span class="fa fa-calendar space"></span>Staff Activity Scheduler</a>
                    </li>
                    <li>
                        <a href="dashboard_site/phone_book"><span class="fa fa-phone space"></span>Phonebook</a>
                    </li>
                    <li>
                        <a href="dashboard_site/approvals"><span class="fa fa-bell space"></span>Approvals</a>
                    </li>
                    <li>
                        <a href="dashboard_site/trash_log"><span class="fa fa-trash-o space"></span>Trash</a>
                    </li>
                </ul>
            </div>
        </nav>
    </li>
</ul>
<!--<script src="<?php /*echo base_url();*/?>assets/chart-plugin/Chart.js"></script>
<script src="<?php /*echo base_url();*/?>assets/js/dialog.js"></script>-->
<script src="<?php /*echo base_url()*/?>assets/dashboard_menu/js/classie.js"></script>
<script src="<?php echo base_url()?>assets/dashboard_menu/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script>
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#attendence" ).click(function() {
            $( "#dialog" ).dialog( "open" );
        });
    });
</script>

<!------------------------------------Menu Dashboard Approvals End------------------------------>


<div class="contents-container">
	<div class="bredcrumb">Dashboard / Downloads</div>
<?php $this->load->view('includes/dl_left'); ?>
	<div class="right-contents1">
		<div class="head">Downloads HRMS Application</div>
			<ul class="downloads">
				<li>
					<a href="assets/Apps/Parexons.apk">
						<img src="assets/img/Parexon_apps.png" alt="" width="70%" height="70%"><br>
						<p style="color:#00F">Download Attendce App</p>
					</a>
				</li>


                <div class="row">  
<!--             <?php /*if(!empty($download)){
					foreach($download as $load){ 
					$explod = explode('.', $load->attached_file);
					//echo "<pre>"; print_r($explod);die;*/?>
				<li>
                <?php /*if($explod[1] == 'pdf'){*/?>
					<a href="dashboard_site/download/<?php /*echo $load->attached_file */?>" target="new">
						<img src="assets/img/pdf.jpg" alt="" width="70%" height="70%"><br>
						<p><?php /*echo $load->remarks */?></p>
					</a>
                    <?php /*}else if($explod[1] == 'docx'){*/?>
					<a href="dashboard_site/download/<?php /*echo $load->attached_file */?>" target="new">
						<img src="assets/img/ms.jpg" alt="" width="70%" height="70%"><br>
						<p><?php /*echo $load->remarks */?></p>
					</a>  
                    <?php /*}else if($explod[1] == 'xls' || $explod[1] == 'xlsx'){*/?>
					<a href="dashboard_site/download/<?php /*echo $load->attached_file */?>" target="new">
						<img src="assets/img/excel.jpg" alt="" width="70%" height="70%"><br>
						<p><?php /*echo $load->remarks */?></p>
					</a>       
                    <?php /*}else {*/?>
					<a href="dashboard_site/download/<?php /*echo $load->attached_file */?>" target="new">
						<img src="upload/Download_Files/<?php /*echo $load->attached_file */?>" alt="" width="20%" height="20%"><br>
						<p><?php /*echo $load->remarks */?></p>
					</a>                   
                    <?php /*} */?>
				</li>
                --><?php /*} } */?>
                </div>
               
				<!--<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>
				<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>
				<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>
				<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>
				<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>
				<li>
					<a href="">
						<img src="assets/img/chart.png" alt="" width="70%" height="70%"><br>
						<p>Download Attendce App</p>
					</a>
				</li>-->
			</ul>
		</div>
    <div class="right-contents1">
        <div class="head">Downloads Files</div>
        <div class="row downloads">
            <?php
            if(!empty($downloads)) {
                foreach ($downloads as $download) {
                    echo "<a href='upload/Download_Files/" . $download->attached_file . "'>";
                    $extension = end(explode('.', $download->attached_file));
                    if (strtolower($extension) === "pdf"  || strtolower($extension) === 'pdf') {
                        echo '<div style="width: 11%;float: left;padding:1%;" data-toggle="tooltip" data-placement="top" title="'.$download->remarks.'">';
                        echo "<img src='assets/img/pdf.jpg' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    elseif (strtolower($extension) === "docx" || strtolower($extension) === "doc") {
                        echo '<div style="width: 11%;float: left; padding:1%;" data-toggle="tooltip" data-placement="top" title="'.$download->remarks.'">';
                        echo "<img src='assets/img/ms.jpg' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    elseif (strtolower($extension) === 'xlsx' || strtolower($extension) === 'xls') {
                        echo '<div style="width: 11%;float: left;padding:1%;" data-toggle="tooltip" data-placement="top" title="'.$download->remarks.'">';
                        echo "<img src='assets/img/excel.jpg' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    elseif (strtolower($extension) === 'png' || strtolower($extension) === 'png') {
                        echo '<div style="width: 11%;float: left;padding:1%;" data-toggle="tooltip" data-placement="top"title="'.$download->remarks.'">';
                        echo "<img src='assets/img/Preview-icon.png' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    elseif (strtolower($extension) === 'jpg' || strtolower($extension) === 'jpg') {
                        echo '<div style="width: 11%;float: left;padding:1%;" data-toggle="tooltip" data-placement="top"title="'.$download->remarks.'">';
                        echo "<img src='assets/img/Preview-icon.png' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    elseif (strtolower($extension) === 'gif' || strtolower($extension) === 'gif') {
                        echo '<div style="width: 11%;float: left;padding:1%;" data-toggle="tooltip" data-placement="top"title="'.$download->remarks.'">';
                        echo "<img src='assets/img/Preview-icon.png' alt=''  width='90%' height='90%'>";
                        echo "<p style='color:#00F'>".substr($download->remarks,0,6)."...</p>";
                        echo '</div>';
                    }
                    echo "</a>";

                }
            }
            ?>
        </div>
    </div>
</div><!--/.container-->
<!-- contents -->
<!--<link rel="stylesheet" href="--><?php //echo base_url()?><!--assets/jqueryModals/style-popup.css" type="text/css"/>-->
<!--<script type="text/javascript" src="--><?php //echo base_url();?><!--assets/js/jquery.min.js"></script>-->
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
