<style>
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    /*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
    .paginate_button
    {
        padding: 6px;
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
    .dataTables_length {
        position: relative;
        left: -0.2%;
        top: -20px;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -21px;
        float:left;
    }

    .dataTables_filter input {
        width: 180px;
    }
    .filter{
        display: inline-block;
    }
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }
</style>
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard /Approvals /Contract Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>
	<div class="right-contents1">

		<div class="head">Contract Approvals
            <div class="btn-right">
                <a class="btn green" id="batchApproveBtn" style="cursor: hand;">Approve</a>
                <a class="btn red" id="batchDeclineBtn" style="cursor: hand;">Decline</a>
            </div>
        </div>

			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
                <input type="text" id="tableSearchBox">
        <?php echo form_dropdown('status_title',$status,'',"class='select_47 filter1' id='status_title'");?>

        <?php echo form_close();?>
			</div>
			  <table cellspacing="0" id="extensionApprovalsTable">
				<thead class="table-head">
                    <td>ID</td>
                    <td><input type="checkbox" id="selectAllCheckBoxes"> All</td>
                    <td>Employee ID</td>
                    <td>Employee Name</td>
                    <td>Designation</td>
                    <td>Start Date</td>
                    <td>Completion Date</td>
                    <td>Status</td>
                    <td>Action</td>
				</thead>
                <tbody></tbody>
			</table>  	
	   </div>
	</div>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(e){

    //// Need To Work ON New Way Of DataTables..

    oTable ='';
    var extensionApprovalsTable = $('#extensionApprovalsTable');
    var url_DT = "<?php echo base_url(); ?>dashboard_site/extend_approval/list";
    var aoColumns_DT = [
        /* ID */ {
            "mData": "ContractID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
        /* checkboxes */ {
            "mData": "checkboxes",
            "bVisible": true,
            "bSortable": false,
            "bSearchable": false
        },
        /* Employee Code */ {
            "mData" : "employee_code"
        },
        /* Employee Full Name */ {
            "mData" : "full_name"
        },
        /* Employee Designation Here */ {
            "mData" : "designation_name"
        },
        /* Employee Designation Here */ {
            "mData" : "strt_date"
        },
        /* Employee Designation Here */ {
            "mData" : "end_date"
        },
        /* Reported By (Employee Name) */ {
            "mData" : "status_title"
        },
        /* Leave From Date */ {
            "mData" : "Action"
        }
    ];
    var HiddenColumnID_DT = 'ContractID';
    var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    var filters = 'aoData.push({"name":"status_title","value":$("#status_title").val()});';
    commonDataTablesFiltered(extensionApprovalsTable,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters)

    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
    $('tbody').addClass('table-row');


    $(".filter1").on('change',function(e) {
        oTable.fnDraw();
    });


    ///Now Other Functionality Of this Page Like Batch Approvals Checkboxes Selectors Etc.

    $('#status_title').select2();
    $('.btn-right').hide();

    //Custom Page Work..
    extensionApprovalsTable.on('click','#selectAllCheckBoxes',function(e){
        if($(this).is(':checked')){
            $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',true);
            $('.btn-right').show();
        }else{
            $(this).parents('table').find('.batchApprovalCheckBox').prop('checked',false);
            $('.btn-right').hide();
        }
    });


    extensionApprovalsTable.on('change', '.batchApprovalCheckBox', function (e) {
        if ($(this).is(':checked')) {
            $('.btn-right').show();
        } else {
            if ($('.batchApprovalCheckBox:checked').length > 0) {
                $('.btn-right').show();
            } else {
                $('.btn-right').hide();
                $('#selectAllCheckBoxes').prop('checked',false);
            }
        }
    });



    $('#batchApproveBtn').on('click',function(e){
        //first we need to get all the checked checkboxes
        var approvalIDs = new Array();
        var array = $('#extensionApprovalsTable .batchApprovalCheckBox:checked');
        $.each(array, function (i, e) {
            approvalIDs.push($(e).closest('tr').attr('data-id'));
        });

        var postData = {
            batchType:"approval",
            IDs:JSON.stringify(approvalIDs)
        };

        $.ajax({
            url:"<?=base_url();?>dashboard_site/extend_approval/batch",
            data:postData,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    oTable.api().ajax.reload();
                    $('.btn-right').hide();
                    $('#selectAllCheckBoxes').prop('checked',false);
                }else if(data[0] === 'FAIL'){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    $('#batchDeclineBtn').on('click',function(e){
        //first we need to get all the checked checkboxes
        var approvalIDs = new Array();
        var array = $('#extensionApprovalsTable .batchApprovalCheckBox:checked');
        $.each(array, function (i, e) {
            approvalIDs.push($(e).closest('tr').attr('data-id'));
        });

        var postData = {
            batchType:"decline",
            IDs:JSON.stringify(approvalIDs)
        };

        $.ajax({
            url:"<?=base_url();?>dashboard_site/extend_approval/batch",
            data:postData,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    oTable.api().ajax.reload();
                    $('.btn-right').hide();
                    $('#selectAllCheckBoxes').prop('checked',false);
                }else if(data[0] === 'FAIL'){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });


});
</script>

