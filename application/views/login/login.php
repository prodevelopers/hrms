<!DOCTYPE html>
<?php error_reporting(0);?>
<html lang="en" class="no-js">
<head>
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<!-- <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Montserrat+Alternates' rel='stylesheet' type='text/css'> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'> -->
<style>
*{
	padding:0;
	margin: 0;
}

body
{
	font-family: "calibri";

}
.container{
	width: 80%;
	margin: 0px auto;
	min-height: 300px;
	overflow: hidden;
}
.left-side{
	width: 69.6%;
	float: left;
}
.right-side{
	width: 30%;
	background: #fff;
	border: 1px solid #ccc;
	background: #fff;
	margin-top: 1em;
	height: 400px;
	float: left;
}
h3{
	font-size: 14px;
	text-align: center;
	font-weight: normal;
	color: #f60;
	line-height: 40px;
	background: #333;
}
.login-head{
	background: #4D6684;
	color: #FFF;
	line-height: 35px;
	font-size: 18px;
	padding-left: 15px;
}
.row{
	padding-left: 2.7em;
}
.row h4{
	font-weight: normal;
} 
@media (max-width:469px)
{
.firsta{
	width: 100%;
}
.firsta h1{
	color: #4D6684;
	font-size: 12px;
	/*font-style: italic;*/
	font-weight: normal;
	text-transform: capitalize;
}
.first h1{
	color: #4D6684;
	font-size: 1px;
}
input[type="text"],input[type="password"]{
	width: 80%;
	height: 28px;
	background: #fff;
	border:thin solid #ccc;
	margin: 0.5em 0;
	padding-left: 5px;
}
.right-side{
	width: 100%;
	background: #fff;
	border: 1px solid #ccc;
	background: #fff;
	margin-top: 1em;
	height: 200px;
	float: left;
}
input[type="submit"]{
	padding: 0.5em 1.5em;
	margin-left:14.5%;
	width: 72.8%;
	background: #4D6684;
	box-shadow: 0px 0px 2px rgba(0,0,0,0.6);
	border:none;
	color: #fff;
	margin-top: 0.6em;
}
input[type="submit"]:hover{
	background: #666;
	width: 72.8%;
	-webkit-transition:background ease-in-out 0.5s;
	-o-transition:background ease-in-out 0.5s;
	transition:background ease-in-out 0.5s;
}
input[type="text"]:hover,input[type="password"]:hover{
	border:thin solid cyan;
}
input[type="text"]:focus,input[type="password"]:focus{
	background: #fff;
}
}
@media (max-width:1089px)
{
.firsta{
	width: 100%;
}
.firsta h1{
	color: #4D6684;
	font-size: 12px;
	/*font-style: italic;*/
	font-weight: normal;
	text-transform: capitalize;
}
.first h1{
	color: #4D6684;
	font-size: 1px;
}
input[type="text"],input[type="password"]{
	width: 98%;
	height: 28px;
	background: #fff;
	border:thin solid #ccc;
	margin: 0.5em 0;
	padding-left: 5px;
}
.right-side{
	width: 98%;
	background: #fff;
	border: 1px solid #ccc;
	background: #fff;
	margin-top: 1em;
	height: 400px;
	float: left;
}
input[type="submit"]{
	width: 62.8%;
	background: #4D6684;
	box-shadow: 0px 0px 2px rgba(0,0,0,0.6);
	border:none;
	color: #fff;
	margin-top: 0.6em;
}
input[type="submit"]:hover{
	background: #666;
	width: 72.8%;
	-webkit-transition:background ease-in-out 0.5s;
	-o-transition:background ease-in-out 0.5s;
	transition:background ease-in-out 0.5s;
}
input[type="text"]:hover,input[type="password"]:hover{
	border:thin solid cyan;
}
input[type="text"]:focus,input[type="password"]:focus{
	background: #fff;
}
}
input[type="text"],input[type="password"]{
	width: 80%;
	height: 28px;
	background: #fff;
	border:thin solid #ccc;
	margin: 0.5em 0;
	padding-left: 5px;
}
input[type="submit"]{
	padding: 0.5em 1.5em;
	margin-left:14.5%;
	width: 69%;
	background: #4D6684;
	box-shadow: 0px 0px 2px rgba(0,0,0,0.6);
	border:none;
	color: #fff;
	margin-top: 0.6em;
}
input[type="submit"]:hover{
	background: #666;
	width: 69%;
	-webkit-transition:background ease-in-out 0.5s;
	-o-transition:background ease-in-out 0.5s;
	transition:background ease-in-out 0.5s;
}
input[type="text"]:hover,input[type="password"]:hover{
	border:thin solid cyan;
}
input[type="text"]:focus,input[type="password"]:focus{
	background: #fff;
}
#link{
	position: relative;
	left: 14%;
	width: 200px;
	top:10px;
}
#link a{
	font-size: 11px;
	text-decoration: none;
	color: #000;
}
#link a:hover{
	text-decoration:underline;
}
.first{
	width: 100%;
	border-bottom: solid 2em #4D6684;
}
.first h1{
	color: #4D6684;
	font-size: 33px;
	line-height: 80px;
	margin-left: 0.5em;
}
.firsta{
	width: 100%;
}
.firsta h1{
	color: #4D6684;
	font-size: 26px;
	/*font-style: italic;*/
	padding: 2em 1em 0.7em 2em;
	font-weight: normal;
	text-transform: capitalize;
}
.firsta span{
	font-weight: bold;
}
.first img{
	float: left;
}
.second{
	width: 65%;
	margin: 0 auto;
}
footer{
	width: 80%;
	margin: 0px auto;
	border-top: 3px solid #4D6684;
	margin-top: 1em;
}
footer img{
	padding:1em;
	float: left;
}
footer p{
	float: left;
	font-size: 13px;
	line-height: 18px;
	font-weight: bold;
	padding: 1.2em 0em;
}
footer a{
	text-decoration: none;
	color: #333;	
}
footer a:hover{
	color: #888;
}
@media (max-width:769px)
{
.firsta{
	width: 100%;
}
.firsta h1{
	color: #4D6684;
	font-size: 16px;
	/*font-style: italic;*/
	font-weight: normal;
	text-transform: capitalize;
}
.first h1{
	color: #4D6684;
	font-size: 15px;
	margin-left: 0.5em;
}
input[type="text"],input[type="password"]{
	width: 75%;
	height: 28px;
	background: #fff;
	border:thin solid #ccc;
	margin: 0.5em 0;
	padding-left: 5px;
}
.right-side{
	width: 98%;
	background: #fff;
	border: 1px solid #ccc;
	background: #fff;
	margin-top: 1em;
	height: 260px;
	float: left;
}
input[type="submit"]{
	padding: 0.5em 2em;
	margin-left:18%;
	width: 60.8%;
	background: #4D6684;
	box-shadow: 0px 0px 2px rgba(0,0,0,0.6);
	border:none;
	color: #fff;
	margin-top: 0.6em;
}
input[type="submit"]:hover{
	background: #666;
	width: 60.8%;
	-webkit-transition:background ease-in-out 0.5s;
	-o-transition:background ease-in-out 0.5s;
	transition:background ease-in-out 0.5s;
}
input[type="text"]:hover,input[type="password"]:hover{
	border:thin solid cyan;
}
input[type="text"]:focus,input[type="password"]:focus{
	background: #fff;
}
}





</style>	
</head>
<body>
<div class="container">
<div class="first">
		<h1>Human Resource <span style="color:#999;">Management System</span><br>
		</h1>
</div>
<!-- < -->
<div class="left-side">
	<div class="firsta">
		<h1><span>A Performance Driven Human Resource Management Solution </span><br>By Parexons IT Solutions & Services</h1>
	</div>
	
	<div class="second">
		<img src="assets/images/hrmain.jpg" width="60%" height="60%">
	</div>
	
</div>
<div class="right-side">
<h3 id="hide"><?php echo validation_errors('<p class="error">');  ?><?php echo $msg; ?></h3>
	 <div id="login">
		<div class="login-head">Recover Password </div>
        <?php echo form_open('');?>
			<div class="row" style="width:84%; margin-top:0.7em;">
				<h4>User Name </h4>
				<input type="text" name="user_email" placeholder="Email Address Here" required>
			</div>
			<br class="clear">
			
			<input type="submit" class="btn green" name="Login" value="Send">
            <?php echo form_close();?>
	</div>
<!--<a href="<?php /*echo base_url();*/?>">sign in </a>-->
</div>
</div>
<footer>
	<img src="assets/images/pxn.png" width="5%" height="5%" />
	<p>Parexons<br> IT Solutions & Services<br><a href="http://parexonx.com">Business@parexons.com</a> </p>
</footer>
</body>
</html>
<script src="assets/js/jquery-1.9.1.js"></script>
<script>
$(document).ready(function(){
	$("#hide").delay(2000).slideUp().hide(2000);
});
</script>