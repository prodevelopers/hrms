<!DOCTYPE html>
<?php error_reporting(0);?>
<html lang="en" class="no-js">
<head>
	<title>Parexons HRM</title>
     <base href="<?php echo base_url(); ?>">
	<link href="img/favicon-img.png" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
	<link type="text/css" rel="stylesheet" href="assets/css/popModal.css">
	<link rel="stylesheet" type="text/css" href="assets/css/form.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">

</head>


<body>





<!-- header ends -->

<br style="clear:both;">

<!-- contents -->

<div class="contents-container">

<style type="text/css">
	#login{
		width: 30%;
		margin: 0 auto;
		/*float: right;*/
		/*border: dotted 1px #ccc;*/
		padding-bottom: 10px;
		background: #fff;
		min-height: 180px;
		box-shadow: 1px 1px 2px 1px #666;
	}
	.login-head{
	background: #4D6684;
	color: #fff;
	padding: 8px 0 8px 20px;
}
.login .row label{
	float: none !important;
}
.message{
	width: 50%;
	margin: 0 auto;
	display: table;
	text-align: center;
	padding: 10px;
	/*background: rgba(225,41,43,0.4);*/
	/*border: solid 1px rgb(255,41,43);*/
}
.message h3{
	color: rgb(255,41,43);
	font-weight: normal;
	font-size: 0.9em;
}
</style>

	<div class="message">
    
		<h3><?php echo validation_errors('<p class="error">');  ?><?php echo $msg; ?></h3>
	</div>

	<div id="login">
		<div class="login-head">Login Here</div>
		<!--<form action="site/validate_credential">-->
        <?php echo form_open('');?>
			<div class="row" style="width:100%">
				<h4>User Name</h4>
				<input type="text" name="user_name" required>
			</div>
			<br class="clear">
			<div class="row" style="width:100%">
				<h4>Password</h4>
				<input type="password" name="password" required>
			</div>
			<input type="submit" class="btn green" name="log" value="Login">
			<!--<button class="btn green">Login</button>-->
			<h4 style="float:left">forgot Password</h4>
            <?php echo form_close();?>
		<!--</form>-->

	</div>

	
	

</div>
<!-- contents -->

<br style="clear:both;">

<?php #include("includes/footer.php"); ?>