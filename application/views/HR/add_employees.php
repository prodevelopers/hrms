<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee</div> <!-- bredcrumb -->

	<div class="right-contents full">

	<!-- hide show -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#login").hide();
    });
	//});
/// To Check Employee Code Already Exist
 function check_emp_code(){
   $(document).ready(function () {
    var employee_code = $('#emp_code').val();
    $.ajax({
      type: "POST",
      url: "human_resource/check_employee_code",
      data: {employee_id:employee_code} ,
      success: function(data) {
      $('#check_emp').html(data); 
  }
});
});  
 }
/// Function for Password Confirmation


</script>
		<script type="text/javascript">
			function check_cnic()
			{
				$(document).ready(function ()
				{
					var fisrt_part = $('#nic1').val();
					var second_part = $('#nic2').val();
					var third_part = $('#nic3').val();
					var cnic = fisrt_part +'-'+second_part +'-'+third_part;
					$.ajax({
						type: "POST",
						url: "human_resource/check_cnic_exist",
						data: {cnic:cnic} ,
						success: function(data)
						{
							$('#check_cnic').html(data);
						}
					});
				});
			}
			/// To Forward Focus to Next Textfield
			$(document).ready(function()
			{
				$('input').keyup(function()
				{
					if(this.value.length == $(this).attr("maxlength"))
					{
						$(this).next().focus();
					}
				});
			});
		</script>


<style type="text/css">
div.activeToolTip 
{
float:left;
 display:block;
 visibility:visible;
 position:relative;
 left:-3em;
 top:0.6em;
 background-color:rgba(0,0,0,0.2);
 font-family: Verdana, Arial, Helvetica, sans-serif;
 font-size: 0.75em;
 line-height: 30px;
 font-weight:bold;
 color: #000;
 text-align:center;
 border:1px solid black;
 filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);
 width:200px;
 height:30px;
}
div.idleToolTip 
{
 display:none;
 visibility:hidden;
}
</style>

		<div class="head">Add Employee</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php echo  $this->session->flashdata('msg');?></div>
			
			<?php echo form_open_multipart(); ?>

		<div class="row">
			<h4>Employee ID</h4>
<!--			<input type="text" name="employee_code" id="emp_code" onchange="return check_emp_code()" required onmouseover="document.getElementById('toolTipDiv').className='activeToolTip'" onmouseout="document.getElementById('toolTipDiv').className='idleToolTip'">-->
			<input type="text" name="employee_code" id="emp_code" value="<?php echo isset($newFullEmployeeCode)?$newFullEmployeeCode:'' ?>" readonly>
		</div>
		<!-- To Display Message "Ok" or "Already Exist" -->
		<div id="check_emp"></div>
		<div class="relative">
			<div id="toolTipDiv" class="idleToolTip"><?php echo "Last Inserted Code! &nbsp;".$last_emp_code;?></div>
		</div>
		<br class="clear">
				<div class="row">
					<h4>First Name</h4>
					<input type="text" name="first_name" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Last Name</h4>
					<input type="text" name="last_name" required="required">
				</div>
		<br class="clear">
		<div class="row">
			<h4>CNIC</h4>
			<input type="text" id="input" name="nic1" style="width: 40px; float: left;" maxlength="5" placeholder="13568" required="required">
			<input type="text" id="input1" name="nic2" style="width: 110px; float: left;" maxlength="7" placeholder="2684359" required="required">
			<input type="text" id="input2" name="nic3" style="width: 20px; float: left;" maxlength="1" placeholder="7" onkeyup="return check_cnic()" required="required">
			<div id="check_cnic"></div>
		</div>


				<br class="clear">
				<div class="row">
					<h4>Photo</h4>
					<input type="file" name="employee_photo">
				</div>

		<br class="clear">
				<div class="row">
					<h4>Create Login Details</h4>
					<input type="checkbox" value="login-details" name="login" id="check">
				</div>
		<!--<br class="clear">

    <!--<div class="row login">
            <h4>User Name</h4>
            <input type="text" name="user_name">
        </div>-->
				<br class="clear">
		<div id="login">
                <div class="row login">
					<h4>User Name (Email)</h4>
					<input type="email" name="employee_email" id="email"  onmouseover="document.getElementById('toolTipDiv').className='activeToolTip'" onmouseout="document.getElementById('toolTipDiv').className='idleToolTip'" />
				</div>
                <div class="relative">    
                	<div id="toolTipDiv" class="idleToolTip">Enter Valid Email Address</div>
				</div>
                <br class="clear" />
				<div class="row login">
					<h4>User Group</h4>
                    <?php echo form_dropdown('user_group',$user_group,'','id="user"','id="user"'); ?>
					<!--<select>
						<option>Select A Group</option>
						<option>User Group 1</option>
						<option>User Group 2</option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Password</h4>
					<input type="password" name="password" id="pass" />
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Confirm Password</h4>
					<input type="password" name="confrim_password" id="conf_pass" onblur="return valid()">
					<div id="warn"></div>
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Status</h4>
					<select name="status">
						<option value="1">Enable</option>
						<option value="0">Disable</option>
					</select>
				</div>
		</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" class="btn green" name="continue" value="Add" ">
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			

			<?php echo form_close(); ?>
		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
	//alert(msg);
	//setTimeout(function(){ msg.hide() }, 3000);
	$(document).ready(function(){
		$('#check').change(function(){
			if(this.checked){
				$('#login').fadeIn('slow');
				$("#email").attr("required","true");
				$("#user").attr("required","true");
			}

			else{
				$('#login').fadeOut('slow');
				$("#email").removeAttr("required");
				$("#user").removeAttr("required");
			}

		});
	});

	function valid() {
		if($("#email").val())
		{$("#warn").append("<p style='color: red'>plz enter email.....!</p>");}
		if ($('#pass').val() != $('#conf_pass').val()) {
			$("#warn").append("<p style='color: red'>Sorry Password Doesn't Match.....!</p>");
			return false;
		} else {
			$("#warn").hide();
			return true;
		}
	}
</script>