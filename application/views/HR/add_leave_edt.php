<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Leave</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />

				<div class="row">
					<h4>Leave type</h4>
					<?php echo @form_dropdown('ml_leave_type_id', $leave_type,'required="required"','id="drop_leave"'); ?>
					<a id="lev"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>No Of Days</h4>
					<input type="text" name="no_of_leaves">
				</div>

				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_leave" value="Add" class="btn green" />
					<a href="human_resource/edit_emp_entitlement_increment/<?php echo $employee->employee_id;?>"> <input type="button" value="Cancel" class="btn gray" /></a>
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Leave Type</td>
					<td>Allocated Leaves</td>
                    <td>Leaves Entitled</td>
					<td>Approved Date</td>
					<td>Approved By</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($leave_details)){
					foreach($leave_details as $leave){?>
				<tr class="table-row">
					<td><?php echo $leave->leave_type;?></td>
					<td><?php echo $leave->no_of_leaves_allocated;?></td>
					<td><?php echo $leave->no_of_leaves_entitled;?></td>
					<td><?php echo $leave->date_approved;?></td>
					<td><?php echo $leave->emp_name;?></td>
					<td><?php if($leave->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_single_leave_type/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<!--<td><a href="human_resource/edit_single_leave_type/<?php /*echo $employee->employee_id;*/?>/<?php /*echo $leave->leave_entitlement_id;*/?>"><span class="fa fa-pencil"></span></a></td>-->
					<td><a href="human_resource/send_2_trash_leave/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>" onclick="return confirm('Are You Sure..!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
		
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<div id="leave" title="Add Leave" style="display:none; width:600px;">
	<form id="leaveForm" action="human_resource/add_leave_type/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="leave_type" id="txt_leave"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
</script>
<script>
	$(document).ready(function()
	{
		$("#leave").on('click',function(e){
			e.preventDefault();
			var formData = $('#leaveForm').serialize();
			$.ajax({
				url: "human_resource/add_leave_type/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_leave').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_leave').append(appendData);
						$("#leave").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
	});
</script>