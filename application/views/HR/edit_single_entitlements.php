<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Increments</div>
		<?php echo form_open('human_resource/edit_emp_single_entitlement_increment/'.@$employee->employee_id.'/'.@$increment->increment_id); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        
        	<br class="clear">
            <div class="row increment-field">
					<h4>Increment Type</h4>
					<?php 
					$slctd_increment = (isset($increment->increment_type_id) ? $increment->increment_type_id : '');
					echo @form_dropdown('increment_type_id', $increment_type, $slctd_increment,'required="required"','required="required"'); ?>
				</div>

				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text" name="increment_amount" value="<?php echo @$increment->increment_amount; ?>">
				</div>
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="joining_date" value="<?php echo @$increment->date_effective; ?>">
				</div>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_increment" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
		<?php echo form_close(); ?>


				<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Increment Type</td>
					<td>Icrement</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if($increment_details){
					foreach($increment_details as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->increment_type;?></td>
					<td><?php echo $increment->increment_amount;?></td>
					<td><a href="human_resource/edit_emp_entitlement_increment/<?php echo @$employee->employee_id;?>/<?php echo @$increment->increment_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>


		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Allowances</div>
		<?php echo form_open('human_resource/edit_emp_single_entitlement_increment/'.@$employee->employee_id.'/'.@$allowance->allowance_id); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        
				<div class="row">
					<h4>Allowance type</h4>
					<?php
					$slctd_allowance = (isset($allowance->ml_allowance_type_id) ? $allowance->ml_allowance_type_id : '');
					echo @form_dropdown('ml_allowance_type_id', $allowance_type, $slctd_allowance); ?>
				</div>

				<div class="row">
					<h4>Amount</h4>
					<input type="text" name="allowance_amount" value="<?php echo @$allowance->allowance_amount; ?>">
				</div>
                
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_allowance" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($allowance_details)){
					foreach($allowance_details as $allowance){?>
				<tr class="table-row">
					<td><?php echo $allowance->allowance_type;?></td>
					<td><?php echo $allowance->allowance_amount;?></td>
					<td><a href="human_resource/edit_emp_entitlement_increment/<?php echo @$employee->employee_id;?>/<?php echo @$allowance->allowance_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>

		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Benefits</div>
		<?php echo form_open('human_resource/edit_emp_single_entitlement_increment/'.@$employee->employee_id.'/'.@$benefit->emp_benefit_id); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        
				<div class="row">
					<h4>Benefit type</h4>
					<?php 
					$slctd_benefits = (isset($benefit->benefit_type_id) ? $benefit->benefit_type_id : '');
					echo @form_dropdown('benefit_type_id', $benefit_type, $slctd_benefits); ?>
				</div>

				<div class="row">
					<h4>Benefit</h4>
					<?php 
					$slctd_benefit_list = (isset($benefit_list->benefit_item_id) ? $benefit_list->benefit_item_id : '');
					echo @form_dropdown('benefits_list_id', $benefit_list, $slctd_benefit_list); ?>
				</div>
<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="date_of_birth" value="<?php echo @$benefit->date_effective; ?>">
				</div>
                
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_benefit" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($benefit_details)){
					foreach($benefit_details as $benefit){?>
				<tr class="table-row">
					<td><?php echo $benefit->benefit_type_title;?></td>
					<td><?php echo $benefit->date_effective;?></td>
					<td><a href="human_resource/edit_emp_entitlement_increment/<?php echo @$employee->employee_id;?>/<?php echo @$benefit->emp_benefit_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Leave</div>
		<?php echo form_open('human_resource/edit_emp_single_entitlement_increment/'.@$employee->employee_id.'/'.@$leave->leave_entitlement_id); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />

				<div class="row">
					<h4>Leave type</h4>
					<?php 
					$slctd_leave = (isset($leave->ml_leave_type_id) ? $leave->ml_leave_type_id : '');
					echo @form_dropdown('ml_leave_type_id', $leave_type, $slctd_leave); ?>
				</div>

				<div class="row">
					<h4>No Of Days</h4>
					<input type="text" name="no_of_leaves" value="<?php echo @$leave->no_of_leaves; ?>">
				</div>

			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_leave" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Leave Type</td>
					<td>No Of Days</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($leave_details)){
					foreach($leave_details as $leave){?>
				<tr class="table-row">
					<td><?php echo $leave->leave_type;?></td>
					<td><?php echo $leave->no_of_leaves;?></td>
					<td><a href="human_resource/edit_emp_entitlement_increment/<?php echo @$employee->employee_id;?>/<?php echo @$leave->leave_entitlement_id;?>"><span class="fa fa-pencil"></span></a></td>					
					<td><a href="human_resource/send_2_trash_leave/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
		
	</div>
<!-- contents -->