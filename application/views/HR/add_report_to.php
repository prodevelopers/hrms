<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
    #autoSuggestionsList {
        width: 205px;
        overflow: hidden;
        border: 1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: none;
    }

    #autoSuggestionsList li {
        list-style: none;
        cursor: pointer;
        padding: 5px;
        font-size: 14px;
        font-family: Arial;
        margin: 2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #autoSuggestionsList li:hover {
        border: 1px solid #CCC;
    }

    .serch {
        width: 200px;
        height: 25px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: black;
        padding-left: 5px;
        border: 1px solid #999;
    }

    #autoSuggestionsList2 {
        width: 205px;
        overflow: hidden;
        border: 1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: none;
        z-index: 1000;
    }

    #autoSuggestionsList2 li {
        list-style: none;
        cursor: pointer;
        padding: 5px;
        font-size: 14px;
        font-family: Arial;
        margin: 2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        z-index: 1000;
    }

    #autoSuggestionsList2 li:hover {
        border: 1px solid #CCC;
    }

    .serch2 {
        width: 200px;
        height: 25px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: black;
        padding-left: 5px;
        border: 1px solid #999;
    }
    .select2TemplateImg { padding:0.2em 0; clear: both;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .select2-chosen div.select2TemplateImg img{ width: 24px; height: 24px;}
</style>
<script type="text/javascript">
    $(document).ready(function () {

        ////Selectors For Selecting The
        var supervisorSelector = $('#selectSupervisorEmployee');
        var url = "<?php echo base_url(); ?>human_resource/loadReportToEmployees/<?=$this->uri->segment(3)?>";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Line Manager";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(supervisorSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);

        var SubOrdinateSelector = $('#selectSubOrdinateEmployee');
        var url = "<?php echo base_url(); ?>human_resource/loadReportToEmployees/<?=$this->uri->segment(3)?>";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Sub Ordinate";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(SubOrdinateSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
        $('.select2-container').css("width","46%");




        $('.addSupervisorForm').submit(function (e) {
            var superVisorID = $('#selectSupervisorEmployee').val();
            var reportingMethod = $(this).find('.suggestionsList').val();

            //First We Will Check For The selected Employee, If the Employee is not selected, it will prevent the page to move forward.
            if(superVisorID.length == 0 || empName.length == 0 ){
                Parexons.notification('You Must Select The Supervisors','warning');
                return false;
            }
            if(typeof reportingMethod == 'undefined'){
                Parexons.notification('You Must Select The Reporting Method','warning');
                return false;
            }
            if(reportingMethod.length == 0){
                Parexons.notification('You Must Select The Reporting Method','warning');
                return false;
            }

        });

        $('.addSubForm').submit(function (e) {
            var empName = $(this).find('[name="emp_id"]').val();
            var subOrdinateID = $(this).find('#selectSubOrdinateEmployee').val();
            var reportingMethod = $(this).find('#subReportingMethod').val();

            //First We Will Check For The selected Employee, If the Employee is not selected, it will prevent the page to move forward.
            if(typeof subOrdinateID == 'undefined' || typeof empName == 'undefined'){
                Parexons.notification('You Must Select The Subordinates','warning');
                return false;
            }
            if(subOrdinateID.length == 0 || empName.length == 0 ){
                Parexons.notification('You Must Select The Subordinates','warning');
                return false;
            }
            if(typeof reportingMethod == 'undefined'){
                Parexons.notification('You Must Select The Reporting Method','warning');
                return false;
            }
            if(reportingMethod.length == 0){
                Parexons.notification('You Must Select The Reporting Method','warning');
                return false;
            }

        });
    });


</script>
<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Employees / Add Employee / Personal Info</div>
    <!-- bredcrumb -->

    <?php $this->load->view('includes/employee_left_nav'); ?>


    <div class="right-contents1">

     <div class="head">Add Report To</div>
     <div class="row"><div class="head">Add Supervisor</div></div>


        

        <?php
        $formAttributes = array(
            'id' => 'addSupervisorForm',
            'class' => 'addSupervisorForm'
        );
        echo form_open(base_url('human_resource/add_emp_report_to'), $formAttributes); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>"/>
        <br class="clear">

        <div class="row">
            <h4>Name</h4>
            <input type="hidden" id="selectSupervisorEmployee" name="supervisor_id">
        </div>

        <br class="clear">

        <div class="row">
            <h4>Reporting Method</h4>
            <?php echo @form_dropdown('supervisor_method', $report_method,'','class="suggestionsList"'); ?>
            <!--<select>
                <option></option>
            </select>-->
        </div>

        <br class="clear">

        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <input type="submit" name="add_supervisor" value="Add" class="btn green"/>
                <input type="reset" value="Reset" class="btn gray"/>
            </div>
        </div>
        <?php echo form_close(); ?>
        <table cellspacing="0">
            <thead class="table-head" style="background:#A3AAA3;">
            <td>Supervisor Name</td>
            <td>Reporting Method</td>
            <!--<td>Remarks</td>-->
            <td>Delete</td>
            <td>Trash</td>
            </thead>
            <?php if (!empty($record)) {
                foreach ($record as $row) {
                    ?>
                    <tr class="table-row">
                        <td><?php echo $row->full_name; ?></td>
                        <td><?php echo $row->reporting_option; ?></td>
                        <!--<td><?php //echo $row->comments;?></td>-->
                        <td>
                            <a href="human_resource/delete_complete/<?php echo $this->uri->segment(3); ?>/<?php echo $row->report_heirarchy_id; ?>" onclick="return confirm('Are You Sure..!')">Remove
                        </td>
                        <td>
                            <a href="human_resource/send_2_trash_report_to/<?php echo $employee->employee_id; ?>/<?php echo $row->report_heirarchy_id; ?>/<?php echo 'report_heirarchy_id'; ?>" onclick="return confirm('Are you Sure..!')"><span
                                    class="fa fa-trash-o"></span></a></td>
                    </tr>
                <?php }
            } else {
            } ?>
        </table>
        <br class="clear">

        <?php
        $formAttributes = array(
            'id' => 'addSupervisorForm',
            'class' => 'addSubForm'
        );
        echo form_open('', $formAttributes); ?>
        <input type="hidden" name="emp_id" value="<?php echo isset($employee->employee_id) ? $employee->employee_id: ''; ?>"/>

        <div class="head">Add Subordinates</div>


        <br class="clear">

        <div class="row">
            <h4>Name</h4>
            <input type="hidden" name="SubOrdinateEmployeeID" id="selectSubOrdinateEmployee">
        </div>


        <br class="clear">

        <div class="row">
            <h4>Reporting Method</h4>
            <?php echo @form_dropdown('subordinate_method', $report_method,'','class="suggestionsList" id="subReportingMethod"'); ?>
        </div>

        <br class="clear">

        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <input type="submit" name="add_sub_ordinate" value="Add" class="btn green"/>
                <input type="reset" value="Reset" class="btn gray"/>
            </div>
        </div>
        <?php echo form_close(); ?>
        <table cellspacing="0">
            <thead class="table-head" style="background:#A3AAA3;">
            <td>Subordinate Name</td>
            <td>Reporting Method</td>
            <!--<td>Remarks</td>-->
            <td>Delete</td>
            <td>Trash</td>
            </thead>
            <?php if (!empty($reports)) {
                foreach (@$reports as $r) {
                    ?>
                    <tr class="table-row">
                        <td><?php echo $r->full_name; ?></td>
                        <td><?php echo $r->reporting_option; ?></td>
                        <!--<td><?php //echo $row->comments;?></td>-->
                        <td>
                            <a href="human_resource/delete_complete/<?php echo $employee->employee_id; ?>/<?php echo $r->report_heirarchy_id; ?>" onclick="return confirm('Are you Sure...!')">Remove
                        </td>
                        <td>
                            <a href="human_resource/send_2_trash_report_to/<?php echo $employee->employee_id; ?>/<?php echo $r->report_heirarchy_id; ?>/<?php echo 'report_heirarchy_id'; ?>" onclick="return confirm('Are you Sure...!')"><span
                                    class="fa fa-trash-o"></span></a></td>
                    </tr>
                <?php }
            } else {
            } ?>
        </table>
    </div>

</div>
<!-- contents -->
<script>
    $("#txt").delay(3000).fadeOut('slow');
    $("#sms").delay(3000).fadeOut('slow');

    $(document).ready(function(e){
        $('#leavetype').select2();

        //Page Notifications If Any..
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>