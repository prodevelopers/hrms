<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<?php /*$this->load->view('includes/hr_left_nav'); */?>
	
		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Allowances</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
				<div class="row">
					<h4>Allowance type</h4>
					<?php echo @form_dropdown('ml_allowance_type_id', $allowance_type,'required="required"','required="required"'); ?>
				</div>
				<div class="row">
					<h4>Pay Frequency</h4>
					<?php echo @form_dropdown('pay_frequency', $pay_frequency); ?>
				</div>
				<div class="row">
					<h4>Amount</h4>
					<input type="text" name="allowance_amount">
				</div>
                <div class="row">
					<h4>Date Effective From</h4>
					<input type="text" name="effective_date" id="effective_date">
				</div>
                
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_allowance" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Pay Frequency</td>
					<td>Ammount</td>
					<td>Date Effective From</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($allowance_details)){
					foreach($allowance_details as $allowance){?>
				<tr class="table-row">
					<td><?php echo $allowance->allowance_type;?></td>
					<td><?php echo $allowance->pay_frequency;?></td>
					<td><?php echo $allowance->allowance_amount;?></td>
					<td><?php echo date_format_helper($allowance->effective_date);?></td>
					<td><a href="human_resource/edit_emp_single_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
	
	</div>
<!-- contents -->
<script>
    $("#alert").delay(3000).fadeOut('slow');
    $( "#effective_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
        /*onClose: function( selectedDate ) {
         $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
         }*/

    });
</script>

<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->