<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.resize{
	width: 220px;
}
</style>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
<!-- Right Side Historic Detail -->
		<div class="head">Employee Info</div>
    	<div class="form-right">
        <div class="row2">
        <h5 class="headingfive">Employee Name</h5>
        <i class="italica" ><?php echo @$kpi_emp_h->full_name;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Employee ID</h5>
        <i class="italica"><?php echo @$kpi_emp_h->employee_code;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Designation</h5>
        <i class="italica"><?php echo @$kpi_emp_h->designation_name;?></i>
        </div>
	</div>
            <!-- Right Side Historic Detail -->
            <br class="clear" />
		<div class="head">Annual Reviews</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<?php 
				echo form_open(); 
					
					echo @form_dropdown('year', $year, $slctd_year,"class='resize' id='year' onChange='this.form.submit()'");

					//echo @form_dropdown('month', $month, $slctd_month, "class='resize' id='month' onChange='this.form.submit()'"); ?>
				
				<?php echo form_close(); ?>
			</div>
            <br class="clear" />


			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>Review Year</td>
					<td>Reviewer Name</td>
					<!--<td>Reviewer Comments</td>
					<td>Employee Feedback</td>-->
					<td>View Detail</td>
					<!--<td>Evaluation</td>
                    <td>Employee Progress Report</td>-->
				</thead>
                <?php if(!empty($kpi_eval)){
					foreach($kpi_eval as $kpi){ ?>
                <tr class="table-row">
                	<td><?php $date = $kpi->submit_date; $year=date('Y',strtotime($date)); echo $year;?></td>
                	<td><?php echo $kpi->full_name; ?></td>
                	<td><a href="human_resource/annual_history/<?php echo $kpi->employer; ?>"><span class="fa fa-eye"></span></a></td>
                    <?php /*?><td><a href="human_resource/add_performance_evalution/<?php echo $kpi->employee_id ?>/<?php echo $kpi->job_assignment_id; ?>"><button class="btn green">Evaluate</button></a></td><?php */?>
                   
                </tr>
                <?php } }else{echo "<td colspan='6'><span style='color:red'>No Record Found ....</span></td>";} ?>
                
                
			</table>
            

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#year').select2();
    });

    </script>
    <!-- leftside menu end -->