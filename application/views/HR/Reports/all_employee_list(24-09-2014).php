<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerParams": function (aoData, fnCallBack){
			//aoData.push({"name":"progress","value":$('#progress').val()});
			aoData.push({"name":"type","value":$('#grant').val()});
			aoData.push({"name":"region","value":$('#region').val()});
			aoData.push({"name":"district","value":$('#district').val()});
			aoData.push({"name":"sector","value":$('#sector').val()});
			aoData.push({"name":"quarter","value":$('#quarter').val()});
		}
	});
	
	
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_left_nav'); ?>

	<div class="right-contents">

		<div class="head">Personnel List</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Job Title</option>
				</select>
				<select>
					<option>Designation</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>

			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list">
				<thead class="table-head">
                <tr>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td width="140"><span class="fa fa-pencil"></span> &nbsp; &nbsp; | &nbsp; &nbsp;  <span class="fa fa-eye"></span>&nbsp; &nbsp; |  &nbsp; &nbsp; <span class="fa fa-print"></span></td>
                </tr>
                </thead>
                <tbody>
                </tbody>
				
			</table>
<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_employee_acct/" ><input type="button" name="continue" value="Add" class="btn green" /></a>
				</div>
			</div>

		</div>

	</div>
<!-- contents -->