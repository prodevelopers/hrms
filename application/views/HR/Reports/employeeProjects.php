<style>
    .EmployeeProjectsTable > thead.table-head > tr > th {
        text-align: left;
    }
    .ui-datepicker-calendar {
        display: none;
    }
    .actionColumn{
        display:none;
    }
    .activeBorder{
        border-bottom: 2px solid #0c98c1;
    }
</style>
<!-- contents -->
<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Manage Projects</div> <!-- bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
<div class="right-contents">

    <div class="head">Manage Employee Projects <!--<span style="float: right;"><button id="queuedProjectsButton" class="btn red" style="display: block;">Queued Projects</button><button id="currentProjectsButton" class="btn green" style="display: block; display: none;"> Current Projects</button></span>--></div>
    <div class="notice info">
        <p>Read Before Allocation of Projects</p>
        <ul>
            <li>To assign new projects, Respective dates must be specified.</li>
            <li>New project can only be assigned for start of Current Month or start of Next Month</li>
            <li>Already assigned projects can only be revoked for next month</li>
            <li>For currently assigned projects, Projects Percentage Charges must all SUM up to <strong>100%</strong></li>
            <li>This Page Will Only Display Currently Assigned Projects, To View Previously Assigned Projects Please Check Out <strong>Employees Wise Project Report</strong></li>
        </ul>
    </div>
    <!-- button group -->
    <div style="margin-left: 15px;">
        <button class="btn" style="float: none;" id="selectCurrentMonthBtn"><?=$currentMonth?></button>
        <button class="btn" style="float: none;" id="selectNextMonthBtn"><?=$nextMonth?></button>
    </div>
    <div class="form-left">
        <input type="hidden" value="currentMonth" id="selectedMonth">
        <? //Table For Current Month ?>
        <table cellspacing="0" id="currentMonthTable" class="EmployeeProjectsTable">
            <thead class="table-head">
            <tr>
                <th>SelectBox</th>
                <th>Project Title</th>
                <th>Project (%)Charge</th>
                <th>Project Assign Date</th>
                <th>Project Start Date</th>
                <th class="actionColumn">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($employeeProjectsInCurrentMonth) && !empty($employeeProjectsInCurrentMonth) && is_array($employeeProjectsInCurrentMonth)){
                foreach($employeeProjectsInCurrentMonth as $projectKey=>$projectValue){
                    echo '<tr class = "table-row" data-project="'.$projectValue->AllProjectIDs.'"><td><input type="checkbox" disabled="disabled" class="employeeProjectCheckBox"';
                    if(isset($projectValue->ProjectID) and !empty($projectValue->ProjectID)){
                        echo "checked";
                        $assignedStatus = true;
                    }else{
                        $assignedStatus = false;
                    }
                    echo '></td><td>'.$projectValue->ProjectTitle.'</td><td>'.$projectValue->EmployeePercentCharged.'</td><td>'.((isset($projectValue) && !empty($projectValue->ProjectAssignedDate))?date('d-m-Y',strtotime($projectValue->ProjectAssignedDate)):" - ").'</td><td>'.((isset($projectValue) && !empty($projectValue->ProjectStartDate))?date('d-m-Y',strtotime($projectValue->ProjectStartDate)):'').'</td><td class="actionColumn">'.((isset($assignedStatus) && $assignedStatus === true)?"Assigned":"").'</td></tr>';
                }
            }
            ?>
            </tbody>
        </table>

        <? //Table For Next Month ?>
        <table cellspacing="0" id="nextMonthTable" class="EmployeeProjectsTable" style="display: none;">
            <thead class="table-head">
            <tr>
                <th>SelectBox</th>
                <th>Project Title</th>
                <th>Project (%)Charge</th>
                <th>Project Assign Date</th>
                <th>Project Start Date</th>
                <th class="actionColumn">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($employeeProjects) && !empty($employeeProjects) && is_array($employeeProjects)){
                foreach($employeeProjects as $projectKey=>$projectValue){
                    echo '<tr class = "table-row" data-project="'.$projectValue->AllProjectIDs.'"><td><input type="checkbox" disabled="disabled" class="employeeProjectCheckBox"';
                    if(isset($projectValue->ProjectID) and !empty($projectValue->ProjectID)){
                        echo "checked";
                        $assignedStatus = true;
                    }else{
                        $assignedStatus = false;
                    }
                    echo '></td><td>'.$projectValue->ProjectTitle.'</td><td>'.$projectValue->EmployeePercentCharged.'</td><td>'.((isset($projectValue) && !empty($projectValue->ProjectAssignedDate))?date('d-m-Y',strtotime($projectValue->ProjectAssignedDate)):" - ").'</td><td>'.((isset($projectValue) && !empty($projectValue->ProjectStartDate))?date('d-m-Y',strtotime($projectValue->ProjectStartDate)):'').'</td><td class="actionColumn">'.((isset($assignedStatus) && $assignedStatus === true)?"Assigned":"").'</td></tr>';
                }
            }
            ?>
            </tbody>
        </table>

        <div class="button-group">
            <button id="assignRevokeBtn" class="btn green">Assign/Revoke Projects</button>
            <button id="cancelProjectAllocation" class="btn red">Cancel</button>
            <button id="EditModeBtn" class="btn green">Edit/Update Mode</button>
        </div>
    </div>

    <div class="form-right">
        <table cellspacing="0" class="table-approved" style="margin-top: 0.7em; width: 100%">
            <thead class="table-head">
            <td colspan="4">Employee Information</td>
            </thead>
            <tr class="table-row">
                <th>Employee Code</th>
                <td><?php echo isset($employeeData)?$employeeData->EmployeeCode:'' ?></td>
            </tr>
            <tr class="table-row">
                <th>Employee Name</th>
                <td><?php echo isset($employeeData)?$employeeData->EmployeeName:'' ?></td>
            </tr>
            <tr class="table-row">
                <th>Designation</th>
                <td><?php echo isset($employeeData)?$employeeData->Designation:'' ?></td>
            </tr>
            <tr class="table-row">
                <th>Department</th>
                <td><?php echo isset($employeeData)?$employeeData->Department:'' ?></td>
            </tr>
        </table>
    </div>
</div>
</div>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    var $MaxPercentage;
    var chargePool;
    var chargePoolNextMonth;
    $(document).ready(function () {

        //Getting Button Selectors
        $MaxPercentage = 100;
        chargePool = 0;
        chargePoolNextMonth = 0;
        var pSum = 0;
        var nextMonthPercentSum = 0;

        $('#currentMonthTable .percentInput').each(function(e){
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                pSum += parseFloat(this.value);
            }
        });

        $('#nextMonthTable .percentInput').each(function(e){
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                nextMonthPercentSum += parseFloat(this.value);
            }
        });


        //Initializing Some Stuff In Start For Later Use.
        chargePool = $MaxPercentage - pSum;
        chargePoolNextMonth = $MaxPercentage - nextMonthPercentSum;
        var employeeID = <?php echo isset($employeeData)?$employeeData->EmployeeID:'' ?>;
        var assignRevokeBtn = $('#assignRevokeBtn');
        var cancelProjectAllocationBtn = $('#cancelProjectAllocation');
        var EditUpdateBtn = $('#EditModeBtn');
        assignRevokeBtn.hide();
        cancelProjectAllocationBtn.hide();
        $('.employeeProjectCheckBox').attr('disabled','disabled');

        //New Update
        var currentMonthTable = $('#currentMonthTable');
        var nextMonthTable = $('#nextMonthTable');
        //Hiding Next Month Table On Page Load.
        nextMonthTable.hide();

        //Doing Stuff When Respective Buttons Are Pressed..
        EditUpdateBtn.on('click',function(e){
            //On Click of This Specific Button We Need To Create Environment for User to Edit/Update Project Allocations
            assignRevokeBtn.show();
            cancelProjectAllocationBtn.show();
            EditUpdateBtn.hide();
            //Need To convert Some Things In Editable Mode.
                $('.EmployeeProjectsTable tbody tr td:nth-child(3)').each(function () {
                    var html = $(this).html();
                    var strippedToNumericOnly = html.replace(/\D/g,'');
                    var input;
                    if($(this).closest('tr').find('.employeeProjectCheckBox').is(':checked')){
                        input = $('<input class="percentInput" type="text" />');
                    }else{
                        input = $('<input class="percentInput" type="text" disabled="disabled" />');
                    }

                    input.val(strippedToNumericOnly);
                    $(this).html(input);
                    $(this).append('%');
                });
            $('.employeeProjectCheckBox').removeAttr('disabled');
            var rowForCurrentMonthPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePool+'%</td></tr>';
            var rowForNextMonthPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePoolNextMonth+'%</td></tr>';
            $('#currentMonthTable tbody').append(rowForCurrentMonthPool);
            $('#nextMonthTable tbody').append(rowForNextMonthPool);
            $('.actionColumn').css('display','block');
        });
        cancelProjectAllocationBtn.on('click',function(e){
            assignRevokeBtn.hide();
            cancelProjectAllocationBtn.hide();
            EditUpdateBtn.show();
            //Convert Fields Back To Non Editable Mode
            $('.EmployeeProjectsTable tbody tr td:nth-child(3)').each(function () {
                var inputValue = $(this).find('input[type="text"]').val();
                var strippedToNumericOnly = inputValue.replace(/\D/g,'');
                $(this).html(strippedToNumericOnly+'%');
            });
            $('.employeeProjectCheckBox').attr('disabled','disabled');
            $('#currentMonthTable tbody tr:last, #nextMonthTable tbody tr:last').remove();
            $('.actionColumn').css('display','none');
        });


        //The Complicated Calculation Section :)
        //Complicated Calculation Section for Current Month
        $('#currentMonthTable').on('keyup','.percentInput',function(e){
            var percentChargedInput = $(this).val();
            if(percentChargedInput.length === 0){
                percentChargedInput = '0';
            }
            var SUM = 0;
            var nextMonthSUM = 0;
            $(this).parents('table').find('.percentInput').each(function(e){
                //Getting Only Values Of Checked Boxes..
                var checkBoxStatus = $(this).closest('tr').find('.employeeProjectCheckBox');
                if(checkBoxStatus.is(':checked')){
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        SUM += parseFloat(this.value);
                    }
                }
            });
            if(SUM > 100){
                Parexons.notification('Total Percent Pool For <b>Current Month</b> Can Not Go Beyond <b>100%</b>','warning');
                $(this).val(0);
                chargePool = $MaxPercentage - SUM;
                var inputValue = percentChargedInput.replace(/\D/g,'');
                chargePool = chargePool + parseInt(inputValue);
            }else{
                chargePool = $MaxPercentage - SUM;
                var inputValue = percentChargedInput.replace(/\D/g,'');
                $(this).val(parseInt(inputValue));
            }
            chargePool =  parseInt(chargePool);
            //Taking Out The Last Row
            $('#currentMonthTable tbody tr:last').remove();
            //Putting The Last Row Back In.
            var rowForPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePool+'%</td></tr>';
            $('#currentMonthTable tbody').append(rowForPool);
        });

        //Complicated Calculation Section For Next Month
        $('#nextMonthTable').on('keyup','.percentInput',function(e){
            var percentChargedInput = $(this).val();
            if(percentChargedInput.length === 0){
                percentChargedInput = '0';
            }
            var SUM = 0;
            $(this).parents('table').find('.percentInput').each(function(e){
                //Getting Only Values Of Checked Boxes..
                var checkBoxStatus = $(this).closest('tr').find('.employeeProjectCheckBox');
                if(checkBoxStatus.is(':checked')){
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        SUM += parseFloat(this.value);
                    }
                }
            });
            if(SUM > 100){
                Parexons.notification('Total Percent Pool For <b>Next Month</b> Can Not Go Beyond <b>100%</b>','warning');
                $(this).val(0);
                chargePoolNextMonth = $MaxPercentage - SUM;
                var inputValue = percentChargedInput.replace(/\D/g,'');
                chargePoolNextMonth = chargePoolNextMonth + parseInt(inputValue);
//                console.log(inputValue);
            }else{
                chargePoolNextMonth = $MaxPercentage - SUM;
                var inputValue = percentChargedInput.replace(/\D/g,'');
                $(this).val(parseInt(inputValue));
            }
            chargePoolNextMonth=  parseInt(chargePoolNextMonth);
            //Taking Out The Last Row
            $('#nextMonthTable tbody tr:last').remove();
            //Putting The Last Row Back In.
            var rowForPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePoolNextMonth+'%</td></tr>';
            $('#nextMonthTable tbody').append(rowForPool);
        });


        //Now Finally The Most Important Button For Which We Did Put 2 Extra Buttons. Assigning And Revoking of Projects Button.
      assignRevokeBtn.on('click',function(e){
          /**
           * @type {*|jQuery|HTMLElement}
           * Little Way Changed Here. Now As We Have Two Buttons/Tabs For Two Different Months.
           * We Need To Check The Both Current Month Percentage And Next Month Percentage.
           */

        //First Lets Work For The Current Months

          var currentMonthTableCheckBoxes = $('#currentMonthTable .employeeProjectCheckBox:checked');
          var currentMonthTableCheckBoxesChecked = [];
          currentMonthTableCheckBoxes.each(function(e){
              var rowPercentValue = $(this).closest('tr').find('.percentInput').val();
              var projectID = $(this).closest('tr').attr('data-project');
              $data = {
                  proID: projectID,
                  percentCharge: rowPercentValue
              };
              currentMonthTableCheckBoxesChecked.push($data);
          });



          //Now Lets Work For Next Month Data..

          var nextMonthTableCheckBoxes = $('#nextMonthTable .employeeProjectCheckBox:checked');
          var nextMonthTableCheckBoxesChecked = [];
          nextMonthTableCheckBoxes.each(function(e){
              var rowPercentValue = $(this).closest('tr').find('.percentInput').val();
              var projectID = $(this).closest('tr').attr('data-project');
              $data = {
                  proID: projectID,
                  percentCharge: rowPercentValue
              };
              nextMonthTableCheckBoxesChecked.push($data);
          });


            console.log(chargePoolNextMonth);
          //Now Finally The Place Where We Need To Post Data Using Jquery Ajax

          if(currentMonthTableCheckBoxesChecked.length > 0){
              //if checkboxes are checked then this portion should execute;
              if(chargePool === 0 && chargePoolNextMonth === 0){
              $.ajax({
                  url:"<?php echo base_url(); ?>human_resource/update_employee_projects",
                  data:{currentMonthJsonData:JSON.stringify(currentMonthTableCheckBoxesChecked),
                  nextMonthJsonData:JSON.stringify(nextMonthTableCheckBoxesChecked),
                  empID:employeeID},
                  type: "POST",
                  success:function(output){
                    var data = output.split('::');
                      if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                      }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                      }
                  }
              });
              }else if(chargePool !== 0){
                  Parexons.notification('To Update Projects You Must Allocate <b>100%</b> fully to Current Month projects, So That Available Percentage Pool Must Be <b>0%</b>','error');
                  return;
              }else if(chargePoolNextMonth !== 0){
                  Parexons.notification('To Update Projects You Must Allocate <b>100%</b> fully to Next Month projects, So That Available Percentage Pool Must Be <b>0%</b>','error');
                  return;
              }
          } else {
              Parexons.notification('No CheckBox Has Been Checked, Please Assign At Least <b>1</b> Project', 'warning');
              return;
          }



      });

        //Do Some Action If CheckBox Value is Changed Respective to Check Status
        $('.employeeProjectCheckBox').on('change',function(e){
            var selectedTD = $(this).closest('tr').find('td:nth-child(3)');
            var selectedDateTD = $(this).closest('tr').find('td:nth-child(4)'); //Extra Date Field Added To Show Or Assign Projects To Employee At Particular Date
            var selectedRowProjectID = $(this).parents('tr').attr('data-project');
            var selectedTableID = $(this).parents('table').prop('id');

            //Now Lets Define Total Statuses for the projects
            //Assigned = Project That is Currently Assigned In Database
            //To Assign = Project That is Not Currently Assigned In Database but Has Been Marked To Assign
            //To Revoke = Project That is Currently Assigned In The Database To User, But Has Been UnMarked To Revoke From Selected User
            var currentlyAssigned = 'Assigned',
                toAssign = 'To Assign',
                toRevoke = 'To Revoke',
                notAssigned = '';
            var currentStatusCurrentMonth = $(this).parents('tr').find('.actionColumn').text();
            var currentStatusNextMonth = $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.actionColumn').text();

            if($(this).is(':checked')){
                selectedTD.find('.percentInput').removeAttr('disabled');

                //Lets First Work For Current Month
                if(currentStatusCurrentMonth === currentlyAssigned){
                    //nothing To Do As Its Already Been Currently Assigned
                }else if(currentStatusCurrentMonth === toRevoke){
                    $(this).parents('tr').find('.actionColumn').text(currentlyAssigned);
                }else if(currentStatusCurrentMonth === notAssigned){
                    $(this).parents('tr').find('.actionColumn').html('<span style="color:#008000;">'+toAssign+'</span>');
                }//End Of Current Month If/Else Statements

                if(selectedTableID === 'currentMonthTable'){
                    //If Checkbox Selected In Current Month Table Then Need To Perform Some Steps.
                    $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.employeeProjectCheckBox').prop( "checked", true );
                    $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('td:nth-child(3)').find('.percentInput').removeAttr('disabled');

                    //Now Lets Work For Next Month If Selected Table is For Current Month
                    if(currentStatusCurrentMonth === currentlyAssigned){
                        //nothing To Do As Its Already Been Currently Assigned
                    }else if(currentStatusCurrentMonth === toRevoke){
                        $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.actionColumn').text(currentlyAssigned);
                    }else if(currentStatusCurrentMonth === notAssigned){
                        $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.actionColumn').html('<span style="color:#008000;">'+toAssign+'</span>');
                    }//End Of Current Month If/Else Statements

                }
            }else{
                selectedTD.find('.percentInput').attr('disabled','disabled');

                //Lets First Work For Current Month
                if(currentStatusCurrentMonth === currentlyAssigned){
                    $(this).parents('tr').find('.actionColumn').html('<span style="color:red;">'+toRevoke+'</span>');
                }else if(currentStatusCurrentMonth === toAssign){
                    $(this).parents('tr').find('.actionColumn').text(notAssigned);
                }else if(currentStatusCurrentMonth === notAssigned){
                    //Nothing To Perform. Its Already Not Been Assigned IN First Place.
                }//End Of Current Month If/Else Statements

                if(selectedTableID === 'currentMonthTable'){
                    //If Checkbox Selected In Current Month Table Then Need To Perform Some Steps.
                    $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.employeeProjectCheckBox').prop( "checked", false );
                    $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('td:nth-child(3)').find('.percentInput').attr('disabled','disabled');

                    //Now Lets Work For Next Month If Selected Table is For Current Month
                    if(currentStatusCurrentMonth === currentlyAssigned){
                        $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.actionColumn').html('<span style="color:red;">'+toRevoke+'</span>');
                    }else if(currentStatusCurrentMonth === toAssign){
                        $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.actionColumn').text(notAssigned);
                    }else if(currentStatusCurrentMonth === notAssigned){
                        //Nothing To Perform. Its Already Not Been Assigned IN First Place.
                    }//End Of Current Month If/Else Statements
                }
            }

            //Recalculate The Pool For Current Month
            var SUM = 0;
            $(this).parents('table').find('.percentInput').each(function(e){
                //Getting Only Values Of Checked Boxes..
                var checkBoxStatus = $(this).closest('tr').find('.employeeProjectCheckBox');
                if(checkBoxStatus.is(':checked')){
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        SUM += parseFloat(this.value);
                    }
                }
            });
            if(SUM > 100){
                Parexons.notification('Total Percent Pool Can Not Go Beyond <b>100%</b>','warning');
                var currentInputBoxValue = selectedTD.find('.percentInput').val();
                selectedTD.find('.percentInput').val(0);
                if(selectedTableID === 'currentMonthTable'){
                    chargePool = $MaxPercentage - SUM;
                    var inputValue = currentInputBoxValue.replace(/\D/g,'');
                    chargePool = chargePool + parseInt(inputValue);
                    console.log(inputValue);
                }else{
                    chargePoolNextMonth = $MaxPercentage - SUM;
                    var inputValue = currentInputBoxValue.replace(/\D/g,'');
                    chargePoolNextMonth = chargePoolNextMonth + parseInt(inputValue);
                }

            }else{
                chargePool = $MaxPercentage - SUM;
            }
            chargePool =  parseInt(chargePool);

            //Taking Out The Last Row
            $(this).parents('table').find('tbody tr:last').remove();
            //Putting The Last Row Back In.
            var rowForPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePool+'%</td></tr>';
            $(this).parents('table').find('tbody').append(rowForPool);


            ////Now Do it For Next Month If Current Month Tab Checkbox value changed.
            if(selectedTableID === 'currentMonthTable'){
                //Recalculate The Pool For Next Month
                var NextMonthSUM = 0;
                var NextMonthProjectPercentValue = $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('.percentInput').val();
                $('#nextMonthTable').find('.percentInput').each(function(e){
                    //Getting Only Values Of Checked Boxes..
                    var checkBoxStatus = $(this).parents('tr').find('.employeeProjectCheckBox');
                    if(checkBoxStatus.is(':checked')){
                        //add only if the value is number
                        if(!isNaN(this.value) && this.value.length!=0) {
                            NextMonthSUM += parseFloat(this.value);
                        }
                    }
                });



                if(NextMonthSUM > 100){
                    Parexons.notification('Total Percent Pool Can Not Go Beyond <b>100%</b>','warning');
                    var currentInputBoxValue = $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('td:nth-child(3)').find('.percentInput').val();
                    $('#nextMonthTable tbody').find('tr[data-project="'+selectedRowProjectID+'"]').find('td:nth-child(3)').find('.percentInput').val(0);
                    chargePoolNextMonth = $MaxPercentage - NextMonthSUM;
                    var inputValue = currentInputBoxValue.replace(/\D/g,'');
                    chargePoolNextMonth = chargePoolNextMonth + parseInt(inputValue);
//                    console.log(inputValue);
                }else{
                    chargePoolNextMonth = $MaxPercentage - NextMonthSUM;
                }
                chargePoolNextMonth =  parseInt(chargePoolNextMonth);


                //Taking Out The Last Row
                $('#nextMonthTable tbody tr:last').remove();
                //Putting The Last Row Back In.
                var rowForPool = '<tr style="text-align:left" class="table-row"><td colspan="2">Available Percentage In Pool</td><td class = "percentPrizePool" colspan="2" style="font-weight:bold;">'+chargePoolNextMonth+'%</td></tr>';
                $('#nextMonthTable tbody').append(rowForPool);

            }

        });



        //On Page Load Check Which Table is Visible Show Active with Button Accordingly..
        if($('#nextMonthTable').is(':visible')){
            $('#selectNextMonthBtn').addClass('activeBorder');
        }else if($('#currentMonthTable').is(':visible')){
            $('#selectCurrentMonthBtn').addClass('activeBorder');
        }


        //Updated Project Allocation Work
        //Here We Will Gonna Use The Buttons Technique.. Two Buttons , One Should Be For Current Month, And The Other For Next Month
        $('#selectCurrentMonthBtn').on('click',function(e){
            nextMonthTable.hide();
            currentMonthTable.show();
            $('#selectedMonth').val('currentMonth');
            //Active Class For Buttons
            $('#selectNextMonthBtn').removeClass('activeBorder');
            $(this).addClass('activeBorder');
        });
        $('#selectNextMonthBtn').on('click',function(e){
            currentMonthTable.hide();
            nextMonthTable.show();
            $('#selectedMonth').val('nextMonth');
            //Active Class For Buttons
            $('#selectCurrentMonthBtn').removeClass('activeBorder');
            $(this).addClass('activeBorder');
        });

        });//End Of Document Ready Function

    function ifExist(element){
        if($(element).length){
            return true;
        }else{
            return false;
        }
    }

    //We Need To Show Only Month..
function customRange(input){
    var currentTime = new Date();
    var month = currentTime.getMonth();
    var year = currentTime.getFullYear();
    var dateMin = new Date(year, month, 1);
    var dateMax = new Date(year, month + 1, 1);
    return {
        minDate: dateMax,
        maxDate: dateMax
    };
}
</script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- left side menu end -->