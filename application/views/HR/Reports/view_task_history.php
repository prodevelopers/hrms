<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Task Management</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">

		<div class="head">Task's History</div>

			<!-- filter -->
			<?php /*?><div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
                <?php //echo @form_dropdown('designation', $designation, $slct_design, "class='resize' id='title' onChange='this.form.submit()'"); ?>
				
                <?php echo @form_dropdown('task', $task_name, $slct_task, "class='resize' id='task' onChange='this.form.submit()'"); ?>
				
				<?php echo form_close();?>
            </div><?php */?>

<div class="form-right">
                    
                            <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$job_history->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Employee ID</h5>
                                <i class="italica"><?php echo @$job_history->employee_code;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$job_history->designation_name;?></i>
                        </div>
            </div>
			<!-- table -->
			<table cellpadding="0" cellspacing="0">
				<thead class="table-head">
					<!--<td>Employee ID </td>
					<td>Employee Name</td>
					<td>Designation</td>-->
					<td>Assigned Task</td>
					<td>Project Name</td>
					<td>Start Date</td>
					<td>Completion Date</td>
                    <td>Task Description</td>
					<td>Status</td>
					<!--<td>Approval Date</td>
					<td>Status</td>-->
					<!--<td><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>-->
				</thead>
                <?php if(!empty($task_history)){
					foreach($task_history as $task){?>
                <tr class="table-row">
                	<?php /*?><td><?php echo $task->employee_code;?></td>
                	<td><?php echo $task->full_name;?></td>
                	<td><?php echo $task->designation_name;?></td><?php */?>
                	<td><?php echo $task->task_name;?></td>
                	<td><?php echo $task->project_title;?></td>
                	<td><?php echo $task->start_date;?></td>
                	<td><?php echo date_format_helper($task->completion_date);?></td>
                	<td><?php echo $task->task_description?></td>
                    <td><?php echo $task->status;?></td>
                	<!--<td><?php //if(!empty($task->app_emp_name)){echo $task->app_emp_name;}else{echo "Not Approved";}?></td>
                	<td><?php //echo $task->status;?></td>
                	<td><?php //echo $task->status_title;?></td>-->
                </tr>
				<?php } }else{ ?>
                <tr><td colspan="5"><?php echo "No Record Found ... !";?></td></tr>
                <?php } ?>
			</table>
			<div id="container">
        <ul>
        <?php //echo $links;?>
        </ul>
        </div>
			<!-- button group -->
			<!--<div class="row">
				<div class="button-group">
					<a href="assign_job.php"><button class="btn green">Assign Task</button></a>
					 <button class="btn red">Delete</button> 
				</div>
			</div>-->

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->