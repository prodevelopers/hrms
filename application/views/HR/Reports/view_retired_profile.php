<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
<script type="text/javascript">
/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");		
	}
</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> 

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Complete Details <span class="fa fa-print" style="float:right; padding-right:40px;padding-top:5px; color:#fff; cursor:pointer" onclick="return print_report('<?php echo @$employee_id ?>')"></span></div>

			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="5">Personal Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td rowspan="4" width="150"><img src="upload/Thumb_Nails/<?php echo @$personal_info->thumbnail; ?>" width="150" height="150"></td>
					<td>Employee Name</td>
					<td><?php echo @$personal_info->full_name; ?></td>
					<td>Employee ID</td>
					<td><?php echo @$personal_info->employee_code; ?></td>
				</tr>
				<tr class="table-row">
					<td>Father Name</td>
					<td><?php echo @$personal_info->father_name; ?></td>
					<td>CNIC</td>
					<td><?php echo @$personal_info->CNIC; ?></td>
				</tr>
				<tr class="table-row">
					<td>Martial Status</td>
					<td><?php echo @$gender_etc->marital_status_title; ?></td>
					<td>Date Of Birth</td>
					<td><?php echo date_format_helper($personal_info->date_of_birth); ?></td>
				</tr>	
				<tr class="table-row">
					<td>Nationality</td>
					<td><?php echo @$gender_etc->nationality; ?></td>
					<td>Religion</td>
					<td><?php echo @$gender_etc->religion_title; ?></td>
				</tr>		
				</tbody>	
			</table>
			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="4">Contact Information (Current)</td>
				</thead>
				<tbody>

				<tr class="table-row">
					<td>Current address</td>
					<td><?php echo @$cont->address; ?></td>
					<td>Telephone</td>
					<td><?php echo @$cont->home_phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php echo @$cont->mob_num; ?></td>
					<td>Email</td>
					<td><?php echo @$cont->email_address; ?></td>
				</tr>
				<tr class="table-row">
					<td>Work Telephone</td>
					<td><?php echo @$cont->office_phone; ?></td>
					<td>Work Email</td>
					<td><?php echo @$cont->official_email; ?></td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Permanent)</td>
				</thead>
				<tr class="table-row">
					<td>Permanent address</td>
					<td><?php echo @$contact->address; ?></td>
					<td>Telephone</td>
					<td><?php echo @$contact->phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>City</td>
					<td><?php echo @$contact->city_name; ?></td>
					<td>District</td>
					<td><?php echo @$contact->district_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Province</td>
					<td><?php echo @$contact->province_name; ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Emergency)</td>
				</thead>
				<tr class="table-row">
					<td>Emergency Contacts</td>
					<td><?php echo @$emr->cotact_person_name; ?></td>
						<td>Telephone</td>
					<td><?php echo @$emr->home_phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php echo @$emr->mobile_num; ?></td>
					<td>Relationship</td>
					<td><?php echo @$jn_emr->relation_name; ?></td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Educational Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Degree</td>
					<td><?php echo @$qua->qualification; ?></td>
					<td>Institiute</td>
					<td><?php echo @$qua->institute; ?></td>
				</tr>
				<tr class="table-row">
					<td>Specialization</td>
					<td><?php echo @$qua->major_specialization; ?></td>
					<td>GPA/Score</td>
					<td><?php echo @$qua->gpa_score; ?></td>
				</tr>
				<tr class="table-row">
					<td>Year Of Completion</td>
					<td><?php echo @$qua->year; ?></td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Experiance</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Job Title</td>
					<td><?php echo @$jn_exp->designation_name; ?></td>
					<td>Organization</td>
					<td><?php echo @$exp->organization; ?></td>
				</tr>
				<tr class="table-row">
					<td>Employement Type</td>
					<td><?php echo @$jn_exp->employment_type; ?></td>
					<td>Start Date</td>
					<td><?php echo date_format_helper(@$exp->from_date); ?></td>
				</tr>
				<tr class="table-row">
					<td>End Date</td>
					<td><?php echo date_format_helper(@$exp->to_date); ?></td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Skills</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Skill Name</td>
					<td><?php echo @$jn_skl->skill_name; ?></td>
					<td>Experiance in Years</td>
					<td><?php echo @$skill->experience_in_years; ?></td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Employement Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Current Job Title</td>
					<td><?php echo @$jn_exp->designation_name; ?></td>
					<td>Job Specification</td>
					<td><?php echo @$emplyeee->job_specifications; ?></td>
				</tr>
				<tr class="table-row">
					<td>Employment Type</td>
					<td><?php echo @$jn_exp->employment_type; ?></td>
					<td>Job Category</td>
					<td><?php echo @$emplyeee->job_category_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Department</td>
					<td><?php echo @$emplyeee->department_name; ?></td>
					<td>City</td>
					<td><?php echo @$emplyeee->city_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Location</td>
					<td><?php echo @$emplyeee->posting_location; ?></td>
					<td>Work Shift</td>
					<td><?php echo @$emplyeee->shift_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Contract Expiry Date</td>
					<td><?php echo date_format_helper(@$emplyeee->contract_expiry_date); ?></td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Dependents</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
                    <td><?php echo @$depend->dependent_name; ?></td>
					<td style="none;"><strong>Relationship</strong></td>
					<td><?php echo @$depend->relation_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Date Of Birth</td>
					<td><?php echo date_format_helper(@$depend->date_of_birth); ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Pay Package</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td><?php echo @$pay->pay_grade; ?></td>
					<td>Pay Frequency</td>
					<td><?php echo @$pay->pay_frequency; ?></td>
				</tr>
				<tr class="table-row">
					<td>Currency</td>
					<td><?php echo @$pay->currency_name; ?></td>
					<td>Payrate</td>
					<td><?php echo @$pay->payrate; ?></td>
				</tr>	
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Entitlements</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Increments</td>
					<td><?php echo @$last->increment_amount; ?></td>
					<td>Increments Type</td>
					<td><?php echo @$last->increment_type; ?></td>
				</tr>
				<tr class="table-row">
					<td>Benefits</td>
					<td><?php echo @$last->benefit_name; ?></td>
					<td>Benefits Type</td>
					<td><?php echo @$last->benefit_type_title; ?></td>
				</tr>
				<tr class="table-row">
                	<td>Allowance Amount</td>
					<td><?php echo @$last->allowance_amount; ?></td>
					<td>Allowance Type</td>
					<td><?php echo @$last->allowance_type; ?></td>
				</tr>
				<tr class="table-row">
					<td>Leaves</td>
					<td><?php echo @$last->no_of_leaves; ?></td>
					<td>Leave Type</td>
					<td><?php echo @$last->leave_type; ?></td>
				</tr>
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Report To</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td><?php echo @$last->emp_name; ?></td>
					<td>Reporting Method</td>
					<td><?php echo @$last->reporting_option; ?></td>
				</tr>
				
				</tbody>
			</table>
	</div>
	<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->