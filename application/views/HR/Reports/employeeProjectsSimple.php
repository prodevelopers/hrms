<style>
    .EmployeeProjectsTable > thead.table-head > tr > th {
        text-align: left;
    }
/*    .actionColumn{
        display:none;
    }*/
    .activeBorder{
        border-bottom: 2px solid #0c98c1;
    }
</style>
<!-- contents -->
<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Manage Projects</div> <!-- bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Manage Employee Projects <!--<span style="float: right;"><button id="queuedProjectsButton" class="btn red" style="display: block;">Queued Projects</button><button id="currentProjectsButton" class="btn green" style="display: block; display: none;"> Current Projects</button></span>--></div>


        <div class="notice info">
            <p>Read Before Allocation of Projects</p>
            <ul>
                <li>To assign new projects, Respective dates must be specified.</li>
                <li>If No Date Provided, Then Current Date will be picked Automatically.</li>
                <li>If No Date Provided, Then Refresh Page, After Assigning of Projects to View Updated Dates.</li>
            </ul>
        </div>

        <!-- button group -->
        <div class="form-left">
            <input type="hidden" value="currentMonth" id="selectedMonth">
            <? //Table For Current Month ?>
            <table cellspacing="0" id="currentMonthTable" class="EmployeeProjectsTable">
                <thead class="table-head">
                <tr>
                    <th>SelectBox</th>
                    <th>Project Title</th>
                    <th>Project Assign Date</th>
                    <th>Project Start Date</th>
                    <th class="actionColumn">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(isset($employeeProjects) && !empty($employeeProjects) && is_array($employeeProjects)){
                    foreach($employeeProjects as $projectKey=>$projectValue){
                        echo '<tr class = "table-row" data-project="'.$projectValue->AllProjectIDs.'"><td><input type="checkbox" class="employeeProjectCheckBox"';
                        if(isset($projectValue->ProjectID) and !empty($projectValue->ProjectID)){
                            echo "checked";
                            $assignedStatus = true;
                        }else{
                            $assignedStatus = false;
                        }
                        echo '></td><td>'.$projectValue->ProjectTitle.'</td><td id="dateColumn">'.((isset($projectValue) && !empty($projectValue->ProjectAssignedDate))?date('d-m-Y',strtotime($projectValue->ProjectAssignedDate)):" - ").'</td><td>'.((isset($projectValue) && !empty($projectValue->ProjectStartDate))?date('d-m-Y',strtotime($projectValue->ProjectStartDate)):'').'</td><td class="actionColumn">'.((isset($assignedStatus) && $assignedStatus === true)?"Assigned":"").'</td></tr>';
                    }
                }
                ?>
                </tbody>
            </table>
            <div class="button-group">
                <button id="assignRevokeBtn" class="btn green">Assign/Revoke Projects</button>
            </div>
        </div>

        <div class="form-right">
            <table cellspacing="0" class="table-approved" style="margin-top: 0.7em; width: 100%">
                <thead class="table-head">
                <td colspan="4">Employee Information</td>
                </thead>
                <tr class="table-row">
                    <th>Employee Code</th>
                    <td><?php echo isset($employeeData)?$employeeData->EmployeeCode:'' ?></td>
                </tr>
                <tr class="table-row">
                    <th>Employee Name</th>
                    <td><?php echo isset($employeeData)?$employeeData->EmployeeName:'' ?></td>
                </tr>
                <tr class="table-row">
                    <th>Designation</th>
                    <td><?php echo isset($employeeData)?$employeeData->Designation:'' ?></td>
                </tr>
                <tr class="table-row">
                    <th>Department</th>
                    <td><?php echo isset($employeeData)?$employeeData->Department:'' ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">

    //Code Here For the Ajax Edit/Update
    $(document).ready(function(){
        $('.employeeProjectCheckBox').on('change',function(e){
            var dateCol = $(this).parents('tr').find('td#dateColumn');
            var dateColCurrentValue = dateCol.text();

            if($(this).is(':checked')){
                if(dateColCurrentValue === '-'){
                    //If its equal to the minus sign then we need to show the date selector

                }else if(isValidDate(dateColCurrentValue)){
                    //No Need To Do Anything As project is Already Been Assigned.
                    $(this).parents('tr').find('.actionColumn').html('Assigned');
                }else{
                    dateCol.html('<input type="text" class="dateSelector" style="min-width: 65%;" />');
                    $(this).parents('tr').find('.actionColumn').html('<span style="color:green">To Assign</span>')
                }
            }else if(!$(this).is(':checked')){
                if(isValidDate(dateColCurrentValue)){
                    //Change Status To Be Rovoked.
                    $(this).parents('tr').find('.actionColumn').html('<span style="color:red">Revoke</span>')
                }else{
                    dateCol.html(' - ');
                    $(this).parents('tr').find('.actionColumn').html('')
                }
            }
        });


        ////////////Now Need To Work On Ajax Post..

        //First Need To Get the Values Of Checked And UnChecked.
        $('#assignRevokeBtn').on('click', function () {

            //First Get Projects That Needs To Be Assigned.
            var selectedProjectCheckBoxes = $('#currentMonthTable .employeeProjectCheckBox:checked');
            var selectedProjectCheckBoxesChecked = [];
            selectedProjectCheckBoxes.each(function (e) {
                var rowSelectedDate = $(this).parents('tr').find('.dateSelector').val();
                var projectID = $(this).closest('tr').attr('data-project');
                if(typeof (rowSelectedDate) !== 'undefined'){
                    $data = {
                        proID: projectID,
                        projectAssignDate: rowSelectedDate,
                        status:'ToAssign'
                    };
                    selectedProjectCheckBoxesChecked.push($data);
                }
            });


            //Now Get Already Assigned Projects That Needs Removal.
            var unselectedProjects = $('#currentMonthTable .employeeProjectCheckBox:not(:checked)');
            var toBeRevokedProjects = [];
            unselectedProjects.each(function (e) {
                var unselectedRowDateSection = $(this).parents('tr').find('td#dateColumn').text();
                var status = $(this).parents('tr').find('.actionColumn').text();
                var projectID = $(this).closest('tr').attr('data-project');

                if(isValidDate(unselectedRowDateSection) && status === 'Revoke'){
                    $data = {
                        proID:projectID,
                        status:'ToRevoke'
                    };
                    toBeRevokedProjects.push($data);
                }
            });


            $.ajax({
                url:"<?php echo base_url(); ?>human_resource/update_employee_projects_simple",
                data:{ToAssignProjects:JSON.stringify(selectedProjectCheckBoxesChecked),
                    ToRevokeProjects:JSON.stringify(toBeRevokedProjects),
                    empID:'<?php echo $this->uri->segment(3) ?>'},
                type: "POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);

                        //If Everything Went Ok, Then Please Remove All the DatePickers.
                        var openedDatePickers = $('.dateSelector');
                        openedDatePickers.each(function(e){
                            var date = $(this).val();
                            $(this).parents('tr').find('td#dateColumn').html(date);
                        });

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });




        $('body').on('focus', ".dateSelector", function () {
            $(this).datepicker({
                dateFormat: "dd-mm-yy",
                firstDay: 1
            });
        });

        function isValidDate(date){
                try{
                    var splitData = date.split('-');
                    var parsedDate = Date.parse(splitData[2]+'-'+splitData[1]+'-'+splitData[0]);
                    if(!isNaN(parsedDate) && parsedDate !== "Invalid Date"){
                        return true;
                    }else{
                        return false;
                    }
                }
            catch(ex){
                return false;
            }
        }
    });

</script>


<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- left side menu end -->