<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.resize{
	width: 220px;
}
</style>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head">Performance Evaluation</div>
<!-- Right Side Historic Detail -->
	<div class="form-right">
		<table class="table">
			<thead class="table-head">
				<td colspan="2">Employee Info</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee Name</h5></td>
					<td><?php echo @$kpi_emp_h->full_name;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee ID</h5></td>
					<td><?php echo @$kpi_emp_h->employee_code;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Designation</h5></td>
					<td><?php echo @$kpi_emp_h->designation_name;?></td>
				</tr>
			</tbody>
		</table>
	</div>
            <!-- Right Side Historic Detail -->
            <br class="clear" />
            <!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<?php 
				echo form_open();?>
                <select name="project" class='resize' id='project_id' onclick="populate_tasks();" required="required">
                    <option value="">Select Project</option>
                    <?php if(!empty($projects)){
                        foreach($projects as $projects){
                    }?>
                    <option value="<?php echo $projects->project_id;?>"><?php echo $projects->project_title;?></option>
                    <?php }?>
                </select>
                <?php
                //echo @form_dropdown('project', $projects, $slctd_project, "class='resize' id='project_id' onChange='populate_tasks();'");

                echo @form_dropdown('tasks', $tasks, $slctd_task, "class='resize' id='tasks' onChange='this.form.submit()' disabled='disabled'");
                //echo @form_dropdown('month', $month, $slctd_month, "class='resize' id='month' onChange='this.form.submit()'"); ?>
				
				<?php echo form_close(); ?>
			</div>
            <br class="clear" />


			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>KPI's</td>
					<td>Tasks</td>
					<td>Start Date</td>
					<td>Completion Date</td>
					<td>Evaluation</td>
					<td>Print</td>
					<td>Action</td>

				</thead>
                <?php if(!empty($kpi_eval)){
					foreach($kpi_eval as $kpi){ ?>
                <tr class="table-row">
                	<td><?php echo $kpi->kpi; ?></td>
                	<td><?php echo $kpi->task_name; ?></td>
                	<td><?php echo date_format_helper($kpi->start_date); ?></td>
                	<td><?php echo date_format_helper($kpi->completion_date); ?></td>
                	<td><div class="meter orange"><?php echo  intval($kpi->percent_achieved); ?>%<span style="width: <?php echo $kpi->percent_achieved; ?>%"></span></div></td>
					<td><a href="human_resource/print_perform/<?php echo $kpi->employment_id ?>/<?php echo $kpi->job_assignment_id; ?>"><span class="fa fa-print"></span></a></td>
                    <td><a href="human_resource/add_performance_evalution/<?php echo $kpi->employment_id ?>/<?php echo $kpi->job_assignment_id; ?>"><button class="btn green">Progress</button></a></td>
                </tr>
                <?php } }else{echo "<td colspan='6'><span style='color:red'>No Record Found ....</span></td>";} ?>
				<tr><td><a href="human_resource/assign_position/<?php echo $employeee_id?>"><button class="btn green">Go Back</button></a></td></tr>
           </table>
       </div>
</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
     $(document).ready(function(e){
        $('#task, #year').select2();
    });


    function populate_tasks()
    {
        var proj_id=$("#project_id").val();

        $.ajax({
            type:"POST",
            url:"<?php echo base_url(); ?>human_resource/task_populate",
            dataType:"JSON",
            data:{project_id:proj_id},
            success:function(result){
                $('#tasks').removeAttr("disabled");
                $('#tasks').text('');
                $.each(result,function(ml_assign_task_id,task_name){

                    var opt=$('<option/>');
                    opt.val(ml_assign_task_id);
                    opt.text(task_name);
                    $('#tasks').append(opt);

                });
            }
        });
    }
    </script>
    <!-- leftside menu end -->
