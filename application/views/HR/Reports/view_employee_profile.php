<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
<script type="text/javascript">
/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");
	}
</script>
<!-- contents -->
<?php
//employee ID from the Segment
$employeeIDFromSegment = $this->uri->segment(3);
?>
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Human Resource / Employee Profile View</div>
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Complete Details <span class="fa fa-print" style="float:right; padding-right:40px;padding-top:5px; color:#fff; cursor:pointer" onclick="return print_report('<?php echo @$employee_id ?>')"></span></div>

			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="5">Personnel Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td rowspan="4" width="150"><img src="<?php echo get_user_avatar($employeeIDFromSegment); ?>" width="150" height="150"></td>
					<td>Employee Name</td>
					<td><?php if(isset($personal_info)){echo $personal_info->full_name;}else{}?></td>
					<td>Employee ID</td>
					<td><?php if(isset($personal_info)){echo $personal_info->employee_code;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Father Name</td>
					<td><?php if(isset($personal_info)){echo $personal_info->father_name;}else{} ?></td>
					<td>CNIC</td>
					<td><?php if(isset($personal_info)){echo $personal_info->CNIC;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Martial Status</td>
					<td><?php if(isset($gender_etc)){echo $gender_etc->marital_status_title;}else{}?></td>
					<td>Date Of Birth</td>
					<td><?php if(isset($personal_info)){echo date_format_helper($personal_info->date_of_birth);}else{}?></td>
				</tr>	
				<tr class="table-row">
					<td>Nationality</td>
					<td><?php if(isset($gender_etc)){echo $gender_etc->nationality;}else{}?></td>
					<td>Religion</td>
					<td><?php if(isset($gender_etc)){echo $gender_etc->religion_title;}else{} ?></td>
				</tr>		
				</tbody>	
			</table>
			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="4">Contact Information (Current)</td>
				</thead>
				<tbody>

				<tr class="table-row">
					<td>Current address</td>
					<td><?php if(isset($cont)){echo $cont->address;}else{}?></td>
					<td>Telephone</td>
					<td><?php  if(isset($cont)){echo $cont->home_phone;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php if(isset($cont)){echo $cont->mob_num;}else{} ?></td>
					<td>Email</td>
					<td><?php if(isset($cont)){echo $cont->email_address;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Work Telephone</td>
					<td><?php  if(isset($cont)){echo $cont->office_phone;}else{}?></td>
					<td>Work Email</td>
					<td><?php if(isset($cont)){echo $cont->official_email;}else{}?></td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Permanent)</td>
				</thead>
				<tr class="table-row">
					<td>Permanent address</td>
					<td><?php if(isset($contact)){echo $contact->address;}else{}?></td>
					<td>Telephone</td>
					<td><?php if(isset($contact)){echo $contact->phone;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>City</td>
					<td><?php if(isset($contact)){echo $contact->city_name;}else{}?></td>
					<td>District</td>
					<td><?php if(isset($contact)){echo $contact->district_name;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Province</td>
					<td><?php if(isset($contact)){echo $contact->province_name;}else{}?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Emergency)</td>
				</thead>
				<tr class="table-row">
					<td>Emergency Contacts</td>
					<td><?php if(isset($emr)){echo $emr->cotact_person_name;}else{}?></td>
						<td>Telephone</td>
					<td><?php if(isset($emr)){echo $emr->home_phone;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php if(isset($emr)){echo $emr->mobile_num;}else{}?></td>
					<td>Relationship</td>
					<td><?php if(isset($jn_emr)){echo $jn_emr->relation_name;}else{}?></td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Educational Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Degree</td>
					<td><?php if(isset($qua)){echo $qua->qualification;}else{}?></td>
					<td>Institute</td>
					<td><?php if(isset($qua)){echo $qua->institute;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Specialization</td>
					<td><?php if(isset($qua)){echo $qua->major_specialization;}else{}?></td>
					<td>GPA/Score</td>
					<td><?php if(isset($qua)){echo $qua->gpa_score;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Year Of Completion</td>
					<td><?php if(isset($qua)){echo $qua->year;}else{}?></td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Experience</td>
				</thead>
				<tbody>



                        <?php if(!empty($exp)){
                            foreach($exp as $xp){?>
                                <tr class="table-row">

                               <?php echo "<td>".$xp->organization." </td><td> ".$xp->designation_name." </td><td> ".$xp->from_date."</td> <td>".$xp->to_date."</td>";?>

                    </tr>
                            <?php  }}?>
                        <tr class="table-row">
                            <td>Total Experience</td>
					<td>
                        <?php
                        $contract_extends=$emplyeee->extension;
                        $contract_extends_start = new DateTime($emplyeee->e_start_date);
                        $contract_extends_end = strtotime($emplyeee->e_end_date);
                        $contract_extends_end2 = new DateTime($emplyeee->e_end_date);
                        $contract_start = new DateTime($emplyeee->contract_start_date);
                        $contract_end = new DateTime($emplyeee->contract_expiry_date);
                        $current_date = strtotime(date("Y-m-d"));
                        $month=0;
                        $year=0;
                        if($contract_extends == 1) {

                            $ContDiff = $contract_extends_end > $current_date;
                            if ($ContDiff > 0) {
                                $ContStartDate = $contract_start;
                                //$ContEndDate = new DateTime($contract_extends_end);
                                $current_date = new DateTime(date("Y-m-d"));
                                $interval = $ContStartDate->diff($current_date);
                                $total_current_exp=$interval->y .".".$interval->m;
                                $month=$interval->m;
                                $year=$interval->y.".0";
                                //echo $interval->y . " Year's, " . $interval->m . " Month's";
                              //  die;
                            } else {

                                $interval = $contract_start->diff($contract_extends_end2);
                                $total_current_exp=$interval->y .".".$interval->m;
                                $month=$interval->m;
                                $year=$interval->y.".0";
                                //echo $interval->y . " Year's, " . $interval->m . " Month's";
                               // die;
                            }
                        }else{

                            $interval = $contract_start->diff($contract_end);
                            $total_current_exp=$interval->y .".".$interval->m;
                            $month=$interval->m;
                            $year=$interval->y.".0";

                            //echo $interval->y . " Year's, " . $interval->m . " Month's";
                           // die;
                        }

                        if(!empty($exp)){
                            //echo "test"; die;
                            $monthx=0;
                            $yearx=0;
                            $total=0;
                            foreach($exp as $exps){

                      /*  if(isset($exp->to_date) && !empty($exps->to_date) && isset($exps->from_date) && !empty($exp->from_date)){*/
                           //echo "test"; die;
                            $startDate = new DateTime($exps->from_date);
                           $endDate = new DateTime($exps->to_date);
                            $interval = $startDate->diff($endDate);
                            $total_exp=$interval->y .".".$interval->m;
                            $total_expv=$interval->y ." Years, ".$interval->m." Months";
                            $monthx +=$interval->m;
                             $yearx +=$interval->y.".0";

                            $total +=$total_exp;
                            //echo "test"; die;
                         /*}*/ }

                            $total_months =$month + $monthx;
                            $total_years=$year + $yearx;
                            if($total_months > 0)
                            {
                               $month_exp=round($total_months/12,2); echo "<br>";
                                $years_exp=$total_years;
                                $add=$years_exp + $month_exp;
                                $total_experience= round($add,PHP_ROUND_HALF_UP);
                                $explode=explode('.',$total_experience);
                                echo @$explode[0]." Year(s) , ".@$explode[1]." Month(s)";
                            }else{echo "";}
                        //$explode=explode(".",@$total);
                            //$explodeTotal_exp=@$explode[0].".".@$explode[1];
                            //$grandTotal =$total + @$total_current_exp;
                           // $explode=explode(".",$grandTotal);
                       // echo @$explode[0]." Year(s) , ".@$explode[1]." Month(s)";
                        }
                        //print_r($exp);?></td>

				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Skills</td>
				</thead>
				<tbody>
				<tr class="table-row">
				<td>Skill Name</td>
                <td>Levels</td>
                <td></td>
                <td></td>
                </tr>
				<?php
                if(!empty($skill)){
                foreach($skill as $skills){
                ?>
                <tr>
                <td><?php echo $skills->skill_name;?></td>
                <td><?php echo $skills->skill_level;?></td>
                <td></td>
                <td></td>
                </tr>
                <?php } }?>
                </tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Employment Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Current Job Title</td>
					<td><?php if(isset($jn_exp)){echo $jn_exp->designation_name;}else{}?></td>
					<td>Job Specification</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->job_specifications;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Employment Type</td>
					<td><?php if(isset($jn_exp)){echo $jn_exp->employment_type;}else{}?></td>
					<td>Job Category</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->job_category_name;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Department</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->department_name;}else{}?></td>
					<td>City</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->city_name;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Location</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->posting_location;}else{}?></td>
					<td>Work Shift</td>
					<td><?php if(isset($emplyeee)){echo $emplyeee->shift_name;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Contract Start Date</td>
					<td><?php if(isset($emplyeee)){echo date_format_helper($emplyeee->contract_start_date);}else{}?></td>
                    <td>Contract Expiry Date</td>
                    <td><?php if(isset($emplyeee)){echo date_format_helper($emplyeee->contract_expiry_date);}else{}?></td>
				</tr>

				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Dependents</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
                    <td><?php if(isset($depend)){echo $depend->dependent_name;}else{}?></td>
					<td style="none;"><strong>Relationship</strong></td>
					<td><?php if(isset($depend)){echo $depend->relation_name;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Date Of Birth</td>
					<td><?php if(isset($depend)){echo date_format_helper($depend->date_of_birth);}else{}?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Pay Package</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td><?php if(isset($pay)){echo $pay->pay_grade;}else{}?></td>
					<td>Pay Frequency</td>
					<td><?php if(isset($pay)){echo $pay->pay_frequency;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Currency</td>
					<td><?php if(isset($pay)){echo $pay->currency_name;}else{}?></td>
					<td>Payrate</td>
					<td><?php if(isset($pay)){echo $pay->payrate;}else{}?></td>
				</tr>	
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Entitlements</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Increments</td>
					<td><?php if(isset($last)){echo $last->increment_amount;}else{}?></td>
					<td>Increments Type</td>
					<td><?php if(isset($last)){echo $last->increment_type;}else{}?></td>
				</tr>
				<tr class="table-row">
				<!--	<td>Benefits</td>
					<td><?php /*if(isset($last)){echo $last->benefit_name;}else{echo "No Record Found";}*/?></td>-->
					<td>Benefits Type</td>
					<td><?php if(isset($last)){echo $last->benefit_type_title;}else{}?></td>
				</tr>
				<tr class="table-row">
                	<td>Allowance Amount</td>
					<td><?php if(isset($last)){echo $last->allowance_amount;}else{}?></td>
					<td>Allowance Type</td>
					<td><?php if(isset($last)){echo $last->allowance_type;}else{}?></td>
				</tr>
				<tr class="table-row">
					<td>Leaves</td>
					<td><?php if(isset($last)){echo $last->no_of_leaves_allocated;}else{}?></td>
					<td>Leave Type</td>
					<td><?php if(isset($last)){echo $last->leave_type;}else{}?></td>
				</tr>
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Report To</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td><?php if(isset($last)){echo $last->emp_name;}else{}?></td>
					<td>Reporting Method</td>
					<td><?php if(isset($last)){echo $last->reporting_option;}else{}?></td>
				</tr>
				
				</tbody>
			</table>
	</div>
	<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->