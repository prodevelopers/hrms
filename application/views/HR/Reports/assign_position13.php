<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Manage Position</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_left_nav'); ?>
	

	<div class="right-contents">

		<div class="head">Manage Position</div>
		<div class="form-left">
		 <?php echo form_open();?>
				<div class="row2">
					<h4>Pay Grade</h4>
                    <?php  echo form_dropdown('pay_grade', $paygrade,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Designation</h4>
                    <?php echo form_dropdown('designation_name', $designation,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Job Specification</h4>
                    <input type="text" name="job_specifications"  />
					<!--<select>
						<option></option>
					</select>-->
				</div>
              
				
				<br class="clear">
				<div class="row2">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective_from" id="joining_date">
				</div>
			 
            	<div class="row2">
					<div class="button-group">
						<input type="submit" name="add" value="Mange Position" class="btn green" />
					</div>
				</div>
				<?php echo form_close();?>
                </div>
            
            <!-- Right Side Historic Detail -->
            <div class="form-right">
                    <div class="head">Employee Position Info</div>
                            <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$mp_history->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$mp_history->designation_name;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Pay Grade</h5>
                                <?php if(!empty($mp_history_wd)):
									foreach($mp_history_wd as $mp_history): ?>
                                <i class="italica"><?php echo @$mp_history->pay_grade;?></i>
                                <?php endforeach;
									endif;?>
                        </div>
                        <div class="row2">
                                <h5 class="headingfive">Job Specification</h5>
                                <?php if(!empty($mp_history_wd)):
									foreach($mp_history_wd as $mp_history): ?>
                                <i class="italica"><?php echo @$mp_history->job_specifications;?></i>
                                <?php endforeach;
									endif;?>
                        </div>
            </div>
            <!-- Right Side Historic Detail -->
          

<table cellspacing="0">
				<thead class="table-head">
					<td>Pay Grade</td>
					<td>Job Specification</td>
					<td>Date Effective From</td>
					<td>Status</td>
				</thead>
                <?php if (!empty($join)){?>
			<?php foreach($join as $record):
			//echo "<pre>"; print_r($join); die;
			?>
				<tr class="table-row">
					<td><?php echo $record->pay_grade; ?></td>
					<td><?php echo $record->job_specifications; ?></td>
					<td><?php echo $record->date_effective_from; ?></td>
					<td><?php echo $record->status_title; ?></td>
				</tr>
				  <?php endforeach; 
            	 } else { ?> 
			<td colspan="6"><?php echo " <span style='color:#F00'>No record found..</span>";?> </td>
			<?php }?> 
			</table>
		</div>

	</div>
<!-- contents -->


