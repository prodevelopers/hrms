

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Terminate Employees</div>

		
			<?php echo form_open();?>
				<div class="row">
					<h4>Separation Type</h4>
                    <?php echo @form_dropdown('separationType', $reason,'required = "required"','required = "required"'); ?>
					<!--<select>
						<option></option>
					</select-->
				</div>
        <br class="clear">
        <div class="row">
            <h4>Reason</h4>
            <?php echo form_dropdown('reason', $reason2,'required = "required"','required = "required"'); ?>
            <!--<select>
                <option></option>
            </select-->
        </div>
				<br class="clear">
				<div class="row">
					<h4>Date</h4>
					<input type="text" name="date" id="date" required="required">
				</div>
				
				<br class="clear">
				<div class="row">
					<h4>Note</h4>
					<textarea name="note"></textarea>
				</div>

			<!-- button group -->
            <br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="terminateBtn" value="Terminate" class="btn green" />
						<!--<button class="btn green">Terminate</button>
				 <button class="btn red">Delete</button> -->
				</div>
			</div>
<?php echo form_close();?>
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $( "#date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function( selectedDate ) {
            $("#applyForLeaveFromDate").datepicker( "option", "maxDate", selectedDate );
        }
    });
    </script>
    <!-- leftside menu end -->