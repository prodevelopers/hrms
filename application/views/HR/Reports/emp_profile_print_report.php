<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="no-js">
<head>
	<title>HR Management System</title>
    <base href="<?php echo base_url(); ?>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
    <link rel="stylesheet" type="text/css" href="assets/multslect/multiple-select.css">
	<link rel="stylesheet" type="text/css" href="assets/css/form.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script type="text/javascript" src="assets/js/jquery-1.9.1.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
        var a=confirm("Do You Want to Print . . . . !");
        if(a==true)
        {
            window.print() ;
        }
	});
	</script>
</head>
<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>

		<div class="table-head class">Complete Details</div>

<table cellspacing="0">
	<thead class="table-head class">
	<td colspan="5">Personnel Information</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td rowspan="4" width="150"><img src="upload/Thumb_Nails/<?php echo @$personal_info->thumbnail; ?>" width="150" height="150"></td>
		<td>Employee Name</td>
		<td><?php echo @$personal_info->full_name; ?></td>
		<td>Employee ID</td>
		<td><?php echo @$personal_info->employee_code; ?></td>
	</tr>
	<tr class="table-row">
		<td>Father Name</td>
		<td><?php echo @$personal_info->father_name; ?></td>
		<td>CNIC</td>
		<td><?php echo @$personal_info->CNIC; ?></td>
	</tr>
	<tr class="table-row">
		<td>Martial Status</td>
		<td><?php echo @$gender_etc->marital_status_title; ?></td>
		<td>Date Of Birth</td>
		<td><?php echo date_format_helper($personal_info->date_of_birth); ?></td>
	</tr>
	<tr class="table-row">
		<td>Nationality</td>
		<td><?php echo @$gender_etc->nationality; ?></td>
		<td>Religion</td>
		<td><?php echo @$gender_etc->religion_title; ?></td>
	</tr>
	</tbody>
</table>
<table cellspacing="0">
	<thead class="table-head class">
	<td colspan="4">Contact Information (Current)</td>
	</thead>
	<tbody>

	<tr class="table-row">
		<td>Current address</td>
		<td><?php echo @$cont->address; ?></td>
		<td>Telephone</td>
		<td><?php echo @$cont->home_phone; ?></td>
	</tr>
	<tr class="table-row">
		<td>Mobile</td>
		<td><?php echo @$cont->mob_num; ?></td>
		<td>Email</td>
		<td><?php echo @$cont->email_address; ?></td>
	</tr>
	<tr class="table-row">
		<td>Work Telephone</td>
		<td><?php echo @$cont->office_phone; ?></td>
		<td>Work Email</td>
		<td><?php echo @$cont->official_email; ?></td>
	</tr>
	<thead class="table-head class">
	<td colspan="4">Contact Information (Permanent)</td>
	</thead>
	<tr class="table-row">
		<td>Permanent address</td>
		<td><?php echo @$contact->address; ?></td>
		<td>Telephone</td>
		<td><?php echo @$contact->phone; ?></td>
	</tr>
	<tr class="table-row">
		<td>City</td>
		<td><?php echo @$contact->city_name; ?></td>
		<td>District</td>
		<td><?php echo @$contact->district_name; ?></td>
	</tr>
	<tr class="table-row">
		<td>Province</td>
		<td><?php echo @$contact->province_name; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<thead class="table-head class">
	<td colspan="4">Contact Information (Emergency)</td>
	</thead>
	<tr class="table-row">
		<td>Emergency Contacts</td>
		<td><?php echo @$emr->cotact_person_name; ?></td>
		<td>Telephone</td>
		<td><?php echo @$emr->home_phone; ?></td>
	</tr>
	<tr class="table-row">
		<td>Mobile</td>
		<td><?php echo @$emr->mobile_num; ?></td>
		<td>Relationship</td>
		<td><?php echo @$jn_emr->relation_name; ?></td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Educational Information</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Degree</td>
		<td><?php echo @$qua->qualification; ?></td>
		<td>Institiute</td>
		<td><?php echo @$qua->institute; ?></td>
	</tr>
	<tr class="table-row">
		<td>Specialization</td>
		<td><?php echo @$qua->major_specialization; ?></td>
		<td>GPA/Score</td>
		<td><?php echo @$qua->gpa_score; ?></td>
	</tr>
	<tr class="table-row">
		<td>Year Of Completion</td>
		<td><?php echo @$qua->year; ?></td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Experiance</td>
	</thead>
	<tbody>
    <?php if(!empty($exp)){
        foreach($exp as $xp){?>
            <tr class="table-row">

                <?php echo "<td>".$xp->organization." </td><td> ".$xp->designation_name." </td><td> ".$xp->from_date."</td> <td>".$xp->to_date."</td>";?>

            </tr>
        <?php  }}?>
    <tr class="table-row">
        <td>Total Experience</td>
        <td>
            <?php
            $contract_extends=$emplyeee->extension;
            $contract_extends_start = new DateTime($emplyeee->e_start_date);
            $contract_extends_end = strtotime($emplyeee->e_end_date);
            $contract_extends_end2 = new DateTime($emplyeee->e_end_date);
            $contract_start = new DateTime($emplyeee->contract_start_date);
            $contract_end = new DateTime($emplyeee->contract_expiry_date);
            $current_date = strtotime(date("Y-m-d"));
            $month=0;
            $year=0;
            if($contract_extends == 1) {

                $ContDiff = $contract_extends_end > $current_date;
                if ($ContDiff > 0) {
                    $ContStartDate = $contract_start;
                    //$ContEndDate = new DateTime($contract_extends_end);
                    $current_date = new DateTime(date("Y-m-d"));
                    $interval = $ContStartDate->diff($current_date);
                    $total_current_exp=$interval->y .".".$interval->m;
                    $month=$interval->m;
                    $year=$interval->y.".0";
                    //echo $interval->y . " Year's, " . $interval->m . " Month's";
                    //  die;
                } else {

                    $interval = $contract_start->diff($contract_extends_end2);
                    $total_current_exp=$interval->y .".".$interval->m;
                    $month=$interval->m;
                    $year=$interval->y.".0";
                    //echo $interval->y . " Year's, " . $interval->m . " Month's";
                    // die;
                }
            }else{

                $interval = $contract_start->diff($contract_end);
                $total_current_exp=$interval->y .".".$interval->m;
                $month=$interval->m;
                $year=$interval->y.".0";

                //echo $interval->y . " Year's, " . $interval->m . " Month's";
                // die;
            }

            if(!empty($exp)){
                //echo "test"; die;
                $monthx=0;
                $yearx=0;
                $total=0;
                foreach($exp as $exps){

                    /*  if(isset($exp->to_date) && !empty($exps->to_date) && isset($exps->from_date) && !empty($exp->from_date)){*/
                    //echo "test"; die;
                    $startDate = new DateTime($exps->from_date);
                    $endDate = new DateTime($exps->to_date);
                    $interval = $startDate->diff($endDate);
                    $total_exp=$interval->y .".".$interval->m;
                    $total_expv=$interval->y ." Years, ".$interval->m." Months";
                    $monthx +=$interval->m;
                    $yearx +=$interval->y.".0";

                    $total +=$total_exp;
                    //echo "test"; die;
                    /*}*/ }

                $total_months =$month + $monthx;
                $total_years=$year + $yearx;
                if($total_months > 0)
                {
                    $month_exp=round($total_months/12,2); echo "<br>";
                    $years_exp=$total_years;
                    $add=$years_exp + $month_exp;
                    $total_experience= round($add,PHP_ROUND_HALF_UP);
                    $explode=explode('.',$total_experience);
                    echo @$explode[0]." Year(s) , ".@$explode[1]." Month(s)";
                }else{echo "";}
                //$explode=explode(".",@$total);
                //$explodeTotal_exp=@$explode[0].".".@$explode[1];
                //$grandTotal =$total + @$total_current_exp;
                // $explode=explode(".",$grandTotal);
                // echo @$explode[0]." Year(s) , ".@$explode[1]." Month(s)";
            }
            //print_r($exp);?></td>
    </tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Skills</td>
	</thead>
    <tbody>
    <tr class="table-row">
        <td>Skill Name</td>
        <td>Levels</td>
        <td></td>
        <td></td>
    </tr>
    <?php
    if(!empty($skill)){
        foreach($skill as $skills){
            ?>
            <tr>
                <td><?php echo $skills->skill_name;?></td>
                <td><?php echo $skills->skill_level;?></td>
                <td></td>
                <td></td>
            </tr>
        <?php } }?>
    </tr>
    </tbody>

</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Employement Information</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Current Job Title</td>
		<td><?php echo @$jn_exp->designation_name; ?></td>
		<td>Job Specification</td>
		<td><?php echo @$emplyeee->job_specifications; ?></td>
	</tr>
	<tr class="table-row">
		<td>Employment Type</td>
		<td><?php echo @$jn_exp->employment_type; ?></td>
		<td>Job Category</td>
		<td><?php echo @$emplyeee->job_category_name; ?></td>
	</tr>
	<tr class="table-row">
		<td>Department</td>
		<td><?php echo @$emplyeee->department_name; ?></td>
		<td>City</td>
		<td><?php echo @$emplyeee->city_name; ?></td>
	</tr>
	<tr class="table-row">
		<td>Location</td>
		<td><?php echo @$emplyeee->posting_location; ?></td>
		<td>Work Shift</td>
		<td><?php echo @$emplyeee->shift_name; ?></td>
	</tr>
	<tr class="table-row">
		<td>Contract Expiry Date</td>
		<td><?php echo date_format_helper($emplyeee->contract_expiry_date); ?></td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Dependents</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Name</td>
		<td><?php echo @$depend->dependent_name; ?></td>
		<td style="none;"><strong>Relationship</strong></td>
		<td><?php echo @$depend->relation_name; ?></td>
	</tr>
	<tr class="table-row">
		<td>Date Of Birth</td>
		<td><?php echo date_format_helper($depend->date_of_birth); ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Pay Package</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Pay Grade</td>
		<td><?php echo @$pay->pay_grade; ?></td>
		<td>Pay Frequency</td>
		<td><?php echo @$pay->pay_frequency; ?></td>
	</tr>
	<tr class="table-row">
		<td>Currency</td>
		<td><?php echo @$pay->currency_name; ?></td>
		<td>Payrate</td>
		<td><?php echo @$pay->payrate; ?></td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Entitlements</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Increments</td>
		<td><?php echo @$last->increment_amount; ?></td>
		<td>Increments Type</td>
		<td><?php echo @$last->increment_type; ?></td>
	</tr>
	<tr class="table-row">
		<td>Benefits</td>
		<td><?php echo @$last->benefit_name; ?></td>
		<td>Benefits Type</td>
		<td><?php echo @$last->benefit_type_title; ?></td>
	</tr>
	<tr class="table-row">
		<td>Allowance Amount</td>
		<td><?php echo @$last->allowance_amount; ?></td>
		<td>Allowance Type</td>
		<td><?php echo @$last->allowance_type; ?></td>
	</tr>
	<tr class="table-row">
		<td>Leaves</td>
		<td><?php echo @$last->no_of_leaves; ?></td>
		<td>Leave Type</td>
		<td><?php echo @$last->leave_type; ?></td>
	</tr>
	</tbody>
</table>
<table>
	<thead class="table-head class">
	<td colspan="4">Report To</td>
	</thead>
	<tbody>
	<tr class="table-row">
		<td>Name</td>
		<td><?php echo @$last->emp_name; ?></td>
		<td>Reporting Method</td>
		<td><?php echo @$last->reporting_option; ?></td>
	</tr>

	</tbody>
</table>