
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
<script type="text/javascript">
				//  $(document).ready(function(){
				//  	$(".insurance-filed").hide();
    //   			 $("select").change(function(){
    //         $( "select option:selected").each(function(){
    //             if($(this).attr("value")=="insurance"){
    //                 $(".insurance-filed").show();
    //             }
    //         });
    //     }).change();
    // });
			</script>
	<div class="right-contents">

		<div class="head">Add Insurance</div>
			<?php echo form_open(); ?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee_id; ?>" />
            <?php //print_r($dependents);?>
				<div class="row">
					<h4>Dependent Name</h4>
					<input type="text" name="dependent_name" value="<?php echo @$dependents->dependent_name; ?>" required="required">
				</div>
				
				<br class="clear">
				<div class="row">
					<h4>Date of Birth</h4>
					<input type="text" name="date_of_birth" id="date_of_birth" value="<?php echo @$dependents->date_of_birth; ?>">
				</div>
                <br class="clear" />
                <div class="row">
                	<h4>Insurance Cover</h4>
                    <input type="checkbox" name="insurance_cover" value="1"<?php if(isset($dependents->insurance_cover)){if($dependents->insurance_cover == 1){echo "checked='checked'";}}?>>
                </div>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_insurance" value="Add Insurance" class="btn green" />
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
            <?php echo form_close(); ?>

				<br class="clear">
				<br class="clear">
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->