<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboardsss / Human Resource / Task Management</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Employee Info</div>

                            <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$job_history->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Employee ID</h5>
                                <i class="italica"><?php echo @$job_history->employee_code;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$job_history->designation_name;?></i>
                   	</div>
		<br class="clear">
		<br class="clear">
		<div class="head">Project Detail</div>

		<!-- table -->
		<table cellpadding="0" cellspacing="0">
			<thead class="table-head">
			<!--<td>Employee ID </td>
            <td>Employee Name</td>
            <td>Designation</td>-->
			<td>Project Name</td>
			<td>Project Abbreviation</td>
			<td>Project Start Date</td>
			<td>Project Complete Date</td>
			<td>Project Duration</td>
			<td>Donor</td>
			<td>Project Status</td>
			<!--<td>Approval Date</td>
            <td>Status</td>-->
			<!--<td><span class="fa fa-eye"></span></td>
            <td><span class="fa fa-print"></span></td>-->
			</thead>
			<?php if(!empty($project)){
				foreach($project as $task){?>
					<tr>
						<?php /*?><td><?php echo $task->employee_code;?></td>
                	<td><?php echo $task->full_name;?></td>
                	<td><?php echo $task->designation_name;?></td><?php */?>
						<td><?php echo $task->project_title;?></td>
						<td><?php echo $task->project_abbreviation;?></td>
						<td><?php echo $task->project_start_date?></td>
						<td><?php echo $task->project_end_date?></td>
						<td><?php echo $task->project_duration?></td>
						<td><?php echo $task->donor?></td>
						<td><?php echo $task->project_status?></td>
						<!--<td><?php //if(!empty($task->app_emp_name)){echo $task->app_emp_name;}else{echo "Not Approved";}?></td>
                	<td><?php //echo $task->status;?></td>
                	<td><?php //echo $task->status_title;?></td>-->
					</tr>
				<?php } }else{ ?>
				<tr><td colspan="5"><?php echo "No Record Found ... !";?></td></tr>
			<?php } ?>
		</table>

		<br class="clear">
		<br class="clear">
		<div class="head">Contract Detail</div>

			<!-- table -->
			<table cellpadding="0" cellspacing="0">
				<thead class="table-head">
					<!--<td>Employee ID </td>
					<td>Employee Name</td>
					<td>Designation</td>-->
					<td>Job Category</td>
					<td>Employment Type</td>
					<td>Completion Date</td>
                    <td>Project Comments</td>
					<!--<td>Approval Date</td>
					<td>Status</td>-->
					<!--<td><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>-->
				</thead>
                <?php if(!empty($rec)){
					foreach($rec as $task){?>
                <tr>
                	<?php /*?><td><?php echo $task->employee_code;?></td>
                	<td><?php echo $task->full_name;?></td>
                	<td><?php echo $task->designation_name;?></td><?php */?>
                	<td><?php echo $task->job_category_name;?></td>
                	<td><?php echo $task->employment_type;?></td>
                	<td><?php echo $task->contract_expiry_date?></td>
                	<td><?php echo $task->comments?></td>
                	<!--<td><?php //if(!empty($task->app_emp_name)){echo $task->app_emp_name;}else{echo "Not Approved";}?></td>
                	<td><?php //echo $task->status;?></td>
                	<td><?php //echo $task->status_title;?></td>-->
                </tr>
				<?php } }else{ ?>
                <tr><td colspan="5"><?php echo "No Record Found ... !";?></td></tr>
                <?php } ?>
			</table>



		<br class="clear">
		<br class="clear">
		<div class="head">Employee  Experience</div>

		<!-- table -->
		<table cellpadding="0" cellspacing="0">
			<thead class="table-head">
			<!--<td>Employee ID </td>
            <td>Employee Name</td>-->
			<td>Skill Name</td>
			<td>Experience (Years)</td>
			<td>Comments</td>
			<!--<td>Approval Date</td>
            <td>Status</td>-->
			<!--<td><span class="fa fa-eye"></span></td>
            <td><span class="fa fa-print"></span></td>-->
			</thead>
					<tr>
						<?php /*?><td><?php echo $task->employee_code;?></td>
                	<td><?php echo $task->full_name;?></td>
                	<td><?php echo $task->designation_name;?></td><?php */?>
						<td><?php if(!empty($skill)) { echo $skill->skill_name;}else{echo"No Skill Yet";}?></td>
						<td><?php if(!empty($exp)){echo $exp->experience_in_years;}else{echo"Not Experienced Yet";}?></td>
						<td><?php echo $exp->comments?></td>
						<!--<td><?php //if(!empty($task->app_emp_name)){echo $task->app_emp_name;}else{echo "Not Approved";}?></td>
                	<td><?php //echo $task->status;?></td>
                	<td><?php //echo $task->status_title;?></td>-->
					</tr>
		</table>
		<br class="clear">
		<br class="clear">
		<div class="head">Employee  Contact</div>

		<!-- table -->
		<table cellpadding="0" cellspacing="0">
			<thead class="table-head">
			<td>Address</td>
			<td>Mobile Number</td>
			<td>Email</td>
			</thead>
			<tr>
				<td><?php if(!empty($contact)) { echo $contact->address;}else{echo"No Present Yet";}?></td>
				<td><?php if(!empty($contact)){echo $contact->mob_num;}else{echo"Not Provided Yet";}?></td>
				<td><?php if(!empty($contact)){ echo $contact->email_address;}else{echo"Not Provided Yet";}?></td>

			</tr>
		</table>
			<div id="container">
        <ul>
        <?php //echo $links;?>
        </ul>
        </div>

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->