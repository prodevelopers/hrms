<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Transfer</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Transfer History</div>
            <div class="form-right">
                <div class="row2">
                    <h5 class="headingfive">Employee Name</h5>
                    <i class="italica" ><?php echo @$job_history->full_name;?></i>
                </div>
                <div class="row2">
                    <h5 class="headingfive">Employee ID</h5>
                    <i class="italica"><?php echo @$job_history->employee_code;?></i>
                </div>
                <div class="row2">
                    <h5 class="headingfive">Designation</h5>
                    <i class="italica"><?php echo @$job_history->designation_name;?></i>
                </div>
                <div class="row2">
                <a href="human_resource/view_employee_transfer"><span class="btn green">Employe Transfer</span></a>
                </div>
            </div>
			<table cellspacing="0">
				<thead class="table-head">
					<!--<td>Employee Name</td>-->
					<td>Department</td>
					<td>City</td>
					<td>Branch</td>
					<td>Transfer Location</td>
					<td>Shift</td>
                    <td>Posting Date</td>
                    <td>Posting End Date</td>
                    <td>Duration</td>
					<td>Approved By</td>
					<td>Approved Date</td>
				</thead>
                <?php if(!empty($trans_details)){
					foreach($trans_details as $trans){ ?>
			<tr class="table-row">
					<?php /*?><td><?php echo $trans->full_name?></td><?php */?>
					<td><?php echo $trans->department_name?></td>
					<td><?php echo $trans->city_name?></td>
					<td><?php echo $trans->branch_name?></td>
					<td><?php echo $trans->posting_location?></td>
					<td><?php echo $trans->shift_name?></td>
					<td><?php echo date_format_helper($trans->poting_start_date);?></td>
					<td><?php echo date_format_helper($trans->posting_end_date);?></td>
					<td><?php echo $trans->duration;?></td>
                    <td><?php echo $trans->emp_name?></td>
                     <td><?php if($trans->date_approved == 0000-00-00){echo"Pending";}else {echo date_format_helper($trans->date_approved);}?></td>
					<?php /*?><td><?php echo $trans->posting_reason?></td><?php */?>
				</tr>
                <?php } }else{ ?>
                <tr><td colspan="7"><?php echo "No Record Found ... !";?></td></tr>
                <?php } ?>
			</table>

		</div>

	</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->