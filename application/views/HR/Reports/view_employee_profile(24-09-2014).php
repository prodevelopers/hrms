<!-- contents -->

<div class="contents-container">
<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
	<div class="bredcrumb">Dashboard / Human Resource</div> 

	<?php $this->load->view('includes/hr_left_nav'); ?>

	<div class="right-contents">

		<div class="head">Complete Details<a href="#"><span class="fa fa-print" style="float:right; padding-right:40px;padding-top:5px; color:#fff;"></span></a></div>

			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="5">Personal Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td rowspan="4" width="150"><img src="upload/<?php echo @$complete_profile->photograph; ?>" width="150" height="150"></td>
					<td>Employee Name</td>
					<td><?php echo @$complete_profile->full_name; ?></td>
					<td>Employee ID</td>
					<td><?php echo @$complete_profile->employee_code; ?></td>
				</tr>
				<tr class="table-row">
					<td>Father Name</td>
					<td><?php echo @$complete_profile->father_name; ?></td>
					<td>CNIC</td>
					<td><?php echo @$complete_profile->CNIC; ?></td>
				</tr>
				<tr class="table-row">
					<td>Martial Status</td>
					<td><?php echo @$complete_profile->marital_status_title; ?></td>
					<td>Date Of Birth</td>
					<td><?php echo @$complete_profile->date_of_birth; ?></td>
				</tr>	
				<tr class="table-row">
					<td>Nationality</td>
					<td><?php echo @$complete_profile->nationality; ?></td>
					<td>Religion</td>
					<td><?php echo @$complete_profile->religion_title; ?></td>
				</tr>		
				</tbody>	
			</table>
			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="4">Contact Information (Current)</td>
				</thead>
				<tbody>

				<tr class="table-row">
					<td>Current address</td>
					<td><?php echo @$complete_profile->address; ?></td>
					<td>Telephone</td>
					<td><?php echo @$complete_profile->home_phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php echo @$complete_profile->mob_num; ?></td>
					<td>Email</td>
					<td><?php echo @$complete_profile->email_address; ?></td>
				</tr>
				<tr class="table-row">
					<td>Work Telephone</td>
					<td><?php echo @$complete_profile->office_phone; ?></td>
					<td>Work Email</td>
					<td><?php echo @$complete_profile->official_email; ?></td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Permanent)</td>
				</thead>
				<tr class="table-row">
					<td>Permanent address</td>
					<td><?php echo @$complete_profile->address; ?></td>
					<td>Telephone</td>
					<td><?php echo @$complete_profile->phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>City</td>
					<td><?php echo @$complete_profile->city_name; ?></td>
					<td>District</td>
					<td><?php echo @$complete_profile->district_name; ?></td>
				</tr>
				<tr class="table-row">
					<td>Province</td>
					<td><?php echo @$complete_profile->province_name; ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Emergency)</td>
				</thead>
				<tr class="table-row">
					<td>Emergency Contacts</td>
					<td><?php echo @$complete_profile->cotact_person_name; ?></td>
						<td>Telephone</td>
					<td><?php echo @$complete_profile->home_phone; ?></td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Relationship</td>
					<td><?php echo @$complete_profile->relation_name; ?></td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Educational Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Degree</td>
					<td>Bs(Computer Science)</td>
					<td>Institiute</td>
					<td>IBMS/University Of Agriculture</td>
				</tr>
				<tr class="table-row">
					<td>Specialization</td>
					<td>Web Engineer</td>
					<td>GPA/Score</td>
					<td>3.55</td>
				</tr>
				<tr class="table-row">
					<td>Year Of Completion</td>
					<td>2012</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Experiance</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Job Title</td>
					<td>Head-Officer</td>
					<td>Organization</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
					<td>Employement Type</td>
					<td>Full Time</td>
					<td>Start Date</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>End Date</td>
					<td>12-08-2000</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Skills</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Skill Name</td>
					<td>Web-Designer</td>
					<td>Experiance in Years</td>
					<td>Company name</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Employement Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Current Job Title</td>
					<td>Head-Officer</td>
					<td>Job Specification</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
					<td>Employment Type</td>
					<td>Full Time</td>
					<td>Job Category</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>Department</td>
					<td>12-08-2000</td>
					<td>City</td>
					<td>12-08-2000</td>
				</tr>
				<tr class="table-row">
					<td>Location</td>
					<td>12-08-2000</td>
					<td>Work Shift</td>
					<td>12-08-2000</td>
				</tr>
				<tr class="table-row">
					<td>Contract Expiry Date</td>
					<td>12-08-2000</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Dependents</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td style="none;"><strong>Relationship</strong></td>
					<td>Date Of Birth</td>
				</tr>
				<tr class="table-row">
					<td>12-08-1992</td>
					<td>Company name</td>
					<td>Head-Officer</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Pay Package</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Pay Frequency</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
				</tr>
				<tr class="table-row">
					<td>Currency</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Payrate</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
				</tr>	
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Entitlements</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Increments</td>
					<td><?php echo @$complete_profile->increment_amount; ?></td>
					<td>Increments Type</td>
					<td><?php echo @$complete_profile->increment_type; ?></td>
				</tr>
				<tr class="table-row">
					<td>Benefits</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Benefits Type</td>
					<td><?php echo @$complete_profile->benefit_type_title; ?></td>
				</tr>
				<tr class="table-row">
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Allowance Type</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
				</tr>
				<tr class="table-row">
					<td>Leaves</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
					<td>Leave Type</td>
					<td><?php echo @$complete_profile->mobile_num; ?></td>
				</tr>
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Report To</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td><?php echo @$complete_profile->full_name; ?></td>
					<td>Reporting Method</td>
					<td><?php echo @$complete_profile->reporting_option; ?></td>
				</tr>
				
				</tbody>
			</table>
	</div>