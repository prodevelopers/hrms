<style>
    #pencil:hover{
        cursor: pointer;
    }
</style>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Transfer</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Employee Info</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php echo  $this->session->flashdata('msg');?></div>
            <div class="form-right">
                <div class="row2">
                    <h5 class="headingfive">Employee Name</h5>
                    <i class="italica" ><?php echo @$job_history->full_name;?></i>
                </div>
                <div class="row2">
                    <h5 class="headingfive">Employee ID</h5>
                    <i class="italica"><?php echo @$job_history->employee_code;?></i>
                </div>
                <div class="row2">
                    <h5 class="headingfive">Designation</h5>
                    <i class="italica"><?php echo @$job_history->designation_name;?></i>
                </div>
            </div>
           	<br class="clear">
				<br class="clear">
        <div class="head">Transfer</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="employment_id" value="<?php echo @$employment_id->employment_id?>" required="required"  />

        <div class="row">
            <h4>City</h4>
            <?php echo @form_dropdown('ml_city_id', $city, 'required="required"', 'id="drop_city"'); ?>
            <a id="city"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>
		<div class="row">
			<h4>Duration</h4>

			<select name="duration" required="required">
				<option> --Select Month -- </option>
                <option value="1">1 Month</option>
                <option value="2">2 Month</option>
                <option value="3">3 Month</option>
                <option value="4">4 Month</option>
                <option value="5">5 Month</option>
                <option value="6">6 Month</option>
                <option value="7">More Than 6 Month</option>
            </select>
		</div>


		<div class="row">
					<h4>Branch</h4>
                    <?php echo @form_dropdown('ml_branch_id', $branch,' required="required"','id="branches" required="required"'); ?>
                    <a id="branch"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>
        <div class="row">
					<h4>Work Location</h4>
                    <?php echo @form_dropdown('location', $location,' required="required"','id="drop_loc" required="required"'); ?>
            <a id="loc"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Shift</h4>
                    <?php echo @form_dropdown('shift', $work_shift,' required="required"','id="workShift" required="required"'); ?>
                    <a id="ws"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
                </div>
				<br class="clear">
				<div class="row">
					<h4>Department</h4>
                    <?php echo @form_dropdown('ml_department_id', $department,' required="required"','id="drop_dept" required="required"'); ?>
                    <a id="dep"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Reason of Transfer</h4>
                    <?php echo @form_dropdown('reason_transfer', $reason_transfer,'required="required"','id="TransferId" required="required"'); ?>
                    <a id="Transfer"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<br class="clear">
				<div class="row">
					<h4>Remarks</h4>
					<textarea name="transfer_remarks" ></textarea>
				</div>

				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_transfer" value="Transfer" class="btn green" />

				</div>
			</div>
            <?php echo form_close(); ?>
            
			<table cellspacing="0">
				<thead class="table-head">
					<!--<td>Employee Name</td>-->
					<td>City</td>
					<td>Branch</td>
					<td>Current Location</td>
					<td>Shift</td>
					<td>Department</td>
					<td>Reason Of Transfer</td>
                    <td>Approved By</td>
					<td>Approved Date</td>
					<td>Posting Date</td>

				</thead>
                <?php if(!empty($trans_details)){
					foreach($trans_details as $trans){ ?>
			<tr class="table-row">
					<!--<td><?php //echo $trans->full_name?></td>-->
					<td><?php echo $trans->city_name?></td>
					<td><?php echo $trans->branch_name?></td>
					<td><?php echo $trans->posting_location?></td>
					<td><?php echo $trans->shift_name?></td>
					<td><?php echo $trans->department_name?></td>
					<td><?php echo $trans->posting_reason?></td>
                    <td><?php echo $trans->emp_name;?></td>
					<td><?php if($trans->date_approved == 0000-00-00){echo"Pending";}else {echo date_format_helper($trans->date_approved);}?></td>
					<td><?php echo date_format_helper($trans->poting_start_date);?></td>
                   <!-- <td><?php /*echo $trans->posting_end_date*/?></td>-->
				</tr>
                <?php } } ?>
			</table>

		</div>

	</div>
	</div>
<!-- contents -->

<!-- City Dialog-->
<div id="city-pop" title="Add City" style="display:none; width:600px;">
    <form id="cityForm" action="human_resource/addCityEdit/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="city_name" id="txt_city"/>
            <br><br>
            <input type="submit" value="Add" id="CITYID" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- Branch -->
<div id="Branch_d" title="Add Branch" style="display:none; width:600px;">
    <form id="Brnh">
        <div class="data">
            <input type="text" class="text_field" name="add_brach" id="branch_b"/>
            <br><br>
            <input type="submit" value="Add" id="BRANCH" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- Location dialog -->
<div id="location" title="Add Location" style="display:none; width:600px;">
    <form id="locForm" action="human_resource/AddPostingLocationTransfer/<?php echo @$employee->employee_id; ?>"
          method="post">
        <div class="data">
            <input type="text" class="text_field" name="posting_location" id="txt_loc"/>
            <br><br>
            <input type="submit" value="Add" id="LOCATIONId" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- Work Shift -->
<div id="work_d" title="Add Work Shift" style="display:none; width:600px;">
    <form id="wsf">
        <div class="data">
            <input type="text" class="text_field" name="add_ws" id="ws_b"/>
            <br><br>
            <input type="submit" value="Add" id="WORKID" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- designaiton dialog -->
<div id="department" title="Add Department" style="display:none; width:600px;">
    <form id="deptForm" action="human_resource/AddDepartmentTransfer/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="department_name" id="txt_dept"/>
            <br><br>
            <input type="submit" value="Add" id="DEPARTMENT" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Transfer Of Reason dialog -->
<div id="TransferReason" title="Add Transfer Reason" style="display:none; width:600px;">
    <form id="transForm" action="human_resource/AddReasonTransfer/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="transfer" id="txt_trasnfer"/>
            <br><br>
            <input type="submit" value="Add" id="TransferReasonId" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<script src="<?php echo base_url() ?>assets/js/edit-dialogs.js"></script>

<script>
    $(document).ready(function () {

        ////// Function For Add City Using Ajax ///
       $("#CITYID").on('click', function (e) {

        e.preventDefault();
        var formData = {
            CityName: $("#txt_city").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/addCityEdit/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var provinceNameEnterdValue = $('#txt_city').val();
                    var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                    $('#drop_city').append(appendData);
                    $("#city-pop").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });
        ///END


        //Pay Grade
        $("#BRANCH").on('click', function (e) {
            e.preventDefault();
            var formData = {
                AddBranch: $("#branch_b").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/AddBranchTransferEmployee/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var JobCatEnterdValue = $('#branch_b').val();
                        var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                        $('#branches').append(appendData);
                        $("#Branch_d").dialog("close");
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });

        //Location
        $("#LOCATIONId").on('click', function (e) {
            e.preventDefault();
            var formData = $('#locForm').serialize();
            $.ajax({
                url: "human_resource/AddPostingLocationTransfer/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var provinceNameEnterdValue = $('#txt_loc').val();
                        var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                        $('#drop_loc').append(appendData);
                        $("#location").dialog("close");
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });

        //Work shift
        $("#WORKID").on('click', function (e) {
            e.preventDefault();
            var formData = {
                WorkShift: $("#ws_b").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/AddWorkShiftTransfer/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var WsEnterdValue = $('#ws_b').val();
                        var appendData = '<option value="' + data[3] + '">' + WsEnterdValue + '</option>';
                        $('#workShift').append(appendData);
                        $('#work_d').dialog('close');
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });

        // Function For Reason Of Transfer Master List Pencil Using Ajax
        $("#TransferReasonId").on('click', function (e) {
            e.preventDefault();
            var formData = {
                TransferName: $("#txt_trasnfer").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/AddReasonTransfer/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var provinceNameEnterdValue = $('#txt_trasnfer').val();
                        var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                        $('#TransferId').append(appendData);
                        $("#TransferReason").dialog("close");
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });


        });

        //Department

        $("#DEPARTMENT").on('click', function (e) {
            e.preventDefault();
            var formData = {
                DeptName: $("#txt_dept").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/AddDepartmentTransfer/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var provinceNameEnterdValue = $('#txt_dept').val();
                        var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                        $('#drop_dept').append(appendData);
                        $("#department").dialog("close");
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });


        });

    });


</script>
<script>
	$("#alert").delay(3000).fadeOut('slow');
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->