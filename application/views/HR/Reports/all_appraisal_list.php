<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	font-size: 0px;
}
.dataTables_length select{
	width: 100px;
    float:right;
    margin-right:11px;
}
.fa-pencil:hover{cursor: pointer}
.dataTables_filter{
	position: relative;
	top: -21px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}

    /*tooltip css*/
[title]{
    position:relative;

}
[title]:after{
    content:attr(title);
    color:#000000;
    font-family: arial, sans-serif;
    background:#b0fbf1;
    padding:6px;
    position:absolute;
    left:-9999px;
    border-radius: 5px;
    opacity:0;
    bottom:100%;
    white-space:nowrap;
    -webkit-transition:0.25s linear opacity;
}
[title]:hover:after {
    left: 5px;
    opacity: 1;

}
#table_list{width:auto important;}


/*Add Employee Green Button*/
.btn-right {
    float: right;
    height: 25px;
    margin-top: -6px;
    width: auto;
}

/*tooltip css*/
</style>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
$(document).ready(function() {
    oTable ='';
    var employeeListTableSelector = $('#table_list');
    var url_DT = "<?php echo base_url(); ?>human_resource/all_appraisal_list/list";
    var aoColumns_DT = [
        /* Name of The Employee Who Broke/Violated Company Discipline/Rule */ {
            "mData" : "employee_code"
        },
        /* Discipline That Have Been Breached/Violated By Employee */ {
            "mData" : "full_name",
            "bSearchable": true
        },
        /* Breach Date/Date of Occurrence */ {
            "mData" : "designation_name"
        },
        /* Disciplinary Action */ {
            "mData" : "posting_location"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "setup"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "setup"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "setup"
        }

    ];
    var HiddenColumnID_DT = '';
    var sDom_DT = '<"H"r>lt<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    var designation = $("#design").val();
    if(designation.length === 0){
        designation = 0;
    }
    var filters = 'aoData.push({"name":"designation","value":'+designation+'});';
    commonDataTablesFiltered(employeeListTableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    $('tbody').addClass("table-row");

    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
//Page Messages
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
        if(!empty($message) && isset($message)){
                echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0],data[1]);

    <?php
    }
    }
}
?>
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});
	/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");		
	}
</script>
<!-- contents -->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Human Resource / Appraisal Management</div> <!-- Bread Crumbs -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head">Appraisal Management
<!--        <div class="btn-right">-->
<!--            <a href="human_resource/add_employee_acct/" class="btn green">Add Appraisal</a>-->
<!--        </div>-->
        </div>
		<!-- filter -->
		<div class="filter">
			<h4>Filter By</h4>
            <?php echo form_open();?>
            <input type="text" id="tableSearchBox" placeholder="Search Employees" style="width: 30%;">
			<?php echo form_dropdown('designation', $designation, $design, "class='resize' id='design' onChange='this.form.submit()'"); ?>
                <?php echo form_close();?>
			</div>
			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list" class="marge ">
                <div class="table-width">
				<thead class="table-head">
					<td width="100">EmployeeID</td>
					<td>Employee Name</td>
					<td>Designation</td>
                    <td>Location</td>
					<td>Year</td>
					<td>Q1</td>
					<td>Q2</td>
					<td>Q3</td>
					<td>Annual</td>
				</thead>
                    </div>
              <tbody>
				</tbody>
			</table>
		</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
        $(document).ready(function(e){
            $('#dept, #design, #project').select2();
        });
    </script>
    <!-- leftside menu end -->

