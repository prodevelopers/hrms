<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Qualification Report </div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Qualification Reports</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Qualification">
				<select>
					<option>Select By</option>
				</select>

				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>
		<hr>
	<div style="width: 50%; margin:20px auto;">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["BS","MS","B.Tech","FA","FSc","Matric","Degree"],
		datasets : [
			
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>
		</div>
	</div>
</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/reports_list_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->