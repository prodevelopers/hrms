
<div class="contents-container">
<div class="bredcrumb">Dashboard / Human Resource / Assign Task</div> <!-- bredcrumb -->
<style type="text/css">
   .ciruler{
    border:thin solid #ccc;
    padding:0.5em 0 0.5em 0;
    margin-bottom:0.5em;
    min-height:100px;
    overflow:hidden;
   }
   #alert_t{background-color: #dc143c; color: #ffffff;}
</style>
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
    <div class="right-contents">
		<div class="head">Assign Task</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php echo  $this->session->flashdata('msg');?></div>
        <div class="form-left"> 
        <h4 class="head">Task Information</h4>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee_id; ?>" />

        	<div class="row2">
                <div id="warning"><?php echo $this->session->flashdata('warning');?></div>
					<h4>Project</h4>
				<select name="project_id" id="project_id" onchange="populate_tasks()" required="required">
                    <option value="">-- Select Project --</option>
                    <?php
                    if(!empty($projects)){
                    foreach($projects as $project){?>
					<option value="<?php echo $project->project_id;?>"><?php echo $project->project_title;?></option>
				<?php }}else{}?>
                </select>
					<?php //echo @form_dropdown('project_title', $projects); ?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Task</h4>
                    <select name="ml_assign_task_id" id="tasks" disabled="disabled" required="required">
                        <option>Select Task</option>
                  </select>
                    <?php //echo @form_dropdown('ml_assign_task_id', $tasks,'','required="required" disabled="disabled" id="tasks"');?>
                    <a id="task"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
                </div>
				<br class="clear">
				<div class="row2">
					<h4>Start Date</h4>
					<input type="text" id="start_date" name="start_date" style="width:235px">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Completion Date</h4>
					<input type="text" id="completion_date" name="completion_date" style="width:235px">
				</div>
                <br class="clear">
                <div class="row2">
                    <h4>Task / Project Description</h4>
                    <textarea name="task_description" rows="7" cols="33"></textarea>
                </div>
                <br class="clear">
				<br class="clear">
                <h2 class="head">Personal KPI's</h2>
				<div class="row2">
				<h4>Key Performance Indicators</h4>
                    <select multiple name="ml_kpi_type_id[]" id="kpis" disabled="disabled" required="required">
                        <option>Select kpi</option>
                    </select>
				<?php //echo @form_multiselect('ml_kpi_type_id[]',$kpi,'','id="kpis" disabled="disabled"'); ?>
				<div class="tipping">
	                <p>
	                Hold Down the Ctrl Button to Select Multiple Options.
	                </p>
                </div>
				</div>
				<!--<br class="clear">
				<div class="row2">
					<h4>Status</h4>
					<select name="status">
                    	<option value="-1">-- Select Task Status --</option>
                    	<option value="In Progress">In Progress</option>
                    	<option value="Stopped">Stopped</option>
                    	<option value="Completed">Completed</option>
                    </select>
				</div>-->
                <br class="clear">
                <br class="clear">
                <h2 class="head">MileStone</h2>
                <div class="row2 input_fields ">
                    <div class="row2">
                    <h4 id="milestone">Mile Stone</h4>
                      <input type="text" name="milestones[]" style="width:235px">
                     </div>
                    <br class="clear">
                    <div class="row2">
                        <h4>Estimate Completion Date</h4>
                          <input type="text" id="estimate" name="estimate_date[]" class="estimate_date" style="width:235px" >
                    </div>
                    <br class="clear">
                    <div class="row2">
                        <h4>Actual Completion Date</h4>
                          <input type="text" id="actuals" name="actual_date[]" class="actual_date" style="width:235px">
                    </div>
                    <br class="clear">
                    <div class="row2">
                        <h4>Remarks</h4>
                          <textarea name="remarks[]" id="" cols="33" rows="7"></textarea>
                        <input type="hidden" name="employment" value="<?php echo $employment->employment_id;?>"/>
                    </div>
                </div>
                  <br class="clear">
                <div class="row2">
                    <h4>&nbsp;</h4>
                        <button type="button" class="btn green add_field"><span class="fa fa-plus"></span></button>
                        <button type="button" class="btn green rem_field"><span class="fa fa-minus"></span></button>
                </div>
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Assign Task" class="btn green" />
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
            <div class="form-right">
            <table class="table">
            	<thead class="table-head">
            		<td colspan="2">Employee Info</td>
            	</thead>
            	<tbody>
            		<tr class="table-row">
            			<td><h5 class="headingfive">Employee Name</h5></td>
            			<td><?php echo @$job_history->full_name;?></td>
            		</tr>
            		<tr class="table-row">
            			<td><h5 class="headingfive">Employee ID</h5></td>
            			<td><?php echo @$job_history->employee_code;?></td>
            		</tr>
            		<tr class="table-row">
            			<td><h5 class="headingfive">Designation</h5></td>
            			<td><?php echo @$job_history->designation_name;?></td>
            		</tr>
            		<tr class="table-row">
            			<td><h5 class="headingfive">Skills</h5></td>
            			<td><?php echo @$job_history->skill_name;?></td>
            		</tr>
            		<tr class="table-row">
            			<td><h5 class="headingfive">Experience In Years</h5></td>
            			<td><?php echo @$job_history->experience_in_years."Years";?></td>
            		</tr>
                  
            	</tbody>
            </table>  
            </div>
            <!-- Right Side Historic Detail -->
            <table cellspacing="0" cellpadding="0">
				<thead class="table-head">
					<td>Project Name</td>
					<td>Task</td>
					<td>Start Date</td>
					<td>Completion Date</td>
					<td>Key Performance Indicators</td>
					<td>Edit</td>
					<td>Status</td>
               <!--<td><span class="fa fa-pencil"></span></td>-->
                    <td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php
				if(!empty($project_details)){
					foreach($project_details as $pd){ ?>
				<tr class="table-row">
					<td><?php echo $pd->project_title; ?></td>
					<td><?php echo $pd->task_name; ?></td>

					<td><?php echo date_format_helper($pd->start_date); ?></td>
					<td><?php echo date_format_helper($pd->completion_date); ?></td>
					<td><?php echo $pd->kpi;?></td>
                    <td><a href="human_resource/edit_assign_job/<?php echo $employee_id;?>/<?php echo $pd->assign_job_id; ?>"> <span class="fa fa-pencil"></span></a></td>
					<td><?php echo $pd->status;?></td>
                   <!-- <td><span class="fa fa-pencil"></span></td>-->
                    <td><a href="human_resource/delete_assign_job/<?php echo $employee_id;?>/<?php echo $pd->assign_job_id; ?>" onclick="return confirm('Are you Sure..!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>	
	</table>

	</div>
  </div>
</div>
<!-- contents -->

<script src="<?php echo base_url() ?>assets/js/edit-dialogs.js"></script>
<!-- Gender dialog popup -->
<div id="dialog" title="Add Task" style="display:none; width:600px;">
    <form id="taskForm" action="human_resource/add_task" method="post">
        <div class="data">
            <input type="text" class="text_field" name="project_task" id="txt_task"/>
            <input type="hidden" name="employee_id" id="employee_id" value="<?php echo @$employee_id;?>">
            <br><br>
            <input type="submit" value="Add" class="btn green addedto" name="add" id="add">
        </div>
    </form>
</div>
<!------End ------->
<!---- alert----->
<div id="alert_t" style="display: none"><p>Please Select Project first !</p>
    <br><br>
<button id="close" onclick="close_dialog();" style="background-color: #008000; width: 80px; border-radius: 7px; border-color: #008000;">Ok</button>
</div>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
/*        $(document).ready(function()
        {
            $("#tasks").attr('disabled="disabled"');
        });*/

    /////// Function for Populate Tasks According to Project
        function populate_tasks()
        {
            var proj_id=$("#project_id").val();

            $.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>human_resource/task_populate",
                dataType:"JSON",
                data:{project_id:proj_id},
                success:function(result){
                    $('#tasks').removeAttr("disabled");
                    $('#tasks').text('');
                    $.each(result,function(ml_assign_task_id,task_name){

                    var opt=$('<option/>');
                        opt.val(ml_assign_task_id);
                        opt.text(task_name);
                        $('#tasks').append(opt);

                    });
                    populate_kpi();
                }
            });
        }

    /////// Function for Populate Tasks According to Project
    function populate_kpi()
    {
        var proj_id=$("#project_id").val();

        $.ajax({
            type:"POST",
            url:"<?php echo base_url(); ?>human_resource/kpi_populate",
            dataType:"JSON",
            data:{project_id:proj_id},
            success:function(result){
                $('#kpis').removeAttr("disabled");
                $('#kpis').text('');
                $.each(result,function(ml_kpi_id,kpi){
                    var opt=$('<option/>');
                    opt.val(ml_kpi_id);
                    opt.text(kpi);
                    $('#kpis').append(opt);
                });
            }
        });
    }



//End of populate_tasks Function
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
    $("#warning").delay(3000).fadeOut('slow');
</script>
<script>
	$(document).ready(function(){

	var max_fields = 20; //maximum input boxes allowed
    var wrapper    = $(".input_fields"); //Fields wrapper
    var add_button = $(".add_field"); //Add button ID
    var rem_button = $(".rem_field"); //Add button ID
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="clear"></div>' +
            '<div class="ciruler" id="'+(x-1)+'"><div class="row2">'
                    +'<h4 id="milestone">Mile Stone</h4>'
                    +'<input type="text" name="milestones[]" style="width:235px">'
                    +'</div>'
                     +'<br class="clear">'
                     +'<div class="row2">'
                        +'<h4>Estimate Completion Date</h4>'
                           +'<input type="text" name="estimate_date[]" class="estimate_date" style="width:235px">'
                    +'</div>'
                     +'<br class="clear">'
                    +'<div class="row2">'
                        +'<h4>Actual Completion Date</h4>'
                           +'<input type="text" name="actual_date[]" class="actual_date" style="width:235px">'
                    +'</div>'
                    +'<br class="clear">'
                    +'<div class="row2">'
                        +'<h4>Remarks</h4>'
                           +'<textarea name="remarks[]" id="" cols="33" rows="7"></textarea>'
                     +'</div>');}

                });

    $(rem_button).click(function(e){ // on add input button click
        e.preventDefault();
        if(x < max_fields){ // max input box allowed
           // text box increment
           $(wrapper).find('div[id="'+(x-1)+'"]').remove(); //add input box 
           x--;  
        }
    });

});
function close_dialog()
{
    $("#alert_t").dialog("close");
}
    $(document).ready(function()
    {
        $("#add").on('click',function(e){
            e.preventDefault();
            var formData = $('#taskForm').serialize();
            var proj_id=$("#project_id").val();
            if(proj_id === "")
            {$("#alert_t").dialog();
            $("#dialog").dialog("close");
             return;
               }
            var task=$("#txt_task").val();
            var employee_id=$("#employee_id").val();
           $.ajax({
                url: "human_resource/add_task",
                data:{
                    project_id:proj_id,
                    tasks:task,
                    employee_id:employee_id
                },
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                   var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        var provinceNameEnterdValue = $('#txt_task').val();
                        var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
                        $('#tasks').append(appendData);
                        $("#dialog").dialog("close");
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });


        });

    });
</script>
<script>
    //Little Fix for Dynamically Created Input Boxes..
    $('body').on('focus','.actual_date, .estimate_date',function(e){
       console.log('Hello World');
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            timeFormat: "HH:MM",
            changeMonth: true,
            changeYear: true
        });
    });
</script>
<script>
    $( "#start_date,#completion_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });



</script>
<?php $msg=$this->session->flashdata("update");
if(!empty($msg)){
    echo "<span id='msgs' style='display: none'>".$msg."</span>";
    ?>
    <script>
        var msg =$("#msgs").val();
        var data = msg.split('::');
        if(data[0] === "OK"){
            Parexons.notification(data[1],data[2]);
        }
    </script>
<?php }else{echo $msg;}?>