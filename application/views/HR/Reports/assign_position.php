<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?><!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Manage Position</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Manage Position</div>
	<div class="form-left">
		 <?php echo form_open();?>
				<div class="row2">
					<h4>Pay Grade</h4>
                    <?php  echo form_dropdown('pay_grade', $paygrade,'required="required"','required="required"'); ?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Designation</h4>
                    <?php echo form_dropdown('designation_name', $designation,'required="required"','required="required"'); ?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Job Specification</h4>
                    <input type="text" name="job_specifications"  />
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Date (Effective From)</h4>
					<input type="text" name="from_date" id="dateEffective" value="<?php echo date_format_helper(date('Y-m-d'));?>" readonly required="required">
				</div>
            	<div class="row2">
					<div class="button-group">
						<input type="submit" name="add" value="Manage Position" class="btn green" />
						<input type="button" class="btn green" value="View Performance" onclick="return View_performance();"/>
					</div>
				</div>
				<?php echo form_close();?>
                </div>
            <!-- Right Side Historic Detail -->
            	<div class="form-right">

                        <table class="table">
                        	<thead class="table-head">
                        		<td colspan="2">Current Position</td>
                        	</thead>
                        	<tbody>
                        		<tr class="table-row">
                        			<td><h5 class="headingfive">Employee Name</h5></td>
                        			<td><?php echo @$mp_history->full_name;?></td>
                        		</tr>
                        		<tr class="table-row">
                        			<td><h5 class="headingfive">Designation</h5></td>
                        			<td><?php echo @$join->designation_name;?></td>
                        		</tr>
                        		<tr class="table-row">
                        			<td valign="top"><h5 class="headingfive">Pay Grade</h5></td> 
                        			<td><?php if(!empty($join)):
											echo @$join->pay_grade;
										endif;?>
									</td>
                        		</tr>
                        		<tr class="table-row">
                        			<td valign="top"><h5 class="headingfive">Job Specification</h5></td>
                        			<td> 
                        			<?php if(!empty($join)):
									echo @$join->job_specifications;
									endif;?>
									</td>
                        		</tr>
                        	</tbody>
                        </table>
          		</div>
            <!-- Right Side Historic Detail -->
<table cellspacing="0">
				<thead class="table-head">
					<td>Pay Grade</td>
					<td>Designation</td>
					<td>Job Specification</td>
					<td>Date From</td>
					<td>Date To</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
				</thead>
                <?php if (!empty($mp_history_wd)){?>
			<?php foreach($mp_history_wd as $record):
			//echo "<pre>"; print_r($join); die;
			?>
				<tr class="table-row">
					<td><?php echo $record->pay_grade; ?></td>
					<td><?php echo $record->designation_name; ?></td>
					<td><?php echo $record->job_specifications; ?></td>
					<td><?php echo date_format_helper($record->from_date); ?></td>
					<td><?php echo date_format_helper($record->from_date); ?></td>
					<td><?php echo $record->emp_name; ?></td>
					<td><?php echo date_format_helper($record->approval_date); ?></td>
					<td><?php echo $record->status_title; ?></td>
					<td><?php if($record->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_pend_rec/<?php echo $record->position_id;?>/<?php echo $mp_history->employee_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<!--<td><?php /*if($record->status == 1){ echo '<a href="human_resource/edit_pend_rec/<?php echo $record->position_id; ?>/<?php echo @$mp_history->employee_id;?>" ><span class="fa fa-pencil"></span></a>'; }else{echo"&nbsp;";}*/?></td>-->
				</tr>
				  <?php endforeach; 
            	 } else { ?> 
			<td colspan="6"><?php echo " <span style='color:#F00'>No record found..</span>";?> </td>
			<?php }?> 
			</table>
		</div>

	</div>
<!-- contents -->
<script>
    $(document).ready(function(){
        <?php $success=$this->session->flashdata('alt');
         $warn=$this->session->flashdata('msg');
        if(!empty($success)){?>
	$("#sucess").show().delay(3000).fadeOut('slow');
        <?php }
        if(!empty($warn)){?>
	$("#alert").show().delay(3000).fadeOut('slow');
            <?php }?>
    });
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $( "#from_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });


    function View_performance(id) {
        var id = '<?php echo isset($mp_history->employee_id) ? $mp_history->employee_id: ""; ?>';
        window.open('human_resource/view_kpi_perf_eval/' + id, "", "width=2000,height=2000");
    }

    </script>
    <!-- leftside menu end -->
<script>
    $(document).ready(function(e) {
        //Page Notifications If Any..
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0], data[1]);
        <?php
        }
        }
    }
    ?>
    });

	$('#dateEffective').datepicker({
		dateFormat: "dd-mm-yy",
		changeMonth: true,
		changeYear: true
	});

</script>