<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.resize{
	width: 220px;
}
</style>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head">Task Progress</div>
<!-- Right Side Historic Detail -->
	<div class="form-right">
		<table class="table">
			<thead class="table-head">
				<td colspan="2">Employee Info</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee Name</h5></td>
					<td><?php echo @$employee_info->full_name;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee ID</h5></td>
					<td><?php echo @$employee_info->employee_code;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Designation</h5></td>
					<td><?php echo @$employee_info->designation_name;?></td>
				</tr>
			</tbody>
		</table>
	</div>
            <!-- Right Side Historic Detail -->
            <br class="clear" />

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open();?>
                <div>
                <select name="task"  id="task"  class='resize' onchange="this.form.submit();">
                    <option>Select Task</option>
                        <?php if(!empty($tasks)){
                            foreach($tasks as $task){?>
                                <option value="<?php echo $task->ml_assign_task_id;?>"><?php echo $task->task_name;?></option>
                        <?php }}else{echo "<span style='color: #ff0000'>Sorry no record found !</span>";}?>
                </select>
                <?php echo form_close();?>
                    </div>
			</div>
            <br class="clear" />
			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>Milestone</td>
					<td>Tasks</td>
					<td>Estimated Completion Date</td>
                    <td>Reported Date</td>
                    <td>Reported By</td>
					<td>Progress</td>
					<td>Remarks</td>
					<td>Action</td>
				</thead>
                <?php if(!empty($task_info)){
                    foreach($task_info as $info){?>
                <tr class="table-row">
                	<td><?php echo $info->milestones;?></td>
                	<td><?php echo $info->task_name;?></td>
                	<td><?php echo date_format_helper($info->estimated_date);?></td>
                    <td><?php echo date_format_helper($info->reported_date);?></td>
                    <td><?php echo @$info->name; ?></td>
                    <td><div class="meter orange"><span style="width:<?php if($info->percent_achieved > 0) {echo $info->percent_achieved.'%';}else{echo '0%';}?>" title="<?php if($info->percent_achieved > 0) { echo $info->percent_achieved.'%';}else{echo '0%';}?>"><?php if($info->percent_achieved > 0){echo @$info->percent_achieved."%";}else{echo "0%";}?></span></div></td>
					<td><?php echo @$info->remarks;?></td>
					<td><?php if($info->percent_achieved  != 100 || $info->percent_achieved < 100){?>
                        <a href="<?php echo base_url();?>human_resource/task_eval/<?php echo $info->employee_id;?>/<?php echo $info->id;?>/<?php echo $info->tp_id;?>" class="btn green">Progress</a>
                        <?php }else{?><a  class="btn green" style="background: darkgray;" disabled="true">Progress</a> <?php }?></td>
                </tr>
                <?php }}else{echo "<span style='color: #ff0000'>Sorry no record found !</span>";} ?>
			</table>
		</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    /////
    $(document).ready(function(e){
        $('#task').select2();
    });

    </script>
    <!-- leftside menu end -->