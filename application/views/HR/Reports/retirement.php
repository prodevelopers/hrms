<style>
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers {
        margin-top: 8px;
    }

    .dataTables_info {
        color: #3474D0;
        font-size: 14px;
        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter {
        float: right;
    }
    input[type="button"]:disabled {
        background: #dddddd;
    }
    .dataTables_length{
    position: relative;
    left: 0.2%;
    top: -20px;
    font-size: 1px;
}
.dataTables_length select{
    width: 60px;
}

.dataTables_filter{
    position: relative;
    top: -22px;
    left:-31%;
}
.dataTables_filter input{
    width: 180px;
}
.resize{
    width: 220px;
}
</style>


<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Retirement - Separation / Current Employees</div>
    <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
     <div class="right-contents">

        <div class="head">Current Employees</div>

        <!-- filter -->
        <?php echo form_open(); ?>
        <div class="filter">
            <h4>Filter By</h4>
            <input type="text" id="searchby" placeholder="Search Here" style="width: 30%;">
            <?php echo @form_dropdown('designation', $designation, $design, "class='resize' id='design' onChange='this.form.submit()'"); ?>
        </div>
        <?php echo form_close(); ?>
        <!-- table -->

        <table class="table effect " id="table_list" cellspacing="0" >
            <thead class="table-head">
            <tr id="table-row">
                <td>Employee Code</td>
                <td>Employee Name</td>
                <td>Designation</td>
                <td>Status</td>
                <td>Discharge</td>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>
<!-- contents -->


<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#table_list').dataTable({
            "bProcessing": true,
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bServerSide": true,
            "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
            "sAjaxSource": window.location + "/list",
            "bDestroy": true,
            "sServerMethod": "POST",
            "aaSorting": [[0, "asc"]],
            "fnServerParams": function (aoData, fnCallBack) {
                aoData.push({"name": "designation", "value": $('#design').val()});
            }
        });

        $('#searchby').keyup(function(){
            oTable.fnFilter( $(this).val() );
        });


        $(".filter1").change(function (e) {
            oTable.fnDraw();
        });
        $('tbody').addClass("table-row");
    });



    function doc_file() {
        var r = confirm('if you are upload this file again !\n\ please follow check out method');
        if (r == true) {
            return true;
        } else if (r == false) {
            return false;
        }
    }

    function confirm() {
        alert('Are You Sure to Delete Record ...?');
    }


</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });

    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });



    $(document).ready(function(e){
        $('#design').select2();
    });
    </script>
    <!-- leftside menu end -->