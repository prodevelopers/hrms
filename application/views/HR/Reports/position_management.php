<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url<?php echo base_url();?>(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -0.2%;
	top: -20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
		"aoColumns":[
			/* EmployeeCode*/ {
				"mData": "EmployeeCode"
			},
			/* Name of The Employee Who Broke/Violated Company Discipline/Rule */ {
				"mData" : "EmployeeName"
			},
			/* Discipline That Have Been Breached/Violated By Employee */ {
				"mData" : "Designation"
			},
			/* Breach Date/Date of Occurrence */ {
				"mData" : "JobSpecification"
			},
			/* Breach Date/Date of Occurrence */ {
				"mData" : "Status"
			},
			/* Breach Date/Date of Occurrence */ {
				"mData" : "Action"
			}
		],
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"job_title","value":$('#jobtitle').val()});
			aoData.push({"name":"designation","value":$('#design').val()});
					        
                
		}
                
	});

    $('#searchby').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });



    $('tbody').addClass("table-row");
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Position Management</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Position Management</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                 <?php echo form_open();?>
                <input type="text" id="searchby" placeholder="Search Here" style="width: 30%;">
			<?php echo @form_dropdown('designation', $designation, $design, "class='resize' id='design' onChange='this.form.submit()'"); ?>


                <?php echo form_close();?>
			</div>


			<!-- table -->
            <table class="table" id="table_list" cellspacing="0">
                   <thead class="table-head">
                   <tr id="table-row">
                    <td>Employee Code</td>   
					<td>Employee Name</td>
                    <td>Current Position</td>
					<!--<td width="20%">Current Job</td>-->
					<td>Job Specification</td>
					<!--<td width="20%">User Role</td>-->
					<td>Status</td>
				<td>Manage Position &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; View</td>
					
					
                           
                    </tr>
                </thead>
                    <tbody>
                    </tbody>
         </table>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Current Position</td>
					<td>Current Job</td>
					<td>Job Category</td>
					<td>Status</td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
			</table>-->

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="assign_position.php"><button class="btn green"></button></a>-->
					 
				</div>
			</div>
		</div>
	</div>
	</div>
<!-- contents -->


<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#design').select2();
    });

    </script>
    <!-- leftside menu end -->