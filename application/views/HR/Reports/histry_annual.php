<!-- contents -->
<?php /*?><?php 
foreach($name as $info)
{
	
	$date=$info->date;
	$year=date('Y',strtotime($date));
	//echo $year;
	//die;
}
?><?php */?>
<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Progress Report </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">
<!-- Right Side Historic Detail -->
		<div class="head">Employee Info</div>
    	<div class="form-right">
        <div class="row2">
        <h5 class="headingfive">Employee Name</h5>
        <i class="italica" ><?php echo @$name->full_name;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Employee ID</h5>
        <i class="italica"><?php echo @$name->employee_code;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Designation</h5>
        <i class="italica"><?php echo @$name->designation_name;?></i>
        </div>
	</div>
            <!-- Right Side Historic Detail -->
            <br class="clear" />
		<div class="head">Review Report</div>
        
       	<!-- filter -->
			<!--<div class="filter">
				<h4>Filter By</h4>
                <form method="post">
				<select name="data" onchange="this.form.submit()">
                <option> --Select Year-- </option>
               <?php // foreach($name as $info)
//{ echo"<option>$year</option>";}?>
				</select>
                </form>
	
			</div>-->
   

	


		<?php echo form_open(); ?>
				<div class="row">
					<h4>Employee Feedback</h4>
					<textarea name="reviewer" rows="7" cols="33" required="required" readonly="readonly"><?php echo $full->reviewer;?></textarea>
				</div>
                
				<br class="clear">
				<div class="row">
					<h4>Reviewer Comments</h4>
					<textarea name="comments" rows="7" cols="33" readonly="readonly"><?php echo $full->comments;?></textarea>
				</div>
                  <br class="clear">
				<div class="row">
					<h4>Reviewer Name</h4>
					<input type="text" value="<?php echo $exp->full_name;?>" readonly="readonly"/>
				</div>
        		<br class="clear">
				<div class="row">
					<h4>Review Date</h4>
					<input type="text"   name="submit_date" value="<?php echo $full->submit_date;?>" readonly="readonly"/>
				</div>
            <!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <a href="human_resource/history_annual/<?php echo $full->employee_id;?>"><span class="btn green">Back</span></a>
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
		
	</div>
</div>

<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->