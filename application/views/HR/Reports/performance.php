<div class="contents-container">
 
<link rel="stylesheet" href="css/bootstrap-3.1.1.min.css" type="text/css"/>
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
 
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
	<div class="bredcrumb">Dashboard / Human Resource / Performance Evaluation</div> <!-- bredcrumb -->
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect').multiselect();
});
</script>
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">
   		<!-- Right Side Historic Detail -->
		<div class="head">Performance Progress</div>
    	<div class="form-right">
        <div class="row2">
        <h5 class="headingfive">Employee Name</h5>
        <i class="italica" ><?php echo @$kpi_achive->full_name;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Employee ID</h5>
        <i class="italica"><?php echo @$kpi_achive->employee_code;?></i>
        </div>
        <div class="row2">
        <h5 class="headingfive">Designation</h5>
        <i class="italica"><?php echo @$kpi_achive->designation_name;?></i>
        </div>
	</div>
            <!-- Right Side Historic Detail -->
              <br class="clear"  />
		<div class="head"> Task Info</div>
         <table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Task</td>
					<td>Project</td>
					<td>Start Date</td>
					<td>Expected Completion Date</td>
					<td>Status</td>
				<!--	<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>-->
				</thead>
                <?php if(!empty($task)){
					foreach($task as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->task_name;?></td>
					<td><?php echo $increment->project_title;?></td>
					<td><?php echo date_format_helper($increment->start_date);?></td>
					<td><?php echo date_format_helper($increment->completion_date);?></td>
					<td><?php echo $increment->status;?></td>
					<?php /*?><td><a href="human_resource/edit_emp_single_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-trash-o"></span></a></td><?php */?>
				</tr>
                <?php } } ?>
			</table>
   <br class="clear"  />
		<div class="head"> Performance Evaluation</div>
		<?php echo form_open(); ?>
				<div class="row" style="width:98%;">
					<h4 style="width:20%">KPI</h4>
					<h4 style="width:24%">Percentage Achieved</h4>
                    <h4 style="width:26%">Evaluation Date</h4>
					<h4 style="width:25%">Remarks</h4>
				</div>
				<?php 
				$counter = 0;
					foreach($kpi_select as $select):?>
				<div class="row" style="width:48%;">
                <?php
					$selctd = (isset($select->ml_kpi_type_id) ? $select->ml_kpi_type_id : '');
					echo @form_dropdown('ml_kpi_type_id[]', $kpi, $selctd, "disabled='disabled'"); ?>
					<input type="text" name="percent_achieved[]" value="<?php echo @$select->percent_achieved; ?>">
				<input type="hidden" name="kpi[]" value="<?php echo $select->ml_kpi_type_id; ?>"  /> 
                </div>
                <div class="row" style="width:48%;">
					<input type="text" name="date_evaluated[]" class="evaluateddate"<?php echo $counter; ?> value="<?php echo date_format_helper($select->date); ?>">
					<input type="text" name="remarks[]"  value="<?php echo @$select->remarks; ?>">
                    <br class="clear">
				
                <input type="hidden" name="performance_id[]" value="<?php echo $select->performance_evaluation_id;?>"  />
                </div>
				<?php $counter++; endforeach;?>
				<!--<br class="clear">
				<div class="row">
					<h4>Evaluated By</h4>
					<input type="text" name="">
				</div>-->
				<!--<div class="row"   style="width:48%;">
					<input type="text" name="date_evaluated" id="joining_date" value="<?php //echo @$kpi_rec->date; ?>">
					<textarea name="remarks" style="height:40px;"><?php //echo @$kpi_rec->remarks; ?></textarea>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Remarks</h4>
				</div>-->
                
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="hidden" name="emp_id" value="<?php echo $this->session->userdata('employee_id');?>"  />
                <input type="submit" name="add_performance" value="Save" class="btn green" />
					<!--<a href="assign-job.html"><button class="btn green">Add Performance</button></a>
					 <button class="btn red">Delete</button> -->
				</div>
			</div>
 
		<?php echo form_close(); ?>
			
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });


    </script>
<script>
    $( ".evaluateddate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });

</script>
    <!-- leftside menu end -->