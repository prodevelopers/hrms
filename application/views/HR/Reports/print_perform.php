<base href="<?php echo base_url(); ?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
<link rel="stylesheet" type="text/css" href="assets/multslect/multiple-select.css">
<link rel="stylesheet" type="text/css" href="assets/css/form.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<script type="text/javascript" src="assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var a=confirm("Do You Want to Print . . . . !");
		if(a==true)
		{
			window.print() ;
            return false;
		}
	});
</script>
<style>
	.contents-container {
		width: 595px;
		height: 742px;
		margin: 30px auto;
	}
	.print:last-child {
		page-break-after: auto;
	}
	@media print {
		html, body {
			height: 99%;
		}
	}
	.printOnly {
		display:block !important;
	}

</style>

<!-- contents -->
<div class="contents-container">
	<div class="right-contents">
	<div class="head">Performance Evaluation</div>
<!-- Right Side Historic Detail -->
	<div class="form-right">
		<table class="table">
			<thead class="table-head">
				<td colspan="2">Employee Info</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee Name</h5></td>
					<td><?php echo isset($kpi_emp_h)?$kpi_emp_h->full_name:'';?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee ID</h5></td>
					<td><?php echo isset($kpi_emp_h)?$kpi_emp_h->employee_code:'';?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Designation</h5></td>
					<td><?php echo isset($kpi_emp_h)?$kpi_emp_h->designation_name:'';?></td>
				</tr>
			</tbody>
		</table>
	</div>
            <!-- Right Side Historic Detail -->
            <br class="clear" />
            <br class="clear" />
			<!-- table -->
			<table cellpadding="0" cellspacing="0">
				<thead class="table-head">
					<td>KPI's</td>
					<td>Tasks</td>
					<td>Start Date</td>
					<td>Completion Date</td>
					<td>Evaluation</td>

				</thead>

                <?php
                if(!empty($kpi_eval)){
					foreach($kpi_eval as $kpi_eval_val){ ?>
                <tr class="table-row">
                	<td><?php echo $kpi_eval_val->kpi; ?></td>
                	<td><?php echo $kpi_eval_val->task_name; ?></td>
                	<td><?php echo date_format_helper($kpi_eval_val->start_date); ?></td>
                	<td><?php echo date_format_helper($kpi_eval_val->completion_date); ?></td>
                	<td><div class="meter orange"><?php echo  intval($kpi_eval_val->percent_achieved); ?>%<span style="width: <?php echo intval($kpi_eval_val->percent_achieved); ?>%"></span></div></td>
                    </tr>
                <?php }} else{echo "<td colspan='6'><span style='color:red'>No Record Found ....</span></td>";} ?>
			</table>

		<div class="head"> Performance Evaluation</div>

		<div class="row" style="width:98%;">
            <table>
                <thead>
                <tr class="table-head">
                    <th> KPI </th>
                    <th> Percentage</th>
                    <th> Evaluation Date</th>
                    <th> Remarks</th>
                </tr>
                </thead>
                <tbody>
		<?php
		$counter = 0;
		foreach($kpi_select as $select):?>
            <tr>
				<td><?php echo $select->kpi; ?></td>
				<td><?php echo $select->percent_achieved; ?></td>
				<td><?php echo date_format_helper($select->date); ?></td>
                <td><?php echo $select->remarks; ?></td>
            </tr>
			<?php  endforeach;$counter++;?>
                </tbody>
            </table>

			</div>

		</div>

	</div>
<!-- contents -->