<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Leave & Attendence Reports / Time Sheet Report</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">

		<div class="head">Time Sheet Report</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" name="" placeholder="Employee Name">
				<select>
					<option>Hours Type</option>
				</select>
				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>
		<hr>
		<div class="row2">
			<div class="left-into">
				<h5 class="head">Time Sheet Standards</h5>
				<table class="doing">
				<tr>
					<td>Standard Hours</td>
					<td>24</td>
				</tr>
				<tr>
					<td>In Date Hours</td>
					<td>12</td>
				</tr>
				<tr>
					<td>Out Date Hours</td>
					<td>08</td>
				</tr>
				<tr>
					<td>Total Hours</td>
					<td>14</td>
				</tr>
				</table>
			</div>
			<div class="right-info">
				<h5 class="head">Employee Info</h5>
				<table class="doing">
				<tr>
					<td>Employee Name</td>
					<td>Syed a Jan</td>
				</tr>
				<tr>
					<td>Department</td>
					<td>Web Design</td>
				</tr>
				<tr>
					<td>Employee ID</td>
					<td>NE12</td>
				</tr>
				<tr>
					<td>Project</td>
					<td>Designing</td>
				</tr>
				</table>
			</div>
		</div>
		<table cellspacing="0">
				<thead class="table-head">
					<td>Standard Hours</td>
					<td>Working Hours</td>
					<td>Leave Hours</td>
					<td>Total Hours</td>
				</thead>
				<tr class="table-row">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
				</tr>
				<tr class="table-row">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
				</tr>
				<tr class="table-row">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
				</tr>
				<tr class="table-row">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
				</tr>
				<tr class="table-row">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
				</tr>
			</table>
	</div>
</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php  $this->load->view('includes/reports_list_nav');?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->