
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Position Management/view Position</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">View Retirement Record</div>

			<!-- filter -->
			
<?php //echo "<pre>";print_r($complete_profile);?>
			
			<!-- table -->
			<table cellspacing="0" id="table_list">
				<thead class="table-head">
                	<!--<td>Link</td>-->
					<td>Employee Code</td> 
					<td>Employee Name</td>
                    <td>Position </td>
					<!--<td>Reason Of Ret.</td>-->
					<td>Job Specification</td>
					<td>Separation Type</td>
					<td>Separation Date </td>
					<!--<td>To Date</td>-->
			
				</thead>
                <?php if(empty($complete_profile)){ echo"no data founded";}else{
					foreach( $complete_profile as  $complete_profile):
					?>
				<tr class="table-row">
                <td><?php  echo $complete_profile->employee_code;?></td> 
					<td><?php  echo $complete_profile->full_name;?></td>
                    <td><?php  echo $complete_profile->designation_name;?></td>
					<!--<td><?php /* echo $complete_profile->posting_reason;*/?></td>-->
                    <td><?php  echo $complete_profile->job_specifications;?></td>
                    <td><?php  echo $complete_profile->separation_type;?></td>
                    <td><?php  echo date_format_helper($complete_profile->date);?></td>
                   <?php /*?> <td><?php  echo $complete_profile->posting_reason;?></td><?php */?>
                </tr>
                <?php endforeach;
				}
				?>
			</table>
<!-- button group 
			<div class="row">
				<div class="button-group">
					<a href="add_employees.php"></a>
				</div>
			</div>-->

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->