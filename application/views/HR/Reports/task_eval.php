<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php  echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.resize{
	width: 220px;
}
</style>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head">Task Progress</div>
<!-- Right Side Historic Detail -->
	<div class="form-right">
		<table class="table">
			<thead class="table-head">
				<td colspan="2">Employee Info</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee Name</h5></td>
					<td><?php echo @$employee_info->full_name;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Employee ID</h5></td>
					<td><?php echo @$employee_info->employee_code;?></td>
				</tr>
				<tr class="table-row">
					<td><h5 class="headingfive">Designation</h5></td>
					<td><?php echo @$employee_info->designation_name;?></td>
				</tr>
			</tbody>
		</table>
	</div>
            <br class="clear" />
			<!-- table -->
        <form action="human_resource/task_progress_report" method="post">
			<table cellpadding="0" cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>Milestone</td>
					<td>Tasks</td>
                    <td>Percent Complete</td>
					<td>Remarks</td>
				</thead>

                <tr class="table-row">
                	<td><?php echo $task_info->milestones;?></td>
                	<td><?php echo $task_info->task_name;?></td>
                    <td><input type="text" name="percent" id="percent" value="<?php echo @$TaskProgressInfo->percent_achieved;?>"  placeholder="Only Numbers..." required="required">&nbsp<span id="errmsg"></span></td>
					<td><textarea name="remarks" id="" cols="30" rows="3"><?php echo @$TaskProgressInfo->remarks;?></textarea></td>
                    <input type="hidden" name="milestone_id" value="<?php echo $task_info->id;?>">
                    <input type="hidden" name="taskID" value="<?php echo @$taskProgressId;?>">
                    <input type="hidden" name="employee_id" value="<?php echo $this->uri->segment(3);?>">
                </tr>

			</table>
		<div class="row">
			<div class="button-group">
					<input type="submit" class="btn green" value="Save">
			</div>
        </form>
		</div>
	
		</div>	
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->

<!--validation for numeric fields-->
<style>
    #errmsg
    {
        color: red;
    }
</style>
<script>
    $(document).ready(function () {
        //called when key is pressed in textbox
        $("#percent").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;
            }
        });


        //Page Notifications
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>