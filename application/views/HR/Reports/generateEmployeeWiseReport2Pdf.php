<div id="print_area">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script>
	function print_table(id) {
		var printContents = document.getElementById(id).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}</script>


<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
body{
	background: #f8f8f8;
}
table th{
	font-size: 12px;
	font-weight:bold;
}
table td{
	font-size: 11px;
}
.shadow h5{
	display:inline-block;
	margin-left: 4em;
}
    .projectSec{float:left;width:auto;height: 40px;}
    .left1{float:left;width:120px;height: 40px;margin-right: 7px;}
    .right1{float:left;width:auto;height: 40px;margin-right: 7px;}

th{background: #4D6684;color:white;}
</style>

<div class="row" style=" height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-10 col-md-offset-1">
			<div class="col-lg-12">
				<h4 class="text-center"><b><u>Programe Wise Report</u></b></h4>

            <!--    <li class="fa fa-circle-o" style="color: red"> Short Expiry</li>-->
			</div>
            <?php if(!empty($info) && is_array($info)){
                foreach($info as $key=>$val){
            ?>
                    <?php echo $val['projectTitle'];?>
			<table class="table table-striped table-bordered table-condensed reports">
				<thead>

                <!--<div class="projecName">
                    <div class="projectSec">
                        <div class="left1" >
                            <h5 style="margin-right: 15px;"><b><u>Project Name:</u></b></h5>
                        </div>
                    </div>
                    <!--<h5 style="margin-right: 15px;"><b><u>Project Name:</u></b></h5>
                    <h5><?php /*/*echo $rec->project_title;*/?></h5>
                </div> -->
				  <tr>
					<td style="padding: 8px;">Emp Code</td>
					<td style="padding: 8px;">Name</td>
					<td style="padding: 8px;">Designation</td>
					<td style="padding: 8px;">Work Location</td>
					<td style="padding: 8px;">Contract Start Date</td>
					<td style="padding: 8px;">Contract End date</td>


				  </tr>
				</thead>
				<tbody>
                      <?php foreach($info[$key]['employees'] as $subKey=>$subVal){
                           $date1=date("Y-m-d");
                          $date2=$subVal['contractExpiry'];
                          $ts1 = strtotime($date1);
                          $ts2 = strtotime($date2);

                          $year1 = date('Y', $ts1);
                          $year2 = date('Y', $ts2);

                          $month1 = date('m', $ts1);
                          $month2 = date('m', $ts2);

                          $diff = intval(($year2 - $year1) * 12) + ($month2 - $month1);
                          if($diff < 3){
                          echo '<tr style="color:red" >';
                        echo "<td>".$subVal['employeeCode']."</td>";
                        echo "<td>".$subVal['employeeName']."</td>";
                        echo "<td>".$subVal['designation']."</td>";
                        echo "<td>".$subVal['posting']."</td>";
                        echo "<td>".$subVal['contractStart']."</td>";
                        echo "<td>".$subVal['contractExpiry']."</td>";

                       // echo "<td>".$diff."</td>";
                          echo '</tr>';}
                          else{
                              echo '<tr>';
                              echo "<td>".$subVal['employeeCode']."</td>";
                              echo "<td>".$subVal['employeeName']."</td>";
                              echo "<td>".$subVal['designation']."</td>";
                              echo "<td>".$subVal['posting']."</td>";
                              echo "<td>".$subVal['contractStart']."</td>";
                              echo "<td>".$subVal['contractExpiry']."</td>";

                             // echo "<td>".$diff."</td>";
                              echo '</tr>';
                          }
                      }
                      ?>
				</tbody>

			</table>
            <?php }}else{echo "sorry no record found !";} ?>
	</div>
</div>
	</div>
<!---------------------------Send Email Modal--------------------------------------->
<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Email To</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-3 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="firstname"
                                   placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname"
                                   placeholder="Reply to Email(Optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname"
                                   placeholder="Attached Generated Payroll RDF">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Attachments</label>
                        <div class="col-sm-9">
                            <label class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

