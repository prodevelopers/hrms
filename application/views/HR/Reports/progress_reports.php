<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Progress Report </div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Progress Reports</div>
        
       	<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <form method="post">
				<select name="data" onchange="this.form.submit()">
                <option> --Select Year-- </option>
               <?php  foreach($name as $info)
{ echo"<option>$year</option>";}?>
				</select>
                </form>
	
			</div>
		<hr style="width: 100%;
float: left;
margin-top: 1em;
margin-bottom: 1em;">
	<div style="width: 50%; margin:20px auto;">
     <h4><?php
          echo $full->full_name;?></h4><br />
          <h5><?php 
if(empty($name)){echo "No Task Assign";}else { foreach($name as $info)
{
	
	$date=$info->date;
	$year=date('Y',strtotime($date));
	//echo $year;
	//die;
}}
?></h5>
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : [<?php if(empty($name)){echo "No Task Assign";}else {foreach($name as $nam){  echo '"';echo $nam->kpi;echo '",'; }}?>],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php foreach($name as $nam){
					 echo '"';echo $nam->percent_achieved; echo '",'; }?>],
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>

<div class="form-left">
		<?php echo form_open(); ?>
        
		<input type="hidden"   name="employer" value="<?php  echo $full->employee_id;?>" readonly="readonly"/>
			
         <br class="clear">
				<div class="row2">
					<h4>Date</h4>
					<input type="text" id="submit_date"  name="submit_date" reqiured/>
				</div>
          <br class="clear">
				<div class="row2">
					<h4>Employee Feedback</h4>
					<textarea name="reviewer" rows="7" cols="33" required></textarea>
				</div>
                
				<br class="clear">
				<div class="row2">
					<h4>Reviewer Comments</h4>
					<textarea name="comments" rows="7" cols="33" required></textarea>
				</div>
            <!-- button group -->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Submit" class="btn green" />
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
		</div>
	</div>
</div>

<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $( "#submit_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });
    </script>
    <!-- leftside menu end -->