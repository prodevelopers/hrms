<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 1/16/2015
 * Time: 10:54 AM
 */

?>
<style>
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
    .move{
        position: relative;
        left: 48%;
        top: 60px;
    }
    .resize{
        width: 220px;
    }
    .rehieght{
        position: relative;
        top: -5px;
    }
    .dataTables_length{
        position: relative;
        left: -0.2%;
        top: -20px;
        font-size: 1px;
    }
    .dataTables_length select{
        width: 60px;
    }

    .dataTables_filter{
        position: relative;
        top: -21px;
        left: -19%;
    }
    .dataTables_filter input{
        width: 180px;
    }
    .revert{
        margin-left: -15px;
    }
    .marge{
        margin-top: 40px;
    }
</style>


<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Employees Projects Allocation</div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">

        <div class="head">Available Employees</div>

        <!-- filter -->
        <div class="filter">
            <h4>Filter By</h4>
            <input type="text" placeholder="Search Employee" id="searchEmployeeDTSDom">
        </div>

<div class="clear"></div>
        <!-- table -->
        <table class="table" id="listAvailableEmployees" cellspacing="0">
            <thead class="table-head">
            <tr id="table-row">
                <td>Employee ID</td>
                <td>Employee Code</td>
                <td>Employee Name</td>
                <td>Manage Projects</td>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <!--<a href="assign_position.php"><button class="btn green"></button></a>-->

            </div>
        </div>
    </div>
</div>
</div>
<!-- contents -->


<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    var oTable;
    $(document).ready(function (e) {
        oTable = '';
        //Load List of Approvals in DataTables.
        var selectorTableManualTimeSheet =  $('#listAvailableEmployees');
        var url_DT = "<?php echo base_url(); ?>human_resource/view_projects_management/DT";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "EmployeeID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Employee Name */ {
                "mData" : "EmployeeCode"
            },
            /* Employee Code */ {
                "mData" : "EmployeeName"
            },
            /* Manage Button*/{
                "mData" : "Manage"
            }
        ];
        var HiddenColumnID_DT = 'EmployeeID';
        var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);

        $("#searchEmployeeDTSDom").keyup(function() {
            oTable.fnFilter(this.value);
        });

        //Function To Work When Manage Button is Pressed.
        selectorTableManualTimeSheet.on('click','.ManageProjectsButton', function (e) {
            var empID = $(this).closest('tr').attr('data-id');
            var postData = {
                employee: empID
            };

            //Old Way
//            $.redirect('<?php //echo base_url(); ?>//human_resource/manage_employee_projects',postData,'POST');

            //For New Simple Method.
            window.location.href = "<?php echo base_url(); ?>human_resource/employee_projects/"+empID;



        });
    });
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->