<div class="contents-container">
<div class="bredcrumb">Dashboard / Human Resource / Assign Task</div> <!-- bredcrumb -->
<!--<script src="assets/js/jquery-1.9.1.js"></script>-->
<script type="text/javascript">

</script>
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Assign Task</div>
        <div class="form-left">
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee_id; ?>" />
        	<div class="row2">
                <div id="warning"><?php echo $this->session->flashdata('warning');?></div>
					<h4>Project</h4>
					<?php

					$selctd_title = (isset($edit_aj->project_id) ? $edit_aj->project_id : '');
					echo form_dropdown('project_id',$project,$selctd_title,'required="required" id="project_id" onchange="populate_tasks();"'); ?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Task</h4>
					<?php 
					$selctd = (isset($edit_aj->ml_assign_task_id) ? $edit_aj->ml_assign_task_id : '');
					echo form_dropdown('ml_assign_task_id', $tasks, $selctd,'required="required" id="tasks"','required="required"'); ?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Start Date</h4>
					<input type="text" id="joining_date" name="start_date" value="<?php echo $edit_aj->start_date;?>" required="required">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>End Date</h4>
					<input type="text" id="complete_date" name="completion_date" value="<?php echo $edit_aj->completion_date;?>" required="required">
				</div>
				<br class="clear">
				<div class="row2">
				<h4>Key Performance Indicators</h4>
				<?php 
					$selctd_kpi = (isset($edit_aj->ml_kpi_type_id) ? $edit_aj->ml_kpi_type_id : '');
					echo @form_multiselect('ml_kpi_type_id[]', $kpi, $selctd_kpi,'id="kpis" disabled="disabled"'); ?>
				<div class="tipping">
	                <p>
	                Hold Down the Ctrl Button to Select Multiple Options.
	                </p>
                </div>
				</div>
                
				<br class="clear">
				<!--<div class="row2">
					<h4>Status</h4>
					<select name="status">
                    	<option value="-1">-- Select Task Status --</option>
                    	<option value="In Progress"<?php /*if(isset($edit_aj->status)){if($edit_aj->status == "In Progress"){echo "selected='selected'";}} */?>>In Progress</option>
                    	<option value="Stopped"<?php /*if(isset($edit_aj->status)){if($edit_aj->status == "Stopped"){echo "selected='selected'";}} */?>>Stopped</option>
                    	<option value="Completed"<?php /*if(isset($edit_aj->status)){if($edit_aj->status == "Completed"){echo "selected='selected'";}} */?>>Completed</option>
                    </select>
				</div>
                <br class="clear">-->
                
				<br class="clear">
				<div class="row2">
					<h4>Task / Project Description</h4>
					<textarea name="task_description" rows="7" cols="33"><?php if(isset($edit_aj->task_description))echo $edit_aj->task_description; ?></textarea>
				</div>
            <!-- button group -->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="update" value="Update Task" class="btn green" />
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
            <div class="form-right">
                    <div class="head">Assigned Task</div>
                           <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$job_history->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Employee ID</h5>
                                <i class="italica"><?php echo @$job_history->employee_code;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$job_history->designation_name;?></i>
                        </div>
            </div>

	</div>
  </div>
</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
        $("#warning").delay(3000).fadeOut('slow');
        /////// Function for Populate Tasks According to Project
        function populate_tasks()
        {
            var proj_id=$("#project_id").val();

            $.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>human_resource/task_populate",
                dataType:"JSON",
                data:{project_id:proj_id},
                success:function(result){
                    if(result == false){
                        $('#tasks').text('');
                        var opt=$('<option/>');
                        opt.text("No Tasks");
                        $('#tasks').append(opt);
                        $('#tasks').attr('disabled','disabled');
                        $('#tasks').attr('title','No Tasks for this Project');
                        //populate_kpi();
                        return;
                    }
                    $('#tasks').removeAttr("disabled");
                    $('#tasks').removeAttr('title');
                    $('#tasks').text('');
                    $.each(result,function(ml_assign_task_id,task_name){

                        var opt=$('<option/>');
                        opt.val(ml_assign_task_id);
                        opt.text(task_name);
                        $('#tasks').append(opt);

                    });
                    //populate_kpi();
                }
            });
        }
        /////// Function for Populate Tasks According to Project
        /*function populate_kpi()
        {
            var proj_id=$("#project_id").val();
            $.ajax({
                type:"POST",
                url:"<?php echo base_url(); ?>human_resource/kpi_populate",
                dataType:"JSON",
                data:{project_id:proj_id},
                success:function(result){
                    if(result == false){
                        $('#kpis').text('');
                        var opt=$('<option/>');
                        opt.text("no kpis");
                        $('#kpis').append(opt);
                        $('#kpis').attr('disabled','disabled');
                        $('#kpis').attr('title','No Kpi for this Project');
                        return;
                    }
                    $('#kpis').removeAttr("disabled");
                    $('#kpis').text('');
                    $.each(result,function(ml_kpi_id,kpi){
                        var opt=$('<option/>');
                        opt.val(ml_kpi_id);
                        opt.text(kpi);
                        $('#kpis').append(opt);
                    });

                }
            });
        }*/
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
        $(function() {
            $( "#joining_date").datepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:MM",
                changeMonth: true,
                changeYear: true
            });
            $( "#complete_date" ).datepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:MM",
                changeMonth: true,
                changeYear: true
            });
        });

    </script>
    <!-- leftside menu end -->