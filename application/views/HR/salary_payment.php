<?php include('includes/header.php'); ?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll</div> <!-- bredcrumb -->

	<?php include('includes/payroll_left_nav.php'); ?>
	
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Salary Payment</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Status</option>
					<option>Fully Paid</option>
					<option>Partialy Paid</option>
					<option>Pending</option>
				</select>
				<select>
					<option>Month</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>

			

			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Paygrade</td>
					<td>Month</td>
					<td>Base salary</td>
					<td>Allowances</td>
					<td>Payable</td>
					<td>Status</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-white.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
					<!--<td>Pay Salary</td>-->
				</thead>
				<tr class="table-row">
					<td>004</td>
					<td>Employee</td>
					<td>bps 15</td>
					<td>August</td>
					<td>Rs 5000</td>
					<td>Rs 40000</td>
					<td>Rs 45000</td>
					<td>Payed</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-black.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
					
					<!--<td>Pay Salary</td>-->
				</tr>
				<tr class="table-row">
					<td>004</td>
					<td>Employee</td>
					<td>bps 15</td>
					<td>August</td>
					<td>Rs 5000</td>
					<td>Rs 40000</td>
					<td>Rs 45000</td>
					<td>Payed</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-black.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
					<!--<td>Pay Salary</td>-->
				</tr>
				<tr class="table-row">
					<td>004</td>
					<td>Employee</td>
					<td>bps 15</td>
					<td>August</td>
					<td>Rs 5000</td>
					<td>Rs 40000</td>
					<td>Rs 45000</td>
					<td>Payed</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-black.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
					<!--<td>Pay Salary</td>-->
				</tr>
				<tr class="table-row">
					<td>004</td>
					<td>Employee</td>
					<td>bps 15</td>
					<td>August</td>
					<td>Rs 5000</td>
					<td>Rs 40000</td>
					<td>Rs 45000</td>
					<td>Payed</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-black.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
					<!--<td>Pay Salary</td>-->
				</tr>
				<tr class="table-row">
					<td>004</td>
					<td>Employee</td>
					<td>bps 15</td>
					<td>August</td>
					<td>Rs 5000</td>
					<td>Rs 40000</td>
					<td>Rs 45000</td>
					<td>Payed</td>
					<td align="center"><a href="pay_salary.php"><img src="img/icon-black.png" width="20" height="20"></a></td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>					
					<td><span class="fa fa-print"></span></td>
					<!--<td>Pay Salary</td>-->
				</tr>
			</table>

			

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
