<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Dependents</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Add Dependents</div>
		<?php echo form_open(); ?>
		<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Name</h4>
					<input type="text" name="dependent_name">
				</div>

				<br class="clear">

				<div class="row">
					<h4>Relation</h4>
                    <?php
					echo @form_dropdown('ml_relationship_id', $relation,'required="required"','id="drop_rel"'); ?>
					<a id="rel"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<br class="clear">

				<div class="row">
					<h4>Date of Birth</h4>
					<input type="text" name="date_of_birth" id="dateofbirth">
				</div>
        <br class="clear">

        <div class="row">
            <h4>Existing Illness</h4>
            <input type="text" name="illness" >
        </div>

        <br class="clear">

        <div class="row">
            <h4>Insurance</h4>
            <input type="checkbox" name="insurance" id="selecting" >
        </div>
        <br class="clear">
        <div class="row" id="insurance" style="display: none;">
            <h4>Insurance Starts Date</h4>
            <input type="text" name="insurance" id="joining_date" >
        </div>
			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>


		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Dependents</div>

			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Name</td>
					<td>Relation</td>
                    <td>Date of Birth</td>
                    <td>Existing Illness</td>

                    <!--<td><span class="fa fa-trash-o"></span></td>-->
				</thead>
                <?php if(!empty($dependent_details)){
					foreach($dependent_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->dependent_name;?></td>
					<td><?php echo $row->relation_name;?></td>
					<td><?php echo date_format_helper($row->date_of_birth);?></td>
					<td><?php echo $row->illness;?></td>
					<!--<td><span class="fa fa-trash-o"></span></td>-->
				</tr>
				<?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
    $('#selecting').click(function(){
        $('#insurance').toggle();
    });
</script>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<!-- designaiton dialog -->
	<div id="relation" title="Add Relation" style="display:none; width:600px;">
	<form id="relForm" action="human_resource/add_relation23/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="relation_name" id="txt_rel"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script>
	$(document).ready(function()
	{
		$("#relation").on('click',function(e){
			e.preventDefault();
			var formData = $('#relForm').serialize();
			$.ajax({
				url: "human_resource/add_relation23/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_rel').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_rel').append(appendData);
						$("#relation").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

    $( "#dateofbirth" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        maxDate:0,
        yearRange:"<?php echo yearRang(); ?>"

    });
</script>