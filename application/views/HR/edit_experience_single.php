<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Experience</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Experience</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
					<h4>Job Title</h4>
					<?php $selctd_title = (isset($experience->job_title) ? $experience->job_title : '');
					echo @form_dropdown('job_title', $job_title, $selctd_title,'required="required"','required="required"'); ?>
					<?php /*?><input type="text" name="job_title" value="<?php echo @$experience->job_title; ?>"><?php */?>
				</div>

				<div class="row">
					<h4>Organization</h4>
					<input type="text" name="organization" value="<?php echo @$experience->organization; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employment Type</h4>
                    <?php 
					$selctd_type = (isset($experience->ml_employment_type_id) ? $experience->ml_employment_type_id : '');
					echo @form_dropdown('ml_employment_type_id', $employment, $selctd_type); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
				<div class="row">
					<h4>From</h4>
					<input type="text" name="from_date" value="<?php  echo date_format2_helper($experience->from_date); ?>" id="fromDate">
				</div>
				<br class="clear">
				<div class="row">
					<h4>To</h4>
					<input type="text" name="to_date" value="<?php echo date_format2_helper($experience->to_date); ?>" id="toDate">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comment"><?php echo @$experience->comment; ?></textarea>
				</div>
                
			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
			

		</div>
	</div>
<!-- contents -->
<script type="text/javascript">
    $(document).ready(function(e){

        $( "#fromDate,#toDate" ).datepicker({
            dateFormat: "dd-mm-yy",
            timeFormat: "HH:MM",
            changeMonth: true,
            changeYear:true,
            yearRange:"<?php echo yearRang(); ?>"
        });
    });
</script>