<?php include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

	<?php include('includes/hr_left_nav.php'); ?>

	<div class="right-contents">

		<div class="head">Personnel List</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Job Title</option>
				</select>
				<select>
					<option>Designation</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>

			
			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><a href="view_employee_profile.php"><span class="fa fa-eye" style="color:#000;"></span></a></td>
					<td><span class="fa fa-print"></span></td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><a href="view_employee_profile.php"><span class="fa fa-eye" style="color:#000;" style="color:#000;"></span></a></td>
					<td><span class="fa fa-print"></span></td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><a href="view_employee_profile.php"><span class="fa fa-eye" style="color:#000;"></span></a></td>
					<td><span class="fa fa-print"></span></td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><a href="view_employee_profile.php"><span class="fa fa-eye" style="color:#000;"></span></a></td>
					<td><span class="fa fa-print"></span></td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><a href="view_employee_profile.php"><span class="fa fa-eye" style="color:#000;"></span></a></td>
					<td><span class="fa fa-print"></span></td>
				</tr>
			</table>
<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="add_employees.php"><button class="btn green">Add</button></a>
				</div>
			</div>

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>