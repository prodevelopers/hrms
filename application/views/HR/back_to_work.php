<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Employement Management / Re-Join</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_left_nav.php'); ?>
	

	<div class="right-contents">
					<div class="head">Rejoin Employee</div>

        <?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />


        <br class="clear">
        <div class="row">
            <h4>Employment Type</h4>
            <?php
            $slctd_emp_type = (isset($contract->employment_type) ? $contract->employment_type : '');
            echo @form_dropdown('employment_type', $employment_type, $slctd_emp_type); ?>
        </div>

        <br class="clear">
        <div class="row">
            <h4>Job Category</h4>
            <?php
            $slctd_job_cat = (isset($contract->employment_category) ? $contract->employment_category : '');
            echo @form_dropdown('job_category', $job_category, $slctd_job_cat); ?>
        </div>

				<br class="clear">
				<div class="row">
					<h4>Contract Expiry Date</h4>
					<input type="text" name="contract_expiry_date" id="visa_issue_date" value="<?php echo @$contract->contract_expiry_date; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Send Contract Expiry Alert</h4>
				<!--	<input type="text" name="contract_exp_alert" value="">-->
                   
                   
                   <select name="contract_exp_alert">
                    <option value="0" <?php if ($contract->contract_exp_alert == 0){echo "selected = 'selected'";} ?> >Yes</option>
                    <option value="1" <?php if ($contract->contract_exp_alert == 1){echo "selected = 'selected'";} ?>>No</option>
                    </select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert Date</h4>
					<input type="text" name="date_contract_expiry_alert" id="contract_exp_date_id" value="<?php echo @$contract->date_contract_expiry_alert; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comments"><?php echo @$contract->comments; ?></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Save" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>		

		</div>


		
	</div>
<!-- contents -->