
<style>
    #pencil:hover{
        cursor: pointer;
    }
</style>

<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Employment Info</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav.php'); ?>
	

	<div class="right-contents1">

		<div class="head">Employment Info</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
					<h4>Current Job Designation</h4>
					<?php
					 $selected_job_title =(isset($position_mgt->ml_designation_id) ? $position_mgt->ml_designation_id : '');
					echo @form_dropdown('curr_job_title',$job_title,$selected_job_title,'id="drop_title" required="required"','required="required",required="required"'); ?>
					<a id="desi"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<!--<div class="row">
					<h4>Job Specification</h4>
					<input type="text" name="job_specification" value="<?php /*echo @$position_mgt->job_specifications; */?>">
				</div>-->

				<br class="clear">
				<div class="row">
					<h4>Employment Type</h4>
					<?php  
					$slctd_emp_type = (isset($contract->employment_type) ? $contract->employment_type : '');
					echo @form_dropdown('employment_type', $employment_type, $slctd_emp_type,'id="emp_typ"','required="required"','required="required"'); ?>
                    <a id="emp_type"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Job Category</h4>
					<?php  
					$slctd_job_cat = (isset($contract->employment_category) ? $contract->employment_category : '');
					echo @form_dropdown('job_category', $job_category, $slctd_job_cat,'id="job_categories"','required="required"','required="required"'); ?>
                    <a id="job_cat"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Pay Grade</h4>
					<?php   
					$slctd_pay_grade = (isset($position_mgt->ml_pay_grade_id) ? $position_mgt->ml_pay_grade_id : '');
					echo @form_dropdown('pay_grade', $paygrade, $slctd_pay_grade,'id="pay_grades"','required="required"','required="required"'); ?>
                    <a id="pay_grade"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Department</h4>
					<?php  
					$slctd_dept = (isset($posting->ml_department_id) ? $posting->ml_department_id : '');
					echo @form_dropdown('department', $department, $slctd_dept,'id="drop_dept" required="required"','required="required"'); ?>
					<a id="dep"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Branch / Office</h4>
					<?php   
					$slctd_branch = (isset($posting->ml_branch_id) ? $posting->ml_branch_id : '');
					echo @form_dropdown('branch', $branch, $slctd_branch,'id="branches"','required="required"','required="required"'); ?>
                    <a id="branch"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>City</h4>
					<?php   
					$slctd_city = (isset($posting->ml_city_id) ? $posting->ml_city_id : '');
					echo @form_dropdown('city', $city, $slctd_city,'id="drop_city" required="required"','required="required"'); ?>
					<a id="city"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>


				<div class="row">
					<h4>Joining Date</h4>
					<input type="text" name="joining_date" id="joining_date" value="<?php echo date_format_helper(@$employment->joining_date); ?>">
				</div>
				<div class="row">
					<h4>Location</h4>
					<?php
					$slctd_loc = (isset($posting->location) ? $posting->location : '');
					echo @form_dropdown('location', $location, $slctd_loc,'id="drop_loc"','required="required"'); ?>
					<a id="loc"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
        <br class="clear">
       <!-- <div class="row">
            <h4>Project Name</h4>
            <?php /*if(!empty($project)){ foreach($project as $project) {@$slctd_loc = (isset($project->project_id) ? $project->project_id : '');}}
           /*echo @form_multiselect('project_id[]',$approval,$slctd_loc,'required="required"','required="required"');*/?>
          <?php /*if(!empty($approval))
		  {
			 */?>
			<select name="project_id[]" multiple>
				<?php /* foreach($approval as $rec){*/?>
					<option value="<?php /*echo $rec->project_id.":".$rec->employee_project_id;*/?>"selected><?php /*echo $rec->project_title;*/?></option>

				<?php /*}}else{*/?>
			<?php /*echo"<select multiple><option> -- No Project-- </option></select>";}*/?>
			</select>

           <input type="hidden" name="project_name" value="<?php /*//echo $project->project_name;*/?>" >
        </div>-->





        <div class="row">
					<h4>Work Shift</h4>
					<?php   
					$slctd_work_shift = (isset($posting->shift) ? $posting->shift : '');
					echo @form_dropdown('work_shift', $work_shift, $slctd_work_shift,'id="workShift"'); ?>
            <a id="ws"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
				</div>

				<br class="clear">
				<br class="clear">
					<div class="head">Employment Contract</div>
				
				<br class="clear">
				<div class="row">
					<h4>Contract Start Date</h4>
					<input type="text" name="contract_start_date" id="contract_start_date" value="<?php echo date_format_helper(@$contract->contract_start_date); ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Expiry Date</h4>
					<input type="text" name="contract_expiry_date" id="contract_expiry_date" value="<?php echo date_format_helper(@$contract->contract_expiry_date); ?>">
				</div>
				<br class="clear">

        <div class="row">
            <h4>Contract Alert Date</h4>
            <input type="text" name="date_contract_expiry_alert" id="date_contract_expiry_alert" value="<?php echo date_format_helper(@$contract->date_contract_expiry_alert); ?>">
        </div>
		<div class="row">
			<h4>Probation Period (Days)</h4>
			<input type="text" name="probation" id="probationDays" value="<?php echo @$contract->probation;?>">
		</div>
		<br class="clear">
		<div class="row">
			<h4>Probation Alert Date</h4>
			<input type="text" name="prob_alert_dat" id="probationAlertDate" value="<?php echo date_format_helper(@$contract->prob_alert_dat); ?>">
		</div>


				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comments"><?php echo @$contract->comments; ?></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>		

		</div>


		
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->

<!-- Employment Type dialog -->
<div id="employmentType" title="Add Employment Type" style="display:none; width:600px;">
    <form id="EmpTForm">
        <div class="data">
            <input type="text" class="text_field" name="employment_type" id="employment_type"/>
            <br><br>
            <input type="submit" value="Add" id="EmpTY" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- End of Employment Type Form -->
<!-- Job Category dialog -->
<div id="JobCategory" title="Add Job Category" style="display:none; width:600px;">
    <form id="JobCat">
        <div class="data">
            <input type="text" class="text_field" name="Job_Category" id="Job_Categ"/>
            <br><br>
            <input type="submit" value="Add" id="JobAdd" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- End of Job Category Form-->

<!-- Branch -->
<div id="Branch_d" title="Add Branch" style="display:none; width:600px;">
    <form id="Brnh">
        <div class="data">
            <input type="text" class="text_field" name="add_brach" id="branch_b"/>
            <br><br>
            <input type="submit" value="Add" id="BRANCH" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- Pay Grade dialog -->
<div id="PayGrade" title="Add Pay Grade" style="display:none; width:600px;">
    <form id="payGrd">
        <div class="data">
            <input type="text" class="text_field" name="Pay_grads" id="pay_g"/>
            <br><br>
            <input type="submit" value="Add" id="PAYGRADE" class="btn green addedto" name="add">
        </div>
    </form>
</div>
	<!-- Designaiton dialog -->
	<div id="designation" title="Add Designation" style="display:none; width:600px;">
	<form id="desForm" action="human_resource/add_designation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="designation_name" id="txt_title"/>
			<br><br>
			<input type="submit" value="Add" id="DESIGNATION" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- Department dialog -->
	<div id="department" title="Add Department" style="display:none; width:600px;">
	<form id="deptForm" action="human_resource/add_department2/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="dept_name" id="txt_dept"/>
			<br><br>
			<input type="submit" value="Add" id="DEPARTMENT" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- City dialog -->
	<div id="city-pop" title="Add City" style="display:none; width:600px;">
	<form id="cityForm" action="human_resource/add_city3/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="city_name" id="txt_city"/>
			<br><br>
			<input type="submit" value="Add" id="CITYID" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- Location dialog -->
	<div id="location" title="Add Location" style="display:none; width:600px;">
	<form id="locForm" action="human_resource/add_posting_location/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="posting_location" id="txt_loc"/>
			<br><br>
			<input type="submit" value="Add" id="LOCATIONId" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<!-- Work Shift -->
<div id="work_d" title="Add Work Shift" style="display:none; width:600px;">
    <form id="wsf">
        <div class="data">
            <input type="text" class="text_field" name="add_ws" id="ws_b"/>
            <br><br>
            <input type="submit" value="Add" id="WORKID" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<script>
	$(document).ready(function()
	{
		$("#LOCATIONId").on('click',function(e){
			e.preventDefault();
			var formData = $('#locForm').serialize();
			$.ajax({
				url: "human_resource/add_posting_location/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_loc').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_loc').append(appendData);
						$("#location").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

    $(document).ready(function () {

        //Work shift
        $("#WORKID").on('click', function (e) {
            e.preventDefault();
            var formData = {
                WorkS: $("#ws_b").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/AddWorkShiftEdit/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var WsEnterdValue = $('#ws_b').val();
                        var appendData = '<option value="' + data[3] + '">' + WsEnterdValue + '</option>';
                        $('#workShift').append(appendData);
                        $('#work_d').dialog('close');
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });
    });
	$(document).ready(function()
	{
		$("#CITYID").on('click',function(e){
			e.preventDefault();
			var formData = $('#cityForm').serialize();
			$.ajax({
				url: "human_resource/add_city3/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_city').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_city').append(appendData);
						$("#city-pop").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#DEPARTMENT").on('click',function(e){
			e.preventDefault();
			var formData = $('#deptForm').serialize();
			$.ajax({
				url: "human_resource/add_department2/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_dept').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_dept').append(appendData);
						$("#department").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
 });
    });

    //ADD BRANCH
    $(document).ready(function()
    {
    $("#BRANCH").on('click', function (e) {
        e.preventDefault();
        var formData = {
            AddBranch: $("#branch_b").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_branchEdit/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#branch_b').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#branches').append(appendData);
                    $("#Branch_d").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });
    });
    //Pay Grade
    $(document).ready(function()
    {
    $("#PAYGRADE").on('click', function (e) {
        e.preventDefault();
        var formData = {
            paygrade: $("#pay_g").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_pay_gradeEdit/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#pay_g').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#pay_grades').append(appendData);
                    $("#PayGrade").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });
    });
    //Job Category
    $(document).ready(function()
    {
    $("#JobAdd").on('click', function (e) {
        e.preventDefault();
        var formData = {
            JobCat: $("#Job_Categ").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_job_categoryEdit/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#Job_Categ').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#job_categories').append(appendData);
                    $("#JobCategory").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });
    });
        //Employment Type

    $(document).ready(function()
    {
        $("#EmpTY").on('click', function (e) {
            e.preventDefault();
            var formData = {
                Employment: $("#employment_type").val()
            };
            //alert(desi);
            $.ajax({

                url: "human_resource/add_employment_typeEdit/<?php echo @$employee->employee_id;?>",
                data: formData,
                type: "POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if (data[0] === "OK") {
                        Parexons.notification(data[1], data[2]);
                        var EmpTypeEnterdValue = $('#employment_type').val();
                        var appendData = '<option value="' + data[3] + '">' + EmpTypeEnterdValue + '</option>';
                        $('#emp_typ').append(appendData);
                        $("#employmentType").dialog("close");
                    } else if (data[0] === "FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });


	});
	$(document).ready(function()
	{
		$("#DESIGNATION").on('click',function(e){
			e.preventDefault();
			var formData = $('#desForm').serialize();
			$.ajax({
				url: "human_resource/add_designation/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_title').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_title').append(appendData);
						$("#designation").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});
        $('#probationDays').on('change', function (e) {
            //probation Days.
            if(isNaN($(this).val())){
                Parexons.notification('Only Numbers Are Allowed','error');
                $(this).val(0);
                return;
            }
            var totalProbationDays = $(this).val();
            var probationAlertDate = $('#probationAlertDate').val();
            var maxProbationDays = 120;
            var contractStartDate = $('#date_of_birth').val(); //This ID is Given by some genius Here.. To Save Time i am not changing the ID Name..
            if(probationAlertDate.length === 0 && totalProbationDays > maxProbationDays){
                Parexons.notification('Probation Can Not Be More Than 120 Days','warning');
                $(this).val(120);
                return;
            }else if(probationAlertDate.length > 0){
                var diff = new Date(Date.parse(probationAlertDate) - Date.parse(contractStartDate));
                var totalDays = diff / 1000 / 60 / 60 / 24;
                if(totalProbationDays > totalDays){
                    Parexons.notification('You Have Extended The Probation Date More Than the Probation Alert Date','warning');
                    $('#probationAlertDate').val('');
                }
                if(totalProbationDays > maxProbationDays){
                    Parexons.notification('Probation Can Not Be More Than 120 Days','warning');
                    $(this).val(120);
                    return;
                }
            }

        });
        var probationAlertDateSelector = $('#probationAlertDate');
        probationAlertDateSelector.on('change',function(e){
            //Probation Alert Date Must Be Picked From Here.
            var totalProbationDays = $('#probationDays').val();
            if(totalProbationDays.length > 0){
//                get the date of after adding dates
                var newDate = new Date(new Date().getTime()+(totalProbationDays*24*60*60*1000));
            }
        });

        probationAlertDateSelector.datepicker({
            beforeShow: customRange,
            dateFormat: "dd-mm-yy",
            firstDay: 1
        });

        //Session Messages..
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
	});
    function customRange(input){
        var contractStartDate = $('#date_of_birth').val();
        var maxProbationDays = 120;
        var totalProbationDays = $('#probationDays').val();
        //Setting Up Min And Max Dates.
        var min = new Date(contractStartDate),
        dateMin = min,
        dateMax = new Date(min.getTime()+(maxProbationDays*24*60*60*1000));
        if(totalProbationDays.length > 0 && !isNaN(totalProbationDays)){
            dateMax = new Date(min.getTime()+(totalProbationDays*24*60*60*1000));
        }
        return {
            minDate: dateMin,
            maxDate: dateMax
        };
    }

    //*********** Datepicker ****************//
    $( "#joining_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",maxDate: '0',
        changeMonth: true,
        changeYear: true

    });

    $( "#contract_start_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function( selectedDate ) {
            $( "#contract_expiry_date" ).datepicker( "option", "minDate", selectedDate );
        }

    });

    $( "#contract_expiry_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function( selectedDate ) {
            $( "#contract_start_date" ).datepicker( "option", "maxDate", selectedDate );
        }

    });

    $( "#date_contract_expiry_alert" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM", minDate: '0',
        changeMonth: true,
        changeYear: true

    });
</script>