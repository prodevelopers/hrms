<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
		<div class="right-contents1">

		<div class="head">Benefits</div>
			<div id="txt" style="background-position: center">
				<?php echo  $this->session->flashdata('txt');?></div>
		<?php
        $formAttributes = array('onSubmit' => 'return check();');
        $formPOSTURL = base_url('human_resource/add_benefit_edt').'/'.$this->uri->segment(3);
        echo form_open($formPOSTURL,$formAttributes); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$empl->employee_id; ?>" />
        
				<div class="row">
					<h4>Benefit type</h4>
					<?php echo @form_dropdown('benefit_type_id', $benefit_type,'required ="required"','id ="drop_ben"'); ?>
					<a id="ben"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
					<!--<select>
						<option></option>
					</select>-->
				</div>
			<br class="clear">
				<!--<div class="row">-->
				<!--	<h4>Benefit</h4>
<!--					--><?php //echo @form_dropdown('benefits_list_id', $benefit_list, 'required="required"','required ="required"'); ?>
					<!--<input type="text" name="">
				</div>-->
<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="joining_date" >
				</div>
            <!--<div class="row increment-field">
                <h4>Insurance No</h4>
                <input type="text" name="insurance_no" id="insurance">
            </div>-->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_benefit" value="Add" class="btn green" />
					<a href="human_resource/edit_emp_entitlement_increment/<?php echo $employee->employee_id;?>"> <input type="button" value="Cancel" class="btn gray" /></a>
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
<td>Benefit Type</td>
					<td>Date</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($benefit_details)){
					foreach($benefit_details as $benefit){?>
				<tr class="table-row">
					<td><?php echo $benefit->benefit_type_title;?></td>
					<td><?php echo date_format_helper($benefit->date_effective);?></td>
					<td><a href="human_resource/edit_single_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<div id="benifit" title="Add Benefit" style="display:none; width:600px;">
	<form id="benForm" action="human_resource/add_benefit_type/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="benefit_type_title" id="txt_ben"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
	$("#txt").delay(3000).fadeOut('slow');
</script>
<script>
	$(document).ready(function()
	{
		$("#benifit").on('click',function(e){
			e.preventDefault();
			var formData = $('#benForm').serialize();
			$.ajax({
				url: "human_resource/add_benefit_type/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_ben').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_ben').append(appendData);
						$("#benifit").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
    function check()
    {
        var date_effectiveFrom = document.getElementById("joining_date").value;
        if(date_effectiveFrom === '')
        {
            Parexons.notification('Please Select Date','error');
            return false;
        }
        var drop_ben = $('#drop_ben').val();
        if(drop_ben.length === 0){
            Parexons.notification('Please Select Benefit Type From Drop Down','error');
            return false;
        }
    }

    //*********** Datepicker ****************//
    $( "#joining_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });
</script>