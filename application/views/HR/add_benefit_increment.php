<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php /*$this->load->view('includes/hr_left_nav'); */?>

    <!--For Menu---------------------------------->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">
		<div class="head">Increments</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
        	<br class="clear">
            <div class="row increment-field">
					<h4>Increment Type</h4>
					<?php echo @form_dropdown('increment_type_id', $increment_type,'required = "required"','required = "required"'); ?>
                  <!--<select>
                      <option></option>
                  </select>-->
				</div>

				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text" name="increment_amount">
				</div>
				<br class="clear">
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="date_effective">
				</div>

			<!-- button group -->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit"  name="add_increment" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
		<?php echo form_close(); ?>
				<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Increment Type</td>
					<td>Increment</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($increment_details)){
					foreach($increment_details as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->increment_type;?></td>
					<td><?php echo $increment->increment_amount;?></td>
					<td><a href="human_resource/edit_emp_single_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>
    <script type="text/javascript">

    </script>
		
	</div>
<!-- contents -->
<script>
    $(document).ready(function(e){
        $('input[name="add_increment"]').on('click', function (e) {
            var incrementTextBox = $('input[name="increment_amount"]').val();
            if(incrementTextBox.length === 0){
                Parexons.notification('Please Enter Increment Amount','warning');
                return false;
            }
            var dateTextBox = $('#training_date').val();
            if(dateTextBox.length === 0){
                Parexons.notification('Please Select Date From DatePicker','warning');
                return false;
            }
            return true;
        });
    });
</script>
<script>
    //$("#alert").delay(3000).fadeOut('slow');
    $( "#date_effective" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
        /*onClose: function( selectedDate ) {
         $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
         }*/

    });
</script>

<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->
