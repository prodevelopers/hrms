<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Add Skills</div>
        <?php echo form_open();?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
                    
				<br class="clear">
				<div class="row">
					<h4>Skill Name</h4>
                    <?php echo @form_dropdown('ml_skill_type_id', $skill_type,'required="required"','id="drop_skill"');?>
					<a id="ski"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Skill Level</h4>
                    <?php echo @form_dropdown('level', $skill_level,'','required="required"');?>
				</div>

				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="comments">
				</div>

				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_skill" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close();?>				
				
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Skill Name</td>
					<td>Level</td>
					<td>Remarks</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($skill_details)){
					foreach($skill_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->skill_name;?></td>
					<td><?php echo $row->skill_level;?></td>
					<td><?php echo $row->comments;?></td>
					<td><a href="human_resource/edit_single_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>/<?php echo 'skill_record_id';?>" onclick="return confirm('Are You Sure...!')" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<!-- Designaiton dialog -->
	<div id="skills" title="Add Skills" style="display:none; width:600px;">
	<form id="skillForm" action="human_resource/add_skill/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="skill_name" id="txt_skill"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
$( "#accordion" ).accordion();
$( "#accordion1" ).accordion();
</script>
<script>
	$(document).ready(function()
	{
		$("#skills").on('click',function(e){
			e.preventDefault();
			var formData = $('#skillForm').serialize();
			$.ajax({
				url: "human_resource/add_skill/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_skill').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_skill').append(appendData);
						$("#skills").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});

        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>

	});
</script>
