<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Skills</div>
        <?php echo form_open('human_resource/edit_emp_single_skill/'.$employee->employee_id.'/'.$skill->skill_record_id);?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
                    
				<br class="clear">
				<div class="row">
					<h4>Skill Name</h4>
                    <?php 
					$slctd_skill_name = (isset($skill->ml_skill_type_id) ? $skill->ml_skill_type_id : '');
					echo @form_dropdown('ml_skill_type_id', $skill_type, $slctd_skill_name,'required="required"','required="required"');?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

				<div class="row">
					<h4>Experience In Years</h4>
                    <?php //echo @form_dropdown('experience_in_years', $year);?>
					<select name="experience_in_years">
						<option value="">-- Select Experience in Year --</option>
						<option value="1"<?php if($skill->experience_in_years == 1){echo "selected = 'selected'";} ?>>1</option>
						<option value="2"<?php if($skill->experience_in_years == 2){echo "selected = 'selected'";} ?>>2</option>
						<option value="3"<?php if($skill->experience_in_years == 3){echo "selected = 'selected'";} ?>>3</option>
						<option value="4"<?php if($skill->experience_in_years == 4){echo "selected = 'selected'";} ?>>4</option>
						<option value="5"<?php if($skill->experience_in_years == 5){echo "selected = 'selected'";} ?>>5</option>
					</select>
				</div>

				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="comments" value="<?php echo $skill->comments ?>">
				</div>

				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_skill" value="Update" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close();?>				
				
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Skill Name</td>
					<td>Experience</td>
					<td>Remarks</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($skill_details)){
					foreach($skill_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->skill_name;?></td>
					<td><?php echo $row->experience_in_years;?></td>
					<td><?php echo $row->comments;?></td>
					<td><a href="human_resource/edit_emp_single_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Trainings</div>
			<?php echo form_open('human_resource/edit_emp_single_skill/'.$employee->employee_id.'/'.$training->training_record_id); ?>
        	<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<br class="clear">
				<div class="row">
					<h4>Training Name</h4>
					<input type="text" name="training_name" value="<?php echo $training->training_name; ?>">
				</div>

				<div class="row">
					<h4>Date</h4>
					<input type="text" name="date" id="joining_date" value="<?php echo $training->date; ?>">
				</div>

				<div class="row">
					<h4>Description</h4>
					<textarea name="description"><?php echo $training->description; ?></textarea>
				</div>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_training" value="Update" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>
			
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Training</td>
					<td>Date</td>
					<td>Description</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($training_detail)){
					foreach($training_detail as $train){ ?>
				<tr class="table-row">
					<td><?php echo $train->training_name;?></td>
					<td><?php echo $train->date;?></td>
					<td><?php echo $train->description;?></td>
					<td><a href="human_resource/edit_emp_single_skill/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_training/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->