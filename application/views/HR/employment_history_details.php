
<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 12/25/2014
 * Time: 3:27 PM
 */ ?>

<style>
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    /*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
    .move{
        position: relative;
        left: 48%;
        top: 60px;
    }
    .resize{
        width: 220px;
    }
    .rehieght{
        position: relative;
        top: -5px;
    }
    .dataTables_length{
        position: relative;
        left: -0.2%;
        top: -20px;
        font-size: 1px;
    }
    .dataTables_length select{
        width: 60px;
    }

    .dataTables_filter{
        position: relative;
        top: -21px;
        left: -19%;
    }
    .dataTables_filter input{
        width: 180px;
    }
    .revert{
        margin-left: -15px;
    }
    .marge{
        margin-top: 40px;
    }
</style>



<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Employement's Position History</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


    <div class="right-contents">

        <div class="head">Employement's Position History</div>

      <!--  <!-- filter -->
       <!-- <div class="filter">
            <h4>Filter By</h4>
            <input type="hidden" id="selectEmployees">
        </div>-->

        <!-- table -->
        <table class="table" id="employmentHistoryDetailsList" cellspacing="0">
            <thead class="table-head">
            <tr id="table-row">
                <td>EmploymentID</td>
                <td>Designation</td>
                <td>Grade</td>
                <td>Department</td>
                <td>Projects</td>
                <td>WorkLocation</td>
                <td>FromDate</td>
                <td>ToDate</td>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <!--<a href="assign_position.php"><button class="btn green"></button></a>-->

            </div>
        </div>
    </div>
</div>
</div>
<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
    $(document).ready(function(e) {
        oTable = '';

            var EmpID = <?php echo $employeeID ?>;
            var EmploymentID = <?php echo $employmentID ?>;
            //DataTables Function Start Here..
            var selector = $('#employmentHistoryDetailsList');
            var url = "<?php echo base_url(); ?>human_resource/employment_history_details_DT";
            var aoColumns = [
                /* ID */ {
                    "mData": "EmploymentID",
                    "bVisible": false,
                    "bSortable": false,
                    "bSearchable": false
                },
                /* Designation */ {
                    "mData": "Designation"
                },
                /* Grade */ {
                    "mData": "Grade"
                },
                /* Departments List */ {
                    "mData": "DepartmentName"
                },
                /* Projects */ {
                    "mData": "Projects"
                },
                /* Branch */ {
                    "mData": "Branch"
                },
                /* From Date */ {
                    "mData": "FromDate"
                },
                /* To Date */ {
                    "mData": "ToDate"
                }
            ];
            var HiddenColumnID = 'EmploymentID';
            var sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
            var filters = 'aoData.push({"name":"EmpID","value":'+EmpID+'}),aoData.push({"name":"Employment","value":'+EmploymentID+'});';
//        commonDataTables(selector,url,aoColumns,sDom,HiddenColumnID);
        commonDataTablesFiltered(selector,url,aoColumns,sDom,HiddenColumnID,filters);
            //So End Of Data Tables Main Function.
        });
</script>

<!--Left Menu Setting--------------------------------------->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->