<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Qualification</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<script type="text/javascript">
				 $(document).ready(function(){
				 	$(".diploma-field").hide();
				 	$(".degree-field").hide();
				 	$(".certificate-field").hide();
      			 $("select").change(function(){
            $( "select option:selected").each(function(){
                if($(this).attr("value") == "1"){
                    $(".degree-field").show();
                    $(".certificate-field").hide();
                    $(".diploma-field").hide();
                }
                if($(this).attr("value") == "2"){
                    $(".degree-field").hide();
                    $(".certificate-field").hide();
                    $(".diploma-field").show();
                }
                if($(this).attr("value") == "3"){
                    $(".degree-field").hide();
                    $(".certificate-field").show();
                    $(".diploma-field").hide();
                }
            });
        }).change();
    });
			</script>

	<div class="right-contents1">
		<div class="head">Qualification</div>
		<div class="row">
			Qualification 
		</div>
        <?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
                    <?php 
					$selectd_type = (isset($qualify->qualification_type_id) ? $qualify->qualification_type_id : '');
					echo @form_dropdown('qualification_type_id', $qualification, $selectd_type,'required="required"','required="required"'); ?>
				</div>
				<br class="clear">
				<div class="row degree-field">
					<h4>Degree/Level</h4>
					<input type="text" name="qualification1" value="<?php echo @$qualify->qualification; ?>">
				</div>
				<div class="row degree-field">
					<h4>Institute</h4>
					<input type="text" name="institute1" value="<?php echo @$qualify->institute; ?>">
				</div>
				<br class="clear">
				<div class="row degree-field">
					<h4>Specialization</h4>
					<input type="text" name="major_specialization" value="<?php echo @$qualify->major_specialization; ?>">
				</div>
				<div class="row degree-field">
					<h4>Year of Completion</h4>
					<input type="text" name="year1" id="datebirth" value="<?php echo date_format_helper(@$qualify->year); ?>">
				</div>
				<br class="clear">
				<div class="row degree-field">
					<h4>GPA / Division</h4>
					<input type="text" name="gpa_score" value="<?php echo @$qualify->gpa_score; ?>">
				</div>

				<div class="row diploma-field">
					<h4>Diploma</h4>
					<input type="text" name="qualification2" value="<?php echo @$qualify->qualification; ?>">
				</div>

				<div class="row diploma-field">
					<h4>Institute</h4>
					<input type="text" name="institute2" value="<?php echo @$qualify->institute; ?>">
				</div>

				<br class="clear">
				<div class="row diploma-field">
					<h4>Duration</h4>
					<input type="text" name="duration" value="<?php echo @$qualify->duration; ?>">
				</div>

				<div class="row diploma-field">
					<h4>Year</h4>
					<input type="text" name="year2" id="year2" value="<?php echo date_format_helper($qualify->year); ?>">
				</div>

				<div class="row certificate-field">
					<h4>Certificate</h4>
					<input type="text" name="qualification3" value="<?php echo @$qualify->qualification; ?>">
				</div>

				<div class="row certificate-field">
					<h4>Institute</h4>
					<input type="text" name="institute3" value="<?php echo @$qualify->institute; ?>">
				</div>
				<br class="clear">
				<div class="row certificate-field">
					<h4>Year</h4>
					<input type="text" name="year3" id="year3" value="<?php echo date_format_helper($qualify->year); ?>">
				</div>
                
			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>

		</div>


		<?php /*?><div class="right-contents" style="margin-top:20px;">
			<div class="head">Added Education & Qualification</div>

			<table cellspacing="0" >
				<thead class="table-head secondry">
					<td>Qualification</td>
					<td>Type</td>
					<td>Institute</td>
					<td>Year</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($qualification_detail)){
					foreach($qualification_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->qualification; ?></td>
					<td><?php echo $row->qualification_title; ?></td>
					<td><?php echo $row->institute; ?></td>
					<td><?php echo $row->year; ?></td>
					<td><a href="human_resource/send_2_trash_qualification/<?php echo $employee->employee_id; ?>/<?php echo $row->qualification_id; ?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>				
			</table>			
			
		</div><?php */?>

	</div>
<!-- contents -->
<script>

    $( "#datebirth,#year2,#year3" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"


    });
</script>