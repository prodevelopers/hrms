<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -0.2%;
	top: -20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"job_title","value":$('#jobtitle').val()});
			aoData.push({"name":"designation","value":$('#design').val()});
					        
                
		}
                
	});
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Employement Contract</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_left_nav'); ?>
	

	<div class="right-contents">

		<div class="head">Employee Contrat</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                 <?php echo form_open();?>
				<!--<input type="text" placeholder="Employee Name">
				<select>
					<option>Job Title</option>
				</select>
				<select>
					<option>Designation</option>
				</select>-->
                <?php //echo @form_dropdown('job_title', $job_title, $title, "class='resize' id='jobtitle' onChange='this.form.submit()'"); ?>
			<?php echo @form_dropdown('designation', $designation, $design, "class='resize' id='design' onChange='this.form.submit()'"); ?>

				<!--<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>-->
                <?php echo form_close();?>
			</div>


			<!-- table -->
            <table class="table" id="table_list" cellspacing="0">
                   <thead class="table-head">
                   <tr id="table-row">
                    <td>Employee Code</td>   
					<td>Employee Name</td>
                    <td>Designation</td>
					<!--<td width="20%">Current Job</td>
					<td>Job Specification</td>-->
					<!--<td width="20%">User Role</td>
					<td>Status</td>-->
				<td><span class="fa fa-eye"></span></td>
					
					
                           
                    </tr>
                </thead>
                    <tbody>
                    </tbody>
         </table>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Current Position</td>
					<td>Current Job</td>
					<td>Job Category</td>
					<td>Status</td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
			</table>-->

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="assign_position.php"><button class="btn green"></button></a>-->
					 
				</div>
			</div>
		</div>
	</div>
	</div>
<!-- contents -->


