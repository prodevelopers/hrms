<?php include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php include('includes/hr_left_nav.php'); ?>
	

	<div class="right-contents">

		<div class="head">Master Lists</div>

			<?php include('includes/hr_master_list_nav.php'); ?>	
			

			<div class="right-list">
				<form>

				<div class="row">
					<h4><b>Job Type</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Job Type</h4>
					<input type="text">
				</div>
				
				<!-- button group -->
			<div class="row">
					<a href="add.php"><button class="btn green">Add</button></a>
			</div>
				
			</form>


			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Job Type</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Job Type</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>
			</div>

			

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
