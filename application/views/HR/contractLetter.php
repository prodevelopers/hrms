<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan.
 * Date: 1/13/2015
 * Time: 5:49 PM
 */

if(isset($organizationData)){
    $orgName = $organizationData->org_name;
    $orgAddress = $organizationData->address;
    $orgCity = $organizationData->city;
    $orgCountry = $organizationData->country;
    $orgPhone = $organizationData->phone;
    $orgEmail = $organizationData->email;
    $orgWebsite = $organizationData->website;
}
else{
    $orgName = 'Company Name';
    $orgCity = 'City';
    $orgCountry = 'Country';
    $orgAddress = 'Company Address';
    $orgPhone = 'Company Phone';
    $orgEmail = 'Company Email';
    $orgWebsite = 'Company Website';
}
if(isset($employeeData) && !empty($employeeData)){
    $eID = $employeeData->EmployeeID;
    $eFullName = $employeeData->FullName;
    $eAddress = $employeeData->Address;
    $ePosition = $employeeData->Position;
    $eStartDate = $employeeData->StartDate;
    $eEndDate = $employeeData->EndDate;
    $eBaseSalary = $employeeData->BaseSalary;
}else{
    $eID = '';
    $eFullName = 'Employee Name';
    $eAddress = 'Employee Address';
    $ePosition = 'Employee Position';
    $eStartDate = 'Contract Start Date';
    $eEndDate = 'Contract Expiry Date';
    $eBaseSalary = 'Employee Base Salary';
}
?>
<style>
    select[class="tinyeditor-size"],
    select[class="tinyeditor-style"],
    select[class="tinyeditor-font"]{width:140px;}
    address{
        font-style: normal;
        font-size: 14px;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/jeditor/tinyeditor.css">
<script src="<?php echo base_url();?>assets/jeditor/tiny.editor.packed.js"></script>
<div class="contents-container">

    <div class="bredcrumb">Dashboard / Human Resource / Contract</div> <!-- bredcrumb -->

    <?php $this->load->view('includes/edit_employee_left_nav'); ?>

    <div class="right-contents1">

        <div class="head">Contract</div>
        <div class="notice info">
            <p style="color: orange">Read before Generating Contact Letter</p>
            <ul>
                <li>Current Contact is Required for Employee</li>
                <li>Employment is Required for Employee</li>
                <li>Employment Approval is Required for Employee.</li>
                <li>Employee must have an Active and Approved  <strong>Position</strong>.</li>
                <li>Employee Salary Must have been set.</li>
                <li>Employee must have an Active Contract.</li>
            </ul>
        </div>
        <div class="right-list">
            <form action="">
                <div class="row">
<!--                    <address>
                        <strong><?php /*echo $orgName; */?></strong><br>
                        <abbr><?php /*echo $orgAddress; */?></abbr><br>
                        <abbr title=""><?php /*echo $orgPhone; */?></abbr><br>
                        <abbr title=""><?php /*echo $orgEmail; */?></abbr><br>
                        <abbr title=""><?php /*echo $orgWebsite; */?></abbr><br>
                        <abbr title=""><?php /*echo date('l jS \of F Y');*/?></abbr><br><br>
                    </address>-->
                    <address>
                        <abbr title=""><strong><?php echo $eFullName; ?></strong></abbr><br>
                        <abbr title="">Address: </abbr><?php echo $eAddress; ?><br>
                        <abbr title="">Position: </abbr><?php echo $ePosition; ?><br>
                        <abbr title="">Start Date: </abbr><?php echo date_format_helper($eStartDate); ?><br>
                        <abbr title="">End Date: </abbr><?php echo date_format_helper($eEndDate); ?><br>
                        <abbr title="">Base Salary: <b><?php echo $eBaseSalary ?></b></abbr><br>
                    </address>
                </div>
                <div class="row2">
                    <textarea id="tinyeditor" name="content" style="width:100%">
                        <?php if(isset($letterData)){ echo $letterData->LetterText; } ?>
                    </textarea>
                </div>

            </form>
            <div class="row">
                <div class="button-group">
                    <button class="btn green" id="Generate" <?php echo isset($employeeData) && !empty($employeeData)? '':'disabled="disabled" style="background:grey;"' ?> >Generate</button>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- contents -->
<script>


    $(document).ready(function (e) {
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });

        //Function When Clicked on Generate.
        $('#Generate').on('click', function (e) {
            tinymce.triggerSave();
            var data = {
                tinyData: $('#tinyeditor').val()
            };
            $.ajax({
                url: '<?php echo base_url() ?>hr_site/update_contractLetter',
                type: 'POST',
                data: data,
                success: function (output) {
                    var data = output.split('::');
                    if(data[0] == 'OK'){
                        console.log('Im OK');
                        window.location.href = "<?php echo base_url() ?>hr_site/contract_letter/<?php echo $eID ?>";
                    } else{
                        console.log('Im Not OK');
                        window.location.href = "<?php echo base_url() ?>hr_site/contract_letter/<?php echo $eID ?>";
                    }
                }
            });
        });

        <?php
 if(!isset($employeeData) || empty($employeeData)){
    ?>
        Parexons.notification('Employee Information is Not Available At The Moment, Make Sure Employee is Active in System','error');
        <?php
         }
         ?>

    });
</script>
<?php //include('includes/footer.php'); ?>
