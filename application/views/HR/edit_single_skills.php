<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Skills</div>
        <?php echo form_open('human_resource/edit_emp_single_skill/'.$employee->employee_id.'/'.$skill->skill_record_id);?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
                    
				<br class="clear">
				<div class="row">
					<h4>Skill Name</h4>
                    <?php 
					$slctd_skill_name = (isset($skill->ml_skill_type_id) ? $skill->ml_skill_type_id : '');
					echo @form_dropdown('ml_skill_type_id', $skill_type, $slctd_skill_name,'required="required"','required="required"');?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

				<div class="row">
					<h4>Skill Level</h4>
                    <?php $skill_level_name= (isset($skill->ml_skill_level_id) ? $skill->ml_skill_level_id : '');
                     echo @form_dropdown('level', $skill_level,$skill_level_name);?>

				</div>

				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="comments" value="<?php echo $skill->comments ?>">
				</div>

				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_skill" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close();?>
		</div>
	</div>
<!-- contents -->