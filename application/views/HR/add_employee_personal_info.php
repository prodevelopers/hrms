<script type="text/javascript">
 function check_cnic()
 {
	 $(document).ready(function () 
	 {
		 var fisrt_part = $('#nic1').val();
		 var second_part = $('#nic2').val();
		 var third_part = $('#nic3').val();
		 var cnic = fisrt_part +'-'+second_part +'-'+third_part;
		 $.ajax({
			 type: "POST",
			 url: "human_resource/check_cnic_exist",
			 data: {cnic:cnic} ,
			 success: function(data) 
			 {
				 $('#check_cnic').html(data);
			 }
		 });
	 });  
 }
/// To Forward Focus to Next Textfield
$(document).ready(function()
{
    $('input').keyup(function()
	{
        if(this.value.length == $(this).attr("maxlength"))
		{
            $(this).next().focus();
        }
    });
});
</script>
<?php
//$photo 	= $employee->thumbnail;
$exp 	= explode('-', $employee->CNIC);
 ?>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Personal Info</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>

	<div class="right-contents1">

		<div class="head">Personal Info</div>

			<?php //echo form_open(); ?>
            <form action="" method="post" enctype="multipart/form-data" name="form1" onsubmit="return validation()">
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Full Name</h4>
					<input type="text" placeholder="Employee Name" name="full_name" value="<?php echo @$employee->first_name." ".$employee->last_name; ?>">
				</div>

				<div class="row">
					<h4>Employee ID</h4>
					<input type="text" name="employee_code" value="<?php echo @$employee->employee_code; ?>" readonly="readonly">
				</div>

				<br class="clear"> 
				<div class="row">
					<h4>Father Name</h4>
					<input type="text" name="father_name">
				</div>

				<div class="row">
					<h4>CNIC</h4>
					<input type="text" id="input" name="nic1" style="width: 40px; float: left;" maxlength="5" placeholder="13568" value="<?php echo @$exp[0]; ?>" readonly>
					<input type="text" id="input1" name="nic2" style="width: 110px; float: left;" maxlength="7" placeholder="2684359" value="<?php echo @$exp[1]; ?>" readonly>
					<input type="text" id="input2" name="nic3" style="width: 20px; float: left;" maxlength="1" placeholder="7" onkeyup="return check_cnic()" value="<?php echo @$exp[2]; ?>" readonly>
					<div id="check_cnic"></div>
				</div>

				<div class="row">
					<h4>Gender</h4>
                    <?php echo @form_dropdown('gender', $gender,'required="required"','id="drop_gender"'); ?>
                    <a id="gender"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Marital Status</h4>
                    <?php echo @form_dropdown('marital_status', $marital_status,'required="required"','id="drop_status"'); ?>
                    <a id="mari"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Date of Birth</h4>
					<input type="text" name="date_of_birth" id="date_of_birth">
				</div>
				<div class="row">
					<h4>Nationality</h4>
                    <?php echo @form_dropdown('nationality', $nationality,'required="required"','id="drop_nation"'); ?>
                    <a id="nation"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Religion</h4>
                    <?php echo @form_dropdown('religion', $religion,'required="required"','id="drop_religion"'); ?>
                    <a id="reli"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Employee Tax No</h4>
					<input type="text" name="tax_number">
				</div>

				

				<br class="clear">

				<!-- hide show -->
				<script type="text/javascript">
					
					$(document).ready(function(){
					    $(".immigration-fields").hide();
					    $('input[type="checkbox"]').click(function(){
					        if($(this).attr("value")=="Immigration"){
					            $(".immigration-fields").toggle();
					        }
					    });
					});
				</script>
				<div class="row">
					<h4><b>Immigration</b></h4>
					<input type="checkbox" value="Immigration">
				</div>

				<br class="clear">

				<div class="row immigration-fields">
					<h4>Passport Number</h4>
					<input type="text" name="passport_num">
				</div>

				<div class="row immigration-fields">
					<h4>Visa (Country)</h4>
					<input type="text" name="visa_num">
				</div>

				<br class="clear"> 

				<div class="row immigration-fields">
					<h4>Visa Expiry Date</h4>
					<input type="text" name="visa_expiry" id="visa_expiry" value="<?php echo @$immigration->visa_expiry; ?>">
				</div>

				<!--<div class="row immigration-fields">
					<h4>Visa Expiry Date</h4>
					<input type="text" name="visa_expiry">
				</div>-->

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
					<button class="btn gray">Reset</button>
				</div>
			</div>
            </form>
            <?php //echo form_close(); ?>

			

		</div>

	</div>
		<!-- Dialog Sections -->
	<!-- Gender dialog -->
	<div id="dialog" title="Add Gender" style="display:none; width:600px;">
	<form id="genderForm" action="human_resource/add_gender/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="gender_type_title" id="txt_gender"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="gender">
		</div>
	</form>
	</div>
	<!-- Nationality Dialog -->
	<div id="nationality" title="Add Nationality" style="display:none; width:600px;">
	<form id="nationForm" action="human_resource/add_nation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="nationality" id="txt_nation"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add_nation" id="nation">
		</div>
	</form>
	</div>
	<!-- Religion Dialog -->
	<div id="religion" title="Add Religion" style="display:none; width:600px;">
	<form id="religionForm" action="human_resource/add_religion/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="religion_title" id="txt_religion"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="religion">
		</div>
	</form>
	</div>
	<div id="marital" title="Add Marital Status" style="display:none; width:600px;">
	<form id="maritalFrom" action="human_resource/add_marital/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="marital_status_title" id="txt_status"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="marital">
		</div>
	</form>
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<script>
	function validation()
	{

		if(document.form1.gender.value=="0")
		{alert("Please select Gender !");
		return false;
		
		}
		if(document.form1.marital_status.value=="0")
		{alert("Please select Marital Status !");
		return false;
		
		}
		if(document.form1.nationality.value=="0")
		{alert("Please select Nationality !");
		return false;
		
		}
		if(document.form1.religion.value=="0")
		{alert("Please select Religion !");
		return false;
		
		}
		
	}
	$(document).ready(function()
	{
		$("#dialog").on('click',function(e){
			e.preventDefault();
			var formData = $('#genderForm').serialize();
			$.ajax({
				url: "human_resource/add_gender/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);

						$("#dialog").dialog("close");
						var provinceNameEnterdValue = $('#txt_gender').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_gender').append(appendData);
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#nationality").on('click',function(e){
			e.preventDefault();
			var formData = $('#nationForm').serialize();
			$.ajax({
				url: "human_resource/add_nation/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_nation').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_nation').append(appendData);
						$("#nationality").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#religion").on('click',function(e){
			e.preventDefault();
			var formData = $('#religionForm').serialize();
			$.ajax({
				url: "human_resource/add_religion/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_religion').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_religion').append(appendData);
						$("#religion").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#marital").on('click',function(e){
			e.preventDefault();
			var formData = $('#maritalFrom').serialize();
			$.ajax({
				url: "human_resource/add_marital/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_status').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_status').append(appendData);
						$("#marital").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

    //My Code Need To Check Validations SHH
    $(document).ready(function(e){
        var formSelector = $('form[name="form1"]');
        var submitButton = formSelector.find('input[type="submit"]');
        submitButton.on('click',function(e){
            e.preventDefault();
            var empName = formSelector.find('input[name="full_name"]').val();
            var empCode = formSelector.find('input[name="employee_code"]').val();
            var CNIC1 = formSelector.find('input[name="nic1"]').val();
            var CNIC2 = formSelector.find('input[name="nic2"]').val();
            var CNIC3 = formSelector.find('input[name="nic3"]').val();
            var taxNumber = formSelector.find('input[name="tax_number"]').val();
            var genderDropDown = formSelector.find('#drop_gender').val();
            var maritalStatusDropDown = formSelector.find('#drop_status').val();
            var nationalityDropDown = formSelector.find('#drop_nation').val();
            var religionDropDown = formSelector.find('#drop_religion').val();
            var dateOfBirthDropDown = formSelector.find('#date_of_birth').val();

            if(empName.length < 3){
                Parexons.notification('Name Must Be At Least 3 Characters long','warning');
                return false;
            }
            if(empCode.length === 0){
                Parexons.notification('There is Some Problem in Posting This Page, Please Contact System Administrator For Further Assistance','warning');
                return false;
            }
            if(CNIC1.length < 5 || CNIC1.length > 5){
                Parexons.notification('Please Enter Valid CNIC','warning');
                return false;
            }
            if(CNIC2.length < 7 || CNIC2.length > 7){
                Parexons.notification('Please Enter Valid CNIC','warning');
                return false;
            }
            if(CNIC3.length < 1 || CNIC3.length > 1){
                Parexons.notification('Please Enter Valid CNIC','warning');
                return false;
            }
            if(genderDropDown.length === 0){
                Parexons.notification('Please Select Gender From DropDown','warning');
                return false;
            }
            if(maritalStatusDropDown.length === 0){
                Parexons.notification('Please Select Marital Status From DropDown','warning');
                return false;
            }
            if(nationalityDropDown.length === 0){
                Parexons.notification('Please Select Nationality From DropDown','warning');
                return false;
            }
            if(religionDropDown.length === 0){
                Parexons.notification('Please Select Religion From DropDown','warning');
                return false;
            }
            if(dateOfBirthDropDown.length === 0){
                Parexons.notification('Please Select <b>Date Of Birth</b> From DatePicker','warning');
                return false;
            }
            if(taxNumber.length === 0){
                //No Need To Prevent Submit For Tax. As Tax Number is Optional..
            }
            formSelector.submit();
        });

    });

    $( "#date_of_birth").datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM", maxDate:'0',
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"

    });
    $( "#visa_expiry" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });


</script>