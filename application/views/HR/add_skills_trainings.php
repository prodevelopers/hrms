<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Add Skills</div>
        <?php echo form_open('human_resource/add_emp_skill'/$this->uri->segment(3));?>
        <input type="hidden" name="employment_id" value="<?php echo $empl->employment_id; ?>" />
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
				<br class="clear">
				<div class="row">
					<h4>Skill Name</h4>
                    <?php echo @form_dropdown('ml_skill_type_id', $skill_type,'required="required"','required="required"','id="drop_skill"');?>
					<a id="ski"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
		<div class="row">
			<h4>Skill Level</h4>
			<?php echo @form_dropdown('Skill_level', $skill_level,'required="required"','required="required"');?>

		</div>

				<br class="clear">
				<div class="row">
					<h4>Remarks</h4>
				<textarea  name="comments" id="" cols="30" rows="6"></textarea>
				</div>
				<br class="clear">
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close();?>				
				
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Skill Name</td>
					<td>Level</td>
					<td>Remarks</td>
					<td>Trash</td>
				</thead>
                <?php
				if(!empty($skill_details)){

					foreach($skill_details as $row){
						$count=count($row);
						//$total1 = $row->experience_in_years;

						?>
				<tr class="table-row">
					<td><?php echo $row->skill_name;?></td>
					<td><?php echo $row->skill_level;?></td>
					<td><?php echo $row->comments;?></td>
					<td><a href="human_resource/send_2_trash_skill_add/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>/<?php echo 'skill_record_id';?>" onclick="return confirm('Are you Sure...!')"><span class="fa fa-trash-o"></span></a></td>

				</tr>

						<?php }}else{?>
					<td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
				<?php }?>

			</table>
		</div>

		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Add Trainings</div>
			<?php echo form_open('human_resource/add_emp_training'); ?>
        	<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<br class="clear">
				<div class="row">
					<h4>Training Name</h4>
					<input type="text" name="training_name" required="required">
				</div>
			<br class="clear">
			<div class="row">
				<h4>Training Institute</h4>
				<input type="text" name="Institute" required="required">
			</div>
			<br class="clear">
				<div class="row">
					<h4>From Date</h4>
					<input type="text" name="frm_date" id="frm_date" onchange="mydata2();" required="required">
				</div>
			<br class="clear">
			<div class="row">
				<h4>To Date</h4>
				<input type="text" name="to_date" id="to_date" onchange="mydata2();" required="required">
			</div>
				<br class="clear">
			<div class="row">
				<h4>Total Days:</h4>
				<input type="text" name="tdays" id="tdays2" value="" readonly>
			</div>
				<br class="clear">
				<div class="row">
					<h4>Description</h4>
					<textarea name="description"></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>
			
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Training</td>
					<td>Institute </td>
					<td>From Date</td>
					<td>To Date</td>
					<td>Total Duration</td>
					<td>Description</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($training_detail)){
					foreach($training_detail as $train){ ?>
				<tr class="table-row">
					<td><?php echo $train->training_name;?></td>
					<td><?php echo $train->Institute;?></td>
					<td><?php echo date_format_helper($train->frm_date);?></td>
					<td><?php echo date_format_helper($train->to_date);?></td>
					<td><?php echo $train->tdays;?></td>
					<td><?php echo $train->description;?></td>
					<td><a href="human_resource/send_2_trash_training_add/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>/<?php echo 'training_record_id';?>" onclick="return confirm('Are you Sure...!')" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->
<!-- Dialog Sections -->
	<!-- skills dialog -->
	<div id="skills" title="Add Skills" style="display:none; width:600px;">
	<form id="skillForm" action="human_resource/add_skill/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="skill_name" id="txt_skill"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<script>
	function mydata(){

		var fromDate=$("#applyForLeaveFromDate").val();
		var toDate=$("#applyForLeaveToDate").val();

		//console.log(fromDate.length);
if(fromDate.length === 0 || toDate.length === 0){
	return;
}
		fromDate = fromDate.split('-');
		toDate = toDate.split('-');

		var diff = new Date(Date.parse(toDate[2]+'-'+toDate[1]+'-'+toDate[0]) - Date.parse(fromDate[2]+'-'+fromDate[1]+'-'+fromDate[0]));
		var totalDays = diff / 1000 / 60 / 60 / 24;
		//Assign The Total Days to The TextField.

		$("#tdays").val(totalDays);
	}
</script>

<script>
	$("#alert").delay(3000).fadeOut('slow');
	$( "#applyForLeaveFromDate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#applyForLeaveToDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#applyForLeaveToDate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#applyForLeaveFromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
</script>
<script>
	function mydata2(){

		var fromDate=$("#frm_date").val();
		var toDate=$("#to_date").val();
		if(fromDate.length === 0 || toDate.length === 0){
			return;
		}
        fromDate = fromDate.split('-');
        toDate = toDate.split('-');

		var diff = new Date(Date.parse(toDate[2]+'-'+toDate[1]+'-'+toDate[0]) - Date.parse(fromDate[2]+'-'+fromDate[1]+'-'+fromDate[0]));
		var totalDays = diff / 1000 / 60 / 60 / 24;
		//Assign The Total Days to The TextField.
		$("#tdays2").val(totalDays);
	}
</script>

<script>
	$("#alert").delay(3000).fadeOut('slow');
	$( "#frm_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#to_date" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#to_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#frm_date" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	$(document).ready(function()
	{
		$("#skills").on('click',function(e){
			e.preventDefault();
			var formData = $('#skillForm').serialize();
			$.ajax({
				url: "human_resource/add_skill/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_skill').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_skill').append(appendData);
						$("#skills").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});
$('#tdays2').attr('readonly','readonly');
	});

</script>

    <script>
        $(document).ready(function()
        {
            <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
        if(!empty($message) && isset($message)){
                echo "message = '".$message."';"; ?>
            var data = message.split("::");
            Parexons.notification(data[0],data[1]);
            <?php
            }
            }
        }
        ?>

        });
    </script>
