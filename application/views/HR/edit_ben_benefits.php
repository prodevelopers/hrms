<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_left_nav'); ?>
	
		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Benefits</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        	
				<div class="row">
					<h4>Benefit type</h4>
					<?php 
					$slctd_benefits = (isset($benefit->benefit_type_id) ? $benefit->benefit_type_id : '');
					echo @form_dropdown('benefit_type_id', $benefit_type, $slctd_benefits,'required="required"','required="required"'); ?>
				</div>

<!--				<br class="clear">-->
<!--				<div class="row">-->
<!--					<h4>Benefit</h4>-->
<!--					--><?php //
//					$slctd_benefit_list = (isset($benefit->benefits_list_id) ? $benefit->benefits_list_id : '');
//					echo @form_dropdown('benefits_list_id', $benefit_list, $slctd_benefit_list); ?>
<!--				</div>-->
        		<br class="clear">
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="date_effective" value="<?php echo date_format_helper($benefit->date_effective); ?>">
				</div>
                
			<br class="clear">			
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_benefit" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>

		</div>
	</div>
<!-- contents -->

<script>
    $("#alert").delay(3000).fadeOut('slow');
    $( "#date_effective" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
        /*onClose: function( selectedDate ) {
         $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
         }*/

    });
</script>