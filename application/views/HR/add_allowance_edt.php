<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#month").hide();
            $("#year").hide();
            $("#frequ").change(function(){
                $( "select option:selected").each(function(){
                    if($(this).attr("value") == "1"){
                        $("#month").hide();
                        $("#year").hide();
                    }
                    if($(this).attr("value") == "2"){
                        $("#month").show();
                        $("#year").hide();
                    }
                    if($(this).attr("value") == "3"){
                        $("#year").show();
                        $("#month").show();
                    }
                });
            }).change();
        });
    </script>
		<div class="right-contents1">

		<div class="head">Allowances</div>
			<div id="txt" style="background-position: center">
				<?php echo  $this->session->flashdata('txt');?></div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
				<div class="row">
					<h4>Allowance type</h4>
					<?php echo @form_dropdown('ml_allowance_type_id', $allowance_type,'required = "required"','id = "drop_allow"'); ?>
					<a id="at"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Pay Frequency</h4>
					<?php echo @form_dropdown('pay_frequency', $pay_frequency,'required = "required"','required = "required" id="frequ"'); ?>
				</div>
            <div class="row" id="month">
                <h4>Monthly</h4>
                <input type="text" name="allowance_amount">
            </div>
            <div class="row" id="year">
                <h4>Yearly</h4>
                <input type="text" name="allowance_amount">
            </div>
            <br class="clear"/>
            <div class="row">
                <h4>Amount</h4>
                <input type="text" name="allowance_amount">
            </div><div class="row">
					<h4>Date Effective From</h4>
					<input type="text" name="effective_date" id="effective_date">
				</div>
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_allowance" value="Add" class="btn green" />
                <a href="human_resource/edit_emp_entitlement_increment/<?php echo $employee->employee_id;?>"> <input type="button" value="Cancel" class="btn gray" /></a>
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Pay Frequency</td>
					<td>Ammount</td>
					<td>Date Effective From</td>
					<td>Approved Date</td>
					<td>Approved By</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($allowance_details)){
					foreach($allowance_details as $allowance){?>
				<tr class="table-row">
					<td><?php echo @$allowance->allowance_type;?></td>
					<td><?php echo @$allowance->pay_frequency;?></td>
					<td><?php echo @$allowance->allowance_amount;?></td>
					<td><?php echo @$allowance->effective_date;?></td>
					<td><?php echo @$allowance->date_approved;?></td>
					<td><?php echo @$allowance->emp_name;?></td>
					<td><a href="human_resource/edit_emp_single_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
	
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	
	<div id="all-type" title="Add Allowance" style="display:none; width:600px;">
	<form id="allowForm" action="human_resource/add_allowance/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="allowance_type" id="txt_allow"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
</script>
<script>

	$(document).ready(function()
	{
  //////////////// Notification Message ////////////////////

        <?php if(!empty($pageMessages) && is_array($pageMessages)){
          echo "var message;";
          foreach($pageMessages as $key=>$message){
              if(!empty($message) && isset($message)){
                      echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>


		$("#all-type").on('click',function(e){
			e.preventDefault();
			var formData = $('#allowForm').serialize();
			$.ajax({
				url: "human_resource/add_allowance/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_allow').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_allow').append(appendData);
						$("#all-type").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});

		});

            //DatePickers and Other Little Stuffs.
        $('#effective_date').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
	});
</script>