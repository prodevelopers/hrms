<!-- contents -->

<script>
	$(document).ready(function(){
		$("#recordformdiv").hide();

	});
	function view_form(){
		$("#recordformdiv").show();
	}

</script>

<div class="contents-container">


	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Pay Package</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">
    <div class="head">Pay Package</div>
    <?php if(empty($record)){ ?>
    <div id="add_recordDiv">
        <form action="<?php echo base_url(); ?>human_resource/edit_add_emp_pay_package/<?php echo $this->uri->segment(3); ?>" method="post" name="form1" onsubmit="return validation()">
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
            <input type="hidden" name="employment_id" value="<?php echo @$employment->employment_id; ?>" />
            <div class="row">
                <h4>Pay Grade</h4>
                <?php echo @form_dropdown('ml_pay_grade', $pay_grade ,'required="required"','required="required"'); ?>
            </div>

            <br class="clear">

            <div class="row">
                <h4>Pay Frequency</h4>
                <?php echo @form_dropdown('ml_pay_frequency', $pay_frequency,'required="required"','required="required"'); ?>
            </div>

            <br class="clear">

            <div class="row">
                <h4>Currency</h4>
                <?php
                echo @form_dropdown('ml_currency', $currency,'required="required"','id="drop_curr"'); ?>
                <a id="curr"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
            </div>

            <br class="clear">
            <script type="text/javascript">
                $(document).ready(function(){


                    $(".hay").change(function(){
                        $( ".hay option:selected").each(function(){
                            if($(this).attr("value")=="1"){
                                $(".Monthly").show();
                                $(".Hourly-Pay").hide();
                                $(".Daily-pay").hide();
                                $(".Yearly-pay").hide();
                                $(".Weekly-pay").hide();
                            }
                            if($(this).attr("value")=="2"){
                                $(".Monthly").hide();
                                $(".Hourly-Pay").show();
                                $(".Daily-pay").hide();
                                $(".Yearly-pay").hide();
                                $(".Weekly-pay").hide();

                            }
                            if($(this).attr("value")=="3"){
                                $(".Monthly").hide();
                                $(".Daily-pay").show();
                                $(".Weekly-pay").hide();
                                $(".Yearly-pay").hide();
                                $(".Hourly-Pay").hide();
                            }
                            if($(this).attr("value")=="4"){
                                $(".Monthly").hide();
                                $(".Daily-pay").hide();
                                $(".Weekly-pay").show();
                                $(".Yearly-Pay").hide();
                                $(".Hourly-Pay").hide();
                            }

                            if($(this).attr("value")=="5"){
                                $(".Monthly").hide();
                                $(".Daily-pay").hide();
                                $(".Weekly-pay").hide();
                                $(".Yearly-Pay").show();
                                $(".Hourly-Pay").hide();
                            }
                        });
                    }).change();
                });

            </script>

            <div class="row">
                <h4>Payrate</h4>
                <?php echo @form_dropdown('ml_pay_rate_id', $pay_rate,'', "class='hay'"); ?>
                <!--<select class="hay">
                    <option>Please Select</option>
                    <option value="1" id="Monthly">Monthly</option>
                    <option value="2" id="Weekly">Weekly</option>
                    <option value="3" id="Daily">Daily</option>
                </select>-->
            </div>

            <br class="clear">



            <div class="row Weekly-pay" style="display: none">
                <h4>Weekly Pay</h4>
                <input type="text" name="Weekly_pay">
            </div>

            <div class="row Daily-pay" style="display: none">
                <h4>Daily Pay</h4>
                <input type="text" name="Daily_pay">
            </div>
            <div class="row Monthly" style="display: none">
                <h4>Monthly Pay</h4>
                <input type="text" name="Monthly">
            </div>

            <div class="row Yearly-Pay" style="display: none">
                <h4>Yearly Pay</h4>
                <input type="text" name="Yearly-Pay">
            </div>
            <div class="row Hourly-Pay" style="display: none">
                <h4>Hourly Pay</h4>
                <input type="text" name="Hourly-Pay">
            </div>

            <br class="clear">

            <script type="text/javascript">

                $(document).ready(function(){
                    $(".increment-field").hide();
                    $('input[type="Checkbox"]').click(function(){
                        if($(this).attr("value")=="Increment"){
                            $(".increment-field").toggle();
                        }
                    });
                });
            </script>

            <!-- button group -->
            <div class="row">
                <div class="button-group">
                    <input type="submit" name="continue" value="Add" class="btn green" />
                    <input type="reset" value="Reset" class="btn gray" />
                </div>
            </div>

            <?php //echo form_close(); ?>
        </form>
    </div>
<?php }?>


            <table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					
                    <td>Base Salary</td>
                    <td>Increments</td>
					<td>Pay Frequency</td>
					<td>Currency</td>
                    <td>Payrate</td><!--
                    <td>Created By</td>
                    <td>Created Date</td>-->
                    <td>Approved Date</td>
                    <td>Approved By</td>
                    <td>Status</td>
					
				</thead>
                <?php if(!empty($record)){
					foreach($record as $row){ ?>
				<tr class="table-row">
                    <td><?php echo $row->base_salary;?></td>
                    <td><?php echo $row->Increments;?></td>
					<td><?php echo $row->pay_frequency;?></td>
					<td><?php echo $row->currency_name;?></td>
                    <td><?php echo $row->payrate;?></td>
                    <!--<td><?php /*echo $row->creater;*/?></td>
                    <td><?php
/*                        $date=$row->date_created;
                        if(!empty($date)){
                        $date_new=date("d-m-Y",strtotime($date));
                        echo $date_new;}*/?></td>-->
                    <td><?php
                        $date_apr=$row->date_approved;
                        if(!empty($date_apr) && $rec->status == 2){
                            $date_approved=date("d-m-Y",strtotime($date_apr));
                        echo $date_approved;}else{echo"Not Approved";}?></td>
                    <td><?php if($rec->status == 2){echo $row->apprved;}else{echo"Not Approved";}?></td>
					<td><?php if($row->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_emp_pay_package2/<?php echo $employee->employee_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
                   <!-- <td><?php /*if($rec > 0){ echo'<a href="human_resource/edit_emp_pay_package2/<?php echo @$employee->employee_id;?>"><span class="fa fa-pencil" id="pencil" onclick="view_form()"></span></a>'; }else{ echo $row->status_title;}*/?></td>-->
				</tr>
                <?php } } ?>
			</table>
		</div>


		<!--<div class="right-contents" style="margin-top:20px;">

		<div class="head">Allowances</div>


			<form>

				<div class="row">
					<h4>Allowance type</h4>
					<select>
						<option></option>
					</select>
				</div>

				<div class="row">
					<h4>Ammount</h4>
					<input type="text">
				</div>
				
			</form>

			
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Add</button></a>
					<button class="btn gray">Reset</button>
				</div>
			</div>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
			</table>


		</div>

		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Benefits</div>


			<form>

				<div class="row">
					<h4>Benefit type</h4>
					<select>
						<option></option>
					</select>
				</div>

				<div class="row">
					<h4>Benefit</h4>
					<select>
						<option></option>
					</select>
				</div>

				
			</form>

			button group 
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Add</button></a>
					<button class="btn gray">Reset</button>
				</div>
			</div>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
			</table>
-->

		</div>

	</div>
<!-- contents -->
