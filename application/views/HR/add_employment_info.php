<style>
    #pencil:hover{
        cursor: pointer;
    }
</style>
<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

    <div class="bredcrumb">Dashboard / Employees / Add Employee / Employment Info</div>
    <!-- bredcrumb -->

    <?php $this->load->view('includes/employee_left_nav.php'); ?>


    <div class="right-contents1">

        <div class="head">Employment Information</div>
        <div id="alert"
             style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;"><?php echo $this->session->flashdata('alert'); ?></div>
        <?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>"/>

        <div class="row">
            <h4>Current Job Designation</h4>
            <?php echo @form_dropdown('curr_job_title', $job_title, 'required="required"', 'id="drop_des" id="emp_typ" required="required"'); ?>
            <a id="desi"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>

        <!--<div class="row">
            <h4>Job Specification</h4>
            <input type="text" name="job_specification">
            a
        </div>-->

        <br class="clear">

        <div class="row">
            <h4>Employment Type</h4>
            <?php echo @form_dropdown('employment_type', $employment_type, 'required="required"', ' id="emp_typ" required="required"'); ?>
            <a id="emp_type"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
        </div>

        <div class="row">
            <h4>Job Category</h4>
            <?php echo @form_dropdown('job_category', $job_category, 'required="required"', 'id="job_categories" required="required"'); ?>
            <a id="job_cat"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
        </div>

        <div class="row">
            <h4>Pay Grade</h4>
            <?php echo @form_dropdown('pay_grade', $paygrade, 'required="required"', 'id="pay_grades" required="required"'); ?>
            <a id="pay_grade"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
        </div>

        <div class="row">
            <h4>Department</h4>
            <?php echo @form_dropdown('department', $department, 'required="required"', 'id="drop_dept" id="emp_typ" required="required"'); ?>
            <a id="dep"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>

        <div class="row">
            <h4>Branch / Office</h4>
            <?php echo @form_dropdown('branch', $branch, 'required="required"', 'id="branches" required="required"'); ?>
            <a id="branch"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
        </div>

        <div class="row">
            <h4>City</h4>
            <?php echo @form_dropdown('city', $city, 'required="required"', 'id="drop_city" required="required"'); ?>
            <a id="city"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>
        <div class="row">
            <h4>Joining Date</h4>
            <input type="text" name="joining_date" id="joining_date" required="required">
        </div>

        <div class="row">
            <h4>Location</h4>
            <?php echo @form_dropdown('location', $location, 'required="required"', 'id="drop_loc" required="required"'); ?>
            <a id="loc"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>

        <br class="clear">

       <!-- <div class="row">
            <h4>Project Name</h4>
            <?php /*echo @form_multiselect('project_id[]', $approval, $approval_types, 'required="required"', 'required="required"'); */?>
        </div>-->


        <div class="row">
            <h4>Work Shift</h4>
            <?php echo @form_dropdown('work_shift', $work_shift, 'required="required"', 'id="workShift" required="required"'); ?>
            <a id="ws"><span class="fa fa-pencil" id="pencil" style="font-size: 10px;"></span></a>
        </div>

        <br class="clear">

        <div class="head">Employment Contract</div>

        <br class="clear">

        <div class="row">
            <h4>Contract Start Date</h4>
            <input type="text" name="contract_start_date" id="contract_start_date" required="required">
        </div>
        <br class="clear">

        <div class="row">
            <h4>Contract Expiry Date</h4>
            <input type="text" name="contract_expiry_date" id="contract_expiry_date" required="required">
        </div>
        <br class="clear">

        <div class="row">
            <h4>Contract Alert Date</h4>
            <input type="text" name="date_contract_expiry_alert" id="contract_exp_date_id" required="required"
                   disabled="disabled">
        </div>

        <div class="row">
            <h4>Probation Period (Days)</h4>
            <input type="text" name="probation" required="required" id="probationDays" disabled="disabled">
        </div>
        <br class="clear">

        <div class="row">
            <h4>Probation Alert Date</h4>
            <input type="text" name="prob_alert_dat" id="probationAlertDate" required="required" disabled="disabled">
        </div>
        <br class="clear">


        <br class="clear">

        <div class="row">
            <h4>Comments</h4>
            <textarea name="comments"></textarea>
        </div>

        <!-- button group -->
        <br class="clear">

        <div class="row">
            <div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green"/>
                <input type="reset" value="Reset" class="btn gray"/>
            </div>
        </div>
        <?php echo form_close(); ?>

    </div>
</div>
<!-- contents -->
<!-- Dialog Sections -->
<!-- designaiton dialog -->
<div id="designation" title="Add Designation" style="display:none; width:600px;">
    <form id="desFrom" action="human_resource/add_designation5/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="designation_name" id="txt_des"/>
            <br><br>
            <input type="button" value="Add" id="DESIGNATION" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- designaiton dialog -->
<div id="department" title="Add Department" style="display:none; width:600px;">
    <form id="deptForm" action="human_resource/add_department2/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="department_name" id="txt_dept"/>
            <br><br>
            <input type="submit" value="Add" id="DEPARTMENT" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- city dialog -->
<div id="city-pop" title="Add City" style="display:none; width:600px;">
    <form id="cityForm" action="human_resource/add_city3/<?php echo @$employee->employee_id; ?>" method="post">
        <div class="data">
            <input type="text" class="text_field" name="city_name" id="txt_city"/>
            <br><br>
            <input type="submit" value="Add" id="CITYID" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Location dialog -->
<div id="location" title="Add Location" style="display:none; width:600px;">
    <form id="locForm" action="human_resource/add_posting_location/<?php echo @$employee->employee_id; ?>"
          method="post">
        <div class="data">
            <input type="text" class="text_field" name="posting_location" id="txt_loc"/>
            <br><br>
            <input type="submit" value="Add" id="LOCATIONId" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Employment Type dialog -->
<div id="employmentType" title="Add Employment Type" style="display:none; width:600px;">
    <form id="EmpTForm">
        <div class="data">
            <input type="text" class="text_field" name="employment_type" id="employment_type"/>
            <br><br>
            <input type="submit" value="Add" id="EMPTYPE" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Job Category dialog -->
<div id="JobCategory" title="Add Job Category" style="display:none; width:600px;">
    <form id="JobCat">
        <div class="data">
            <input type="text" class="text_field" name="Job_Category" id="Job_Categ"/>
            <br><br>
            <input type="submit" value="Add" id="JobAdd" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Pay Grade dialog -->
<div id="PayGrade" title="Add Pay Grade" style="display:none; width:600px;">
    <form id="payGrd">
        <div class="data">
            <input type="text" class="text_field" name="Pay_grads" id="pay_g"/>
            <br><br>
            <input type="submit" value="Add" id="PAYGRADE" class="btn green addedto" name="add">
        </div>
    </form>
</div>

<!-- Branch -->
<div id="Branch_d" title="Add Branch" style="display:none; width:600px;">
    <form id="Brnh">
        <div class="data">
            <input type="text" class="text_field" name="add_brach" id="branch_b"/>
            <br><br>
            <input type="submit" value="Add" id="BRANCH" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<!-- Work Shift -->
<div id="work_d" title="Add Work Shift" style="display:none; width:600px;">
    <form id="wsf">
        <div class="data">
            <input type="text" class="text_field" name="add_ws" id="ws_b"/>
            <br><br>
            <input type="submit" value="Add" id="WORKID" class="btn green addedto" name="add">
        </div>
    </form>
</div>
<script src="<?php echo base_url() ?>assets/js/edit-dialogs.js"></script>
<script>

$(document).ready(function () {

    //Work shift
    $("#WORKID").on('click', function (e) {
        e.preventDefault();
        var formData = {
            ws: $("#ws_b").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_workShift/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var WsEnterdValue = $('#ws_b').val();
                    var appendData = '<option value="' + data[3] + '">' + WsEnterdValue + '</option>';
                    $('#workShift').append(appendData);
                    $('#work_d').dialog('close');
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });

    //Pay Grade
    $("#BRANCH").on('click', function (e) {
        e.preventDefault();
        var formData = {
            add_branch: $("#branch_b").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_branch/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#branch_b').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#branches').append(appendData);
                    $("#Branch_d").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });

    //Pay Grade
    $("#PAYGRADE").on('click', function (e) {
        e.preventDefault();
        var formData = {
            pay_grade: $("#pay_g").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_pay_grade/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#pay_g').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#pay_grades').append(appendData);
                    $("#PayGrade").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });

    //Job Category
    $("#JobAdd").on('click', function (e) {
        e.preventDefault();
        var formData = {
            Job_category: $("#Job_Categ").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_job_category/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var JobCatEnterdValue = $('#Job_Categ').val();
                    var appendData = '<option value="' + data[3] + '">' + JobCatEnterdValue + '</option>';
                    $('#job_categories').append(appendData);
                    $("#JobCategory").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });



    //Employment Type
    $("#EMPTYPE").on('click', function (e) {
        e.preventDefault();
        var formData = {
            employmentType: $("#employment_type").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_employment_type/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var EmpTypeEnterdValue = $('#employment_type').val();
                    var appendData = '<option value="' + data[3] + '">' + EmpTypeEnterdValue + '</option>';
                    $('#emp_typ').append(appendData);
                    $("#employmentType").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });


    //Designation
    $("#DESIGNATION").on('click', function (e) {
        e.preventDefault();
        var formData = {
            desi: $("#txt_des").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_designation5/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var provinceNameEnterdValue = $('#txt_des').val();
                    var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                    $('#drop_des').append(appendData);
                    $("#designation").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });


    //Department
    $("#DEPARTMENT").on('click', function (e) {
        e.preventDefault();
        var formData = {
            dept_name: $("#txt_dept").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_department2/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var provinceNameEnterdValue = $('#txt_dept').val();
                    var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                    $('#drop_dept').append(appendData);
                    $("#department").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });


    });
    //City Pop
    $("#CITYID").on('click', function (e) {

        e.preventDefault();
        var formData = {
            city_name: $("#txt_city").val()
        };
        //alert(desi);
        $.ajax({

            url: "human_resource/add_city3/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var provinceNameEnterdValue = $('#txt_city').val();
                    var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                    $('#drop_city').append(appendData);
                    $("#city-pop").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });


    });

    //Location
    $("#LOCATIONId").on('click', function (e) {
        e.preventDefault();
        var formData = $('#locForm').serialize();
        $.ajax({
            url: "human_resource/add_posting_location/<?php echo @$employee->employee_id;?>",
            data: formData,
            type: "POST",
            success: function (output) {
                //Output Here If Success.
                var data = output.split('::');
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                    var provinceNameEnterdValue = $('#txt_loc').val();
                    var appendData = '<option value="' + data[3] + '">' + provinceNameEnterdValue + '</option>';
                    $('#drop_loc').append(appendData);
                    $("#location").dialog("close");
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
        });
    });

    //My Code Starts Here -- SHH

    //Lets Get All The Required Selectors
    var contractStartDateSelector = $('#contract_start_date');
    var contractExpiryDateSelector = $('#contract_expiry_date');
    var contractExpiryDateIDSelector = $('#contract_exp_date_id');
    var probationAlertDateSelector = $('#probationAlertDate');
    var probationDaysSelector = $('#probationDays');

    //DatePickers
    contractStartDateSelector.datepicker({
        dateFormat: "dd-mm-yy",
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        onClose: function (selectedDate) {
            $("#contract_expiry_date").datepicker("option", "minDate", selectedDate);
        }
    }).change(function () {
        var contractStartDate = contractStartDateSelector.val();
        var contractExpiryDate = contractExpiryDateSelector.val();
        if (contractStartDate.length > 0 && contractExpiryDate.length > 0) {
            //If Dates Are Picked Then We Need To Remove the disabled Property From the Text Boxes.
            probationAlertDateSelector.removeAttr('disabled');
            probationDaysSelector.removeAttr('disabled');
            contractExpiryDateIDSelector.removeAttr('disabled');
        }
    });

    contractExpiryDateSelector.datepicker({
        dateFormat: "dd-mm-yy",
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        onClose: function (selectedDate) {
            $("#contract_start_date").datepicker("option", "maxDate", selectedDate);

        }
    }).change(function () {
        var contractStartDate = contractStartDateSelector.val();
        var contractExpiryDate = contractExpiryDateSelector.val();
        if (contractStartDate.length > 0 && contractExpiryDate.length > 0) {
            //If Dates Are Picked Then We Need To Remove the disabled Property From the Text Boxes.
            probationAlertDateSelector.removeAttr('disabled');
            probationDaysSelector.removeAttr('disabled');
            contractExpiryDateIDSelector.removeAttr('disabled');
        }
    });

    //Changes For Probation...

    probationDaysSelector.on('change', function (e) {
        //probation Days.
        if (isNaN($(this).val())) {
            Parexons.notification('Only Numbers Are Allowed', 'error');
            $(this).val(0);
            return;
        }
        var totalProbationDays = $(this).val();
        var probationAlertDate = probationAlertDateSelector.val();
        var maxProbationDays = 120;
        var contractStartDate = $('#date_of_birth').val(); //This ID is Given by some genius Here.. To Save Time i am not changing the ID Name..
        if (probationAlertDate.length === 0 && totalProbationDays > maxProbationDays) {
            Parexons.notification('Probation Can Not Be More Than 120 Days', 'warning');
            $(this).val(120);
            return;
        } else if (probationAlertDate.length > 0) {
            var diff = new Date(Date.parse(probationAlertDate) - Date.parse(contractStartDate));
            var totalDays = diff / 1000 / 60 / 60 / 24;
            if (totalProbationDays > totalDays) {
                Parexons.notification('You Have Extended The Probation Date More Than the Probation Alert Date', 'warning');
                $('#probationAlertDate').val('');
            }
            if (totalProbationDays > maxProbationDays) {
                Parexons.notification('Probation Can Not Be More Than 120 Days', 'warning');
                return;
            }
        }

    });
    probationAlertDateSelector.on('change', function (e) {
        //Probation Alert Date Must Be Picked From Here.
        var totalProbationDays = $('#probationDays').val();

        if (totalProbationDays.length > 0) {
//                get the date of after adding dates
            var newDate = new Date(new Date().getTime() + (totalProbationDays * 24 * 60 * 60 * 1000));
        }
    });

    probationAlertDateSelector.datepicker({
        beforeShow: customRange,
        dateFormat: "dd-mm-yy",
        firstDay: 1,
        changeMonth: true,
        changeYear: true
    });

    //Page Messages Script Should Go Under
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0],data[1]);
    <?php
    }
    }
}
?>
});

//Custom Date Range.
function customRange(input) {
    var contractStartDate = $('#contract_start_date').val();
    var maxProbationDays = 120;
    var totalProbationDays = $('#probationDays').val();
    //Setting Up Min And Max Dates.
    var min = new Date(contractStartDate),
        dateMin = min,
        dateMax = new Date(min.getTime() + (maxProbationDays * 24 * 60 * 60 * 1000));
    if (totalProbationDays.length > 0 && !isNaN(totalProbationDays)) {
        dateMax = new Date(min.getTime() + (totalProbationDays * 24 * 60 * 60 * 1000));
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

  //***************** Datepicker ***************//

$( "#joining_date" ).datepicker({
    dateFormat: "dd-mm-yy",
    timeFormat: "HH:MM",
    changeMonth: true,
    changeYear: true,
    maxDate: '0',
    yearRange:"<?php echo yearRang(); ?>"


});
$( "#contract_exp_date_id" ).datepicker({
    dateFormat: "dd-mm-yy",
    timeFormat: "HH:MM",
    changeMonth: true,
    changeYear: true,
    minDate: '0',
    yearRange:"<?php echo yearRang(); ?>"

});

</script>