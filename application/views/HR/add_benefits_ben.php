<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Benefits</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
				<div class="row">
					<h4>Benefit type</h4>
					<?php echo @form_dropdown('benefit_type_id', $benefit_type,'required="required"','required ="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

<!--				<div class="row">-->
<!--					<h4>Benefit</h4>-->
<!--					--><?php //echo @form_dropdown('benefits_list_id', $benefit_list,'required="required"','required ="required"'); ?>
<!--					<!--<input type="text" name="">-->
<!--				</div>-->
<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="date_effective">
				</div>
                
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_benefit" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($benefit_details)){
					foreach($benefit_details as $benefit){?>
				<tr class="table-row">
					<td><?php echo $benefit->benefit_type_title;?></td>
					<td><?php echo date_format_helper($benefit->date_effective);?></td>
					<td><a href="human_resource/edit_single_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
	</div>
<!-- contents -->

<script>
    $("#alert").delay(3000).fadeOut('slow');
    $( "#date_effective" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
        /*onClose: function( selectedDate ) {
         $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
         }*/

    });
</script>

<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->