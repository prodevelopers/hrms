<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Employment Info</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav.php'); ?>
	

	<div class="right-contents1">

		<div class="head">Employment Info</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
					<h4>Current Job Title</h4>
					<?php 
					$slctd_job_title = (isset($experience->job_title) ? $experience->job_title : '');
					echo @form_dropdown('curr_job_title', $job_title, $slctd_job_title,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Job Specification</h4>
					<input type="text" name="job_specification" value="<?php echo @$position_mgt->job_specifications; ?>">
				</div>

				<br class="clear">
				<div class="row">
					<h4>Employment Type</h4>
					<?php  
					$slctd_emp_type = (isset($contract->employment_type) ? $contract->employment_type : '');
					echo @form_dropdown('employment_type', $employment_type, $slctd_emp_type); ?>
				</div>

				<div class="row">
					<h4>Job Category</h4>
					<?php  
					$slctd_job_cat = (isset($contract->employment_category) ? $contract->employment_category : '');
					echo @form_dropdown('job_category', $job_category, $slctd_job_cat); ?>
				</div>

				<div class="row">
					<h4>Department</h4>
					<?php  
					$slctd_dept = (isset($posting->ml_department_id) ? $posting->ml_department_id : '');
					echo @form_dropdown('department', $department, $slctd_dept); ?>
				</div>

				<div class="row">
					<h4>Branch</h4>
					<?php   
					$slctd_branch = (isset($posting->ml_branch_id) ? $posting->ml_branch_id : '');
					echo @form_dropdown('branch', $branch, $slctd_branch); ?>
				</div>

				<div class="row">
					<h4>City</h4>
					<?php   
					$slctd_city = (isset($posting->ml_city_id) ? $posting->ml_city_id : '');
					echo @form_dropdown('city', $city, $slctd_city); ?>
				</div>


				<div class="row">
					<h4>Joining Date</h4>
					<input type="text" name="joining_date" id="joining_date" value="<?php echo @$employment->joining_date; ?>">
				</div>

				<br class="clear">
				<div class="row">
					<h4>Location</h4>
					<?php  
					$slctd_loc = (isset($posting->location) ? $posting->location : '');
					echo @form_dropdown('location', $location, $slctd_loc); ?>
				</div>
				

				<div class="row">
					<h4>Work Shift</h4>
					<?php   
					$slctd_work_shift = (isset($posting->shift) ? $posting->shift : '');
					echo @form_dropdown('work_shift', $work_shift, $slctd_work_shift); ?>
				</div>

				<br class="clear">
				<div class="row">
					<h4><b>Employment Contract</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Expiry Date</h4>
					<input type="text" name="contract_expiry_date" id="visa_issue_date" value="<?php echo @$contract->contract_expiry_date; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert</h4>
					<input type="text" name="contract_exp_alert" value="<?php echo @$contract->contract_exp_alert; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert Date</h4>
					<input type="text" name="date_contract_expiry_alert" id="contract_exp_date_id" value="<?php echo @$contract->date_contract_expiry_alert; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comments"><?php echo @$contract->comments; ?></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>		

		</div>


		
	</div>
<!-- contents -->