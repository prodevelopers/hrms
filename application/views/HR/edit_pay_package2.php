<!-- contents -->

<script>
	function view_form(){
		$("#recordformdiv").show();
	}

</script>

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Pay Package</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Pay Package</div>
<div id="recordformdiv">
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        <input type="hidden" name="employment_id" value="<?php echo @$employment->employment_id; ?>" />
		<div class="row">
			<h4>Pay Grade</h4>
			<?php
			$slctd_pay_frequency = (isset($salary->ml_pay_grade) ? $salary->ml_pay_grade : '');
			echo @form_dropdown('ml_pay_grade', $pay_grade,$slctd_pay_frequency,'required="required"','required="required"' ); ?>
		</div>

				<br class="clear">

				<div class="row">
					<h4>Pay Frequency</h4>
                    <?php 
					$slctd_pay_frequency = (isset($salary->ml_pay_frequency) ? $salary->ml_pay_frequency : '');
					echo @form_dropdown('ml_pay_frequency', $pay_frequency, $slctd_pay_frequency); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

				<br class="clear">

				<div class="row">
					<h4>Currency</h4>
                    <?php
					$slctd_currency = (isset($salary->ml_currency) ? $salary->ml_currency : ''); 
					echo @form_dropdown('ml_currency', $currency, $slctd_currency); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

				<br class="clear">
				<script type="text/javascript">
					$(document).ready(function(){
				 	$(".weekly-pay").hide();
				 	$(".salary").hide();
				 	$(".daily-pay").hide();
      			 $(".hay").change(function(){
            $( ".hay option:selected").each(function(){
                if($(this).attr("value")=="1"){
                    $(".salary").show();
                    $(".daily-pay").hide();
                    $(".weekly-pay").hide();
                }
                if($(this).attr("value")=="2"){
                    $(".salary").hide();
                    $(".daily-pay").hide();
                    $(".weekly-pay").show();
                }
                if($(this).attr("value")=="3"){
                    $(".salary").hide();
                    $(".daily-pay").show();
                    $(".weekly-pay").hide();
                }
            });
        }).change();
    });

				</script>

				<div class="row">
					<h4>Payrate</h4>
                    <?php 
					$slctd_pay_rate = (isset($salary->ml_pay_rate_id) ? $salary->ml_pay_rate_id : '');
					echo @form_dropdown('ml_pay_rate_id', $pay_rate, $slctd_pay_rate, "class='hay'"); ?>
					<!--<select class="hay">
						<option>Please Select</option>
						<option value="1" id="Monthly">Monthly</option>
						<option value="2" id="Weekly">Weekly</option>
						<option value="3" id="Daily">Daily</option>
					</select>-->
				</div>

				<br class="clear">

				<div class="row salary">
					<h4>Base Salary</h4>
					<input type="text" name="base_salary" value="<?php echo @$salary->base_salary?>">
				</div>

				<div class="row weekly-pay">
					<h4>Weekly Pay</h4>
					<input type="text" name="weekly_pay" value="<?php echo @$salary->base_salary?>">
				</div>

				<div class="row daily-pay">
					<h4>Daily Pay</h4>
					<input type="text" name="daily_pay" value="<?php echo @$salary->base_salary?>">
				</div>

				<br class="clear">

				<script type="text/javascript">
					
					$(document).ready(function(){
					    $(".increment-field").hide();
					    $('input[type="Checkbox"]').click(function(){
					        if($(this).attr("value")=="Increment"){
					            $(".increment-field").toggle();
					        }
					    });
					});
				</script>

				<!--<div class="row">
					<h4>Increments</h4>
					<input type="Checkbox" value="Increment">					
				</div>

				<br class="clear">

				<div class="row increment-field">
					<h4>Increment Type</h4>
					<select>
						<option></option>
					</select>
				</div>

				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text">
				</div>-->
                
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
	</div>

            <table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					
                    <td>Base Salary</td>
					<td>Pay Frequency</td>
					<td>Currency</td>
                    <td>Payrate</td>
                    <td>Created By</td>
                    <td>Created Date</td>
                    <td>Approved Date</td>
                    <td>Approved By</td>
                    <td>Status</td>
					
				</thead>
                <?php if(!empty($record)){
					foreach($record as $row){ ?>
				<tr class="table-row">
                    <td><?php echo $row->base_salary;?></td>
					<td><?php echo $row->pay_frequency;?></td>
					<td><?php echo $row->currency_name;?></td>
                    <td><?php echo $row->payrate;?></td>
                    <td><?php echo $row->creater;?></td>
                    <td><?php
                        $date=$row->date_created;
                        if(!empty($date)){
                            $date_new=date("d-m-Y",strtotime($date));
                            echo $date_new;}?></td>
                    <td><?php
                        $date_apr=$row->date_approved;
                        if(!empty($date_apr) && empty($rec->status)){
                            $date_approved=date("d-m-Y",strtotime($date_apr));
                            echo $date_approved;} else{echo"Not Approved";}?></td>
                    <td><?php if(empty($rec->status)){echo $row->apprved;}else{echo"Not Approved";}?></td>
                    <td><?php $state = @$rec->status; if($state == 1){?><a href="human_resource/edit_emp_pay_package2/<?php echo @$employee->employee_id;?>"><span class="fa fa-pencil" id="pencil" onclick="view_form()"></span></a><?php }else{ echo @$row->status_title;}?></td>
				</tr>
                <?php } } ?>
			</table>
		</div>


		<!--<div class="right-contents" style="margin-top:20px;">

		<div class="head">Allowances</div>


			<form>

				<div class="row">
					<h4>Allowance type</h4>
					<select>
						<option></option>
					</select>
				</div>

				<div class="row">
					<h4>Ammount</h4>
					<input type="text">
				</div>
				
			</form>

			
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Add</button></a>
					<button class="btn gray">Reset</button>
				</div>
			</div>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
			</table>


		</div>

		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Benefits</div>


			<form>

				<div class="row">
					<h4>Benefit type</h4>
					<select>
						<option></option>
					</select>
				</div>

				<div class="row">
					<h4>Benefit</h4>
					<select>
						<option></option>
					</select>
				</div>

				
			</form>

			button group 
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Add</button></a>
					<button class="btn gray">Reset</button>
				</div>
			</div>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td>Benefit Type</td>
					<td>Benefit</td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
			</table>
-->

		</div>

	</div>
<!-- contents -->
