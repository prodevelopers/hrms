<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Attachments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Attachments</div>
		<?php echo form_open_multipart();?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        	<div class="row">
					<h4>Attachment</h4>
					<input type="file" name="attached_file" required="required">
				</div>

				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="remarks">
				</div>

				<br class="clear">

				
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>

		</div>


		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Uploaded Files</div>

			<table cellspacing="0">
				<tr class="table-head" style="background:#A3AAA3;">
					<td>File Name</td>
					<td>Remarks</td>
					<td>Trash</td>
				</tr>
                <?php if(!empty($attach_details)){
					foreach($attach_details as $file){?>
				<tr class="table-row">
					<td><?php echo $file->attached_file; ?></td>
					<td><?php echo $file->remarks; ?></td>
					<td><a href="human_resource/send_2_trash_attachment_add/<?php echo $employee->employee_id; ?>/<?php echo $file->attachment_id; ?>" onclick="return confirm('Are you Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } }else{echo"<div class=\"row\">No Record Exists.</div>";} ?>
			</table>

			<!-- button group 
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Submit</button></a>
					<button class="btn gray">Reset</button> 
				</div>-->
			<!-- </div>-->

			

		</div>


		

	</div>
<!-- contents -->
<script>
//////////// Notification Message ////////////////
$(document).ready(function() {
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0], data[1]);
    <?php
    }
}
}
?>
});
</script>