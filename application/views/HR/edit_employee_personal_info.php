<script type="text/javascript">
    $(document).ready(function(){
        <?php $msg=$this->session->flashdata('update');
        if(!empty($msg)){?>
        $("#msg").show().delay(4000).fadeOut();
        <?php }?>

    });
 function check_cnic()
 {
	 $(document).ready(function () 
	 {
		 var fisrt_part = $('#nic1').val();
		 var second_part = $('#nic2').val();
		 var third_part = $('#nic3').val();
		 var cnic = fisrt_part +'-'+second_part +'-'+third_part;
		 $.ajax({
			 type: "POST",
			 url: "human_resource/check_cnic_exist",
			 data: {cnic:cnic} ,
			 success: function(data) 
			 {
				 $('#check_cnic').html(data);
			 }
		 });
	 });  
 }
/// To Forward Focus to Next Text Field
$(document).ready(function()
{

    $('input').keyup(function()
	{
        if(this.value.length == $(this).attr("maxlength"))
		{
            $(this).next().focus();
        }
    });
});
 function validateForm() {
	 var x = document.forms["myForm"]["email"].value;
	 var atpos = x.indexOf("@");
	 var dotpos = x.lastIndexOf(".");
	 if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
		 alert("Not a valid Email address");
		 return false;
	 }
 }
</script>
<?php 
	$photo 	= $employee->thumbnail; 
	$exp 	= explode('-', $employee->CNIC);
	
	$action = $this->uri->segment(4);
	if($action == "action"){ ?>
    <script>
		function load()
		{
			alert('Record Updated Successfully .... !');
		}
	</script>
    <?php } ?>

<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Personal Info</div> <!-- bread Crumbs -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>

	<div class="right-contents1">

		<div class="head">Personal Info	</div>
        <div id="msg" style="display: none; background-color: #008000; color: #ffffff; text-align: center;"><?php echo $this->session->flashdata('update');?></div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php echo  $this->session->flashdata('msg');?></div>
			<?php /*?><?php echo form_open_multipart(); ?><?php */?>
            <form action="" method="post" enctype="multipart/form-data" name="form1" onsubmit="return validation()">
            <input type="hidden" name="emp_id" value="<?php echo $employee->employee_id;?>" />
            <input type="hidden" name="uploaded_photo" value="<?php echo $employee->photograph;?>" />
			<div style="width:49%; float:left;">
				<div class="row2">
					<h4>Employee ID</h4>
					<input type="text" name="employee_code" value="<?php echo $employee->employee_code; ?>" readonly>
				</div>
				 <div class="row2">
                	<h4>First Name</h4>
					<input type="text" name="first_name" value="<?php echo @$employee->first_name; ?>" required="required">
				</div> 	        
                <div class="row2">
                	<h4>Last Name</h4>
					<input type="text" name="last_name" value="<?php echo @$employee->last_name; ?>">
				</div>
				<div class="row2">
					<h4>Father Name</h4>
					<input type="text" name="father_name" value="<?php echo @$employee->father_name; ?>">
				</div>
				<div class="row2">
					<h4>CNIC</h4>
					<input type="text" id="input" name="nic1" style="width: 40px; float: left;" maxlength="5" placeholder="" value="<?php echo $exp[0]; ?>" >
					<input type="text" id="input1" name="nic2" style="width: 110px; float: left;" maxlength="7" placeholder="" value="<?php echo $exp[1]; ?>" >
					<input type="text" id="input2" name="nic3" style="width: 20px; float: left;" maxlength="1" placeholder=""  value="<?php echo $exp[2]; ?>" onkeyup="return check_cnic()">
					<div id="check_cnic"></div>
				</div>
				<div class="row2">
					<h4>Gender</h4>
                    <?php
					$selctd_gender = (isset($employee->gender) ? $employee->gender : '');
					echo @form_dropdown('gender', $gender, $selctd_gender,' id="drop_gen"', 'required="required"'); ?>
                    <a id="gender"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row2">
					<h4>Marital Status</h4>
                    <?php 
					$selctd_marital = (isset($employee->marital_status) ? $employee->marital_status : '');
					echo @form_dropdown('marital_status', $marital_status, $selctd_marital); ?>
					<!--<select>
						<option>Married</option>
						<option>Single</option>
						<option>Engaged</option>
					</select>-->
				</div>
  				<div class="row2">
					<h4>Employee Tax No</h4>
					<input type="text" name="tax_number" value="<?php echo @$employee->tax_number; ?>">
				</div>

				<div class="row2">
					<h4>Disability / Illness</h4>
					<input type="text" name="disabled_people" value="<?php echo @$employee->disabled_people; ?>">
				</div>
			</div>
			<div style="width:49%; float:left;">
				<div class="row2">
					<h4>Upload Picture</h4>
					<input type="file" name="employee_photo" value="<?php echo @$employee->thumbnail; ?>">
				</div> 
				<div class="row2">
					<h4>Date of Birth</h4>
					<input type="text" name="date_of_birth" id="date_of_birth" value="<?php echo date_format_helper($employee->date_of_birth); ?>" required="required">
				</div>
				<div class="row2">
					<h4>Nationality</h4>
                    <?php 
					$selctd_nationality = (isset($employee->nationality) ? $employee->nationality : '');
					echo @form_dropdown('nationality', $nationality, $selctd_nationality,'id="drop_nation"'); ?>
                    <a id="nation"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row2">
					<h4>Religion</h4>
                    <?php
					$selctd_relegion = (isset($employee->religion) ? $employee->religion : '');
					echo @form_dropdown('religion', $religion, $selctd_relegion,'id="drop_rel"'); ?>
                    <a id="reli"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row2">
					<h4>Blood Group</h4>
					<select name="blood_group">
						<option  value="<?php if(@$employee->blood_group = NULL){echo @$employee->blood_group;}?>" selected> -- Select Blood Group -- </option>
						<option value="<?php if(@$employee->blood_group = A-Positive){echo @$employee->blood_group;}?>" selected>A-Positive</option>
						<option value="<?php if(@$employee->blood_group = B-Positive){echo @$employee->blood_group;}?>" selected>B-Positive</option>
						<option value="<?php if(@$employee->blood_group = A-Negative){echo @$employee->blood_group;}?>" selected>A-Negative</option>
						<option value="<?php if(@$employee->blood_group = B-Negative){echo @$employee->blood_group;}?>" selected>B-Negative</option>
						<option value="<?php if(@$employee->blood_group = AB-Positive){echo @$employee->blood_group;}?>" selected>AB-Positive</option>
						<option value="<?php if(@$employee->blood_group = O-Positive){echo @$employee->blood_group;}?>" selected>O-Positive</option>
						<option value="<?php if(@$employee->blood_group = O-Negative){echo @$employee->blood_group;}?>" selected>O-Negative</option>
					</select>
				</div>
			</div>
				<br class="clear">

				<!-- hide show -->
				<script type="text/javascript">
					
					$(document).ready(function(){
//						$(".immigration-fields").hide();
					    $('#immigrationCheckBox').on('change',function(){
					        if($(this).is(':checked')){
					            $(".immigration-fields").show();
					        }else{
								$(".immigration-fields").hide();
							}
					    });
						<?php $passport=@$immigration->passport_num; if(!empty($passport)){?>

						$(".immigration-fields").show();
						<?php }else{?>

						$(".immigration-fields").hide();<?php }?>
					});
				</script>
				<div class="row">
					<h4><b>Immigration</b></h4>
					<input type="checkbox" value="Immigration" id="immigrationCheckBox">
				</div>
				<br class="clear">
				<div class="row immigration-fields">
					<h4>Passport</h4>
					<input type="text" name="passport_num" value="<?php echo @$immigration->passport_num; ?>">
				</div>
				<div class="row immigration-fields">
					<h4>Visa</h4>
					<input type="text" name="visa_num" value="<?php echo @$immigration->visa_num; ?>">
				</div>
				<br class="clear"> 
				<div class="row immigration-fields">
					<h4>Visa Expiry Date</h4>
					<input type="text" name="visa_expiry" id="visa_issue_date" value="<?php echo date_format_helper(@$immigration->visa_expiry); ?>">
				</div>
				<div class="row immigration-fields">
                </div>
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            </form>
		</div>
	</div>

	<!-- Dialog Sections -->

	<!-- Gender dialog -->
	<div id="dialog" title="Add Gender" style="display:none; width:600px;">
	<form id="genForm" action="human_resource/add_gender/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="gender_type_title" id="txt_gen"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>

	<!-- Nationality Dialog -->
	<div id="nationality" title="Add Nationality" style="display:none; width:600px;">
	<form id="nationForm" action="human_resource/add_nation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="nationality" id="txt_nation"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add_nation">
		</div>
	</form>
	</div>
	<!-- Religion Dialog -->
	<div id="religion" title="Add Religion" style="display:none; width:600px;">
	<form id="relForm" action="human_resource/add_religion/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="religion_title" id="txt_rel"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<!-- contents -->
<style>
    a .btn{
        font-size: 18px;
    }
</style>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<script>
	$(document).ready(function()
	{
		$("#religion").on('click',function(e){
			e.preventDefault();
			var formData = $('#relForm').serialize();
			$.ajax({
				url: "human_resource/add_religion/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_rel').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_rel').append(appendData);
						$("#religion").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});

		});


        ///For CheckBox..
        var passportField = $('input[name="passport_num"]').val();
        if(passportField.length > 0){
            $('#immigrationCheckBox').prop('checked',true);
        }else{
            $('#immigrationCheckBox').prop('checked',false);
        }

        $('input[name="continue"]').on('click',function(e){
            if($('#immigrationCheckBox').is(':checked')){
                e.preventDefault();
                Parexons.notification('You Have Checked Immigration Checkbox, MayBe you Forgot to Enter Immigration Data','warning');
                return false;
            }else{
                return true;
            }
        });

	});

	function validation()
	{
		if(document.form1.gender.value=="0")
		{alert("Please select Gender !");
		return false;
		}
		if(document.form1.marital_status.value=="0")
		{alert("Please select Marital Status !");
		return false;
		}
		if(document.form1.nationality.value=="0")
		{alert("Please select Nationality !");
		return false;
		}
		if(document.form1.religion.value=="0")
		{alert("Please select Religion !");
		return false;
		}
	}
	$(document).ready(function()
	{
		$("#nationality").on('click',function(e){
			e.preventDefault();
			var formData = $('#nationForm').serialize();
			$.ajax({
				url: "human_resource/add_nation/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_nation').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_nation').append(appendData);
						$("#nationality").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

	$(document).ready(function()
	{
		$("#dialog").on('click','.addedto',function(e){
			e.preventDefault();
			var formData = $('#genForm').serialize();
			$.ajax({
				url: "human_resource/add_gender/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output){
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_gen').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_gen').append(appendData);
						$("#dialog").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

	$( "#date_of_birth" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM", maxDate: '0',
		changeMonth: true,
		changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"

	});

	$( "#visa_issue_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
		changeMonth: true,
		changeYear: true

	});

</script>
