<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Experience</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	

	<div class="right-contents1">
		<div class="head">Add Experience</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
						<h4>Designation</h4>
                    <?php echo @form_dropdown('job_title', $job_title,'',' required="required" id="drop_title"'); ?>
					<a id="desi"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Organization</h4>
					<input type="text" name="organization" required="required">
				</div>

				<br class="clear">
				<div class="row">
					<h4>From</h4>
					<input type="text" name="from_date" id="from_date" required="required">
				</div>

				
				<div class="row">
					<h4>To</h4>
					<input type="text" name="to_date" id="to_date" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employment Type</h4>
                    <?php echo @form_dropdown('ml_employment_type_id', $employment,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comment"></textarea>
				</div>
                
			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
			

		</div>


		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Experience</div>

			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Company</td>
					<td>Job Title</td>
					<td>From</td>
					<td>To</td>
					<td>Total</td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($experience_detail)){
					foreach($experience_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->organization;?></td>
					<td><?php echo $row->designation_name;?></td>
					<td><?php echo date_format_helper($row->from_date);?></td>
					<td><?php echo date_format_helper($row->to_date);?></td>
					<td>
                        <?php
                        $from_date=$row->from_date;
                        $to_date=$row->to_date;
                        $exp_start_date= new DateTime($from_date);
                        $exp_to_date= new DateTime($to_date);


                        if(!empty($from_date)) {

                            $interval = $exp_start_date->diff($exp_to_date);
                           echo  $total_current_exp=$interval->y ." Year(s), ".$interval->m." Month(s)";
                        }?>
                    </td>
					<td><a href="human_resource/send_2_trash_experience_add/<?php echo $employee->employee_id;?>/<?php echo $row->experience_id;?>/<?php echo 'experience_id';?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php } } ?>
			</table>
			
		

		</div>

	</div>
<!-- contents -->
<!-- Dialog Sections -->
	<!-- Designation dialog -->
	<div id="designation" title="Add Designation" style="display:none; width:600px;">
	<form id="titleForm" action="human_resource/add_designation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="designation_name" id="txt_title"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<script>
	$(document).ready(function()
	{
		$("#designation").on('click',function(e){
			e.preventDefault();
			var formData = $('#titleForm').serialize();
			$.ajax({
				url: "human_resource/add_designation/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_title').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_title').append(appendData);
						$("#designation").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

        //////////// Notification Message ////////////////

        <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0], data[1]);
        <?php
        }
        }
    }
    ?>


	});

    //************ Datepicker *************//

    $( "#from_date" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"

    });

    $( "#to_date" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"

    });
</script>

