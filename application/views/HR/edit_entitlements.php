<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Increments</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_increment_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Increment" class="btn green" /></a>
				</div>
			</div>
            <table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
                <tr>
					<td>Increment Type</td>
					<td>Increment</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td>Edit</td>
					<td>Trash</td>
                </tr>
				</thead>
                <?php if(!empty($increment_details)){
					foreach($increment_details as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->increment_type;?></td>
					<td><?php echo $increment->increment_amount;?></td>
					<td><?php echo $increment->emp_name;?></td>
					<td>
                        <?php
                        $date=$increment->approval_date;
                        if(!empty($date)){
                        $date_new=date("d-m-Y",strtotime($date));
                        echo $date_new;}?></td>
					<td><?php echo $increment->status_title;?></td>
					<td><?php if($increment->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_emp_single_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<!--<td><a href="human_resource/edit_emp_single_increment/<?php /*echo $employee->employee_id;*/?>/<?php /*echo $increment->increment_id;*/?>"><span class="fa fa-pencil"></span></a></td>-->
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>


		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Allowances</div>
        
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_allowance_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Allowance" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
                <tr>
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td>Edit</td>
					<td>Trash</td>
                </tr>
				</thead>
                <?php if(!empty($allowance_details)){
					foreach($allowance_details as $allowance){?>
				<tr class="table-row">
					<td><?php echo $allowance->allowance_type;?></td>
					<td><?php echo $allowance->allowance_amount;?></td>
					<td><?php echo $allowance->emp_name;?></td>
					<td><?php
                        $date=$allowance->date_approved;
                        if(!empty($date)){
                            $date_new=date("d-m-Y",strtotime($date));
                        echo $date_new;}?></td>
					<td><?php echo $allowance->status_title;?></td>
					<td><?php if($allowance->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_emp_single_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<!--<td><a href="human_resource/edit_emp_single_allowance/<?php /*echo $employee->employee_id;*/?>/<?php /*echo $allowance->allowance_id;*/?>"><span class="fa fa-pencil"></span></a></td>-->
					<td><a href="human_resource/send_2_trash_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>

		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Benefits</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_benefit_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Benefit" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
					<!--<td>Benefit</td>-->
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($benefit_details)){
					foreach($benefit_details as $benefit){?>
				<tr class="table-row">
					<td><?php echo $benefit->benefit_type_title;?></td>
					<!--<td><?php /*echo $benefit->benefit_name;*/?></td>-->
					<td><?php echo $benefit->emp_name;?></td>
					<td><?php
                        $date=$benefit->date_approved;
                        if(!empty($date)){
                            $date_new=date("d-m-Y",strtotime($date));
                        echo $date_new;}?></td>
					<td><?php echo $benefit->status_title;?></td>
					<td><?php if($benefit->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_single_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<!--<td><a href="human_resource/edit_single_benefit/<?php /*echo $employee->employee_id;*/?>/<?php /*echo $benefit->emp_benefit_id;*/?>"><span class="fa fa-pencil"></span></a></td>-->
					<td><a href="human_resource/send_2_trash_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
    <div class="right-contents1" style="margin-top:20px;">
        <div class="head">Insurance Cover</div>
		<div id="txt" style="background-position: center">
			<?php echo  $this->session->flashdata('txt');?></div>
<form action="human_resource/edit_emp_entitlement_increment/<?php echo $this->uri->segment(3);?>" method="post">

        <div class="row">
            <h4>Insurance Type</h4>
            <?php echo form_dropdown('insurance_type_id',$insurance_type,'','id="drop_ins"');?>
			<a id="ins"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
        </div>
        <div class="row">
            <h4>Policy No</h4>
            <input type="text" name="insurance_no">
        </div>
		<div class="row">
			<h4>Certificate No</h4>
			<input type="text" name="certificate_number">

		</div>
        <div class="row">
            <h4>Date Effective From</h4>
            <input type="text" name="effective_from" id="joining_date">
        </div>
        <br class="clear">
        <div class="row">
            <div class="button-group">
                <input type="submit" name="add_insurance" value="Add" class="btn green" />
				<a href="human_resource/edit_emp_entitlement_increment/<?php echo (isset($employee) && !empty($employee))?$employee->employee_id:'';?>"> <input type="button" value="Cancel" class="btn gray" /></a>
            </div>
        </div>

</form>
        <table cellspacing="0">
            <thead class="table-head" style="background:#A3AAA3;">
            <td>Insurance</td>
            <td>Policy No</td>
            <td>Certificate No</td>
            <td>Effective From</td>
            <td>Delete</td>
            <td>Trash</td>
            </thead>
			<?php if(!empty($insurance)){
				foreach($insurance as $rec){
			?>
            <tr class="table-row">
                <td><?php echo $rec->insurance_type_name;?></td>
                <td><?php echo $rec->policy_number;?></td>
                <td><?php echo $rec->certificate_number;?></td>
                <td><?php
                    $idate=$rec->effective_from;
                    if(!empty($idate)){
                        $idate_new=date("d-m-Y",strtotime($idate));
                    echo $idate_new;}?></td>
                <td><a href="human_resource/delete_complete_insure/<?php echo $employee->employee_id;?>/<?php echo $rec->employee_insurance_id;?>" onclick="return confirm('Are You Sure..!')">Remove</a></td>
                <td><a href="human_resource/send_2_trash_insure/<?php echo $employee->employee_id;?>/<?php echo $rec->employee_insurance_id;?>/<?php echo 'employee_insurance_id';?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>

            </tr>
			<?php }} else{ echo" No record Founded";}?>
        </table>


    </div>
<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Leave</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_leave_edt/<?php echo $this->uri->segment(3)?>"><input type="button" name="add_increment" value="Add Leave" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Leave Type</td>
					<td>Leaves Allocated</td>
					<td>Leaves Entitled</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($leave_details)){
					foreach($leave_details as $leave){?>
				<tr class="table-row">
					<td><?php echo $leave->leave_type;?></td>
					<td><?php echo $leave->no_of_leaves_allocated;?></td>
					<td><?php echo $leave->no_of_leaves_entitled;?></td>
					<td><?php echo $leave->emp_name;?></td>
					<td><?php
                        $date=$leave->date_approved;
                        if(!empty($date)){
                            $date_new=date("d-m-Y",strtotime($date));
                        echo $date_new; }?></td>
					<td><?php echo $leave->status_title;?></td>

					<td><?php if($leave->status_title=="Approved"){?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved"></span><?php }else{?>
						<a href="human_resource/edit_single_leave_type/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-pencil"></span></a><?php }?></td>
					<td><a href="human_resource/send_2_trash_leave/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>" onclick="return confirm('Are You Sure..!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<div id="insurance" title="Add Insurance" style="display:none; width:600px;">
	<form id="insForm" action="human_resource/add_insure23/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="insurance_type_name" id="txt_drop"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script>
	$("#txt").delay(3000).fadeOut('slow');
	$(document).ready(function()
	{
		$("#insurance").on('click',function(e){
			e.preventDefault();
			var formData = $('#insForm').serialize();
			$.ajax({
				url: "human_resource/add_insure23/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_drop').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_ins').append(appendData);
						$("#insurance").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
</script>
<script>
	$(document).ready(function() {
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
	});

    //*********** Datepicker ****************//
    $( "#joining_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });
</script>
