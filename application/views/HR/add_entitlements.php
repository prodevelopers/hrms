<!-- contents -->
<style xmlns="http://www.w3.org/1999/html">
	.ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family: Verdana,Arial,sans-serif;
    font-size: 0.7em;
}
</style>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Entitlements</div> <!-- breadcrumb -->
	<?php $this->load->view('includes/employee_left_nav'); ?>
    <div class="right-contents1">
    <div id="accordion" class="marge entitlements_design">
	    <h4 class="head">Increments</h4>
		<div id="incrementsDiv">
			<?php echo form_open('human_resource/add_emp_entitlement_increment'); ?>
			<div id="alert" style="background-position: center">
				<?php echo  $this->session->flashdata('msg');?></div>
	        <input type="hidden" name="emp_id" value="<?php echo $empl->employment_id; ?>" />
	        	<br class="clear">
	            <div class="row increment-field">
						<h4>Increment Type *</h4>
						<?php echo @form_dropdown('increment_type_id', $increment_type,'','required="required" id="incrementTypeSelector"'); ?>
					</div>
					<div class="row increment-field">
						<h4>Increment (Amount) *</h4>
						<input type="text" name="increment_amount" id="incrementAmountTextBox" required="required">
					</div>
					<br class="clear">
					<div class="row increment-field">
						<h4>Date (Effective From) *</h4>
						<input type="text" name="date_effective" id="incrementDateEffective">
					</div>
					<br class="clear">
				<div class="row">
					<div class="button-group">
	                <input type="submit" name="add_increment" value="Add" id="add_increment" class="btn green" />
	                <input type="reset" value="Reset" class="btn gray" />
					</div>
				</div>
			<?php echo form_close(); ?>
					<table cellspacing="0" id="incrementTable">
					<thead class="table-head" style="background:#A3AAA3;">
						<td>Increment Type</td>
						<td>Increment</td>
						<td>Amount</td>
						<td>Delete</td>
					</thead>
                        <tbody>
	                <?php if(!empty($increment_details)){
						foreach($increment_details as $increment){?>
					<tr class="table-row">
						<td><?php echo $increment->increment_type;?></td>
						<td><?php echo (isset($increment) && !empty($increment->date_effective))?date('d-m-Y',strtotime($increment->date_effective)):'';?></td>
						<td><?php echo $increment->increment_amount;?></td>
						<td><a class="confirm" href="human_resource/trash_increment/<?php echo $increment->increment_id; ?>/<?php echo $employee->employee_id; ?>" onclick="return confirm('Are you Sure...!')">Remove</a></td>
					</tr>
	                <?php }} ?>
                        </tbody>
				</table>
				</div>
		<h4 class="head">Allowances</h4>
		<div id="allowanceID">
				<form action="human_resource/add_emp_entitlement_allowance" method="post" id="AloID" >
			<div id="txt" style="background-position: center">
				<?php echo  $this->session->flashdata('txt');?></div>
		        <input type="hidden" name="emp_id" value="<?php echo $empl->employment_id; ?>" />

						<div class="row">
							<h4>Allowance type</h4>
							<?php echo @form_dropdown('ml_allowance_type_id', $allowance_type,'required="required"','id="drop_allow"'); ?>
							<a id="at"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
						</div>
						<div class="row">
							<h4>Pay Frequency</h4>
							<?php echo @form_dropdown('pay_frequency', $pay_frequency,'required="required"','id="pay_allow"'); ?>
						</div>
						<div class="row">
							<h4>Amount</h4>
							<input type="text" name="allowance_amount" required="required" id="am_allow">
						</div>
		                <div class="row">
							<h4>Date Effective From</h4>
							<input type="text" name="effective_date" id="allowanceEffectiveDate">
						</div>
					<br class="clear">
					<div class="row">
						<div class="button-group">
		                <input type="button" id="add_allowance" value="Add" class="btn green" />
		                <input type="reset" value="Reset" class="btn gray" />
						</div>
					</div>
					</form>
					<table cellspacing="0" id="tbl_allow">
						<thead class="table-head" style="background:#A3AAA3;">
							<td>Allowance Type</td>
							<td>Pay Frequency</td>
							<td>Amount</td>
							<td>Date Effective From</td>
							<td>Delete</td>
						</thead>
                        <tbody>
		                <?php if(!empty($allowance_details)){
							foreach($allowance_details as $allowance){ ?>
						<tr class="table-row">
							<td><?php echo $allowance->allowance_type;?></td>
							<td><?php echo $allowance->pay_frequency;?></td>
							<td><?php echo $allowance->allowance_amount;?></td>
							<td><?php echo (isset($allowance) && !empty($allowance->effective_date))?date('d-m-Y',strtotime($allowance->effective_date)):'';?></td>
							<td><a class="confirm" href="human_resource/trash_emp_entitlement_allowance/<?php echo $allowance->allowance_id; ?>/<?php echo $employee->employee_id;?>">Remove</a> </td>
						</tr>
		                <?php } } ?>
                        </tbody>
					</table>
					</div>
		<h4 class="head">Benefits</h4>
		<div id="benefitsDiv">
			<?php echo form_open('human_resource/add_emp_entitlement_benefit'); ?>
			<div id="bn" style="background-position: center">
				<?php echo  $this->session->flashdata('smse');?></div>
	        <input type="hidden" name="emp_id" value="<?php echo $empl->employment_id; ?>" />
					<div class="row">
						<h4>Benefit type</h4>
						<?php echo @form_dropdown('benefit_type_id', $benefit_type,'required="required"','id="drop_ben"'); ?>
						<a id="ben"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
					</div>
					<div class="row increment-field">
						<h4>Date (Effective From)</h4>
						<input type="text" name="date_effective" id="benefitDateEffective">
					</div>
					<br class="clear">
				<div class="row">
					<div class="button-group">
	                <input type="submit" name="add_benefit" value="Add" id="add_benefit" class="btn green" />
	                <input type="reset" value="Reset" class="btn gray" />
					</div>
				</div>
				<?php echo form_close(); ?>
				<table cellspacing="0" id="benefitsTable">
					<thead class="table-head" style="background:#A3AAA3;">
                    <tr>
						<th>Benefit Type</th>
						<th>Date Effective From</th>
						<th>Delete</th>
                    </tr>
					</thead>
                    <tbody>
	                <?php if(!empty($benefit_details)){
						foreach($benefit_details as $benefit){
							?>
					<tr class="table-row">
						<td><?php echo $benefit->benefit_type_title;?></td>
						<td><?php echo (isset($benefit) && !empty($benefit->date_effective))?date('d-m-Y',strtotime($benefit->date_effective)):'';?></td>
						<td><a class="confirm" href="human_resource/trash_benefit_emp_entitlement/<?php echo $benefit->emp_benefit_id;?>/<?php echo $employee->employee_id;?>">Remove</a></td>
					</tr>
	                <?php } } ?>
                    </tbody>
				</table>
				</div>
	    <h4 class="head">Insurance</h4>
	    <div id="insuranceDiv">
	        <form action="human_resource/add_insure/<?php echo $employee->employee_id;?>" method="post">
				<div id="innn" style="background-position: center">
					<?php echo  $this->session->flashdata('smser');?></div>
	        <input type="hidden" name="emp_id" value="<?php echo $employee->employee_id;?>" />
	        <div class="row">
	            <h4>Insurance Type</h4>
				<?php echo form_dropdown('insurance_type_id',$insurance_type,'required="required"','id="drop_ins"');?>
				<a id="ins"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
	        </div>
	        <div class="row">
	            <h4>Policy No</h4>
				<input type="text" name="insurance_no" required="required">
	        </div>
				<div class="row">
					<h4>Certificate No</h4>
					<input type="text" name="certificate_number" required="required">
				</div>
	        <div class="row">
	            <h4>Date Effective From</h4>
	            <input type="text" name="effective_from" id="insuranceDateEffective">
	        </div>
	        <br class="clear">
	        <div class="row">
	            <div class="button-group">
	                <input type="submit" name="add_insurance" id="add_insurance" value="Add" class="btn green" />
	                <input type="reset" value="Reset" class="btn gray" />
	            </div>
	        </div>
	        </form>
			<table cellspacing="0" id="insuranceTable">
				<thead class="table-head" style="background:#A3AAA3;">
				<td>Insurance</td>
				<td>Policy No</td>
				<td>Certificate No</td>
				<td>Effective From</td>
				<td>Delete</td>
				</thead>
                <tbody>
				<?php if(!empty($insurance)){
					foreach($insurance as $rec){
						?>
						<tr class="table-row">
							<td><?php echo $rec->insurance_type_name;?></td>
							<td><?php echo $rec->policy_number;?></td>
							<td><?php echo $rec->certificate_number;?></td>
							<td><?php echo date_format_helper($rec->effective_from);?></td>
							<td><a class="confirm" href="human_resource/trash_insurance_emp_entitlement/<?php echo $rec->employee_insurance_id;?>/<?php echo $employee->employee_id;?>">Remove</a></td>
                        </tr>
					<?php }} else{ echo"<tr id='noRecordDummyRowInsurance'><td colspan='5'>No record Founded</td></tr>";}?>
                </tbody>
			</table>
	        </div>
		<h4 class="head">Leave</h4>
		<div id="leaveEntitlementsDiv">
			<div id="leav" style="background-position: center">
			<?php echo  $this->session->flashdata('lsms');?></div>
			<?php echo form_open('human_resource/add_emp_entitlement_leave'); ?>
	        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />

					<div class="row">
						<h4>Leave type</h4>
						<?php echo @form_dropdown('ml_leave_type_id', $leave_type,'required="required"','id="drop_leave"'); ?>
						<a id="lev"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>

					</div>

					<div class="row">
						<h4>Days Allocated</h4>
						<input type="text" name="no_of_leaves" required="required">
					</div>

					<br class="clear">
				<div class="row">
					<div class="button-group">
	                <input type="submit" name="add_leave" id="add_leave" value="Add" class="btn green"  />
	                <input type="reset" value="Reset" class="btn gray" />
					</div>
				</div>
				<?php echo form_close(); ?>


				<table cellspacing="0" id="leaveEntitlementsTable">
					<thead class="table-head" style="background:#A3AAA3;">
                    <tr>
						<th>Leave Type</th>
						<th>Leaves Allocated</th>
						<th>Leaves Entitled</th>
						<th>Approved Date</th>
						<th>Approved By</th>
						<th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
	               <?php if(!empty($leave_details)){
						foreach($leave_details as $leave){?>
					<tr class="table-row">
						<td><?php echo $leave->leave_type;?></td>
						<td><?php echo $leave->no_of_leaves_allocated;?></td>
						<td><?php echo $leave->no_of_leaves_entitled;?></td>
						<td><?php echo (isset($leave) && !empty($leave->date_approved))?date('d-m-Y',strtotime($leave->date_approved)):'';?></td>
						<td><?php echo $leave->emp_name;?></td>
						<td><a class="confirm" href="human_resource/trash_leave_emp_entitlement/<?php echo $leave->leave_entitlement_id;?>/<?php echo $employee->employee_id;?>">Remove</a> </td>
					</tr>
	                <?php } }?>
                    </tbody>
				</table>
		</div>

	</div> <div class="row">
		<div class="button-group">
	   <a href="human_resource/add_emp_report_to/<?php echo $this->uri->segment(3);?>"><input type="button" name="add_leave" value="Next" class="btn green" /></a>
		</div>
	</div>
    </div>

    </div>
    <!-- contents -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery_confirm/jquery.confirm.min.js"></script>
<script>
	$(document).ready(function()
	{
		$("#allowanceID").on('click','#add_allowance',function(e){
			e.preventDefault();
			var formData = $('#AloID').serialize();
			$.ajax({
				url: "human_resource/add_emp_entitlement_allowance/<?php echo @$employee->employee_id; ?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
                    var splitCharacter = '>>>';
                    var data;
                    var topData;
                    if(output.indexOf(splitCharacter) > -1){
                        topData = output.split('>>>');
                        data = topData[0].split('::');
                    }else{
                        data = output.split('::');
                    }
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);

                        //Converting String To JSON OBJECT ARRAY
                        var tableData = JSON.parse(topData[1]);
                        var $tr;
						$.each(tableData, function(i, item) {
                            var removeLink = '<a class="confirm" href="<?php echo base_url(); ?>human_resource/trash_emp_entitlement_allowance/'+item.AllowanceID+'/<?php echo $this->uri->segment(3); ?>">Remove</a>';
							$tr = $('<tr class="table-row">').append(
								$('<td>').text(item.AllowanceType),
								$('<td>').text(item.PayFrequency),
								$('<td>').text(item.AllowanceAmount),
								$('<td>').text(item.EffectiveDate),
								$('<td>').html(removeLink)
							);
                            //Append Elements To Empty Table
                            $tr.appendTo('#tbl_allow tbody');
						});

					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
                }
			});
		}); //End Of Adding Of Allowance

        $('#incrementsDiv').on('click','#add_increment',function(e){
            e.preventDefault();
            var dateEffectiveTextBox = $('#incrementDateEffective').val();
            var incrementAmountTextBox = $('#incrementAmountTextBox').val();
			var incrementTypeSelector = $('#incrementTypeSelector').val();

            if(incrementTypeSelector.length === 0){
                Parexons.notification('Please Select Increment Type','warning');
                return false;
            }

			if(incrementAmountTextBox.length === 0){
				Parexons.notification('Please Enter Amount of Increment','warning');
				return false;
			}

			if(dateEffectiveTextBox.length === 0){
				Parexons.notification('Please Select Effective Date','warning');
				return false;
			}
            var formData = $(this).closest('form').serializeArray();
            var addIncrementURL = "<?php echo base_url(); ?>human_resource/add_increment";
            $.ajax({
                url: addIncrementURL,
                data:formData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        //As Successfully Added In The Database So Lets Show the Data Inside The Table Also.
                        var date_effective = formData[3].value;
                        var selectedIncrementType = $("#incrementTypeSelector option:selected").text();
                        var incrementAmount = formData[2].value;
                        //Setting Up The TR for Table's body
                        var removeLink = '<a class="confirm" href="<?php echo base_url(); ?>human_resource/trash_increment/'+data[3]+'/<?php echo $this->uri->segment(3); ?>">Remove</a>';
                        var $tr = $('<tr class="table-row">').append(
                            $('<td>').text(selectedIncrementType),
                            $('<td>').text(date_effective),
                            $('<td>').text(incrementAmount),
                            $('<td>').html(removeLink)
                        );
                        $tr.appendTo('#incrementTable tbody');
                        //increments Form
                        var incrementsForm = $('#incrementsDiv').find('form');
                        incrementsForm[0].reset();

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax
        }); //End Of Adding Increment.

        $('#benefitsDiv').on('click','#add_benefit',function(e){
            e.preventDefault();
            var dateEffectiveTextBox = $('#benefitDateEffective').val();
            if(dateEffectiveTextBox.length === 0){
                Parexons.notification('Please Select Effective Date','warning');
                return false;
            }
            var formData = $(this).closest('form').serializeArray();
            var addIncrementURL = "<?php echo base_url(); ?>human_resource/add_benefit_emp_entitlement";
            $.ajax({
                url: addIncrementURL,
                data:formData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        //As Successfully Added In The Database So Lets Show the Data Inside The Table Also.
                        var date_effective = dateEffectiveTextBox;
                        var selectedBenefitType = $("#drop_ben option:selected").text();
                        //Setting Up The TR for Table's body
                        var removeLink = '<a class="confirm" href="<?php echo base_url(); ?>human_resource/trash_benefit_emp_entitlement/'+data[3]+'/<?php echo $this->uri->segment(3); ?>">Remove</a>';
                        var $tr = $('<tr class="table-row">').append(
                            $('<td>').text(selectedBenefitType),
                            $('<td>').text(date_effective),
                            $('<td>').html(removeLink)
                        );
                        $tr.appendTo('#benefitsTable tbody');
                        //increments Form
                        var incrementsForm = $('#benefitsDiv').find('form');
                        incrementsForm[0].reset();

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax
        }); //End Of Adding Increment.

        $('#insuranceDiv').on('click','#add_insurance',function(e){
            e.preventDefault();
            var dateEffectiveTextBox = $('#insuranceDateEffective').val();
            if(dateEffectiveTextBox.length === 0){
                Parexons.notification('Please Select Effective Date','warning');
                return false;
            }
            var formData = $(this).closest('form').serializeArray();
            var selectedForm = $(this).closest('form');
            var addInsuranceURL = "<?php echo base_url(); ?>human_resource/add_insurance_emp_entitlement";
            $.ajax({
                url: addInsuranceURL,
                data:formData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        //As Successfully Added In The Database So Lets Show the Data Inside The Table Also.
                        var date_effective = dateEffectiveTextBox;
                        var policyNo = selectedForm.find('input[name="insurance_no"]').val();
                        var certificateNumber = selectedForm.find('input[name="certificate_number"]').val();
                        var selectedInsuranceType = $("#drop_ins option:selected").text();
                        //Setting Up The TR for Table's body
                        var removeLink = '<a class="confirm" href="<?php echo base_url(); ?>human_resource/trash_insurance_emp_entitlement/'+data[3]+'/<?php echo $this->uri->segment(3); ?>">Remove</a>';
                        var $tr = $('<tr class="table-row">').append(
                            $('<td>').text(selectedInsuranceType),
                            $('<td>').text(policyNo),
                            $('<td>').text(certificateNumber),
                            $('<td>').text(date_effective),
                            $('<td>').html(removeLink)
                        );
						$('tr#noRecordDummyRowInsurance').remove();
                        $tr.appendTo('#insuranceTable tbody');
                        //increments Form
                        var incrementsForm = $('#insuranceDiv').find('form');
                        incrementsForm[0].reset();

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax
        }); //End Of Adding Increment.


        $('#leaveEntitlementsDiv').on('click','#add_leave',function(e){
            e.preventDefault();
            var formData = $(this).closest('form').serializeArray();
            var selectedForm = $(this).closest('form');
            var addInsuranceURL = "<?php echo base_url(); ?>human_resource/add_leaves_emp_entitlement";
            $.ajax({
                url: addInsuranceURL,
                data:formData,
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        //As Successfully Added In The Database So Lets Show the Data Inside The Table Also.
                        var leavesEntitled = selectedForm.find('input[name="no_of_leaves"]').val();
                        var selectedInsuranceType = $("#drop_leave option:selected").text();
                        //Setting Up The TR for Table's body
                        var removeLink = '<a class="confirm" href="<?php echo base_url(); ?>human_resource/trash_leave_emp_entitlement/'+data[3]+'/<?php echo $this->uri->segment(3); ?>">Remove</a>';
                        var $tr = $('<tr class="table-row">').append(
                            $('<td>').text(selectedInsuranceType),
                            $('<td>').text(leavesEntitled),
                            $('<td>').text(''),
                            $('<td>').text(''),
                            $('<td>').html(removeLink)
                        );
                        $tr.appendTo('#leaveEntitlementsTable tbody');
                        //increments Form
                        var incrementsForm = $('#leaveEntitlementsDiv').find('form');
                        incrementsForm[0].reset();

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax
        }); //End Of Adding Increment.
	});
</script>
<script type="text/javascript">
    $( "#accordion" ).accordion();
    $( "#accordion1" ).accordion();
</script>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<!-- designation dialog -->
	<div id="all-type" title="Add Allowance Type" style="display:none; width:600px;">
	<form id="allowForm" action="human_resource/add_allowance/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="allowance_type" required="required" id="txt_allow"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- designation dialog -->
	<div id="insurance" title="Add Insurance Type" style="display:none; width:600px;">
	<form id="insForm" action="human_resource/add_insure23/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="insurance_type_name" required="required" id="txt_ins"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- designaiton dialog -->
	<div id="benifit" title="Add Benefit Type" style="display:none; width:600px;">
	<form id="benForm" action="human_resource/add_benefit_type/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="benefit_type_title" required="required" id="txt_ben"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- designaiton dialog -->
	<div id="leave" title="Add Leave Type" style="display:none; width:600px;">
	<form id="leaveForm" action="human_resource/add_leave_type/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="leave_type" required="required" id="txt_leave"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script>
	$(document).ready(function()
	{
		$("#leave").on('click',function(e){
			e.preventDefault();
			var formData = $('#leaveForm').serialize();
			$.ajax({
				url: "human_resource/add_leave_type/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_leave').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_leave').append(appendData);
						$("#leave").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#insurance").on('click',function(e){
			e.preventDefault();
			var formData = $('#insForm').serialize();
			$.ajax({
				url: "human_resource/add_insure23/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_ins').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_ins').append(appendData);
						$("#insurance").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#all-type").on('click',function(e){
			e.preventDefault();
			var formData = $('#allowForm').serialize();
			$.ajax({
				url: "human_resource/add_allowance/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_allow').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_allow').append(appendData);
						$("#all-type").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#benifit").on('click',function(e){
			e.preventDefault();
			var formData = $('#benForm').serialize();
			$.ajax({
				url: "human_resource/add_benefit_type/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_ben').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_ben').append(appendData);
						$("#benifit").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

        //removeBtn Inside Increments Table
        $('#incrementTable').on('click','.confirm',function(e){
            e.preventDefault();
        if(confirm("Are you sure?")) {
            //delete here
            var deleteURL = $(this).attr('href');
            var selectedRemove = $(this);
            $.ajax({
               url:deleteURL,
                type: "POST",
                data:{delType:"delete"},
                success: function (output) {
                    var data  =  output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        selectedRemove.parents('tr').remove();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
            return false;
    });

        //removeBtn Inside Allowance
        $('#tbl_allow').on('click','.confirm',function(e){
            e.preventDefault();
        if(confirm("Are you sure?")) {
            //delete here
            var deleteURL = $(this).attr('href');
            var selectedRemove = $(this);
            $.ajax({
               url:deleteURL,
                type: "POST",
                data:{delType:"delete"},
                success: function (output) {
                    var data  =  output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        selectedRemove.parents('tr').remove();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
            return false;
    });

        //removeBtn Inside Benefits
        $('#benefitsTable').on('click','.confirm',function(e){
            e.preventDefault();
        if(confirm("Are you sure?")) {
            //delete here
            var deleteURL = $(this).attr('href');
            var selectedRemove = $(this);
            $.ajax({
               url:deleteURL,
                type: "POST",
                data:{delType:"delete"},
                success: function (output) {
                    var data  =  output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        selectedRemove.parents('tr').remove();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
            return false;
    });

        //removeBtn Inside Insurance
        $('#insuranceTable').on('click','.confirm',function(e){
            e.preventDefault();
        if(confirm("Are you sure?")) {
            //delete here
            var deleteURL = $(this).attr('href');
            var selectedRemove = $(this);
            $.ajax({
               url:deleteURL,
                type: "POST",
                data:{delType:"delete"},
                success: function (output) {
                    var data  =  output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
						var tbody = selectedRemove.parents('tbody');
                        selectedRemove.parents('tr').remove();

						if (tbody.children().length == 0) {
							tbody.html("<tr id='noRecordDummyRowInsurance'><td colspan='5'>No record Founded</td></tr>");
						}
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
            return false;
    });

        //removeBtn Inside Leave Entitlement Table
        $('#leaveEntitlementsTable').on('click','.confirm',function(e){
            e.preventDefault();
        if(confirm("Are you sure?")) {
            //delete here
            var deleteURL = $(this).attr('href');
            var selectedRemove = $(this);
            $.ajax({
               url:deleteURL,
                type: "POST",
                data:{delType:"delete"},
                success: function (output) {
                    var data  =  output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        selectedRemove.parents('tr').remove();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
            return false;
    });


        //DatePickers and Other Little Stuffs.
        $('#incrementDateEffective, #allowanceEffectiveDate, #benefitDateEffective, #insuranceDateEffective').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            yearRange:"<?php echo yearRang(); ?>"

        });
	});
</script>