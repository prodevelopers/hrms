<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Dependents</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Dependents</div>
		<?php echo form_open(); ?>
		<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Name</h4>
					<input type="text" name="dependent_name" value="<?php echo @$depend->dependent_name?>">
				</div>

				<br class="clear">

				<div class="row">
					<h4>Relation</h4>
                    <?php 
					$selctd_relation = (isset($depend->ml_relationship_id) ? $depend->ml_relationship_id : '');
					echo @form_dropdown('ml_relationship_id', $relation, $selctd_relation,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

				<br class="clear">

				<div class="row">
					<h4>Date of Birth</h4>
					<input type="text" name="date_of_birth" id="datebirth" value="<?php echo date_format_helper(@$depend->date_of_birth);?>">
				</div>

		<br class="clear">

		<div class="row">
			<h4>Existing Illness</h4>
			<!--<input type="text" name="illness" id="date_of_birth" value="<?php /*echo @$depend->illness*/?>">-->
			<input type="text" name="illness" id="" value="<?php echo @$depend->illness?>">
		</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>


		<?php /*?><div class="right-contents" style="margin-top:20px;">
			<div class="head">Assigned Dependents</div>

			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Name</td>
					<td>Relation</td>
					<td>Date of Birth</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($dependent_details)){
					foreach($dependent_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->dependent_name;?></td>
					<td><?php echo $row->relation_name;?></td>
					<td><?php echo $row->date_of_birth;?></td>
					<td><a href="human_resource/edit_emp_single_dependent/<?php echo $employee->employee_id; ?>/<?php echo $row->dependent_id; ?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_dependent/<?php echo $employee->employee_id; ?>/<?php echo $row->dependent_id; ?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php } } ?>
			</table>
		</div><?php */?>

	</div>
<!-- contents -->
<script>


    $( "#datebirth" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"


    });

</script>