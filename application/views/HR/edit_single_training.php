<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings / Edit Training</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	<div class="right-contents1">
			<div class="head">Edit/Update Trainings</div>
			<?php echo form_open('human_resource/edit_single_training/'.$employee->employee_id.'/'.$training->training_record_id); ?>
        	<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<br class="clear">
				<div class="row">
					<h4>Training Name</h4>
					<input type="text" name="training_name" value="<?php echo $training->training_name; ?>" required="required">
				</div>
				<div class="row">
					<h4>Institute</h4>
					<input type="text" name="institute" value="<?php echo $training->Institute; ?>" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>From Date</h4>
					<input type="text" name="from_date" id="fromDate" value="<?php echo date('d-m-Y',strtotime($training->frm_date)); ?>">
				</div>
				<div class="row">
					<h4>To Date</h4>
					<input type="text" name="to_date" id="toDate" value="<?php echo date('d-m-Y',strtotime($training->to_date)); ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Total Days</h4>
					<input type="text" name="total_days" id="totalDays" value="<?php echo $training->tdays; ?>" readonly>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Description</h4>
					<textarea name="description"><?php echo $training->description; ?></textarea>
				</div>

			<!-- button group -->
            <br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_training" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
	</div>
<!-- contents -->
<script>
    $(document).ready(function(e){

        var fromDateSelector = $('#fromDate');
        var toDateSelector = $('#toDate');
        fromDateSelector.datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,


            onClose: function( selectedDate ) {
                toDateSelector.datepicker( "option", "minDate", selectedDate );
            }
        }).change(function(e){
            var toDate = toDateSelector.val();
            var fromDate = $(this).val();
            if(toDate.length > 0 && fromDate.length > 0){
                var splittedToDate = toDate.split('-');
                var splittedFromDate = fromDate.split('-');
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(splittedFromDate[2],splittedFromDate[1],splittedFromDate[0]);
                var secondDate = new Date(splittedToDate[2],splittedToDate[1],splittedToDate[0]);
                var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
                $('#totalDays').val(diffDays + 1);
            }
        });

        toDateSelector.datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,


            onClose: function( selectedDate ) {
                fromDateSelector.datepicker( "option", "maxDate", selectedDate );
            }
        }).change(function(e){
            var toDate = $(this).val();
            var fromDate = fromDateSelector.val();
            if(toDate.length > 0 && fromDate.length > 0){
                var splittedToDate = toDate.split('-');
                var splittedFromDate = fromDate.split('-');
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(splittedFromDate[2],splittedFromDate[1],splittedFromDate[0]);
                var secondDate = new Date(splittedToDate[2],splittedToDate[1],splittedToDate[0]);
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
                $('#totalDays').val(diffDays + 1);
            }
        });
    });
</script>