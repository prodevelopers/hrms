<?php include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">
<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
	<div class="bredcrumb">Dashboard / Human Resource</div> 

	<?php include('includes/hr_left_nav.php'); ?>

	<div class="right-contents">

		<div class="head">Complete Details<a href="#"><span class="fa fa-print" style="float:right; padding-right:40px;padding-top:5px; color:#fff;"></span></a></div>

			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="5">Personal Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td rowspan="4" width="150"><img src="img/user.png" width="150" height="150"></td>
					<td>Employee Name</td>
					<td>Azhar Khan</td>
					<td>Employee ID</td>
					<td>987</td>
				</tr>
				<tr class="table-row">
					<td>Father Name</td>
					<td>Mohammad Ali</td>
					<td>CNIC</td>
					<td>13601-3673673-8</td>
				</tr>
				<tr class="table-row">
					<td>Martial Status</td>
					<td>Single</td>
					<td>Date Of Birth</td>
					<td>16-july-1990</td>
				</tr>	
				<tr class="table-row">
					<td>Nationality</td>
					<td>13601-3673673-8</td>
					<td>Religion</td>
					<td>13601-3673673-8</td>
				</tr>		
				</tbody>	
			</table>
			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="4">Contact Information (Current)</td>
				</thead>
				<tbody>

				<tr class="table-row">
					<td>Current address</td>
					<td>Abdara Road Deans Plaza Peshawar KPK</td>
					<td>Telephone</td>
					<td>+92187232323</td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td>+92344446738</td>
					<td>Email</td>
					<td>data@gmail.com</td>
				</tr>
				<tr class="table-row">
					<td>Work Telephone</td>
					<td>+08387223773</td>
					<td>Work Email</td>
					<td>wrorking@yahoo.com</td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Permanent)</td>
				</thead>
				<tr class="table-row">
					<td>Permanent address</td>
					<td>Abdara Road Deans Plaza Peshawar KPK</td>
						<td>Telephone</td>
					<td>+92187232323</td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td>+92344446738</td>
					<td>Email</td>
					<td>data@gmail.com</td>
				</tr>
				<tr class="table-row">
					<td>Work Telephone</td>
					<td>+08387223773</td>
					<td>Work Email</td>
					<td>wrorking@yahoo.com</td>
				</tr>	
				<thead class="table-head class">
					<td colspan="4">Contact Information (Emergency)</td>
				</thead>
				<tr class="table-row">
					<td>Emergency Contacts</td>
					<td>Abdara Road Deans Plaza Peshawar KPK</td>
						<td>Telephone</td>
					<td>+92187232323</td>
				</tr>
				<tr class="table-row">
					<td>Mobile</td>
					<td>+92344446738</td>
					<td>Email</td>
					<td>data@gmail.com</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Educational Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Degree</td>
					<td>Bs(Computer Science)</td>
					<td>Institiute</td>
					<td>IBMS/University Of Agriculture</td>
				</tr>
				<tr class="table-row">
					<td>Specialization</td>
					<td>Web Engineer</td>
					<td>GPA/Score</td>
					<td>3.55</td>
				</tr>
				<tr class="table-row">
					<td>Year Of Completion</td>
					<td>2012</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Experiance</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Job Title</td>
					<td>Head-Officer</td>
					<td>Organization</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
					<td>Employement Type</td>
					<td>Full Time</td>
					<td>Start Date</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>End Date</td>
					<td>12-08-2000</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Skills</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Skill Name</td>
					<td>Web-Designer</td>
					<td>Experiance in Years</td>
					<td>Company name</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Employement Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Current Job Title</td>
					<td>Head-Officer</td>
					<td>Job Specification</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
					<td>Employment Type</td>
					<td>Full Time</td>
					<td>Job Category</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>Department</td>
					<td>12-08-2000</td>
					<td>City</td>
					<td>12-08-2000</td>
				</tr>
				<tr class="table-row">
					<td>Location</td>
					<td>12-08-2000</td>
					<td>Work Shift</td>
					<td>12-08-2000</td>
				</tr>
				<tr class="table-row">
					<td>Contract Expiry Date</td>
					<td>12-08-2000</td>
				</tr>	
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Dependents</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td style="none;"><strong>Relationship</strong></td>
					<td>Date Of Birth</td>
				</tr>
				<tr class="table-row">
					<td>12-08-1992</td>
					<td>Company name</td>
					<td>Head-Officer</td>
				</tr>
				</tbody>	
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Pay Package</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td>Head-Officer</td>
					<td>Pay Frequency</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
					<td>Currency</td>
					<td>Full Time</td>
					<td>Payrate</td>
					<td>12-08-1992</td>
				</tr>	
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Entitlements</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Increments</td>
					<td>Head-Officer</td>
					<td>Increments Type</td>
					<td>Company name</td>
				</tr>
				<tr class="table-row">
				<td>Benefits</td>
					<td>Full Time</td>
					<td>Benefits Type</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>Allowance</td>
					<td>Full Time</td>
					<td>Allowance Type</td>
					<td>12-08-1992</td>
				</tr>
				<tr class="table-row">
					<td>Leaves</td>
					<td>Full Time</td>
					<td>Leave Type</td>
					<td>12-08-1992</td>
				</tr>
				</tbody>
			</table>
			<table>
			<thead class="table-head class">
					<td colspan="4">Report To</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td>Name</td>
					<td>Head-Officer</td>
					<td>Reporting Method</td>
					<td>Company name</td>
				</tr>
				
				</tbody>
			</table>
	</div>

<?php include('includes/footer.php'); ?>


