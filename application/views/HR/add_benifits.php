<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerParams": function (aoData, fnCallBack){
			//aoData.push({"name":"progress","value":$('#progress').val()});
			aoData.push({"name":"job_title","value":$('#jobtitle').val()});
			aoData.push({"name":"designation","value":$('#design').val()});
		}
	});
	
	
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});
	/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");		
	}
</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Add Benifitssssss</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_left_nav'); ?>

<div class="right-contents">
                    <div class="head">Employee Info</div>
                            <div class="row2">
                                <h5 class="headingfive">Employee Name</h5>
                                <i class="italica" ><?php echo @$assignd_benft->full_name;?></i>
                            </div>
                            <div class="row2">
                                <h5 class="headingfive">Designation</h5>
                                <i class="italica"><?php echo @$assignd_benft->designation_name;?></i>
                        </div>
                            <div class="row2">
                                <h5 class="headingfive">Benefits</h5>
                                <?php if(!empty($assignd_benft_wd)):
									foreach($assignd_benft_wd as $assignd_benft): ?>
                                <i class="italica"><?php echo @$assignd_benft->benefit_type_title;?>,</i>
                                <?php endforeach;
									endif;?>
                        </div>
                        <div class="row2">
                        <a href="human_resource/progress_report/<?php echo @$assignd_benft->employee_id;?>"><span class="btn green">Employee Progress</span></a>
                        </div>
            </div>
	</div>

         <div class="right-contents">

		<div class="head">Increments</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_benefit_increment/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Increment" class="btn green" /></a>
				</div>
			</div>
            <table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Increment Type</td>
					<td>Increment</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($increment_details)){
					foreach($increment_details as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->increment_type;?></td>
					<td><?php echo $increment->increment_amount;?></td>
					<td><?php echo $increment->emp_name;?></td>
					<td><?php echo $increment->approval_date;?></td>
					<td><?php echo $increment->status_title;?></td>
					<td><a href="human_resource/edit_emp_single_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>


		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Allowances</div>
        
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_allowance_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Allowance" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Allowance Type</td>
					<td>Ammount</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($allowance_details)){
					foreach($allowance_details as $allowance){?>
				<tr class="table-row">
					<td><?php echo $allowance->allowance_type;?></td>
					<td><?php echo $allowance->allowance_amount;?></td>
					<td><?php echo $allowance->emp_name;?></td>
					<td><?php echo $allowance->date_approved;?></td>
					<td><?php echo $allowance->status_title;?></td>
					<td><a href="human_resource/edit_emp_single_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_allowance/<?php echo $employee->employee_id;?>/<?php echo $allowance->allowance_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>

		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Benefits</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_benefit_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Benefit" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Benefit Type</td>
<!--					<td>Benefit</td>-->
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($benefit_details)){
					foreach($benefit_details as $benefit){?>
				<tr class="table-row">
					<td><?php echo $benefit->benefit_type_title;?></td>
					<td><?php //echo $benefit->benefit_name;?></td>
					<td><?php echo $benefit->emp_name;?></td>
					<td><?php echo $benefit->date_approved;?></td>
					<td><?php echo $benefit->status_title;?></td>
					<td><a href="human_resource/edit_single_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_benefit/<?php echo $employee->employee_id;?>/<?php echo $benefit->emp_benefit_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
<div class="right-contents" style="margin-top:20px;">

		<div class="head">Leave</div>
		
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_leave_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_increment" value="Add Leave" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Leave Type</td>
					<td>No of Days Allocated</td>
					<td>Approved By</td>
					<td>Approval Date</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($leave_details)){
					foreach($leave_details as $leave){?>
				<tr class="table-row">
					<td><?php echo $leave->leave_type;?></td>
					<td><?php echo $leave->no_of_leaves_allocated;?></td>
					<td><?php echo $leave->emp_name;?></td>
					<td><?php echo $leave->date_approved;?></td>
					<td><?php echo $leave->status_title;?></td>
					<td><a href="human_resource/edit_single_leave_type/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_leave/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>


		</div>
	

            <!-- Right Side Historic Detail -->
            
            <!-- Right Side Historic Detail -->

		</div>
        
        
        </div>
        </div>
	</div>
<!-- contents -->