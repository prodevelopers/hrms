<!--<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="assets/css/popup.css">

<script type="application/javascript">
	function check(input) {
		if(input.validity.typeMismatch){
			input.setCustomValidity("'" + input.value + "' is not a Valid Email Address.");
		}
		else {
			input.setCustomValidity("");
		}
	}

</script>
<!--<style>
    .style strong{padding:10px;}
    .left-nav ul li a{text-decoration: none;font-size: 14px;}
    .nav a:hover{text-decoration: none important;}
</style>-->
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Contact Details</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>


        <div class="right-contents1">
            <div class="head">Current Address Details</div>
            <?php

            if (isset($TCurrentContacts) && intval($TCurrentContacts) === 0) { ?>
            <div id="alert" style="background-position: center">
                <?php echo $this->session->flashdata('alert'); ?></div>
            <?php echo form_open('human_resource/add_emp_curr_contact'); ?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>"/>

            <div class="row">
                <h4>Address<i>*</i></h4>
                <input type="text" name="address" placeholder="Address ....">
            </div>
            <div class="row">
                <h4>City/Village</h4>
                <?php echo @form_dropdown('city_village', $city, '', 'class="cityVillageDropDown"'); ?>
                <a id="city"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
            </div>
            <div class="row">
                <h4>Province</h4>
                <?php echo @form_dropdown('province', $province, 'required="required', 'class="province_id_drop"'); ?>
                <a id="pro"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
            </div>
            <!-- <br class="clear"> -->
            <div class="row">
                <h4>Home Telephone</h4>
                <input type="text" name="home_phone" placeholder="Telephone number ....">
            </div>
            <br class="clear">

            <div class="row">
                <h4>Mobile</h4>
                <input type="text" name="mob_num" placeholder="Mobile number ....">
            </div>
            <div class="row">
                <h4>Private Email</h4>
                <input type="email" name="email_address" placeholder="Private Email ....">
            </div>
            <div>

            </div>
            <br class="clear">

            <div class="row">
                <h4><b>Work Contacts</b></h4>
            </div>
            <br class="clear">

            <div class="row">
                <h4>Work Telephone</h4>
                <input type="text" name="office_phone" placeholder="Office phone number ....">
            </div>

            <div class="row">
                <h4> Work Email</h4>
                <input type="email" name="official_email" placeholder="Email ....">
            </div>

            <h6>* Required Fields</h6>

            <!-- button group -->
            <br class="clear">

            <div class="row">
                <div class="button-group">
                    <input type="submit" name="continue" id="currentAddressDetails" value="Add" class="btn green"/>
                    <input type="reset" value="Reset" class="btn gray"/>
                </div>
            </div>
            <?php echo form_close();
            } else{
                /*echo "<div class='alert alert-success style' style='margin: 10px;'>
            <strong>Current Address</strong>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</div>.";*/

                echo "<div class='notify success'><span><strong>Current Address</strong></span>
            <span>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</span><i class='fa fa-home'></i></div>";
            }

            ?>
        </div>

<style>
    @media screen and (max-width: 850px){
        .success {
            width:95%;
            height: auto;
        }
    }
    @media screen and (max-width: 650px){
        .success {
            width:94%;
            height: auto;
        }
    }
    @media screen and (max-width: 500px){
        .success {
            width:85%;
            height: auto;
        }
    }
    /*As Above Red Button*/
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }



</style>


<div class="right-contents1" style="margin-top:20px;">
		<div class="head">Permanent Address Details
            <div class="btn-right">
                <a class="btn green" id="permAddAsAboveBtn">As Above</a>
            </div>
        </div>
    <?php if (isset($TPermanentContacts) && intval($TPermanentContacts) === 0) { ?>
	<div id="alert1" style="background-position: center">
		<?php echo  $this->session->flashdata('msg');?></div>
        <?php echo form_open('human_resource/add_emp_perm_contact');?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Address<i>*</i></h4>
					<input type="text" name="address" id="permAddressTextBox" required="required">
				</div>
				<div class="row">
					<h4>City/Village</h4>
					<?php echo @form_dropdown('city_village', $city,'','class="cityVillageDropDown" id="permCityVillageDropDown"'); ?>
               		<a id="city1"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>District (Domicile)</h4>
                    <?php echo @form_dropdown('district', $district,'','id="Drop_distt_id"'); ?>
					 <a id="dist"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Province</h4>
                    <?php echo @form_dropdown('province', $province,'','class="province_id_drop" id="permProvinceDropDown"'); ?>
                    <a id="pro1"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="phone" id="permHomePhone">
				</div>

				<h6>* Required Fields</h6>	

			<!-- button group -->
            <br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
        <?php echo form_close();
        } else{
            /*echo "<div class='alert alert-success style' style='margin: 10px;'>
                <strong>Permanent Address</strong>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</div>.";*/
        echo "<div class='notify success'><span><strong>Permanent Address</strong></span>
            <span>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</span><i class='fa fa-home'></i></div>";
    }
        ?>
		</div>

		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Emergency Contacts</div>

            <?php
//            If Not Set Emergency Contacts Then Show The Form
            if (isset($TEmergencyContacts) && intval($TEmergencyContacts) === 0) {
                ?>

                <div id="al" style="background-position: center">
                    <?php echo $this->session->flashdata('sms'); ?></div>
                <?php echo form_open('human_resource/add_emp_emerg_contact'); ?>
                <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>"/>
                <div class="row">
                    <h4>Name</h4>
                    <input type="text" name="cotact_person_name">
                </div>

                <div class="row">
                    <h4>Relationship</h4>
                    <?php echo @form_dropdown('relationship', $relation, '', 'id="relation_drop"'); ?>
                    <a id="rel"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
                </div>

                <div class="row">
                    <h4>Home Telephone</h4>
                    <input type="text" name="home_phone">
                </div>

                <div class="row">
                    <h4>Mobile</h4>
                    <input type="text" name="mobile_num">
                </div>

                <br class="clear">
                <div class="row">
                    <h4>Work Telephone</h4>
                    <input type="text" name="work_phone">
                </div>
                <br class="clear">
                <div class="row">
                    <div class="button-group">
                        <input type="submit" name="continue" value="Add" class="btn green" onclick="return check();"/>
                        <input type="reset" value="Reset" class="btn gray"/>
                    </div>
                </div>
                <?php echo form_close();

            }else{
                /*echo "<div class='alert alert-success style' style='margin: 10px;'>
                <strong>Emergency Contacts</strong>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</div>.";*/
                echo "<div class='notify success'><span><strong>Emergency Contacts</strong></span>
            <span>Already Added Sucessfully. To Update Go to Edit Section in Human Resource</span><i class='fa fa-home'></i></div>";
            }
        ?>


		</div>
	</div>
	<!-- Dialog Sections -->
	<!-- City dialog -->
	<div id="city-pop"  title="Add City" style="display:none; width:600px;">
	<form id="cityForm" name="city" action="human_resource/add_city2/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="city_name" id="cityNameTextField"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="simple-post">
		</div>
	</form>
	</div>
	<!-- Province dialog -->
	<div id="province" title="Add Province" style="display:none; width:600px;">
	<form id="provinceForm" action="human_resource/add_province2/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="province_name" id="provinceID"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="province">
		</div>
	</form>
	</div>
	<!-- Relation dialog -->
	<div id="relation" title="Add Relation" style="display:none; width:600px;">
	<form id="relationForm" action="human_resource/add_relation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="relation_name" id="txt_relation" />
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="relation">
		</div>
	</form>
	</div>
	<!-- District dialog -->
	<div id="district" title="Add District" style="display:none; width:600px;">
	<form id="disttForm" action="human_resource/add_distt/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="district_name" id="Dist_id"/>
			<br><br>
			<input type="button" value="Add" class="btn green addedto" name="add" id="distt">
		</div>
	</form>
	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<script>
	$("#alert").delay(3000).fadeOut('fast');
	$("#alert1").delay(3000).fadeOut('fast');
	$("#al").delay(3000).fadeOut('fast');
</script>
<script>
	$(document).ready(function(){
		$("#simple-post").on('click',function(e){
            e.preventDefault();
           var formData = $('#cityForm').serialize();
            $.ajax({
                url: "human_resource/add_city2/<?php echo @$employee->employee_id;?>",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        var cityNameEnteredValue = $('#cityNameTextField').val();
                        var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
                        $('.cityVillageDropDown').append(appendData);
                        $("#city-pop").dialog("close");
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });


		});

        //Adding Field
        $("#province").on('click',function(e){
            e.preventDefault();
            var formData = $('#provinceForm').serialize();
            $.ajax({
                url: "human_resource/add_province2/<?php echo @$employee->employee_id;?>",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        var provinceNameEnterdValue = $('#provinceID').val();
                        var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
                        $('.province_id_drop').append(appendData);
                        $("#province").dialog("close");
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        //Adding District Field
        $("#district").on('click',function(e){
            e.preventDefault();
            var formData = $('#disttForm').serialize();
            $.ajax({
                url: "human_resource/add_distt/<?php echo @$employee->employee_id;?>",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        var provinceNameEnterdValue = $('#Dist_id').val();
                        var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
                        $('#Drop_distt_id').append(appendData);
                        $("#district").dialog("close");
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

        //Adding Field
        $("#relation").on('click',function(e){
            e.preventDefault();
            var formData = $('#relationForm').serialize();
            $.ajax({
                url: "human_resource/add_relation/<?php echo @$employee->employee_id;?>",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        var provinceNameEnterdValue = $('#txt_relation').val();
                        var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
                        $('#relation_drop').append(appendData);
                        $("#relation").dialog("close");
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });


        $('#permAddAsAboveBtn').on('click', function (e) {
            e.preventDefault();
            //When the Button is Pressed We Need To Gather Data From Current Contacts for This Employee.
            //Ajax Should Go Here..
            var postData = {
                empID: "<?php echo $this->uri->segment(3); ?>"
            };
            $.ajax({
                url:"human_resource/getCurrentAddressDetails/",
                data:postData,
                type:"POST",
                success:function(output){
                    //output if success
                    try{
                        var outputData = JSON.parse(output);

                        //For Address Field
                        $('#permAddressTextBox').val(outputData.Address);

                        //For City DropDown
                        $('#permCityVillageDropDown option').filter(function() {
                            return ($(this).text() == outputData.City); //To select Blue
                        }).prop('selected', true);


                        //For District
                        $('#Drop_distt_id option').filter(function() {
                            return ($(this).text() == outputData.District); //To select Blue
                        }).prop('selected', true);


                        //For Province
                        $('#permProvinceDropDown option').filter(function() {
                            return ($(this).text() == outputData.Province); //To select Blue
                        }).prop('selected', true);


                        //For Perm Home Phone
                        $('#permHomePhone').val(outputData.Phone);
                    }
                    catch(ex){

                    }

                }
            });

        });
	});
</script>