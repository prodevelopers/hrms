<?php include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php include('includes/hr_left_nav.php'); ?>
	

	<div class="right-contents">

		<div class="head">Benifits Management</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Job Title</option>
				</select>
				<select>
					<option>Designation</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>


			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Job Title</td>
					<td>Job Category</td>
					<td>Status</td>
				</tr>
			</table>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="add_benifits.php"><button class="btn green">Add Benifits</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
