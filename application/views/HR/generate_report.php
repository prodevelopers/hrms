<div class="contents-container">
    <link rel="stylesheet" href="css/bootstrap-3.1.1.min.css" type="text/css"/>
    <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
    <div class="bredcrumb">Dashboard / Human Resource / Report Generation</div>
    <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>    <div class="right-contents">
        <div class="head">Generate Report</div>
        <form>
            <div class="row">
                <h4>Employee Name</h4>
                <input type="hidden" id="selectEmployees">
            </div>
            <div class="row">
                <h4>Project Name</h4>
                <input type="hidden" id="selectProjects">
            </div>
            <br class="clear">

            <div class="row">
                <h4>Domicile</h4>
                <input type="hidden" id="selectDomicile">
            </div>
            <div class="row">
                <h4>Report Heading (Optional)</h4>
                <input type="text" id="reportHeading" placeholder="Heading For Report (Optional)">
            </div>
        </form>
        <br class="clear">
        <table style="width:98%" id="checkBoxSelects">
            <tr class="table-row">
                <td><input type="checkbox" name="NationalIDCardNo"></td>
                <td>CNIC</td>
                <td><input type="checkbox" name="ET.PS.MLDP.Department"></td>
                <td>Department</td>
                <td><input type="checkbox" name="MLC.CityVillage"></td>
                <td>Village/City</td>
                <td><input type="checkbox" name="MLD.Domicile"></td>
                <td>Domicile</td>
                <td><input type="checkbox" name="DOB"></td>
                <td>Date Of Birth</td>
                <td><input type="checkbox" name="ET.PM.MLDg.Designation"></td>
                <td>Designation</td>
                <td><input type="checkbox" name="ET.PM.MLPG.Grade"></td>
                <td>Grade</td>
                <td><input type="checkbox" name="QF.Qualification"></td>
                <td>Qualification</td>
            </tr>
            <tr class="table-row">
                <td><input type="checkbox" name="ET.PS.MLB.OfficeLocation"></td>
                <td>Office/Location</td>
                <td><input type="checkbox" name="ET.JoiningDate"></td>
                <td>Joining Date</td>
                <td><input type="checkbox" name="PC.PostalAddress"></td>
                <td>Postal Address</td>
                <td><input type="checkbox" name="CC.Contact"></td>
                <td>Contact</td>
                <td><input type="checkbox" name="CC.OfficialEmail"></td>
                <td>Official Email</td>
            </tr>
            <tr class="table-row">
                <td><input type="checkbox" name="ET.EI.MLIT.InsuranceNo"></td>
                <td>Insurance No.</td>
                <td><input type="checkbox" name="DD.EOBI"></td>
                <td>EOBI NO</td>
                <td><input type="checkbox" name="ET.CN.ContractExpiry"></td>
                <td>Contract Expiry</td>
                <td><input type="checkbox" name="ED.Dependents"></td>
                <td>Dependents</td>
                <td><input type="checkbox" name="GEN.gender_type_title"></td>
                <td>Gender</td>
            </tr>
        </table>
        <!-- button group -->
        <div class="row">
            <input type="button" class="btn green" value="Generate" id="generateReportButton">
        </div>
    </div>
</div>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function (e) {
        /**
         * Need To Populate the Select Inputs
         */
        //selectEmployees
        var selectEmployees = $('#selectEmployees');
        var url = "<?php echo base_url(); ?>human_resource/loadAvailableEmployees";
        var id = "EmployeeID";
        var text = "EmployeeName";
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var multiple = true;
        commonSelect2(selectEmployees, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width", "223px");
        selectEmployees.on('change',function(e){
            var values = $(this).select2('data');
            if(values.length > 0){
                $('#selectProjects').select2('disable');
                $('#selectDomicile').select2('disable');
            }else{
                $('#selectProjects').select2('enable');
                $('#selectDomicile').select2('enable');
            }
        });

        //selectProjects
        var selectProjects = $('#selectProjects');
        var url = "<?php echo base_url(); ?>human_resource/loadAvailableProjects";
        var id = "ProjectID";
        var text = "ProjectName";
        var minInputLength = 0;
        var placeholder = "Select Projects";
        var multiple = true;
        commonSelect2(selectProjects, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width", "223px");
        selectProjects.on('change',function(e){
            var values = $(this).select2('data');
            if(values.length > 0){
                $('#selectEmployees').select2('disable');
                $('#selectDomicile').select2('disable');
            }else{
                $('#selectEmployees').select2('enable');
                $('#selectDomicile').select2('enable');
            }
        });

        //selectEmployeeDomiciles
        var selectDomicile = $('#selectDomicile');
        var url = "<?php echo base_url(); ?>human_resource/loadAvailableEmployeeDomiciles";
        var id = "DistrictID";
        var text = "DistrictName";
        var minInputLength = 0;
        var placeholder = "Select Domiciles";
        var multiple = true;
        commonSelect2(selectDomicile, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width", "223px");
        selectDomicile.on('change',function(e){
            var values = $(this).select2('data');
            if(values.length > 0){
                $('#selectEmployees').select2('disable');
                $('#selectProjects').select2('disable');
            }else{
                $('#selectEmployees').select2('enable');
                $('#selectProjects').select2('enable');
            }
        });
        //End of the Common Select2 DropDowns.

        /**
         * Now Need to Work on CheckBoxes and the Generate Button..
         */
        $('#generateReportButton').on('click', function (e) {
            var Selects = new Array();
            var array = $('#checkBoxSelects input:checked');
            $.each(array, function (i, e) {
                if ($(e).attr("id") != 'selectAllCheckBoxes') {
                    Selects.push($(e).attr('name'))
                }
            });

            var selectedEmployees = JSON.stringify(selectEmployees.select2('data'));
            var selectedProjects = JSON.stringify(selectProjects.select2('data'));
            var selectedDomiciles = JSON.stringify(selectDomicile.select2('data'));
            var reportHeading = $('#reportHeading').val();


            //if the ARRAY IS NOT empty then redirect
            var redirectURL = "<?php echo base_url() ?>human_resource/generate_report_print";
            var postData = {
                'selects': Selects
            };
            if(selectedEmployees != '[]'){
                postData.employees = selectedEmployees;
            }
            if(selectedProjects != '[]'){
                postData.projects = selectedProjects;
            }
            if(selectedDomiciles != '[]'){
                postData.domiciles = selectedDomiciles;
            }
            if(reportHeading.length > 0){
                postData.reportHeading = reportHeading;
            }
            $.redirect(redirectURL,postData,'POST');
        });
    });
</script>

