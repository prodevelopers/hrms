<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Qualification</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<script type="text/javascript">
				 $(document).ready(function(){
				 	$(".diploma-field").hide();
				 	$(".degree-field").hide();
				 	$(".certificate-field").hide();
      			 $("select").change(function(){
            $( "select option:selected").each(function(){
                if($(this).attr("value") == "1"){
                    $(".degree-field").show();
                    $(".certificate-field").hide();
                    $(".diploma-field").hide();
                }
                if($(this).attr("value") == "2"){
                    $(".degree-field").hide();
                    $(".certificate-field").hide();
                    $(".diploma-field").show();
                }
                if($(this).attr("value") == "3"){
                    $(".degree-field").hide();
                    $(".certificate-field").show();
                    $(".diploma-field").hide();
                }
            });
        }).change();
    });
			</script>

	<div class="right-contents1">
		<div class="head">Employee Qualification</div>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
					
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_qualification/<?php echo $this->uri->segment(3)?>"><input type="submit" name="continue" value="Add Qualification" class="btn green" /></a>
				<!--<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />-->
				</div>
			</div>
		</div>

		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Education & Qualifications</div>

			<table cellspacing="0" >
				<thead class="table-head secondry">
					<td>Qualification</td>
					<td>Type</td>
					<td>Institute</td>
					<td>Year</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($qualification_detail)){
					foreach($qualification_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->qualification; ?></td>
					<td><?php echo $row->qualification_title; ?></td>
					<td><?php echo $row->institute; ?></td>
					<td><?php echo date_format_helper($row->year); ?></td>
					<td><a href="human_resource/edit_emp_qualification_single/<?php echo $employee->employee_id; ?>/<?php echo $row->qualification_id; ?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_qualification/<?php echo $employee->employee_id; ?>/<?php echo $row->qualification_id; ?>/<?php echo'qualification_id';?>" onclick="return confirm('Are You Sure...! ')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>				
			</table>			
			
		</div>

	</div>
<!-- contents -->