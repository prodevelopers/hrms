<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Dependents</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Dependents</div>
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_dependent_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="continue" value="Add Dependent" class="btn green" /></a>
				</div>
			</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Name</td>
					<td>Relation</td>
					<td>Date of Birth</td>
					<td>Existing Illness</td>
					<td>Insurance Start Date</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($dependent_details)){
					foreach($dependent_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->dependent_name;?></td>
					<td><?php echo $row->relation_name;?></td>
					<td><?php
                        $date_birth=$row->date_of_birth;
                        if(!empty($date_birth)){
                        $date_new_b=date("d-m-Y",strtotime($date_birth));

                        echo $date_new_b;}?></td>
					<td><?php echo $row->illness;?></td>
					<td><?php
                        $date=$row->insurance_start_date;
                        if(!empty($date)){
                        $date_new=date("d-m-Y",strtotime($date));

                        echo $date_new;}?></td>
					<td><a href="human_resource/edit_emp_single_dependent/<?php echo $employee->employee_id; ?>/<?php echo $row->dependent_id; ?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_dependent/<?php echo $employee->employee_id; ?>/<?php echo $row->dependent_id; ?>/<?php echo 'dependent_id';?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->