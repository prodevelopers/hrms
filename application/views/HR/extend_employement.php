<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Employment Management / Extensions</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>




	<div class="right-contents">

		<div class="head">Contract Extension</div>
		<div class="form-right">
			<table class="table">
				<tr class="table-row">
					<td>Employee Name</td>
					<td><?php echo @$employee->full_name;?></td>
				</tr>
				<tr class="table-row">
					<td>Employee ID</td>
					<td><?php echo @$employee->employee_code;?></td>
				</tr>
			</table>
		</div>

		<br class="clear">
					<div class="head">Extend Employment Contract</div>

        <?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />

		<div class="form-left">
			<br class="clear">
			<div class="row2">
				<h4>Extension Start Date</h4>
				<input type="text" name="extension_start_date" id="joining_d" value="<?php echo date("Y-m-d");?>" readonly>
			</div>

				<br class="clear">
				<div class="row2">
					<h4>Extension Expiry Date</h4>
					<input type="text" name="extension_expiry_date" id="Exp_date" placeholder="Set Extension Expiry Date">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Set Contract Expiry Alert</h4>
                   <select name="exp_alert">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                    </select>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Expiry Alert Date</h4>
					<input type="text" name="date_contract_expiry_alert" id="contract_exp_date_id" placeholder="Set Expiry Alert Date">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Comments</h4>
					<textarea cols="50" rows="7" name="comments" placeholder="Comments to Identify the Reason to Extend the Contract"></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row2">
				<div class="button-group">
                <input type="submit" name="updateBtn" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>

			</div>
			</div>
            <?php echo form_close(); ?>

		<div class="form-right">
			<div class="head">Contracts</div>


			<?php //foreach($history as $benefit):
			//echo "<pre>"; print_r($benefit);
			?>
			<table class="table">
				<tr class="table-row">
					<td>Cotratct Start</td>
					<td><?php  if(empty($rec)){echo"No Contract Founded";}else {echo date_format_helper($rec->contract_start_date);}?></td>
				</tr>
				<tr class="table-row">
					<td>Cotratct Expire</td>
					<td><?php  if(empty($rec)){echo"No Contract Founded";}else {echo date_format_helper($rec->contract_expiry_date);}?></td>
				</tr>
				<tr class="table-row">
					<td>Employment Type</td>
					<td><?php  echo $rec->employment_type;?></td>
				</tr>
				<tr class="table-row">
					<td>Job Category</td>
					<td><?php  echo $rec->job_category_name;?></td>
				</tr>
				<tr class="table-row">
					<td>Cotratct Comments</td>
					<td><?php  echo $rec->comments;?></td>
				</tr>
			</table>

			
		</div>


		</div>
		</div>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/hr_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

	$('#joining_d').datepicker({
		dateFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true
	});
    </script>
    <!-- leftside menu end -->


		
	</div>
<!-- contents -->