<?php include('includes/header.php'); ?>
<div class="contents-container">
 
<link rel="stylesheet" href="css/bootstrap-3.1.1.min.css" type="text/css"/>
<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
 
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect').multiselect();
});
</script>
	<?php include('includes/hr_left_nav.php'); ?>

	<div class="right-contents">

		<div class="head">Assign Job</div>


			<form>
				<div class="row">
					<h4>Project</h4>
					<select>
						<option></option>
					</select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Task</h4>
					<select>
						<option></option>
					</select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Start Date</h4>
					<input type="text">
				</div>
				<br class="clear">
				<div class="row">
					<h4>End Date</h4>
					<input type="text">
				</div>
				<br class="clear">
				
				<div class="row">
				<h4>Key Performance Indicators</h4>
					<select class="multiselect" multiple="multiple">
					<option value="cheese">Cheese</option>
					<option value="tomatoes">Tomatoes</option>
					<option value="mozarella">Mozzarella</option>
					<option value="mushrooms">Mushrooms</option>
					<option value="pepperoni">Pepperoni</option>
					<option value="onions">Onions</option>
					</select>
				</div>

				<br class="clear">
				<div class="row">
					<h4>Task / Project Description</h4>
					<textarea></textarea>
				</div>
			</form>
			<div class="row">
				<div class="button-group">
					<a href="assign-job.html"><button class="btn green">Assign Job</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
<table cellspacing="0">
				<thead class="table-head">
					<td>Project Name</td>
					<td>Task</td>
					<td>Start Date</td>
					<td>Completion Date</td>
					<td>Key Performance</td>
					<td>Total Hours</td>
				</thead>
				<tr class="table-row">
					<td>Project Name</td>
					<td>Activity Name</td>
					<td>22-08-2014/09:30am</td>
					<td>22-08-2014/08:30am</td>
					<td>8hr 40min</td>
					<td>Total Hours</td>
				</tr>
				<tr class="table-row">
					<td>Project Name</td>
					<td>Activity Name</td>
					<td>22-08-2014/09:30am</td>
					<td>22-08-2014/08:30am</td>
					<td>8hr 40min</td>
					<td>Total Hours</td>
				</tr>
			</table>
			<!-- button group -->
			
		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
