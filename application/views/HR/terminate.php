<?php include('includes/header.php'); ?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->

<?php include('includes/hr_left_nav.php'); ?>
	

	<div class="right-contents">

		<div class="head">Terminate Employees</div>

			<div class="selected-list">
				<div class="list-row">
				<h4>Employee</h4>
				<h4>Contract</h4>
				<h4>22-11-2013</h4>
				<h4>22-11-2014</h4>
				<span class="fa fa-times"></span>
			</div>

			<div class="list-row">
				<h4>Employee</h4>
				<h4>Contract</h4>
				<h4>22-11-2013</h4>
				<h4>22-11-2014</h4>
				<span class="fa fa-times"></span>
			</div>
				<h6>View 2 More Employees</h6>
			</div>

			<form>
				<div class="row">
					<h4>Reason</h4>
					<select>
						<option></option>
					</select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Date</h4>
					<input type="text">
				</div>
				
				<br class="clear">
				<div class="row">
					<h4>Note</h4>
					<textarea></textarea>
				</div>

				
			</form>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<button class="btn green">Terminate</button>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
