<!-- contents -->
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Skills</div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_skill_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_skill" value="Add Skill" class="btn green" /></a>
				</div>
			</div>			
				
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Skill Name</td>
					<td>Level</td>
					<td>Remarks</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($skill_details)){
					foreach($skill_details as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->skill_name;?></td>
					<td><?php echo $row->skill_level;?></td>
					<td><?php if(!empty($row->comments)){echo $row->comments;}?></td>
					<td><a href="human_resource/edit_single_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_skill/<?php echo $employee->employee_id; ?>/<?php echo $row->skill_record_id; ?>/<?php echo 'skill_record_id';?>" onclick="return confirm('Are You Sure..!')" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Trainings</div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                 <a href="human_resource/add_training_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="add_training" value="Add Training" class="btn green" /></a>
				</div>
			</div>
            <?php echo form_close(); ?>
			
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Training</td>
					<td>Institute</td>
					<td>From Date</td>
					<td>To Date</td>
					<td>Description</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($training_detail)){
					foreach($training_detail as $train){ ?>
				<tr class="table-row">
					<td><?php echo $train->training_name;?></td>
					<td><?php echo $train->Institute;?></td>
					<td><?php echo (isset($train) && !empty($train->frm_date))?date("d-m-Y",strtotime($train->frm_date)):''; ?></td>
					<td><?php echo (isset($train) && !empty($train->to_date))?date("d-m-Y",strtotime($train->to_date)):''; ?></td>
					<td><?php echo $train->description;?></td>
					<td><a href="human_resource/edit_single_training/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_training/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>/<?php echo 'training_record_id';?>" ><span class="fa fa-trash-o" onclick="return confirm('Are You Sure..!')"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->
<script>
    $(document).ready(function () {
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
if(!empty($message) && isset($message)){
    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>