<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Increments</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">

		<div class="head">Increments</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        
            <div class="row increment-field">
					<h4>Increment Type</h4>
					<?php 
					$slctd_increment = (isset($increment->increment_type_id) ? $increment->increment_type_id : '');
					echo @form_dropdown('increment_type_id', $increment_type, $slctd_increment,'required="required"','required="required"'); ?>
                  <!--<select>
                      <option></option>
                  </select>-->
				</div>

        		<br class="clear">
				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text" name="increment_amount" value="<?php echo @$increment->increment_amount; ?>">
				</div>
        		<br class="clear">
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="date_effective" value="<?php echo (isset($increment) && !empty($increment->date_effective))?date_format_helper($increment->date_effective):''; ?>">
				</div>

			<!-- button group -->
        	<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_increment" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
		<?php echo form_close(); ?>
		</div>

	</div>
<!-- contents -->
<script>
$("#alert").delay(3000).fadeOut('slow');
$( "#date_effective" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
changeMonth: true,
changeYear: true
/*onClose: function( selectedDate ) {
$("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
}*/

});
</script>

<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/leave_left_nav.php'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- leftside menu end -->