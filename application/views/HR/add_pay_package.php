<?php $count= count($rec);
if($count > 0)
{
	?>
<script>
	$(document).ready(function (e) {
		$('#add_recordDiv').css('display','none');
	});

</script>
<?php
}
?>

<!-- contents -->
<script>
	$(document).ready(function(){
		$(".Monthly").hide();
		$(".Weekly-pay").hide();
		$(".Hourly-pay").hide();
		$(".Daily-pay").hide();
		$(".Yearly-pay").hide();});
</script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Pay Package</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Pay Package</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php echo  $this->session->flashdata('alert');?></div>
		<?php //echo form_open(); ?>
		<div id="add_recordDiv">
         <form action="" method="post" name="form1" onsubmit="return validation()">
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        <input type="hidden" name="employment_id" value="<?php echo @$employment->employment_id; ?>" />
        		<div class="row">
					<h4>Pay Grade</h4>
                    <?php echo @form_dropdown('ml_pay_grade', $pay_grade ,'required="required"','required="required"'); ?>
				</div>

				<br class="clear">

				<div class="row">
					<h4>Pay Frequency</h4>
                    <?php echo @form_dropdown('ml_pay_frequency', $pay_frequency,'required="required"','required="required"'); ?>
				</div>

				<br class="clear">

				<div class="row">
					<h4>Currency</h4>
                    <?php 
					echo @form_dropdown('ml_currency', $currency,'required="required"','id="drop_curr"'); ?>
					<a id="curr"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<br class="clear">
				<script type="text/javascript">
					$(document).ready(function(){


      			 $(".hay").change(function(){
            $( ".hay option:selected").each(function(){
                if($(this).attr("value")=="1"){
                    $(".Monthly").show();
                    $(".Hourly-Pay").hide();
                    $(".Daily-pay").hide();
					$(".Yearly-pay").hide();
					$(".Weekly-pay").hide();
                }
                if($(this).attr("value")=="2"){
					$(".Monthly").hide();
					$(".Hourly-Pay").show();
					$(".Daily-pay").hide();
					$(".Yearly-pay").hide();
                    $(".Weekly-pay").hide();

                }
                if($(this).attr("value")=="3"){
					$(".Monthly").hide();
                    $(".Daily-pay").show();
                    $(".Weekly-pay").hide();
					$(".Yearly-pay").hide();
					$(".Hourly-Pay").hide();
                }
				if($(this).attr("value")=="4"){
					$(".Monthly").hide();
					$(".Daily-pay").hide();
					$(".Weekly-pay").show();
					$(".Yearly-Pay").hide();
					$(".Hourly-Pay").hide();
				}

				if($(this).attr("value")=="5"){
					$(".Monthly").hide();
					$(".Daily-pay").hide();
					$(".Weekly-pay").hide();
					$(".Yearly-Pay").show();
					$(".Hourly-Pay").hide();
				}
            });
        }).change();
    });

				</script>

				<div class="row">
					<h4>Payrate</h4>
                    <?php echo @form_dropdown('ml_pay_rate_id', $pay_rate,'', "class='hay'"); ?>
					<!--<select class="hay">
						<option>Please Select</option>
						<option value="1" id="Monthly">Monthly</option>
						<option value="2" id="Weekly">Weekly</option>
						<option value="3" id="Daily">Daily</option>
					</select>-->
				</div>

				<br class="clear">



				<div class="row Weekly-pay" style="display: none">
					<h4>Weekly Pay</h4>
					<input type="text" name="Weekly_pay">
				</div>

				<div class="row Daily-pay" style="display: none">
					<h4>Daily Pay</h4>
					<input type="text" name="Daily_pay">
				</div>
			 <div class="row Monthly" style="display: none">
				 <h4>Monthly Pay</h4>
				 <input type="text" name="Monthly">
			 </div>

			 <div class="row Yearly-Pay" style="display: none">
				 <h4>Yearly Pay</h4>
				 <input type="text" name="Yearly-Pay">
			 </div>
			 <div class="row Hourly-Pay" style="display: none">
				 <h4>Hourly Pay</h4>
				 <input type="text" name="Hourly-Pay">
			 </div>

				<br class="clear">

				<script type="text/javascript">
					
					$(document).ready(function(){
					    $(".increment-field").hide();
					    $('input[type="Checkbox"]').click(function(){
					        if($(this).attr("value")=="Increment"){
					            $(".increment-field").toggle();
					        }
					    });
					});
				</script>
                
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<input type="submit" name="continue" value="Add" class="btn green" />
                    <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>

			<?php //echo form_close(); ?>
            </form>
	</div>
		<table cellspacing="0">
			<thead class="table-head" style="background:#A3AAA3;">

			<td>Base Salary</td>
			<td>Pay Frequency</td>
			<td>Currency</td>
			<td>Payrate</td>
			<td>Created By</td>
			<td>Created Date</td>
			<td>Approved Date</td>
			<td>Approved By</td>
			<td>Status</td>

			</thead>
			<?php if(!empty($record)){
				foreach($record as $row){ ?>
					<tr class="table-row">
						<td><?php echo $row->base_salary;?></td>
						<td><?php echo $row->pay_frequency;?></td>
						<td><?php echo $row->currency_name;?></td>
						<td><?php echo $row->payrate;?></td>
						<td><?php echo $row->creater;?></td>
						<td><?php echo date_format_helper($row->date_created);?></td>
						<td><?php echo date_format_helper($row->date_approved);?></td>
						<td><?php echo $row->apprved;?></td>
						<td><?php echo $row->status_title;?></td>
					</tr>
				<?php } } ?>
		</table>
		</div>

		</div>

	</div>
<!-- contents -->
<script>
	function validation()
	{

		if(document.form1.ml_pay_grade.value=="0")
		{alert("Please select Pay Grade !");
		return false;
		
		}
		if(document.form1.ml_pay_frequency.value=="0")
		{alert("Please select Pay Freequency !");
		return false;
		
		}
		if(document.form1.ml_currency.value=="0")
		{alert("Please select Currency !");
		return false;
		
		}
		if(document.form1.ml_pay_rate_id.value=="0")
		{alert("Please select Pay Rate !");
		return false;
		
		}
		
	}

</script>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<!-- designaiton dialog -->
	<div id="currency" title="Add Currency" style="display:none; width:600px;">
	<form id="currForm" action="human_resource/add_curr22/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="currency_name" id="txt_curr"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script>
	$(document).ready(function()
	{
		$("#currency").on('click',function(e){
			e.preventDefault();
			var formData = $('#currForm').serialize();
			$.ajax({
				url: "human_resource/add_curr22/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var provinceNameEnterdValue = $('#txt_curr').val();
						var appendData = '<option value="'+data[3]+'">'+provinceNameEnterdValue+'</option>';
						$('#drop_curr').append(appendData);
						$("#currency").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
</script>