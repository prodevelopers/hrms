<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Employment Info</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">
		<div class="head">Employment Information</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
					<h4>Current Job Designation</h4>
					<?php echo @form_dropdown('curr_job_title', $job_title,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Job Specification</h4>
					<input type="text" name="job_specification" required="required">
				</div>

				<br class="clear">
				<div class="row">
					<h4>Employment Type</h4>
					<?php echo @form_dropdown('employment_type', $employment_type,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Job Category</h4>
					<?php echo @form_dropdown('job_category', $job_category,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Pay Grade</h4>
					<?php echo @form_dropdown('pay_grade', $paygrade,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Department</h4>
					<?php echo @form_dropdown('department', $department,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>Branch</h4>
					<?php echo @form_dropdown('branch', $branch,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>City</h4>
					<?php echo @form_dropdown('city', $city,'required="required"','required="required"'); ?>
				</div>


				<div class="row">
					<h4>Joining Date</h4>
					<input type="text" name="joining_date" id="joining_date" required="required">
				</div>

				<div class="row">
					<h4>Location</h4>
					<?php echo @form_dropdown('location', $location,'required="required"','required="required"'); ?>
				</div>

        <br class="clear">
        <div class="row">
            <h4>Project Name</h4>
            <?php echo @form_multiselect('project_id[]',$approval,$approval_types,'required="required"','required="required"');?>
        </div>




        <div class="row">
					<h4>Work Shift</h4>
					<?php echo @form_dropdown('work_shift', $work_shift,'required="required"','required="required"'); ?>
				</div>

				<br class="clear">
					<div class="head">Employment Contract</div>
                    
				<br class="clear">
				<div class="row">
					<h4>Contract Start Date</h4>
					<input type="text" name="contract_start_date" id="contract_start_date" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Expiry Date</h4>
					<input type="text" name="contract_expiry_date" id="contract_expiry_date" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert</h4>
					<select name="contract_exp_alert">
						<option value="1">Yes</option>
						<option value="0">No</option>
						</select>
					<!--<input type="text" name="contract_exp_alert" required="required">-->
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert Date</h4>
					<input type="text" name="date_contract_expiry_alert" id="date_contract_expiry_alert" required="required">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comments"></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>		

		</div>


		
	</div>
<!-- contents -->

<!--Menu Setting----------------------------------------------->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });

    $( "#joining_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });
    $( "#contract_start_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });
    $( "#contract_expiry_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });
    $( "#date_contract_expiry_alert" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });
</script>
<!-- leftside menu end -->