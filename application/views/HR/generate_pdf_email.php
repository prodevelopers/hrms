<?php //echo $query;?>

<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}</script>
<div id="pdf_f">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">

<style>
    @media print {
        #printButton {
            display: none;
        }
        a:link:after, a:visited:after {
            content: "" !important;
            display: none !important;
            background: black;
        }
    }
body{
	background: #f8f8f8;
}
table th{
	font-size: 13px;
	font-weight:bold;
}
table td{
	font-size: 13px;
}
.report_msg{
    padding: 2em 1em;
    font-size: 18px;
    font-weight: bold;
    color: darkred;
    width:30%;
    margin:5em 12em;
    border: 1px solid #000066;
}
</style>
<?php
if(isset($EmployeeData) && !empty($EmployeeData)){
$EmployeeDataArray = json_decode(json_encode($EmployeeData),true);

?>
<div class="row" style="width:99%; height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-12">
			<div class="col-lg-11">
				<h4 class="text-center">
                    <?php if(isset($reportHeading) && !empty($reportHeading)){
                        echo $reportHeading;
                    }else{
                        echo "List Of Employees";
                    }?>
                </h4>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
				  <tr>
                      <?php
                       foreach($EmployeeDataArray[0] as $key=>$value){
                           echo "<th>".str_replace('_', ' ', $key)."</th>";
                       }
                      ?>
				  </tr>
				</thead>
				<tbody>
                      <?php
                      foreach($EmployeeDataArray as $key=>$value){
                          echo "<tr>";
                          foreach($value as $subKey=>$subValue){
                            echo "<td>".$subValue."</td>";
                          }
                          echo "</tr>";
                      }
                      ?>
				</tbody>
			</table>
	</div>
</div>
<?php
}else{
    echo "<div class=\"report_msg\"> No Records Available For Employees</div>";
}
?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<!--- Script for PDF file ---->
<script>
    $(document).ready(function(){
$("#pdfButton").on('click', function(){
var targetURL = '<?php echo base_url();?>human_resource/downloadPdfHrReport';
<?php if(isset($view) && !empty($view)){ ?>
    var postData = [];
    postData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
    console.log(postData);
    $.ajax({
    url: targetURL,
    data:postData,
    type:"POST",
    success: function (output) {
    var data = output.split("::");
    if(data[0] === "Download"){
    //console.log(data[1]);
    //document.location = <?php //echo base_url();?>data[1];
    }
    }
    });
<?php } ?>
});});
</script>

<!--- Script for CSV file ---->
<script>
    $(document).ready(function(){
        $("#sendMailWithAttachment").on('click', function(){
            var targetURL = '<?php echo base_url();?>human_resource/emailPdfHrReport';
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            //console.log(formData);
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });});
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>