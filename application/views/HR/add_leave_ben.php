<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- Bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Leave</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />

				<div class="row">
					<h4>Leave type</h4>
					<?php echo @form_dropdown('ml_leave_type_id', $leave_type,'required="required"','required="required"'); ?>
				</div>

				<div class="row">
					<h4>No Of Days</h4>
					<input type="text" name="no_of_leaves">
				</div>

				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_leave" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>


			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Leave Type</td>
					<td>No of Days Allocated</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($leave_details)){
					foreach($leave_details as $leave){?>
				<tr class="table-row">
					<td><?php echo $leave->leave_type;?></td>
					<td><?php echo $leave->no_of_leaves_allocated;?></td>
					<td><a href="human_resource/edit_single_leave_type/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_leave/<?php echo $employee->employee_id;?>/<?php echo $leave->leave_entitlement_id;?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>
	</div>

<!--Menus Slider-->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div> <!--End Of Menus-->
<!-- contents -->
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>