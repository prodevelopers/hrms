<?php //echo $query;?>

<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
</script>
<div id="pdf_f">
    <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
   <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
   <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
     <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>

    <style>
        thead  th {
            font-size:15px;
        }
        .infor th{
            font-size: 14px;
        }
        tbody td{
            font-size:14px;
        }
        .detail td{
            font-size: 13px;
        }
    </style>
<style>
    @media print {
        #printButton {
            display: none;
        }
        a:link:after, a:visited:after {
            content: "" !important;
            display: none !important;
            background: black;
        }
    }
body{
	background: #f8f8f8;
}
table th{
	font-size: 13px;
	font-weight:bold;
}
table td{
	font-size: 13px;
}
.report_msg{
    padding: 2em 1em;
    font-size: 18px;
    font-weight: bold;
    color: darkred;
    width:30%;
    margin:5em 12em;
    border: 1px solid #000066;
}
</style>
<?php
if(isset($EmployeeData) && !empty($EmployeeData)){
$EmployeeDataArray = json_decode(json_encode($EmployeeData),true);

?>
<div class="row" style="width:99%; height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-12">
			<div class="col-lg-11">
				<h4 class="text-center">
                    <?php if(isset($reportHeading) && !empty($reportHeading)){
                        echo $reportHeading;
                    }else{
                        echo "List Of Employees";
                    }?>
                </h4>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
				  <tr>
                      <?php
                       foreach($EmployeeDataArray[0] as $key=>$value){
                           echo "<th>".str_replace('_', ' ', $key)."</th>";
                       }
                      ?>
				  </tr>
				</thead>
				<tbody>
                      <?php
                      foreach($EmployeeDataArray as $key=>$value){
                          echo "<tr>";
                          foreach($value as $subKey=>$subValue){
                            echo "<td>".$subValue."</td>";
                          }
                          echo "</tr>";
                      }
                      ?>
				</tbody>
			</table>
	</div>
</div>
    <button type="button" id="printButton" onClick="window.print()" class="btn btn-default btn-sm pull-right">Print</button>
    <button type="button" id="pdfButton" class="btn btn-default btn-sm pull-right">PDF</button>
    <a href=<?php echo base_url().$files?>><button type="button" class="btn btn-default btn-sm pull-right">CSV</button></a>
    <button type="button" id="emailButton" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#myModal">Email</button>
<!-- Email popup--->
    <div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Email To:</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="mailForm">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                            </div>
                        </div>
                      <!--  <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                            <div class="col-sm-9">
                                <label  class="col-sm-2 control-label">work.pdf</label>
                            </div>
                        </div>-->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </div>
 <!---End--->
<?php
}else{
    echo "<div class=\"report_msg\"> No Records Available For Employees</div>";
}
?>
</div>

<!--- Script for PDF file ---->
<script>
    $(document).ready(function(){
$("#pdfButton").on('click', function(){
var targetURL = '<?php echo base_url();?>human_resource/downloadPdfHrReport';
<?php if(isset($view) && !empty($view)){ ?>
    var postData = [];
    postData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
    console.log(postData);
    $.ajax({
    url: targetURL,
    data:postData,
    type:"POST",
    success: function (output) {
    var data = output.split("::");
    if(data[0] === "Download"){
    //console.log(data[1]);
    //document.location = <?php //echo base_url();?>data[1];
    }
    }
    });
<?php } ?>
});});
</script>

<!--- Script for CSV file ---->
<script>
    $(document).ready(function(){
        $("#sendMailWithAttachment").on('click', function(){
            var targetURL = '<?php echo base_url();?>human_resource/emailPdfHrReport';
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            //console.log(formData);
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });
    });
</script>
