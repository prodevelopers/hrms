<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Attachments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	

	<?php /*?><div class="right-contents">

		<div class="head">Attachments</div>
		<?php echo form_open_multipart();?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        	<div class="row">
					<h4>Attachment</h4>
					<input type="file" name="attached_file" value="<?php echo @$attach_single->attached_file; ?>">
                    <?php if(!empty($attach_single->attached_file)){ ?>
                    <img src="upload/<?php echo $attach_single->attached_file; ?>" width="100" height="100"/>
					<?php }else{?>
                    <img src="upload/default.png" width="100" height="100"/>
                    <?php } ?>
                </div>

				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="remarks" value="<?php echo @$attach_single->remarks ?>">
				</div>

				<br class="clear">

				
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="hidden" name="attach_id" value="<?php echo @$attach_single->attachment_id; ?>" />
               <!-- <input type="submit" name="continue" value="Update" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />-->
				</div>
			</div>
			<?php echo form_close(); ?>

		</div><?php */?>


		<div class="right-contents1">

		<div class="head">Uploaded Files</div>
		<div class="row">
		<a href="human_resource/add_emp_attachment/<?php echo $this->uri->segment(3);?>">
			<input type="button" class="btn green" value="Add"></a>	
		</div>
			
			<br class="clear">
			<table cellspacing="0">
				<tr class="table-head" style="background:#A3AAA3;">
					<td>File Name</td>
					<td>Remarks</td>
					<td>Edit</td>
					<td>Trash</td>
				</tr>
                <?php if(!empty($attach_details)){
					foreach($attach_details as $file){?>
				<tr class="table-row">
					<td><img src="upload/<?php echo $file->attached_file; ?>" width="50" height="50"  /></td>
					<td><?php echo $file->remarks; ?></td>
					<td><a href="human_resource/edit_single_attachment/<?php echo $employee->employee_id; ?>/<?php echo $file->attachment_id; ?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_attachment/<?php echo $employee->employee_id; ?>/<?php echo $file->attachment_id; ?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>

			<!-- button group 
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Submit</button></a>
					<button class="btn gray">Reset</button> 
				</div>-->
			<!-- </div>-->

		</div>

	</div>
<!-- contents -->
<script>

    $(document).ready(function()
    {
    //////////// Notification Message ////////////////

    <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
if(!empty($message) && isset($message)){
    echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0], data[1]);
    <?php
    }
    }
}
?>
    });

</script>