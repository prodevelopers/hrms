<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Attachments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Attachments</div>
		<?php echo form_open_multipart();?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
		<input type="hidden" name="uploaded_file" value="<?php echo @$attach_single->attached_file; ?>">
        	<div class="row">
					<h4>Attachment</h4>
					<input type="file" name="attached_file" value="<?php echo @$attach_single->attached_file; ?>">
                    <?php if(!empty($attach_single->attached_file)){ ?>
                    <img src="upload/<?php echo $attach_single->attached_file; ?>" width="150" height="150"/>
					<?php }else{?>
                    <img src="upload/default.png" width="100" height="100" />
                    <?php } ?>
                </div>
				<br class="clear"  />
				<div class="row">
					<h4>Remarks</h4>
					<input type="text" name="remarks" value="<?php echo @$attach_single->remarks ?>" required="required">
				</div>

				<br class="clear">

				
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="hidden" name="attach_id" value="<?php echo @$attach_single->attachment_id; ?>" />
                <input type="submit" name="continue" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>

		</div>


		<?php /*?><div class="right-contents" style="margin-top:20px;">

		<div class="head">Added Files</div>

			<table cellspacing="0">
				<tr class="table-head" style="background:#A3AAA3;">
					<td>File Name</td>
					<td>Remarks</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
                <?php if(!empty($attach_details)){
					foreach($attach_details as $file){?>
				<tr class="table-row">
					<td><?php echo $file->attached_file; ?></td>
					<td><?php echo $file->remarks; ?></td>
					<td><a href="human_resource/edit_single_attachment/<?php echo $employee->employee_id; ?>/<?php echo $file->attachment_id; ?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_attachment/<?php echo $employee->employee_id; ?>/<?php echo $file->attachment_id; ?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>

			<!-- button group 
			<div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Submit</button></a>
					<button class="btn gray">Reset</button> 
				</div>-->
			<!-- </div>-->

			

		</div><?php */?>


		

	</div>
<!-- contents -->