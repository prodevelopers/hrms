<?php include('includes/header.php'); ?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense Management / Add Expense</div> <!-- bredcrumb -->

	<?php include('includes/payroll_left_nav.php'); ?>
	

	<div class="right-contents">

		<div class="head">Salary Cheque</div>

			<div class="cheque">
			<h3 class="h3">Salary Cheque</h3>
				<ul>
					<li>Employee Name</li>
					<li>Azhar Khan</li>
					<li>Employee ID</li>
					<li>00686</li>
					<li>NIC</li>
					<li>41880-4782747-8</li>
					<li>Account Details</li>
					<li>HBL Peshawar</li>
				</ul>
				<ul>
					<li>Account No</li>
					<li>3334-4344-2343-2343</li>
					<li>Base Salary</li>
					<li>Rs 30000/-</li>
					<li>Allowances</li>
					<li>Rs 6000/-</li>
					<li>Transaction Date</li>
					<li>24-july-2014</li>
				</ul>
				
					<hr class="br">
					<span class="net"><p>Net Pay Salary:</p></span>
					<span class="total"><p>Rs 36000/-</p></span>
			</div>

		</div>

	</div>
<!-- contents -->

<?php include('includes/footer.php'); ?>
