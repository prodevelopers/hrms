<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Experience</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>

	<div class="right-contents1">
	<div class="head">Employee Experience</div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <a href="human_resource/add_experience_edt/<?php echo $this->uri->segment(3)?>"><input type="submit" name="continue" value="Add Experience" class="btn green" /></a>
				</div>
			</div>
			<?php echo form_close(); ?>
			

		</div>
		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Experience</div>
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Company</td>
					<td>Job Title</td>
					<td>From</td>
					<td>To</td>
					<td>Total Duration</td>
					<td>Edit</td>
					<td>Trash</td>

				</thead>
                <?php if(!empty($experience_detail)){
                    $sum=0;
                    $month=0;
                    $year=0;
					foreach($experience_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->organization;?></td>
					<td><?php echo $row->designation_name;?></td>
					<td><?php
                        $date=$row->from_date;
                        $date_from=date("d-m-Y",strtotime($date));
                        echo $date_from;?></td>
					<td><?php
                        $date_t=$row->to_date;
                        $date_to=date("d-m-Y",strtotime($date_t));
                        echo $date_to;?></td>
					<td><?php
                        $startDate = new DateTime($row->from_date);
                        $endDate = new DateTime($row->to_date);
                        $interval = $startDate->diff($endDate);
                        $total_expv=$interval->y ." Year(s) , ".$interval->m." Month(s)";
                        $total_exp=$interval->y.".".$interval->m;

                       // echo $interval->y ." years, ".$interval->m." months and ".$interval->d." days ";
                        echo $total_expv;?></td>
					<td><a href="human_resource/edit_emp_single_experience/<?php echo $employee->employee_id;?>/<?php echo $row->experience_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_experience/<?php echo $employee->employee_id;?>/<?php echo $row->experience_id;?>" onclick="return confirm('Are You Sure..!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php $sum +=$total_exp;
                    $month += $interval->m;
                    $year += $interval->y;} } ?>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total Experience</td>
                    <td><?php
                        $add=0;

                        @$explode=explode(".",$sum);
                        if($month)
                        {
                           //echo "<br>";
                           $mn= round($month/12,2);
                            $year=$year.".0";


                            $add=$year + $mn;
                            echo round($add,PHP_ROUND_HALF_UP);

                        }else{
                       echo "";}?>
                    </td>
                </tr>
			</table>
		</div>
	</div>
<!-- contents -->

<script>
    $(document).ready(function() {

        //////////// Notification Message ////////////////
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0], data[1]);
        <?php
        }
        }
    }
    ?>
    });

</script>