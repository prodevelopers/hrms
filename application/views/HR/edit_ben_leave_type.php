<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_left_nav'); ?>
	
	<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Leave</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />

				<div class="row">
					<h4>Leave type</h4>
					<?php 
					$slctd_leave = (isset($leave->ml_leave_type_id) ? $leave->ml_leave_type_id : '');
					echo @form_dropdown('ml_leave_type_id', $leave_type, $slctd_leave,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>

        		<br class="clear">
				<div class="row">
					<h4>No Of Days</h4>
					<input type="text" name="no_of_leaves" value="<?php echo @$leave->no_of_leaves_allocated; ?>">
				</div>

        	<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_leave" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
<!-- contents -->