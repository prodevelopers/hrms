<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Qualification</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav'); ?>
	
	<script type="text/javascript">
				 $(document).ready(function(){
				 	$(".diploma-field").hide();
				 	$(".degree-field").hide();
				 	$(".certificate-field").hide();
      			 $("select").change(function(){
            $( "select option:selected").each(function(){
                if($(this).attr("value") == "1"){
                    $(".degree-field").show();
                    $(".certificate-field").hide();
                    $(".diploma-field").hide();
                }
                if($(this).attr("value") == "2"){
                    $(".degree-field").hide();
                    $(".certificate-field").hide();
                    $(".diploma-field").show();
                }
                if($(this).attr("value") == "3"){
                    $(".degree-field").hide();
                    $(".certificate-field").show();
                    $(".diploma-field").hide();
                }
            });
        }).change();
    });
			</script>

	<div class="right-contents1">
		<div class="head">Add Qualification</div>
		<div class="row">
		<h4>Qualification</h4>
		</div>

        <?php
        $formAttributes = array('onSubmit' => 'return check();',
            'id' => 'qualificationAddForm');
        $formPOSTURL = base_url('human_resource/add_emp_qualification').'/'.$this->uri->segment(3);
        echo form_open($formPOSTURL,$formAttributes); ?>

        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
					<div class="row">
                    <?php echo @form_dropdown('qualification_type_id', $qualification ,'','required="required" id="qualificationType"'); ?>
				    </div>
				<br class="clear">
				<div class="row degree-field">
					<h4>Degree</h4>
					<input type="text" name="qualification1" id="qualification1">
				</div>
				<div class="row degree-field">
					<h4>Institute</h4>
					<input type="text" name="institute1" id="institute1">
				</div>
				<br class="clear">
				<div class="row degree-field">
					<h4>Specialization</h4>
					<input type="text" name="major_specialization" id="major_specialization">
				</div>

				<div class="row degree-field">
					<h4>Year of Completion</h4>
					<input type="text" name="year1" id="year1">
				</div>
				<br class="clear">

				<div class="row degree-field">
					<h4>GPA / DIVISION </h4>
					<input type="text" name="gpa_score" id="gpa_score">
				</div>

				<div class="row diploma-field">
					<h4>Diploma</h4>
					<input type="text" name="qualification2" id="qualification2">
				</div>

				<div class="row diploma-field">
					<h4>Institute</h4>
					<input type="text" name="institute2" id="institute2">
				</div>
				<br class="clear">

				<div class="row diploma-field">
					<h4>Duration</h4>
					<input type="text" name="duration" id="duration">
				</div>

				<div class="row diploma-field">
					<h4>Year</h4>
					<input type="text" name="year2"  id="year2">
				</div>

				<div class="row certificate-field">
					<h4>Certificate</h4>
					<input type="text" name="qualification3" id="qualification3">
				</div>

				<div class="row certificate-field">
					<h4>Institute</h4>
					<input type="text" name="institute3" id="institute3">
				</div>

				<br class="clear">
				<div class="row certificate-field">
					<h4>Year</h4>
					<input type="text" name="year3"  id="year3">
				</div>
                
			<!-- button group -->
            <br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>

		</div>


		<div class="right-contents1" style="margin-top:20px;">
			<div class="head">Education & Qualifications</div>

			<table cellspacing="0" >
				<thead class="table-head secondry">
					<td>Qualification</td>
					<td>Type</td>
					<td>Institute</td>
					<td>Year</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($qualification_detail)){
					foreach($qualification_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->qualification; ?></td>
					<td><?php echo $row->qualification_title; ?></td>
					<td><?php echo $row->institute; ?></td>
					<td><?php echo date_format_helper($row->year); ?></td>
					<td><a href="human_resource/send_2_trash_qualificationadd/<?php echo $employee->employee_id; ?>/<?php echo $row->qualification_id; ?>/<?php echo'qualification_id';?>" onclick="return confirm('Are you Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>				
			</table>
		</div>
	</div>
<!-- contents -->
<script>
    $(document).ready(function()
    {
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
if(!empty($message) && isset($message)){
    echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0],data[1]);
    <?php
    }
    }
}
?>
    });
    function check()
    {
        var qualificationTypeSelector = $('#qualificationType');
        var qualificationType = qualificationTypeSelector.val();
        if(qualificationType > 0){
            //Get All The Form Fields First.

            var degree = $('#qualification1').val();
            var degreeInstitute = $('#institute1').val();
            var major_specialization = $('#major_specialization').val();
            var degreeYear = $('#year1').val();
            var score = $('#gpa_score').val();

            var diploma = $('#qualification2').val();
            var diplomaInstitute = $('#institute2').val();
            var duration = $('#duration').val();
            var diplomaYear = $('#year2').val();

            var certificate = $('#qualification3').val();
            var certificateInstitute = $('#institute3').val();
            var certificateYear = $('#year3').val();


            if(parseInt(qualificationType) === 1){
                //Means If Degree Is Selected
                if(degree.length === 0){
                    Parexons.notification('Please Fill The Degree Field','error');
                    return false;
                }else if(degreeInstitute.length === 0){
                    Parexons.notification('Please Provide Institute Name','error');
                    return false;
                }else if(degreeYear.length === 0){
                    Parexons.notification('Please Provide Year Information','error');
                    return false;
                }else if(score.length === 0){
                    Parexons.notification('Please Provide GPA Information','error');
                    return false;
                }
            }else if(parseInt(qualificationType) === 2){
                //Means If Diploma DropDown Is Selected
                if(diploma.length === 0){
                    Parexons.notification('Please Provide GPA Information','error');
                    return false;
                }
                if(diplomaInstitute.length === 0){
                    Parexons.notification('Please Provide Institute Name','error');
                    return false;
                }
                if(diplomaYear.length === 0){
                    Parexons.notification('Please Provide Diploma Year','error');
                    return false;
                }
            }else if(parseInt(qualificationType) === 3){
                //Means If Certificate DropDown Is Selected
                if(certificate.length === 0){
                    Parexons.notification('Please Provide Certificate Information','error');
                    return false;
                }
                if(certificateInstitute.length === 0){
                    Parexons.notification('Please Provide Institute Name','error');
                    return false;
                }
                if(certificateYear.length === 0){
                    Parexons.notification('Please Provide Year','error');
                    return false;
                }
            }
        }else{
            Parexons.notification('Some Problem In Posting Form, Please Contact System Administrator For Further Assistance.','error');
        }
    }

    $( "#year1,#year2,#year3" ).datepicker({
        dateFormat: "dd-mm-yy",
        timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange:"<?php echo yearRang(); ?>"

    });


</script>