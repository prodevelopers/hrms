<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<?php
//var_dump($employee_id); ?>
<style type="text/css">
    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .mytab tr:nth-child(odd) td {
        background-color: #ECECFF;
    }

    .mytab tr:nth-child(even) td {
        background-color: #F0F0E1;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers
    {
        margin-top: 8px;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;
        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 0px;
    }
    .select2TemplateImg { padding:0.2em 0; clear: both;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .select2-chosen div.select2TemplateImg img{ width: 24px; height: 24px;}
    p{
        margin:0 !important;
    }
    textarea { resize: none; }
    .select2-result-selectable{clear:both;}

    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }
    h4 {
        font-size: 22px;
        font-weight: 400;
        line-height: 22px;
    }
    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
    height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        height:35px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
div.text form{
    display: inline-block;
}

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
</style>

<!-- contents -->

<div class="contents-container">
    <div class="bredcrumb">Dashboard / Human Resource / Disciplinary Action </div> <!-- Bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Disciplinary Action
        <div class="btn-right">
            <a class="btn green below right" id="addReportDisciplineBreachBtn">Report Discipline Breach</a>
        </div>
        </div>

        <!-- filter -->
        <div class="filter">
            <h4>Filter By</h4>
            <?php echo form_open();?>
            <input type="text" id="tableSearchBox" placeholder="Search Employees" style="width: 30%;">
            <input type="hidden" name="DisciplinaryAction" id="selectDisciplinaryAction">
            <?php echo form_close();?>
        </div>

        <!-- table -->
        <table cellpadding="0" cellspacing="0" id="reportedBreachesTable" class="marge ">
            <div class="table-width">
                <thead class="table-head">
                <td>Report ID</td>
                <td>Accused Employee</td>
                <td>Breach of discipline</td>
                <td>Date of occurrence</td>
                <td>Reported By</td>
                <td>Investigated By</td>
                <td>Disciplinary Action</td>
                <td>Status</td>
                <td width="140"><span class="fa fa-pencil" title="Edit Record"></span> &nbsp; &nbsp;|&nbsp; &nbsp;
                    <span class="fa fa-eye" title="View Record"></span>
                </td>
                </thead>
            </div>
            <tbody>
            </tbody>
        </table>

        <!-- button group -->

    </div>
</div>
<!-- contents -->
</div>

        </div>

<!----------------------------Add Report Discipline Breach----------------------------->
<div id="disciplinaryAddActionReportModal" class="modal-demo" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Report Discipline Breach</h3>
    </div>
    <form class="form-horizontal" id="addNewBreachReport">
        <div class="pop_row">
            <div class="left_side">
                <h5>Employee</h5>
            </div>
            <div class="right_side">
                <input type="hidden" name="addEmployee" id="addSelectEmployee" class="right_txt" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Breach of discipline</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="addDisciplineBreached" id="addDisciplineBreached" placeholder="Discipline Breached" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Description</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="addDisciplineBreachedDesc" id="addDisciplineBreachedDesc" placeholder="Description of Violated/Breached Discipline"></textarea>
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Date of occurrence</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" id="addDateOfOccurrence"  name="addDateOfOccurrence" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="btn-row">
            <input type="button" class="btn-pop" id="addBreachReport" value="Submit Report">
        </div>
    </form>

</div>
<!-------------------------Edit Model Finish------------------------->


<!----------------------------Edit Model----------------------------->
    <div id="disciplinaryActionReportEditModal" class="modal-demo" style="display: none;">
        <div class="top_header">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only"></span>
    </button>
    <h3 class="title">Investigation Report</h3>
        </div>
        <form class="form-horizontal" id="reportForm">
        <div class="pop_row">
            <div class="left_side">
                <h5>Employee</h5>
            </div>
            <div class="right_side">
                <input type="hidden" name="employee" id="selectEmployee" class="right_txt" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Breach of discipline</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="disciplineBreached" id="disciplineBreached" placeholder="Discipline Breached" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Description</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="disciplineBreachedDesc" id="disciplineBreachedDesc" placeholder="Description of Violated/Breached Discipline"></textarea>
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Date of Occurrence</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" id="dateOfOccurrence"  name="dateOfOccurrence" style="width:240px;height:35px;">
            </div>
        </div>

            <div class="btn-row">
                <input type="submit" class="btn-pop" value="Submit">
            </div>
            </form>
    </div>
<!-------------------------Edit Model Finish------------------------->

<!----------------------View Model------------------------------------>
<div id="disciplinaryActionReportViewDetails" class="modal-demo view_model" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Report Details</h3>
    </div>
    <form class="form-horizontal" id="reportForm">
        <div class="container_view" style="float:left;width:990px;height:auto;">
        <div class="left_photo">
            <img id="userAvatar" src="assets/images/user.png">
        </div>

        <div class="right_sec">
            <div class="left_block">
            <div class="left_lbl">
                <h5>Employee Code</h5>
            </div>
            <div class="right_lbl">
                <h5 id="employeeCode"> - </h5>
            </div>

                <div class="left_lbl">
                    <h5>Accused Employee</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="employeeName"> - </h5>
                </div>

                <div class="left_lbl">
                    <h5>Breach of discipline</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="BreachedDiscipline"> - </h5>
                </div>

                <div class="left_lbl">
                    <h5>Date of occurrence</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="DateOfOccurrence"> - </h5>
                </div>

            </div>

            <div class="left_block" >

                <div class="left_lbl">
                    <h5>Reported By</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="ReportedBy"> - </h5>
                </div>

                <div class="left_lbl">
                    <h5>Investigated By</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="InvestigatedBy"> - </h5>
                </div>

                <div class="left_lbl">
                    <h5>Disciplinary Action</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="DisciplinaryAction"> - </h5>
                </div>

                <div class="left_lbl">
                    <h5>Status</h5>
                </div>
                <div class="right_lbl">
                    <h5 id="ReportStatus"> - </h5>
                </div>
            </div>
            <style>
                .left_block_coments{
                    width:100%;
                    height: auto;
                    float:left;
                }
                .right_lbl_coments{
                    width:470px;
                    height: auto;
                    float:left;
                }
                .left_lbl_coments{
                    width: 24%;
                    float: left;
                    line-height: 35px;
                    padding-left: 5px;
                }
                .coments_row{
                    float: left;
                    width: 100%;
                    height: auto;
                    margin-bottom: 10px;
                }
            </style>
            <!---for Dialogoe---->
            <style>
            .tip {
            width: 0px;
            height: 0px;
            position: absolute;
            background: transparent;
            border: 10px solid #ccc;
            }

            .tip-up {
            top: -25px; /* Same as body margin top + bordere */
            left: 10px;
            border-right-color: transparent;
            border-left-color: transparent;
            border-top-color: transparent;
            }

            .tip-down {
            bottom: -25px;
            left: 10px;
            border-right-color: transparent;
            border-left-color: transparent;
            border-bottom-color: transparent;
            }

            .tip-left {
            top: 10px;
            left: -25px;
            border-top-color: transparent;
            border-left-color: transparent;
            border-bottom-color: transparent;
            }

            .tip-right {
            top: 10px;
            right: -25px;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            }

            .dialogbox .body {
            position: relative;
            max-width: 100%;
            height: auto;
           /* margin: 20px 10px;*/
            padding: 5px;
            background-color: #DADADA;
            border-radius: 3px;
            border: 5px solid #ccc;
            }

            .body .message {
            min-height: 30px;
            border-radius: 3px;
            font-family: Arial;
            font-size: 14px;
            line-height: 1.5;
            color: #797979;
            }

            </style>

            <div class="left_block_coments">
                <div class="coments_row">
                <div class="left_lbl_coments">
                    <h5>Employee Feedback</h5>
                </div>
                <div class="right_lbl_coments">
                    <div class="dialogbox">
                        <div class="body">
                            <span class="tip tip-left"></span>
                            <div class="message">
                                <span id="employeeFeedBack"></span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="coments_row">
                    <div class="left_lbl_coments">
                        <h5>Investigator Remarks</h5>
                    </div>
                    <div class="right_lbl_coments">
                        <div class="dialogbox">
                            <div class="body">
                                <span class="tip tip-left"></span>
                                <div class="message">
                                    <span id="investigatorRemarksSpan"> - </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="coments_row">
                    <div class="left_lbl_coments">
                        <h5>Reviewer Remarks</h5>
                    </div>
                    <div class="right_lbl_coments">
                        <div class="dialogbox">
                            <div class="body">
                                <span class="tip tip-left"></span>
                                <div class="message">
                                    <span id="reviewerRemarks"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
            <style>
                .btn_rights{float:right;width:auto;height: 35px;background-color: #6AAD6A;margin-right: 35px;
                    color:white;}
                .btn_rights:hover{cursor:pointer;}
            </style>

          <div class="btn_row">
                <input type="button" class="btn_rights" id="WithindisciplinaryActionReportAddModal" value="Update Investigation Report">
          </div>

    </form>
</div>
</div>
<!------------------------End View Model------------------------------>

<!---------------------------Model Within Another Page---------------------------->
<div id="WithindisciplinaryActionReportAddModal" class="modal-demo" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Investigation Report</h3>
    </div>
    <form class="form-horizontal" id="InvestigationReportForm">
        <input type="hidden" name="IReportID" id="IReportID" style="display: none;">
        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Employee Remarks</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="employeeRemarks" id="employeeRemarks" placeholder="Employee Remarks If Any"></textarea>
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Investigator Remarks</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="investigatorRemarks" id="investigatorRemarks" placeholder="Investigator Remarks"></textarea>
            </div>
        </div>
        <div class="btn_row">
            <input type="button" class="btn_rights" id="updateInvestigationReport" value="Update">
        </div>

    </form>

</div>

<!--------------------------------End of Model with Another Page------------------------->
<!--End of Investigate User Modal-->
<style>
    .modal-footer{
        /*float: right;*/
        width: auto;
        height: auto;
        margin-right: 20px;
        margin-top: 5px;
        margin-bottom: 40px;
        display: inline-block;
    }
</style>

<!-----------------------------------------end models----------------------------->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>
<!-- left side menu End -->

<!---------------Model Javascript--------------------------->
<script type="text/javascript">
    $(function(){
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".close" });
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".btn-default" });
        $('.disciplinaryActionReportEdit').leanModal({ top: 110, overlay: 0.45, closeButton: ".close" });
    });
</script>
<!--------------------------End Model--------------------------->

<!--------------------------Data Tables--------------------------->
<script type="text/javascript">
    var oTable;
$(document).ready(function(e){

    ///////////////////////////Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    /*
     * New Select Employee
     * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
     * */
    var selectEmployee = $('#selectEmployee');
    var url = "<?php echo base_url(); ?>human_resource/loadAvailableEmployees";
    var tDataValues = {
        id: "EmployeeID",
        text: "EmployeeName",
        avatar:'EmployeeAvatar',
        employeeCode:'EmployeeCode'
    };
    var minInputLength = 0;
    var placeholder = "Select Employee";
    var baseURL = "<?php echo base_url(); ?>";
    var templateLayoutFunc = function format(e) {
        if (!e.id) return e.text;
        return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
    };
    var templateLayout = templateLayoutFunc.toString();
    commonSelect2Templating(selectEmployee,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);

    //Add Report Employee Selector
    var addReportEmployeeSelector = $('#addSelectEmployee');
    var addUrl = "<?php echo base_url(); ?>human_resource/loadAvailableEmployees";
    var addTDataValues = {
        id: "EmployeeID",
        text: "EmployeeName",
        avatar:'EmployeeAvatar',
        employeeCode:'EmployeeCode'
    };
    var addMinInputLength = 0;
    var addPlaceholder = "Select Employee";
    var addBaseURL = "<?php echo base_url(); ?>";
    var addTemplateLayoutFunc = function format(e) {
        if (!e.id) return e.text;
        return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
    };
    var addTemplateLayout = addTemplateLayoutFunc.toString();
    commonSelect2Templating(addReportEmployeeSelector,addUrl,addTDataValues,addMinInputLength,addPlaceholder,addBaseURL,addTemplateLayout);


    //Filter Selectors
    //Select Disciplinary Action
    var DisciplinaryActionSelector = $('#selectDisciplinaryAction');
    var url = "<?php echo base_url(); ?>human_resource/loadAvailableDisciplinaryActions";
    var id = "DisciplinaryActionID";
    var text = "DisciplinaryAction";
    var minInputLength = 0;
    var placeholder = "Select Tasks";
    var multiple = false;
    commonSelect2(DisciplinaryActionSelector,url,id,text,minInputLength,placeholder,multiple);
    DisciplinaryActionSelector.on('change',function(e){
//        console.log('Hello World');
        oTable.fnDestroy();
        var filteredDisciplinaryAction = $(this).val();
        var filters = 'aoData.push({"name":"filteredDisciplinaryAction","value":'+$(this).val()+'});';
        commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });

    ///////////////////////End Of Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\

    $('#addBreachReport').on('click',function(e){
        e.preventDefault();
        //Need To Post Form Using Ajax When Button Clicked.
        var formData = $('#addNewBreachReport').serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/report_discipline_breach",
            type: "POST",
            data:formData,
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    //Clear Form Fields
                    $('#addNewBreachReport')[0].reset();
                    //Reset Cant Work With The Select 2, So Need To Reset The Select2 Using Below Script/Command
                    $('#addSelectEmployee').select2('data', null);

                    //Need To Reload The DataTables To Show The Updated Record
                    //First Destroy Old Table
                    oTable.fnDestroy();
                    //Recreate New Table
                    commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);

                    //Close The Popup Finally.
                    Custombox.close('disciplinaryAddActionReportModal');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });
    $('#updateInvestigationReport').on('click',function(e) {
        e.preventDefault();
        //Need To Update The Investigation Report.
        var investigationReportFormSelector = $('#InvestigationReportForm');
        var formData = investigationReportFormSelector.serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/update_investigation_report",
            type: "POST",
            data: formData,
            success: function (output) {
                var data = output.split('::');
                if(data[0] === 'OK'){
                    Parexons.notification(data[1],data[2]);
                    oTable.fnClearTable();
                    oTable.fnDraw();
                    Custombox.close();
                }else if(data[0] === 'FAIL'){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    $("#disciplinaryActionReportViewDetails").on('click',function(){
        var data=1;
        $.ajax({

            url:"<?php echo base_url(); ?>human_resource/disciplinary_action/list",
            data:data,
            type:"POST",
            success:function(result){

            }
        });
    });
    //End Of DataTables


//Need To Display the list of All The Reported/Violated Disciplines
    oTable ='';
    var tableSelector = $('#reportedBreachesTable');
    var url_DT = "<?php echo base_url(); ?>human_resource/disciplinary_action/list";
    var aoColumns_DT = [
        /* ID */ {
            "mData": "ReportID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
        /* Name of The Employee Who Broke/Violated Company Discipline/Rule */ {
            "mData" : "AccusedEmployee"
        },
        /* Discipline That Have Been Breached/Violated By Employee */ {
            "mData" : "BreachedDiscipline"
        },
        /* Breach Date/Date of Occurrence */ {
            "mData" : "DateOfBreach"
        },
        /* Reported By (Employee Name) */ {
            "mData" : "ReportedBy"
        },
        /* Disciplinary Action */ {
            "mData" : "InvestigatedBy"
        },
        /* Disciplinary Action */ {
            "mData" : "DisciplinaryAction"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "ReportStatus"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "ViewEditActionButtons"
        }
    ];
    var HiddenColumnID_DT = 'ReportID';
    var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
    //End Of DataTables






    ///
    $(document).on('click','.disciplinaryActionReportEdit', function(e){
        var reportID = $(this).closest('tr').attr('data-id');
        //Getting Form Selectors For Specific Modal To Assign Values
        var modalSelector = $('#disciplinaryActionReportEditModal');
        var selectedEmployee = modalSelector.find('#selectEmployee');
        var breachedDiscipline = modalSelector.find('#disciplineBreached');
        var breachedDisciplineDesc = modalSelector.find('#disciplineBreachedDesc');
        var dateOfOccurrence = modalSelector.find('#dateOfOccurrence');
        //Get Values For Edit Modal
        var postData = {
            reportID: reportID
        };
        $.ajax({
            url:"<?php echo base_url(); ?>human_resource/get_report_details/edit",
            type: "POST",
            data: postData,
            success:function(output){
                try
                {
                    //If Result is In JSON then Show The Edit Fields In Modal.
                    var json = JSON.parse(output);
                    breachedDiscipline.val(json.BreachedDiscipline);
                    breachedDisciplineDesc.val(json.BreachedDisciplineDescription);
                    dateOfOccurrence.val(json.BreachedDate);
                    selectedEmployee.select2('data', {id:json.AccusedEmployeeID, text:json.AccusedEmployeeName, avatar:json.EmployeeAvatar});
                }
                catch(e)
                {
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }
        });

        Custombox.open({
            target: '#disciplinaryActionReportEditModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });

    // for Details View of Report.
    ///
    $(document).on('click','.disciplinaryActionReportViewDetails', function(e){
        var reportID = $(this).closest('tr').attr('data-id');
        //Get Values For View Modal
        var postData = {
            reportID: reportID
        };
        $.ajax({
            url:"<?php echo base_url(); ?>human_resource/get_report_details/view",
            type: "POST",
            data: postData,
            success:function(output){
                try
                {
                    //If Result is In JSON then Show The Edit Fields In Modal.
                    var jsonViewData = JSON.parse(output);
                    $('#userAvatar').attr('src',jsonViewData.EmployeeAvatar);
                    $('#employeeName').text(jsonViewData.AccusedEmployeeName);
                    $('#employeeCode').text(jsonViewData.AccusedEmployeeCode);
                    $('#BreachedDiscipline').text(jsonViewData.BreachedDiscipline);
                    $('#DateOfOccurrence').text(jsonViewData.BreachedDate);
                    $('#InvestigatedBy').text(jsonViewData.InvestigatedByEmployeeName);
                    $('#ReportedBy').text(jsonViewData.ReportedByEmployeeName);
                    $('#DisciplinaryAction').text(jsonViewData.DisciplinaryActionType);
                    $('#ReportStatus').html(jsonViewData.ReportStatus);
                    $('#employeeFeedBack').text(jsonViewData.EmployeeFeedBack);
                    $('#investigatorRemarksSpan').text(jsonViewData.InvestigatorRemarks);
                    $('#reviewerRemarks').text(jsonViewData.ReviewerRemarks);

                    //If Has Authority Of Investigation Then Assign ReportID
                    $('#IReportID').val(reportID);
                }
                catch(e)
                {
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }
        });

        Custombox.open({
            target: '#disciplinaryActionReportViewDetails',
            effect: 'fadein'
        });
        e.preventDefault();
    });
    //End

    //////////////////////////////////////////// for Add Report
    ///
    $('#addReportDisciplineBreachBtn').on('click', function(e){
        Custombox.open({
            target: '#disciplinaryAddActionReportModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });

    //////////////////////////////////////////// With Disceiplinary Action Page
    ///
    $('#WithindisciplinaryActionReportAddModal').on('click', function(e){
        Custombox.open({
            target: '#WithindisciplinaryActionReportAddModal',
            //It is different effect.. ie fadein,sign etc.
            effect: 'sign'
        });
        e.preventDefault();
    });

    //DatePickers
    $('#dateOfOccurrence, #addDateOfOccurrence').datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });

    //Defining Width For All Selectors Of the Filters
    $('.select2-container').css("width","223px");
    //Little CSS for rows
    $('tbody').addClass("table-row");
    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
});
</script>

