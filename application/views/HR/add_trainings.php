<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Skills & Trainings</div> <!-- bredcrumb -->

	<?php
    $pageMessages = array();
    $pageMessages['msg'] = $this->session->flashdata('msg');
    $this->load->view('includes/edit_employee_left_nav'); ?>
	
    <div class="right-contents1">
			<div class="head">Add Trainings</div>
			<?php echo form_open(); ?>
        	<input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<br class="clear">
				<div class="row">
					<h4>Training Name</h4>
					<input type="text" name="training_name">
				</div>

				<div class="row">
					<h4>Institute</h4>
					<input type="text" name="Institute">
				</div>

				<div class="row">
					<h4>From Date</h4>
					<input type="text" name="from_date" id="fromDate">
				</div>
				<div class="row">
					<h4>To Date</h4>
					<input type="text" name="to_date" id="toDate">
				</div>

				<div class="row">
					<h4>Total Days</h4>
					<input type="text" id="totalDays" name="total_days" readonly="readonly">
				</div>

				<br class="clear">
				<div class="row">
					<h4>Description</h4>
					<textarea name="description"></textarea>
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_training" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>
			
			<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Trainings</td>
					<td>Institute</td>
					<td>From Date</td>
					<td>To Date</td>
					<td>Description</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($training_detail)){
					foreach($training_detail as $train){ ?>
				<tr class="table-row">
					<td><?php echo $train->training_name;?></td>
					<td><?php echo $train->Institute;?></td>
					<td><?php echo date_format_helper($train->frm_date); ?></td>
					<td><?php echo date_format_helper($train->to_date); ?></td>
					<td><?php echo $train->description;?></td>
					<td><a href="human_resource/edit_single_training/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_training/<?php echo $employee->employee_id; ?>/<?php echo $train->training_record_id; ?>" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>

	</div>
<!-- contents -->
<script>
    $(document).ready(function(e){

        var fromDateSelector = $('#fromDate');
        var toDateSelector = $('#toDate');
        fromDateSelector.datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
                toDateSelector.datepicker( "option", "minDate", selectedDate );
            }
        }).change(function(e){
            var toDate = toDateSelector.val();
            var fromDate = $(this).val();
            if(toDate.length > 0 && fromDate.length > 0){
                var splittedToDate = toDate.split('-');
                var splittedFromDate = fromDate.split('-');
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(splittedFromDate[2],splittedFromDate[1],splittedFromDate[0]);
                var secondDate = new Date(splittedToDate[2],splittedToDate[1],splittedToDate[0]);
                var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
                $('#totalDays').val(diffDays + 1);
            }
        });

        toDateSelector.datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
                fromDateSelector.datepicker( "option", "maxDate", selectedDate );
            }
        }).change(function(e){
            var toDate = $(this).val();
            var fromDate = fromDateSelector.val();
            if(toDate.length > 0 && fromDate.length > 0){
                var splittedToDate = toDate.split('-');
                var splittedFromDate = fromDate.split('-');
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(splittedFromDate[2],splittedFromDate[1],splittedFromDate[0]);
                var secondDate = new Date(splittedToDate[2],splittedToDate[1],splittedToDate[0]);
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
                $('#totalDays').val(diffDays + 1);
            }
        });



        ///Load The Messages Of The Page, If Exist Any In The Session.
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>