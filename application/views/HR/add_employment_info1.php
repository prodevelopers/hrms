<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Add Employee / Personal Info</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/employee_left_nav.php'); ?>
	

	<div class="right-contents">

		<div class="head">Employment Information</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
				<div class="row">
					<h4>Current Job Title</h4>
					<?php echo @form_dropdown('curr_job_title', $job_title); ?>
				</div>

				<div class="row">
					<h4>Job Specification</h4>
					<input type="text" name="job_specification">
				</div>

				<div class="row">
					<h4>Employment Type</h4>
					<?php echo @form_dropdown('employment_type', $employment_type); ?>
				</div>

				<div class="row">
					<h4>Job Category</h4>
					<?php echo @form_dropdown('job_category', $job_category); ?>
				</div>

				<div class="row">
					<h4>Department</h4>
					<?php echo @form_dropdown('department', $department); ?>
				</div>

				<div class="row">
					<h4>Branch</h4>
					<?php echo @form_dropdown('branch', $branch); ?>
				</div>

				<div class="row">
					<h4>City</h4>
					<?php echo @form_dropdown('city', $city); ?>
				</div>


				<div class="row">
					<h4>Joining Date</h4>
					<input type="text" name="joining_date" id="joining_date">
				</div>

				<div class="row">
					<h4>Location</h4>
					<?php echo @form_dropdown('location', $location); ?>
				</div>
				

				<div class="row">
					<h4>Work Shift</h4>
					<?php echo @form_dropdown('work_shift', $work_shift); ?>
				</div>

				<br class="clear">
				<div class="row">
					<h4><b>Employment Contract</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Expiry Date</h4>
					<input type="text" name="contract_expiry_date">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert</h4>
					<input type="text" name="contract_exp_alert">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Contract Alert Date</h4>
					<input type="text" name="date_contract_expiry_alert" id="contract_exp_date_id">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Comments</h4>
					<textarea name="comments"></textarea>
				</div>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
            <?php echo form_close(); ?>		

		</div>


		
	</div>
<!-- contents -->