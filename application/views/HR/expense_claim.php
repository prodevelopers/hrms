<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.dataTables_info {
visibility: hidden;
color: #3474D0;
font-size: 14px;
margin: 6px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"status_title","value":$('#status_title').val()});
			aoData.push({"name":"designation_name","value":$('#Designations').val()});
					        
                
		}
                
	});
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard /Approvals / Expense Claim Approvals</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/aproval_menu'); ?>
<script>
 var navigation = responsiveNav(".nav-collapse");
</script>
	<div class="right-contents">

		<div class="head">Expense Claim Approvals</div>

			<div class="filter">
				<h4>Filter By</h4>
				<?php echo form_open('');?>
        <?php echo form_dropdown('status_title',$status,$status_title,,'required="required"','required="required"'"class='select_47 filter1' id='status_title' onchange='this.form.submit()'");?>
        
        <?php echo form_close();?>	
			</div>

			  <table cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Expense Amount</td>
					<td> Month</td>
					<td>Year</td>
					<td>Status</td>
					<td>Action</td>
				</thead>
				<tbody>
                </tbody>
			</table>  	
	   </div>


	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
</script>
