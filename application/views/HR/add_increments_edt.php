
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Entitlements</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Increments</div>
		<div id="txt" style="background-position: center">
            <?php //echo  $this->session->flashdata('txt');?></div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
        	<br class="clear">
            <div class="row increment-field">
					<h4>Increment Type</h4>
					<?php echo @form_dropdown('increment_type_id', $increment_type,'','required="required"'); ?>
                  <!--<select>
                      <option></option>
                  </select>-->
				</div>

				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text" name="increment_amount">
				</div>
				<br class="clear">
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="training_date">
				</div>

			<!-- button group -->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_increment" value="Add" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
		<?php echo form_close(); ?>


				<table cellspacing="0">
				<thead class="table-head" style="background:#A3AAA3;">
					<td>Increment Type</td>
					<td>Increment</td>
					<td>Edit</td>
					<td>Trash</td>
				</thead>
                <?php if(!empty($increment_details)){
					foreach($increment_details as $increment){?>
				<tr class="table-row">
					<td><?php echo $increment->increment_type;?></td>
					<td><?php echo $increment->increment_amount;?></td>
					<td><a href="human_resource/edit_emp_single_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_increment/<?php echo $employee->employee_id;?>/<?php echo $increment->increment_id;?>" onclick="return confirm('Are You Sure...!')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
			</table>
		</div>
		
	</div>
<!-- contents -->
<script>
	$("#txt").delay(3000).fadeOut('slow');

    //////////// Notification Message ////////////////
    $(document).ready(function() {
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
    echo "var message;";
    foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0], data[1]);
        <?php
        }
        }
    }

    ?>
    });
    //*********** Datepicker ****************//
    $( "#training_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });



</script>