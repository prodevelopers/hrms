<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Allowance</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Allowances</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo $empl->employee_id; ?>" />
        
				<div class="row">
					<h4>Allowance type</h4>
					<?php
					$slctd_allowance = (isset($allowance->ml_allowance_type_id) ? $allowance->ml_allowance_type_id : '');
					echo @form_dropdown('ml_allowance_type_id', $allowance_type, $slctd_allowance,'required="required"','required="required"'); ?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
        		<br class="clear">
				<div class="row">
					<h4>Amount</h4>
					<input type="text" name="allowance_amount" value="<?php echo @$allowance->allowance_amount; ?>">
				</div>
        		<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_allowance" value="Update" class="btn green" />
                <input type="reset" value="Reset" class="btn gray" />
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>		
	</div>
<!-- contents -->
<script>
    $(document).ready(function() {
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
   echo "var message;";
   foreach($pageMessages as $key=>$message){
       if(!empty($message) && isset($message)){
               echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });

</script>