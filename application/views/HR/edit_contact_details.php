 <script type="application/javascript">
     $(document).ready(function(){
         <?php $msg=$this->session->flashdata('cmsg');
         if(!empty($msg)){?>
         $("#cmsg").show().delay(4000).fadeOut();
         <?php }?>
         <?php $msg=$this->session->flashdata('pmsg');
         if(!empty($msg)){?>
         $("#pmsg").show().delay(4000).fadeOut();
         <?php }?>
         <?php $msg=$this->session->flashdata('emsg');
         if(!empty($msg)){?>
         $("#emsg").show().delay(4000).fadeOut();
         <?php }?>
     });
	function validateForm() {
		var x = document.forms["myForm"]["email"].value;
		var atpos = x.indexOf("@");
		var dotpos = x.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			alert("Not a valid Email address");
			return false;
		}
	}
</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Contact Details</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	

	<div class="right-contents1">

		<div class="head">Current Address Details</div>
        <div id="cmsg" style="display: none; background-color: #008000; color: #ffffff; text-align: center;"><?php echo $this->session->flashdata('cmsg');?></div>
        <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo isset($employee)?$employee->employee_id:'';?>" />
				<div class="row">
					<h4>Address<i>*</i></h4>
					<input type="text" name="address" value="<?php echo isset($curr_add)?$curr_add->address:''; ?>">
				</div>

				<div class="row">
					<h4>City/Village</h4>
                    <?php 
					$selctd_city = (isset($curr_add->city_village) ? $curr_add->city_village : '');
					echo @form_dropdown('city_village', $city, $selctd_city,'class="drop_city"'); ?>
						<a id="city"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>
				<div class="row">
					<h4>Province</h4>
                    <?php 
					$selctd_province = (isset($curr_add->province) ? $curr_add->province : '');
					echo @form_dropdown('province', $province, $selctd_province,'class="drop_province"','required="required'); ?>
					<a id="pro"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="home_phone" value="<?php echo @$curr_add->home_phone; ?>" placeholder="<?php echo @$curr_add->home_phone; ?>">
				</div>

				 <br class="clear"> 
				<div class="row">
					<h4>Mobile</h4>
					<input type="text" name="mob_num" value="<?php echo @$curr_add->mob_num; ?>">
				</div>

				<div class="row">
					<h4>Private Email</h4>
					<input type="email" name="email_address" value="<?php echo @$curr_add->email_address; ?>" onchange="return validateForm()" onmouseover="document.getElementById('toolTipDiv').className='activeToolTip'" onmouseout="document.getElementById('toolTipDiv').className='idleToolTip'">
				</div>
				<br class="clear">
				<div class="row">
					<h4><b>Work Contacts</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Work Telephone</h4>
					<input type="text" name="office_phone" value="<?php echo @$curr_add->office_phone; ?>" >
				</div>

				<div class="row">
					<h4> Work Email</h4>
					<input type="email" name="official_email" value="<?php echo @$curr_add->official_email; ?>" onchange="return validateForm()" onmouseover="document.getElementById('toolTipDiv').className='activeToolTip'" onmouseout="document.getElementById('toolTipDiv').className='idleToolTip'">
				</div>

				<h6>* Required Fields</h6>		

			<!-- button group -->
			<br class="clear">
			<div class="row">
                <input type="submit" name="add_current" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
			</div>
        <?php echo form_close();?>

		</div>
<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Permanent Address Details</div>
    <div id="pmsg" style="display: none; background-color: #008000; color: #ffffff; text-align: center;"><?php echo $this->session->flashdata('pmsg');?></div>

    <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Address<i>*</i></h4>
					<input type="text" name="address" value="<?php echo @$perm_add->address;?> ">
				</div>
				<div class="row">
					<h4>City/Village</h4>
                    <?php 
					$selctd_perm_city = (isset($perm_add->city_village) ? $perm_add->city_village : '');
					echo @form_dropdown('city_village', $city, $selctd_perm_city,'class="drop_city"'); ?>
						<a id="city1"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
					<!--<select name="city_village">
						<option>Select City/Village</option>
					</select>-->
				</div>
				<div class="row">
					<h4>District (Domicile)</h4>
                    <?php					
					$selctd_perm_dist = (isset($perm_add->district) ? $perm_add->district : ''); 
					echo @form_dropdown('district', $district, $selctd_perm_dist,'id="drop_distt"'); ?>
					<a id="dist"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
					<!--<select name="district">
						<option>Select District</option>
					</select>-->
				</div>
				<div class="row">
					<h4>Province</h4>
                    <?php
					$selctd_perm_prov = (isset($perm_add->province) ? $perm_add->province : '');  
					echo @form_dropdown('province', $province, $selctd_perm_prov,'class="drop_province"'); ?>
						<a id="pro1"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
					<!--<select name="province">
						<option>Select Province</option>
					</select>-->
				</div>
				<!-- <br class="clear"> -->

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="phone" value="<?php echo @$perm_add->phone; ?>">
				</div>

				<h6>* Required Fields</h6>	

			<!-- button group -->
			<br class="clear">
				<div class="row">
                <input type="submit" name="add_permanant" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
        <?php echo form_close();?>

		</div>
		<div class="right-contents1" style="margin-top:20px;">

		<div class="head">Emergency Contacts</div>
            <div id="emsg" style="display: none; background-color: #008000; color: #ffffff; text-align: center;"><?php echo $this->session->flashdata('emsg');?></div>

            <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Name</h4>
					<input type="text" name="cotact_person_name" value="<?php echo @$emerg_add->cotact_person_name; ?>">
				</div>

				<div class="row">
					<h4>Relationship</h4>
                    <?php 
					$selct_relation = (isset($emerg_add->relationship) ? $emerg_add->relationship : '');
					echo @form_dropdown('relationship', $relation, $selct_relation,'id="drop_rel" required = "required"'); ?>
					<a id="rel"><span class="fa fa-pencil" style="font-size: 10px;"></span></a>
				</div>

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="home_phone" value="<?php echo @$emerg_add->home_phone; ?>">
				</div>

				<div class="row">
					<h4>Mobile</h4>
					<input type="text" name="mobile_num" value="<?php echo @$emerg_add->mobile_num; ?>">
				</div>

				 <br class="clear"> 

				<div class="row">
					<h4>Work Telephone</h4>
					<input type="text" name="work_phone" value="<?php echo @$emerg_add->work_phone; ?>">
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
                <input type="submit" name="add_emergency" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
			</div>
        <?php echo form_close();?>

			<?php /*?><table cellspacing="0" >
				<thead class="table-head secondry">
					<td>Name</td>
					<td>Relationship</td>
					<td>Home telephone</td>
					<td>Mobile</td>
					<td>Work telephone</td>
                    <td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($emrg_detail)){
					foreach($emrg_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->cotact_person_name; ?></td>
					<td><?php echo $row->relation_name; ?></td>
					<td><?php echo $row->home_phone; ?></td>
					<td><?php echo $row->mobile_num; ?></td>
					<td><?php echo $row->work_phone; ?></td>
                    <td><a href="human_resource/edit_emp_single_curr_contact/<?php echo $employee->employee_id?>/<?php echo $row->emergency_contact_id?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_contct/<?php echo $employee->employee_id?>/<?php echo $row->emergency_contact_id?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
				
			</table><?php */?>
		</div>

	</div>
<!-- contents -->
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Dialog Sections -->
	<!-- city dialog -->
	<div id="city-pop" title="Add City" style="display:none; width:600px;">
	<form id="cityForm" action="human_resource/add_city12/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="city_name" id="cityNameTextField"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- Province dialog -->
	<div id="province" title="Add Province" style="display:none; width:600px;">
	<form id="provinceForm" action="human_resource/add_province/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="province_name" id="txt_province"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- District dialog -->
	<div id="district" title="Add District" style="display:none; width:600px;">
	<form id="disttForm" action="human_resource/add_distt/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="district_name" id="txt_distt"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
	<!-- Relationship dialog -->
	<div id="relation" title="Add Relationship" style="display:none; width:600px;">
	<form id="relForm" action="human_resource/add_relation/<?php echo @$employee->employee_id;?>" method="post">
		<div class="data">
			<input type="text" class="text_field" name="relation_name" id="txt_rel"/>
			<br><br>
			<input type="submit" value="Add" class="btn green addedto" name="add">
		</div>
	</form>
	</div>
<script>
	$(document).ready(function()
	{
		$("#relation").on('click',function(e){
			e.preventDefault();
			var formData = $('#relForm').serialize();
			$.ajax({
				url: "human_resource/add_relation/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_rel').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_rel').append(appendData);
						$("#relation").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});


	$(document).ready(function()
	{
		$("#district").on('click',function(e){
			e.preventDefault();
			var formData = $('#disttForm').serialize();
			$.ajax({
				url: "human_resource/add_distt/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_distt').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('#drop_distt').append(appendData);
						$("#district").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});

	$(document).ready(function()
	{
		$("#province").on('click',function(e){
			e.preventDefault();
			var formData = $('#provinceForm').serialize();
			$.ajax({
				url: "human_resource/add_province/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#txt_province').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('.drop_province').append(appendData);
						$("#province").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});


		});

	});
	$(document).ready(function()
	{
		$("#city-pop").on('click',function(e){
			e.preventDefault();
			var formData = $('#cityForm').serialize();
			$.ajax({
				url: "human_resource/add_city2/<?php echo @$employee->employee_id;?>",
				data:formData,
				type:"POST",
				success: function (output) {
					//Output Here If Success.
					var data = output.split('::');
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						var cityNameEnteredValue = $('#cityNameTextField').val();
						var appendData = '<option value="'+data[3]+'">'+cityNameEnteredValue+'</option>';
						$('.drop_city').append(appendData);
						$("#city-pop").dialog("close");
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});
	});
</script>

