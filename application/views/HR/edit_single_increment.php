<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Increments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/edit_employee_left_nav'); ?>
	
	<div class="right-contents1">

		<div class="head">Increments</div>
		<?php echo form_open(); ?>
        <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id; ?>" />
        
            <div class="row increment-field">
					<h4>Increment Type</h4>
					<?php 
					$slctd_increment = (isset($increment->increment_type_id) ? $increment->increment_type_id : '');
					echo @form_dropdown('increment_type_id', $increment_type, $slctd_increment,'required="required"','required="required"'); ?>
                  <!--<select>
                      <option></option>
                  </select>-->
				</div>

        		<br class="clear">
				<div class="row increment-field">
					<h4>Increment</h4>
					<input type="text" name="increment_amount" value="<?php echo @$increment->increment_amount; ?>">
				</div>
        		<br class="clear">
				<div class="row increment-field">
					<h4>Date (Effective From)</h4>
					<input type="text" name="date_effective" id="joining_date" value="<?php echo @$increment->date_effective; ?>">
				</div>

			<!-- button group -->
        	<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_increment" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
		<?php echo form_close(); ?>
		</div>

	</div>
<!-- contents -->
<script>
    $(document).ready(function() {
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
   echo "var message;";
   foreach($pageMessages as $key=>$message){
       if(!empty($message) && isset($message)){
               echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });

</script>