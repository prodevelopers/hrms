<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login HRMS</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <style>
        .top-header{
            height:30px;
            background:#4D6684;
        }
        .head{
            color:#4D6684;
            font-size: 28px !important;
            font-weight:bold;
        }
        h3{
            color:#4D6684;
            font-size:22px;
            border-bottom:thin solid #ccc;
            padding:5px 0;
        }
        .p{ font-size:12px;}
        .text{ -webkit-box-shadow:0px 0 1px rgba(0,0,0,0.3);
            -moz-box-shadow:0px 0 1px rgba(0,0,0,0.3);
            -ms-box-shadow:0px 0 1px rgba(0,0,0,0.3);
            -o-box-shadow:0px 0 1px rgba(0,0,0,0.3);
            box-shadow:0px 0 1px rgba(0,0,0,0.3);
            padding:1em 2em;
        }
        .a{ padding:0.5em;}
        .a img{border:thin solid #ccc;}
        .a img,.a p{ float:left;}
        .a p{ padding:1em;}
        .footer{ border-bottom:22px solid #666; height:120px; background:#eeeef0;margin-top:2.2em;}
        .radius{
            border-radius:0;
        }
    </style>
</head>
<body>
<div class="container-fluid top-header"></div>
<!-- End of top header -->
<div class="container">
    <div class="col-md-12">
        <div class="page-header">
            <h1 class="head">Human Resource <br><small>Management System</small></h1>
        </div>
    </div>
    <div class="col-md-4 text">
        <div class="col-sm-12" id="hide" class="text-danger">
            <p><?php echo validation_errors('<p class="error">');echo $msg; ?></p>
        </div>
        <h3>System Login</h3>
        <p class="text-success p">Please Enter Your Credentials To Access The System</p>
        <?php echo form_open(base_url('login/index'));?>
        <div class="form-group">
            <label for="exampleInputEmail1">Username:</label>
            <input type="text" class="form-control radius" name="user_name" placeholder="Enter Username">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password:</label>
            <input type="password" class="form-control radius" name="password" placeholder="Enter Password">
        </div>
        <p><small><a href="login/forget_password">Forgot Username/Password?</a></small></p>
        <p><input type="submit" class="btn btn-default pull-right"  name="Login" value="Login"></p>
        <?php echo form_close();?>
        <!-- form ends -->
    </div>
    <div class="col-md-6 col-md-offset-1">
        <p class="text-center"><img src="<?php echo base_url()?>assets/img/new_logo2.jpg" alt="" height="30%" width="30%"></p>
        <p class="text-center text-success"><b>(Your <strong style="color: #810E12">Company Name</strong> and <strong style="color: #810E12">Slogan</strong> Here)</b></p>
        <br>
        <p class="text-center text-success">
            "A Performance Driven Human Resource Management Solution"
            <br><small>By Parexons IT Solutions & Services</small>
        </p>
    </div>
</div>
<div class="container-fluid footer">
    <div class="container">
        <div class="col-md-12 a">
            <img src="<?php echo base_url()?>assets/img/pxn.png" alt="" height="80" width="80">
            <p><b>Parexons</b><br><small>IT Solutions & Services</small><br><small>business@parexons.com</small></p>
        </div>
    </div>
</div>
</body>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script>
    $(document).ready(function(){
        $("#hide,.error").delay(2000).slideUp().hide(2000);
    });
</script>
</html>