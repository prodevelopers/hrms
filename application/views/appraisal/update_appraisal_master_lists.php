<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource /Master List/ Appraisal Type</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_master_list_nav'); ?>

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
			<form action="hr_site/UpdateAppraisal" method="post">

				<div class="row">
					<h4><b>Appraisal</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Update Appraisal</h4>
					<input type="text" name="AppraisalTitle" value="<?php echo $Appraisal->AppraisalTitle;?>" required>
					<input type="hidden" name="AppraisalHiddenId" value="<?php echo $Appraisal->ID;?>">
				</div>

				<!-- button group -->
				<div class="row">
					<input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
				</div>

			</form>


			<!-- table -->



		
		</div>



	</div>

</div>
<!-- contents -->

<script>
	$("#alert").delay(3000).fadeOut('slow');


	$(document).ready(function(e){

		//Load Page Messages
		<?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
		var data = message.split("::");
		Parexons.notification(data[0],data[1]);
		<?php
        }
        }
    }
    ?>
	})

</script>
