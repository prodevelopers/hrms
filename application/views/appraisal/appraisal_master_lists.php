
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource /Master List/ Appraisal Type</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_master_list_nav'); ?>

	<div class="right-contents" style="width:70%">

		<div class="head">Appraisal Terms</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
			<form action="hr_site/appraisal" method="post">

				<div class="row">
					<h4><b>Appraisal</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Appraisal</h4>
					<input type="text" name="AppraisalTitle" value="" required>
				</div>

				<!-- button group -->
				<div class="row">
					<input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
				</div>

			</form>


			<!-- table -->


			<table cellspacing="0">
				<thead class="table-head">

				<td>Appraisal Term</td>
<!--				<td>Status</td>-->
				<td><span class="fa fa-pencil"></span></td>
<!--				<td><span class="fa fa-trash-o"></span></td>-->
				</thead>
				<?php if (!empty($Appraisal)){?>
					<?php foreach($Appraisal as $key=>$AppraisalTitle):?>
							<?php if($key==0){?>
							<tr class="table-row">

								<td><?php echo $AppraisalTitle->AppraisalTitle; ?></td>
								<?php /*if($year->enabled == 1): */?><!--
								<td><?php /*echo "Enabled"; */?></td>
							<?php /*else : */?>
								<td><?php /*echo "Disabled"; */?></td>
							--><?php /*endif;*/?>
								<td><span class="fa fa-pencil"></span></td>
<!--								<td><span class="fa fa-trash-o"></span></td>-->
							</tr>
							<?php } else{?>
						<tr class="table-row">

							<td><?php echo $AppraisalTitle->AppraisalTitle; ?></td>
							<?php /*if($year->enabled == 1): */?><!--
								<td><?php /*echo "Enabled"; */?></td>
							<?php /*else : */?>
								<td><?php /*echo "Disabled"; */?></td>
							--><?php /*endif;*/?>
							<td><a href="hr_site/UpdateAppraisal/<?php echo $AppraisalTitle->AppraisalId;?>"><span class="fa fa-pencil"></span></a></td>
<!--							<td><a href="hr_site/DeleteAppraisal/--><?php //echo $AppraisalTitle->AppraisalId;?><!--" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>-->
						</tr>
							<?php }?>
					<?php endforeach; ?>
				<?php } else { echo "<div class=\"result\">No record found..</div>";} ?>

			</table>

		</div>



	</div>

</div>
<!-- contents -->

<script>
	$("#alert").delay(3000).fadeOut('slow');

	<?php if(!empty($this->session->flashdata('msg'))){?>
	var message = "<?php echo $this->session->flashdata('msg')?>";
	setInterval(function(){
		var data = message.split("::");
		Parexons.notification(data[0],data[1]);
	});

	<?php }?>

	$(document).ready(function(e){


	});

</script>
