<style type="text/css">


#Select All checkbox{
	float:right;
}
.dataTables_length{
	position: relative;
	top: -40px;
	font-size: 1px;
	left: 1%;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -41px;
left: -11.9em;
}
.dataTables_filter input{
	width: 180px;
}
.table_style
{
	width:890px;
	margin-left:0;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="application/javascript" src="assets/js/data-tables/dataTables.tableTools.js"></script>
<script type="text/javascript">
/*$.fn.dataTable.TableTools.buttons.copy_to_div = $.extend(
    true,
    $.fn.dataTable.TableTools.buttonBase,
    {
        "sNewLine":    "<br>",
        "sButtonText": "Copy to element",
        "target":      "",
        "fnClick": function( button, conf ) {
            $(conf.target).html( this.fnGetTableData(conf) );
        }
    }
);*/
$(document).ready(function() {
	$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
} );


/*$(".filter").change(function(e) {
    oTable.fnDraw();
});*/

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
	pr.onload=pr.print() ;
	pr.focus();
	}
	
function validate() 
{
if( document.form1.month.value == "-1")
   {
     alert( "Please select Month ! " );
     return false;
   }


 if(document.form1.year.value == "-1")
   {
     alert( "Please select Year ! " );
     return false;
   }
}
///////////
    function do_this(){

        var checkboxes = document.getElementsByName('check[]');
        var button = document.getElementById('toggle');

        if(button.value == 'select'){
            for (var i in checkboxes){
                checkboxes[i].checked = 'FALSE';
            }
            button.value = 'deselect All checkbox'
        }else{
            for (var i in checkboxes){
                checkboxes[i].checked = '';
            }
            button.value = 'Select All checkbox';
        }
    }
///////////
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Pay Salary Time</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		

		<div class="head">Salary <?php echo @$month_sal.",".@$year_sal?></div>

                    <form action="payroll/pay_salary" method="post" name="form1" id="form1">

				<br class="clear">
			<div class="table-responsive">
                <div style="display: none; color: red;" id="msg"> Please Select at least one Record !</div>
				<table cellspacing="0" id="examples" style="margin-top:1em;">
				<thead class="table-head">
                    <td><input type="checkbox" name="select" id="checkAll" title="Check/Uncheck All Employee"/></td>
					<td>Code</td>
					<td>Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<!--<td>Paygrade</td>
                    <td>&nbsp;Date</td>-->
					<td>Base salary + Increments</td>
                    <td title="Add with Base Salary">+ Allowance</td>
                    <td title="Add with Base Salary">+ Expense</td>
                    <td title="Subtract From Base Salary">- Advances</td>
                    <td title="Subtract From Base Salary">- Deductions</td>
                    <td>Payable</td>
                <?php  
				 ?>
				<span><?php if($info==1){ echo "<span style='color:green'>Please select Salary Month and Year !</span>";}elseif(!empty($info)){
				?></span>
				</thead>
                <?php 	 
				foreach($info as $salary_pay){
				?>
                <tr class="table-row">
              <td><input type="checkbox" name="check[]" 
              value="<?php echo $salary_pay->employee_id; ?>" />
              <input type="hidden" name="sal_month" value="<?php echo @$sal_m;?>" />
              <input type="hidden" name="sal_year" value="<?php echo @$sal_y;?>" />
              
              <input type="hidden" name="salary_month" value="<?php echo @$month_sal;?>" />
              <input type="hidden" name="salary_year" value="<?php echo @$year_sal;?>" />
              </td>
					<td><?php echo $salary_pay->employee_code; ?></td>
					<td><?php echo $salary_pay->full_name?></td>
					<!--<td><?php //echo $salary_pay->pay_grade?></td>-->
					<!--<td>
					<?php //echo $salary_pay->month.",".$salary_pay->year?>&nbsp;
                    </td>-->
                    <?php $Increment =  ((!empty($salary_pay->Increment) && is_numeric($salary_pay->Increment))?$salary_pay->Increment:0);?>
                    <td><?php echo Active_currency($salary_pay->base_salary + $Increment);?></td>

					 <?php $alwnc_amount=$salary_pay->allowance_amnt;?>
                    <td><?php echo Active_currency($alwnc_amount);?></td>

					<?php $expense_amount=$salary_pay->exp_amount;
					?>
                    <td><?php echo Active_currency($expense_amount);?></td>
                     <?php

					$loan_advance=$salary_pay->loan_amount;
					
					 ?>
					<td><?php  echo Active_currency($loan_advance);?></td>
                     <?php

				 $deduction_amount=$salary_pay->ded_amount;?>
                    <td><?php echo Active_currency($deduction_amount);?></td>
					<?php
					$base_salary=$salary_pay->base_salary;
					
			//+$expense_amount+$alwnc_amount  -$loan_advance-$deduction_amount;
$payable=$base_salary+$Increment+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					 ?>
					<td><?php echo Active_currency($payable);?></td>
                </tr><?php }}else{echo "Sorry No record Found!";}?>
			</table>
			</div>
            <div class="row">
				<div class="button-group">
					<input type="button" name="submitz" value="Proceed" class="btn green" onclick="check_box();"/>
				</div>
            </div>
            </form>
            </div>
		</div>

	</div>
<!-- contents -->
<script>
    function check_box()
    {
        var checked=$("[name='check[]']:checked").length;
        if(checked === 0)
        { $("#msg").show("fast");
            $("#msg").fadeOut(4000);}
        else{
            $("#form1").submit();
        }
    }
</script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftSide menu end -->