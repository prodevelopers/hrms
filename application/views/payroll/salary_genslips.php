<style type="text/css">

.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.move{
	position: relative;
	left: -12em;
	top: 1em;
}
.
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.table_style
{
	width:auto;
}

</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});


});

/*$("#smonth").change(function(e) {
    oTable.fnDraw();
});*/

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
	pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
    </script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Salary Payments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>

	<!--<script>
      var navigation = responsiveNav(".nav-collapse");
    </script>-->
	<div class="right-contents">

		<div class="head">Salary Payment</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <form action="payroll/salary_gen_slip" method="post">
                <div class="row">
				<input type="text" name="emp_name" placeholder="Employee Name">
				<?php echo @form_dropdown('month',$months)?>
                </div>
                <div class="row">
           	    <?php echo @form_dropdown('year',$years)?>

				<input type="submit" value="Search" class="btn green">
				</form>
				<!--<button class="btn gray">Reset</button>-->
				</div>
                <span class="fa fa-print move" style="cursor:pointer; left: 0; right: 0;" title="Print All" onclick="print_all()"></span>


			<!-- table -->
            <form action="payroll/slips" method="post" id="form1">
           	<input type="button" value="Generate Pay Slips" class="btn green" onclick="check_box();">
           	<br class="clear"/>
            <div class="table-responsive">
				<div style="display: none; color: red;" id="msg"> No Record Selected !</div>
            <div id="print_table">
            	<table cellspacing="0" class="table">
				<thead class="table-head">
                    <td><input type="checkbox" name="select" id="checkAll" /></td>
					<td>Code&nbsp;&nbsp;&nbsp;</td>
					<td>Name&nbsp;&nbsp</td>
					<td>Paygrade</td>
                    <td>Month/Year&nbsp;&nbsp;&nbsp;</td>
                   	<td>Base salary</td>
                    <!--<td>Allowance</td>
                    <td>Expense</td>
                    <td>loan/advances</td>
                    <td>Deductions</td>
                    <td>Payable</td>-->
                    <td>Paid</td>
                    <!--<td>Remains</td>-->


		<!--<td align="center"><a href="payroll/pay_salary"><img src="assets/img/icon-black.png" width="20" height="20"></a></td>-->
					<!--<td align="center"><img src="assets/img/icon-black.png" width="20" height="20">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-print">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-eye"></span></span></td>-->
					<!--<td align="center"><span class="fa fa-eye"></span></td>-->

					 <span><?php if(!empty($info)){?></span>
				</thead>
                <?php foreach($info as $salary_rec){ 
				$salary_payment_id=$salary_rec->salary_payment_id;
				?>
				<tr class="table-row">
				  <td>
				  <input type="checkbox" name="salemp[]" id="salemp" value="<?php echo $salary_rec->salary_payment_id;//echo $salary_rec->employee_id;//?>" />
				  <input type="hidden" name="month" value="<?php echo $salary_rec->ml_month_id?>" />
				  <input type="hidden" name="year" value="<?php echo $salary_rec->ml_year_id?>" />
                  <input type="hidden" name="trans_date[]" value="<?php echo $salary_rec->transaction_date?>" />

				  </td>
					<td><?php echo $salary_rec->employee_code;?></td>
					<td><?php echo $salary_rec->full_name;?></td>
					<td><?php echo $salary_rec->pay_grade;?></td>
					<td><?php echo $salary_rec->month." / ". $salary_rec->year;?></td>
                    <td><?php echo $salary_rec->base_salary;?></td>
                    <?php /*?> <?php
				   	$alwnc_month=$salary_rec->alwnc_month;
					$alwnc_year=$salary_rec->alwnc_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$allowance_freq=$salary_rec->pay_frequency;
					if($allowance_freq = 1)
					{$alwnc_amount=$salary_rec->allowance_amnt;}
				elseif($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$salary_rec->allowance_amnt;}else{$alwnc_amount=0;}
					 ?>
                    <td><?php echo $alwnc_amount;?></td>
                  <?php
				    $exp_id=$salary_rec->expense_claim_id;
					$exp_dis_id=$salary_rec->expense_id;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$exp_month=$salary_rec->exp_month;
					$exp_year=$salary_rec->exp_year;
					if($exp_id == $exp_dis_id)
					{$expense_amount=0;}
					elseif($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$salary_rec->exp_amount;}else{$expense_amount=0;}
					//elseif($exp_id != $exp_dis_id && $salary_month == $exp_month && $salary_year == $exp_year)
					//{$expense_amount=$salary_rec->exp_amount;}
					//if($expense_amount==0){ $exp_amt="paid";}else{$exp_amt=$salary_rec->exp_amount;;}
					 ?>
                    <td><?php echo $expense_amount;?></td>
                     <?php
					$loan_advance_id=$salary_rec->loan_advance_id;
					$loan_payback_id=$salary_rec->loan_payback_id;
				   	$lonad_month=$salary_rec->alonad_month;
					$lonad_year=$salary_rec->alonad_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					else
					if($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$salary_rec->loan_amount;}else{$loan_advance=0;}
					 ?>
					<td><?php  echo $loan_advance;?></td>
                     <?php
				   	$ded_month=$salary_rec->ded_month;
					$ded_year=$salary_rec->ded_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$pay_requency=$salary_rec->deduction_frequency;
					if($pay_requency = 1)
					{$deduction_amount=$salary_rec->ded_amount;}
					elseif($salary_month == $ded_month && $salary_year == $ded_year)
				    {$deduction_amount=$salary_rec->ded_amount;}
				    else{$deduction_amount=0;}
					 ?>
                    <td><?php echo $deduction_amount;?></td><?php */?>
					<?php /*?><?php
					$base_salary=$salary_rec->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?>
					<td><?php echo $payable;?></td><?php */?>
					<td><?php echo $salary_rec->transaction_amount;?></td>
                    <?php /*?><?php
					$paid_amount=$salary_rec->transaction_amount;
					$remain_amount=$payable-$paid_amount;
					?><?php */?>
                    <!--<td><?php //echo $remain_amount;?></td>-->
<!--                    <td>--><?php ////if($salary_rec->status_id==2){echo "Approved";}else{}?><!--</td>-->

				</tr>
                <?php }?>
				<?php } else {echo "<span style='color:red'> Sorry no Records Found !</span>";} ?>

			</table>

            </div>
        </div>
            </form>

        <br />
             <br />
        <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>

</div>
</div>


<!-- contents -->
<script type="text/javascript">

	function check_box()
	{
var checked=$("[name='salemp[]']:checked").length;
		if(checked === 0)
		{ $("#msg").show("fast");
			$("#msg").fadeOut(4000);}
		else{
		$("#form1").submit();}
	}
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}
</script>