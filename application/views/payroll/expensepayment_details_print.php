<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>

<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">

  <style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	padding-left: 7em;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight:normal;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 12px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
	
}
  </style>

<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<table cellspacing="0" width="543" class="printing">
				<tr>
					<td colspan="2" style="padding: 1em 0;" class="head">
						<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Expense Details</h3></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Name</td>
					<td width="337" class="no-apply"><i><?php echo $details->full_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Code</td>
					<td class="no-apply"><i><?php echo $details->employee_code;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Designation</td>
					<td class="no-apply"><i><?php echo $details->designation_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Pay Grade</td>
					<td class="no-apply"><i><?php echo $details->pay_grade;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Expense Type</td>
					<td class="no-apply"><i><?php echo $details->expense_type_title;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Amount</td>
					<td class="no-apply"><i><?php echo Active_currency($details->amount);?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Status</td>
					<td class="no-apply"><i><?php echo $details->status_title;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Paid</td>
					<td class="no-apply"><i>
					<?php $paid=$details->paid;
					if($paid==1)
					{echo "Yes";}else
					{echo "No";}
					?></i></td>
				</tr>
			<!--	<tr>
					<td width="200" class="apply">Advance</td>
					<td class="no-apply"><i><?php //echo $details->lon_amount;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Total</td>
					<td class="no-apply"><i><?php //echo $details->totals;?></i></td>
				</tr>-->
			</table>
		</div>

