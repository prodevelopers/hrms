<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Advance Salary /Edit Pay Advance Salary</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>

	<div class="right-contents">

		<div class="head">Update Advance Payment</div>

			

			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").show();
			   $(".account").hide();
			   $(".cash").hide();
			   
				/*$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});*/
			  
			});
			</script>
<div class="form-left">
			<form action="payroll/update_loan_advance_pay" method="post" role="form">
				<div class="row2">
					<h4>Employee Name cheque</h4>
    <input name="name" id="id_input"  class="serch" type="text"  value="<?php echo $row_cheque->full_name;?>" readonly="readonly"/>
      <input type="hidden" name="loan_advance_id" id="loan_advance_id" value="<?php echo $row_cheque->loan_advance_id;?>" />
       <input type="hidden" name="cheque_payment_id" id="cheque_payment_id" value="<?php echo $row_cheque->cheque_payment_id;?>" />
        <input type="hidden" name="payment_mode_id" id="payment_mode_id" value="<?php echo $row_cheque->payment_mode_id;?>" />
         <input type="hidden" name="transaction_id" id="transaction_id" value="<?php echo $row_cheque->transaction_id;?>" />
          <input type="hidden" name="loan_advance_payment_id" id="loan_advance_payment_id" 
          value="<?php echo $row_cheque->loan_advance_payment_id;?>" />
          
     
				
				<br class="clear">
				<div class="row2">
					<h4>Payment Type</h4>
					<?php 
					 $selected =(isset($row_cheque->payment_type_id) ? $row_cheque->payment_type_id:'');
					echo form_dropdown('payment_type',$payment_type,$selected)?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Payment Amount</h4>
					<input type="text" name="payment_amount" value="<?php echo $row_cheque->amount;?>">
				</div>	
				<br class="clear">
				<div class="row2">
					<h4>Payment mode</h4>
					<?php 
					$selected1 =(isset($row_cheque->payment_mode_type_id ) ? $row_cheque->payment_mode_type_id :'');
					echo form_dropdown('payment_mode',$payment_mode,$selected1,'disabled="disabled"','id="selected"')?>
                    <input type="hidden" name="payment_mode" value="<?php echo $selected1;?>" />
                
				
                </div>
                <br class="clear">
				<div class="row2">
					<h4>Status</h4>
					<?php 
					$selected =(isset($row_cheque->status_id) ? $row_cheque->status_id:'');
					echo form_dropdown('status',$status,$selected)?>
				</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="cheque_no" value="<?php echo $row_cheque->cheque_no;?>">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_chiq" value="<?php echo $row_cheque->bank_name;?>">
            	</div>
                <div class="row1 account" style="display:none">
                    <h4>Account No</h4>
                    <input type="text" name="account_no">
                </div>
                <div class="row1 account" style="display:none">
                    <h4>Account Title</h4>
                    <input type="text" name="account_title">
                </div>
                <div class="row1 account" style="display:none">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account" id="account">
                    <h4>Branch</h4>
                    <input type="text" name="branch">
                </div>
                <div class="row1 cash">
                    <h4>Received</h4>
                    <input type="text" name="recieved" value="">
                </div>
                <div class="row1 cash">
                    <h4>Remarks</h4>
                    <input type="text" name="remarks" value="">
                </div>
				<br class="clear">
				<div class="row2">
					<input type="submit"  class="btn green" value="Update Pay Advance" />
				</div>
				
			</form>
</div>
<div class="form-right">
<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<h5 class="headingfive">Employee Name</h5>
                    	<i class="italica" id="employee_name"></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Code</h5>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">CNIC</h5>
                    	<i class="italica" id="cnic"></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Department</h5>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">position</h5>
                    	<i class="italica" id="position"></i>
				</div>
				
			</div>
    </div>
	
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="#"><button class="btn green"></button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>
	</div>
<!-- contents -->