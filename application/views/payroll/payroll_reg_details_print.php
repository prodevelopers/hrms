<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
	thead  th {
		font-size:15px;
	}
	.infor th{
		font-size: 14px;
	}
	tbody td{
		font-size:14px;
	}
	.detail td{
		font-size: 13px;
	}
</style>
<button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right"  style="float:right" data-toggle="modal" data-target="#myModal">
	Send
</button>
<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">

  <style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	padding-left: 7em;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight: bold;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 12px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
	
}
  </style>
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<table cellspacing="0" width="543" class="printing">
				<tr>
					<td colspan="2"  class="head"><h3>Payroll Registry details  <?php echo $details->month.", ".$details->year;?></h3></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Name</td>
					<td width="337" class="no-apply"><i><?php echo $details->full_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Code</td>
					<td class="no-apply"><i><?php echo $details->employee_code;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Designation</td>
					<td class="no-apply"><i><?php echo $details->designation_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Pay Grade</td>
					<td class="no-apply"><i><?php echo $details->pay_grade;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Base Salary</td>
					<td class="no-apply"><i><?php echo Active_currency($details->base_salary);?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Deductions</td>

					<td class="no-apply"><i><?php if(!empty($ded_trans->transaction_amount)){echo Active_currency($ded_trans->transaction_amount);}else{echo "0";}?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Allowance</td>

					<td class="no-apply"><i><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo Active_currency($allw_trans->transaction_amount);}else{echo "0";}?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Expense</td>

					<td class="no-apply"><i><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo Active_currency($exp_trans->transaction_amount);}else{echo "0";}?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Advances</td>

					<td class="no-apply"><i><?php 
					if(!empty($advance_trans->advance_trans_amount))
					{
					echo Active_currency($advance_trans->advance_trans_amount);}else{echo "0";}?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Transaction date</td>
					<td class="no-apply"><i><?php 
					$date=$details->transaction_date;
					$new=date("d-m-Y",strtotime($date));
					echo @$new;?></i></td>
				</tr>
				
				<tr>
					<td width="200" class="apply">Transaction Status</td>
                    <td class="no-apply"><?php echo $details->status_title;?></td>
				</tr>
                 <?php if($details->status==2){?>
                <tr>
					<td width="200" class="apply">Approved by</td>
                    <td class="no-apply"><?php echo @$details->name;?></td>
				</tr>
                 <tr>
					<td width="200" class="apply">Approved date</td>
                    <td class="no-apply"><?php 
				$date=$details->ap_date;
				if(!empty($date)){
				$new=date("d-m-Y",strtotime($date));
				echo @$new;}else{echo "";}?></td>
				</tr>
                <?php }?>
				<tr>
				  <td class="apply">Totals</td>

				  <td class="no-apply"><i><?php echo Active_currency($salary_info->transaction_amount);?></i></td>
    </tr>
    <tr>
        <td class="apply">Balance</td>

        <td class="no-apply"><i><?php echo Active_currency($salary_info->arears);?></i></td>
    </tr>
			</table>
		</div>
