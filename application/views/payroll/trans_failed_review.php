<!-- contents -->
<style>
    .beauty h4{
        font-size:13px;
        font-weight:normal;
    }
    .beauty td{
        font-size: 13px;
    }
    .beauty{
        width:23%;
        margin:1em 0.5em;
        float:left;
    }
    .head1{
        background: #4D6684;
        color: #fff;
        font-weight:500;
        font-size: 0.9em;
        line-height: 20px;
    }
    .bubble {
        position: relative;
        background-color:red;
        color:#000000;
        margin: 0;
        padding:10px;
        text-align:center;
        width:180px;
        -moz-border-radius:10px;
        -webkit-border-radius:10px;
        -webkit-box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        -moz-box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        box-shadow: 0px 0 3px rgba(0,0,0,0.25);
        left: 20%;
    }
    .bubble:after {
        position: relative;
        display: block;
        content: "";
        border-color: red transparent transparent transparent;
        border-style: solid;
        border-width: 10px;
        height:0;
        width:0;
        position:relative;
        bottom:-27px;
        left:1em;
    }
</style>
<script>
   $(document).ready(function(){
       //$("#remarks").hide();

   });
</script>
<div class="contents-container">
    <div class="bredcrumb">Payroll /Transactions /Transaction Failed Review</div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <script>
        var navigation = responsiveNav(".nav-collapse");
    </script>
    <div class="right-contents">
        <div class="head" style="font-size:1em; font-weight:bold;">Transaction Details</div>

            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head1">
                    <th colspan="2" style="text-align:left;">Employee Infomation</th>
                </tr>
                <tr class="table-row">
                    <td>Name</td>
                    <td><h4><?php echo @$details->full_name;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>Employee Code</td>
                    <td><h4><?php echo @$details->employee_code;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>Designation</td>
                    <td><h4><?php echo @$details->designation_name;?></h4></td>
                </tr>
                <tr class="table-row">
                    <td>Pay Grade</td>
                    <td><h4><?php echo @$details->pay_grade;?></h4></td>
                </tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Salary Info</th>
                </tr>
                <tr >
                    <td>Base Salary</td>
                    <td><h4><?php echo @$details->base_salary;?></h4></td>
                </tr>
                <tr>
                    <td>Deducations</td>

                    <td><h4><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Allowances</td>

                    <td><h4><?php
                            if(!empty($allw_trans->transaction_amount)){
                                echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Expenses</td>

                    <td><h4><?php
                            if(!empty($exp_trans->transaction_amount)){
                                echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Advances</td>

                    <td><h4><?php
                            if(!empty($advance_trans->transaction_amount))
                            {
                                echo @$advance_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Total Amount</td>

                    <td><h4><?php echo Active_currency($salary_info->transaction_amount);?></h4></td>
                </tr>


            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Arrears Details</th>
                </tr>
                <tr class="table-row">
                    <td>Arrears </td>
                    <td><h4>
                            <?php echo Active_currency($salary_info->arears);?> </h4></td>
                </tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8">Deductions</th>
                </tr>
                <tr class="table-row">
                    <?php
                    $ded_type=@$ded_trans->ded_rectp; $ded_amt=@$ded_trans->ded_recmnt;
                    ?>
                    <td>
                        <?php $explode_dedtyp=explode(",",$ded_type);
                        $explode_dedamt=explode(",",$ded_amt);
                        ?>

                        <?php foreach($explode_dedtyp as $ded_typ)
                        {
                            echo $ded_typ."<br />";

                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_dedamt as $ded_mt)
                            {
                                echo $ded_mt."<br />";

                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total <!--Deductions--> </td>
                    <td><h4><?php
                            $deduction_amount=@$ded_trans->total_ded_amount;
                            echo Active_currency($deduction_amount);?></h4></td></tr>
            </table>

            <br class="clear"/>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Allowances</th>
                </tr>

                <tr>
                    <?php
                    $alw_rectp=@$allw_trans->alw_rectp; $alw_recm=@$allw_trans->alw_recm;
                    ?>
                    <td>
                        <?php $explode_alw_rectp=explode(",",$alw_rectp);
                        $explode_alw_recm=explode(",",$alw_recm);
                        ?>

                        <?php foreach($explode_alw_rectp as $alw_typ)
                        {
                            echo $alw_typ."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_alw_recm as $alw_mt)
                            {
                                echo $alw_mt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total</td>
                    <td><h4><?php
                            $allowance_amount=@$allw_trans->total_alw_recm;
                            echo Active_currency($allowance_amount);?></h4></td></tr>

            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Expenses</th>
                </tr>

                <tr>
                    <?php
                    $exp_rectp=@$exp_trans->exp_rectp; $exp_recm=@$exp_trans->exp_recm;
                    ?>
                    <td>
                        <?php $explode_exp_rectp=explode(",",$exp_rectp);
                        $explode_exp_recm=explode(",",$exp_recm);
                        ?>

                        <?php foreach($explode_exp_rectp as $expense_type)
                        {
                            echo $expense_type."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_exp_recm as $expense_amt)
                            {
                                echo $expense_amt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total </td>
                    <td><h4><?php
                            $exp_amount=@$exp_trans->total_exp_recm;
                            echo Active_currency($exp_amount);?></h4></td></tr>


            </table>
            <table class="table beauty" cellpadding="0" cellspacing="0">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Advances</th>
                </tr>
                <tr>
                    <?php
                    $advance_rectp=@$advance_trans->loan_rectp; $advance_recm=@$advance_trans->loan_recm;
                    ?>
                    <td>
                        <?php $explode_advance_rectp=explode(",",$advance_rectp);
                        $explode_advance_recm=explode(",",$advance_recm);
                        ?>

                        <?php foreach($explode_advance_rectp as $advance_type)
                        {
                            echo $advance_type."<br />";
                        }?>
                    </td>
                    <td><h4>
                            <?php foreach($explode_advance_recm as $advance_amt)
                            {
                                echo $advance_amt."<br />";
                            }
                            ?></h4></td>
                </tr>
                <tr>
                    <td class="headingfive">Total </td>
                    <td><h4>
                            <?php
                            $advance_amount=@$advance_trans->total_advamount;
                            echo Active_currency($advance_amount);?></h4>
                    </td></tr>
            </table>

            <table class="table beauty" cellpadding="0" cellspacing="0" style="width:45%">
                <tr class="head">
                    <th colspan="8" style="text-align:left;">Total Amount of <?php echo @$details->month." ".$details->year; ?></th>
                </tr>
                <tr >
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><?php echo @$details->salary_date;?></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Net Payable</td>
                    <td>
                        <h4><?php echo Active_currency($salary_info->transaction_amount)." /-";?></h4></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        <table class="table beauty" cellpadding="0" cellspacing="0" style="width:45%">
            <tr class="head">
                <th colspan="8" style="text-align:left;">Remarks</th>
            </tr>
            <tr >
                <td><?php echo $failed_info->remarks;?></td>
            </tr><td></td>

        </table>
            <br class="clear">
        <form action="payroll/re_transaction_request/<?php echo @$salary_info->transaction_id;?>" method="post" name="approve">
            <div id="msg" style="display: none;" class="bubble">Please select status !</div>
            <div class="row">
                <input type="hidden" name="alw_trans_id" value="<?php echo @$allw_trans->alw_trans_id; ?>" />
                <input type="hidden" name="ded_trans_id" value="<?php echo @$ded_trans->ded_trans_id; ?>" />
                <input type="hidden" name="exp_trans_id" value="<?php echo @$exp_trans->exp_trans_id; ?>" />
                <input type="hidden" name="adv_trans_id" value="<?php echo @$advance_trans->adv_trans_id; ?>" />
                <input type="hidden" name="review_id" value="<?php echo @$failed_info->review_id; ?>" />


                <?php	$expense_ids=explode(",",@$exp_trans->exp_ids);
                foreach($expense_ids as $exp_id)
                {?>
                    <input type="hidden" name="exp_ids[]" value="<?php echo $exp_id;?>" />
                <?php
                }
                ?>
            </div>
            <br class="clear">
        </form>
            <div class="row">
                <div class="button-group">
                    <!--<a href="add_employee_personal_info.php"><button class="btn green">Add</button></a>-->
                    <input type="button" name="add" value="Resubmit for Approval" class="btn green" onclick="submit();">
<!--                   <input type="button" name="remarks" class="btn gray"  value="Review Fail" onclick="remark();">-->
                </div>
            </div>

    </div>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript">
       function check()
       {
            var status1=$("#status").val();
            if(status1==3)
            {$("#remarks").show();}
            else{$("#remarks").hide();}
        }
        $( "#accordion" ).accordion();
        $( "#accordion1" ).accordion();
        function submit()
        {
            document.approve.submit();
         }

    </script>

    <!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
        $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
        $('#close-panel-bt').click(function() {
            $.panelslider.close();
        });
    </script>
    <!-- leftside menu end -->


