<!-- contents -->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Payroll / Payroll Transaction Detail</div>
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		<div class="head">Transaction Details of <?php echo $details->full_name;?></div>
			<form>
				<div class="row">
					<h4>Employee Name</h4>
					<h4><i><?php echo $details->full_name;?></i></h4>
				</div>
                
                <div class="row">
					<h4>Employee Code</h4>
					<h4><i><?php echo $details->employee_code;?></i></h4>
				</div>
				<div class="row">
					<h4>Designation</h4>
					<h4><i><?php echo $details->designation_name;?></i></h4>
				</div>
				<div class="row">
					<h4>Pay Grade</h4>
					<h4><i><?php echo $details->pay_grade;?></i></h4>
				</div>
                <div class="row">
					<h4>Transaction Month</h4>
					<h4><i><?php echo $details->calender_month;?></i></h4>
				</div>
				<div class="row">
					<h4>Transaction Type</h4>
					<h4><i><?php echo $details->transaction_type;?></i></h4>
				</div>
                 <div class="row">
					<h4>Transaction date</h4>
					<h4><b><i><?php 
				$date=$details->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></i></h4>
				</div>
                <div class="row">
					<h4>Transaction Status</h4>
					<h4><b><i>
					<?php echo $details->status_title;?></i></h4>
				</div>
                <?php if($details->status_title=="Approved"){?>
                <div class="row">
					<h4>Approved by</h4>
					<h4><b><i>
					<?php echo $details->name;?></i></h4>
				</div>
                <div class="row">
					<h4>Approved date</h4>
					<i><h4>
                    <?php 
				$date=$details->approval_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?>
					</h4></i>
				</div>
                
                <?php }?>
				<div class="row">
					<h4>Transaction Amount</h4>
					<h4><i><?php echo Active_currency($details->transaction_amount);?></i></h4>
				</div>
				
			</form>
		</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->