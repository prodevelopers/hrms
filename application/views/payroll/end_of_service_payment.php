<style type="text/css">

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.dataTables_length{
	position: relative;
	top: 15px;
	font-size: 1px;
	left: -183em;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -15px;
left: -40em;
}
.dataTables_filter input{
	width: 180px;
} 
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
select[name="des"],select[name="mon"]{
	width: 22%;
}
</style>
<!-- contents -->

	<div class="bredcrumb">Dashboard / Payroll / End of Service</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
    <div class="form-left" style="width:50%;">
		<div class="head">End of Service</div>
        <form id="form1">
        <table cellspacing="0" id="table_lists" class="mytab slect">
            <?php if(!empty($info)){
                $joiningDate=$info->contract_start_date;
                $toDate=$info->contract_expiry_date;
                $lastMonth=date("m",strtotime($toDate));
                $lastDays=date("d",strtotime($toDate));

                $diff= abs(strtotime($joiningDate) - strtotime($toDate));
                $years   = floor($diff / (365*60*60*24));
                $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days  = floor(($diff - $months * 30*60*60*24) / (24*60*60));?>
            <tr>
                <td>
                    Name:
                </td>
                <td>
                   <?php echo $info->full_name;?>
                    <input type="hidden" name="employment_id" id="employment_id" value="<?php echo $info->employment_id;?>">
                </td>
                </tr>
                <tr>
                    <td>Designation:</td>
                    <td><?php echo $info->designation_name;?></td>
                </tr>
                <tr><td> Base Salary:</td>
                <td><?php echo $info->base_salary;?></td>
                </tr>
                <tr><td>Salary to pay:</td>
                <td>
                    <?php if($lastDays > 0)
                    {$to_pay=($info->base_salary/30) * ($lastDays);}else
                    {$to_pay=$info->base_salary;}
                    echo round($to_pay);?>
                </td>
                </tr>
                <tr><td>Total Deductions:</td>
                <td><?php  $ded=$info->ded_amount * $months; echo $ded;?> </td>
                    <input type="hidden" name="ded_amount" value="<?php echo $ded;?>">
                    <input type="hidden" name="ded_id" value="<?php echo $info->ded_ids;?>">
                </tr>
                <tr><td>Expenses:</td>
                    <td><?php $expense=$info->exp_amount; echo $expense;?></td>
                    <input type="hidden" name="exp_id" value="<?php echo $info->exp_ids;?>">
                    <input type="hidden" name="exp_amount" value="<?php echo $expense;?>">
                </tr>
                <tr><td>Allowances:</td>
                    <td><?php $allowance=$info->allowance_amnt; echo $allowance;?></td>
                    <input type="hidden" name="alw_id" value="<?php echo $info->allowance_ids;?>">
                    <input type="hidden" name="alw_amount" value="<?php echo $allowance;?>">
                </tr>
                <tr><td>Advance:</td>
                    <td><?php  $advance=$info->loan_amount; echo $advance;?></td>
                    <input type="hidden" name="adv_id" value="<?php echo $info->loan_ids;?>">
                    <input type="hidden" name="adv_amount" value="<?php echo $advance;?>">
                </tr>
                <tr><td>Payable amount:</td>
                <td><?php echo $payable=$ded+round($to_pay)+$expense+$allowance-$advance;?>
                    <input  type="hidden" name="payable" id="payable" value=" <?php echo $payable;?>">
                    <input  type="hidden" name="month" id="month" value=" <?php echo $this->uri->segment(4);?>">
                    <input  type="hidden" name="year" id="year" value=" <?php echo $this->uri->segment(5);?>">
                    <input  type="hidden" name="salary_id" id="salary_id" value=" <?php echo $info->salary_id;?>">
                </td>
            </tr>
                <tr><th>Account Info</th><th></th>
                </tr>
                <?php if(!empty($account_info)){?>
                <tr>
                    <td>Account Title</td>
                    <td><?php echo $account_info->account_title;?></td>
                    <input type="hidden" name="account_id" id="account_id" value="<?php echo $account_info->bank_account_id;?>">
                </tr>
                <tr>
                    <td>Account Number</td>
                    <td><?php echo $account_info->account_no;?></td>
                </tr>
                <tr>
                    <td>Bank</td>
                    <td><?php echo $account_info->bank_name;?></td>
                </tr>
                <tr>
                    <td>Bank Branch</td>
                    <td><?php echo $account_info->branch;?></td>
                </tr>
                    <?php }?>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th><button class="btn green" id="paynow">Submit for Approval</button></th>
                    <th></th>
                </tr>
            <?php }?>
        </table>
        </form>

			





    </div>
    <style>
        .rightSide{float:left;width:45%;height:auto;background: #CCC;margin-top:15px;}
        .topHeader{float:left;width:97%;height:35px;background: #4D6684;color:white;margin-bottom:0px;line-height: 35px;padding-left:20px;}
    .leftPortion{float:left;width:100%;height:auto;background: white;margin-right: 10px;}
        .leftPortion p{color:white;line-height: 35px;padding-left:20px;}
.LtopHeader{float:left;height:35px;background:#4D6684;width:99%;}

    </style>
    <div class="rightSide" id="info" style="">
        <div class="topHeader" style="">
           <p>Basic Information</p>
        </div>
        <div class="leftPortion" style="">
            <!--<div class="LtopHeader" style=" ">
                <p>Deductions</p>
            </div>-->
            <!--<div class="form-right" id="info">-->
            <table>
                <tbody>
                <tr class="table-head">
                    <td colspan="2" style="font-size:16px;color:white;">Deductions</td>
                </tr>
                <?php
                $ded_type = $info->ded_rectp;
                $ded_amt = $info->ded_recm;
                ?><?php $explode_dedtyp = explode(",", $ded_type);
                $explode_dedamt = explode(",", $ded_amt);
                ?>
                <tr class="table-row">
                    <td>
                        <?php foreach ($explode_dedtyp as $ded_typ) {
                            echo "<span>" . $ded_typ . "</span><br class='clear'><br>";

                        }?>
                    </td>

                    <td>
                        <?php foreach ($explode_dedamt as $ded_mt) {
                            echo "<span>" . $ded_mt . "</span><br class='clear'><br>";

                        }
                        ?>
                    </td>
                </tr>
                <tr class="table-row">
                    <td><b>Total Deductions</b></td>
                    <td><b><?php echo $ded; ?></b></td>
                </tr>

                <tr class="table-row">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="table-head">
                    <td colspan="2" style="color:white;font-size:17px;">Allowances</td>
                </tr>
                <?php
                $alwnc_amt = $info->alw_recm;
                $alwnc_type = $info->alw_rectp;
                $explode_alwtyp = explode(",", $alwnc_type);
                $explode_alwamt = explode(",", $alwnc_amt);
                ?>
                <tr class="table-row">
                    <td>
                        <?php foreach ($explode_alwtyp as $alw_typ) {
                            echo "<span>" . $alw_typ . "</span><br><br>";

                        }?>
                    </td>
                    <td>
                        <?php  foreach ($explode_alwamt as $alw_mt) {
                            echo "<span>" . $alw_mt . "</span><br><br>";

                        }
                        ?>
                    </td>
                <tr class="table-row">
                    <td><b>Total Allowance</b></td>
                    <td><b><?php echo $info->allowance_amnt; ?></b></td>
                </tr>
                <tr class="table-row">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="table-head">
                    <td colspan="2" style="color:white;font-size: 17px;">Expenses</td>
                </tr>
                <tr class="table-row">
                    <td>
                        <?php
                        $exp_type = $info->exp_rectp;
                        $epx_amt = $info->exp_recm;
                        $explode_exptyp = explode(",", $exp_type);
                        $explode_expamt = explode(",", $epx_amt);
                        ?>
                        <?php foreach ($explode_exptyp as $expense_typ) {
                            echo "<span>" . $expense_typ . "</span><br><br>";

                        }?>
                    </td>
                    <td>
                        <?php  foreach ($explode_expamt as $expense_mt) {
                            echo "<span>" . $expense_mt . "</span><br><br>";

                        }
                        ?>
                    </td>
                </tr>
                <tr class="table-row">
                    <td><b>Total Expenses</b></td>
                    <td><b><?php  echo $info->exp_amount; ?></b></td>
                </tr>
                <tr class="table-row">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="table-head">
                    <td colspan="2">Advances</td>
                </tr>
                <tr class="table-row">
                    <td>
                        <?php
                        $loan_type = $info->loan_rectp;
                        $loan_amt = $info->loan_recm;
                        $explode_loantyp = explode(",", $loan_type);
                        $explode_loanamt = explode(",", $loan_amt);
                        ?>
                        <?php
                        if ($info->balance_loan_amount > 0) {
                            echo "<span>Balance</span><br/><br/>";
                        } else {
                            foreach ($explode_loantyp as $advance_typ) {
                                echo "<span>" . $advance_typ . "</span><br/><br/>";

                            }
                        }?></td>
                    <td>
                        <?php
                        if ($info->balance_loan_amount > 0) {
                            echo "<span>" . $info->loan_amount . "</span><br/><br/>";
                        } else {
                            foreach ($explode_loanamt as $advance_mt) {
                                echo "<span>" . $advance_mt . "</span><br/><br/>";

                            }
                        }
                        ?>
                    </td>
                <tr class="table-row">
                    <td><b>Total Advances</b></td>
                    <td><b><?php echo $advance; ?></b></td>
                </tr>
                <tr class="table-row">
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <!--</div>-->


        </div>
        <!--<div class="leftPortion" style="">
            <!--<div class="LtopHeader" ">
                <p>Allowances</p>
            </div>-->

        </div>
    </div>
	</div>

</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
    <script>
   /* $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });*/

    /* $('#ded_type,#frequency').select2();*/


    $("#paynow").on('click',function(e){
       var data= $("#form1").serialize();
        e.preventDefault();

        $.ajax({
            url:'payroll/payment_end_of_service',
            data:data,
            type:'POST',
            success: function(output) {
                var data = output.split("::");
                if (data[0] === "OK") {
                    Parexons.notification(data[1], data[2]);
                } else if (data[0] === "FAIL") {
                    Parexons.notification(data[1], data[2]);
                }
            }
             });
            });
  </script>
    <!-- leftside menu end -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>
<!-- leftside menu end -->
