 <?php  ?>
 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
  } else {
        $.post("payroll/autocomplete_deduction/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,amount,id,position,department_name,type) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#amount').append(amount);
	$('#emp_name').append(thisValue);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#ded_type').append(type);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   
        $(document).ready(function(){
            $("#due_month").hide();
            $("#due_year").hide();
				$("#selected").change(function(e){
					if($(this).val()==1){
                        $("#due_month").hide();
                        $("#due_year").hide();
					}
						else if($(this).val()==2)
						{
                            $("#due_month").show();
                            $("#due_year").hide();
                        }
                    else if($(this).val()==3)
                    {
                        $("#due_month").show();
                        $("#due_year").show();
                    }
					});
			});
        $(document).ready(function(){
            $("#eobino").hide();
            $("#eobi").change(function(e){
                if($(this).val()==5){
                    $("#eobino").show();
                }
                else
                {$("#eobino").hide();}
            });
});
</script>
 
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Deductions / Add Deductions</div>
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Add Deductions</div>

			<div id="flash" ><?php echo $this->session->flashdata('message');?></div>
		<div class="form-left">
			<form role="form" method="post" action="payroll/add_deduction_record">
			<div class="row2">
					<h4>Employee Name</h4>
		<input name="name" id="id_input" style="width:180px;" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
   <input type="hidden" name="emp_id" id="emp_id"  />
   
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Deduction Type</h4>
					<?php echo form_dropdown('deduction_type',$deduction,'','id="eobi"' ,'style="width:180px;"','required="required"')?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Deduction Frequency</h4>
                    <?php echo form_dropdown('deduction_freq',$deduction_freq,'','id="selected"',' style="width:180px;"','required="required"')?>

				</div>
                <br class="clear">
                <div class="row2" id="eobino">
                    <h4>EOBI No</h4>
                    <input type="text" name="eobi_no" style="width:180px;" >
                </div>
				<br class="clear">
				<div class="row2">
					<h4>Amount (<?php echo currency_label();?>)</h4>
					<input type="text" name="amount" style="width:180px;" required="required">
				</div>
				<br class="clear">
                <!--<div id="due_date">-->
                    <div id="due_month">
                    <div class="row2">
                        <h4>Due Date</h4>
                        <select name="month" id="month" style="width:180px;">
                            <option value="-1" selected="selected">--Select Month--</option>
                            <?php foreach($months as $mon):?>
                                <option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    </div>
                    <br class="clear"/>
                    <div id="due_year">
                    <div class="row2">
                        <h4>&nbsp;&nbsp;</h4>
                        <select name="year" id="year" style="width:180px;">
                            <option value="-1" selected="selected">--Select Year--</option>
                            <?php foreach($years as $yer):?>
                                <option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                        </div>
                <!--</div>-->
				<!--<div class="row2">
					<h4>Status</h4>
					<?php //echo form_dropdown('status',$status,'required="required"','required="required"')?>
				</div>-->
				<div class="row2">
				<div class="button-group">
					<input type="submit" class="btn green" name="add_rec" value="Submit for Approval ">
				</div>
                </div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<span  class="headingfive">Department</span>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<span  class="headingfive">position</span>
                    	<i class="italica" id="position"></i>
				</div>
                <div class="row2">
                    	<span  class="headingfive">Last Deduction</span>
                    	<i class="italica" id="amount"><span id="ded_type"></span>&nbsp;&nbsp;&nbsp;&nbsp;</i>
				</div>
				
			</div>
    </div>
    </div>
<!-- contents -->

</body>
</html>
<script>
$(function() {
   $('#flash').delay(500).fadeIn('slow', function() {
      $(this).delay(2500).fadeOut('slow');
   });
});
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
