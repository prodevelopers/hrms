<style type="text/css">
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.table_style
{
	width:auto;
	margin-left:0; 
}
select[name="ded_type"],select[name="year"],select[name="month"],select[name="frequency"]{
	width: 22%;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
		function trashed()
		{
		}
	var oTable = $('#table_list').dataTable( {/*
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		    aoData.push({"name":"ded_type","value":$('#ded_type').val()});
			aoData.push({"name":"frequency","value":$('#frequency').val()});
			
                
		}
                
	*/});
	
	//var t=$('#status_lilnk').text();
	//alert(t);
	
	//if()
	
});

/*$(".filter").change(function(e) {
    oTable.fnDraw();
});
*/
function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll /Account Management</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Account Management</div>
		<!-- filter -->
			 <!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
				<input type="text" name="name" style="width:200px" placeholder="Search by Name.." onchange="this.form.submit()">
				<input type="text" name="bank" style="width:200px" placeholder="Search by Bank.." onchange="this.form.submit()">
				<input type="text" name="branch" style="width:200px" placeholder="Search by Branch.." onchange="this.form.submit()">
				<select name="active" id="status" style="width:200px" onchange="this.form.submit();">
					<option value="" selected>-- Select Status--</option>
					<option value="1">Active</option>
					<option value="2">Inactive</option>
				</select>
<!--               <span class="fa fa-print move" style="cursor:pointer" title="Print All" onclick="print_all()"></span>-->
                 <?php echo form_close();?>
			</div>
			<br class="clear">
			<div class="table-responsive">
            <div id="print_table">
				<table cellspacing="0" id="table_lists">
				<thead class="table-head">
                	<td>Code&nbsp;</td>
					<td>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>Account no</td>
                    <td>Bank</td>
                    <td>Branch</td>
                    <td>Branch Code</td>
					<td>Active</td>

				</thead>
					<?php if(!empty($account_info)){
						foreach($account_info as $rec){?>
					<tr class="table-row">
                	<td><?php echo $rec->employee_code;?>&nbsp;</td>
					<td><?php echo $rec->full_name;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><?php echo $rec->account_no;?></td>
                    <td><?php echo $rec->bank_name;?></td>
                    <td><?php echo $rec->branch;?></td>
                    <td><?php echo $rec->branch_code;?></td>
					<td>
					<?php if($rec->active==2){?>
					<a href="payroll/active_account/<?php echo $rec->bank_account_id;?>/<?php echo $rec->employee_id;?>"> <span class="fa fa-check-circle-o" style="cursor: pointer; color: #808080;" title="Click to Activate"></span></a>
					<?php }if($rec->active==1){?>
					<span class="fa fa-check-circle" style="cursor: pointer;  color: green;"></span>
					<?php }?></td>

				</tr>
                <?php }}else{echo "<span style='color:red'> Sorry no Records Found !</span>";}?>
			</table>
			</div>
			
            </div>
			 <div id="container">
        <ul>
        <?php //echo $links;?>
        </ul>
        </div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="payroll/add_account"><button class="btn green">Add Account</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}

function active()
{
	/*$.ajax({
		url: 'payroll/active_account',
		type: 'post', // performing a POST request
		data : {
			act: '1' // will be accessible in $_POST['data1']
		},
		dataType: 'json',
		success: function(data)
		{
			// etc...
		}
	});*/
}

$(document).ready(function() {
	//var pending=$("#status_text").html();
	
	//alert($("#status_text").val());
	});

$(document).ready(function(e){
    $('#status').select2();
})
</script>