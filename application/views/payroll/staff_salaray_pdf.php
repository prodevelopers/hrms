<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
<link rel="stylesheet" type="text/css" href="assets/select2/select2.css">
<link rel="stylesheet" type="text/css" href="assets/css/form.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/data_picker/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style type="text/css">
    .dropdown-menu{
        position: relative;
        left: 70%;
    }
    .margino{
        padding-left: 2em;
        font-family: "Myriad Pro";
    }
    .margino .header {
        width: 200px;
        background: #4D6684;
        color: #fff;
        border:none;
    }
    .marginol{
        position: relative;
        left: 600px;
    }
    .margino ul{
        float: left;
    }
    .margino .menu li{
        float: left;
        width: 200px;
        background: #fff;
        border:none;
    }
    .margino .menu h4{
        font-size: 14px;
        float: left;
    }
    .margino .menu small{
        float: right;
        padding: 0.2em 0;
    }
    .margino .menu a{
        padding: 0.2em 0.5em;
        background: green;
        color: #fff;
    }
    .margino .menu a:hover{
        color: #f8f8f8;
    }
    .margino .menu p{
        float: left;
        width: 100%;
        padding: 0.2em;
        font-size: 13px;
        font-weight: bold;;
    }
    .margino .menu p:hover{
        background: #f8f8f8;
    }
    .margino ul ul{
        position: relative;
        left: -2px;
        top: 27px;
    }
    .margino ul ul li {
        float: left;
    }
    .margino h3 {
        font-size: 12px;
        padding: 0.5em;
    }

    .ol{
        padding: 0;
        position: absolute;
        left: 0;
        /*width: 175px;*/
        height: 50px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        display: none;
        opacity: 0;
        visibility: hidden;
        -webkit-transiton: opacity 0.2s;
        -moz-transition: opacity 0.2s;
        -ms-transition: opacity 0.2s;
        -o-transition: opacity 0.2s;
        -transition: opacity 0.2s;
    }
    /*ul li ol {

    }*/
    .li {
        background: #333;
        display: block;
        /*width: 175px;*/
        color: #fff;
        font-size: 16px;
        float: left;
    }
    .li a{
        font-size: 16px;
    }

    .ol.li:hover{
        /*width: 180px;*/
        padding: 0;
        margin: 0;
        -webkit-transition:none;
        -o-transition:none;
        transition:none;
    }
    ul li:hover .ol {
        display: block;
        opacity: 1;
        visibility: visible;
        position: relative;
    }
    .li h4 {
        padding: 0;
        position: absolute;
        opacity: 0;
        left: 0;
        display: none;
    }
    .li:hover h4{
        display: block;
        font-weight: normal;
        background: #333;
        opacity: 1;
        visibility: visible;
        position: relative;
        padding: 0;
    }
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>

<div class="contents-container">

			<div class="table-responsive">
                <div><h4>Staff Salary List</h4></div>
            <div id="print_table">
            <table class="mytab slect" id="table_lists" cellspacing="0">
                   <thead class="table-head">
                   <tr>
					<td>S.No</td>
                    <td>Name</td>
					<td>Designation</td>
					<td>Project</td>
					<td>Job Grade</td>
					<td>Salary Package</td>
                   </tr>
				</thead>
                <?php if(!empty($info)){
					$no=1;
					foreach($info as $record){
						$count=count($info);
					?>
                <tr class="table-row">
                    <td><?php echo $no;?></td>
                    <td><?php echo $record->full_name;?></td>
					<td><?php echo $record->designation_name;?></td>
					<td><?php echo $record->project_title;?></td>
					<td><?php echo $record->pay_grade;?></td>
					<td><?php echo Active_currency($record->base_salary);?></td>
				</tr>
                <?php $no++;}}else{echo "<span style='color:red'>Sorry no record found!</span>";}?>
                 <tbody>
                    </tbody>
        	 </table>
             
         </div>
			</div>

</div>
