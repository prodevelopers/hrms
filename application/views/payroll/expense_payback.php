<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense Management / Expense Disbursment</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Expense Disbursment</div>

<div class="form-left">
			<form action="payroll/expense_disburments" method="post">
				<div class="row2">
					<h4>Employee Name</h4>
			<input name="name" id="name"  type="text"  value="<?php echo $auto_fill->full_name;?>" readonly="readonly">
            <input type="hidden" name="expense_id" id="expense_id" value="<?php echo $exp_id; ?>"  />
            <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>" />
				</div>
                <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
				<br class="clear">
				<div class="row2">
					<h4>Expense Ammount</h4>
					<input type="text" name="exp_amount" id="exp_amount" value="<?php echo $auto_fill->amount;?>">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Expense Type</h4>
                    <input type="text" name="exp_type" id="exp_type" value="<?php echo $auto_fill->expense_type_title;?>" readonly="readonly">
                    
				</div>
				<!-- hide/show -->
			<script type="text/javascript">
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
				 <br class="clear">
                <div class="row1">
					<h4>Month</h4>
			       <input type="text" name="month_show" value="<?php echo $auto_fill->month;?>"  readonly="readonly"/>
                     <input type="hidden" name="month" value="<?php echo $auto_fill->exp_month;?>"  readonly="readonly"/>
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Year</h4>
                     <input type="text" name="year_show" value="<?php echo $auto_fill->year;?>"  readonly="readonly"/>
                     <input type="hidden" name="year" value="<?php echo $auto_fill->exp_year;?>"  readonly="readonly"/>
					
				</div>
				<br class="clear">
				<div class="row2">
            		<h4 >Payment Mode</h4>
            		<?php echo form_dropdown('payment_mode',$payment_mode,'required="required"','id="selected" required')?>
            	</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="no_cheque">
            	</div>
				<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_cheque" >
            	</div>
                <?php if(!empty($account_info)){
					?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account" value="<?php echo $account_info->account_no;?>" readonly>
                    <input type="hidden" name="account_id" value="<?php echo $account_info->bank_account_id;?>" />
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account" value="<?php echo $account_info->account_title;?>" readonly>
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account" value="<?php echo $account_info->bank_name;?>" readonly>
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account" value="<?php echo $account_info->branch;?>" readonly>
                </div>
                    <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account" value="<?php echo $account_info->branch_code;?>" readonly>
                </div>
                <?php }else{?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div>
                    <div class="row1 account">
                    <h4>Branch code</h4>
                    <input type="text" name="branch_code">
                </div>
                <?php }?>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="received" value="" >
                </div>
                <div class="row1 cash">
                    <h4>Remarks</h4>
                    <input type="text" name="remarks" value="" >
                </div>
                <div class="row">
				<div class="button-group">
					<input type="submit" name="submit" class="btn green" value="Pay Expense">
				</div>
                </div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<span class="headingfive">Employee Name</span>
                    	<i class="italica" id="employee_name"><?php echo $auto_fill->full_name;?></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"><?php echo $auto_fill->employee_code;?></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Department</span>
                    	<i class="italica" id="department"><?php echo $auto_fill->department_name;?></i>
				</div>
				<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"><?php echo $auto_fill->designation_name;?></i>
				</div>
               
                <div class="row2">
                    	<span class="headingfive">Expense Type</span>
                    	<i class="italica" id="amount"> <?php echo $auto_fill->expense_type_title;?></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Expense amount</span>
                    	<i class="italica" id="amount"><?php echo $auto_fill->amount;?></i>
				</div>
                 <div class="row2">
                    	<span class="headingfive">Month</span>
                    	<i class="italica" id="month"><?php echo $auto_fill->month;?></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Year</span>
                    	<i class="italica" id="month"><?php echo $auto_fill->year;?></i>
				</div>
				
			</div>
    </div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->