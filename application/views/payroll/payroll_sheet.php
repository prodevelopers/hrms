<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>
<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
	<div id="print_area">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
body{
	background: #f8f8f8;
}
table th{
	font-size: 12px;
	font-weight:bold;
}
table td{
	font-size: 11px;
}
.shadow h5{
	display:inline-block;
	margin-left: 4em;
}
</style>

<div class="row" style=" height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-12">
			<div class="col-lg-11">
				<h5 class="text-center">Payroll Sheet Of  <?php echo @$month_info->month.", ".@$year_info->year; ?> </h5>
				<h5 class="text-center"><?php echo @$project_info->project_title; ?> </h5>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
				  <tr>
					<th>E.ID</th>
					<th>Name</th> 
					<th>Designation</th> 
					<th>Basic Salary</th>
					<!--<th>HRA</th>
					<th>Utilities</th> -->
					<th>Allowances</th>
					<th>Total Allowances </th>
					<th>Total Expenses </th>
					<th>Balance</th>
					<th>Gross Salary</th>
					<th>Income deduction at source</th>
					<th>Advances</th> 
					<th>EOBI deducted @ PKR 80/month</th>
					<!--<th>Personal Phone Calls</th>-->
					<th>Other Deduction</th>
					<th>Total Deductions</th>
					<th>Net Salary Payable</th>
					<th>Remarks</th>
				  </tr>
				</thead>
				<tbody>
				<?php if(!empty($info)){
				foreach($info as $rec){
				?>
				  <tr class="table-row">
					<td><?php echo $rec->employee_code;?></td>
					<td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<td><?php $base_salary=$rec->base_salary; echo $rec->base_salary;?></td>
					<!--<td><?php /*echo "HRA";*/?></td>
					<td><?php /*echo $rec->allowance_utilities;*/?></td>-->
                      <?php
                      $alwnc_amt=@$rec->grp_allow_amnt;
                      $alwnc_type=@$rec->grp_allow_type;
                      $explode_alwtyp=explode(",",$alwnc_type);
                      $explode_alwamt=explode(",",$alwnc_amt);
                      ?>
                      <td>
                          <?php foreach($explode_alwtyp as $alw_typ)
                          {
                              echo "<span>".$alw_typ."</span><br>";

                          }?>

                          <?php foreach($explode_alwamt as $alw_mt)
                          {
                              echo "<span>".$alw_mt."</span><br>";

                          }
                          ?>
                      </td>
                      <!--<td>Conv.Allowance</td>-->
					<td><?php $allowance=$rec->allowance_amnt; echo $allowance;?></td>
					<td><?php $expense=$rec->exp_amount; echo $expense;?></td>
					<td><?php echo $rec->salary_arears;?></td>
					<td><?php $gross=$base_salary+$expense+$allowance; echo $gross;?></td>
					<td><?php echo $rec->tax_ded;?></td>
					<td><?php $advances=$rec->loan_amount; echo $advances;?></td>
					<td><?php echo $rec->eobi_ded;?></td>
					<!--  <td>Personal Phone Calls</td>-->
					  <td><?php echo $rec->other_ded;?></td>
					<?php $deduction_amount=$rec->ded_amount;?>
					<td><?php echo $deduction_amount;?></td>

					<td><?php echo $rec->transaction_amount;//$net_salary=$gross-$deduction_amount-$advances; echo $net_salary;?></td>
					<td>Remarks</td>
				  </tr>
				<?php }}else{echo "No record found !";}?>
				</tbody>
			</table>
			<div class="col-lg-11 shadow">
				<h5>Prepared By:______________</h5>
				<h5>Checked By:_______________</h5>
				<h5>Authorized By:______________</h5>
				<h5>Approved By: _______________</h5>
			</div>
	</div>
</div>
		</div>
