<style type="text/css">
html, body {
    height: 100%;
    width:104%;
}

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 40px;
}
.resize{
	width: 210px;
}
.rehieght{
	position: relative;
	top: -5px;
}
select[name="year"]{
	position: relative;left: -2em;
}
.dataTables_length{
	position: relative;
	left: -1.3%;
	top: -10px;
	font-size: 1px;
}
.dataTables_length select{
	width: 50px;
}

.dataTables_filter{
	position: relative;
	top: -39px;
	left: -86px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
.table_style
{
	width:auto;
	margin-left:0; 
}
td{
    width:10%;
}

</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {/*
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"oLanguage": {
         "sProcessing": "<img src='assets/images/ajax-loader.gif'>"
        },  
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		    aoData.push({"name":"exp_type","value":$('#exp_type').val()});
                
		}
                
	*/});
	
	/*$('a:contains("Approved")').filter(function(index)
{
    return $(this).text() === "This One";
});*/
     /* search next span and remove it */
    
});

/*$(".filter").change(function(e) {
    oTable.fnDraw();
});*/

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_expensepayment/'+id, "width=800,hight=600");
	/*var pr;
    pr.onload=pr.print() ;
	pr.focus();*/
	}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense Management</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Expense Management</div>

			 <!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
                <?php echo @form_dropdown('exp_type',$expense_type,$slct_exp,"id='exp_type' class='resize' onchange='this.form.submit()'")?>
				<?php echo @form_dropdown('month',$month,$slct_m,"id='month' class='resize' onchange='this.form.submit()'")?>
           	   <?php echo @form_dropdown('year',$year,$slct_y,"id='year' class='resize' onchange='this.form.submit()'")?>								               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <span class="fa fa-print move" style="cursor: pointer; margin-left: 0px; margin-top: 0px; top: 0px; left: 0px;" title="Print All" onclick="print_all()"></span>
                 <?php echo form_close();?>
			</div>

			<div class="table-responsive1">
            <div id="print_table">
			<table cellspacing="0">
				<thead class="table-head">
                    <td>Code </td>
					<td>Name </td>
					<td>Designation</td>
                    <!--<td>Employee Type</td>-->
					<td>Type</td>
					<td>Amount</td>
                    <td>Expense Date</td>
                    <td>Attachment</td>
					<td>Status</td>
                    <!--<td>transaction</td>-->
					<td><span class="fa fa-pencil"></span> &nbsp; &nbsp; | &nbsp;&nbsp;<span class="fa fa-print">
                    </span></td>
                    <td>Pay</td>
				 <span><?php if(!empty($info)){?></span>
				</thead>
                <?php foreach($info as $exp_rec){  
				$status=$exp_rec->status;
                    $file=$exp_rec->file_name;?>

                <tr class="table-row">
					<td><?php echo $exp_rec->employee_code?></td>
					<td><?php echo $exp_rec->full_name?></td>
					<td><?php echo $exp_rec->designation_name?></td>
                    <!--<td>Employee Type</td>-->
					<td><?php echo $exp_rec->expense_type_title?></td>
					<td><?php echo Active_currency($exp_rec->amount);?></td>
                    <td><?php
					$date=$exp_rec->expense_date;
					$new=date("d-m-Y", strtotime($date));
					 echo $new;?></td>
                    <td><?php if($file)
                        {?><a href="upload/expense_receipts/<?php echo $file;?>"><span class="fa fa-paperclip" style="color: #000000"></span></a>
                        <?php }else{echo "n/a";}?></td>
					<td><?php
					if($status == 2)
					{echo "<span style='color:green'>Approved</span>";}
					else{echo "<span style='color:red'>Pending</span>";}?><!--<a href="payroll/expense_status_update/<?php //echo $exp_rec->status_title;?>/<?php //echo $exp_rec->expense_claim_id;?>">
					<?php  //echo $exp_rec->status_title;?></a>--><?php //}?>
                    </td>
                    
                    <!--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php /*?><?php if($exp_rec->status=2 && $exp_rec->transaction_id== NULL){?>
                     <a href="payroll/expense_transaction/<?php echo $exp_rec->employee_id; ?>/<?php echo $exp_rec->amount; ?>/<?php echo $exp_rec->exp_id; ?>">
                     <span class="fa fa-money"></span></a><?php }
					elseif($exp_rec->status=2 && $exp_rec->transaction_id !== NULL)
					 {echo "<span style='color:green;'>Done !</span>";}
					 else{} ?><?php */?>
                     </td>-->
                     
					<td><?php 
					if($status == 2)
					{echo '&nbsp;&nbsp;&nbsp;';}
					else{?><a href="payroll/edit_expense/<?php echo $exp_rec->expense_claim_id; ?>"><span class="fa fa-pencil"></span></a>
					<?php }?>&nbsp; &nbsp; | &nbsp;&nbsp;
                    <span onclick="return print_report(<?php echo $exp_rec->expense_claim_id; ?>)" class="fa fa-print" style="cursor:pointer"></span>
                    </span>
                    </td>
                    
                    <td><?php 
					//$exp_dis_id= $exp_rec->expense_id;
					$exp_id=$exp_rec->exp_id;
					$exp_dis_id=$exp_rec->exp_dis_id;
				    //$exp_dis_month=$exp_rec->exp_dis_month;
				   // $exp_dis_year=$exp_rec->exp_dis_year;
					$exps_month=$exp_rec->exp_month;
					 $exps_year=$exp_rec->exp_year;
					$exp_dis_transaction=$exp_rec->transaction_id;
					$exp_paid=$exp_rec->paid;
					
					if($status == 1){}
					 elseif($status == 2 && $exp_paid==1){echo "<span style='color:#181818 ;'>Paid</span>";}
				elseif($status == 2 && $exp_paid == 0)
					 {?>
				<a href="payroll/expense_disb/<?php echo $exp_rec->employee_id; ?>/<?php echo $exp_rec->expense_claim_id; ?>" style="color:green">Pay</a><?php }else{}?>
                    </td>
                    
                    </tr>
	            <?php }}
			else {echo "<span style='color:red'> Sorry no Records Found !</span>";} ?>
			</table>
			</div>
			 <br />
            <br />
			</div>
        <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>
       
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="payroll/add_expense"><button class="btn green">New Expense</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e)
    {
        $('#exp_type,#month,#year').select2();
    })

    </script>
    <!-- leftside menu end -->