<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 240px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		/*$('#employee_inf0').hide();*/
		location.reload();
	
    } else {
        $.post("payroll/autocomplete_employees/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name,base_salary) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	$('#base_salary').val(base_salary);
	$('#base_salaryli').append(base_salary);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Account management</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Account Management</div>

			
			<!-- hide/show -->

			<div class="form-left">
			<form action="payroll/add_account_info" method="post">
                <?php //if(!empty($account_info)){
                    ?>
                    <div class="row1">
                        <h4>Employee Name</h4>
                        <input name="name" id="id_input" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
                        <div id="suggestions" style="margin:0px 0px 0px 250px;">
                            <div class="autoSuggestionsList_l" id="autoSuggestionsList">
                            </div>
                        </div>
                        <input type="hidden" name="emp_id" id="emp_id"  />
                    </div>
                    <br class="clear">
                    <div class="row1 account">
                        <h4>Account No</h4>
                        <input type="text" name="no_account" required="required">
                    </div>
                    <div class="row1 account">
                        <h4>Account Title</h4>
                        <input type="text" name="title_account" required="required">
                    </div>
                    <div class="row1 account">
                        <h4>Bank</h4>
                        <input type="text" name="bank_account" required="required">
                    </div>
                    <div class="row1 account">
                        <h4>Branch</h4>
                        <input type="text" name="branch_account" required="required">
                    </div>
                <div class="row1 account">
                        <h4>Branch code</h4>
                        <input type="text" name="branch_code" required="required">
                    </div>
                <?php //}?>
                <br class="clear">
                <div class="row1">
                    <h4></h4>
                    <input type="submit" name="submit" value="submit" class="btn green">
                </div>
			</form>
			</div>
        <div class="form-right" id="employee_inf0">
            <div class="head">Information</div>
            <div class="row2">
                <span  class="headingfive">Department</span>
                <i class="italica" id="department"></i>
            </div>
            <div class="row2">
                <span  class="headingfive">position</span>
                <i class="italica" id="position"></i>
            </div>

        </div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->