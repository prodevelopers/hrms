<style type="text/css">

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.dataTables_length{
	position: relative;
	top: 15px;
	font-size: 1px;
	left: -183em;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -15px;
left: -40em;
}
.dataTables_filter input{
	width: 180px;
} 
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
select[name="des"],select[name="mon"]{
	width: 22%;
}
.select2-results li{
    width: 300px;
    border-bottom:thin solid #ccc;
    height:60px;
}
.select2TemplateImg { padding:0.2em 0;}
.select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
.select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
.resize{width: 220px;}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    function print_report(id) {
        window.open('payroll/print_salarypayment/' + id, "width=800,hight=600");
        <!--pr.onload=pr.print() ;-->
        <!--pr.focus();-->
    }
});
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / End of Service</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">End of Service</div>
			<div class="filter">
                <form action="" method="post">
				<input type="hiiden" name="name" id="selectEmployee" onchange="this.form.submit()" style="width:22%; ">
				<!--<select name="des">
					<option>Designation</option>
				</select>-->
				<?php if(!empty($months)){
                echo form_dropdown('month',$months,$selected_month,'id="month" class="resize"');}?>
				<input type="submit" class="btn green" value="Search">
                </form>
            </div>
		<!-- table -->
			<table cellspacing="0" id="table_lists" class="mytab slect">
				<thead class="table-head">
					<td>Name</td>
					<td>Designation</td>
					<!--<td>Paygrade</td>-->
                   <!-- <td>Contract From Date</td>-->
					<td>Contract Expiry</td>
					<td>Base Salary</td>

                    <td>Salary to pay</td>
                    <td>Refund Deductions</td>
					<td>Expense</td>
					<td>Allowance</td>
					<td>Advance</td>
                    <td>Payable Amount </td>
                    <td>EOS Settlement </td>
                    <td>View</td>
				</thead>
                <?php if(!empty($info)){
				foreach($info as $rec){
                    $joiningDate=$rec->contract_start_date;
                    $toDate=$rec->contract_expiry_date;
                    $lastMonth=date("m",strtotime($toDate));
                    $lastDays=date("d",strtotime($toDate));
                    $diff= abs(strtotime($joiningDate) - strtotime($toDate));
                    $years   = floor($diff / (365*60*60*24));
                    $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days  = floor(($diff - $months * 30*60*60*24) / (24*60*60));
                    $eos_payment_status=$rec->settlement_payment_status;

                    if($lastDays > 0)
                    {$to_pay=round(($rec->base_salary/30) * ($lastDays));}else
                    {$to_pay=round($rec->base_salary);}
				?>
                <tr class="table-row">
					<td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<!--<td><?php /*echo $rec->pay_grade;*/?></td>-->
					<!--<td><?php /*echo date_format_helper($rec->contract_start_date);*/?></td>-->
					<td><?php echo date_format_helper($rec->contract_expiry_date);?></td>
                    <td><?php echo Active_currency($rec->base_salary);?></td>
					<td title="<?php echo "Salary of ".$lastDays." days =".$to_pay;?>"><?php
                    echo Active_currency($to_pay);?></td>
                    <td title=" <?php echo "Deduction per month =".$rec->ded_amount."\n deducted months =".$months."\n Total refund =".$ded=$rec->ded_amount * $months;
                    ?>"><?php echo $ded;?></td>
                    <!--<td title="<?php /*if($days > 0){echo "Base Salary is =".$rec->base_salary."\n Salary of ".$days." days =".round($to_pay=($rec->base_salary/30) * ($days));}*/?>"><?php /*$to_pay=($rec->base_salary/30) * ($days); echo round($to_pay);//$rec->transaction_amount;*/?></td>-->
                    <td><?php $expense=$rec->exp_amount;echo Active_currency($expense);?></td>
                    <td><?php $allowance=$rec->allowance_amnt; echo Active_currency($allowance);?></td>
                    <td><?php $advance=$rec->loan_amount; echo Active_currency($rec->loan_amount);?></td>
                    <td><?php echo $payable=Active_currency($ded+round($to_pay)+$expense+$allowance-$advance);?></td>
                    <td><?php if($eos_payment_status == 1){echo "<span style='color: #008000'>Paid</span>";}else{?><a href="payroll/end_of_service_payment_view/<?php echo $rec->employment_id.'/'.$month.'/'.$current_year;?>">EOS Settelment</a><?php }?></td>
                    <td><a href="payroll/end_of_service_payment_details/<?php echo $rec->employment_id.'/'.$month.'/'.$current_year;?>"><span class="fa fa-eye"></span></td>
				</tr>
                <?php }}else{echo "<span style='color:red'> Sorry no Records Found !</span>";}?>
				
			</table>
			
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e)
    {
        $('#ded_type,#frequency').select2();
    })
    </script>
    <!-- leftside menu end -->
<script type="text/javascript">
    $(document).ready(function() {

        /* New Select Employee
         * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
         * */
        var selectEmployeeSelector = $('#selectEmployee');
        var url = "<?php echo base_url(); ?>payroll/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar: 'EmployeeAvatar',
            employeeCode: 'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";

        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector, url, tDataValues, minInputLength, placeholder, baseURL, templateLayout);
        $('.select2-container').css("width", "30%");
    });
</script>
<script>
    $(document).ready(function(e){
        $('#month').select2();
    })
</script>