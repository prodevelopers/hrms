<style type="text/css">
    #autoSuggestionsList {
        width: 205px;
        overflow: hidden;
        border: 1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display: none;
    }

    .designing {
        top: 200px;
        right: 200px;
    }

    #autoSuggestionsList li {
        list-style: none;
        cursor: pointer;
        padding: 5px;
        font-size: 14px;
        font-family: Arial;
        margin: 2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #autoSuggestionsList li:hover {
        border: 1px solid #CCC;
    }

    .serch {
        width: 200px;
        height: 25px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: black;
        padding-left: 5px;
        border: 1px solid #999;
    }

    .row1 h4, .row2 h4, .row1 span {
        font-size: 13px;
    }

    #employee_inf0 {
        width: 240px;
        overflow: hidden;
        border: 1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: none;
        alignment-adjust: central;
    }

    input[type=submit]:disabled {
        background: #dddddd;
    }

    .disabled {
        background-color: #808080;
    }
    tr td{
        word-wrap: break-word;
    }
</style>
<script type="text/javascript">
    function lookup(inputString) {
        if (inputString.length == 0) {
            $('#suggestions').hide();
            /*$('#employee_inf0').hide();*/
            location.reload();

        } else {
            $.post("payroll/autocomplete_paysalary/", {queryString: "" + inputString + ""}, function (data) {
                if (data.length > 0) {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').show();
                    $('#autoSuggestionsList').html(data);
                }
            });
        }
    }
    function clear_div() {
        document.getElementById('#employee_inf0').innerHTML = "";
    }

    function fill(thisValue, employee_code, cnic, id, position, department_name, base_salary, salary_id) {
        $('#id_input').val(thisValue);
        $('#employee_code').append(employee_code);
        $('#cnic').append(cnic);
        $('#emp_id').val(id);
        $('#position').append(position);
        $('#department').append(department_name);
        $('#employee_name').append(thisValue);
        $('#base_salary').val(base_salary);
        $('#base_salaryli').append(base_salary);
        $('#salary_id').val(salary_id);
        setTimeout("$('#suggestions').hide();", 200);
        $('#employee_inf0').show();

    }

</script>
<!-- contents -->
<div class="contents-container">

<div class="bredcrumb">Dashboard / Payroll / Pay Salary</div>
<!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel"><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;"
                                                   class="fa fa-bars fa-2x"></span></a>

<div class="right-contents">

<div class="head">Salary  <?php echo $salary_month . ", " . $salary_year; ?></div>
<!-- hide/show -->
<script type="text/javascript">
    $(document).ready(function () {
        if($('#par_pay').is(':checked')){
            $(".partial").show();
            var payableAmount = parseInt($('#p').val());
            var amountToPay = parseInt($('#to_pay').val());
//            var balance = parseInt($('#arears').val());
            console.log($('#to_pay').val().length);
            if($('#to_pay').val().length === 0){
                $('#arears').val(payableAmount);
            }else if($('#to_pay').val().length > 0){
                var balance = payableAmount - amountToPay;
                $('#arears').val(balance);
            }
        }else{
            $(".partial").hide();
        }
        $("#par_pay").change(function (e) {
            if ($(this).is(':checked')) {
                $(".partial").show();
            } else {
                $(".partial").hide();
                $("#submitbtn").removeAttr('disabled', true);
            }
        });
        $(".cheque").hide();
        $(".account").show();
        $(".cash").hide();
        $("#selected").change(function (e) {
            if ($(this).val() == 3) {
                $(".account").show();
                $(".cheque").hide();
                $(".cash").hide();
            }
            if ($(this).val() == 2) {
                $(".cheque").show();
                $(".account").hide();
                $(".cash").hide();
            }
            if ($(this).val() == 1) {
                $(".cash").show();
                $(".cheque").hide();
                $(".account").hide();
            }
        });

    });
</script>
<div class="form-left">
<form action="payroll/pay_salary_amount" method="post">
<table class="table" style="width:60%; margin-left: 1em; float: left;">
    <thead class="table-head">
    <tr>
        <th colspan="2">Salary Information</th>
    </tr>
    </thead>
    <tbody>
    <tr class="table-row">
        <td><h4>Name</h4></td>
        <td><span><?php echo $info->full_name; ?></span></td>
    </tr>
    <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $info->employee_id; ?>"/>
    <input type="hidden" name="salary_id" id="salary_id" value="<?php echo $info->salary_id; ?>"/>
    <input type="hidden" name="month" value="<?php echo $month_id; ?>"/>
    <input type="hidden" name="year" value="<?php echo $year_id; ?>"/>

    <div id="suggestions" style="margin:0px 0px 0px 250px;">
        <div class="autoSuggestionsList_l" id="autoSuggestionsList">
        </div>
    </div>
    <tr class="table-row">
        <td><h4>Base Salary + Increments</h4></td>
        <?php
            $CurrentSalary = $info->base_salary + (isset($totalIncrementedAmount)?$totalIncrementedAmount:0);
        ?>
        <td><span><?php echo Active_currency($info->base_salary) ." + ". $totalIncrementedAmount; ?></span><input type="hidden" name="base_salary" id="base_salary"
                                                                 value="<?php echo $CurrentSalary; ?>"
                                                                 readonly="readonly"></td>
    </tr>
    <tr class="table-row">
        <td><h4>Allowances</h4></td>
        <td> <?php
            $alwnc_amount = $info->allowance_amnt;?>
            <span><?php echo Active_currency($alwnc_amount); ?></span>
            <input type="hidden" name="allowance_amount" value="<?php echo $alwnc_amount; ?>" readonly
                   style="width:113px;">
            <?php    $alwnce_ids = explode(",", $info->allowance_ids);
            foreach ($alwnce_ids as $alw_id) {
                ?>
                <input type="hidden" name="alw_ids[]" value="<?php echo $alw_id; ?>"/>
            <?php
            }
            ?></td>
    </tr>
    <tr class="table-row">
        <td><h4>Expenses</h4></td>
        <td>
            <?php $expense_amount = $info->exp_amount; ?>
            <span><?php echo Active_currency($expense_amount); ?></span>
            <input type="hidden" name="expense_amount" value="<?php echo $expense_amount; ?>" readonly
                   style="width:113px;">
            <?php  $exp_ids = explode(",", $info->exp_ids);
            foreach ($exp_ids as $exp_id) {
                ?>
                <input type="hidden" name="exp_ids[]" value="<?php echo $exp_id; ?>"/>
            <?php
            }
            ?></td>
    </tr>
    <tr class="table-row">

        <?php
        if (@$info->balance_loan_amount > 0){
        $loan_advance = $info->balance_loan_amount;
        ?>
        <td><h4>Advances Balance (Pay back <?php echo currency_label();?>)</h4></td>
        <td><input type="text" name="loan_amount" value="<?php echo @$loan_advance; ?>" id="loan_amount"
                   onkeyup="total_adv()">
            <input type="hidden" name="total_loan_amount" value="<?php echo @$loan_advance; ?>" id="total_loan_amount">
            <input type="hidden" name="advance_balance" value="<?php echo @$loan_advance; ?>"/>
            <input type="hidden" name="remain_balance" value="0" id="remain_balance"/>
            <?php }else{
            @$loan_advance = $info->loan_amount; ?>
        <td><h4>Advances (Pay back)</h4></td>
        <td><input type="text" name="loan_amount" value="<?php echo @$loan_advance; ?>" id="loan_amount"
                   onkeyup="total_adv()">
            <input type="hidden" name="total_loan_amount" value="<?php echo @$loan_advance; ?>" id="total_loan_amount">
            <?php } ?>

            <br/>
            <br/>
            <span style="font-size: smaller" title="You can deduct Advances in Instalments !">( You can deduct Advances in Instalments !)</span>
            <span id="check_adv" style='color: red; display: none'>Payment exceeded !<small></small></span>
            <span id="remain_adv" style='color: green; display: none'></span>

            <!--<input type="hidden" name="loan_amount" value="<?php /*echo $loan_advance;*/ ?>" readonly style="width:113px;">-->
            <?php  $loan_ids = explode(",", $info->loan_ids);
            foreach ($loan_ids as $loan_id) {
                ?>
                <input type="hidden" name="loan_ids[]" value="<?php echo $loan_id; ?>"/>
            <?php } ?></td>
        <script>
            function total_adv() {
                $("#remain_adv").text("");
                //alert("working");
                var amount1 = parseInt($("#loan_amount").val());
                var total1 = parseInt($("#total_loan_amount").val());
                //alert(total1);
                if ((amount1 === total1) || (amount1 < total1)) {
                    $("#submitbtn").removeAttr('disabled');
                    $("#submitbtn").removeClass("disabled")
                    $("#check_adv").hide();
                    var remain1 = total1 - amount1;
                    $("#remain_adv").append("Balance " + remain1);
                    $("#remain_balance").val(remain1);

                    $("#remain_adv").show(remain1);
                    pay_able(amount1);
                }
                else {
                    $("#check_adv").show();
                    $("#submitbtn").attr('disabled', true);
                    $("#submitbtn").addClass("disabled");
                }
            }
        </script>
    </tr>
    <tr class="table-row">
        <td><h4>Total Deductions</h4></td>
        <td><?php $deduction_amount = $info->ded_amount; ?>
            <span></span><?php echo Active_currency($deduction_amount); ?></span>
            <input type="hidden" name="deduction_amount" value="<?php echo $deduction_amount; ?>" readonly
                   style="width:113px;">
            <?php  $ded_ids = explode(",", $info->ded_ids);
            foreach ($ded_ids as $ded_id) {
                ?>
                <input type="hidden" name="ded_ids[]" value="<?php echo $ded_id; ?>"/>
            <?php
            }
            ?></td>
    </tr>

    <?php //IncomeTax Deduction ?>
    <tr class="table-row">
        <td><h4>IncomeTax (<?php echo currency_label();?>)</h4></td>
        <?php if (isset($slabDetails) && !empty($slabDetails->MonthlyTax)) { ?>
            <td><?php echo Active_currency($slabDetails->MonthlyTax); ?></td>
        <?php } else { ?>
            <td>0.00</td><?php } ?>
    </tr>

    <tr class="table-row">
        <td><h4>Arrears (<?php echo currency_label();?>)</h4></td>
        <?php if (!empty($last_salary->arears)) { ?>
            <td><?php echo Active_currency($last_salary->arears); ?></td>
        <?php } else { ?>
            <td>0.00</td><?php } ?>
    </tr>
    <!--                    <tr class="table-row">-->
    <?php
    $base_salary = $info->base_salary;
    $deduction_amount;
    $loan_advance;
    $expense_amount;
    $alwnc_amount;
    //$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;

    //changed base salary with the currentsalary(current salary includes the increments)
    $payable1 = $CurrentSalary + $expense_amount + $alwnc_amount - $deduction_amount;
    ?>

    <input type="hidden" name="pay_amount_1" id="pay_amount_1" value="<?php echo $payable1; ?>"/>

    <script>
        $(document).ready(function () {
            if ($("#loan_amount").val() > 0) {
                var arrears = parseInt($("#arrears").val());
                var pay_grs = parseInt($("#pay_amount_1").val());
                var loan = parseInt($("#loan_amount").val());
                var total_amount = pay_grs + arrears - loan;
                $("#pay").append(total_amount);
                $("#p").val(total_amount);
            }
            if (parseInt($("#loan_amount").val()) == 0) {
                var arrears = parseInt($("#arrears").val());
                var pay_grs = parseInt($("#pay_amount_1").val());
                var total_amount = pay_grs + arrears;
                //alert(total_amount);
                $("#pay").text(total_amount);
                $("#p").val(total_amount);
                console.log(total_amount);
            }
        });
        function pay_able(amount1) {
            $("#pay").text("");
            var pay_1 = parseInt($("#pay_amount_1").val());
            var arrears = parseInt($("#arrears").val());
            var pay_able = pay_1 + arrears - parseInt(amount1);
            $("#pay_amount").val(pay_able);
            $("#pay").append(pay_able);
            $("#p").val(pay_able);


        }
    </script>

    <input type="hidden" name="pay_amount" id="pay_amount" value=""/>
    <input type="hidden" name="deductible_amount" id="deductible_amount" value="<?=$AmountToDeduct?>"/>

    <tr class="table-row">
        <td><h4>Payable Amount (<?php echo currency_label();?>)</h4></td>

        <?php if ((isset($last_salary)?intval($last_salary->arears):0) > 0){ ?>
            <td>
                <input type="hidden" name="arrears" value="<?php echo $last_salary->arears; ?>" id="arrears"/>
                <!-- <span id="pay"></span>-->
                <span id="pay"></span></td>
            <input type="hidden" name="amount_include_arear" id="p" value=""/>
        <?php }else{?>
        <input type="hidden" name="arrears" value="0" id="arrears"/>
        <td><span id="pay"></span>
            <input type="hidden" name="pay_amount" id="p" value=""/></td>
            <?php } ?>
    </tr>
    </tbody>
</table>
    <br class="clear">
    <div class="row1">
        <h4>Absentees Deduction (<?php echo currency_label();?>)</h4>
        <input type="checkbox" id="AttendanceDeduction" name="absenteesDeduction" title="Leaves Deduction"/>
    </div>
<br class="clear">
<div class="row1">
    <h4>Partial Payment Option</h4>
    <span id="pay_partial"><input type="checkbox" id="par_pay" title="Check and enter payment"/></span>
</div>

<br class="clear">

<div class="row2 partial" id="Partial">
    <h4>Amount to Pay (<?php echo currency_label();?>)</h4>
    <input type="text" name="to_pay" id="to_pay" onkeyup="total()"/>
    <span id="check" style='color: red; display: none'>Payment exceeded !<small></small></span>
    <script>
        function total() {
            var p = parseInt($("#p").val());
            var to_pay = parseInt($("#to_pay").val());

            if ((to_pay == p) || (to_pay < p)) {
                $("#submitbtn").removeAttr('disabled');
                $("#check").hide();
                var total = (p - to_pay);
                $("#arears").val(total);

            }else if($('#to_pay').val().length === 0){
                $("#check").hide();
                $("#arears").val(p);
            }
            else {
                $("#check").show();
                $("#submitbtn").attr('disabled', true);
                $("#submitbtn").attr('style="background:#ccc important!"', true);
            }

        }
    </script>
</div>
<br class="clear">

<div class="row2 partial" id="Arears">
    <h4>Balance (<?php echo currency_label();?>)</h4>
    <input type="text" name="arears" id="arears"/>
</div>
<?php if (!empty($last_salary->arears)) { ?>
    <br class="clear">
    <!--<div class="row2 partial" id="PrArears">
					<h4>Paid of Previous Arrears</h4>
					<input type="text" name="prvarears" id="prvarears"/>
					<input type="hidden" name="salary_payment_id" value="<?php /*echo $last_salary->salary_payment_id ;*/ ?>">
				</div>-->
<?php } ?>
<!--<script type="text/javascript">
function payment(){
var salary_field=this['base_salary'].value;
var paym_amount=this['pay_amount'].value;
var remain=(salary_field) - (paym_amount);
$('#remain_amount').val(remain);
}

</script>-->

<br class="clear">
<br class="clear">

<div class="row2">
    <h4>Payment Mode</h4>
    <?php $account = 3;
    echo form_dropdown('payment_mode', $payment_mode, $account, 'id="selected"')?>
</div>
<br class="clear">

<div class="row1 cheque">
    <h4>Cheque No</h4>
    <input type="text" name="no_cheque">
</div>
<div class="row1 cheque">
    <h4>Bank</h4>
    <input type="text" name="bank_cheque">
</div>
<?php if (!empty($account_info)) {
    ?>
    <div class="row1 account">
        <h4>Account No</h4>
        <span><?php echo $account_info->account_no; ?></span>

        <input type="hidden" name="account_id" value="<?php echo $account_info->bank_account_id; ?>"/>
    </div>
    <div class="row1 account">
        <h4>Account Title</h4>
        <span><?php echo $account_info->account_title; ?></span>

    </div>
    <div class="row1 account">
        <h4>Bank</h4>
        <span><?php echo $account_info->bank_name; ?></span>

    </div>
    <div class="row1 account">
        <h4>Branch</h4>
        <span><?php echo $account_info->branch; ?></span>
    </div>
    <div class="row1 account">
        <h4>Branch Code</h4>
        <span><?php echo $account_info->branch_code; ?></span>
    </div>
<?php } else { ?>
    <div class="row1 account">
        <h4>Account No</h4>
        <input type="text" name="no_account">
    </div>
    <div class="row1 account">
        <h4>Account Title</h4>
        <input type="text" name="title_account">
    </div>
    <div class="row1 account">
        <h4>Bank</h4>
        <input type="text" name="bank_account">
    </div>
    <div class="row1 account">
        <h4>Branch</h4>
        <input type="text" name="branch_account">
    </div>
    <div class="row1 account">
        <h4>Branch Code</h4>
        <input type="text" name="branch_code">
    </div>
<?php } ?>
<div class="row1 cash">
    <h4>Received By</h4>
    <input type="text" name="recieved">
</div>
<div class="row1 cash">
    <h4>Remarks</h4>
    <input type="text" name="remarks">
</div>
<div class="row">
    <div class="button-group">
        <?php if ($count == 1){ ?>
            <input type="submit" name="submit" class="btn green" value="Submit for Review" id="submitbtn"/>
        <?php }else{ ?>
        <input type="submit" name="submit" class="btn green" id="submitbtn" value="Submit Next Salary"/>
        <?php } ?><!-- <button class="btn red">Delete</button> -->
    </div>
</div>
</form>
</div>
<div class="form-right" id="info">
    <table style="table-layout: fixed">
        <!-- <thead class="table-head">
             <tr>
                 <td colspan="2">Basic Information</td>
             </tr>
             <tr>
                 <td></td>
                 <td></td>
             </tr>
         </thead>-->
        <tbody>
        <tr class="table-head">
            <td colspan="2" title="Subtract From Salary">Deductions  - </td>
        </tr>
        <?php
        $ded_type = $info->ded_rectp;
        $ded_amt = $info->ded_recm;
        ?>
        <?php $explode_dedtyp = explode(",", $ded_type);
        $explode_dedamt = explode(",", $ded_amt);
        ?>
        <tr class="table-row">
            <td>
                <?php foreach ($explode_dedtyp as $ded_typ) {
                    echo "<span>" . $ded_typ . "</span><br class='clear'><br>";

                }?>
            </td>

            <td>
                <?php foreach ($explode_dedamt as $ded_mt) {
                    echo "<span>" . $ded_mt . "</span><br class='clear'><br>";

                }
                ?>
            </td>
        </tr>
        <tr class="table-row">
            <td><b>Total Deductions</b></td>
            <td><b><?php echo Active_currency($deduction_amount); ?></b></td>
        </tr>

        <tr class="table-row">
            <td></td>
            <td></td>
        </tr>
        <tr class="table-head">
            <td colspan="2" title="Add With Salary">Allowances  +</td>
        </tr>
        <?php
        $alwnc_amt = $info->alw_recm;
        $alwnc_type = $info->alw_rectp;
        $explode_alwtyp = explode(",", $alwnc_type);
        $explode_alwamt = explode(",", $alwnc_amt);
        ?>
        <tr class="table-row">
            <td>
                <?php foreach ($explode_alwtyp as $alw_typ) {
                    echo "<span>" . $alw_typ . "</span><br><br>";

                }?>
            </td>
            <td>
                <?php foreach ($explode_alwamt as $alw_mt) {
                    echo "<span>" . $alw_mt . "</span><br><br>";

                }
                ?>
            </td>
        <tr class="table-row">
            <td><b>Total Allowance</b></td>
            <td><b><?php echo Active_currency($info->allowance_amnt); ?></b></td>
        </tr>
        <tr class="table-row">
            <td></td>
            <td></td>
        </tr>
        <tr class="table-head">
            <td colspan="2" title="Add With Salary">Expenses  +</td>
        </tr>
        <tr class="table-row">
            <td>
                <?php
                $exp_type = $info->exp_rectp;
                $epx_amt = $info->exp_recm;
                $explode_exptyp = explode(",", $exp_type);
                $explode_expamt = explode(",", $epx_amt);
                ?>
                <?php foreach ($explode_exptyp as $expense_typ) {
                    echo "<span>" . $expense_typ . "</span><br><br>";

                }?>
            </td>
            <td>
                <?php foreach ($explode_expamt as $expense_mt) {
                    echo "<span>" . $expense_mt . "</span><br><br>";

                }
                ?>
            </td>
        </tr>
        <tr class="table-row">
            <td><b>Total Expenses</b></td>
            <td><b><?php echo Active_currency($info->exp_amount); ?></b></td>
        </tr>
        <tr class="table-row">
            <td></td>
            <td></td>
        </tr>
        <tr class="table-head">
            <td colspan="2" title="Subtract From Salary">Advances  -</td>
        </tr>
        <tr class="table-row">
            <td>
                <?php
                $loan_type = $info->loan_rectp;
                $loan_amt = $info->loan_recm;
                $explode_loantyp = explode(",", $loan_type);
                $explode_loanamt = explode(",", $loan_amt);
                ?>
                <?php
                if ($info->balance_loan_amount > 0) {
                    echo "<span>Balance</span><br/><br/>";
                } else {
                    foreach ($explode_loantyp as $advance_typ) {
                        echo "<span>" . $advance_typ . "</span><br/><br/>";

                    }
                }?></td>
            <td>
                <?php
                if ($info->balance_loan_amount > 0) {
                    echo "<span>" . $info->loan_amount . "</span><br/><br/>";
                } else {
                    foreach ($explode_loanamt as $advance_mt) {
                        echo "<span>" . $advance_mt . "</span><br/><br/>";

                    }
                }
                ?>
            </td>
        <tr class="table-row">
            <td><b>Total Advances</b></td>
            <td><b><?php echo Active_currency($info->loan_amount); ?></b></td>
        </tr>
        <tr class="table-row">
            <td></td>
            <td></td>
        </tr>
        <tr class="table-head">
            <td colspan="2" title="Subtract From Salary">Income Tax  -</td>
        </tr>
        <tr class="table-row">
            <td>
                Tax Slab No
            <td>
                <?=(isset($slabDetails)&& !empty($slabDetails->SlabNo))?$slabDetails->SlabNo:"-";?>
            </td>
        <tr class="table-row">
            <td><b>Slab Remarks</b></td>
            <td><?=(isset($slabDetails)&& !empty($slabDetails->Remarks))?$slabDetails->Remarks:"-";?></td>
        </tr>
        <tr class="table-row">
            <td><b>Slab Total Fixed Annual</b></td>
            <td><b>Rs. <?=(isset($slabDetails)&& !empty($slabDetails->FixedAmount))?$slabDetails->FixedAmount:"-";?> + </b></td>
        </tr>
        <tr class="table-row">
            <td><b>Monthly Tax (TotalFixed/12)</b></td>
            <td><b>Rs. <?=(isset($slabDetails)&& !empty($slabDetails->MonthlyTax))?$slabDetails->MonthlyTax:"-";?></b></td>
        </tr>
        <tr class="table-row">
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="table-head" colspan="2">Salary Cross Check</td>
        </tr>
        <tr class="table-row">
            <td>Last Salary</td>
            <td><?php if (!empty($last_salary->transaction_amount)){ ?>
                <?php echo Active_currency($last_salary->transaction_amount); ?></td>
        </tr>
        <tr class="table-row">
            <td>Last Arrears</td>
            <td><?php echo Active_currency($last_salary->arears); ?></td>
        </tr>
        <tr class="table-row">
            <td><b>Date</b></td>
            <td><?php echo $last_salary->month . "&nbsp;" . $last_salary->year; ?>
                <?php } else {
                    echo "<span style='color:red'>No previous salary record !</span>";
                } ?></td>
        </tr>
        <?php if(isset($AbsentDays) && !empty($AbsentDays)){?>
        <tr>
            <td class="table-head" colspan="2">Employee Absentee History</td>
        </tr>
        <tr class="table-row">
            <td>Absent On :</td>
            <td><?=$AbsentDays.' (<strong>'.$TotalAbsentDays.' Days</strong>)'?></td>
        </tr>
        <tr class="table-row">
            <td title="Subtract From Salary">Deductable Amount ( - ) :</td>
            <td id="deductableAmount"><?=$AmountToDeduct?></td>
        </tr>
        <?php } ?>
        </tbody>
    </table>

</div>

</div>

<!-- button group -->
</div>
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/payroll_left_nav'); ?>
</div>
<!-- contents -->
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200});
    $('#close-panel-bt').click(function () {
        $.panelslider.close();
    });
</script>
 <script>
     $(document).ready(function(){


         ///Need a Little Fix.
         if($('#AttendanceDeduction').is(':checked')){
             if($("#deductableAmount").length == 0) {
                 //it doesn't exist
             }else{
                 var deductableAmount = $('#deductableAmount').text();
                 var totalAmount = $('#p').val();

                 //Show Deducted Amount
                 var deductedAmount = parseInt(totalAmount) - parseInt(deductableAmount);
                 $('#p').val(deductedAmount);
                 $('#pay').text(deductedAmount);
             }
         }

         $("#AttendanceDeduction").on('click',function(e){

             //e.preventDefault();
             //var formData = $('#applicantLoginForm').serialize();
             $.ajax({
                 url: "<?php echo base_url()?>payroll/CheckAttemdanceType",
                 //data:formData,
                 type:"POST",
                 success: function (output) {
                     var deductableAmount = $('#deductableAmount').text();
                     var totalAmount = $('#p').val();
                    if($('#AttendanceDeduction').is(':checked')){
                        var deductedAmount = parseInt(totalAmount) - parseInt(deductableAmount);
                        $('#p').val(deductedAmount);
                        $('#pay').text(deductedAmount);
                    }else{
                        var addedAmount = parseInt(totalAmount) + parseInt(deductableAmount);
                        $('#p').val(addedAmount);
                        $('#pay').text(addedAmount);
                    }
                     console.log('totalAmount is: '+totalAmount);
                     console.log('deductable Amount is: '+deductableAmount);
                     console.log(deductableAmount);
                 }
             });
         });
     });

 </script>