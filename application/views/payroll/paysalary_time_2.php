<style type="text/css">

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
<link href="assets/js/data-tables/css/dataTables.tableTools.css" type="text/css" />
<link href="assets/js/data-tables/css/jquery.dataTables.css" type="text/css" />
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="application/javascript" src="assets/js/data-tables/dataTables.tableTools.js"></script>
<script type="text/javascript">
$.fn.dataTable.TableTools.buttons.copy_to_div = $.extend(
    true,
    $.fn.dataTable.TableTools.buttonBase,
    {
        "sNewLine":    "<br>",
        "sButtonText": "Copy to element",
        "target":      "",
        "fnClick": function( button, conf ) {
            $(conf.target).html( this.fnGetTableData(conf) );
        }
    }
);
$(document).ready(function() {
	
    var oTable =$('#example').DataTable( {
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sRowSelect": "multi",
            "aButtons": [ "select_all", "select_none" ],
			"aButtons": [ {
                "sExtends":    "copy_to_div",
                "sButtonText": "Copy to div",
                "target":      "#copy"
            } ]
        },
		"sAjaxSource": window.location+"/list",
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"job_title","value":$('#job_title').val()});
			aoData.push({"name":"designation","value":$('#designation').val()});
		        aoData.push({"name":"category_id","value":$('#category_id').val()});
                
		}
    } );
} );


$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
	<!--pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
	
function validate() 
{
if( document.form1.month.value == "-1")
   {
     alert( "Please select Month ! " );
     return false;
   }


 if(document.form1.year.value == "-1")
   {
     alert( "Please select Year ! " );
     return false;
   }
}
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Pay Salary Time</div> <!-- bredcrumb -->

	 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		

		<div class="head">Pay Salary Date/Month/Year</div>

			<div >
			<form action="payroll/pay_salary" method="post" name="form1" onsubmit="return validate(this);">
				<!-- <div class="row1">
					<h4><b>Employee Information</b></h4>
				</div>
			 	 -->
				<div class="row1">
					<h4>Month</h4>
					<select name="month">
                   	<option value="-1" selected="selected">-- Select Month Salary --</option>
                    <?php foreach($months as $mon):?>
						<option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<br class="clear">
				<div class="row1">
					<h4>Year</h4>
					<select name="year" required="required">
                   	<option value="-1" selected="selected">-- Select Year --</option>
                    <?php foreach($years as $yer):?>
						<option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                        <?php endforeach;?>
					</select>
				</div>
			
           
			<table cellspacing="0" id="example" class="display">
				<thead class="table-head">
					<td></td>
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Base Salary</td>
                    <td>Allowances</td>
					<td>Deduction</td>
                    <td>Loan/Advance</td>
                    <td>Expenses</td>
					<td>Payable</td>
					<!--<td>Status</td> 
					<td align="center"><span class="fa fa-pencil"></span></td>
					<td align="center"><span class="fa fa-eye"></span></td>
					<td><span class="fa fa-print"></span></td> -->
					<!--<td>Pay Salary</td>-->
				</thead>
				
			</table>
            <div class="row">
				<div class="button-group">
					<input type="submit" name="submitz" value="Submit" class="btn green"  />
				</div>
                </div>
            
            </form>
            </div>
			<!-- button group -->
			
			

		</div>

	</div>
<!-- contents -->

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->



