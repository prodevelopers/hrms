 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Allowances / Edit Allowances</div>
    
	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Edit Allowances</div>

			

			<form role="form" method="post" action="payroll/updte_allowance_record">
				<div class="row">
					<h4>Employee Name</h4>
   <input name="name"  type="text"  value="<?php echo $info->full_name; ?>" readonly="readonly">
   <input type="hidden" name="alwnc_id" id="alwnc_id" value="<?php echo $info->allowance_id; ?>" />
   </div>
				<br class="clear">
				<div class="row">
					<h4>Allowance Type</h4>
      				 <?php
					 $selected =(isset($info->ml_allowance_type_id) ? $info->ml_allowance_type_id:'');
					 echo form_dropdown('allowance_type',$allowance,$selected)?>
				</div>
                <br class="clear">
				<div class="row">
					<h4>Pay Frequency</h4>
					<?php
					 $selected1 =(isset($info->ml_pay_frequency_id) ? $info->ml_pay_frequency_id:'');
					 echo form_dropdown('pay_frequency',$frequency,$selected1)?>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Amount</h4>
					<input type="text" name="amount" value="<?php echo $info->allowance_amount; ?>" />
				</div>
				<br class="clear">
				<div class="row">
					<h4>Date</h4>
					<input type="text" name="aldate" value="<?php echo $info->effective_date; ?>">
				</div>
                <br class="clear">
                <div class="row">
					<h4>Month</h4>
					<select name="month" disabled="disabled">
                   	<option value="-1" >-- Select Month Salary --</option>
                    <option value="<?php echo $info->ml_month_id;?>" selected="selected"><?php echo $info->month;?></option>
                    <?php 
					 
					foreach($months as $mon):?>
                    
						<option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Year</h4>
					<select name="year" disabled="disabled">
                   	<option value="-1" >-- Select Year --</option>
                    <option value="<?php echo $info->ml_year_id;?>" selected="selected"><?php echo $info->year;?></option>
                    <?php 
					
					foreach($years as $yer):?>
						<option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                        <?php endforeach;?>
					</select>
				</div>
                
                <br class="clear">
				                
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<input type="submit" class="btn green" name="add_rec" value="Update Allowance">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		<?php //endforeach?>
        </form>

	</div>
    </div>
<!-- contents -->

</body>
</html>