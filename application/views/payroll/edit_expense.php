 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 float:left;
 margin-left: 210px;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
 float:right;

}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
}
</style>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense Management / Edit Expense</div> <!-- bredcrumb -->


<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
  
 	<div class="right-contents">

		<div class="head">Update Expense</div>
			<form action="payroll/updte_expense_records" method="post">
            
				<div class="row">
					<h4>Employee Name</h4>
   <input name="name"  type="text"  value="<?php echo $row->full_name; ?>" readonly="readonly">
   <input type="hidden" name="exp_id" id="exp_id" value="<?php echo $row->expense_claim_id; ?>" />
   </div>
				<br class="clear">
				
				<div class="row"> 
					<h4>Expense Type</h4>
					<?php 
					 $selected =(isset($row->expense_type) ? $row->expense_type:'');
					echo form_dropdown('expense_type',$expense,$selected)?>
				</div>
                <script type="text/jscript">
                $(function(){$("#expdate").datepicker({dateFormat:'yy-mm-dd'})});
                </script>
                <br class="clear">
				<div class="row">
					<h4>Expense date</h4>
					<input type="text" name="expdate" id="expensedate" value="<?php echo date_format_helper($row->expense_date); ?>"/><span><img src="assets/icons/data.png" /></span>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Ammount</h4>
					<input type="text" name="expamount" id="expamount" value="<?php echo $row->amount; ?>" />
				</div>
                <br class="clear">
				<!--<div class="row">
				<h4>Status</h4>
					<?php 
					//$selected =(isset($row->status) ? $row->status:'');
					//echo form_dropdown('status',$status,$selected)?>
				</div>-->
                <br class="clear">
				
				<div class="row">
				<div class="button-group">
					<input type="submit" value="Update Expense" class="btn green">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
          
			</form>

			<!-- button group -->
			

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $( "#expensedate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });
    </script>
    <!-- leftside menu end -->