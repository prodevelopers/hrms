<?php
if(isset($SelectedCheckBoxes) and isset($ReportTableData)){
    if(strpos($SelectedCheckBoxes,'Salaries') !== false){
        $salaries = true;
    }else{
        $salaries = false;
    }
    if(strpos($SelectedCheckBoxes,'Allowances') !== false){
        $allowances = true;
    }else{
        $allowances = false;
    }
    if(strpos($SelectedCheckBoxes,'Expenses') !== false){
        $expenses = true;
    }else{
        $expenses = false;
    }
    if(strpos($SelectedCheckBoxes,'Deductions') !== false){
        $deductions = true;
    }else{
        $deductions = false;
    }
}
?>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">
        <button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#myModal">
            Send
        </button>
        <h4 class="text-center"><?php echo isset($reportHeading)?$reportHeading:'Generate EmployeesWise Payroll Report'; ?></h4>
        <?php
        //This Section Will Execute If Employee Filter is Selected
        if (isset($employeeInfo) && !empty($employeeInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>EmployeeName</th>
                    <th>Employee Code</th>
                    <th>Designation</th>
                    <th>Monthly Salary</th>
                    <th>Salary Month and Year</th>
                </tr>
                <?php
                echo "<tr><td>" . $employeeInfo->EmployeeName . "</td><td>" . $employeeInfo->EmployeeCode . "</td><td>" . $employeeInfo->Designation . "</td><td>" . $employeeInfo->Monthly_Salary. "</td><td>" . $filteredYear->Salary_Month_and_Year."</td></tr>";
                ?>
            </table>
        <?php } ?>

        <?php
        //This Section will execute if the Project filter is selected.
        if (isset($projectInfo) && !empty($projectInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>Project Name/Abbreviation</th>
                    <th>Days Worked</th>
                    <th>% Charges</th>
                    <th>Payable Amount</th>
                </tr>
                <?php
                echo "<tr><td>" . $projectInfo->projectTitle . "</td><td>" . $projectInfo->ProjectAbbreviation . "</td><td>" . $filteredYear . "</td>
                <td>" . $filteredYear . "</td></tr>";
                ?>
            </table>
        <?php } ?>

        <?php
        //This Section will execute if the department filter is selected.
        if (isset($departmentInfo) && !empty($departmentInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>Department</th>
                    <th>Year</th>
                </tr>
                <?php
                echo "<tr><td>" . $departmentInfo->DepartmentName . "</td><td>" . $filteredYear . "</td></tr>";
                ?>
            </table>
        <?php } ?>

        <table class="table table-hover table-condensed table-bordered">

            <thead>
            <tr>
            <th>Emp Code</th>
            <?php /*echo $salaries? "<th>Salaries</th>":'' */?><!--
            <?php /*echo $allowances? "<th>Allowances</th>":'' */?>
            <?php /*echo $expenses? "<th>Expenses</th>":'' */?>
            --><?php /*echo $deductions? "<th>Deductions</th>":'' */?>
            <th>Name</th>
                <th>Designation</th>
                <th>Monthly Salary</th>
                <th>% Charges</th>
                <th>No of Days</th>
                <th>Payable Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($ReportTableData) && is_array($ReportTableData)){
                foreach($ReportTableData as $key=>$val){
                    echo "<tr><td>".$key."</td>";
                    echo $salaries? "<td>".(isset($val['Salaries'])?$val['Salaries']:'-')."</td>":'';
                    echo $allowances? "<td>".(isset($val['Allowances'])?$val['Allowances']:'-')."</td>":'';
                    echo $expenses? "<td>".(isset($val['Expenses'])?$val['Expenses']:'-')."</td>":'';
                    echo $deductions? "<td>".(isset($val['Deductions'])?$val['Deductions']:'-')."</td>":'';
                    //Lets Show The Row Total At The End..
                    //echo "<td>Total Here</td>";
                   echo "<td>";
                    $total = 0;
                    if($salaries){
                        $salariesRowTotal = isset($val['Salaries'])?$val['Salaries']:0;
                        $total += $salariesRowTotal;
                    }
                    if($allowances){
                        $allowancesRowTotal = isset($val['Allowances'])?$val['Allowances']:0;
                        $total += $allowancesRowTotal;
                    }
                    if($expenses){
                        $expensesRowTotal = isset($val['Expenses'])?$val['Expenses']:0;
                        $total += $expensesRowTotal;
                    }
                    if($deductions){
                        $deductionsRowTotal = isset($val['Deductions'])?intval($val['Deductions']):0;
                        $total -= $deductionsRowTotal;
                    }
                    echo $total;
                    echo "</td></tr>";
                }
                echo "<tr><td><b>Total</b></td>";
                $superTotal = 0;
                if($salaries){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Salaries'])?$item['Salaries']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($allowances){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Allowances'])?$item['Allowances']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($expenses){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Expenses'])?$item['Expenses']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($deductions){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Deductions'])?$item['Deductions']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal -= $sum;
                }
                echo "<td><b>GrandTotal : ".$superTotal."</b></td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
    $(document).ready(function(){
        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>payroll/sendGeneratedPDFReport';
            $.ajax({
               url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>