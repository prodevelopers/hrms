<style type="text/css">

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
#Select All checkbox{
	float:right;
}
.dataTables_length{
	position: relative;
	top: -40px;
	font-size: 1px;
	left: 1%;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -41px;
left: -11.9em;
}
.dataTables_filter input{
	width: 180px;
}
.table_style
{
	width:890px;
	margin-left:0; 
}                       
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="application/javascript" src="assets/js/data-tables/dataTables.tableTools.js"></script>
<script type="text/javascript">
/*$.fn.dataTable.TableTools.buttons.copy_to_div = $.extend(
    true,
    $.fn.dataTable.TableTools.buttonBase,
    {
        "sNewLine":    "<br>",
        "sButtonText": "Copy to element",
        "target":      "",
        "fnClick": function( button, conf ) {
            $(conf.target).html( this.fnGetTableData(conf) );
        }
    }
);*/
$(document).ready(function() {
	$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});

} );


/*$(".filter").change(function(e) {
    oTable.fnDraw();
});*/

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
   pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
	
function validate() 
{
if( document.form1.month.value == "-1")
   {
     alert( "Please select Month ! " );
     return false;
   }


 if(document.form1.year.value == "-1")
   {
     alert( "Please select Year ! " );
     return false;
   }
}
///////////
    function do_this(){

        var checkboxes = document.getElementsByName('check[]');
        var button = document.getElementById('toggle');

        if(button.value == 'select'){
            for (var i in checkboxes){
                checkboxes[i].checked = 'FALSE';
            }
            button.value = 'deselect All checkbox'
        }else{
            for (var i in checkboxes){
                checkboxes[i].checked = '';
            }
            button.value = 'Select All checkbox';
        }
    }
///////////
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Salary Review Sheet</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		

		<div class="head">Payroll Sheet</div>
       <form action="" method="post">
       <!--<input type="text" name="emp_name" placeholder="Employee Name" style="width:22%" onchange="this.form.submit()">-->
        </form>
			<!--<form action="payroll/pay_salary" method="post" name="form1" onsubmit="return validate(this);">-->
				<form action="payroll/salary_sheet" method="post" name="form1">
                
				<div class="row">
					<h4>Month</h4>
					<select name="month" id="month" class="resize" required="required">
                   	<option value="" selected="selected">-- Select Month Salary --</option>
                    <?php foreach($months as $mon):?>
						<option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>	
				<br class="clear">
				<div class="row">
					<h4>Year</h4>
					<select name="year" required="required" class="resize" style="width:200px;" id="year" onchange="this.form.submit();">
                   	<option value="" selected="selected">-- Select Year --</option>
                    <?php foreach($years as $yer):?>
						<option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                        <?php endforeach;?>
					</select>
                     
                    </form>
                    
            </div>
		</div>

	</div>
<!-- contents -->

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e)
    {
        $('#month,#year').select2();
    })
    </script>
    <!-- leftside menu end -->
