<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete_paysalary/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name,base_salary,salary_id) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	$('#base_salary').val(base_salary);
	$('#base_salaryli').append(base_salary);
	$('#salary_id').val(salary_id);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Pay Salary Transaction</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Salary Transaction</div>

			
			<!-- hide/show -->
			
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
			<div class="form-left">
			<form action="payroll/pay_salary_transaction" method="post">
            
				
            <br class="clear">
				<div class="row1">
					<h4>Name</h4>
					 <input name="name" id="id_input"  type="text"  value="<?php echo $info->full_name;?>" readonly="readonly">
   <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $info->employee_id;?>"  />
   <input type="hidden" name="salary_payment_id" id="salary_payment_id" value="<?php echo $info->salary_payment_id;?>" />
   <input type="hidden" name="month" value="<?php echo $info->salary_month;?>" />
   <input type="hidden" name="year" value="<?php echo $info->salary_year;?>" />
                    
                <br class="clear">
				<div class="row1">
					<h4>Base Salary</h4>
<input type="text" name="base_salary" id="base_salary" value="<?php echo $info->base_salary;?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row2">

					<h4>Deductions</h4>		
<!-- <input type="text" name="deduction_type" value="<?php //echo $info->deduction_type;?>" readonly style="width:120px;">-->
         		  <?php
				   	$ded_month=$info->ded_month;
					$ded_year=$info->ded_year;
					$salary_month=$info->salary_month;
					$salary_year=$info->salary_year;
					$pay_requency=$info->deduction_frequency;
					if($pay_requency = 1)
					{$deduction_amount=$info->ded_amount;}
					elseif($salary_month == $ded_month && $salary_year == $ded_year)
				 {$deduction_amount=$info->ded_amount;}else{$deduction_amount=0;}					 ?>
<input type="text" name="deduction_amount" value="<?php echo $deduction_amount;?>" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row2">

					<h4>Allowances</h4>
<!--<input type="text" name="allowance_type" value="<?php //echo $info->allowance_type;?>" readonly style="width:120px;">-->
                     <?php
				   	$alwnc_month=$info->alwnc_month;
					$alwnc_year=$info->alwnc_year;
					$salary_month=$info->salary_month;
					$salary_year=$info->salary_year;
					$allowance_freq=$info->pay_frequency;
					if($allowance_freq=1)
					{$alwnc_amount=$info->allowance_amnt;}
				elseif($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$info->allowance_amnt;}else{$alwnc_amount=0;} 
					 ?>
	<input type="text" name="allowance_amount" value="<?php echo $alwnc_amount;?>" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row2">
					<h4>Expenses</h4>
<!--<input type="text" name="expense_type" value="<?php //echo $info->expense_type_title;?>" readonly style="width:120px;">-->
                    <?php
				    $exp_id=$info->expense_claim_id;
					$exp_dis_id=$info->expense_id;
					$salary_month=$info->salary_month;
					$salary_year=$info->salary_year;
					$exp_month=$info->exp_month;
					$exp_year=$info->exp_year;
					if($exp_id == $exp_dis_id)
					{$expense_amount=0;}
					elseif($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$info->exp_amount;}else{$expense_amount=0;}
					 ?>
	<input type="text" name="expense_amount" value="<?php echo $expense_amount;?>" readonly style="width:113px;">

				</div>
                <br class="clear">
				<div class="row2">
					<h4>Loan/Advances</h4>
<!--<input type="text" name="expense_type" value="<?php //echo $info->payment_type_title;?>" readonly style="width:120px;">-->
                   <?php
					$loan_advance_id=$info->loan_advance_id;
					$loan_payback_id=$info->loan_payback_id;
				   	$lonad_month=$info->alonad_month;
					$lonad_year=$info->alonad_year;
					$salary_month=$info->salary_month;
					$salary_year=$info->salary_year;
					if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					else
					if($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$info->loan_amount;}else{$loan_advance=0;} 
					 ?>
		<input type="text" name="expense_amount" value="<?php echo $loan_advance;?>" readonly style="width:113px;">

				</div>
				
				<br class="clear">
				<div class="row1">
               <?php
					$base_salary=$info->base_salary;
					$deduction_amount;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					 ?>
					<h4>Payable Ammount</h4>
		<input type="text" name="pay_amount" id="pay_amount" value="<?php echo $payable;?>" readonly="readonly">
				</div>
                
                <!--<script type="text/javascript">
				function payment(){
                var salary_field=this['base_salary'].value;
				var paym_amount=this['pay_amount'].value;
				var remain=(salary_field) - (paym_amount);
				$('#remain_amount').val(remain);
				}
				
                </script>-->
				
				<br class="clear">
				<br class="clear">
				<div class="row2">
            		<h4>Payment Mode</h4>
            		<?php echo form_dropdown('payment_mode',$payment_mode,'class="se"','id="selected"')?>
            	</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="no_cheque">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_cheque">
            	</div>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="recieved">
                </div>
				<div class="row1 cash">
					<h4>Remarks</h4>
					<input type="text" name="remarks">
				</div>
				<div class="row">
				<div class="button-group">
					<input type="submit" name="submit" class="btn green" value="Pay Salary Transaction" />
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
                    <div class="head" >Information</div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Code</h5>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">CNIC</h5>
                    	<i class="italica" id="cnic"></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Department</h5>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">position</h5>
                    	<i class="italica" id="position"></i>
				</div>
                <div class="row2">
                    	<h5 class="headingfive">Base Salary</h5>
                    	<i class="italica" id="base_salaryli"></i>
				</div>
				</div>
            </div>
			<!-- button group -->
			

		

	</div>
<!-- contents -->