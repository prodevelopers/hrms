<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<!--<script src="<?php /*echo base_url()*/?>assets/js/bootstrap.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/css/bootstrap.css" type="text/css"/>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<style>
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
    .sendbtn{
        margin-right: 5px;
        float:right;
        display: inline-block;
        padding: 3px 12px;
        margin-bottom: 0px;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -moz-user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;}
</style>

<style>
  #pagination
  {
      float:left;
      padding:5px;
      margin-top:15px;
  }
#pagination a
{
    padding:5px;
    background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
    color:#5D5D5E;
    font-size:14px;
    text-decoration:none;
    border-radius:5px;
    border:1px solid #CCC;
}
#pagination a:hover
{
    border:1px solid #666;
}
.paginate_button
{
    padding: 6px;
    background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
    color: #5D5D5E;
    font-size: 14px;
    text-decoration: none;
    border-radius: 5px;
    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    border: 1px solid #CCC;
    margin: 1px;
    cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;

}


  .btn-proll{
      width: 67px;
      padding: 5px;
      background: #6AAD6A;
      margin-top: -38px;
      margin-right: -180px;}
  .btn-proll-csv{
      width: 67px;
      /* height: 25px; */
      background: #6AAD6A;
      margin-top: -38px;
      margin-right: -255px;
      float: right;
      padding: 5px;
  color:white;}
    .pdf-btn{
        background: #6AAD6A;
        padding: 5px;
        float:left;
        color:white;}
  .select2-results li{
      width: 300px;
      border-bottom:thin solid #ccc;
      height:60px;
  }
  .select2TemplateImg { padding:0.2em 0;}
  .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
  .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
  .resize{width: 220px;}

</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>

<div class="contents-container">
	<div class="bredcrumb">Dashboard / Staff Salary</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Staff Salary Report</div>
             <div class="filter">
     <h5> Filter By: </h5>
    <div class="row">
    <form action="payroll/staff_salary"  method="post" id="print_staff_salary">
        <input type="hidden"  name="name"  id="selectEmployee" onchange="this.form.submit();"/>
    <select name="project" style="width:220px;" id="project" onchange="this.form.submit();">
    <option value="">-- Select Project --</option>
    <?php if(!empty($projects)){
		foreach($projects as $project){?>
    <option value="<?php echo $project->project_id;?>"><?php echo $project->project_title;?></option>
    <?php }}else{echo "";}?>
    </select>
        <div class="btns" style="float:right;width:610px;height:40px;">
		<input type="hidden" name="projectSelected" id="projectSelected" value="<?php echo $proj;?>">
		<span class="fa fa-print move fa-2x" style="float:left;cursor: pointer; margin-right: 7px;" title="Print All" id="print"></span>
        <?php if(!empty($info)){?>
        <button type="button" class="sendbtn pdf-btn" id="pdfButton">&nbsp;&nbsp;PDF&nbsp;&nbsp;</button></form></div><?php }else{?>
                     <button type="button" class="green" id="pdfButton" disabled="disabled" style="background-color: darkgray;">&nbsp;&nbsp;PDF&nbsp;&nbsp;</button><?php }?>
       <!-- <button type="button" id="sendMail" class="green" data-toggle="modal" data-target="#myModal">Send</button>-->
        <?php if(!empty($info)){?>
            <a href="#loginmodal"  class="btn green  sendbtn btn-proll" id="modaltrigger"  title="Click to Send Email with PDF Attachment !">Send Email</a><?php }else{?>
        <a href="#loginmodal" class="btn green" id="modaltrigger"  title="Click to Send Email with PDF Attachment !" onclick="return false;" style="background-color: darkgray;">Send Email</a><?php }?>
        <?php if(!empty($info)){?> <a href="<?php echo base_url().@$files;?>"> <button type="button" class="sendbtn btn-proll-csv " id="csvButton">&nbsp;&nbsp;CSV&nbsp;&nbsp;</button></a><?php }else{?>
        <a href="<?php echo base_url().@$files;?>"> <button type="button" class="green" id="csvButton" disabled="disabled" style="background-color: darkgray">&nbsp;&nbsp;CSV&nbsp;&nbsp;</button></a><?php }?>

                 </form></div>


        <!----------------------------Send Email Without Bootstrap libraries----------------------------->
        <div id="loginmodal" class="modal-demo" style="display: none;" >
            <div class="top_header">
                <button type="button" class="close" onclick="Custombox.close();">
                    <span>&times;</span><span class="sr-only"></span>
                </button>
                <h3 class="title">Email To</h3>
            </div>
            <form class="form-horizontal" id="mailForm">

                <div class="pop_row">
                    <div class="left_side">
                        <h5>To</h5>
                    </div>
                    <div class="right_side">
                        <input type="text" class="right_txt" name="ToEmail" id="inputEmail3" placeholder="Email" style="width:240px;height:35px;">
                    </div>
                </div>

                <div class="pop_row">
                    <div class="left_side">
                        <h5>Reply To</h5>
                    </div>
                    <div class="right_side">
                        <input type="text" class="right_txt" name="FromEmail" placeholder="Reply To Email (optional)" style="width:240px;height:35px;">
                    </div>
                </div>

                <div class="pop_row">
                    <div class="left_side">
                        <h5>Subject</h5>
                    </div>
                    <div class="right_side">
                        <input type="text" class="right_txt" value="Attached Generated Payroll PDF" name="MessageSubject" style="width:240px;height:35px;">
                    </div>
                </div>

                <div class="pop_row_txtarea">
                    <div class="left_side">
                        <h5>Message</h5>
                    </div>
                    <div class="right_side">
                        <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
                    </div>
                </div>

                <div class="pop_row">
                    <div class="left_side">
                        <h5>Attachment</h5>
                    </div>
                    <div class="right_side">
                        <label  class="label-attac">work.pdf</label>
                    </div>
                </div>


                <div class="btn-row">
                    <input type="button" class="btn-pop" id="sendMailWithAttachment" value="Send">
                </div>
            </form>

        </div>

        <!------------------Script of the Email Btn without Libraies of Bootstrap-------->
        <script>
            $('#modaltrigger').on('click', function(e){
                Custombox.open({
                    target: '#loginmodal',
                    effect: 'fadein'
                });
                e.preventDefault();
            });
        </script>

                 <style>
                     .send{float:right;background-color:  #6AAD6A;border-radius:7px;color:#ffffff}
                     .close{float:right;background-color:#d3d3d3;border-radius:7px;color:#000000}
                 </style>

             </div>
			<div class="table-responsive">
            <div id="print_table">
            <table class="mytab slect" id="table_lists" cellspacing="0" style=" margin-left: 35px;width: 94%;">
                   <thead class="table-head">
					<td>S.No</td>
                    <td>Name</td>
					<td>Designation</td>
					<td>Project</td>
					<td>Job Grade</td>
					<td>Salary Package</td>
				</thead>
                <?php if(!empty($info)){
					$no=1;
					foreach($info as $record){
						$count=count($info);
					?>
                <tr class="table-row">
                    <td><?php echo $no;?></td>
                    <td><?php echo $record->full_name;?></td>
					<td><?php echo $record->designation_name;?></td>
					<td><?php echo $record->project_title;?></td>
					<td><?php echo $record->pay_grade;?></td>
					<td><?php echo Active_currency($record->base_salary);?></td>
				</tr>
                <?php $no++;}}else{echo "<span style='color:red'>Sorry no record found!</span>";}?>
                 <tbody>
                    </tbody>
        	 </table>
             
         </div>
			</div>
    </div>

              <div><?php echo @$links;
              /*if(@$rows==@$count)
				  { echo @$links;}
				  if($page == 0){}
				  else{echo @$links;}*/?>
			  </div>
			</div>
          
		</div>

	</div>
<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
	$('#print').click(function(){
		className:"blue-with-image",
			$.loader({content:getsupport_print()});
		//$.loader('setContent','');
	});
	function getsupport_print()
	{
		var formData = $("#print_staff_salary").serialize();
		formData=formData;
		var data={
			formData:formData
		};
		//document.print_sheet.submit();
		$.redirect('<?php echo base_url(); ?>payroll/staff_salary',data,'POST');
	}
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    $(document).ready(function(e)
    {
        $('#project').select2();
    })
    </script>
    <!-- leftside menu end -->
<script>
    $(document).ready(function(){
        $("#pdfButton").on('click', function(){
            var targetURL = '<?php echo base_url();?>payroll/downloadPdfSalaryStaffReport';
            <?php if(isset($view) && !empty($view)){ ?>
            var postData = [];
            postData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            console.log(postData);
            $.ajax({
                url: targetURL,
                data:postData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "Download"){
                        //console.log(data[1]);
                        //document.location = <?php //echo base_url();?>data[1];
                    }
                }
            });
            <?php } ?>
        });});

    $('#sendMailWithAttachment').on('click', function (e) {
        //console.log('working'); return;
        var formData = $('#mailForm').serializeArray();
        <?php if(isset($view) && !empty($view)){ ?>
        formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
        <?php } ?>
        var targetURL = '<?php echo base_url();?>payroll/sendGeneratedSlaryStaffPDFReport';
        $.ajax({
            url: targetURL,
            data:formData,
            type:"POST",
            success: function (output) {
                var data = output.split("::");
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    }); //End Of On Click Function
</script>
<!---------------Model Javascript--------------------------->
<script type="text/javascript">
    $(function(){
        $('#loginform').submit(function(e){
            return false;
        });

        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".close" });
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".btn-default" });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        /* New Select Employee
         * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
         * */
        var selectEmployeeSelector = $('#selectEmployee');
        var url = "<?php echo base_url(); ?>payroll/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar: 'EmployeeAvatar',
            employeeCode: 'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";

        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector, url, tDataValues, minInputLength, placeholder, baseURL, templateLayout);
        $('.select2-container').css("width", "30%");

    });
</script>
<!--------------------------End Model--------------------------->