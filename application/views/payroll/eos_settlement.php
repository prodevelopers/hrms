
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Advance Salary / EOS Settlement</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
	

	<div class="right-contents">

		<div class="head"> EOS Settlement</div>

			

			<script type="text/javascript">
			$(document).ready(function(){
			    $(".pension-pay").hide();
			    $('select').click(function(){
			        if($(this).attr("value")=="pension"){
			            $(".pension-pay").toggle();
			            $(".empty").hide();
			        }
			        if($(this).attr("value")==""){
			            $(".pension-pay").hide();
			            $(".empty").toggle();
			        }
			    });	
			});
			</script>
<div class="form-left">
			<form>
            <div class="row2">
					<h4>Employee Name</h4>
					<input type="text"  name="" id="" value="">
				</div>
				<div class="row2">
					<h4>Pension Fund</h4>
					<input type="text" disabled>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Pension Payout</h4>
					<select style="float: left; width: 140px;">
						<option selected value="empty">Select Pension</option>
						<option value="pension">Pay</option>
					</select>
					<input type="text" class="pension-pay" style="float: left; width: 105px;">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>EOS Settlement Payment Type:</h4>
					<input type="text" disabled>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>On Exit Additonal Salary</h4>
					<input type="text" disabled>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Total Payment</h4>
					<input type="text" disabled>
				</div>
			</form>
</div>

<div class="form-right">
	<div class="head">Information</div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Name</h5>
                    	<i class="italica">Employee Name</i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">Designation</h5>
                    	<i class="italica">Developer</i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Base Salary</h5>
                    	<i class="italica">10000/-</i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">Advanced Amount</h5>
                    	<i class="italica">RS.6000/-</i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">Advanced Type</h5>
                    	<i class="italica">Incremented</i>
				</div>
</div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="#"><button class="btn green">Pay Now</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->

