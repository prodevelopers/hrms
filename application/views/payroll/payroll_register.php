<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_wrapper{
	width: 100%;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px 10px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	top: -48px;
	left: 49%;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
	top: -49px;
	left: -19%;
}
.dataTables_filter input{
	width: 180px;
	
}
input[name="name"]{
	width: 22%;
}
select[id="month"],select[id="year"]{
	width: 22%;
}
.move{
	position: relative;
	left: 43%;
	top: 45px;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {/*
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"oLanguage": {
         "sProcessing": "<img src='assets/images/ajax-loader.gif'>"
        },  
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		        aoData.push({"name":"category_id","value":$('#category_id').val()});
                
		}
                
	*/});
});

/*$(".filter").change(function(e) {
    oTable.fnDraw();
});
*/
function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
	pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll</div> <!-- bredcrumb -->
	<?php //foreach($months_list as $mon):?>
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">

		<div class="head">Payroll Register</div>

			<!-- filter -->
			<div class="filter">
             <div class="row">
				<h4>Filter By</h4>
               <?php echo @form_open('payroll/payroll_register');?>
				<?php echo @form_dropdown('name',$employees_name,$name,"class='resize' id='name' onchange='this.form.submit()'")?>
				<?php echo @form_dropdown('month',$month,$slct_m,"class='resize' id='month' onchange='this.form.submit()'")?>
           	    <?php echo @form_dropdown('year',$year,$slct_y,"class='resize' id='year' onchange='this.form.submit()'")?>
             </div>
                <?php echo @form_close();?>
             &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;<span class="fa fa-print" style="cursor:pointer" title="Print All" onclick="print_all()"></span>    
			</div>
				 
			<div class="table-responsive">
            <div id="print_table">
			<!-- table -->
            <table class="mytab slect" id="table_lists" cellspacing="0">
                   <thead class="table-head">
					<td>Code</td>
                    <td>Name</td>
					<!--<td>Designation</td>-->
					<td>Base Salary </td>
                    <td>Month / Year </td>
					<!--<td>Allowances</td>
					<td>Deductions</td>
                    <td>Expenses</td>
                    <td>Loan/Advance</td>-->
					<td>Paid Salary </td>
                    <!--<td>Transaction</td>-->
                    <td>Transaction Date</td>
                    
					<td><span class="fa fa-print"></span>&nbsp; | &nbsp;<span class="fa fa-eye"></span></td>
					<td><span class="fa fa-ticket"></span></td>
				</thead>
                <?php if(!empty($msg)){echo $msg;} 
				elseif(!empty($info)){ 
				foreach($info as $rec){?>
                <tr class="table-row">
                    <td><?php echo @$rec->employee_code; ?> </td>
                    <td><?php echo @$rec->full_name; ?> </td>
					<!--<td>Designation</td>-->
					<td><?php echo Active_currency($rec->base_salary); ?></td>
                    <td><?php echo @$rec->sal_month." / ".$rec->sal_year; ?></td>


					<td><?php echo Active_currency($rec->paid_salary);?></td>
                    <!--<td><?php //echo @$rec->calender_month; ?></td>-->
                    <td height="21"><?php 
				$date=$rec->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
                    <!-- Need to change-->
					<td><a href="payroll/print_payroll_reg/<?php echo $rec->employee_id;?>/<?php echo $rec->salary_month;?>/<?php echo $rec->salary_year;?>"><span class="fa fa-print" style="cursor:pointer"></span></a>&nbsp; | &nbsp;
                   <a href="payroll/payroll_reg_detail/<?php echo $rec->employee_id;?>/<?php echo $rec->salary_month;?>/<?php echo $rec->salary_year;?>"><span class="fa fa-eye">
                    </span></a></td>
                    <td><a href="payroll/payroll_reg_slip/<?php echo $rec->employee_id;?>/<?php echo $rec->salary_month;?>/<?php echo $rec->salary_year;?>"><span class="fa fa-ticket"></span></a></td>
				</tr>
                <?php }}else{ echo "<tr><span style='color:red'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sorry no Records Found !</span></tr>";}?>
                    <tbody>
                    </tbody>
        	 </table>
         </div>


			</div>
            
<div id="container">
        <ul>
        <?php echo @$links;?>
        </ul>
        </div>
			</div>
			
		</div>

	</div>
	<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Print All</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body onload="css()">');
	function css(){
		}
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}


$(document).ready(function(e){
    $('#month,#year,#name').select2();
})

</script>
