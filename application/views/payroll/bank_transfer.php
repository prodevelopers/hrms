<!-- contents -->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Bank Transfer File</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
 <script>
	   function getsupport()
		{
			 document.payroll_register.submit() ;
		}
    </script>
	<div class="right-contents">
		<div class="head">Bank Transfer File</div>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
              <form action="" method="post">
				<?php echo @form_dropdown('month',$month,$slct_m,"id='month' class='resize' style='width:160px;' onchange='this.form.submit()'")?>
           	   <?php echo @form_dropdown('year',$year,$slct_y,"id='year' class='resize' style='width:160px;' onchange='this.form.submit()'")?>
               
                </form>
				<div class="button-group">
                <form action="payroll/bank_transfer_invoice" method="post">
					<!--<a href="javascript: getsupport()">-->
                    <input type="hidden" id="month" name="month" value="<?php echo $slct_m; ?>" />
                    <input type="hidden" id="year" name="year" value="<?php echo $slct_y; ?>" />
                    <input type="submit" class="btn green" value="Transfer File" style="float:right; position:relative; top:-0.75em;">
                    </form>
				</div>
			</div>
			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td>S.NO</td>
					<td>Name</td>
                    <td>Bank&nbsp;</td>
                    <td>Branch</td>
                    <td>Branch Code</td>
					<td>Account No</td>
                    <td>Month</td>
					<td>Transaction Date</td>
					<td>Ammount</td>
				</thead>
                <?php if(!empty($info)){
					$no=1;
					foreach($info as $rec){
					 ?>
				<tr class="table-row">
					<td><?php echo $no;?></td>
					<td><?php echo $rec->full_name;?></td>
                    <td><?php echo $rec->bank_name;?>&nbsp;&nbsp;</td>
                    <td><?php echo $rec->branch;?></td>
                    <td><?php echo $rec->branch_code;?></td>
					<td><?php echo $rec->account_no;?></td>
                    <td><?php echo $rec->calender_month;?></td>
					<td><?php
				echo date_format_helper($rec->transaction_date);
				?></td>
					<td><?php echo Active_currency($rec->transaction_amount);?></td>
				</tr>
                <?php $no++;}}else{echo "<span style='color:red'> Sorry no Records Found !</span>";}?>
			</table> 
            <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#month,#year').select2();
    })
    </script>
    <!-- leftside menu end -->