 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		 document.getElementById('#employee_inf0').innerHTML = "";
    } else {
        $.post("payroll/autocomplete/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
                $('#autoSuggestionsList').html(data);
            }
        });
    }
}

function fill(thisValue,employee_code,cnic,id,position,department_name) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
}   


</script>
 
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Increments / Add Increments</div>
    
	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Add Increments</div>

		<div class="form-left">
			<form role="form" method="post" action="payroll/add_increments_record">
			<div class="row2">
					<h4>Employee Name</h4>
					<input name="name" id="id_input"  class="serch" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
                    <input type="hidden" name="emp_id" id="emp_id"  />
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
   
				</div>
       			<br class="clear">
				<div class="row2">
					<h4>Increment Type</h4>
					<?php echo form_dropdown('increment_type',$increment,'required="required"','required="required"')?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Increment Amount</h4>
					<input type="text" name="increment_amount" id="increment_amount" value="" required="required" />
				</div>
                <script type="text/jscript">
                $(function(){$("#increment_date").datepicker({dateFormat:'yy-mm-dd'})});
                </script>
				<br class="clear">
				<div class="row2">
					<h4>Date (Effective From)</h4>
					<input type="text" name="increment_date" id="increment_date" required="required"><span><img src="assets/icons/data.png" /></span>
				</div>
                 <br class="clear">
                <div class="row1">
					<h4>Month</h4>
					<select name="month">
                   	<option value="-1" selected="selected">-- Select Month Salary --</option>
                    <?php foreach($months as $mon):?>
						<option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Year</h4>
					<select name="year" required="required">
                   	<option value="-1" selected="selected">-- Select Year --</option>
                    <?php foreach($years as $yer):?>
						<option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<!--<br class="clear">
				<div class="row2">
					<h4>Status</h4>
					<?php //echo form_dropdown('status',$status,'required="required"','required="required"')?>
				</div>-->
				<div class="row2">
				<div class="button-group">
					<input type="submit" class="btn green" name="add_rec" value="Add Increment">
					
				</div>
                </div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Code</h5>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">CNIC</h5>
                    	<i class="italica" id="cnic"></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Department</h5>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">position</h5>
                    	<i class="italica" id="position"></i>
				</div>
			</div>
			<!-- button group
			<div class="row">
				<div class="button-group">
					<a href="#"><button class="btn green">Add Deductions</button></a>
					 <button class="btn red">Delete</button>
				</div>
			</div>

		</div> --> 
        
        
        
        
        
	</div>
          
    </div>
<!-- contents -->

</body>
</html>
