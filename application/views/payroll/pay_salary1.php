<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete_paysalary/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name,base_salary,salary_id) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	$('#base_salary').val(base_salary);
	$('#base_salaryli').append(base_salary);
	$('#salary_id').val(salary_id);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Pay Salary</div>

			
			<!-- hide/show -->
			
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
			<div class="form-left">
			<form action="payroll/pay_salary_amount" method="post">
				<div class="row1">
					<h4>Employee Name</h4>
					 <input name="name" id="id_input" class="serch" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
   <input type="hidden" name="emp_id" id="emp_id"  />
   <input type="hidden" name="salary_id" id="salary_id"  />
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>

				</div>
                <br class="clear">
				<div class="row1">
					<h4>Base Salary</h4>
					<input type="text" name="base_salary" id="base_salary" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row2">

					<h4>Deductions</h4>
					<input type="text" name="deduction_type" readonly style="width:120px;">
					<input type="text" name="deduction_amount" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row2">

					<h4>Allowances</h4>
					<input type="text" name="allowance_type" readonly style="width:120px;">
					<input type="text" name="allowance_amount" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row2">

					<h4>Expenses</h4>
					<input type="text" name="expense_type" readonly style="width:120px;">
					<input type="text" name="expense_amount" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row2">

					<h4>Increments</h4>
					<input type="text" name="increment_type" readonly style="width:120px;">
					<input type="text" name="increment_amount" readonly style="width:113px;">

				</div>
				<br class="clear">
				<div class="row1">
					<h4>Pay Ammount</h4>
					<input type="text" name="pay_amount" id="pay_amount"  onblur="payment()" equired="required">
				</div>
                
                <script type="text/javascript">
				function payment(){
                var salary_field=this['base_salary'].value;
				var paym_amount=this['pay_amount'].value;
				var remain=(salary_field) - (paym_amount);
				$('#remain_amount').val(remain);
				}
				
                </script>
				<br class="clear">
				<div class="row1">
					<h4>Remaining Amount</h4>
					<input type="text" name="remain_amount" id="remain_amount" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row2">
            		<h4>Payment Mode</h4>
            		<?php echo form_dropdown('payment_mode',$payment_mode,'class="se"','id="selected"')?>
            	</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="no_cheque">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_cheque">
            	</div>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="recieved">
                </div>
				<div class="row1 cash">
					<h4>Remarks</h4>
					<input type="text" name="remarks">
				</div>
				<div class="row">
				<div class="button-group">
					<input type="submit" class="btn green" value="Pay Salary" />
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
                    <div class="head" >Information</div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Code</h5>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">CNIC</h5>
                    	<i class="italica" id="cnic"></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Department</h5>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">position</h5>
                    	<i class="italica" id="position"></i>
				</div>
                <div class="row2">
                    	<h5 class="headingfive">Base Salary</h5>
                    	<i class="italica" id="base_salaryli"></i>
				</div>
				</div>
            </div>
			<!-- button group -->
			

		

	</div>
<!-- contents -->