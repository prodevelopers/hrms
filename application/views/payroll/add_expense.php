 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
    } else {
        $.post("payroll/autocomplete_expense/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
                $('#autoSuggestionsList').html(data);
            }
        });
    }
}

function fill(thisValue,employee_code,amount,id,position,department_name,type) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#amount').append(amount);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#exp_type').append(type);
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
}   


</script>
 
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense / Add Expenses</div>
    
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Add expense</div>
		<div id="flash" ><?php echo $this->session->flashdata('message');?></div>
		<div class="form-left">
			<form action="payroll/add_expense_records" method="post" enctype="multipart/form-data">
			<div class="row2">
					<h4>Employee Name</h4>
					 <input name="name" id="id_input" class="serch" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
   <input type="hidden" name="emp_id" id="emp_id"  />
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>

				</div>
   
				<br class="clear">
				<div class="row2">
					<h4>Expense Type</h4>
					<?php echo form_dropdown('expense_type',$expense,'required="required"','required="required"')?>
				</div>
				<script type="text/jscript">
                $(function(){$("#expdate").datepicker({dateFormat:'yy-mm-dd'})});
                </script>
                <br class="clear">
				<div class="row2">
					<h4>Expense date</h4>
					<input type="text" name="expdate" id="expdate" value="" required="required"/><span><img src="assets/icons/data.png" /></span>
				</div>
                <br class="clear">
				<div class="row2">
					<h4>Amount (<?php echo currency_label();?>)</h4>
					<input type="text" name="expamount" id="expamount" value="" required="required" />
				</div>
               <br class="clear">
                <div class="row2">
                    <h4>Attachment Receipts(optional)</h4>
                    <input type="file" name="receipt" id="receipt"/>
                </div>
                <br class="clear">
				<div class="row2">
				<div class="button-group">
					<input type="submit" class="btn green" name="add_rec" value="Submit for Approval">
					  
				</div>
                </div>
			</form>
			</div>
			<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Department</span>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Last Expense</span>
                    	<i class="italica" id="amount"><span id="exp_type"></span>&nbsp;&nbsp;&nbsp;&nbsp;</i>
				</div>
			</div>
   </div>
    </div>
<!-- contents -->

</body>
</html>
<script>
$(function() {
   $('#flash').delay(500).fadeIn('slow', function() {
      $(this).delay(2500).fadeOut('slow');
   });
});
</script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $( "#expdate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true


    });
    </script>
    <!-- leftside menu end -->