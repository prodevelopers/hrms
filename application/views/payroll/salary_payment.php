<style type="text/css">
html, body {
    height: 100%;
    width:104%;
}
.right-contents {
   background: none repeat scroll 0 0 #fff;
    border: thin solid #4d6684;
    float: left;
    margin-left: 221px;
    margin-top: -272px;
    padding-bottom: 10px;
    width: auto;
}
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.move{
	position: relative;
	left: 20%;
	top: 1em;
}
.
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.table_style
{
	width:auto;
}
select[name="month"],select[name="year"]{
	width: 22%;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	/*var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#smonth').val()});
			
                
		}
                
	});*/
});

/*$("#smonth").change(function(e) {
    oTable.fnDraw();
});*/

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id,smonth,syear){
	window.open('payroll/print_salarypayment/'+id/smonth/syear);
	pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
    </script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Salary Payment Balance</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<!--<script>
      var navigation = responsiveNav(".nav-collapse");
    </script>-->
	<div class="right-contents" style="width:76%">

		<div class="head">Salary Balance</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                
                <form action="payroll/salary_payment" method="post">
				<input type="text" name="emp_name" placeholder="Employee Name" style="width:22%">
                
				<?php echo @form_dropdown('month',$months)?>
           	    <?php echo @form_dropdown('year',$years)?>

					<input type="submit" value="Search" class="btn green">
					<!--<button class="btn gray">Reset</button>-->
                
                </form>
                <span class="fa fa-print move" style="cursor:pointer" title="Print All" onclick="print_all()"></span>
			</div>

			
			
			<!-- table -->
			<div class="table-responsive1">
            <div id="print_table">
				<table cellspacing="0" class="table_style">
				<thead class="table-head">
					<td>Code&nbsp;&nbsp;&nbsp;</td>
					<td>Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>Paygrade</td>
                    <td>&nbsp;Date</td>
					<td>Base salary</td>
                    <td>Allowance</td>
                    <td>Expense</td>
                    <td>loan/advances</td>
                    <td>Deductions</td>
                    <td>Payable</td>
                    <td>Paid</td>
                    <td>Remains</td>
                    <td>Status</td>
                    <td>Transaction</td>
                    <!--<td><img src="assets/img/icon-black.png" width="20" height="20"></td>-->
                    <td><span class="fa fa-print"></span></td>
                    <td><span class="fa fa-eye"></span></td>
                    
		<!--<td align="center"><a href="payroll/pay_salary"><img src="assets/img/icon-black.png" width="20" height="20"></a></td>-->
					<!--<td align="center"><img src="assets/img/icon-black.png" width="20" height="20">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-print">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-eye"></span></span></td>-->
					<!--<td align="center"><span class="fa fa-eye"></span></td>-->
					
					 <span><?php if(!empty($info)){?></span>
				</thead>
                <?php foreach($info as $salary_rec){ 
				$salary_payment_id=$salary_rec->salary_payment_id;
				////////// Allowance process
				   	$alwnc_month=$salary_rec->alwnc_month;
					$alwnc_year=$salary_rec->alwnc_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$allowance_freq=$salary_rec->pay_frequency;
					if($allowance_freq == 1)
					{$alwnc_amount=$salary_rec->allowance_amnt;}
				elseif($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$salary_rec->allowance_amnt;}else{$alwnc_amount=0;} 
				//// End Allowance process
				////////// Deduction process
					$ded_month=$salary_rec->ded_month;
					$ded_year=$salary_rec->ded_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$pay_requency=$salary_rec->deduction_frequency;
					if($pay_requency == 1)
					{$deduction_amount=$salary_rec->ded_amount;}
					elseif($salary_month == $ded_month && $salary_year == $ded_year)
				 {$deduction_amount=$salary_rec->ded_amount;}else{$deduction_amount=0;}
				//// End Deduction process
				////////// Expense process
				    $exp_id=$salary_rec->expense_claim_id;
					$exp_dis_id=$salary_rec->expense_id;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$exp_month=$salary_rec->exp_month;
					$exp_year=$salary_rec->exp_year;
					/*if($exp_id == $exp_dis_id)
					{$expense_amount=0;}else*/
					if($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$salary_rec->exp_amount;}else{$expense_amount=0;}
				//// End Expense process
				////////// Advance process
				    $loan_advance_id=$salary_rec->loan_advance_id;
					$loan_payback_id=$salary_rec->loan_payback_id;
				   	$lonad_month=$salary_rec->alonad_month;
					$lonad_year=$salary_rec->alonad_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					/*if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					else*/
					if($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$salary_rec->loan_amount;}else{$loan_advance=0;} 
				//// End Expense process
				//// Paybale amount
				$base_salary=$salary_rec->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					////END payable 
					////// Remain amount
					$paid_amount=$salary_rec->transaction_amount;
					$remain_amount=$payable-$paid_amount;
					/////////// End
					if($remain_amount !=0){
				?>
				<tr class="table-row">
					<td><?php echo $salary_rec->employee_code?></td>
					<td><?php echo $salary_rec->full_name?></td>
					<td><?php echo $salary_rec->pay_grade?></td>
					<td>
					<?php echo $salary_rec->month.",".$salary_rec->year?>&nbsp;
                    </td>
                    
                    <td><?php echo $salary_rec->base_salary?></td>
                    
                    <td><?php echo $alwnc_amount;?></td>
        
                    <td><?php echo $expense_amount;?></td>
                
					<td><?php  echo $loan_advance;?></td>
                     
                    <td><?php echo $deduction_amount;?></td>
				
					<td><?php echo $payable;?></td>
					<td><?php echo $salary_rec->transaction_amount;?></td>
               
                    <td><?php echo $remain_amount;?></td>
                    
                    <td><?php if($salary_rec->status_title=="Approved")
					{echo "<span style='color:green'>Approved</span>";}
					else{echo "<span style='color:red'>".$salary_rec->status_title."</span>";}?><!--<a href="payroll/update_status_salarypayment/<?php //echo $salary_rec->status_title;?>/<?php //echo $salary_rec->salary_payment_id;?>">
                     <?php  //echo $salary_rec->status_title;?></a>-->
					 <?php //}?>
                    </td>
                    
                     <td>&nbsp;&nbsp;<?php if($salary_rec->status_title=="Approved" && $salary_rec->transaction_id== NULL){?>
                     <a href="payroll/salary_payment_trans/<?php echo $salary_payment_id; ?>"><span class="fa fa-money"></span></a>
					 <?php }
					elseif($salary_rec->status_title=="Approved" && $salary_rec->transaction_id !== NULL)
					 {echo "<span style='color:green;'>Done !</span>";}
					 else{} ?>
                     </td>
                     
                   <!--  <td><?php //if($remain_amount==0){echo '&nbsp;&nbsp;';}else{?><!--<a href="payroll/pay_remain_salary/<?php //echo $salary_rec->employee_id; ?>/<?php //echo $salary_month;?>/<?php //echo $salary_year;?>/<?php //echo $payable;?>"><img src="assets/img/icon-black.png" width="20" height="20"></a>--><?php //}?>
                  <!--   </td> -->
                   
                    <td><?php if($salary_rec->status_title=="Approved" && $salary_rec->transaction_id !== NULL){?>
                    <a href="payroll/print_salarypayment/<?php echo $salary_rec->employee_id;?>/<?php echo $salary_month;?>/<?php echo $salary_year;?>" class="fa fa-print" style="cursor:pointer"></a><?php }else{} ?>
                    </td>
                    
                    <td><?php if($salary_rec->status_title=="Approved" && $salary_rec->transaction_id !== NULL){?>
                    <a href="payroll/payroll_salary_detail/<?php echo $salary_rec->employee_id; ?>/<?php echo $salary_month;?>/<?php echo $salary_year;?>"><span class="fa fa-eye"></span></a><?php }else{} ?>
                    </td>
				</tr>
                <?php }}?>
				<?php } else {echo "<span style='color:red'> Sorry no Records Found !</span>";} ?>
				
			</table>

			</div>
			
            </div>
            <br />
             <br />
        <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>

			

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}
</script>