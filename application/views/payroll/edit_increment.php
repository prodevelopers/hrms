 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 float:left;
 margin-left: 210px;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
 float:right;

}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
}
</style>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Increment / Edit Increment</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Update Increment</div>
			<form role="form" action="payroll/updte_increment_records" method="post">
            
				<div class="row">
					<h4>Employee Name</h4>
   <input name="name"  type="text"  value="<?php echo $row->full_name; ?>" readonly="readonly">
   <input type="hidden" name="incr_id" id="incr_id" value="<?php echo $row->increment_id; ?>" />
   </div>
			
				<br class="clear">
				
				<div class="row"> 
					<h4>Increment Type</h4>
					<?php 
					 $selected =(isset($row->increment_type_id) ? $row->increment_type_id:'');
					echo form_dropdown('increment_type',$increment_type,$selected)?>
				</div>
                <br class="clear">
                <div class="row">
					<h4>Increment Ammount</h4>
					<input type="text" name="incamount" id="incamount" value="<?php echo $row->increment_amount; ?>" />
				</div>
				<br class="clear">
                <script type="text/jscript">
                $(function(){$("#incEftDate").datepicker({dateFormat:'yy-mm-dd'})});
                </script>
                <br class="clear">
				<div class="row">
					<h4>Expense date</h4>
					<input type="text" name="incEftDate" id="incEftDate" value="<?php echo $row->date_effective; ?>"/><span><img src="assets/icons/data.png" /></span>
				</div>
				<br class="clear">
				
				<div class="row">
				<h4>Status</h4>
					<?php 
					$selected =(isset($row->status) ? $row->status:'');
					echo form_dropdown('status',$status,$selected)?>
				</div>
                <br class="clear">
				
				<div class="row">
				<div class="button-group">
					<input type="submit" value="Update Increment" class="btn green">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
          
			</form>

			<!-- button group -->
			

		</div>

	</div>
<!-- contents -->