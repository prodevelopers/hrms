<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>

<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">

  <style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	padding-left: 7em;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight:normal;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 12px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
	
}
  </style>

<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<table cellspacing="0" width="543" class="printing">
				<tr>
					<td colspan="2"  class="head"><h3>Salary Payment detail of &nbsp;<?php echo $details->full_name;?> for <?php echo $details->month;?> &nbsp;<?php echo $details->year;?></h3></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Name</td>
					<td width="337" class="no-apply"><i><?php echo $details->full_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Code</td>
					<td class="no-apply"><i><?php echo $details->employee_code;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Designation</td>
					<td class="no-apply"><i><?php echo $details->designation_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Pay Grade</td>
					<td class="no-apply"><i><?php echo $details->pay_grade;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Base Salary</td>
					<td class="no-apply"><i><?php echo Active_currency($details->base_salary);?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Deductions</td>
                    <?php
				   	$ded_month=$details->ded_month;
					$ded_year=$details->ded_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$pay_requency=$details->deduction_frequency;
					if($pay_requency = 1)
					{$deduction_amount=$details->ded_amount;}
					elseif($salary_month == $ded_month && $salary_year == $ded_year)
				 {$deduction_amount=$salary_rec->ded_amount;}else{$deduction_amount=0;}					 ?>
					<td class="no-apply"><i><?php echo Active_currency($deduction_amount);?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Allowance</td>
                    <?php
				   	$alwnc_month=$details->alwnc_month;
					$alwnc_year=$details->alwnc_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$allowance_freq=$details->pay_frequency;
					if($allowance_freq = 1)
					{$alwnc_amount=$details->allowance_amnt;}
				elseif($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$details->allowance_amnt;}else{$alwnc_amount=0;} 
					 ?>
					<td class="no-apply"><i><?php echo Active_currency($alwnc_amount);?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Expense</td>
                     <?php
				    $exp_id=$details->expense_claim_id;
					$exp_dis_id=$details->expense_id;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$exp_month=$details->exp_month;
					$exp_year=$details->exp_year;
					if($exp_id == $exp_dis_id)
					{$expense_amount=0;}
					elseif($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$details->exp_amount;}else{$expense_amount=0;}
					 ?>
					<td class="no-apply"><i><?php echo Active_currency($expense_amount);?></i></td>
				</tr>
                <tr>
					<td width="200" class="apply">Loan/Advance</td>
                     <?php
					$loan_advance_id=$details->loan_advance_id;
					$loan_payback_id=$details->loan_payback_id;
				   	$lonad_month=$details->alonad_month;
					$lonad_year=$details->alonad_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					else
					if($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$details->loan_amount;}else{$loan_advance=0;} 
					 ?>
					<td class="no-apply"><i><?php echo Active_currency($loan_advance);?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Withholdings</td>
					<td class="no-apply"><i><?php //echo $details->full_name;?></i></td>
				</tr>
				
				<tr>
					<td width="200" class="apply">Totals</td>
                    <?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?>
					<td class="no-apply"><i><?php echo Active_currency($payable);?></i></td>
				</tr>
			</table>
		</div>

