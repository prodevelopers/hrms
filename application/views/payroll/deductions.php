<style type="text/css">

.move{
	position: relative;
	top: 20px;
	left: 5%;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length {
    font-size: 1px;
    left: 84%;
    position: relative;
    top: -50px;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -49px;
left: -3em;
}
.dataTables_filter input{
	width: 180px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.table_style
{
	width:auto;
	margin-left:0; 
}
select[name="ded_type"],select[name="year"],select[name="month"],select[name="frequency"]{
	width: 22%;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
		function trashed()
		{
		}
	var oTable = $('#table_list').dataTable( {/*
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		    aoData.push({"name":"ded_type","value":$('#ded_type').val()});
			aoData.push({"name":"frequency","value":$('#frequency').val()});
			
                
		}
                
	*/});
	
	//var t=$('#status_lilnk').text();
	//alert(t);
	
	//if()
	
});

/*$(".filter").change(function(e) {
    oTable.fnDraw();
});
*/
function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Deductions</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Deductions</div>
		<!-- filter -->
			 <!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
				<!--<input type="text" name="name" id="name" style="width: 220px;" placeholder="Search Name.." onchange='this.form.submit()'/>-->
                <?php echo @form_dropdown('name',$employees_name,$name,"id='name' class='resize' onchange='this.form.submit()'")?>
                <?php echo @form_dropdown('ded_type',$deduction_type,$slct_ded,"id='ded_type' class='resize' onchange='this.form.submit()'")?>
				<?php //echo @form_dropdown('month',$month,$slct_m,"id='month' class='row' 		onchange='this.form.submit()'")?>
                <?php echo @form_dropdown('frequency',$frequency,$slct_freq,"id='frequency' class='resize' onchange='this.form.submit()'")?>
                
           	   <?php //echo @form_dropdown('year',$year,$slct_y,"id='year' class='row' onchange='this.form.submit()'")?>								               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <span class="fa fa-print move" style="cursor:pointer" title="Print All" onclick="print_all()"></span>
                 <?php echo form_close();?>
			</div>
			
			<div class="table-responsive">
            <div id="print_table">
				<table cellspacing="0" id="table_lists">
				<thead class="table-head">
                	<td>Code&nbsp;</td>
					<td>Name</td>
<!--					<td>Base Salary</td>-->
                    <td>Deduction Type</td>
                    <td>Deduction Frequency</td>
					<td>Deductions</td>
                    <!--<td>Month&nbsp;&nbsp;&nbsp;</td>
                    <td>Year&nbsp;&nbsp;&nbsp;</td>-->
					<td>Status &nbsp;&nbsp;&nbsp;</td>
					<td>Approved By </td>
					<td>Approved date </td>
                    <td><span class="fa fa-pencil"></span></td>
                    <td><span class="fa fa-trash-o"></span></td>
				</thead>
					<?php if(!empty($info)){
						foreach($info as $rec){?>
					<tr class="table-row">
                	<td><?php echo $rec->employee_code;?>&nbsp;</td>
					<td><?php echo $rec->full_name;?></td>
<!--					<td>--><?php //echo $rec->base_salary;?><!--</td>-->
                    <td><?php echo $rec->deduction_type;?></td>
                    <td><?php echo $rec->pay_frequency;?></td>
					<td><?php echo Active_currency($rec->deduction_amount);?></td>
                    <!--<td>Month&nbsp;&nbsp;&nbsp;</td>
                    <td>Year&nbsp;&nbsp;&nbsp;</td>-->
					<td><?php 
					if($rec->status==2)
					{echo "<span style='color:green'>Approved</span>";}
					else{echo "<span style='color:red'>".$rec->status_title."</span>";}?><!--<a href="payroll/deduction_status_update/<?php //echo $rec->status_title;?>/<?php //echo $rec->deduction_id;?>">
					<?php  //echo $rec->status_title;?></a>--><?php //}?>
                    </td>
						<td><?php echo $rec->name;?></td>
						<td><?php
							$date=$rec->date_approved;
							$new=date("d-m-Y", strtotime($date));
							if($rec->status==2){
							echo $new;}else{}?></td>
                    <td><?php 
					if($rec->status==2)
					{echo '&nbsp;&nbsp;&nbsp;';}
					else{?><a href="payroll/deduction_edit/
					<?php echo $rec->deduction_id; ?>">
                    <span class="fa fa-pencil"></span></a>
					<?php }?>
                    </td>
                    <td>
                        <span class="fa fa-trash-o"
                    onclick="trashed(<?php echo $rec->deduction_id;?>)" style="cursor: pointer;">
                    </span></td>
				</tr>
                <?php }}else{echo "<span style='color:red'> Sorry no Records Found !</span>";}?>
			</table>
			</div>
			
            </div>
			 <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="payroll/add_deduction"><button class="btn green">Add Deduction</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
    function trashed(id) {
        if (confirm("Are You sure to trash the record")) {
            var id=id;
            $.ajax({
                url:"payroll/deduction_trashed",
            data:{id:id},
            type:"POST",
            success:function(result){
               location.reload();
            }
            });
        } else {

        }
    }

function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}
$(document).ready(function() {
	//var pending=$("#status_text").html();
	
	//alert($("#status_text").val());
	});
$(document).ready(function(e)
{
    $('#ded_type,#frequency,#name').select2();
})
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    </script>
    <!-- leftside menu end -->