<script>
	function print_table(id) {
		var printContents = document.getElementById(id).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}</script>
<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
body{
	background: #f8f8f8;
}
table th{
	font-size: 12px;
	font-weight:bold;
}
table td{
	font-size: 11px;
}
.shadow h5{
	display:inline-block;
	margin-left: 4em;
}
</style>

<div class="row" style=" height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-12">
			<div class="col-lg-11">
				<h5 class="text-center">Staff List With Salary Package</h5>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
				  <tr>
					<th>S.No</th>
					<th>Name</th> 
					<th>Designation</th> 
					<th>Project</th>
					<th>Job Grade</th>
					<th>Salary Package</th>
				  </tr>
				</thead>
				<tbody>
				<?php if(!empty($info)){
					$no=1;
				foreach($info as $rec){
				?>
				  <tr>
					  <td><?php echo $no;?></td>
					  <td><?php echo $rec->full_name;?></td>
					  <td><?php echo $rec->designation_name;?></td>
					  <td><?php echo $rec->project_title;?></td>
					  <td><?php echo $rec->pay_grade;?></td>
					  <td><?php $base_salary=$rec->base_salary; echo Active_currency($rec->base_salary);?></td>
				  </tr>
				<?php $no++;}}else{echo "No record found !";}?>
				</tbody>
			</table>
	</div>
</div>
	</div>
						
