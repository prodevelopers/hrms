<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete_loan_advacne/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,id,position,department_name,amount) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	$('#amount').append(amount);
	
	
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Loan/Advance </div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Advance Payment</div>
		<div class="col-md-12">
		<div class="notice info">
			<p>Important Note Before Submitting the Form</p>
			<ul>
				<li> Advance Payment can only be created if no record already exist for same payment type and for same Month and Year.</li>
				<li> Only one Advance Payment Request can be created per Payment Type Per Month.</li>
			</ul>
		</div>
		</div>
			<div id="flash" ><?php echo $this->session->flashdata('message');?></div>

			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
<div class="form-left">
			<form action="payroll/loan_advance_pay" method="post" role="form">
				<div class="row2">
					<h4>Employee Name</h4>
		<input name="name" id="id_input"  class="serch" type="text" placeholder="Search Employee Name" autocomplete="off" value="" onkeyup="lookup(this.value)" required="required">
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
   <input type="hidden" name="emp_id" id="emp_id"  />
				</div>
				
				<br class="clear">
				<div class="row2">
					<h4>Payment Type</h4>
					<?php echo form_dropdown('payment_type',$payment_type,'','id="PaymentTypeSelect"','required="required"')?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Payment Amount (<?php echo currency_label();?>)</h4>
					<input type="text" id="paymentSelect" name="payment_amount" value="" placeholder="Amount" required="required">
				</div>
                <br class="clear">	
                 <div class="row1">
					<h4>Month</h4>
					<select name="month" id="monthSelector">
                   	<option value="-1" selected="selected">-- Select Month Salary --</option>
                    <?php foreach($months as $mon):?>
						<option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<br class="clear">
				<div class="row1">
					<h4>Year</h4>
					<select name="year" id="yearSelect">
                   	<option value="-1" selected="selected">-- Select Year --</option>
                    <?php foreach($years as $yer):?>
						<option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<br class="clear">
				<!--<div class="row2">
            		<h4>Payment Mode</h4>
            		<?php //echo form_dropdown('payment_mode',$payment_mode,'class="se"','id="selected"')?>
            	</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="no_cheque">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_cheque">
            	</div>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="recieved">
                </div>
				<div class="row1 cash">
					<h4>Remarks</h4>
					<input type="text" name="remarks">
				</div>-->
				<br class="clear">
				<div class="row2">
					<input type="submit" id="submitBtn"  class="btn green" value="Submit for Approval" />
				</div>
				
			</form>
</div>
<div class="form-right">
<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<span class="headingfive">Employee Name</span>
                    	<i class="italica" id="employee_name"></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"></i>
                    </div>
                   
                    <div class="row2">
                    	<span class="headingfive">Department</span>
                    	<i class="italica" id="department"></i>
				</div>
				<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Last advance</span>
                        <!--<li></li>-->
                    	<i class="italica" id="amount"></i>
				</div>
                
			</div>
    </div>
	
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="#"><button class="btn green"></button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>
	</div>
<!-- contents -->
<script>
$(function() {
   $('#flash').delay(500).fadeIn('slow', function() {
      $(this).delay(2500).fadeOut('slow');
   });

	$("#submitBtn").on("click",function(e){
		e.preventDefault();
		var employeeSelect    =$("#id_input").val();
		var PaymentTypeSelect =$("#PaymentTypeSelect").val();
		var paymentSelect     =$("#paymentSelect").val();
		var selectedMonth     =$("#monthSelector").val();
		var yearSelect        =$("#yearSelect").val();

		if(employeeSelect.length == 0){
			Parexons.notification("Please Select Employee Name From Search field","error");
			return false;
		}
		if(PaymentTypeSelect.length == 0){
			Parexons.notification("Please Select Payment Type From DropDown","error");
			return false;
		}
		if(paymentSelect.length == 0){
			Parexons.notification("Please Fill The Amount Field","error");
			return false;
		}
		if(selectedMonth === "-1"){
			Parexons.notification("Please Select Month From DropDown","error");
			return false;
		}
		if(yearSelect === "-1"){
			Parexons.notification("Please Select Year From DropDown","error");
			return false;
		}
		$(this).parents("form").submit();
	});
});
</script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->