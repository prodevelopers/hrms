<!doctype html>
    <html>
<head>



<!-- contents -->
<style type="text/css">
    .beauty h4{
        font-size:13px;
        font-weight:normal;
    }
    .beauty td{
        font-size: 13px;
    }
    .beauty{
        width:23%;
        margin:1em 0.5em;
        float:left;
    }
    .head1{
        background: #4D6684;
        color: #fff;
        font-weight:500;
        font-size: 0.9em;
        line-height: 25px;
    }
    .cheque{
        width:720px;
        min-height: 6em;
        font-family:"calibri";
        overflow: hidden;
        border: 1px solid #e1e1e1;
        -webkit-box-shadow:0 0 2px rgba(0,0,0,0.4);
        margin-left:30px;
    }
    .head{
        font-family:"calibri";
        background-color: #008000;
        padding: 0.5em;
        color:#fff;
        font-weight: bold;
        font-size: 18px;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
<script>
    function print_table(id) {
        var printContents = document.getElementById(id).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</head>
<body>
<div class="contents-container">



    <div class="right-contents">
        <div class="head" style="font-size:1em; font-weight:bold;">PayRoll PaySlip
           <!-- <a href="<?php /*echo base_url(); */?>payroll/domPDF_Test/<?php /*echo $this->uri->segment(3);*/?>/<?php /*echo $this->uri->segment(4);*/?>/<?php /*echo $this->uri->segment(5);*/?>"><input type="button"  name="email" class="btn green" style="float:right" value="Email"></a>-->
           <!-- <button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right"  style="float:right" data-toggle="modal" data-target="#myModal">
                Send
            </button>-->
            <input type="button"  name="print" onclick="print_table('print_area')" class="btn green" style="float:right" value="Print"></div>
        <div id="print_area">
            <table class="cheque" cellpadding="0" cellspacing="0" align="center">
                <tr class="head">
                    <th colspan="4" style="text-align:left;">Salary Slip <?php echo @$details->month.", ".$details->year; ?></th>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><h4><?php echo @$details->full_name;?></h4></td>
                    <td>Employee Code</td>
                    <td><h4><?php echo @$details->employee_code;?></h4></td>
                </tr>
                <tr>
                    <td>Designation</td>
                    <td><h4><?php echo @$details->designation_name;?></h4></td>
                    <td>Pay Grade</td>
                    <td><h4><?php echo @$details->pay_grade;?></h4></td>
                </tr>

                <tr >
                    <td>Base Salary</td>
                    <td><h4><?php echo @$details->base_salary;?></h4></td>

                    <td>Deducations</td>

                    <td><h4><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Allowances</td>

                    <td><h4><?php
                            if(!empty($allw_trans->transaction_amount)){
                                echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>

                    <td>Expenses</td>

                    <td><h4><?php
                            if(!empty($exp_trans->transaction_amount)){
                                echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
                </tr>
                <tr>
                    <td>Advances</td>

                    <td><h4><?php
                            if(!empty($advance_trans->advance_trans_amount))
                            {
                                echo @$advance_trans->advance_trans_amount;}else{echo "0";}?>
                        </h4></td>

                    <td>Last Arrears</td>
                    <?php if(!empty($last_salary->arears)){?>
                        <td><?php echo $last_salary->arears ;?></td>
                    <?php }else{?><td>0</td><?php }?>
                </tr>
                <tr>
                    <td>Total Paid</td>

                    <td><h4><?php echo @$salary_info->transaction_amount;?></h4></td>

                    <td>Balance </td>
                    <td><h4>
                            <?php echo @$salary_info->arears;?></h4></td>
                </tr>

            </table>

            <table>
                <tr>
                    <td>Approved By:____________</td>
                    <td>Designation:____________</td>
                    <td>Signature:____________</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
<!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">From</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" readonly id="inputPassword3" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label"><?php echo  $this->session->flashdata('fileName');?></label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
    $(document).ready(function(){
        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($pdfView) && !empty($pdfView)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($pdfView); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>payroll/sendGeneratedPDFReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>
</html>