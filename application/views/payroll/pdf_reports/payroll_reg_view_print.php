<!-- contents -->

<style type="text/css">
	.beauty h4{
		font-size:13px;
		font-weight:normal;
	}
	.beauty td{
		font-size: 13px;
	}
	.beauty{
		width:23%;
		margin:1em 0.5em;
		float:left;
	}
	.head1{
	background: #4D6684;
	color: #fff;
	font-weight:500;
	font-size: 0.9em;
	line-height: 25px;
}
</style>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
	thead  th {
		font-size:15px;
	}
	.infor th{
		font-size: 14px;
	}
	tbody td{
		font-size:14px;
	}
	.detail td{
		font-size: 13px;
	}
</style>
<script>
	function print_table(id) {
		var printContents = document.getElementById(id).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
	</script>
<base href="<?php echo base_url(); ?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
<link rel="stylesheet" type="text/css" href="assets/multslect/multiple-select.css">
<link rel="stylesheet" type="text/css" href="assets/css/form.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">

<div class="contents-container">


	<div class="right-contents">
		<div class="head" style="font-size:1em; font-weight:bold;">PayRoll Record <?php echo @$details->month.", ".$details->year; ?>

			<!--<a href="mailto:<?php /*echo $email->email_address;*/?>?subject=Admin Payroll Slip&attachment=<?php /*echo  $this->session->flashdata('fileName');*/?>"></a>-->
			<button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right"  style="float:right" data-toggle="modal" data-target="#myModal">
				Send
			</button>
			<!--<a href="payroll/print_payroll_detail/<?php /*echo $this->uri->segment(3);*/?>/<?php /*echo $this->uri->segment(4);*/?>/<?php /*echo $this->uri->segment(5);*/?>"><input type="button"  name="email" class="btn green" style="float:right" value="Email"></a>-->
	<input type="button"  name="print" onclick="print_table('print_area')" class="btn green" style="float:right" value="Print"></div>
		<div id="print_area">
		<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head1">
					<th colspan="2" style="text-align:left;">Employee Infomation</th>
				</tr>
				<tr class="table-row">
					<td>Name</td>
					<td><h4><?php echo @$details->full_name;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Employee Code</td>
					<td><h4><?php echo @$details->employee_code;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Designation</td>
					<td><h4><?php echo @$details->designation_name;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td><h4><?php echo @$details->pay_grade;?></h4></td>
				</tr>
				
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Salary Info</th>
				</tr>
				<tr >
					<td>Base Salary</td>
					<td><h4><?php echo @$details->base_salary;?></h4></td>
				</tr>
				<tr>
					<td>Deducations</td>

                    <td><h4><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>
				<tr>
					<td>Allowances</td>

                    <td><h4><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>	
				<tr>
					<td>Expenses</td>

                    <td><h4><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>	
				<tr>
					<td>Advances</td>

					<td><h4><?php 
					if(!empty($advance_trans->advance_trans_amount))
					{
					echo @$advance_trans->advance_trans_amount;}else{echo "0";}?>
                  </h4></td>
				</tr>
				<tr>
					<td><h4>Last Arrears</h4></td>
					<?php if(!empty($last_salary->arears)){?>
						<td><?php echo $last_salary->arears ;?></td>
					<?php }else{?><td>0</td><?php }?>
				</tr>
				<tr>
					<td>Total Paid</td>

					<td><h4><?php echo @$salary_info->transaction_amount;?></h4></td>
				</tr>


			</table>
		<table class="table beauty" cellpadding="0" cellspacing="0">
			<tr class="head">
				<th colspan="8" style="text-align:left;">Balance Details</th>
			</tr>
			<tr class="table-row">
				<td>Balance </td>
				<td><h4>
						<?php echo @$salary_info->arears;?></h4></td>
			</tr>

		</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8">Deductions</th>
				</tr>
				<tr class="table-row">
				<?php
				$ded_type=@$ded_trans->ded_rectp; $ded_amt=@$ded_trans->ded_recmnt;
				?>
					<td>
				<?php $explode_dedtyp=explode(",",$ded_type);
				$explode_dedamt=explode(",",$ded_amt);
				?>

				<?php foreach($explode_dedtyp as $ded_typ)
				{
					echo $ded_typ."<br />";

				}?>
					</td>
					<td><h4>
				<?php foreach($explode_dedamt as $ded_mt)
				{
					echo $ded_mt."<br />";

				}
				?></h4></td>
				</tr>
				<tr>
				<td class="headingfive">Total <!--Deductions--> </td>
					<td><h4><?php
					$deduction_amount=@$ded_trans->total_ded_amount;
					echo $deduction_amount;?></h4></td></tr>
			</table>

        <br class="clear"/>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Allowances</th>
				</tr>

				<tr>
					<?php
					$alw_rectp=@$allw_trans->alw_rectp; $alw_recm=@$allw_trans->alw_recm;
					?>
					<td>
						<?php $explode_alw_rectp=explode(",",$alw_rectp);
						$explode_alw_recm=explode(",",$alw_recm);
						?>

						<?php foreach($explode_alw_rectp as $alw_typ)
						{
							echo $alw_typ."<br />";
						}?>
					</td>
					<td><h4>
							<?php foreach($explode_alw_recm as $alw_mt)
							{
								echo $alw_mt."<br />";
							}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total</td>
					<td><h4><?php
							$allowance_amount=@$allw_trans->total_alw_recm;
							echo $allowance_amount;?></h4></td></tr>

			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Expenses</th>
				</tr>

				<tr>
					<?php
					$exp_rectp=@$exp_trans->exp_rectp; $exp_recm=@$exp_trans->exp_recm;
					?>
					<td>
						<?php $explode_exp_rectp=explode(",",$exp_rectp);
						$explode_exp_recm=explode(",",$exp_recm);
						?>

						<?php foreach($explode_exp_rectp as $expense_type)
						{
							echo $expense_type."<br />";
						}?>
					</td>
					<td><h4>
							<?php foreach($explode_exp_recm as $expense_amt)
							{
								echo $expense_amt."<br />";
							}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total </td>
					<td><h4><?php
							$exp_amount=@$exp_trans->total_exp_recm;
							echo $exp_amount;?></h4></td></tr>


			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Advances</th>
				</tr>
				<tr>
					<?php
					$advance_rectp=@$advance_trans->loan_rectp; $advance_recm=@$advance_trans->loan_recm;
					?>
					<td>
						<?php $explode_advance_rectp=explode(",",$advance_rectp);
						$explode_advance_recm=explode(",",$advance_recm);
						?>

						<?php foreach($explode_advance_rectp as $advance_type)
						{
                            if(!empty($advance_type))
                            {
							echo $advance_type."<br />";
                            }elseif(!empty($advance_trans->advance_trans_amount)){ echo "Balance"."<br />";}
						}?>
					</td>
					<td><h4>
							<?php foreach($explode_advance_recm as $advance_amt)
							if(!empty($advance_amt))
                            {
								echo $advance_amt."<br />";
                            }
                            elseif(!empty($advance_trans->advance_trans_amount))
                            {echo $advance_trans->advance_trans_amount;}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total </td>
					<td><h4>
					<?php
					$advance_amount=@$advance_trans->total_advamount;
                    if(!empty($advance_amount)){
					echo $advance_amount;}
                    elseif(!empty($advance_trans->advance_trans_amount)){
                        echo $advance_trans->advance_trans_amount;
                    }?></h4>
					</td></tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Payment Mode </th>
				</tr>

				<?php if(!empty($payment_mode->account_id)){?>
				<tr >
					<td>Account Title</td>
					<td><h4><?php
					echo $payment_mode->account_title;?></h4></td>
				</tr>
				<tr >
					<td>Account Number</td>
					<td><h4><?php
					echo $payment_mode->account_no;?></h4></td>
				</tr>
				<tr >
					<td>Bank</td>
					<td><h4><?php
					echo $payment_mode->bank_name;?></h4></td>
				</tr>
				<tr >
					<td>Branch</td>
					<td><h4><?php
					echo $payment_mode->branch;?></h4></td>
				</tr>
				<?php }if(!empty($payment_mode->cash_id)){?>
					<tr >
						<td>Received By</td>
						<td><h4><?php
								echo $payment_mode->received_by;?></h4></td>
					</tr>
					<tr >
						<td>Remarks</td>
						<td><h4><?php
								echo $payment_mode->remarks;?></h4></td>
					</tr>
				<?php }elseif(!empty($payment_mode->cheque_id)){?>
					<tr >
						<td>Cheque No</td>
						<td><h4><?php
								echo $payment_mode->cheque_no;?></h4></td>
					</tr>
					<tr >
						<td>Bank</td>
						<td><h4><?php
								echo $payment_mode->chk_bank;?></h4></td>
					</tr>
				<?php }?>


			</table>


        <br class="clear">
		<table class="table beauty" cellpadding="0" cellspacing="0">
			<tr class="head">
				<th colspan="8" style="text-align:left;">Transaction Details</th>
			</tr>
			<tr >
				<td>Transaction Date</td>
				<td><h4><?php
						$date=$details->transaction_date;
						$new=date("d-m-Y",strtotime($date));
						echo @$new;?></h4></td>
			</tr>
			<tr >
				<td>Transaction ID</td>
				<td><h4><?php echo @$details->transaction_id;?></h4></td>
			</tr>
			<tr >
				<td>Reviewed By</td>
				<?php if($details->status==2){?>
				<td><h4><?php echo @$details->name;?></h4></td>
			</tr>
			<tr>
				<td>Approved Date</td>
				<td><h4> <?php
						$date=$details->ap_date;
						if(!empty($date)){
							$new=date("d-m-Y",strtotime($date));
							echo @$new;}else{echo "";}}?></h4></td>
			</tr>
		</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head1">
					<th colspan="2" style="text-align:left;">Previous Salaries Record of <?php echo @$details->year; ?></th>
				</tr>
                <?php if(!empty($salaries)){
					foreach($salaries as $salary){
					$month=$salary->month;
					$current_year=$salary->year;
					$salary_amount=$salary->transaction_amount;
					if($month=="Jan")
					{$jan_salary=$salary_amount;}
					if($month=="Feb")
					{$feb_salary=$salary_amount;}
					if($month=="Mar")
					{$mar_salary=$salary_amount;}
					if($month=="Apr")
					{$apr_salary=$salary_amount;}
					if($month=="May")
					{$may_salary=$salary_amount;}
					if($month=="Jun")
					{$jun_salary=$salary_amount;}
					if($month=="Jul")
					{$jul_salary=$salary_amount;}
					if($month=="Aug")
					{$aug_salary=$salary_amount;}
					if($month=="Sep")
					{$sep_salary=$salary_amount;}
					if($month=="Oct")
					{$oct_salary=$salary_amount;}
					if($month=="Nov")
					{$nov_salary=$salary_amount;}
					if($month=="Dec")
					{$dec_salarys=$salary_amount;}
					}}else{echo "Sorry no previous salary !";}
					
					?>
				<tr >
					<td>Jan / Feb</td>
					<td><h4><?php if(!empty($jan_salary)){echo @$jan_salary;}
					else{echo "0";}?> / 
					<?php if(!empty($feb_salary)){echo @$feb_salary;}
					else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Mar / Apr</td>
					<td><h4><?php if(!empty($mar_salary)){echo @$mar_salary;}
					else{echo "0";}?> / 
					<?php if(!empty($apr_salary)){echo @$apr_salary;}
					else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>May / Jun</td>
					<td><h4><?php if(!empty($may_salary)){echo @$may_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($jun_salary)){echo @$jun_salary;}
					else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Jul / Aug</td>
					<td><h4><?php if(!empty($jul_salary)){echo @$jul_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($aug_salary)){echo @$aug_salary;}
					else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Sep / Oct</td>
					<td><h4><?php if(!empty($sep_salary)){echo @$sep_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($oct_salary)){echo @$oct_salary;}
					else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Nov / Dec</td>
					<td><h4><?php if(!empty($nov_salary)){echo @$nov_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($dec_salarys)){echo @$dec_salarys;}
					else{ echo "0";}?></h4></td>
				</tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:45%">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Total Amount of <?php echo @$details->month." ".$details->year; ?></th>
				</tr>
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><?php echo @$details->salary_date;?></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>Net Paid</td>
					<td><?php /*?><?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?><?php */?>
					<h4><?php echo "RS: ".$salary_info->transaction_amount." /-";?></h4></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
	</div>
<script src="<?php echo base_url()?>assets/js/edit-dialogs.js"></script>
<!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Email To:</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="mailForm">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">To</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">From</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="FromEmail" readonly id="inputPassword3" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
						<div class="col-sm-9">
							<textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
						<div class="col-sm-9">
							<label  class="col-sm-2 control-label"><?php echo  $this->session->flashdata('fileName');?></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
	$(document).ready(function(){
		$('#sendMailWithAttachment').on('click', function (e) {
			var formData = $('#mailForm').serializeArray();
			<?php if(isset($view) && !empty($view)){ ?>
			formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
			<?php } ?>
			var targetURL = '<?php echo base_url();?>payroll/sendGeneratedPDFReport';
			$.ajax({
				url: targetURL,
				data:formData,
				type:"POST",
				success: function (output) {
					var data = output.split("::");
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});
	});
</script>

<!-- contents -->
