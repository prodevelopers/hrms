<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;
	 
     document.body.innerHTML = printContents;
	 
     window.print();

     document.body.innerHTML = originalContents;
}</script>

<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">
<style type="text/css">
*{
	padding: 0;
	margin:0;
}
.head{
	font-family:"calibri";
	background:#005b96;
	padding: 0.5em;
	color:#fff;
	font-weight: bold;
	font-size: 18px;
}
.cheque{
	width:720px;
	min-height: 6em;
	font-family:"calibri";
	overflow: hidden;
	border: 1px solid #e1e1e1;
	-webkit-box-shadow:0 0 2px rgba(0,0,0,0.4);
	margin-left:30px;	
}
.green {
    background: none repeat scroll 0 0 #6aad6a;
    color: #fff;
}
.btn {
    border: medium none;
    cursor: pointer;
    float: left;
    margin-right: 5px;
    padding: 5px 8px;
}
/*.cheque ul{
		width: 45%;
		float: left;	
		padding: 0.5em;
}
.cheque ul li{
	float: left;
	list-style: none;
	margin: 0.5em 0;
	width: 49%;
}
.cheque li:nth-child(odd){
	font-weight: bold;
	font-size: 14px;
}
.cheque li:nth-child(even){
	font-size: 14px;
}*/
.br{
	margin: 0.1em 0.1em 0.3em 0.1em;
	width: 99%;
	background: rgba(0,0,0,0.1);
	float: left;
}
.net{
	height: 30px;
	width:76%;
	float: left;
	margin: 0.5em 0;
}
.net p{
	float: right;
	font-size: 14px;
	font-weight: bold;
	color:#005b96;
}
.total{
	height: 30px;
	width: 20%;
	float: left;
	margin: 0.5em 0;
}
.total p{
	font-size: 14px;
	font-style: italic;
	padding-left: 0.6em;
	text-decoration: underline;;
}
.td_style{
	width:171px;
	height:38px;
	font-weight:bold;	
	}
</style>
			<?php if(!empty($info)){ if($info->payment_mode_type_id==3){?>
			<div id="account">
			<p>&nbsp;</p>
			<table  border="0" class="cheque" align="center">
			  <tr>
			    <td height="17" colspan="4" class="head">Pay Slip of <?php echo $info->month." ".$info->year;?></td>
		      </tr>
			  <tr>
			    <td class="td_style">Employee Name</td>
			    <td width="250"><?php  echo $info->full_name;?></td>
			    <td class="td_style"> Deductions</td>
                
			    <td width="102"><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Employee ID</td>
			    <td height="21"><?php  echo $info->employee_code;?></td>
			    <td class="td_style">Expenses</td>
              
			    <td height="21"><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >CNIC</td>
			    <td height="21"><?php echo $info->cnic;?></td>
			    <td class="td_style">Allowances</td>
              
			    <td height="21"><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Account Details</td>
			    <td height="21">
				<?php echo $info->bank_name; echo "&nbsp;&nbsp;".$info->branch;?></td>
			    <td class="td_style">Advances</td>
               
			    <td height="21"><?php 
					if(!empty($advance_trans->transaction_amount))
					{
					echo @$advance_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Account No</td>
			    <td height="21"><?php  echo $info->account_no;?></td>
			    <td class="td_style">Transaction Date</td>
			    <td height="21"><?php 
				$date=$info->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
			    
		      </tr>
			  <tr>
			    <td class="td_style" >Base Salary</td>
			    <td height="21"><?php  echo $info->base_salary."/-";?></td>
			    <td height="21">&nbsp;</td>
			    <td height="21">&nbsp;</td>
		      </tr>
			  <tr>
             
			    <td height="21" colspan="4" align="right"><span style="text-align:center; font-weight:bold; color:#069;">Net Pay Salary:</span> &nbsp;&nbsp;&nbsp;<u><?php  echo "Rs: ".@$info->transaction_amount."/-";?>&nbsp;&nbsp;&nbsp;</u>  </td>
		      </tr>
		    </table>
			<p>&nbsp; </p>
            </div>
             <div id="form">
            <form action="" method="post" >
            <input type="hidden" name="next" value="gess" />
            <p style="margin-left:10px; color:#093">
			<?php echo "Remaining Slips: ".$no;?></p>
            <input type="submit" name="submit" class="btn green" style="margin-left:10px" value="Next">
            </form>
            </div>
            <?php }elseif($info->payment_mode_type_id==2){?>
            <!-----------Slip for Cheque----------->

		<div id="cheque">
			<p>&nbsp;</p>
			<table  border="0" class="cheque" align="center">
			  <tr>
			    <td height="17" colspan="4" class="head">Pay Slip of <?php echo $info->month." ".$info->year;?></td>
		      </tr>
			  <tr>
			    <td class="td_style">Employee Name</td>
			    <td width="250"><?php  echo $info->full_name;?></td>
			    <td class="td_style"> Deductions</td>
              
			    <td width="102"><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Employee ID</td>
			    <td height="21"><?php  echo $info->employee_code;?></td>
			    <td class="td_style">Expenses</td>
               
			    <td height="21"><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >CNIC</td>
			    <td height="21"><?php echo $info->cnic;?></td>
			    <td class="td_style">Allowances</td>
              
			    <td height="21"><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Bank</td>
			    <td height="21"><?php echo $info->chk_bank;?></td>
			    <td class="td_style">Advances</td>
               
			    <td height="21"><?php 
					if(!empty($advance_trans->transaction_amount))
					{
					echo @$advance_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Cheque No</td>
			    <td height="21"><?php  echo $info->cheque_no;?></td>
			    <td class="td_style">Transaction Date</td>
			     <td height="21"><?php 
				$date=$info->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
			    
		      </tr>
			  <tr>
			    <td class="td_style" >Base Salary</td>
			    <td height="21"><?php  echo $info->base_salary."/-";?></td>
			    <td height="21">&nbsp;</td>
			    <td height="21">&nbsp;</td>
		      </tr>
			  <tr>
             
			    <td height="21" colspan="4" align="right"><span style="text-align:center; font-weight:bold; color:#069;">Net Pay Salary:</span> &nbsp;&nbsp;&nbsp;<u><?php  echo "Rs: ".@$info->transaction_amount."/-";?>&nbsp;&nbsp;&nbsp;</u>  </td>
		      </tr>
		    </table>
			<p>&nbsp; </p>
            </div>
             <div id="form">
            <form action="" method="post" >
            <input type="hidden" name="next" value="gess" />
             <p style="margin-left:10px; color:#093">
			<?php echo "Remaining Slips: ".$no;?></p>
            <input type="submit" name="submit" class="btn green" style="margin-left:10px" value="Next">
            </form>
            </div>
			<?php }elseif($info->payment_mode_type_id==1){?>
		<!-----------Slip for Cash----------->
        
		<div id="cash">
			<p>&nbsp;</p>
			<table  border="0" class="cheque" align="center">
			  <tr>
			    <td height="17" colspan="4" class="head">Pay Slip of <?php echo $info->month." ".$info->year;?></td>
		      </tr>
			  <tr>
			    <td class="td_style">Employee Name</td>
			    <td width="250"><?php  echo $info->full_name;?></td>
			    <td class="td_style"> Deductions</td>
                
			    <td width="102"><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Employee ID</td>
			    <td height="21"><?php  echo $info->employee_code;?></td>
			    <td class="td_style">Expenses</td>
               
			    <td height="21"><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >CNIC</td>
			    <td height="21"><?php echo $info->cnic;?></td>
			    <td class="td_style">Allowances</td>
              
			    <td height="21"><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Cash Received By</td>
			    <td height="21"><?php echo $info->received_by;?></td>
			    <td class="td_style">Advances</td>
              
			    <td height="21"><?php 
					if(!empty($advance_trans->transaction_amount))
					{
					echo @$advance_trans->transaction_amount;}else{echo "0";}?></td>
		      </tr>
			  <tr>
			    <td class="td_style" >Remarks</td>
			    <td height="21"><?php  echo $info->remarks;?></td>
			    <td class="td_style">Transaction Date</td>
			    <td height="21"><?php 
				$date=$info->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
			    
		      </tr>
			  <tr>
			    <td class="td_style" >Base Salary</td>
			    <td height="21"><?php  echo $info->base_salary."/-";?></td>
			    <td height="21">&nbsp;</td>
			    <td height="21">&nbsp;</td>
		      </tr>
			  <tr>
          
			    <td height="21" colspan="4" align="right"><span style="text-align:center; font-weight:bold; color:#069;">Net Pay Salary:</span> &nbsp;&nbsp;&nbsp;<u><?php  echo "Rs: ".@$info->transaction_amount."/-";?>&nbsp;&nbsp;&nbsp;</u>  </td>
		      </tr>
		    </table>
			<p>&nbsp; </p>
            </div>
            <div id="form">
            <form action="" method="post" >
            <input type="hidden" name="next" value="gess" />
             <p style="margin-left:10px; color:#093">
			<?php echo "Remaining Slips: ".$no;?></p>
            <input type="submit" name="submit" value="Next" class="btn green" 
            style="margin-left:10px">
            </form>
            </div>
            <?php }}else{ echo "<span style='color:red'>Please Select Employee First !</span>";}?>
			</div>