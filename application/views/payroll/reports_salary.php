<?php if(!empty($info)){
$jan_amount=0;	$feb_amount=0;	$mar_amount=0;	$aprl_amount=0;
$may_amount=0;	$jun_amount=0;	$july_amount=0;	$aug_amount=0;
$sep_amount=0;	$oct_amount=0;	$nov_amount=0;	$dec_amount=0;

 foreach($info as $rec)
	{
		if($rec->month == 1){$amt=$rec->transaction_amount;$jan_amount+=$amt;}
		if($rec->month == 2){$amt=$rec->transaction_amount;$feb_amount+=$amt;}
		if($rec->month == 3){$amt=$rec->transaction_amount;$mar_amount+=$amt;}
		if($rec->month == 4){$amt=$rec->transaction_amount;$aprl_amount+=$amt;}
		if($rec->month == 5){$amt=$rec->transaction_amount;$may_amount+=$amt;}
		if($rec->month == 6){$amt=$rec->transaction_amount;$jun_amount+=$amt;}
		if($rec->month == 7){$amt=$rec->transaction_amount;$july_amount+=$amt;}
		if($rec->month == 8){$amt=$rec->transaction_amount;$aug_amount+=$amt;}
		if($rec->month == 9){$amt=$rec->transaction_amount;$sep_amount+=$amt;}
		if($rec->month == 10){$amt=$rec->transaction_amount;$oct_amount+=$amt;}
		if($rec->month == 11){$amt=$rec->transaction_amount;$nov_amount+=$amt;}
		if($rec->month == 12){$amt=$rec->transaction_amount;$dec_amount+=$amt;}
		
		 
	}
	$jan_amount;	$feb_amount;	$mar_amount;	$aprl_amount;
	$may_amount;	$jun_amount;	$july_amount;	$aug_amount;
	$sep_amount;	$oct_amount;	$nov_amount;	$dec_amount;
}else{ echo "No Records Found !";}
?>
<!-- contents -->
<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Reports / Salary Report </div> <!-- bredcrumb -->
	<?php $this->load->view('includes/payroll_left_nav'); ?>

	<div class="right-contents">

		<div class="head">Salary Reports</div>

			<!-- filter -->
			<!--<div class="filter">
				<h4>Filter By</h4>
				<select style="width:160px;">
					<option>Select Month</option>
				</select>
				<select style="width:160px;">
					<option>Select Project</option>
				</select>
				<select style="width:160px;">
					<option>Select Branch</option>
				</select>
				<select style="width:160px;">
					<option>Select department</option>
				</select>

				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>-->
		<hr>
	<div style="width: 50%; margin:20px auto;">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["January","February","March","April","May","June","July","August","September","October","November","December"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [
						<?php echo @$jan_amount;?>,
						<?php echo @$feb_amount;?>,
						<?php echo @$mar_amount;?>,
						<?php echo @$aprl_amount;?>,
						<?php echo @$may_amount;?>,
						<?php echo @$jun_amount;?>,
						<?php echo @$july_amount;?>,
						<?php echo @$aug_amount;?>,
						<?php echo @$sep_amount;?>,
						<?php echo @$oct_amount;?>,
						<?php echo @$nov_amount;?>,
						<?php echo @$dec_amount;?>
						]
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>
		</div>
	</div>
</div>
<!-- contents -->
