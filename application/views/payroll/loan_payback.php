<style type="text/css">

.designing{
	top:200px;
	right:200px;
	}

.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 alignment-adjust:central;
}
.disabled{
    background-color: #808080;
}
</style>
<script type="text/javascript">

</script>
<!-- contents -->
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Advances /  Pay Back</div> 
    <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">

		<div class="head">Advance Payback</div>
			
            <!-- hide/show -->
			
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
//			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
			<div class="form-left">
			<form action="payroll/loan_advance_payback" method="post">
				<div class="row2">
					<h4>Employee Name</h4>
			<input name="name" id="name" type="text" value="<?php echo @$info->full_name;?>" readonly="readonly">
            <input type="hidden" name="loan_advance_id" id="loan_advance_id" value="<?php echo @$info->loan_advance_id;?>" />
            <input type="hidden" name="emp_id" id="emp_id" value="<?php echo @$info->employee_id;?>" />
				<br class="clear">

                    <?php if($info->balance > 0){?>
				<div class="row2">
					<h4>Balance</h4>
					<input type="text" name="payback_amount" value="<?php echo @$info->balance;?>" id="payback_amount" onkeyup="total()">
                    <input type="hidden" name="total_amount" value="<?php echo @$info->balance;?>" id="total_amount">
                </div>
                    <?php }else{?>
                <div class="row2">
                   <h4>Payback Amount</h4>
                   <input type="text" name="payback_amount" value="<?php echo @$info->amount;?>" id="payback_amount" onkeyup="total()">
                    <input type="hidden" name="total_amount" value="<?php echo @$info->amount;?>" id="total_amount">
                </div>
                    <?php }?>
                    <span id="check" style='color: red; display: none'>Payment exceeded !<small></small></span>
                    <span id="remain" style='color: green; display: none'></span>
                    <script>
                        function total(){
                            $("#remain").text("");
                           // alert("working");
                        var amount=parseInt($("#payback_amount").val());
                        var total=parseInt($("#total_amount").val());
                        if((amount === total) || (amount < total))
                        {
                            $("#submitbtn").removeAttr('disabled');
                            $("#submitbtn").removeClass("disabled")
                            $("#check").hide();
                            var remain= total - amount;
                            $("#remain").append("Balance "+remain);
                            $("#remain").show(remain);
                        }
                        else
                        {
                            $("#check").show();
                            $("#submitbtn").attr('disabled',true);
                            $("#submitbtn").addClass("disabled");
                        }
                        }
                    </script>
				<!-- hide/show -->
				 <br class="clear">
                <div class="row1">
					<h4>Month</h4>
					<select name="month" required>
                   	<option value="" selected="selected">-- Select Month Salary --</option>
                    <?php foreach($months as $mon):?>
						<option value="<?php echo @$mon->ml_month_id;?>">
						<?php echo @$mon->month;?></option>
                        <?php endforeach;?>
					</select>
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Year</h4>
					<select name="year" required="required">
                   	<option value="" selected="selected">-- Select Year --</option>
                    <?php foreach($years as $yer):?>
						<option value="<?php echo @$yer->ml_year_id;?>"><?php echo @$yer->year;?></option>
                        <?php endforeach;?>
					</select>
				</div>
				<br class="clear">
				<div class="row2">
            		<h4>Payment Mode</h4>
                    <?php $selected=3; ?>
            		<?php echo form_dropdown('payment_mode',$payment_mode,$selected,'id="selected" ,required="required"')?>
            	</div>
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="no_cheque">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_cheque">
            	</div>
                 <?php if(!empty($account_info)){
					?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account" value="<?php echo $account_info->account_no;?>" readonly>
                    <input type="hidden" name="account_id" value="<?php echo $account_info->bank_account_id;?>" readonly/>
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account" value="<?php echo $account_info->account_title;?>" readonly>
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account" value="<?php echo $account_info->bank_name;?>" readonly>
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account" value="<?php echo $account_info->branch;?>" readonly>
                </div>
                     <div class="row1 account">
                    <h4>Branch Code</h4>
                    <input type="text" name="branch_code" value="<?php echo $account_info->branch_code;?>" readonly>
                </div>
                <?php }else{?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div>
                     <div class="row1 account">
                    <h4>Branch Code</h4>
                    <input type="text" name="branch_code">
                </div>
                <?php }?>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="recieved">
                </div>
				<div class="row1 cash">
					<h4>Remarks</h4>
					<input type="text" name="remarks">
				</div>
            <div class="row">
				<div class="button-group">
					<input type="submit" name="submit" class="btn green" id="submitbtn" value="Pay Back">
				</div>
                </div>
			 </form>
             </div>
           </div>
			<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<span class="headingfive">Employee Name</span>
                    	<i class="italica" id="employee_code"><?php echo @$info->full_name;?></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"><?php echo @$info->employee_code;?></i>
                    </div>
                    
                    <div class="row2">
                    	<span class="headingfive">Department</span>
                    	<i class="italica" id="department"><?php echo @$info->department_name;?></i>
				</div>
				<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"><?php echo @$info->designation_name;?></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Type</span>
                    	<i class="italica" id="cnic"><?php echo @$info->payment_type_title;?></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Amount</span>
                    	<i class="italica" id="amount"><?php echo @$info->amount;?></i>
				</div>
                <div class="row2">
                    <span class="headingfive">Balance</span>
                    <i class="italica" id="amount"><?php echo @$info->balance;?></i>
                </div>
                <div class="row2">
                    	<span class="headingfive">Due Date</span>
                    	<i class="italica" id="amount"><?php echo @$info->month;?> &nbsp;&nbsp;&nbsp;<?php echo @$info->year;?></i>
                        
				</div>
				
			</div>
    </div>
<!-- contents -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/payroll_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- leftside menu end -->

