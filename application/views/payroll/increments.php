<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -89.3%;
	top: 20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -60px;
	left: -65px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		    aoData.push({"name":"incr_type","value":$('#incr_type').val()});
                
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
</script>
<!-- contents -->
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Increments</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Increments</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
                <div class="row">
                <?php echo form_dropdown('incr_type',$increment_type,$slct_incr,"id='incr_type' class='row' onchange='this.form.submit()'")?>
				<?php echo form_dropdown('month',$month,$slct_m,"id='month' class='row' 		onchange='this.form.submit()'")?>
                </div>
                <div class="row">
           	   <?php echo form_dropdown('year',$year,$slct_y,"id='year' class='row' onchange='this.form.submit()'")?>								               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <span class="fa fa-print" style="cursor:pointer" title="Print All" onclick="print_all()"></span>
               </div>
                 <?php echo form_close();?>
			</div>

			
			<div id="print_table">
			<!-- table -->
			<table cellspacing="0" id="table_list" class="mytab slect">
				<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>Date (Effective From)</td>
					<td>Base Salary</td>
					<td>Increment type</td>
					<td>Increment Amount</td>
                    <td>Month</td>
                    <td>Year&nbsp;&nbsp;</td>
					<td>Status  &nbsp;</td>
					<td><span class="fa fa-pencil"></span></td>
				</thead>
				
			</table>
            </div>
            <div class="row">
				<div class="button-group">
					<a href="payroll/add_increment"><button class="btn green">New Increment</button></a>
					
				</div>
			</div> 

			<!-- button group -->
			<!-- <div class="row">
				<div class="button-group">
					<a href="new_increment.php"><button class="btn green">New Increment</button></a>
					<button class="btn red">Delete</button>
				</div>
			</div> -->

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}
</script>