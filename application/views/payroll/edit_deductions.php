 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<!-- contents -->
<script>
$(document).ready(function(){
				//$("#due_date").hide();
                $("#due_month").hide();
                $("#due_year").hide();
				$("#selected").change(function(e){
					if($(this).val()==1){
                        $("#due_month").hide();
                        $("#due_year").hide();
                    }
						///$("#due_date").hide();}
                    if($(this).val()==2){
						$("#due_month").show();
                        }
                    if($(this).val()==3){
						$("#due_month").show();
						$("#due_year").show();
                        }
					});
			});
</script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Deductions / Edit Deductions</div>
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head">Edit Deductions</div>
			<form role="form" method="post" action="payroll/updte_deduction_record">
				<div class="row">
				<h4>Employee Name</h4>
			   <input name="name"  type="text"  value="<?php echo $info->full_name; ?>" readonly="readonly">
			   <input type="hidden" name="ded_id" id="ded_id" value="<?php echo $info->deduction_id; ?>" />
			   </div>
				<br class="clear">
				<div class="row">
					<h4>Deduction Type</h4>
      				 <?php
					 $selected =(isset($info->ml_deduction_type_id) ? $info->ml_deduction_type_id:'');
					 echo form_dropdown('deduction_type',$deduction,$selected)?>
                   
				</div>
				<br class="clear">
                <div class="row">
					<h4>Deduction Frequency</h4>
                   <!-- <input type="text" name="ded_freq" value="<?php /*echo $info->pay_frequency;*/?>" readonly="readonly"/>
                    <input type="hidden" name="deduction_freq" value="<?php /*echo $info->deduction_frequency;*/?>" readonly="readonly"/>
                    --><?php
                    $selected =(isset($info->deduction_frequency) ? $info->deduction_frequency:'');
                    echo form_dropdown('deduction_freq',$dedfrqcy,$selected,'id="selected" required="required"')?>
                    <?php  //$selected =(isset($info->deduction_frequency) ? $info->deduction_frequency:'');
                    //echo form_dropdown('deduction_freq',$dedfrqcy,$selected,'readonly id="selected"')?>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Amount (<?php echo currency_label();?>)</h4>
					<input type="text" name="amount" value="<?php echo $info->deduction_amount; ?>">
				</div>
              <br class="clear">
               <div id="due_date">
                   <div class="row" id="due_month">
					<h4>Month</h4>
					<?php echo form_dropdown('month',$months,'','id="due_month"')?>
				</div>
				<br class="clear">
				<div class="row" id="due_year">
					<h4>Year</h4>
					<?php echo form_dropdown('year',$years,'','id="due_year"')?>
				</div>
                </div>

					
                <br class="clear">
				                
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<input type="submit" class="btn green" name="add_rec" value="Update Deductions">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		<?php //endforeach?>
        </form>

	</div>
    </div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
        $(document).ready(function(){
            $("#due_month").hide();
            $("#due_year").hide();
            $("#selected").change(function(e){
                if($(this).val()==1){
                    $("#due_month").hide();
                    $("#due_year").hide();
                }
                else if($(this).val()==2)
                {
                    $("#due_month").show();
                }
                else if($(this).val()==3)
                {
                    $("#due_month").show();
                    $("#due_year").show();
                }
            });
        });
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
</body>
</html>