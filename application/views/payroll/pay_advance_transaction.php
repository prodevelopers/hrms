<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 alignment-adjust:central;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	
	
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Advance Salary / Pay Advance Salary</div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Advance Payment for </div>
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
<div class="form-left">
			<form action="payroll/loanadvance_transaction" method="post" role="form">
				<div class="row2">
					<h4>Employee Name</h4>
		<input name="name" id="name"  type="text"  value="<?php echo $auto_fill->full_name;?>" readonly="readonly" >
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
   <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $auto_fill->employee_id;?>"  />
    <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $auto_fill->loan_advance_id;?>"  />
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Payment Type</h4>
					<?php //echo form_dropdown('payment_type',$payment_type)?>
                    <input type="text" name="payment_type" value="<?php echo $auto_fill->payment_type_title;?>" readonly="readonly" >
				</div>
				<br class="clear">
                    <div class="row2">
                        <h4>Payment Amount</h4>
                        <input type="text" name="payment_amount" value="<?php echo $auto_fill->amount;?>" readonly>
                    </div>
                <br class="clear">
                 <div class="row1">
					<h4>Month</h4>
             <input type="text" name="month_show"value="<?php echo $auto_fill->month;?>" readonly="readonly">
		<input type="hidden" name="month"value="<?php echo $auto_fill->ml_month_id;?>" readonly="readonly">
						
				</div>
				<br class="clear">
				<div class="row1">
					<h4>Year</h4>
		<input type="text" name="year_show"value="<?php echo $auto_fill->year;?>" readonly="readonly">
		<input type="hidden" name="year"value="<?php echo $auto_fill->ml_year_id;?>" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Payment mode</h4>
					<?php echo form_dropdown('payment_mode',$payment_mode,'class="se"','id="selected"')?>
				</div>
                <br class="clear">
				<!--<div class="row2">
					<h4>Status</h4>
					<?php //echo form_dropdown('status',$status,'required="required"','required="required"')?>
				</div>-->
            	<br class="clear">
            	<div class="row1 cheque">
            		<h4>Cheque No</h4>
            		<input type="text" name="cheque_no">
            	</div>
            	<div class="row1 cheque">
            		<h4>Bank</h4>
            		<input type="text" name="bank_chiq">
            	</div>
                <!--<div class="row1 account" style="display:none">-->
                    <?php if(!empty($account_info)){
					?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account" value="<?php echo $account_info->account_no;?>">
                    <input type="hidden" name="account_id" value="<?php echo $account_info->bank_account_id;?>" />
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account" value="<?php echo $account_info->account_title;?>">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account" value="<?php echo $account_info->bank_name;?>">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account" value="<?php echo $account_info->branch;?>" >
                </div>
                        <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_code" value="<?php echo $account_info->branch_code;?>" >
                </div>
                <?php }else{?>
                <div class="row1 account">
                    <h4>Account No</h4>
                    <input type="text" name="no_account">
                </div>
                <div class="row1 account">
                    <h4>Account Title</h4>
                    <input type="text" name="title_account">
                </div>
                <div class="row1 account">
                    <h4>Bank</h4>
                    <input type="text" name="bank_account">
                </div>
                <div class="row1 account">
                    <h4>Branch</h4>
                    <input type="text" name="branch_account">
                </div
                   <div class="row1 account">
                    <h4>Branch Code</h4>
                    <input type="text" name="branch_code">
                </div>
                <?php }?>
                <div class="row1 cash">
                    <h4>Received By</h4>
                    <input type="text" name="recieved">
                </div>
                <div class="row1 cash">
                    <h4>Remarks</h4>
                    <input type="text" name="remarks">
                </div>
				<br class="clear">
				<div class="row2">
					<input type="submit"  class="btn green" value="Pay Advance" />
				</div>
				
			</form>
</div>
<div class="form-right">
<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<span class="headingfive">Advance Month/Year</span>
                    	<i class="italica" id="employee_name"><?php echo $auto_fill->month." , ".$auto_fill->year;?></i>
                    </div>
                  <!--  <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"><?php //echo $auto_fill->employee_code;?></i>
                    </div>-->
               
                    <div class="row2">
                    	<span class="headingfive">Type</span>
                    	<i class="italica" id="department"><?php echo $auto_fill->payment_type_title;?></i>
				</div>
				<!--<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"><?php //echo $auto_fill->designation_name;?></i>
				</div>-->
                <div class="row2">
                    	<span class="headingfive">Advance</span>
                    	<i class="italica" id="position"><?php echo $auto_fill->amount;?></i>
				</div>
                
			</div>
    </div>
	
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="#"><button class="btn green"></button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->
