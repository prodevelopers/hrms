<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	top: 10px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	top: -48px;
	font-size: 1px;
	left: 49%;
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -50px;
left: -12em;
}
.dataTables_filter input{
	width: 180px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.table_style {
	margin:0 auto;
	float: left;
}
input[type="text"] {
   padding: 0px;
}
</style>

<script type="text/javascript">
function print_report(id,month,year){
	window.open('payroll/print_advancesalary/'+id+'/'+month+'/'+year, "width=800,hight=600");
	pr.onload=pr.print() ;
	<!--pr.focus();-->
	}

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Loan/Advances</div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>  

	<div class="right-contents">

		<div class="head">Advances</div>

			  <!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
                <div class="row2">

                <?php echo @form_dropdown('name',$employees_name,$name,"id='name' class='resize' onchange='this.form.submit()'")?>
                <?php echo @form_dropdown('adnc_type',$advance_type,$slct_adnc,"id='adnc_type' class='resize' onchange='this.form.submit()'")?>
				<?php echo @form_dropdown('month',$month,$slct_m,"id='month' class='resize' onchange='this.form.submit()'")?>
           	   <?php echo @form_dropdown('year',$year,$slct_y,"id='year' class='resize' onchange='this.form.submit()'")?>								               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               </div>
               <span class="fa fa-print move" style="cursor:pointer" title="Print All" onclick="print_all()"></span>

                 <?php echo form_close();?>
			</div>

			
			
			<!-- table -->
			<div class="table-responsive">
            <div id="print_table">
				<table cellspacing="0" class="table_style">
				<thead class="table-head">
					<td>Code &nbsp;</td>
					<td>Name &nbsp;</td>
                    <td>Request Date&nbsp;</td>
					<td>Type</td>
					<td>Month / Year</td>
                   <!-- <td>Year</td>-->
					<td>Amount</td>
					<td>Request Status</td>
                    <td>Pay</td>
                    <td>Balance</td>
                    <td>Transaction Status</td>
                    <td><span class="fa fa-pencil"></span> &nbsp; &nbsp; | &nbsp;&nbsp;<span class="fa fa-print">
                    </span></td>
                    <td>Pay back</td>
                    <?php if(!empty($info)){?>
				</thead>
                <?php foreach($info as $rec){
					//$back_month=$rec->back_month;
					//$back_year=$rec->back_year;
					//$loan_month=$rec->loan_month;
					//$loan_year=$rec->loan_year;?>
				<tr class="table-row">
                    <td><?php echo $rec->employee_code; ?> &nbsp;</td>
					<td><?php echo $rec->full_name; ?> &nbsp;</td>
                    <td><?php date_format_helper($rec->date_created);?></td>
					<td><?php echo $rec->payment_type_title; ?></td>
					<td><?php echo $rec->month." / ".$rec->year; ?></td>
                   <!-- <td><?php //echo $rec->year; ?></td>-->
					<td><?php echo Active_currency($rec->amount); ?></td>
                    
					<td><?php if($rec->status == 2)
					{echo "<span style='color:green'>Approved</span>";}
					else{echo "<span style='color:red'>".$rec->status_title."</span>";}?><!--<a href="payroll/loan_status_update/<?php //echo $rec->status_title;?>/<?php //echo $rec->loan_advance_id;?>">
                     <?php  //echo $rec->status_title;?></a>--><?php //}?>
                     </td>

                    <td>
                   <?php  
					if($rec->status == 2 && $rec->paid == 0)
					{?>
                     <a href="payroll/pay_advance_transaction/<?php echo $rec->loan_advance_id;?>">
                         <span class="fa fa-money"></span></a>
					 <?php }elseif($rec->status == 2 && $rec->paid== 1)
                    {echo "<span style='color:green;'>Paid</span>";}else{}?>
					</td>
                    <td><?php echo Active_currency($rec->balance);?></td>
                    <td><?php if($rec->status == 1 && $rec->paid == 0){echo " ";}
                    elseif($rec->status == 2 && $rec->paid == 0){echo "";}
                    elseif($rec->status == 2 && $rec->paid == 1 && $rec->trans_status==1){echo "<span style='color: red;'>Pending</span>";}
                    elseif($rec->status == 2 && $rec->paid == 1 && $rec->trans_status==2){echo "<span style='color: green;'>Approved</span>";}?>
                    </td>
                   <td> <?php if($rec->status == 2)
					{echo " ";}else{?>
                    <a href="payroll/edit_loan_advance/<?php echo $rec->loan_advance_id;?>"><span class="fa fa-pencil"></span></a>
					<?php }?> &nbsp; &nbsp; | &nbsp;&nbsp;
                    <?php if($rec->status == 2){?>
                    <span onclick="return print_report(<?php echo $rec->employee_id;?>,<?php echo $rec->loan_month;?>,<?php echo $rec->loan_year;?>);" class="fa fa-print" style="cursor:pointer"></span><?php }else{}?>
                    </td>
                    
                    <td><?php
					//$loanadvance_id=$rec->loan_advance_id;
					//$load_advance_id_back=$rec->loanadvance_id_back;
		if($rec->status == 2 && $rec->paid_back == 1)
		{echo "<span style='color:#181818 ;'>Paid back</span>";}
		elseif($rec->status == 2 && $rec->paid == 0){}
		elseif($rec->status == 2 && $rec->paid == 1 && $rec->paid_back == 0 && $rec->trans_status==2){?>
        <a href="payroll/advance_loan_payback/<?php echo $rec->employee_id; ?>/
		<?php echo $rec->loan_advance_id; ?>" style="color:green">Pay back</a>
					<?php }?>
                    </td>
                </tr>
                <?php }}else{echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:red'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sorry no Records Found !</span>";}?>
              
			</table>
			</div>
			
            </div>
             <br />
             <br />
        <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="payroll/pay_advance"><button class="btn green">New Advance Request</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}

</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });



    </script>
<script>
    $(document).ready(function(e){
        $('#adnc_type,#month,#year,#name').select2();
    })
</script>
    <!-- leftside menu end -->