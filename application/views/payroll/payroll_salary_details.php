<!-- contents -->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Payroll / Payroll Salary/ Salary Detail</div>
    <?php $this->load->view('includes/payroll_left_nav'); ?>
	<div class="right-contents">
		<div class="head">Salary detail of &nbsp;<?php echo $details->full_name;?> of <?php echo $details->month;?> &nbsp;<?php echo $details->year;?></div>
			<form>
				<div class="row">
					<h4>Code</h4>
					<h4><i><?php echo $details->employee_code;?></i></h4>
				</div>
				<div class="row">
					<h4>Name</h4>
					<h4><i><?php echo $details->full_name;?></i></h4>
				</div>
				<div class="row">
					<h4>Designation</h4>
					<h4><i><?php echo $details->designation_name;?></i></h4>
				</div>
				<div class="row">
					<h4>Pay Grade</h4>
					<h4><i><?php echo $details->pay_grade;?></i></h4>
				</div>
                <div class="row">
					<h4>Base Salary</h4>
					<h4><i><?php echo $details->base_salary;?></i></h4>
				</div>
				<div class="row">
					<h4>Allowances</h4>
                   <?php
				   	$alwnc_month=$details->alwnc_month;
					$alwnc_year=$details->alwnc_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$allowance_freq=$details->pay_frequency;
					if($allowance_freq = 1)
					{$alwnc_amount=$details->allowance_amnt;}
				elseif($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$details->allowance_amnt;}else{$alwnc_amount=0;} 
					 ?>
					<h4><i><?php echo $alwnc_amount;?></i></h4>
				</div>
                <div class="row">
					<h4>Expenses</h4>
                  <?php
				    $exp_id=$details->expense_claim_id;
					$exp_dis_id=$details->expense_id;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$exp_month=$details->exp_month;
					$exp_year=$details->exp_year;
					if($exp_id == $exp_dis_id)
					{$expense_amount=0;}
					elseif($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$details->exp_amount;}else{$expense_amount=0;}
					 ?>
					<h4><i><?php echo $expense_amount;?></i></h4>
				</div>
				<div class="row">
					<h4>Deductions</h4>
                     <?php
				   	$ded_month=$details->ded_month;
					$ded_year=$details->ded_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					$pay_requency=$details->deduction_frequency;
					if($pay_requency = 1)
					{$deduction_amount=$details->ded_amount;}
					elseif($salary_month == $ded_month && $salary_year == $ded_year)
				 {$deduction_amount=$details->ded_amount;}
				 else{$deduction_amount=0;}?>
					<h4><i><?php echo $deduction_amount;?></i></h4>
				</div>
				<div class="row">
					<h4>Withholdings</h4>
					<h4><i><?php echo $details->full_name;?></i></h4>
				</div>
				
				<div class="row">
					<h4>Loan/Advances</h4>
                    <?php
					$loan_advance_id=$details->loan_advance_id;
					$loan_payback_id=$details->loan_payback_id;
				   	$lonad_month=$details->alonad_month;
					$lonad_year=$details->alonad_year;
					$salary_month=$details->salary_month;
					$salary_year=$details->salary_year;
					if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					else
					if($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$details->loan_amount;}else{$loan_advance=0;} 
					 ?>
					<h4><i><?php echo $loan_advance;?></i></h4>
				</div>
				<div class="row">
					<h4>Total</h4>
                     <?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?>
					<h4><i><?php echo $payable;?></i></h4>
				</div>
			</form>
		</div>
	</div>
<!-- contents -->
