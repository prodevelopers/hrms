<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Assign Job</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Pay Remain Salary </div>

			
			<!-- hide/show -->
			
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
			<div class="form-left">
			<form action="payroll/pay_salary_amount" method="post">
            
				<div class="row1">
					<h4>Employee Code</h4>
<input type="text" name="employee_code" id="employee_code" value="<?php echo $remain_salary->employee_code; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Employee Name</h4>
					 <input name="emp_name" id="emp_name" type="text" value="<?php echo $remain_salary->full_name;?>" readonly="readonly">
   <input type="hidden" name="emp_id" id="emp_id"  />
   <input type="hidden" name="salary_id" id="salary_id"  />
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>

				</div>
                <br class="clear">
				<div class="row1">
					<h4>Pay Grade</h4>
		<input type="text" name="pay_grade" id="pay_grade" value="<?php echo $remain_salary->pay_grade; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Salary Month</h4>
	<input type="text" name="salary_month" id="salary_month" value="<?php echo $month->month; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Salary Year</h4>
	<input type="text" name="salary_year" id="salary_year" value="<?php echo $year->year; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Base Salary</h4>
		<input type="text" name="base_salary" id="base_salary" value="<?php echo $remain_salary->base_salary;?>" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row1">
					<h4>Transaction Amount</h4>
		<input type="text" name="trans_amount" id="trans_amount" value="<?php echo $remain_salary->transaction_amount; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Allowance Amount</h4>
		<input type="text" name="allowance_amount" id="allowance_amount" value="<?php echo $remain_salary->allowance_amount; ?>" readonly="readonly">
				</div>
                 <br class="clear">
				<div class="row1">
					<h4>Deduction Amount</h4>
		<input type="text" name="deduction_amount" id="deduction_amount" value="<?php echo $remain_salary->deduction_amount; ?>" readonly="readonly">
				</div>
                <br class="clear">
				<div class="row1">
					<h4>Loan/Advance</h4>
		<input type="text" name="loan_amount" id="loan_amount" value="<?php echo $remain_salary->loan; ?>" readonly="readonly">
				</div>
                 <br class="clear">
				<div class="row1">
					<h4>Expense Amount</h4>
		<input type="text" name="expense_amount" id="expense_amount" value="<?php echo $remain_salary->amount; ?>" readonly="readonly">
       <!--<input type="hidden" name="payable" id="payale" value="<?php //echo $remain_salary->payable; ?>"/>-->
				</div>
                <?php $payable_amount=$remain_salary->payable;
					  $base_salary=$remain_salary->base_salary
				?>
                <br class="clear">
				<div class="row1">
					<h4>Payable Amount</h4>
                   
		<input type="text" name="payable_amount" id="payable_amount" value="<?php echo $payable; ?>" readonly="readonly">
				</div>
                <!-- <script>
                 $(document).ready(function(){
					 trans_amount=$("#trans_amount").val();
					 payable_amount=$("#payable_amount").val();
					  base_salary=$("#base_salary").val();
					  total=payable_amount-base_salary
					 // alert(total);
					 final_amount=payable_amount-trans_amount;
					 $("#remain_amount").val(final_amount);
					 
					 });
                 </script>-->
                 <?php
				 $transaction_amount=$remain_salary->transaction_amount;
				 $remain_amount=$payable-$transaction_amount;
				  ?>
                <br class="clear">
				<div class="row1">
					<h4>Remain Amount</h4>
		<input type="text" name="remain_amount" id="remain_amount" value="<?php echo $remain_amount; ?>" readonly="readonly">
				</div>
               <script>
			   var remain=$("#remain_amount").val();
               if(remain==0)
			   {
					alert("GO Back Salary Piad Full   !")
			   }
               </script>
               
				<div class="row">
				<div class="button-group">
		<input type="submit" class="btn green" id="sumit" value="Pay Salary" />
				
				</div>
			</div>
            </form>
            </div>
            
			