
<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<style>
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
    .sendbtn{
        margin-right: 5px;
        float:right;
        display: inline-block;
        padding: 3px 12px;
        margin-bottom: 0px;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -moz-user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;}
</style>
<!--<style>
	thead  th {
		font-size:15px;
	}
	.infor th{
		font-size: 14px;
	}
	tbody td{
		font-size:14px;
	}
	.detail td{
		font-size: 13px;
	}
</style>-->
<script>
	function print_table(id) {
		var printContents = document.getElementById(id).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
	</script>

<div class="contents-container">
	<div class="bredcrumb">Dashboard / Payroll / Payroll Register Detail</div>
  	<div id="print_area">
	 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
<div class="right-contents">
		<div class="head" style="font-size:1em; font-weight:bold;">PayRoll Record <?php echo @$details->month.", ".$details->year; ?>
			<button type="button" id="sendEmailButton" class="sendbtn"  style="float:right" data-toggle="modal" data-target="#myModal">
				Send
			</button>
			<!--<a href="payroll/print_payroll_detail/<?php /*echo $this->uri->segment(3);*/?>/<?php /*echo $this->uri->segment(4);*/?>/<?php /*echo $this->uri->segment(5);*/?>"><input type="button"  name="email" class="btn green" style="float:right" value="Email"></a>-->
	<input type="button"  name="print" onclick="print_table('print_area')" class="btn green btn-sm" style="float:right" value="Print"></div>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:30%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="2" style="text-align:left;">Employee Infomation</th>
				</tr>
				<tr>
					<td>Name</td>
					<td><?php echo @$details->full_name;?></td>
				</tr>
				<tr>
					<td>Employee Code</td>
					<td><?php echo @$details->employee_code;?></td>
				</tr>
				<tr>
					<td>Designation</td>
					<td><?php echo @$details->designation_name;?></td>
				</tr>
				<tr>
					<td>Pay Grade</td>
					<td><?php echo @$details->pay_grade;?></td>
				</tr>
				
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:20%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Salary Info</th>
				</tr>
				<tr >
					<td>Base Salary</td>
					<td><?php echo @$details->base_salary;?></td>
				</tr>
				<tr>
					<td>Deductions</td>

                    <td style="color:darkred"> - <?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></td>
				</tr>
				<tr>
					<td>Allowances</td>

                    <td><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></td>
				</tr>	
				<tr>
					<td>Expenses</td>

                    <td><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></td>
				</tr>	
				<tr>
					<td>Advance Paybacks</td>

					<td style="color: darkred;"> - <?php
					if(!empty($advance_trans->advance_trans_amount))
					{
					echo @$advance_trans->advance_trans_amount;}else{echo "0";}?>
                  </td>
				</tr>
				<tr>
					<td>Last Arrears</td>
					<?php if(!empty($last_salary->arears)){?>
						<td><?php echo $last_salary->arears ;?></td>
					<?php }else{?><td>0</td><?php }?>
				</tr>
				<tr>
					<td>Total Paid</td>

					<td><?php echo Active_currency($salary_info->transaction_amount);?></td>
				</tr>


			</table>
		<table class="table beauty" cellpadding="0" cellspacing="0" style="width:20%;margin-left:1em; float:left;">
			<tr class="head">
				<th colspan="8" style="text-align:left;">Balance Details</th>
			</tr>
			<tr>
				<td>Balance </td>
				<td>
						<?php echo Active_currency(@$salary_info->arears);?></td>
			</tr>

		</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:20%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8">Deductions</th>
				</tr>
				<tr>
				<?php
				$ded_type=@$ded_trans->ded_rectp; $ded_amt=@$ded_trans->ded_recmnt;
				?>
					<td>
				<?php $explode_dedtyp=explode(",",$ded_type);
				$explode_dedamt=explode(",",$ded_amt);
				?>

				<?php foreach($explode_dedtyp as $ded_typ)
				{
					echo $ded_typ."<br />";

				}?>
					</td>
					<td>
				<?php foreach($explode_dedamt as $ded_mt)
				{
					echo $ded_mt."<br />";

				}
				?></td>
				</tr>
				<tr>
				<td class="headingfive">Total <!--Deductions--> </td>
					<td><?php
					$deduction_amount=@$ded_trans->total_ded_amount;
					echo Active_currency($deduction_amount);?></td></tr>
			</table>
		<br class="clear">

			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:22%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Allowances</th>
				</tr>

				<tr>
					<?php
					$alw_rectp=@$allw_trans->alw_rectp; $alw_recm=@$allw_trans->alw_recm;
					?>
					<td>
						<?php $explode_alw_rectp=explode(",",$alw_rectp);
						$explode_alw_recm=explode(",",$alw_recm);
						?>

						<?php foreach($explode_alw_rectp as $alw_typ)
						{
							echo $alw_typ."<br />";
						}?>
					</td>
					<td>
							<?php foreach($explode_alw_recm as $alw_mt)
							{
								echo $alw_mt."<br />";
							}
							?></td>
				</tr>
				<tr>
					<td class="headingfive">Total</td>
					<td><?php
							$allowance_amount=@$allw_trans->total_alw_recm;
							echo Active_currency($allowance_amount);?></td></tr>

			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:22%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Expenses</th>
				</tr>

				<tr>
					<?php
					$exp_rectp=@$exp_trans->exp_rectp; $exp_recm=@$exp_trans->exp_recm;
					?>
					<td>
						<?php $explode_exp_rectp=explode(",",$exp_rectp);
						$explode_exp_recm=explode(",",$exp_recm);
						?>

						<?php foreach($explode_exp_rectp as $expense_type)
						{
							echo $expense_type."<br />";
						}?>
					</td>
					<td>
							<?php foreach($explode_exp_recm as $expense_amt)
							{
								echo $expense_amt."<br />";
							}
							?></td>
				</tr>
				<tr>
					<td class="headingfive">Total </td>
					<td><?php
							$exp_amount=@$exp_trans->total_exp_recm;
							echo Active_currency($exp_amount);?></td></tr>
                    </table>


       
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:22%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Advances</th>
				</tr>
				<tr>
					<?php
					$advance_rectp=@$advance_trans->loan_rectp; $advance_recm=@$advance_trans->loan_recm;
					?>
					<td>
						<?php $explode_advance_rectp=explode(",",$advance_rectp);
						$explode_advance_recm=explode(",",$advance_recm);
						?>

						<?php foreach($explode_advance_rectp as $advance_type)
						{
                            if(!empty($advance_type))
                            {
							echo $advance_type."<br />";
                            }elseif(!empty($advance_trans->advance_trans_amount)){ echo "Balance"."<br />";}
						}?>
					</td>
					<td>
							<?php foreach($explode_advance_recm as $advance_amt)
							if(!empty($advance_amt))
                            {
								echo $advance_amt."<br />";
                            }
                            elseif(!empty($advance_trans->advance_trans_amount))
                            {echo $advance_trans->advance_trans_amount;}
							?></td>
				</tr>
				<tr>
					<td class="headingfive">Total </td>
					<td>
					<?php
					$advance_amount=@$advance_trans->total_advamount;
                    if(!empty($advance_amount)){
					echo $advance_amount;}
                    elseif(!empty($advance_trans->advance_trans_amount)){
                        echo Active_currency($advance_trans->advance_trans_amount);
                    }?>
					</td></tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:22%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Payment Mode </th>
				</tr>

				<?php if(!empty($payment_mode->account_id)){?>
				<tr >
					<td>Account Title</td>
					<td><?php
					echo $payment_mode->account_title;?></td>
				</tr>
				<tr >
					<td>Account Number</td>
					<td><?php
					echo $payment_mode->account_no;?></td>
				</tr>
				<tr >
					<td>Bank</td>
					<td><?php
					echo $payment_mode->bank_name;?></td>
				</tr>
				<tr >
					<td>Branch</td>
					<td><?php
					echo $payment_mode->branch;?></td>
				</tr>
                    <tr >
					<td>Branch Code</td>
					<td><?php
					echo $payment_mode->branch_code;?></td>
				</tr>
				<?php }if(!empty($payment_mode->cash_id)){?>
					<tr >
						<td>Received By</td>
						<td><?php
								echo $payment_mode->received_by;?></td>
					</tr>
					<tr >
						<td>Remarks</td>
						<td><?php
								echo $payment_mode->remarks;?></td>
					</tr>
				<?php }elseif(!empty($payment_mode->cheque_id)){?>
					<tr >
						<td>Cheque No</td>
						<td><?php
								echo $payment_mode->cheque_no;?></td>
					</tr>
					<tr >
						<td>Bank</td>
						<td><?php
								echo $payment_mode->chk_bank;?></td>
					</tr>
				<?php }?>


			</table>


		<table class="table beauty" cellpadding="0" cellspacing="0" style="width:20%;margin-left:1em; float:left;">
			<tr class="head">
				<th colspan="8" style="text-align:left;">Transaction Details</th>
			</tr>
			<tr >
				<td>Transaction Date</td>
				<td><?php
						$date=$details->transaction_date;
						$new=date("d-m-Y",strtotime($date));
						echo @$new;?></td>
			</tr>
			<tr >
				<td>Transaction ID</td>
				<td><?php echo @$details->transaction_id;?></td>
			</tr>
			<tr >
				<td>Reviewed By</td>
				<?php if($details->status==2){?>
				<td><?php echo @$details->name;?></td>
			</tr>
			<tr>
				<td>Approved Date</td>
				<td> <?php
						$date=$details->ap_date;
						if(!empty($date)){
							$new=date("d-m-Y",strtotime($date));
							echo @$new;}else{echo "";}}?></td>
			</tr>
		</table>

			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:20%;margin-left:1em; float:left;">
				<tr class="head">
					<th colspan="2" style="text-align:left;">Previous Salaries Record of <?php echo @$details->year; ?></th>
				</tr>
                <?php if(!empty($salaries)){
					foreach($salaries as $salary){
					$month=$salary->month;
					$current_year=$salary->year;
					$salary_amount=Active_currency($salary->transaction_amount);
					if($month=="Jan")
					{$jan_salary=$salary_amount;}
					if($month=="Feb")
					{$feb_salary=$salary_amount;}
					if($month=="Mar")
					{$mar_salary=$salary_amount;}
					if($month=="Apr")
					{$apr_salary=$salary_amount;}
					if($month=="May")
					{$may_salary=$salary_amount;}
					if($month=="Jun")
					{$jun_salary=$salary_amount;}
					if($month=="Jul")
					{$jul_salary=$salary_amount;}
					if($month=="Aug")
					{$aug_salary=$salary_amount;}
					if($month=="Sep")
					{$sep_salary=$salary_amount;}
					if($month=="Oct")
					{$oct_salary=$salary_amount;}
					if($month=="Nov")
					{$nov_salary=$salary_amount;}
					if($month=="Dec")
					{$dec_salarys=$salary_amount;}
					}}else{echo "Sorry no previous salary !";}
					
					?>
				<tr >
					<td>Jan / Feb</td>
					<td><?php if(!empty($jan_salary)){echo @$jan_salary;}
					else{echo "0";}?> / 
					<?php if(!empty($feb_salary)){echo @$feb_salary;}
					else{ echo "0";}?></td>
				</tr>
				<tr >
					<td>Mar / Apr</td>
					<td><?php if(!empty($mar_salary)){echo @$mar_salary;}
					else{echo "0";}?> / 
					<?php if(!empty($apr_salary)){echo @$apr_salary;}
					else{ echo "0";}?></td>
				</tr>
				<tr >
					<td>May / Jun</td>
					<td><?php if(!empty($may_salary)){echo @$may_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($jun_salary)){echo @$jun_salary;}
					else{ echo "0";}?></td>
				</tr>
				<tr >
					<td>Jul / Aug</td>
					<td><?php if(!empty($jul_salary)){echo @$jul_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($aug_salary)){echo @$aug_salary;}
					else{ echo "0";}?></td>
				</tr>
				<tr >
					<td>Sep / Oct</td>
					<td><?php if(!empty($sep_salary)){echo @$sep_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($oct_salary)){echo @$oct_salary;}
					else{ echo "0";}?></td>
				</tr>
				<tr >
					<td>Nov / Dec</td>
					<td><?php if(!empty($nov_salary)){echo @$nov_salary;}
					else{ echo "0";}?> / 
					<?php if(!empty($dec_salarys)){echo @$dec_salarys;}
					else{ echo "0";}?></td>
				</tr>
			</table>

			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:40%; margin-top:2em;">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Total Amount of <?php echo @$details->month." ".$details->year; ?></th>
				</tr>
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><?php echo @$details->salary_date;?></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>Net Paid</td>
					<td><?php /*?><?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?><?php */?>
					<?php echo Active_currency($salary_info->transaction_amount)." /-";?></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<!-- Modal -->
<!--<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Email To:</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="mailForm">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">To</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">From</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="FromEmail"  id="inputPassword3" placeholder="Admin@gmail.com">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
						<div class="col-sm-9">
							<textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
						<div class="col-sm-9">
							<label  class="col-sm-2 control-label"><?php /*echo  $this->session->flashdata('fileName');*/?></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</div>-->
<!----------------------------Send Email Without Bootstrap libraries----------------------------->
<div id="myModal" class="modal-demo" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Email To</h3>
    </div>
    <form class="form-horizontal" id="mailForm">

        <div class="pop_row">
            <div class="left_side">
                <h5>To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="ToEmail" id="inputEmail3" placeholder="Email" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Reply To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="FromEmail"  id="inputPassword3" placeholder="Admin@gmail.com" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Subject</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" value="Attached Generated Payroll PDF" name="MessageSubject" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Message</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Attachment</h5>
            </div>
            <div class="right_side">
                <?php echo  $this->session->flashdata('fileName');?>
            </div>
        </div>


        <div class="btn-row">
            <input type="button" class="btn-pop" id="sendMailWithAttachment" value="Send">
        </div>
    </form>

</div>

<!------------------Script of the Email Btn without Libraies of Bootstrap-------->
<script>
    $('#sendEmailButton').on('click', function(e){
        Custombox.open({
            target: '#myModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });
</script>


<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
	$(document).ready(function(){
		$('#sendMailWithAttachment').on('click', function (e) {
			var formData = $('#mailForm').serializeArray();
			<?php if(isset($pdfView) && !empty($pdfView)){ ?>
			formData.push({name:"viewData",value:<?php echo json_encode($pdfView); ?>});
			<?php } ?>
			var targetURL = '<?php echo base_url();?>payroll/sendGeneratedPDFReport';
			$.ajax({
				url: targetURL,
				data:formData,
				type:"POST",
				success: function (output) {
					var data = output.split("::");
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});
	});
</script>
