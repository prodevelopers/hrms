<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<style>
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }
    .sendbtnupdte,.csvupdte,.sendbtnpdf{
        margin-left: 5px;
        float:right;
        display: inline-block;
        padding: 6px 0px;
        margin-bottom: 0px;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -moz-user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;}

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
    .btn-row {
        float: right;
        width: 225px;
        height: 40px;
        margin-right: 110px;
    }

    .select2-results li{
        width: 300px;
        border-bottom:thin solid #ccc;
        height:60px;
    }
    .select2TemplateImg { padding:0.2em 0;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .resize{width: 220px;}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/css/bootstrap.css" type="text/css"/>-->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Payroll sheet </div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
	<div class="head"> Payroll sheet Report</div>
    <div class="filter">
        <h4> Filter By: </h4>
        <div class="row1">
        <form action="payroll/salary_sheet" method="post" id="print_sheet"><!--
            <input type="text" style="width:200px;" name="code" id="code" placeholder="Search by Code.." onchange="this.form.submit()" />-->
            <input type="hidden"  name="name"  id="selectEmployee" onchange="this.form.submit();"/>
            <input type="hidden" name="month" value="<?php echo $month;?>"/>
            <input type="hidden" name="year" value="<?php echo $year;?>"/>
            <input type="hidden" name="filter_name" value="<?php echo @$name;?>"/>
            <input type="hidden" name="filter_code" value="<?php echo @$code;?>"/>
        	<input type="hidden" name="filter_project" value="<?php echo @$proj;?>"/>
    	<span class="fa fa-print fa-2x move prnt" style="cursor: pointer; margin-left: 0px; margin-top: 0px; top: 0px; left: 0px;" title="Print All" id="print"></span>
            <div class="btn-row">
            <button type="button" class="sendbtnpdf" id="pdfButton">&nbsp;&nbsp;PDF&nbsp;&nbsp;</button>
            <button type="button"  id="sendEmailButton" class="sendbtnupdte" data-toggle="modal" data-target="#myModal">&nbsp;&nbsp;Send Email&nbsp;&nbsp;</button>
            <?php if(!empty($files)){?><a href="<?php echo base_url().@$files;?>" <button type="button"  id="CSVButton">&nbsp;&nbsp;CSV&nbsp;&nbsp;</button></a><?php }else{?>
            <button type="button" class="csvupdte" id="CSVButton">&nbsp;&nbsp;CSV&nbsp;&nbsp;</button><?php }?>
                </div>
        <select name="project" style="width:200px;" id="project" onchange="this.form.submit()">
        <option value="">-- Select Project --</option>
        <?php if(!empty($projects)){
    		foreach($projects as $project){?>
        <option value="<?php echo $project->project_id;?>"><?php echo $project->project_title;?></option>
        <?php }}else{echo "";}?>
        </select>
        </form>
        </div>
     </div>
                <div class="table-responsive1">
            <div id="print_table">
            <table class="mytab slect" id="table_lists" cellspacing="0">
                   <thead class="table-head">
					<td>E.Code</td>
                    <td>Name</td>
					<td>Designation</td>
					<td>B.Salary</td>
                    <td>Allowances</td>
                    <td>Total Allowances</td>
                    <td>Total Expenses</td>
					<td>Balance</td>
					<td>Gross Salary</td>
					<td>Income deduction at source</td>
					<td>Advances</td>
					<td>EOBI deducted @ PKR 80/month</td>
					<td>Other Deduction</td>
					<td>Total Deductions</td>
					<td>Net Salary Payable</td>
					<td>Remarks </td>
				</thead>
                <?php if(!empty($info)){
					foreach($info as $rec){
					?>
                <tr class="table-row">
                    <td><?php echo $rec->employee_code;?></td>
                    <td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<td><?php $base_salary=$rec->base_salary; echo Active_currency($rec->base_salary);?>
                    </td>
                    <?php
                    $alwnc_amt=@$rec->grp_allow_amnt;
                    $alwnc_type=@$rec->grp_allow_type;
                    $explode_alwtyp=explode(",",$alwnc_type);
                    $explode_alwamt=explode(",",$alwnc_amt);
                    ?>
                    <td>
                        <?php foreach($explode_alwtyp as $alw_typ)
                        {
                            echo "<span>".$alw_typ."</span><br>";
                        }?>

                        <?php foreach($explode_alwamt as $alw_mt)
                        {
                            echo "<span>".$alw_mt."</span><br>";
                        }
                        ?>
                    </td>
                    <td><?php $allowance=$rec->allowance_amnt; echo Active_currency($allowance);?></td>
                    <td><?php $expense=$rec->exp_amount; echo Active_currency($expense);?></td>
					<td><?php echo $rec->salary_arears;?></td>
					<td><?php $gross=$base_salary+$expense+$allowance; echo Active_currency($gross);?>
                    </td>
					<td><?php echo Active_currency($rec->tax_ded);?></td>
					<td><?php $advances=$rec->loan_amount; echo Active_currency($advances);?></td>
					<td><?php echo Active_currency($rec->eobi_ded);?></td>
					<td><?php echo Active_currency($rec->other_ded);?></td>
                    <?php $deduction_amount=$rec->ded_amount;?>
					<td><?php echo Active_currency($deduction_amount);?></td>
					<td><?php echo Active_currency($rec->transaction_amount);//$net_salary=$gross-$deduction_amount-$advances; echo $net_salary;?></td>
					<td><?php echo "Remarks";?></td>
				</tr>
                <?php }}else{echo "No record found !";}?>
        	 </table>
         </div>
			</div>
			</div>
            <?php echo $links;?>
	</div>
<!-- Modal -->
<!--<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>-->

<!----------------------------Send Email Without Bootstrap libraries----------------------------->
<div id="myModal" class="modal-demo" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Email To</h3>
    </div>
    <form class="form-horizontal" id="addNewBreachReport">

        <div class="pop_row">
            <div class="left_side">
                <h5>To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="ToEmail" id="inputEmail3" placeholder="Email" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Reply To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="FromEmail" placeholder="Reply To Email (optional)" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Subject</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" value="Attached Generated Payroll PDF" name="MessageSubject" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Message</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Attachment</h5>
            </div>
            <div class="right_side">
                <label>Work.pdf</label>
            </div>
        </div>

        <div class="btn-row">
            <input type="button" class="btn-pop" id="sendMailWithAttachment" value="Send">
        </div>
    </form>

</div>
<!------------------Script of the Email Btn without Libraies of Bootstrap-------->
<script>
    $('#sendEmailButton').on('click', function(e){
        Custombox.open({
            target: '#myModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });
</script>

<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
	$('#print').click(function(){
		className:"blue-with-image",
			$.loader({content:getsupport_print()});
	});
	function getsupport_print()
	{
		var formData = $("#print_sheet").serialize();
		formData=formData+'&print=1';
		var data={
		formData:formData
		};
		$.redirect('<?php echo base_url(); ?>payroll/salary_sheet',data,'POST');
	}
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<script>
    $(document).ready(function(){
        $("#pdfButton").on('click', function(){
            var targetURL = '<?php echo base_url();?>payroll/downloadPdfSalarySheetReport';
            <?php if(isset($view) && !empty($view)){ ?>
            var postData = [];
            postData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            console.log(postData);
            $.ajax({
                url: targetURL,
                data:postData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "Download"){
                        //console.log(data[1]);
                        //document.location = <?php //echo base_url();?>data[1];
                    }
                }
            });
            <?php } ?>
        });
        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>payroll/SendPDFPayrollSheetReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        /* New Select Employee
         * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
         * */
        var selectEmployeeSelector = $('#selectEmployee');
        var url = "<?php echo base_url(); ?>payroll/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar: 'EmployeeAvatar',
            employeeCode: 'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";

        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector, url, tDataValues, minInputLength, placeholder, baseURL, templateLayout);
        $('.select2-container').css("width", "30%");
    });
</script>
<script>
    $(document).ready(function(e){
        $('#project').select2();
    })
</script>