<script>
function print_table(id) {
	
	var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>

<span><input type="button"  name="print" onClick="print_table('print')" value="Print"></button></span>
<div id="print">

  <style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	padding-left: 7em;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight:normal;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 12px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
	
}
  </style>

<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<table cellspacing="0" width="543" class="printing">
				<tr>
					<td colspan="2" style="padding: 1em 0;" class="head">
                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Transaction Details<!-- of --><?php /*echo $details->full_name;*/?></h3>
                    </td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Name</td>
					<td width="337" class="no-apply"><i><?php echo $details->full_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Code</td>
					<td class="no-apply"><i><?php echo $details->employee_code;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Designation</td>
					<td class="no-apply"><i><?php echo $details->designation_name;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply" >Pay Grade</td>
					<td class="no-apply"><i><?php echo $details->pay_grade;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Transaction Month</td>
					<td class="no-apply"><i><?php echo $details->calender_month;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Transaction Type</td>
					<td class="no-apply"><i><?php echo $details->transaction_type;?></i></td>
				</tr>
				<tr>
					<td width="200" class="apply">Transaction date</td>
					<td class="no-apply"><i><?php 
				$date=$details->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></i></td>
				</tr>
				
				<tr>
					<td width="200" class="apply">Transaction Status</td>
                    <td class="no-apply"><?php echo $details->status_title;?></td>
				</tr>
                 <?php if($details->status_title=="Approved"){?>
                <tr>
					<td width="200" class="apply">Approved by</td>
                    <td class="no-apply"><?php echo $details->name;?></td>
				</tr>
                 <tr>
					<td width="200" class="apply">Approved date</td>
                    <td class="no-apply"><?php 
				$date=$details->approval_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
				</tr>
                <?php }?>

				<tr>
					<td width="200" class="apply">Transaction Amount</td>
					<td class="no-apply"><i><?php echo Active_currency($details->transaction_amount);?></i></td>
				</tr>
			</table>
		</div>