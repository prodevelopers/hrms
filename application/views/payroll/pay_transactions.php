<style type="text/css">
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.dataTables_filter
{
	float:right;
}
/*.move{
	position: relative;
	left: 0%;
	top: 0px;
}*/

.dataTables_length{
	position: relative;
	top: -77px;
	font-size: 1px;
	left: 49%;
	
}
.dataTables_length select{
	width: 60px;
}
.dataTables_filter{
	position: relative;
top: -50px;
left: -12em;
}
.resize{
	width: 220px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
#table_list_filter
{
	display:none;
}
input[name="name"]{
	width: 22%;
}
select[id="month"],select[id="year"]{
	width: 22%;
}
</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {/*
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#month').val()});
			aoData.push({"name":"year","value":$('#year').val()});
		        aoData.push({"name":"emp_name","value":$('#emp_name').val()});
                
		}
                
	*/});
});

/*$(".filter").change(function(e) {
    oTable.fnDraw();
});
*/
function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_transactions/'+id, "width=800,hight=600");
	pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll Transactions</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Transactions</div>

			<!-- filter -->
			<div class="filter">
             <div class="row">
			<h4>Filter By</h4>
             <?php echo @form_open('payroll/pay_transaction');?>
           <?php echo @form_dropdown('code',$employees_code,$code,"id='code' class='resize' onchange='this.form.submit()'")?>
			<?php echo @form_dropdown('name',$employees_name,$name,"id='name' class='resize' onchange='this.form.submit()'")?>
			<?php echo @form_dropdown('month',$months,$slct_m,"id='month' class='resize' onchange='this.form.submit()'")?>
           	<?php echo @form_dropdown('year',$years,$slct_y,"id='year' class='resize' onchange='this.form.submit()'")?>
			<span class="fa fa-print move" style="cursor:pointer" title="Print All" onclick="print_all()"></span>
			<?php echo @form_close();?>
			</div>
			</div>
			<!-- table -->
            <div id="print_table">
            <div class="table-responsive">
            	<table cellspacing="0" id="example" class="mytab slect">
				<thead class="table-head">
					<td>Code </td>
					<td>Name </td>
					<!--<td>Paygrade</td>-->
                    <td>Transaction ID </td>
					<td>Transaction Date</td>
                    <!--<td>Transaction Year</td>-->
                    <td>Type</td>
                    <td>Amount</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-print"></span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="fa fa-eye"></span></td>
				</thead>
                 <?php if(!empty($info)){?>
				 <?php foreach($info as $rec){?>
                 <tr class="table-row">
                 
                    <td><?php echo @$rec->employee_code; ?> </td>
					<td><?php echo @$rec->full_name; ?> </td>
					<!--<td><?php //echo @$rec->pay_grade; ?> </td>-->
                    <td><?php echo @$rec->transaction_id; ?> </td>
					<td><?php 
					$date=$rec->transaction_date;
				$new=date("d-m-Y",strtotime($date));
				echo $new;?></td>
                    <!--<td><?php //echo @$rec->calender_year; ?></td>-->
                   
                    <td><?php echo @$rec->transaction_type; ?></td>
                     <?php if(@$rec->transaction_type!="Salary"){?>
                      <td><?php echo @$rec->trans_amount; ?></td>
                	<?php }else{ ?>
                    <td><?php echo Active_currency($rec->transaction_amount); ?></td>
                    <?php }?>
                    <td><?php 
					if($rec->status==2)
					{echo "<span style='color:green'>Approved</span>";}
					else{echo "<span style='color:red'>".$rec->status_title."</span>";}?><!--<a href="payroll/transaction_status_update/<?php //echo $rec->status_title;?>/<?php //echo $rec->transaction_id;?>">
					<?php // echo $rec->status_title;?></a>--><?php //}?>
                    </td>
					 <?php if($rec->status==3 && $rec->trans_type_id==5) {?>
					<td><a href="payroll/review_failed/<?php echo $rec->employment_id;?>/<?php echo $rec->transaction_id;?>/<?php echo $rec->transaction_date;?>"><span class="fa fa-pencil"></span></a></td>
						 <?php }else{?>
						 <td><span>&nbsp;</span></td>
						 <?php }?>
					<td><span onclick="return print_report(<?php echo @$rec->transaction_id; ?>);" class="fa fa-print" style="cursor:pointer"></span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="payroll/transaction_details/<?php echo @$rec->transaction_id; ?>"><span class="fa fa-eye"></span></a></td>
                    </tr>
				 <?php }}else{echo "<tr><span style='color:red'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sorry no Records Found !</span></tr>";}?>
               <tbody></tbody>
			</table>
            </div>
			
            </div>
<div id="container">
        <ul>
        <br />
        <?php echo @$links;?>
        </ul>
        </div>
		</div>

	</div>
	<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/payroll_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<!-- contents -->
<script type="text/javascript">
function print_all()
{
	var data = $('#print_table').html();
    var mywindow = window.open();
    mywindow.document.write('<html><head><title>Planning</title>');
    mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
	mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div id="planning">');
    mywindow.document.write(data);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');

    mywindow.print();

    return true;
}

$(document).ready(function(e){
    $('#month,#year,#name,#code').select2();
})
</script>