<style type="text/css">

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#smonth').val()});
			
                
		}
                
	});
});

$("#smonth").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_report(id){
	window.open('payroll/print_salarypayment/'+id, "width=800,hight=600");
	<!--pr.onload=pr.print() ;
	<!--pr.focus();-->
	}
    </script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<!--<script>
      var navigation = responsiveNav(".nav-collapse");
    </script>-->
	<div class="right-contents">

		<div class="head">Salary Payment</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <form action="payroll/test" method="post">
                <div class="row">
				<input type="text" name="emp_name" placeholder="Employee Name">
                
				<?php echo form_dropdown('month',$months)?>
                </div>
                <div class="row">
           	    <?php echo form_dropdown('year',$years)?>

					<input type="submit" value="Search" class="btn green">
					<button class="btn gray">Reset</button>
				</div>
                
                </form>
			</div>

			

			<!-- table -->
			<table cellspacing="0"  class="mytab slect">
				<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>Paygrade</td>
                    <td>Month</td>
                    <td>Year</td>
					<td>Base salary</td>
                    <td>Allowance</td>
                    <td>Expense</td>
                    <td>loan/advances</td>
                    <td>Deductions</td>
                    <td>Payable</td>
                    <td>Paid</td>
                    <td>Remains</td>
                    <td><img src="assets/img/icon-black.png" width="20" height="20"></td>
                    <td><span class="fa fa-print"></span></td>
                    <td><span class="fa fa-eye"></span></td>
                    
		<!--<td align="center"><a href="payroll/pay_salary"><img src="assets/img/icon-black.png" width="20" height="20"></a></td>-->
					<!--<td align="center"><img src="assets/img/icon-black.png" width="20" height="20">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-print">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-eye"></span></span></td>-->
					<!--<td align="center"><span class="fa fa-eye"></span></td>-->
					
					<!--<td>Pay Salary</td>-->
                    <span><?php if(!empty($info)){?></span>
				</thead>
                <?php foreach($info as $salary_rec){?>
				<tr class="table-row">
					<td><?php echo $salary_rec->employee_code?></td>
					<td><?php echo $salary_rec->full_name?></td>
					<td><?php echo $salary_rec->pay_grade?></td>
					<td><?php echo $salary_rec->month?></td>
                    <td><?php echo $salary_rec->year?></td>
                    <td><?php echo $salary_rec->base_salary?></td>
                     <?php
				   	$alwnc_month=$salary_rec->alwnc_month;
					$alwnc_year=$salary_rec->alwnc_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					if($salary_month == $alwnc_month && $salary_year == $alwnc_year)
					{$alwnc_amount=$salary_rec->allowance_amount;}else{$alwnc_amount=0;} 
					 ?>
                    <td><?php echo $alwnc_amount;?></td>
                   <?php
				    $exp_id=$salary_rec->expense_claim_id;
					$exp_dis_id=$salary_rec->expense_id;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					$exp_month=$salary_rec->exp_month;
					$exp_year=$salary_rec->exp_year;
					if($exp_id == $exp_dis_id)
					{$expense_amount=0;}
					elseif($salary_month == $exp_month && $salary_year == $exp_year)
					{$expense_amount=$salary_rec->exp_amount;}else{$expense_amount=0;}
					//elseif($exp_id != $exp_dis_id && $salary_month == $exp_month && $salary_year == $exp_year)
					//{$expense_amount=$salary_rec->exp_amount;}
					//if($expense_amount==0){ $exp_amt="paid";}else{$exp_amt=$salary_rec->exp_amount;;}
					 ?>
                    <td><?php echo $expense_amount;?></td>
                     <?php
					$loan_advance_id=$salary_rec->loan_advance_id;
					$loan_payback_id=$salary_rec->loan_payback_id;
				   	$lonad_month=$salary_rec->alonad_month;
					$lonad_year=$salary_rec->alonad_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					if($loan_advance_id == $loan_payback_id)
					{$loan_advance=0;}
					elseif($salary_month == $lonad_month && $salary_year == $lonad_year)
					{$loan_advance=$salary_rec->loan_amount;}else{$loan_advance=0;} 
					 ?>
					<td><?php echo $loan_advance;?></td>
                     <?php
				   	$ded_month=$salary_rec->ded_month;
					$ded_year=$salary_rec->ded_year;
					$salary_month=$salary_rec->salary_month;
					$salary_year=$salary_rec->salary_year;
					if($salary_month == $ded_month && $salary_year == $ded_year)
					{$deduction_amount=$salary_rec->ded_amount;}else{$deduction_amount=0;} 
					 ?>
                    <td><?php echo $deduction_amount;?></td>
					<?php
					$base_salary=$salary_rec->base_salary;
					$deduction_amount;
					$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					 ?>
					<td><?php echo $payable;?></td>
					<td><?php echo $salary_rec->transaction_amount;?></td>
                    <?php 
					$paid_amount=$salary_rec->transaction_amount;
					$remain_amount=$payable-$paid_amount;
					?>
                    <td><?php echo $remain_amount;?></td>
                    <td><a href="payroll/pay_remain_salary/<?php echo $salary_rec->employee_id; ?>/<?php echo $salary_month;?>/<?php echo $salary_year;?>/<?php echo $payable;?>"><img src="assets/img/icon-black.png" width="20" height="20"></a></td>
                    <td><span onclick="return print_report(<?php echo $salary_rec->employee_id; ?>);" class="fa fa-print" style="cursor:pointer"></span></td>
                    <td><a href="payroll/payroll_reg_detail/<?php echo $salary_rec->employee_id; ?>/<?php echo $salary_month;?>/<?php echo $salary_year;?>/<?php echo $payable;?>"><span class="fa fa-eye"></span></a></td>
				</tr>
                <?php }?>
				<?php } else {echo "<span style='color:red'> Sorry no Records Found !</span>";} ?>
				
			</table>
            <br />
             <br />
        <div id="container">
        <ul>
        <?php echo $links;?>
        </ul>
        </div>

			

		</div>

	</div>
<!-- contents -->
