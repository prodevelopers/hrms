<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
.designing{
	top:200px;
	right:200px;
	}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 300px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 alignment-adjust:central;
 display:none;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	
    } else {
        $.post("payroll/autocomplete/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_code,cnic,id,position,department_name) {
    $('#id_input').val(thisValue);
	$('#employee_code').append(employee_code);
	$('#cnic').append(cnic);
	$('#emp_id').val(id);
	$('#position').append(position);
	$('#department').append(department_name);
	$('#employee_name').append(thisValue);
	
	
	setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
   
}   

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Loan/Advance Payment</div> <!-- bredcrumb -->

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Edit Loan/Advance </div>
			<script type="text/javascript">
			 
			$(document).ready(function(){
				$(".cheque").hide();
			   $(".account").hide();
			   $(".cash").hide();
			   
				$("#selected").change(function(e){
					if($(this).val()==3){
			            $(".account").show();
						$(".cheque").hide();
						 $(".cash").hide();}
						if($(this).val()==2){
			             $(".cheque").show();
						 $(".account").hide();
			   			 $(".cash").hide();}
						if($(this).val()==1){
			            $(".cash").show()
						$(".cheque").hide();
			   			$(".account").hide();}
					});
			  
			});
			</script>
<div class="form-left">
			<form action="payroll/update_loan_advance" method="post" role="form">
				<div class="row2">
					<h4>Employee Name</h4>
		<input name="name" id="name"  type="text"  value="<?php echo $info->full_name;?>" readonly="readonly" >
                    <div id="suggestions" style="margin:0px 0px 0px 250px;">
                   <div class="autoSuggestionsList_l" id="autoSuggestionsList"> 
                   </div>   
                </div>
   <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $info->employee_id;?>"  />
    <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $info->loan_advance_id;?>"  />
				</div>
				
				<br class="clear">
				<div class="row2">
					<h4>Payment Type</h4>
                    <select name="payment_type" disabled="disabled">
                   	<option value="<?php echo $info->payment_type_id;?>" selected="selected"><?php echo $info->payment_type_title;?></option>
                    <?php //foreach($payment_type as $type):?>
				    <option value="<?php //echo $type->payment_type_id;?>"><?php //echo $type->payment_type_title;?></option>
                        <?php //endforeach;?>
					</select>
                    
                    
					<?php //echo form_dropdown('payment_type',$payment_type)?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Payment Amount</h4>
					<input type="text" name="payment_amount" value="<?php echo $info->amount;?>"  >
				</div>
                <br class="clear">	
                 <div class="row1">
					<h4>Month</h4>
					<!--<select name="month" >
                   	<option value="<?php /*echo $info->ml_month_id;*/?>" selected="selected"><?php /*echo $info->month;*/?></option>
                    <?php /*foreach($months as $mon):*/?>
				    <option value="<?php /*echo $mon->ml_month_id;*/?>"><?php /*echo $mon->month;*/?></option>
                        --><?php /*endforeach;*/?>
						<?php
						$selected =(isset($info->month) ? $info->month:'');
						echo form_dropdown('month',$months,$selected)?>
					</select>
				</div>
				<br class="clear">
				<div class="row1">
					<h4>Year</h4>
					<!--<select name="year" >
                   	<option value="<?php /*echo $info->ml_year_id;*/?>" selected="selected"><?php /*echo $info->year;*/?></option>
                    <?php /*foreach($years as $yer):*/?>
						<option value="<?php /*echo $yer->ml_year_id;*/?>"><?php /*echo $yer->year;*/?></option>
                        <?php /*endforeach;*/?>

					</select>-->
					<?php
					$selectedy =(isset($info->year) ? $info->year:'');
					echo form_dropdown('year',$years,$selectedy)?>
				</div>
				<br class="clear">
				<br class="clear">
				<div class="row2">
					<input type="submit"  class="btn green" value="Update" />
				</div>
				
			</form>
</div>
<div class="form-right">
<div class="form-right" id="employee_inf0">
			<div class="head">Information</div>
            <div class="row2">
                    	<span class="headingfive">Employee Name</span>
                    	<i class="italica" id="employee_name"><?php //echo $info->full_name;?></i>
                    </div>
                    <div class="row2">
                    	<span class="headingfive">Employee Code</span>
                    	<i class="italica" id="employee_code"><?php //echo $info->employee_code;?></i>
                    </div>
               
                    <div class="row2">
                    	<span class="headingfive">Department</span>
                    	<i class="italica" id="department"><?php //echo $info->department_name;?></i>
				</div>
				<div class="row2">
                    	<span class="headingfive">position</span>
                    	<i class="italica" id="position"><?php //echo $info->designation_name;?></i>
				</div>
                <div class="row2">
                    	<span class="headingfive">Loan/Advance</span>
                    	<i class="italica" id="position"><?php //echo $info->amount;?></i>
				</div>
                
			</div>
    </div>
	
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="#"><button class="btn green"></button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>
	</div>
<!-- contents -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/payroll_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
</script>
<!-- leftside menu end -->