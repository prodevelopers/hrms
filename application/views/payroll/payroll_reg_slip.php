<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<!-- contents -->
<style type="text/css">
/*modal CSS*/
.modal-demo {
    float:left;
    background-color: #FFF;
    padding-bottom: 15px;
    border: 1px solid #000;
    border-radius: 10px;
    box-shadow: 0 8px 6px -6px black;
    display: none;
    text-align: left;
    width: 600px;


}
.title {
    border-bottom: 1px solid #ccc;
    font-size: 18px;
    line-height: 18px;
    padding: 10px 20px 15px;
}

h1, h2, h4 {
    font-family: "Dosis",sans-serif;
}
.text{
    padding: 0 20px 20px;
}
.text , .title{
    color: #333;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857;
}
button.close {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 0 none;
    cursor: pointer;
    padding: 0;
}
.close {
    position: absolute;
    right: 15px;
    top: 15px;
}

.close {
    color: #ffffff;
    float: right;
    font-size: 21px;
    font-weight: 700;
    line-height: 1;
    text-shadow: 0 1px 0 #fff;
}

.pop_row{
    float:left;
    width:100%;
    height:auto;
    margin-bottom: 7px;
}
.pop_row_txtarea{
    float:left;
    width:100%;
    height:auto;
    margin-bottom: 7px;
}
.left_side{
    float:left;
    width:40%;
    height:auto;
}
.left_side h5{
    line-height: 35px;
    padding-left: 20px;
}
.right_side{
    float:left;
    width:60%;
    height:auto;
}
.right_txt{
    width:240px important;
    height:auto important;
}
.top_header{
    float:left;
    width:100%;
    height:45px;
    background-color: #4f94cf;
    margin-bottom: 10px;
    border-top-right-radius: 10px;
    border-top-left-radius: 10px;
    border-bottom: 2px solid rosybrown
}
.top_header h3{
    color:#ffffff
}
.right_txt_area{
    width:240px;
    height:100px;
    padding:7px;
}
.btn-row{
    width:auto;
    float:right;
    margin-right:-15px;
}
.btn-pop{
    float:right;
    /*width:80px;*/
    padding: 5px 10px 5px 10px;
    background-color: #6AAD6A;
    color:white;
    margin-right: 120px;
}
.btn-pop:hover{
    cursor: pointer
}
.view_model{
    width:1000px;
}
.left_photo{
    float:left;
    width:100px;
    height:100px;
    margin-right: 10px;
    margin-top: 10px;
    border: 1px solid #d3d3d3
}
.right_sec{
    float:left;
    width:820px;
    height:auto;
    margin-right: 10px;
    margin-bottom: 10px;
}
.left_lbl h5{
    line-height: 35px;
}
.left_lbl{
    float:left;
    width:190px;
    height:35px;
    margin-right: 5px;
    margin-bottom:10px;
    padding-left: 5px;
}
.right_lbl{
    float:left;
    width:190px;
    height:35px;
    margin-bottom:10px
}
.right_lbl{
    line-height: 35px;
    padding-left: 5px;
}
.left_block{
    width:400px;
    height:auto;
    float:left;
}
div.text form{
    display: inline-block;
}

.modal-footer{
    border-top: @gray-lighter solid 1px;
    text-align: right;
}
	.beauty h4{
		font-size:13px;
		font-weight:normal;
	}
	.beauty td{
		font-size: 13px;
	}
	.beauty{
		width:23%;
		margin:1em 0.5em;
		float:left;
	}
	.head1{
	background: #4D6684;
	color: #fff;
	font-weight:500;
	font-size: 0.9em;
	line-height: 25px;
}
    .cheque{
        width:720px;
        min-height: 6em;
        font-family:"calibri";
        overflow: hidden;
        border: 1px solid #e1e1e1;
        -webkit-box-shadow:0 0 2px rgba(0,0,0,0.4);
        margin-left:30px;
    }
    .head{
        font-family:"calibri";
        background-color: #008000;
        padding: 0.5em;
        color:#fff;
        font-weight: bold;
        font-size: 18px;
    }
</style>


<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/css/bootstrap.css" type="text/css"/>-->
<style>
 thead  th {
	 font-size:15px;
 }
.infor th{
	font-size: 14px;
}
tbody td{
	font-size:14px;
}
.detail td{
	font-size: 13px;
}
</style>
<script>
	function print_table(id) {
		var printContents = document.getElementById(id).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
	</script>
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Payroll / Payroll Register Detail</div>
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">PayRoll Record
			<!--<a href="payroll/domPDF_Test/<?php /*echo $this->uri->segment(3);*/?>/<?php /*echo $this->uri->segment(4);*/?>/<?php /*echo $this->uri->segment(5);*/?>"><input type="button"  name="email" class="btn green" style="float:right" value="Email"></a>-->
			<button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right"  style="float:right" data-toggle="modal" data-target="#myModal">
				Send
			</button>
			<input type="button"  name="print" onclick="print_table('print_area')" class="btn green" style="float:right; height:28px;" value="Print"></div>
        <div id="print_area">
        <table class="cheque" cellpadding="0" cellspacing="0">
				<thead>
					<th colspan="4" class="table-head">Salary Slip <?php echo @$details->month.", ".$details->year; ?></th>
				</thead>
				<tr class="table-row">
					<td>Name</td>
					<td><h4><?php echo @$details->full_name;?></h4></td>
					<td>Employee Code</td>
					<td><h4><?php echo @$details->employee_code;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Designation</td>
					<td><h4><?php echo @$details->designation_name;?></h4></td>
					<td>Pay Grade</td>
					<td><h4><?php echo @$details->pay_grade;?></h4></td>
				</tr>

				<tr class="table-row" >
					<td>Base Salary</td>
					<td><h4><?php echo Active_currency($details->base_salary);?></h4></td>

					<td>Deducations</td>

                    <td><h4><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Allowances</td>

                    <td><h4><?php 
					if(!empty($allw_trans->transaction_amount)){
					echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>

					<td>Expenses</td>

                    <td><h4><?php 
					if(!empty($exp_trans->transaction_amount)){
					echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>	
				<tr class="table-row">
					<td>Advances</td>

					<td><h4><?php 
					if(!empty($advance_trans->advance_trans_amount))
					{
					echo @$advance_trans->advance_trans_amount;}else{echo "0";}?>
                  </h4></td>

					<td>Last Arrears</td>
					<?php if(!empty($last_salary->arears)){?>
						<td><?php echo $last_salary->arears ;?></td>
					<?php }else{?><td>0</td><?php }?>
				</tr>
				<tr class="table-row">
					<td>Total Paid</td>

					<td><h4><?php echo Active_currency($salary_info->transaction_amount);?></h4></td>

				<td>Balance </td>
				<td><h4>
						<?php echo Active_currency($salary_info->arears);?></h4></td>
			</tr>

		</table>

            <table>
                <tr>
              <td>Approved By:____________</td>
              <td>Designation:____________</td>
              <td>Signature:____________</td>
                </tr>
            </table>
</div>
	</div>
	</div>
<!-- contents -->
<!-- Modal -->
<!--<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Email To:</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="mailForm">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">To</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">From</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="FromEmail"  id="inputPassword3" placeholder="Admin@gmail.com">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
						<div class="col-sm-9">
							<textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
						<div class="col-sm-9">
							<label  class="col-sm-2 control-label"><?php /*echo  $this->session->flashdata('fileName');*/?></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</div>-->

<!----------------------------Send Email Without Bootstrap libraries----------------------------->
<div id="myModal" class="modal-demo" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Email To</h3>
    </div>
    <form class="form-horizontal" id="mailForm">

        <div class="pop_row">
            <div class="left_side">
                <h5>To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="ToEmail" id="inputEmail3" placeholder="Email" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Reply To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="FromEmail" placeholder="Reply To Email (optional)" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Subject</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" value="Attached Generated Payroll PDF" name="MessageSubject" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Message</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Attachment</h5>
            </div>
            <div class="right_side">
                <label>Work.pdf</label>
            </div>
        </div>

        <div class="btn-row">
            <input type="button" class="btn-pop" id="sendMailWithAttachment" value="Send">
        </div>
    </form>

</div>
<!------------------Script of the Email Btn without Libraies of Bootstrap-------->

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<!--<script src="<?php /*echo base_url()*/?>assets/js/bootstrap.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
	$(document).ready(function(){
		$('#sendMailWithAttachment').on('click', function (e) {
			var formData = $('#mailForm').serializeArray();
			<?php if(isset($pdfView) && !empty($pdfView)){ ?>
			formData.push({name:"viewData",value:<?php echo json_encode($pdfView); ?>});
			<?php } ?>
			var targetURL = '<?php echo base_url();?>payroll/sendGeneratedPDFReport';
			$.ajax({
				url: targetURL,
				data:formData,
				type:"POST",
				success: function (output) {
					var data = output.split("::");
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
					}else if(data[0] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});
	});
</script>

<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/payroll_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- leftside menu end -->

<!------------------Script of the Email Btn without Libraies of Bootstrap-------->
<script>
    $('#sendEmailButton').on('click', function(e){
        Custombox.open({
            target: '#myModal',
            effect: 'fadein'
        });
        e.preventDefault();
    });
</script>