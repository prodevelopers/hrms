 <style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 float:left;
 margin-left: 210px;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
 float:right;

}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
}
</style>
<script type="text/javascript">
function lookup(inputString) {
    if(inputString.length == 0) {
        $('#suggestions').hide();
		$('#employee_inf0').hide();
    } else {
        $.post("site/autocomplete/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
                $('#autoSuggestionsList').html(data);
            }
        });
    }
}

function fill(thisValue,employee_code,cnic,employee_id) {
    $('#id_input').val(thisValue);
	$('#employee_code').val(employee_code);
	$('#cnic').val(cnic);
	$('#emp_id').val(employee_id);
   setTimeout("$('#suggestions').hide();", 200);
   $('#employee_inf0').show();
}   

</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Expense Management / Add Expense</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/payroll_left_nav'); ?>
    
	<div class="right-contents">

		<div class="head">Add Expense</div>


			<form action="site/add_expense_records" method="post">
				<div class="row">
					<h4>Employee Name</h4>
    <input name="name" id="id_input" class="serch" type="text" autocomplete="off" value="" onkeyup="lookup(this.value)">
   <input type="hidden" name="emp_id" id="emp_id"  />
   
   <div id="employee_inf0" style="display:none;">
            				
           <input type="text" name="employee_code" id="employee_code" value=""  readonly="readonly"/>
           <br />
           <input type="text" name="cnic" id="cnic" value="" readonly="readonly" />
           <br />
         
           </div>
          
                <div id="suggestions">
                <div class="autoSuggestionsList_l" id="autoSuggestionsList">    
                </div>
            </div>
                                
				</div>
				
				<br class="clear">
				<div class="row">
					<h4>Employee Type</h4>
					<?php echo form_dropdown('employee_type',$employee)?>
				</div>
				<br class="clear">
				
				<div class="row">
					<h4>Expense Type</h4>
					<?php echo form_dropdown('expense_type',$expense)?>
				</div>
                <script type="text/jscript">
                $(function(){$("#expdate").datepicker({dateFormat:'yy-mm-dd'})});
                </script>
                <br class="clear">
				<div class="row">
					<h4>Expense date</h4>
					<input type="text" name="expdate" id="expdate" value=""/><span><img src="assets/icons/data.png" /></span>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Ammount</h4>
					<input type="text" name="expamount" id="expamount" value="" />
				</div>
				
				
				<div class="row">
				<div class="button-group">
					<input type="submit" value="Add Expense" class="btn green">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
			</form>

			<!-- button group -->
			

		</div>

	</div>
<!-- contents -->