<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>
<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
body{
	background: #f8f8f8;
}
.invoice-header {
  margin: 0 0 10px;
  border-bottom: 1px dashed #B6B6B6;
  display: inline-block;
}
.box {
  display: block;
  z-index: 1999;
  position: relative;
  border: 1px solid #f8f8f8;
  box-shadow: 0 0 4px #D8D8D8;
  background: transparent;
  margin-bottom: 20px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
}
.box-content {
  position: relative;
  -webkit-border-radius: 0 0 3px 3px;
  -moz-border-radius: 0 0 3px 3px;
  border-radius: 0 0 3px 3px;
  padding: 15px;
  background: #FCFCFC;
}
 .box-content {
  height: 100%;
  width: 867px;
  left: 0;
  top: 0;
}
.box-content.dropbox, .box-content.sortablebox {
  overflow: hidden;
}
.m-ticker span {
  display: block;
  font-size: 0.8em;
  line-height: 1em;
}
.box-content .page-header, legend, .full-calendar .page-header {
  margin: 0 0 10px;
  border-bottom: 1px dashed #B6B6B6;
}
.box-content .form-group, .devoops-modal-inner .form-group {
  margin-top:15px;
  margin-bottom:15px;
}
table td{
	font-size: 14px;
}
/*.seting{
	width: 300px !important;
	
}*/
</style>

<div id="content">

<div id="print_area">
<div class="row">
	<div class="col-xs-9">
		<div class="box">
			<div class="box-content">
				<div class="col-xs-6">
					<b>Tansfer From:</b>
					<address>
                       <strong>Habib Bank Limited</strong><br>
						Arbab Road Saddar<br>
						Peshawar, Street No:01<br>
						KPK, Pakistan<br>
						<abbr title="Phone">P:</abbr> (+92) 456-7890<br>
                        <strong>Account no: 124578789965</strong>
					</address>
					<address>
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<address>
						<strong>Parexons IT Solutions & Services</strong><br>
						First Floor 51, Deans<br>
						Trade Center Peshawar<br>
						 Saddar KPK, Pakistan<br>
						<abbr title="Phone">P:</abbr> (+92) 556-7890<br>
						<a href="#">www.parexons.com</a>
						<p>Tansfer Date: 1/12/14</p>
					</address>
					
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-11" style="margin-left:1.8em;">
					<br/>
                    
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>S.No</th>
								<th style="width: 117px;">Account Title</th>
                                <th class="seting">Bank&nbsp;</th>
                                 <th class="seting">Branch</th>
                                 <th class="seting">Branch Code</th>
								 <th style="width: 95px;">Account No</th>
								<th style="width: 138px;">Transaction Date</th>
								<th style="width: 169px;">Transaction Ammount</th>
							</tr>
						</thead>
						<tbody>
                        
						 <?php $amount=0;
						 $no=1;
						  if(!empty($info)){
					foreach($info as $rec){
						
					 ?>
							<tr>
								<td><?php echo $no;?></td>
								<td style="width: 117px;">
								<?php echo $rec->account_title;?></td>
                                <td><?php echo $rec->bank_name;?>&nbsp;&nbsp;</td>
                                <td><?php echo $rec->branch;?></td>
                                <td><?php echo $rec->branch_code;?></td>
								<td style="width: 95px;">
								<?php echo $rec->account_no;?></td>
								<td style="width: 138px;">
								<?php echo date_format_helper($rec->transaction_date);?></td>
								<td style="width: 169px;">
								<?php echo Active_currency($rec->transaction_amount);?></td>
							</tr>
							<?php $amount+=$rec->transaction_amount; $no++;}}else{echo "<span style='color:red'> 
							Sorry no Records Found !</span>";}?>

							<tr class="active">
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td></td>
							  <td><b>Total</b></td>
							  <td><b><?php echo Active_currency($amount)." /--";?></b></td>
						  </tr>
						</tbody>
					</table>
					<small>CC included</small>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<!--<script src="<?php // echo base_url();?>assets/data_picker/jquery-1.10.2.js"></script>	

<script>
<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>
</script>-->