<style type="text/css">
    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers {
        margin-top: 8px;
    }

    .dataTables_info {
        color: #3474D0;
        font-size: 14px;
        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    .move {
        position: relative;
        top: 40px;
        left: 50%;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        font-size: 1px;
        left: 84%;
        position: relative;
        top: -50px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -49px;
        left: -3em;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

</style>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#table_list').dataTable({
            "bProcessing": true,
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bServerSide": true,
            "sDom": '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
            "sAjaxSource": window.location + "/list",
            "bDestroy": true,
            "sServerMethod": "POST",
            "aaSorting": [[0, "asc"]],
            "fnServerParams": function (aoData, fnCallBack) {
                aoData.push({"name": "month", "value": $('#month').val()});
                aoData.push({"name": "year", "value": $('#year').val()});
                aoData.push({"name": "exp_type", "value": $('#exp_type').val()});
            }
        });

        $('#tableSearchBox').keyup(function () {
            oTable.fnFilter($(this).val());
        });
    });

    $(".filter").change(function (e) {
        oTable.fnDraw();
    });

    function doc_file() {
        var r = confirm('if you are upload this file again !\n\ please follow check out method');
        if (r == true) {
            return true;
        } else if (r == false) {
            return false;
        }
    }
</script>
<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Payroll / Expense Management / Expense Disbursment</div>
    <!-- breadCrumb -->
    <a id="right-panel-link" href="#right-panel"><span
            style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

    <script>
        var navigation = responsiveNav(".nav-collapse");
    </script>
    <div class="right-contents">

        <div class="head">Expense Disbursment</div>

        <!-- filter -->
        <div class="filter">
            <h4>Filter By</h4>
            <?php echo form_open(); ?>
            <input type="text" id="tableSearchBox" class="resize" placeholder="Search Employees" style="width: 30%;">
            <?php echo @form_dropdown('exp_type', $expense_type, $slct_exp, "id='exp_type' class='resize' onchange='this.form.submit()'") ?>
            <?php echo @form_dropdown('month', $month, $slct_m, "id='month' class='resize' onchange='this.form.submit()'") ?>
            <?php echo @form_dropdown('year', $year, $slct_y, "id='year' class='resize' onchange='this.form.submit()'") ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="fa fa-print" style="cursor:pointer" title="Print All" onclick="print_all()"></span>

            <?php echo form_close(); ?>
        </div>


        <div id="print_table">
            <!-- table -->
            <table cellspacing="0" id="table_list" class="mytab slect">
                <thead class="table-head">
                <tr>
                    <td>Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Designation</td>
                    <td>Type</td>
                    <td>Paid Amount</td>
                    <td>Approved by</td>
                    <td>Paid Date</td>
                </tr>
                </thead>
            </table>
        </div>
        <!-- button group -->
        <div class="row">
            <div class="button-group">
                <!--<a href="payroll/add_expense"><button class="btn green">New Expense</button></a>-->
                <!-- <button class="btn red">Delete</button> -->
            </div>
        </div>

    </div>

</div>
<!-- contents -->
<script type="text/javascript">
    function print_all() {
        var data = $('#print_table').html();
        var mywindow = window.open();
        mywindow.document.write('<html><head><title>Planning</title>');
        mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/form.css"/>');
        mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>');
        mywindow.document.write('<link type="text/css" rel="stylesheet" href="assets/css/print_css.css"/>');
        mywindow.document.write('</head><body>');
        mywindow.document.write('<div id="planning">');
        mywindow.document.write(data);
        mywindow.document.write('</div>');
        mywindow.document.write('</body></html>');

        mywindow.print();

        return true;
    }

    $(document).ready(function (e) {
        $('#exp_type,#month,#year').select2();
    })
</script>
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/payroll_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200});
    $('#close-panel-bt').click(function () {
        $.panelslider.close();
    });
</script>
<!-- leftside menu end -->