<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
<link rel="stylesheet" type="text/css" href="assets/select2/select2.css">
<link rel="stylesheet" type="text/css" href="assets/css/form.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/data_picker/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <div class="table-responsive1">
    <div><h4>PayRoll Salary Sheet</h4></div>
            <div id="print_table">
            <table class="mytab slect" id="table_lists" cellspacing="0">
                   <thead class="table-head">
                   <tr>
					<td>E.Code</td>
                    <td>Name</td>
					<td>Designation</td>
					<td>B.Salary</td>
                    <td>Allowances</td>
                    <td>Total Allowances</td>
                    <td>Total Expenses</td>
					<td>Balance</td>
					<td>Gross Salary</td>
					<td>Income deduction at source</td>
					<td>Advances</td>
					<td>EOBI deducted @ PKR 80/month</td>
					<td>Other Deduction</td>
					<td>Total Deductions</td>
					<td>Net Salary Payable</td>
					<td>Remarks </td>
                   </tr>
				</thead>
                <?php if(!empty($info)){
					foreach($info as $rec){
					?>
                <tr class="table-row">
                    <td><?php echo $rec->employee_code;?></td>
                    <td><?php echo $rec->full_name;?></td>
					<td><?php echo $rec->designation_name;?></td>
					<td><?php $base_salary=$rec->base_salary; echo Active_currency($rec->base_salary);?>
                    </td>
                    <?php
                    $alwnc_amt=@$rec->grp_allow_amnt;
                    $alwnc_type=@$rec->grp_allow_type;
                    $explode_alwtyp=explode(",",$alwnc_type);
                    $explode_alwamt=explode(",",$alwnc_amt);
                    ?>
                    <td>
                        <?php foreach($explode_alwtyp as $alw_typ)
                        {
                            echo "<span>".$alw_typ."</span><br>";
                        }?>

                        <?php foreach($explode_alwamt as $alw_mt)
                        {
                            echo "<span>".$alw_mt."</span><br>";
                        }
                        ?>
                    </td>
                    <td><?php $allowance=$rec->allowance_amnt; echo Active_currency($allowance);?></td>
                    <td><?php $expense=$rec->exp_amount; echo Active_currency($expense);?></td>
					<td><?php echo Active_currency($rec->salary_arears);?></td>
					<td><?php $gross=$base_salary+$expense+$allowance; echo Active_currency($gross);?>
                    </td>
					<td><?php echo Active_currency($rec->tax_ded);?></td>
					<td><?php $advances=$rec->loan_amount; echo Active_currency($advances);?></td>
					<td><?php echo Active_currency($rec->eobi_ded);?></td>
					<td><?php echo Active_currency($rec->other_ded);?></td>
                    <?php $deduction_amount=$rec->ded_amount;?>
					<td><?php echo Active_currency($deduction_amount);?></td>
					<td><?php echo Active_currency($rec->transaction_amount);//$net_salary=$gross-$deduction_amount-$advances; echo $net_salary;?></td>
					<td><?php echo "Remarks";?></td>
				</tr>
                <?php }}else{echo "No record found !";}?>
        	 </table>
         </div>
			</div>
