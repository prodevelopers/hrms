<?php
//echo $this->session->userdata('pdf_file');

//header('Content-Type: text/plain');
if(isset($SelectedCheckBoxes) and isset($ReportTableData)){
    if(strpos($SelectedCheckBoxes,'Salaries') !== false){
        $salaries = true;
    }else{
        $salaries = false;
    }
    if(strpos($SelectedCheckBoxes,'Allowances') !== false){
        $allowances = true;
    }else{
        $allowances = false;
    }
    if(strpos($SelectedCheckBoxes,'Expenses') !== false){
        $expenses = true;
    }else{
        $expenses = false;
    }
    if(strpos($SelectedCheckBoxes,'Deductions') !== false){
        $deductions = true;
    }else{
        $deductions = false;
    }
}
?>
<?php
        //This Section Will Execute If Employee Filter is Selected
if (isset($employeeInfo) && !empty($employeeInfo)) {
        echo $employee_fields="Employee Name,Employee Code,Designation,Department,Year".'"'."\r\n";

    echo $employee_info=$employeeInfo->EmployeeName.",". $employeeInfo->EmployeeCode.",". $employeeInfo->Designation .",". $employeeInfo->Department ."," . $filteredYear.'"';}?>
   <?php  echo $salary_fields='Payments/Month,'.($salaries? 'Salaries,':'"').($allowances? 'Allowances,':'"').($expenses? 'Expenses,':'"').($deductions? 'Deductions,':'"')."Total".'"'."\r\n";
//echo "\r\n";
?>
        <?php
        //This Section will execute if the Project filter is selected.
        if (isset($projectInfo) && !empty($projectInfo)) {
            echo  $project_info=$projectInfo->projectTitle .",". $projectInfo->ProjectAbbreviation .",".$filteredYear.'"'."\r\n";}?>
       <?php
        //This Section will execute if the department filter is selected.
        if (isset($departmentInfo) && !empty($departmentInfo)) {
            echo $department_info=$departmentInfo->DepartmentName.",".$filteredYear;} ?>
        <?php
            if(isset($ReportTableData) && is_array($ReportTableData)){
                foreach($ReportTableData as $key=>$val){
                    echo $salary_records='"'.$key.",".($salaries?(isset($val['Salaries'])?$val['Salaries']:'-'):'"').",".($allowances?(isset($val['Allowances'])?$val['Allowances']:'-'):'"').",".($expenses?(isset($val['Expenses'])?$val['Expenses']:'-'):'"').",".($deductions?(isset($val['Deductions'])?$val['Deductions']:'-'):'"').",".
                    $total = 0;
                    if($salaries){
                        $salariesRowTotal = isset($val['Salaries'])?$val['Salaries']:0;
                        $total += $salariesRowTotal;
                    }
                    if($allowances){
                        $allowancesRowTotal = isset($val['Allowances'])?$val['Allowances']:0;
                        $total += $allowancesRowTotal;
                    }
                    if($expenses){
                        $expensesRowTotal = isset($val['Expenses'])?$val['Expenses']:0;
                        $total += $expensesRowTotal;
                    }
                    if($deductions){
                        $deductionsRowTotal = isset($val['Deductions'])?intval($val['Deductions']):0;
                        $total -= $deductionsRowTotal;
                    }
                    echo $total.'"'."\r\n";
                }
              $last_titles="Total";
                $superTotal = 0;
                echo  '"'."Total ,";
                if($salaries){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                       $sum += isset($item['Salaries'])?$item['Salaries']:0;
                    }$sum.
                     $superTotal += $sum;
                    echo $sum.",";
                }
                if($allowances){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Allowances'])?$item['Allowances']:0;
                    }
                    $sum.
                    $superTotal += $sum.".";
                    echo $sum.",";
                }
                if($expenses){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Expenses'])?$item['Expenses']:0;
                    }
                    $sum.
                    $superTotal += $sum.",";
                    echo $sum.",";
                }
                if($deductions){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Deductions'])?$item['Deductions']:0;
                    }
                    $sum.
                    $superTotal -= $sum.",";
                    echo $sum.",";
                }
                echo "GrandTotal : ".$superTotal;
            }
            ?>
