<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}</script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
    @media print {
        #printButton {
            display: none;
        }
        a:link:after, a:visited:after {
            content: "" !important;
            display: none !important;
            background: black;
        }
    }
body{
	background: #f8f8f8;
}
table th{
	font-size: 13px;
	font-weight:bold;
}
table td{
	font-size: 13px;
}
.report_msg{
    padding: 2em 1em;
    font-size: 18px;
    font-weight: bold;
    color: darkred;
    width:30%;
    margin:5em 12em;
    border: 1px solid #000066;

}
</style>

<?php
if(isset($EmployeeData) && !empty($EmployeeData)){
$EmployeeDataArray = json_decode(json_encode($EmployeeData),true);

?>
<div class="row" style="width:99%; height:600px; margin:0px auto;background:#fff; padding:3em 0.3em;">
		<div class="col-lg-12">
			<div class="col-lg-11">
				<h4 class="text-center">
                    <?php if(isset($reportHeading) && !empty($reportHeading)){
                        echo $reportHeading;
                    }else{
                        echo "List Of Employees";
                    }?>
                </h4>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
				  <tr>
                      <?php
                       foreach($EmployeeDataArray[0] as $key=>$value){
                           echo "<th>".str_replace('_', ' ', $key)."</th>";
                       }
                      ?>
				  </tr>
				</thead>
				<tbody>
                      <?php
                      foreach($EmployeeDataArray as $key=>$value){
                          echo "<tr>";
                          foreach($value as $subKey=>$subValue){
                            echo "<td>".$subValue."</td>";
                          }
                          echo "</tr>";
                      }
                      ?>
				</tbody>
			</table>
	</div>
</div>
    <button type="button" id="printButton" onClick="window.print()">Print</button>
<?php
}else{
    echo "<div class=\"report_msg\"> No Records Available For Employees</div>";
}
?>
						
