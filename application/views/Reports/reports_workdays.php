
<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Leave & Attendence Reports / Project ManDays Report </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Project ManDays Reports
		<span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
		</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" name="" placeholder="Employee Name">
				<select>
					<option>Project</option>
				</select>
				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>
			<br class="clear">
			<hr>
	<div style="width: 50%; margin:20px auto;" id="chartbar">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<!-- tabuler data -->
	<div id="tablebar">
		<table class="table" cellspacing="0">
			<thead class="table-head">
				<td>Months</td>
				<td>No Of Absenties</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td>January</td>
					<td>5</td>
				</tr>
			</tbody>
		</table>
	</div>
    <script src="<?php echo base_url() ?>assets/js/reports.js"></script>
		<!-- Menu left side  -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
 	</div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
  	</script>
  	<!-- leftside menu end -->
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["Project one","Project 2","Project 3","April","May","June","July"],
		datasets : [
			
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	};
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>
		</div>
	</div>
</div>
<!-- contents