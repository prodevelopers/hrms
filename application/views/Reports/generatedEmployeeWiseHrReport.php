<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/7/2015
 * Time: 4:39 PM
 */
if(isset($SelectedCheckBoxes) and isset($ReportTableData)){
    if(strpos($SelectedCheckBoxes,'Salaries') !== false){
        $salaries = true;
    }else{
        $salaries = false;
    }
    if(strpos($SelectedCheckBoxes,'Allowances') !== false){
        $allowances = true;
    }else{
        $allowances = false;
    }
    if(strpos($SelectedCheckBoxes,'Expenses') !== false){
        $expenses = true;
    }else{
        $expenses = false;
    }
    if(strpos($SelectedCheckBoxes,'Deductions') !== false){
        $deductions = true;
    }else{
        $deductions = false;
    }
}
?>
<!doctype HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Employee Wise Payroll Report</title>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
</head>
<body>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">
        <button type="button" id="PdfButton" class="btn btn-default btn-sm pull-right">
            PDF
        </button>
        <a href="<?php echo base_url().@$files;?>"><button type="button" id="PdfButton" class="btn btn-default btn-sm pull-right">
            CSV
        </button></a>
        <button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#myModal">
            Send As Email Attachment
        </button>
        <h4 class="text-center"><?php echo (isset($reportHeading) && !empty($reportHeading))?$reportHeading:'EmployeesWise Payroll Report'; ?></h4>

        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>EmployeeName</th>
                <th>Employee Code</th>
                <th>Designation</th>
                <th>Monthly Salary</th>
                <th>Salary Month and Year</th>
            </tr>
            </thead>
            <tbody>
        <?php
        //This Section Will Execute If Employee Filter is Selected

        if (isset($EmployeeInfo) && !empty($EmployeeInfo)) {
            ?>
                <?php
                echo "<tr><td>" . $EmployeeInfo->EmployeeName . "</td><td>" . $EmployeeInfo->EmployeeCode . "</td><td>" . $EmployeeInfo->Designation . "</td><td>" . ((isset($projectChargesReportData) && !empty($projectChargesReportData[0]->MonthlySalary))?$projectChargesReportData[0]->MonthlySalary:''). "</td><td>" .$Month.' - '.$filteredYear."</td></tr>";
                ?>
        <?php } ?>
            </tbody>
        </table>
        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>Project Name/Abbreviation</th>
                <th>Days Worked</th>
                <th>% Charges</th>
                <th>Payable Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //List The Projects For The Selected Employees IF Exist Any..
            if (isset($projectChargesReportData) && !empty($projectChargesReportData)) {
                foreach ($projectChargesReportData as $projectKey => $projectVal) {
                    echo "<tr><td>" . $projectVal->ProjectTitle . "(" . $projectVal->ProjectAbbreviation . ")</td><td>" . $projectVal->NumberOfDays . "</td><td>" .$projectVal->PercentCharged . "% </td><td>" . $projectVal->PayableAmount . "</td></tr>";
                }
            }
            ?>
            </tbody>
        </table> <!--End Of Employee Projects Payroll Details-->
    </div>
</div>

<!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
    $(document).ready(function(){
        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>reports_con/SendPDFEmployeeWisePayrollReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });

    $("#PdfButton").click(function(){
        var targetURL = '<?php echo base_url();?>reports_con/downloadGeneratedPDFReportEmployeeWisePayRoll';
        <?php if(isset($viewPdf) && !empty($viewPdf)){ ?>
        var postData = [];
        postData.push({name:"viewData",value:<?php echo json_encode($viewPdf); ?>});
        console.log(postData);
        $.ajax({
            url: targetURL,
            data:postData,
            type:"POST",
            success: function (output) {
                var data = output.split("::");
                if(data[0] === "Download"){
                    //console.log(data[1]);
                    //document.location = <?php //echo base_url();?>data[1];
                }
            }
        });
        <?php } ?>
    });
</script>
</body>
</html>