<?php /**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2/2/2015
 * Time: 1:10 PM
 */ ?>
<!-- contents -->

<script src="<?php echo base_url(); ?>assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Leave & Attendence Reports / Customize Reporting </div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">
		<div class="head">Create Customize Report
        <span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
		</div>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <div id="selectFilters">
				<input type="hidden" id="projectFilterSelector" name="projectFilter">
				<input type="hidden" id="departmentFilterSelector" name="departmentFilter">
				<input type="hidden" id="leaveTypeFilterSelector" name="leaveTypeFilter">
				<input type="hidden" id="selectYearSelector" name="selectYear">
                </div>
                <h4 style="padding-top:20px;">Report Types</h4>
                <div class="checkBoxFilters">
                    <table id="checkBoxSelects" style="width:20%;float: left">
                        <tbody><tr class="table-row">
                            <td><input type="checkbox" name="Leaves"> Leaves</td>
                            <td><input type="checkbox" name="Absentees"> Absenties</td>
                        </tr>
                        </tbody></table>
                </div>
                <br/>
				<div class="button-group">
                    <button class="btn green" id="filterBtn">Show</button>
					<a style="cursor: pointer" class="btn green" id="printBtn"><span class="fa fa-print"> </span> Print</a>
				</div>
			</div>

			<br class="clear">
			<hr>
	<div style="width: 50%; margin:20px auto;" id="chartbar">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>

	<!-- Tabular data -->
	<div id="tablebar">
		<table class="table" cellspacing="0">
			<thead class="table-head">
				<td>Months</td>
				<td>Total Leaves Taken</td>
				<td>Total Absentees</td>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
    <script src="<?php echo base_url() ?>assets/js/jquery.redirect.js"></script>
	<!-- Menu left side  -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
    </div>
 <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
 <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    $('#chartbar').hide();
    $('#chart').click(function(){
        $('#tablebar').hide();
        $('#chartbar').show();
    });
    $('#table').click(function(){
        $('#tablebar').show();
        $('#chartbar').hide();
    });
    </script>

<!-- leftside menu end -->
<script>
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var barChartData = {
        labels : [<?php foreach($leave as $name){  echo '"';echo $name->month;echo '",'; }?>],
        datasets : [
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,0.8)",
                highlightFill : "rgba(151,187,205,0.75)",
                highlightStroke : "rgba(151,187,205,1)",
                data : [<?php foreach($leave as $name){ echo '"';echo $name->counter; echo '",'; }?>]
            }
        ]

    };
    window.onload = function(){
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx).Bar(barChartData, {
            responsive : true
        });
    }

</script>
        <script>
            $(document).ready(function(e){
               //Loading Selectors For the Filters
                /*The Selector for Selecting the Project*/
                var projectFilterSelector = $('#projectFilterSelector');
                var url = "<?php echo base_url(); ?>reports_con/load_all_available_projects";
                var id = "ProjectID";
                var text = "ProjectName";
                var minInputLength = 0;
                var placeholder = "Select Project";
                var multiple = false;
                commonSelect2(projectFilterSelector,url,id,text,minInputLength,placeholder,multiple);
                projectFilterSelector.on('change',function(e){
                    departmentFilterSelector.select2('disable');
                });

                /*The Selector for Selecting the Department*/
                var departmentFilterSelector = $('#departmentFilterSelector');
                var url = "<?php echo base_url(); ?>reports_con/load_all_available_departments";
                var id = "DepartmentID";
                var text = "Department";
                var minInputLength = 0;
                var placeholder = "Select Department";
                var multiple = false;
                commonSelect2(departmentFilterSelector,url,id,text,minInputLength,placeholder,multiple);
                departmentFilterSelector.on('change',function(e){
                    projectFilterSelector.select2('disable');
                });

                /*The Selector for Selecting the Department*/
                var leaveTypeFilterSelector = $('#leaveTypeFilterSelector');
                var url = "<?php echo base_url(); ?>reports_con/load_all_available_leave_types";
                var id = "LeaveTypeID";
                var text = "LeaveType";
                var minInputLength = 0;
                var placeholder = "Select Leave Type";
                var multiple = false;
                commonSelect2(leaveTypeFilterSelector,url,id,text,minInputLength,placeholder,multiple);
                if($('input[name="Leaves"]').is(':checked')){
                    leaveTypeFilterSelector.select2('enable');
                }else{
                    leaveTypeFilterSelector.select2('disable');
                }

                /*The Selector for Selecting the Department*/
                var selectYearSelector = $('#selectYearSelector');
                var url = "<?php echo base_url(); ?>reports_con/load_all_available_years";
                var id = "Year";
                var text = "Year";
                var minInputLength = 0;
                var placeholder = "Select Year";
                var multiple = false;
                commonSelect2(selectYearSelector,url,id,text,minInputLength,placeholder,multiple);

                //Little Fix For Leave CheckBox and Filter..
                $('input[type="checkbox"]').change(function () {
                    if ($(this).attr('name') === 'Leaves') {
                        if (this.checked) {
                            leaveTypeFilterSelector.select2('enable');
                        } else {
                            leaveTypeFilterSelector.select2('disable');
                        }
                    }
                });

                //Resize All The Selectors To Certaint Width
                $('.select2-container').css("width","223px");
                $('#filterBtn').on('click',function(e){
                    //Filter Button
                    //Lets Get The Filters.
                       var selectedProjectID = projectFilterSelector.val();
                       var selectedDepartmentID = departmentFilterSelector.val();
                       var selectedLeaveTypeID = leaveTypeFilterSelector.val();
                       var selectedYear = selectYearSelector.val();


                    //Lets See How Many CheckBoxes Have Been Checked.
                    var reportTypes = new Array();
                    var array = $('#checkBoxSelects input:checked');
                    $.each(array, function (i, e) {
                        reportTypes.push($(e).attr('name'));
                    });

                    if(reportTypes.length >0){
                        var postURL = "<?php echo base_url(); ?>reports_con/generate_leave_attendance_report";
                        var postData = {
                            selectData: reportTypes
                        };
                        //Project Or Department Filter IF Selected Any of Them.
                        if(selectedProjectID.length > 0){
                            postData.projID = selectedProjectID;
                        }else if(selectedDepartmentID.length > 0){
                            postData.departmentID = selectedDepartmentID;
                        }
                        //Post LeaveType If Selected.
                        if(selectedLeaveTypeID.length > 0){
                            postData.leaveTypeID = selectedLeaveTypeID;
                        }
                        //Post LeaveType If Selected.
                        if(selectedYear.length > 0){
                            postData.year = selectedYear;
                        }

                        //As We Got All The Data We Need. We Will Send Request To The Controller.
                        $.ajax({
                            url: postURL,
                            data:postData,
                            type:"POST",
                            dataType:"json",
                            success: function (data) {
                                //Empty The Table First
                                $('.table tbody').html('');
                                $.each(data, function(i, item) {
                                    if(item.hasOwnProperty('TotalLeaves') && item.hasOwnProperty('TotalAbsentees')){
                                        var $tr = $('<tr class="table-row">').append(
                                            $('<td>').text(i),
                                            $('<td>').text(item.TotalLeaves),
                                            $('<td>').text(item.TotalAbsentees)
                                        );
                                        $('.table thead td:eq(1), .table thead td:eq(2)').show();
                                    }else if(item.hasOwnProperty('TotalLeaves')){
                                        var $tr = $('<tr class="table-row">').append(
                                            $('<td>').text(i),
                                            $('<td>').text(item.TotalLeaves)
                                        );
                                        $('.table thead td:eq(1)').show();
                                        $('.table thead td:eq(2)').hide();
                                    }else if(item.hasOwnProperty('TotalAbsentees')){
                                        var $tr = $('<tr class="table-row">').append(
                                            $('<td>').text(i),
                                            $('<td>').text(item.TotalAbsentees)
                                        );
                                        $('.table thead td:eq(1)').hide();
                                        $('.table thead td:eq(2)').show();
                                    }
                                    //Append Elements To Empty Table
                                    $tr.appendTo('.table tbody');
                            });
                        }
                    });
                }else{
                        Parexons.notification('You Must Select At Least 1 Report Type','warning');
                    }
            });
                $('#printBtn').on('click',function(e){
                    //Lets Get The Filters.
                    var selectedProjectID = projectFilterSelector.val();
                    var selectedDepartmentID = departmentFilterSelector.val();
                    var selectedLeaveTypeID = leaveTypeFilterSelector.val();
                    var selectedYear = selectYearSelector.val();

                    //Lets See Again How Many CheckBoxes Have Been Checked.
                    var reportTypes = new Array();
                    var array = $('#checkBoxSelects input:checked');
                    $.each(array, function (i, e) {
                        reportTypes.push($(e).attr('name'));
                    });

                    //If No CheckBox Selected Then No Print To Execute For User, Else Will Execute The Print Option
                    if(reportTypes.length >0){
                        var postURL = "<?php echo base_url(); ?>reports_con/generate_leave_attendance_report/print";
                        var postData = {
                            selectData: reportTypes
                        };
                        //Project Or Department Filter IF Selected Any of Them.
                        if(selectedProjectID.length > 0){
                            postData.projID = selectedProjectID;
                        }else if(selectedDepartmentID.length > 0){
                            postData.departmentID = selectedDepartmentID;
                        }
                        //Post LeaveType If Selected.
                        if(selectedLeaveTypeID.length > 0){
                            postData.leaveTypeID = selectedLeaveTypeID;
                        }
                        //Post LeaveType If Selected.
                        if(selectedYear.length > 0){
                            postData.year = selectedYear;
                        }

                        //Now We Need To Post The Data But Not Through Ajax. :)
                        $.redirect(postURL,postData,'POST');

                    }else{
                        Parexons.notification('You Must Select At Least 1 Report Type','warning');
                    }
                });
            });
        </script>
</div>
</div>
</div>
<!-- contents -->