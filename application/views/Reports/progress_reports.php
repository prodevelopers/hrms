<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Progress Report </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Progress Reports
		<span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
		</div>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee">
				<select>
					<option>Select Project</option>
				</select>

				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>
			<br class="clear">
			<hr>
	<div style="width: 50%; margin:20px auto;" id="chartbar">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<!-- tabuler data -->
	<div id="tablebar">
		<table class="table" cellspacing="0">
			<thead class="table-head">
				<td>Months</td>
				<td>No Of Absenties</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td>January</td>
					<td>5</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- left menu -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
 	</div>
    <script src="<?php echo base_url() ?>assets/js/reports.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
  	</script>
  	<!-- leftside menu end -->
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	var barChartData = {
		labels : [<?php foreach($name as $nam){  echo '"';echo $nam->kpi;echo '",'; }?>],
		datasets : [
		  {
			fillColor : "rgba(151,187,205,0.5)",
			strokeColor : "rgba(151,187,205,0.8)",
			highlightFill : "rgba(151,187,205,0.75)",
			highlightStroke : "rgba(151,187,205,1)",
			data : [<?php foreach($name as $nam){
			echo '"';echo $nam->counter; echo '",'; }?>],
		  }
		]
	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}
	</script>
<div class="form-left">
		<?php echo form_open(); ?>
          <br class="clear">
				<div class="row2">
					<h4>Employee Feedback</h4>
					<textarea name="reviewer" rows="7" cols="33"></textarea>
				</div>
                
				<br class="clear">
				<div class="row2">
					<h4>Reviewer Comments</h4>
					<textarea name="comments" rows="7" cols="33"></textarea>
				</div>
            <!-- button group -->
				<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="continue" value="Submit" class="btn green" />
				</div>
			</div>
            <?php echo form_close(); ?>
		</div>
		</div>
	</div>
</div>

<!-- contents