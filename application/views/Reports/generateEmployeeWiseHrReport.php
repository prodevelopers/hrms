<style>
    .select2-results li{
        width: 300px;
        border-bottom:thin solid #ccc;
        height:60px;
    }
    .select2TemplateImg { padding:0.2em 0;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
</style>
<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/22/2015
 * Time: 11:58 AM
 */

$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->

<div class="contents-container">
    <div class="bredcrumb">Dashboard / Reports / Generate Program Wise Employees Report </div> <!-- breadCrumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Generate Program Wise Employees Report</div>
        <form>
            <div class="row">
                <h4>Employee Name</h4>
                <input type="hidden" id="selectEmployees" name="emp">
            </div>
            <br class="clear">
            <div class="row">
                <h4>Programs</h4>
                <input type="hidden" id="selectPro" name="pro" required="required">
            </div>
        </form>
        <!-- button group -->
        <div class="row">
            <input type="button" class="btn green" value="Generate Report" id="generateReportButton">
            <input type="button" class="btn red" value="Clear" id="clearFormButton">
        </div>
    </div>
</div>
<!--End of Contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function () {

        /*
         * New Select Employee
         * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
         * */
        var selectEmployeesSelector = $('#selectEmployees');
        var url = "<?php echo base_url(); ?>human_resource/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeesSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);

        //Loading All Available Years Filter..
        var selectProSelector = $('#selectPro');
        var url = "<?php echo base_url(); ?>reports_con/loadAllAvailablePrograms";
        var id = "ProgramID";
        var text = "ProTitle";
        var minInputLength = 0;
        var placeholder = "Select Program";
        var multiple = false;
        commonSelect2(selectProSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");


        /***  Finally End Of All The Filters..***/
        $('#generateReportButton').on('click',function(e){
            //If Filters Are Selected Do The Filtering..
            //Project Filter

           /* if(monthID.length === 0){
                Parexons.notification('Please Select Month From Drop Down','warning');
                return false;
            }*/

            var postURL = "<?php echo base_url(); ?>reports_con/programeWiseReports2";


            var project=$('#selectPro').val();
            var employee=$('#selectEmployees').val();


            var postData = {
               ProgramIDS : project,
                Employee: employee
            };
            if(project.length < 1){
                alert("Please Select Program");
            }else{$.redirect(postURL,postData,'POST');}
        });

        //Need A Clear Button To Clear The Form When Pressed..
        $('#clearFormButton').on('click', function (e) {
            $('div.right-contents').find('form')[0].reset();
            $('.select2-container').select2('val', '');
        });

        //Page Messages

        //Page Messages.
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>
<!-- Menu left side -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/reports_list_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>
<!-- leftside menu end -->