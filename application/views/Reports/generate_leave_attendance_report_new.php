<?php /**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2/2/2015
 * Time: 1:10 PM
 */ ?>
<!-- contents -->
<style>
    .DTTT_container{
        background:white;
        margin-top: 10px;
        margin-left:20px;
    }
    .DTTT_button_copy{
        padding:6px;
        margin:7px 5px 5px 5px;
        border:1px solid #d3d3d3;
        border-radius: 5px;
    }
    .DTTT_button_copy:hover,.DTTT_button_csv:hover,
    .DTTT_button_xls:hover,.DTTT_button_pdf:hover,.DTTT_button_print:hover{cursor:pointer}
    .DTTT_button_pdf,.DTTT_button_print,.DTTT_button_xls,.DTTT_button_csv{
        padding:6px;
        margin:7px 5px 5px 5px;
        border:1px solid #d3d3d3;
        border-radius: 5px;
    }
    .btns_row{
        float:right;
        width:380px;
        height:30px;
    }

    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    /*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
</style>
<script src="<?php echo base_url(); ?>assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

<div class="bredcrumb">Dashboard / Reports / Leave & Attendence Reports / Customize Reporting </div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


<div class="right-contents">
<div class="head">Create Leave & Attendance Customize Report
        <span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
</div>
<!-- filter -->
<div class="filter">
    <h4>Filter By</h4>
    <div id="selectFilters">
        <input type="hidden" id="programFilterSelector" name="programFilter">
        <input type="hidden" id="projectFilterSelector" name="projectFilter">
        <input type="hidden" id="departmentFilterSelector" name="departmentFilter">
        <input type="hidden" id="leaveTypeFilterSelector" name="leaveTypeFilter">
        <input type="hidden" id="selectYearSelector" name="selectYear">


    </div>
    <h4 style="padding-top:20px;">Report Types</h4>
    <div class="checkBoxFilters">
        <table id="checkBoxSelects" style="width:20%;float: left">
            <tbody><tr class="table-row">
                <td><input type="checkbox" name="Leaves"> Leaves</td>
                <td><input type="checkbox" name="Absentees"> Absentees</td>
            </tr>
            </tbody></table>
    </div>
    <br/>
    <div class="button-group">
        <button class="btn green" id="filterBtn">Show</button>
        <input type="button" name="" id="FilterResetButton" class="btn red" value="Reset">
<!--        <a style="cursor: pointer" class="btn green" id="printBtn"><span class="fa fa-print"> </span> Print</a>-->
    </div>
</div>

<br class="clear">
<hr>
<div style="width: 50%; margin:20px auto;" id="chartbar">
    <canvas id="canvas" height="450" width="700"></canvas>
</div>

<!-- Tabular data -->
<div id="tablebar">
    <table class="table" cellspacing="0" id="leaveAbsenteesTable">
        <thead class="table-head">
        <tr>
        <td></td>
        <td>Employee Code</td>
        <td>Employee Name</td>
        <td id="leavesTakenTD">Total Leaves Taken</td>
        <td id="absenteesTakenTD">Total Absentees</td>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script src="<?php echo base_url() ?>assets/js/jquery.redirect.js"></script>
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/reports_list_nav'); ?>
</div>
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.min.css" type="text/css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery.panelslider.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/dataTables.tableTools.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $('#chartbar').hide();
    $('#chart').click(function(){
        $('#tablebar').hide();
        $('#chartbar').show();
    });
    $('#table').click(function(){
        $('#tablebar').show();
        $('#chartbar').hide();
    });
</script>

<!-- leftside menu end -->
<script>
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var barChartData = {
        labels : [<?php foreach($leave as $name){  echo '"';echo $name->month;echo '",'; }?>],
        datasets : [
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,0.8)",
                highlightFill : "rgba(151,187,205,0.75)",
                highlightStroke : "rgba(151,187,205,1)",
                data : [<?php foreach($leave as $name){ echo '"';echo $name->counter; echo '",'; }?>]
            }
        ]

    };
    window.onload = function(){
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx).Bar(barChartData, {
            responsive : true
        });
    }

</script>
<script>
var oTable;
    $(document).ready(function(e){


        //Loading Selectors For the Filters
        /*The Selector for Selecting the Project*/
        var projectFilterSelector = $('#projectFilterSelector');
        var url = "<?php echo base_url(); ?>reports_con/load_all_available_projects";
        var id = "ProjectID";
        var text = "ProjectName";
        var minInputLength = 0;
        var placeholder = "Select Project";
        var multiple = false;
        commonSelect2(projectFilterSelector,url,id,text,minInputLength,placeholder,multiple);
        projectFilterSelector.on('change',function(e){
            departmentFilterSelector.select2('disable');
        });

        /// Ajax Function Program Filter ///
        var programFilterSelector = $('#programFilterSelector');
        var url = "<?php echo base_url(); ?>reports_con/load_all_available_Program";
        var id = "PROGRAMID";
        var text = "ProgramName";
        var minInputLength = 0;
        var placeholder = "Select Program";
        var multiple = false;
        commonSelect2(programFilterSelector,url,id,text,minInputLength,placeholder,multiple);
        projectFilterSelector.on('change',function(e){
            programFilterSelector.select2('disable');
        });

        /*The Selector for Selecting the Department*/
        var departmentFilterSelector = $('#departmentFilterSelector');
        var url = "<?php echo base_url(); ?>reports_con/load_all_available_departments";
        var id = "DepartmentID";
        var text = "Department";
        var minInputLength = 0;
        var placeholder = "Select Department";
        var multiple = false;
        commonSelect2(departmentFilterSelector,url,id,text,minInputLength,placeholder,multiple);
        departmentFilterSelector.on('change',function(e){
            projectFilterSelector.select2('disable');
        });

        /*The Selector for Selecting the Department*/
        var leaveTypeFilterSelector = $('#leaveTypeFilterSelector');
        var url = "<?php echo base_url(); ?>reports_con/load_all_available_leave_types";
        var id = "LeaveTypeID";
        var text = "LeaveType";
        var minInputLength = 0;
        var placeholder = "Select Leave Type";
        var multiple = false;
        commonSelect2(leaveTypeFilterSelector,url,id,text,minInputLength,placeholder,multiple);
        if($('input[name="Leaves"]').is(':checked')){
            leaveTypeFilterSelector.select2('enable');
        }else{
            leaveTypeFilterSelector.select2('disable');
        }

        /*The Selector for Selecting the Department*/
        var selectYearSelector = $('#selectYearSelector');
        var url = "<?php echo base_url(); ?>reports_con/load_all_available_years";
        var id = "Year";
        var text = "Year";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
        commonSelect2(selectYearSelector,url,id,text,minInputLength,placeholder,multiple);

        //Little Fix For Leave CheckBox and Filter..
        $('input[type="checkbox"]').change(function () {
            if ($(this).attr('name') === 'Leaves') {
                if (this.checked) {
                    leaveTypeFilterSelector.select2('enable');
                } else {
                    leaveTypeFilterSelector.select2('disable');
                }
            }
        });


        //Datable

        oTable = '';
        //Load List of Approvals in DataTables.
        var selectorTableManualTimeSheet =  $('#leaveAbsenteesTable');
        var url_DT = "<?php echo base_url(); ?>reports_con/generate_employees_leave_attendance_report/listEmployeesLeaveAndAbsentees";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "EmployeeID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Employee Code */ {
                "mData" : "EmployeeCode"
            },
            /* Employee Name */ {
                "mData" : "EmployeeName"
            },
            /* Total Leaves Taken */ {
                "mData" : "TotalLeavesTaken"
            },
            /* Total Absentees */ {
                "mData" : "TotalAbsenteesTaken"
            }
        ];
        var aButtons_DT = [
            "copy",
            "print", {
                "sExtends": "collection",
                "sButtonText": "Save", // button name
                // "aButtons":    [ "csv", "xls", "pdf" ]
                "aButtons": [
                    {
                        "sExtends": "csv",
                        "sButtonText": "CSV",
                        "mColumns": [ 1, 2, 3, 4 ]
                    },
                    {
                        "sExtends": "xls",
                        "sButtonText": "Excel",
                        "sFileName": "*.xls",
                        "mColumns": [ 1, 2, 3, 4 ]
                    },
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "landscape",
                        "sPdfMessage": "Employees Leaves and Absentees Report.",
                        "mColumns": [ 1, 2, 3, 4 ]
                    },
                    "print"
                ]
            }
        ];
        var HiddenColumnID_DT = 'EmployeeID';
        var sDom_DT = '<"H" Tr>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTablesTableTools(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,aButtons_DT);


        //Resize All The Selectors To Certaint Width
        $('.select2-container').css("width","223px");
        $('#filterBtn').on('click',function(e){
            //Filter Button
            //Lets Get The Filters.
            var selectedProjectID = projectFilterSelector.val();
            var selectedProgramID = programFilterSelector.val();
            var selectedDepartmentID = departmentFilterSelector.val();
            var selectedLeaveTypeID = leaveTypeFilterSelector.val();
            var selectedYear = selectYearSelector.val();


            //Lets See How Many CheckBoxes Have Been Checked.
            var reportTypes = new Array();
            var array = $('#checkBoxSelects input:checked');
            $.each(array, function (i, e) {
                reportTypes.push($(e).attr('name'));
            });

            if(reportTypes.length >0){
                aButtons_DT =[];
                oTable.fnDestroy();
//                $(".DTTT_collection").remove();
                if(reportTypes.length === 1){
                    if($.inArray('Leaves', reportTypes) > -1){
                        aoColumns_DT = [
                            /* ID */ {
                                "mData": "EmployeeID",
                                "bVisible": false,
                                "bSortable": false,
                                "bSearchable": false
                            },
                            /* Employee Code */ {
                                "mData" : "EmployeeCode"
                            },
                            /* Employee Name */ {
                                "mData" : "EmployeeName"
                            },
                            /* Total Leaves Taken */ {
                                "mData" : "TotalLeavesTaken"
                            }
                        ];
                        aButtons_DT = [
                            "copy",
                            "print", {
                                "sExtends": "collection",
                                "sButtonText": "Save", // button name
                                // "aButtons":    [ "csv", "xls", "pdf" ]
                                "aButtons": [
                                    {
                                        "sExtends": "csv",
                                        "sButtonText": "CSV",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    {
                                        "sExtends": "xls",
                                        "sButtonText": "Excel",
                                        "sFileName": "*.xls",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    {
                                        "sExtends": "pdf",
                                        "sPdfOrientation": "landscape",
                                        "sPdfMessage": "List Of Employees On Leaves.",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    "print"
                                ]
                            }
                        ];
                        $('table#leaveAbsenteesTable tr').remove();
                        $('table#leaveAbsenteesTable thead').append('<tr><td></td><td></td><td>Employee Name</td><td>Total Leaves Taken</td></tr>');
                    }else if($.inArray('Absentees', reportTypes) > -1){
                        aoColumns_DT = [
                            /* ID */ {
                                "mData": "EmployeeID",
                                "bVisible": false,
                                "bSortable": false,
                                "bSearchable": false
                            },
                            /* Employee Code */ {
                                "mData" : "EmployeeCode"
                            },
                            /* Employee Name */ {
                                "mData" : "EmployeeName"
                            },
                            /* Total Leaves Taken */ {
                                "mData" : "TotalAbsenteesTaken"
                            }
                        ];
                        aButtons_DT = [
                            "copy",
                            "print", {
                                "sExtends": "collection",
                                "sButtonText": "Save", // button name
                                // "aButtons":    [ "csv", "xls", "pdf" ]
                                "aButtons": [
                                    {
                                        "sExtends": "csv",
                                        "sButtonText": "CSV",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    {
                                        "sExtends": "xls",
                                        "sButtonText": "Excel",
                                        "sFileName": "*.xls",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    {
                                        "sExtends": "pdf",
                                        "sPdfOrientation": "landscape",
                                        "sPdfMessage": "List of Employees Taken Absentees.",
                                        "mColumns": [ 1, 2, 3 ]
                                    },
                                    "print"
                                ]
                            }
                        ];
                        $('table#leaveAbsenteesTable tr').remove();
                        $('table#leaveAbsenteesTable thead').append('<tr><td>Employee ID</td><td>Employee Code</td><td>Employee Name</td><td>Total Absentees</td></tr>');
                    }
                }else{
                    aoColumns_DT = [
                        /* ID */ {
                            "mData": "EmployeeID",
                            "bVisible": false,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        /* Employee Code */ {
                            "mData" : "EmployeeCode"
                        },
                        /* Employee Name */ {
                            "mData" : "EmployeeName"
                        },
                        /* Total Leaves Taken */ {
                            "mData" : "TotalLeavesTaken"
                        },
                        /* Total Absentees */ {
                            "mData" : "TotalAbsenteesTaken"
                        }
                    ];

                    aButtons_DT = [
                        "copy",
                        "print", {
                            "sExtends": "collection",
                            "sButtonText": "Save", // button name
                            // "aButtons":    [ "csv", "xls", "pdf" ]
                            "aButtons": [
                                {
                                    "sExtends": "csv",
                                    "sButtonText": "CSV",
                                    "mColumns": [ 1, 2, 3, 4 ]
                                },
                                {
                                    "sExtends": "xls",
                                    "sButtonText": "Excel",
                                    "sFileName": "*.xls",
                                    "mColumns": [ 1, 2, 3, 4 ]
                                },
                                {
                                    "sExtends": "pdf",
                                    "sPdfOrientation": "landscape",
                                    "sPdfMessage": "List of product.",
                                    "mColumns": [ 1, 2, 3, 4 ]
                                },
                                "print"
                            ]
                        }
                    ];
                    $('table#leaveAbsenteesTable tr').remove();
                    $('table#leaveAbsenteesTable thead').append('<tr><td>Employee ID</td><td>Employee Code</td><td>Employee Name</td><td>Total Leaves Taken</td><td>Total Absentees</td></tr>');
                }
//As We Have Selected The Table Data To Show.. It Will Show Data..

                var filters = 'aoData.push({"name":"year","value":"'+selectedYear+'"}); aoData.push({"name":"reportTypes","value":"'+reportTypes+'"});  aoData.push({"name":"project","value":"'+selectedProjectID+'"});  aoData.push({"name":"department","value":"'+selectedDepartmentID+'"});  aoData.push({"name":"leaveType","value":"'+selectedLeaveTypeID+'"}); aoData.push({"name":"ProgramName","value":"'+selectedProgramID+'"});';
                commonDataTablesFilteredTableTools(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,aButtons_DT,filters)
            }else{
                Parexons.notification('You Must Select At Least 1 Report Type','warning');
            }
        });
        $('#printBtn').on('click',function(e){
            //Lets Get The Filters.
            var selectedProjectID = projectFilterSelector.val();
            var selectedprogramID = projectFilterSelector.val();
            var selectedDepartmentID = departmentFilterSelector.val();
            var selectedLeaveTypeID = leaveTypeFilterSelector.val();
            var selectedYear = selectYearSelector.val();

            //Lets See Again How Many CheckBoxes Have Been Checked.
            var reportTypes = new Array();
            var array = $('#checkBoxSelects input:checked');
            $.each(array, function (i, e) {
                reportTypes.push($(e).attr('name'));
            });

            //If No CheckBox Selected Then No Print To Execute For User, Else Will Execute The Print Option
            if(reportTypes.length >0){
                var postURL = "<?php echo base_url(); ?>reports_con/generate_leave_attendance_report/print";
                var postData = {
                    selectData: reportTypes
                };
                //Project Or Department Filter IF Selected Any of Them.
                if(selectedprogramID.length > 0){
                    postData.projID = selectedprogramID;
                }else if(selectedProjectID.length > 0){
                    postData.ProjectID = selectedProjectID;
                }

                //Project Or Department Filter IF Selected Any of Them.
                if(selectedProjectID.length > 0){
                    postData.projID = selectedProjectID;
                }else if(selectedDepartmentID.length > 0){
                    postData.departmentID = selectedDepartmentID;
                }

                 //Project Or Department Filter IF Selected Any of Them.
                if(selectedprogramID.length > 0){
                    postData.projID = selectedprogramID;
                }else if(selectedProjectID.length > 0){
                    postData.departmentID = selectedProjectID;
                }
                //Post LeaveType If Selected.
                if(selectedLeaveTypeID.length > 0){
                    postData.leaveTypeID = selectedLeaveTypeID;
                }
                //Post LeaveType If Selected.
                if(selectedYear.length > 0){
                    postData.year = selectedYear;
                }
                //Now We Need To Post The Data But Not Through Ajax. :)
                $.redirect(postURL,postData,'POST');
            }else{
                Parexons.notification('You Must Select At Least 1 Report Type','warning');
            }
        });
        $('#FilterResetButton').on('click', function (e) {
            programFilterSelector.select2('enable');
            projectFilterSelector.select2('val', '');
            departmentFilterSelector.select2('enable');
            leaveTypeFilterSelector.select2('enable');
            oTable.fnDestroy();
            commonDataTables(selectorTableManualTimeSheet,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
        });
    });
</script>
</div>
</div>
</div>
<!-- contents -->