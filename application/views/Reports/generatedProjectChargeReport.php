<!DOCTYPE HTML>
<html>
<head>
<title><?php echo (isset($reportHeading) && !empty($reportHeading))? $reportHeading:'Project Charge Report' ?></title>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>

<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<!--<script src="--><?php //echo base_url()?><!--assets/js/bootstrap.js"></script>-->
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
<style>
    @media print {
        #printButton {
            display: none;
        }
        a:link:after, a:visited:after {
            content: "" !important;
            display: none !important;
            background: black;
        }
    }
    body{
        background: #f8f8f8;
    }
    table th{
        font-size: 13px;
        font-weight:bold;
    }
    table td{
        font-size: 13px;
    }
    .report_msg{
        padding: 2em 1em;
        font-size: 18px;
        font-weight: bold;
        color: darkred;
        width:30%;
        margin:5em 12em;
        border: 1px solid #000066;
    }
</style>
</head>
<body>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">
        <button type="button" id="sendEmailButton" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#myModal">
            Send
        </button>
        <button type="button" id="PDFButton" class="btn btn-default btn-sm pull-right">
            PDF
        </button>
        <a href="<?php echo base_url().$files?>" <button type="button" class="btn btn-default btn-sm pull-right">
            CSV
        </button></a>
        <h4 class="text-center"><?php echo (isset($reportHeading) && !empty($reportHeading))? $reportHeading:'Project Charge Report' ?></h4>

        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>Program Name</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            </thead>
            <tbody>
            <tr><td><?php echo $projectChargesReportData[0]->PROGRAM; ?></td><td><?php echo isset($Month)?$Month:''; ?></td><td>2015</td></tr>
            </tbody>
        </table>

        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>Project Name</th>
                <th>Project Abbreviation</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            </thead>
            <tbody>
            <tr><td><?php echo $projectChargesReportData[0]->ProjectTitle; ?></td><td><?php echo $projectChargesReportData[0]->ProjectAbbreviation ?> </td><td><?php echo isset($Month)?$Month:''; ?></td><td>2015</td></tr>
            </tbody>
        </table>


        <table class="table table-hover table-condensed table-bordered">

            <thead>
            <tr>
                <th>Emp Code</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Monthly Salary</th>
                <th>%Charge</th>
                <th>No. of Days Worked</th>
                <th>Payable Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($projectChargesReportData) && !empty($projectChargesReportData)) {
                foreach($projectChargesReportData as $key=> $val){
                    echo "<tr><td>$val->EmployeeCode</td><td>$val->EmployeeName</td><td>$val->EmployeeDesignation</td><td>$val->MonthlySalary</td><td>$val->PercentCharged%</td><td>$val->NumberOfDays</td><td>$val->PayableAmount</td></tr>";
                }
            }
            ?>
           </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                 <!--   <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Sends</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
       $('#sendMailWithAttachment').on('click',function(){
           //alert('test'); return;
           var formData = $('#mailForm').serializeArray();
           <?php if(isset($viewData) && !empty($viewData)){?>
           formData.push({name:"viewData",value:'<?php echo json_encode($viewData);?>'});
           <?php }?> var targetURL = '<?php echo base_url();?>reports_con/emailPdfProjectChargeReport';
           $.ajax({
               url: targetURL,
               data:formData,
               type:"POST",
               success: function (output) {
                   var data = output.split("::");
                   if(data[0] === "OK"){
                       Parexons.notification(data[1],data[2]);
                   }else if(data[0] === "FAIL"){
                       Parexons.notification(data[1],data[2]);
                   }
               }
           });
       }); //End of OnClick

   });
</script>
<!--- Script for PDF file ---->
<script>
    $(document).ready(function(){
        $("#PDFButton").on('click', function(){
            //console.log('test'); return;
            var targetURL = '<?php echo base_url();?>reports_con/PdfProjectChargeReport';
            var postData = [];
            <?php if(isset($viewData) && !empty($viewData)){ ?>
            postData.push({name:"viewData",value:<?php echo json_encode($viewData); ?>});
            //console.log('test'); return;
            <?php } ?>
            console.log(postData);
            $.ajax({
                url: targetURL,
                data:postData,
                type:"POST",
                success: function (output) {
//                    var data = output.split("::");
//                    if(data[0] === "Download"){
                        //console.log(data[1]);
                        //document.location = <?php //echo base_url();?>data[1];
                    }
                });
            });

        });
</script>

<!--- Script for CSV file ---->
<script>
    $(document).ready(function(){
        $("#CSVButton").on('click', function(){
            var targetURL = '<?php echo base_url();?>human_resource/emailPdfHrReport';
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            //console.log(formData);
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });
    });
</script>
</body>
</html>
