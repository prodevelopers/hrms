<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/22/2015
 * Time: 11:58 AM
 */

?>
<!-- contents -->
<style type="text/css">
    .select2-results li{
        width: 300px;
        border-bottom:thin solid #ccc;
        height:60px;
    }
    .select2TemplateImg { padding:0.2em 0;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
</style>
<div class="contents-container">
    <div class="bredcrumb">Dashboard / Reports / Generate Salaries Report </div> <!-- bredcrumb -->
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
     <div class="right-contents">
        <div class="head">Generate Report</div>
        <form>
            <div class="row">
                <h4>Employee Name</h4>
                <input type="hidden" id="selectEmployees">
            </div>
            <div class="row">
                <h4>Program Name</h4>
                <input type="hidden" id="selectProgram">
            </div>
            <div class="row">
                <h4>Project Name</h4>
                <input type="hidden" id="selectProject">
            </div>
            <br class="clear">

            <div class="row">
                <h4>Department</h4>
                <input type="hidden" id="selectDepartment">
            </div>
            <div class="row">
                <h4>Year</h4>
                <input type="hidden" id="selectYear">
            </div>
            <div class="row">
                <h4>Report Heading (Optional)</h4>
                <input type="text" id="reportHeading" placeholder="Heading For Report (Optional)">
            </div>
        </form>
        <br class="clear">
        <table style="width:50%" id="checkBoxSelects">
            <tr class="table-row">
                <td><input type="checkbox" name="Salaries"> Salaries</td>
                <td><input type="checkbox" name="Allowances"> Allowances</td>
                <td><input type="checkbox" name="Expenses"> Expenses</td>
                <td><input type="checkbox" name="Deductions"> Deductions</td>
                <td><input type="checkbox" name="Advances"> Advances</td>
            </tr>
        </table>
        <!-- button group -->
        <div class="row">
            <input type="button" class="btn green" value="Generate Report" id="generateReportButton">
            <input type="button" name="" id="FilterResetButton" class="btn red" value="Reset">
        </div>
    </div>
 </div>
<!--End of Contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function () {
        /**
         * Load All The Select2 Filters First..
          */
        //Loading Employees Filter.
        var selectEmployeeSelector = $('#selectEmployees');
        var url = "<?php echo base_url(); ?>payroll/loadAllAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            if (e.avatar === "defaultAvatar.jpg") {
                return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + baseURL + "uploads/Thumb_Nails/d/defaultAvatar.jpg'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>"
            } else if (e.avatar != "defaultAvatar.jpg") {
                return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + baseURL + "upload/Thumb_Nails/" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>"
            }
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
        $('.select2-container').css("width","250px");

        //Loading Projects Filter..
        var selectProjectSelector = $('#selectProject');
        var url = "<?php echo base_url(); ?>payroll/loadAllAvailableProjects";
        var id = "ProjectID";
        var text = "ProjectTitle";
        var minInputLength = 0;
        var placeholder = "Select Project";
        var multiple = false;
        commonSelect2(selectProjectSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading Program Filter.. //
        var selectProgramSelector = $('#selectProgram');
        var url = "<?php echo base_url(); ?>payroll/load_all_available_Program";
        var id = "PROGRAMID";
        var text = "ProgramName";
        var minInputLength = 0;
        var placeholder = "Select Program";
        var multiple = false;
        commonSelect2(selectProgramSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading Department Filter..
        var selectDepartmentSelector = $('#selectDepartment');
        var url = "<?php echo base_url(); ?>payroll/loadAllAvailableDepartments";
        var id = "DepartmentID";
        var text = "Department";
        var minInputLength = 0;
        var placeholder = "Select Department";
        var multiple = false;
        commonSelect2(selectDepartmentSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading Department Filter..
        var selectYearSelector = $('#selectYear');
        var url = "<?php echo base_url(); ?>payroll/loadAllAvailableYears";
        var id = "YearID";
        var text = "YearTitle";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
        commonSelect2(selectYearSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //We Need To Do Some Changes, As We Need Only 1 Filter to Work So we will disable Other filters on change of 1 filter.
        selectEmployeeSelector.on('change', function (e) {
            selectProjectSelector.select2('disable');
            selectProgramSelector.select2('disable');
            selectDepartmentSelector.select2('disable');
        });
        selectProgramSelector.on('change', function (e) {
            selectProjectSelector.select2('disable');
            selectEmployeeSelector.select2('disable');
            selectDepartmentSelector.select2('disable');
        });
        selectProjectSelector.on('change', function (e) {
            selectEmployeeSelector.select2('disable');
            selectProgramSelector.select2('disable');
            selectDepartmentSelector.select2('disable');
        });
        selectDepartmentSelector.on('change', function (e) {
            selectEmployeeSelector.select2('disable');
            selectProgramSelector.select2('disable');
            selectProjectSelector.select2('disable');
        });


        /***  Finally End Of All The Filters..***/

        $('#generateReportButton').on('click',function(e){
            //If Filters Are Selected Do The Filtering..
                //First Lets Work On Employee Filter..
            var employeeID = selectEmployeeSelector.val();

            //Program Filter
            var PROGRAMID = selectProgramSelector.val();

            //Project Filter
            var projectID = selectProjectSelector.val();

            //Department Filter
            var departmentID = selectDepartmentSelector.val();

            //Year Filter
            var yearID = selectYearSelector.val();

            //Set The Heading for the report.
            var reportHeading = $('#reportHeading').val();

            //Do Stuff When Generate Button Is Clicked..
            var reportTypes = new Array();
            var array = $('#checkBoxSelects input:checked');
            $.each(array, function (i, e) {
                reportTypes.push($(e).attr('name'));
            });
            if(reportTypes.length >0){
                var postURL = "<?php echo base_url(); ?>payroll/generateSalaryReport/generate";
                var postData = {
                    selectData:reportTypes
                };
                if(employeeID.length > 0){
                    postData.empID = employeeID;
                }else if(projectID.length > 0){
                    postData.proID = projectID;
                }else if(PROGRAMID.length > 0){
                   postData.ProgID = PROGRAMID;
                }else if(departmentID.length > 0){
                    postData.department = departmentID;
                }
                if(reportHeading.length > 0){
                    postData.reportHead = reportHeading;
                }
                if(yearID.length > 0){
                    var yearText = selectYearSelector.select2('data');
                    postData.selectedYear = yearText.text;
                }
/*                console.log(postData);
                return;*/
                $.redirect(postURL,postData,'POST');
                console.log(reportTypes);
            }else {
                Parexons.notification('You Must Select the Report Type From CheckBox','error');
            }
        });
        $('#FilterResetButton').on('click', function (e) {
            $('div.right-contents').find('form')[0].reset();
            $('.select2-container').select2('val', '');
            selectEmployeeSelector.select2('enable');
            selectProgramSelector.select2('enable');
            selectProjectSelector.select2('enable');
            selectDepartmentSelector.select2('enable');
        });
    });
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/reports_list_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->