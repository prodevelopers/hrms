<style>
	#pagination
	{
		float:left;
		padding:5px;
		margin-top:15px;
	}
	#pagination a
	{
		padding:5px;
		background-image:url(assets/images/top_menu_bg.png);
		color:#5D5D5E;
		font-size:14px;
		text-decoration:none;
		border-radius:5px;
		border:1px solid #CCC;
	}
	#pagination a:hover
	{
		border:1px solid #666;
	}
	/*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
	.paginate_button
	{
		padding: 6px;
		background-image: url(assets/images/top_menu_bg.png);
		color: #5D5D5E;
		font-size: 14px;
		text-decoration: none;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		border: 1px solid #CCC;
		margin: 1px;
		cursor:pointer;
	}
	.paging_full_numbers
	{
		margin-top:8px;
	}
	.dataTables_info
	{
		color:#3474D0;
		font-size:14px;
		margin:6px;
	}
	.paginate_active
	{
		padding: 6px;
		border: 1px solid #3474D0;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		color: #3474D0;
		font-weight: bold;
	}
	.dataTables_filter
	{
		float:right;
	}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		var oTable = $('#table_list').dataTable( {

			"bProcessing": true,
			"bPaginate" :true,
			"sPaginationType": "full_numbers",
			"bServerSide": true,
			"sAjaxSource": window.location+"/list",
			"bDestroy":true,
			"sServerMethod": "POST",
			"aaSorting": [[ 0, "asc" ]],
			"fnServerParams": function (aoData, fnCallBack){
				aoData.push({"name":"status_title","value":$('#status_title').val()});
				aoData.push({"name":"designation_name","value":$('#Designations').val()});


			}

		});
	});

	$(".filter1").change(function(e) {
		oTable.fnDraw();
	});

	function doc_file(){
		var r  =confirm('if you are upload this file again !\n\ please follow check out method');
		if(r == true){
			return true;
		}else if(r == false){
			return false;
		}
	}

	function confirm()
	{
		alert('Are You Sure to Delete Record ...?');
	}
</script>
<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Staff In The Year </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Staff In The Year</div>
		<table cellpadding="0" cellspacing="0">
			<thead class="head">
				<td>S.No</td>
				<td>Name</td>
				<td>Position</td>
				<td>Qualification</td>
				<td>Total Experience(Years)</td>
			</thead>
			<?php if(!empty($staff_report)){
			foreach($staff_report as $staff){



			?>
			<tbody class="table-row">
			<td><?php echo $staff->employee_code;?></td>
			<td><?php echo $staff->full_name;?></td>
			<td><?php echo $staff->designation_name;?></td>
			<td><?php echo $staff->qualification;?></td>
			<td><?php

				$from_d=strtotime($staff->frm_d);

				$to_d=strtotime($staff->to_d);

				 $date_diff=$to_d - $from_d;

				$days=floor($date_diff/(60*60*24));
				echo " ";
				if(!empty($days))
				{
					 $months=$days/30;
					//echo " ";
					echo round($year=$months/12,PHP_ROUND_HALF_UP);


				}

			//echo $staff->$year;?></td>
			<td><?php //echo $staff->to_date?></td>

			</tbody>
			<?php }}else{?>
				<td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
			<?php }?>
		</table>

		<div id="pagination">
			<ul>
				<?php echo $links; ?>
			</ul>
		</div>
            
		</div>
	</div>
</div>
	<!-- Menu left side  -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
 	</div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
  	</script>
  	<!-- leftside menu end -->
<!-- contents -->