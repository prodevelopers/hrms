<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link href="<?php echo base_url()?>assets/img/favicon-img.png" rel="shortcut icon" />
    <title>HR Management System</title>
    <base href="<?php echo base_url(); ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/component.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
    <link rel="stylesheet" type="text/css" href="assets/select2/select2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/form.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/data_picker/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/data_picker/jquery-ui.js"></script>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>
    <script src="<?php echo base_url();?>assets/js/dialog.js"></script>
</head>
<body>
<style type="text/css">
    .dropdown-menu{
        position: relative;
        left: 70%;
    }
    .margino{
        padding-left: 2em;
        font-family: "Myriad Pro";
    }
    .margino .header {
        width: 200px;
        background: #4D6684;
        color: #fff;
        border:none;
    }
    .marginol{
        position: relative;
        left: 600px;
    }
    .margino ul{
        float: left;
    }
    .margino .menu li{
        float: left;
        width: 200px;
        background: #fff;
        border:none;
    }
    .margino .menu h4{
        font-size: 14px;
        float: left;
    }
    .margino .menu small{
        float: right;
        padding: 0.2em 0;
    }
    .margino .menu a{
        padding: 0.2em 0.5em;
        background: green;
        color: #fff;
    }
    .margino .menu a:hover{
        color: #f8f8f8;
    }
    .margino .menu p{
        float: left;
        width: 100%;
        padding: 0.2em;
        font-size: 13px;
        font-weight: bold;;
    }
    .margino .menu p:hover{
        background: #f8f8f8;
    }
    .margino ul ul{
        position: relative;
        left: -2px;
        top: 27px;
    }
    .margino ul ul li {
        float: left;
    }
    .margino h3 {
        font-size: 12px;
        padding: 0.5em;
    }

    .ol{
        padding: 0;
        position: absolute;
        left: 0;
        /*width: 175px;*/
        height: 50px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        display: none;
        opacity: 0;
        visibility: hidden;
        -webkit-transiton: opacity 0.2s;
        -moz-transition: opacity 0.2s;
        -ms-transition: opacity 0.2s;
        -o-transition: opacity 0.2s;
        -transition: opacity 0.2s;
    }
    /*ul li ol {

    }*/
    .li {
        background: #333;
        display: block;
        /*width: 175px;*/
        color: #fff;
        font-size: 16px;
        float: left;
    }
    .li a{
        font-size: 16px;
    }

    .ol.li:hover{
        /*width: 180px;*/
        padding: 0;
        margin: 0;
        -webkit-transition:none;
        -o-transition:none;
        transition:none;
    }
    ul li:hover .ol {
        display: block;
        opacity: 1;
        visibility: visible;
        position: relative;
    }
    .li h4 {
        padding: 0;
        position: absolute;
        opacity: 0;
        left: 0;
        display: none;
    }
    .li:hover h4{
        display: block;
        font-weight: normal;
        background: #333;
        opacity: 1;
        visibility: visible;
        position: relative;
        padding: 0;
    }
</style>




<?php
$loggedInEmployee = $this->session->userdata('employee_id');
if(isset($monthYear) && !empty($monthYear)){
    $Year = $monthYear[1];
    $Month = $monthYear[0];
    $MonthNumber =  date('m', strtotime($Month));
    $date = $Year.'-'.$MonthNumber.'-01';
    $end  = $Year.'-'.$MonthNumber.'-' . date('t', strtotime($date));
}else{
    $date = date('Y-m-01');
    $end = date('Y-m-') . date('t', strtotime($date));
}
$monthYear = explode('-',$date);
?>
<?php
$projectData = array(
    'hoursWorkedInProject' => array()
);
$timeSheetData = array(
    'Date' => array(),
    'Day' => array()
);
if(isset($TimeSheetViewData) && !empty($TimeSheetViewData)){
    $totalProjectsInMonth = array_count_values(array_column(json_decode(json_encode($TimeSheetViewData), true), 'ProjectID'));
    while (strtotime($date) <= strtotime($end)) {
        $day_num = date('d', strtotime($date));
        $day_name = date('l', strtotime($date));
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        foreach ($TimeSheetViewData as $key => $value) {
            if (date('d', strtotime($value->WorkDate)) === $day_num) {
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = $value->WorkHours;
                if(!array_key_exists('Total',$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] = 00;
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] += intval($value->WorkHours);
            }else{
//            echo '<br />Date Not Matching'.$day_num;
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                if(isset($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    if(!array_key_exists($monthYear[0].'-'.$monthYear[1].'-'.$day_num,$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                        $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = '00';
                    }
                }
            }
            ksort($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]);
        }
        array_push($timeSheetData['Date'], $day_num);
        array_push($timeSheetData['Day'], $day_name);
    }
//Need to Add the Total Column in the End.
    $timeSheetData['Date']['Total'] = 'Total Hours Worked';
}else{
    $TimeSheetViewData = 'No Projects Assigned to You, So TimeSheet Can Not Be Enabled for You.';
}

//Now Need to Show the Employee Leaves In Time Sheet.
if(!empty($employeeTotalLeaves) && isset($employeeTotalLeaves)){
    $leaves_array = array_filter($employeeTotalLeaves, function($val){
        return (!empty($val->LeaveFrom) && !empty($val->LeaveTo));
    });
    foreach($leaves_array as $leave) {
        $range = array();
        foreach ($timeSheetData as $tsKey => $value) {
            if($tsKey != 'Day' && $tsKey != 'Date'){
                foreach($value as $vDates=>$vHours){
                    //Now Change the Index
                    try {
                        $new_date = new DateTime($vDates);
                        $from = new DateTime($leave->LeaveFrom);
                        $to = new DateTime($leave->LeaveTo);
                        if($new_date >= $from && $new_date <= $to) {
                            $range[] = $vDates . '-'.(empty($leave->HexColor) ? rand_color(): $leave->HexColor);
                            unset($timeSheetData[$tsKey][$vDates]);
                        }
                    } catch(Exception $e) {
                    }
                }
                // changes
                foreach($range as $range_date) {
                    if(!array_key_exists( $range_date , $timeSheetData[$tsKey])){
                        $timeSheetData[$tsKey][$range_date] = '00';
                    }
                }
                // resort
                ksort($timeSheetData[$tsKey]);
            }
        }
    }
}
/*echo "<pre>";
print_r($timeSheetData);
echo "</pre>";*/
//exit;
?>
<style type="text/css">
#pagination
{
    float:left;
    padding:5px;
    margin-top:15px;
}
#pagination a
{
    padding:5px;
    background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
    color:#5D5D5E;
    font-size:14px;
    text-decoration:none;
    border-radius:5px;
    border:1px solid #CCC;
}
#pagination a:hover
{
    border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
    background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
    background-color:#F0F0E1;
}
.paginate_button
{
    padding: 6px;
    background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
    color: #5D5D5E;
    font-size: 14px;
    text-decoration: none;
    border-radius: 5px;

    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    border: 1px solid #CCC;
    margin: 1px;
    cursor:pointer;
}
.paging_full_numbers"
{
    /*margin-top:8px;*/
}
.dataTables_info
{
    color:#3474D0;
    font-size:14px;

    margin:6px;
}
.paginate_active
{
    padding: 6px;
    border: 1px solid #3474D0;
    border-radius: 5px;
    -webkit-border-radius:5px;
    -moz-border-radius:5px;
    color: #3474D0;
    font-weight: bold;
}
.dataTables_filter
{
    float:right;
}

#autoSuggestionsList
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;

    display:none;
}
#autoSuggestionsList li
{
    list-style:none;
    cursor:pointer;
    padding:5px;
    font-size:14px;
    font-family:Arial;
    margin:2px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
#autoSuggestionsList li:hover
{
    border:1px solid #CCC;
}
.serch
{
    width: 200px;
    height: 25px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    color: black;
    padding-left: 5px;
    border: 1px solid #999;
}

.designing{
    top:200px;
    right:200px;
}
#employee_inf0
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;
    display:none;
    alignment-adjust:central;
}

#autoSuggestionsList
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;

    display:none;
}
#autoSuggestionsList li
{
    list-style:none;
    cursor:pointer;
    padding:5px;
    font-size:14px;
    font-family:Arial;
    margin:2px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
#autoSuggestionsList li:hover
{
    border:1px solid #CCC;
}
.serch
{
    width: 200px;
    height: 25px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    color: black;
    padding-left: 5px;
    border: 1px solid #999;
}

.designing{
    top:200px;
    right:200px;
}
#employee_inf0
{
    width: 205px;
    overflow: hidden;
    border:1px solid #CCC;
    -moz-border-radius: 5px;
    border-radius: 5px;
    display:none;
    alignment-adjust:central;
}
table {
    table-layout: fixed;
    width: 150%;
    *margin-left: -100px;/*ie7*/
}
td, th {
    vertical-align: top;
}
td.columnfirst, th.columnfirst {
    width: 100px;
    font-size:14px;
    font-weight: bold;
}
td.inner{
    font-size: 12pt;
    width: 20px;
}
.outer {position:relative;}
div.inner {
    overflow-x:scroll;
    overflow-y:visible;
}
.inputCopyCat{
    -moz-appearance: textfield;
    -webkit-appearance: textfield;
    background-color: white;
    background-color: -moz-field;
    border: 1px solid darkgray;
    box-shadow: 1px 1px 1px 0 lightgray inset;
    font: -moz-field;
    font: -webkit-small-control;
    margin-top: 5px;
    padding: 2px 3px;
    width: 398px;
}
.select2-results li{
    width: 300px;
    border-bottom:thin solid #ccc;
    height:60px;
}
.select2TemplateImg { padding:0.2em 0;}
.select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
.select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
</style>
<!--PLEASE Designer Dont Include Jquery inside View.. It Messes WIth other script Files. Jquery is already included in Head Section. No Need To Include Again.-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> -->
<script src="http://malsup.github.com/jquery.form.js"></script>
<div class="contents-container">

    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


    <div class="right-contents">
        <div class="head">Manual Timesheet</div>
<!--        DropDown For Employee Selection-->
            <div class="table-responsive">
                <table cellspacing="0" id="table_list" style="margin-left:0.7em; width:70%">
                    <thead class="table-head">
                    <td colspan="4">Employee</td>
                    </thead>
                    <tbody>
                    <tr class="table-row">
                        <td>
                            <h4>Select Employee</h4>
                        </td>
                        <td><input type="hidden" style="float: left; width: 140px;" id="selectEmployee"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
<!--        End Of Employee Selection-->
        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:75%">
                <thead class="table-head">
                <td>Filter</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <br class="clear">
        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:70%">
                <thead class="table-head">
                <td>Color Code</td>
                </thead>
                <tbody>
                <tr class="table-row">
                    <td>
                        <ul class="color-legend">
                            <li class="sun"><span>Sunday</span></li>
                            <li class="casual"><span>Casual</span></li>
                            <li class="sick"><span>Sick</span></li>
                            <li class="annual"><span>Annual</span></li>
                            <li class="holiday"><span>Holiday</span></li>
                            <li class="compen"><span>Compensatory</span></li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
    <!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
        $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
        $('#close-panel-bt').click(function() {
            $.panelslider.close();
        });
    </script>
    <!-- leftside menu end -->
</div>
<div class="right-contents" style="margin-right:1.2em;">

    <table class="table">
        <tr>
            <th>Name:</th>
            <th><?php echo $employeeData->EmployeeName; ?></th>
            <th>Timesheet Month:</th>
            <th> <?php
                if(isset($filterInfo)){
                    echo $filterInfo[0]." ".$filterInfo[1];
                }else{
                    echo date("F Y");
                }

                ?>
            </th>
            <th>Location:</th>
            <th>Swat</th>
        <tr>
    </table>

    <!--    Automated TimeSheet Code-->
    <?php
    function ifColors($dates,$key){

        $attributes = '';
        if($dates === 'Total' && $key === 'Date'){
            $attributes = "rowspan='2' style = 'width:100px;'";
        }
        if($key !== 'Date' && $key !== 'Day' && $dates !== 'Total'){
            $leaveType = explode('-',$dates);
            if(date('l', strtotime($leaveType[0].'-'.$leaveType[1].'-'.$leaveType[2])) === 'Sunday'){
                $attributes = "style='background:none repeat scroll 0 0 #f90;color:#fff;'";
            }elseif(isset($leaveType[3]) && !empty($leaveType[3])){
                $attributes = "style='background:none repeat scroll 0 0 ".$leaveType[3].";color:#fff;'";
            }
        }
        return $attributes;
    }

    if(is_array($TimeSheetViewData) && !is_string($TimeSheetViewData)){
        echo "<div class='outer'><div class='inner'><table class=\"table-responsive1\">";
        echo "<tbody style=\"width:150%\">";

        foreach ($timeSheetData as $key => $value) {
            if($key == 'Day' || $key == 'Date'){
                echo "<tr style='background: none repeat scroll 0 0 #4d6684;color:#fff;'>";
            }else{
                echo "<tr class='tableRow'>";
            }
            if(strpos($key,"::")){
                $key = explode("::",$key);
                echo "<td class=\"columnfirst\" data-project='".$key[0]."'>".$key[1]."</td>";
            }else{
                echo "<td class=\"columnfirst\">".$key."</td>";
            }
            foreach ($value as $vKey=>$valueData){
                if($vKey !== 'Total'){
                    echo "<td ". ifColors($vKey,$key) ." class=\"inner\" td-data='".$vKey."'>";
                }else{
                    echo "<td ". ifColors($vKey,$key) ." class=\"totalTD\" td-data='".$vKey."'>";
                }
                if($key === 'Day'){
                    print_r(substr($valueData, 0, 2));
                }else{
                    print_r($valueData);
                }
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table></div></div>";
    }else{
        echo '<div class="messageBox">'.$TimeSheetViewData.'</div>';
    }
    ?>




<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">
$(document).ready(function(e){

    //if the loggedinUser/Employee is Admin

    /*
     * New Select Employee
     * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
     * */
    var selectEmployeeSelector = $('#selectEmployee');
    var url = "<?php echo base_url(); ?>leave_attendance/loadAvailableEmployees";
    var tDataValues = {
        id: "EmployeeID",
        text: "EmployeeName",
        avatar:'EmployeeAvatar',
        employeeCode:'EmployeeCode'
    };
    var minInputLength = 0;
    var placeholder = "Select Employee";
    var baseURL = "<?php echo base_url(); ?>";
    var templateLayoutFunc = function format(e) {
        if (!e.id) return e.text;
        if (e.avatar === "defaultAvatar.jpg") {
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + baseURL + "uploads/Thumb_Nails/d/defaultAvatar.jpg'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>"
        } else if (e.avatar != "defaultAvatar.jpg") {
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + baseURL + "upload/Thumb_Nails/" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>"
        }
    };
    var templateLayout = templateLayoutFunc.toString();
    commonSelect2Templating(selectEmployeeSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
    $('.select2-container').css("width","100%");

//        Get the Selectors of the Page
    //Get Available Years First.
    //        Get the Selectors of the Page
    var selectYearSelector = $('#selectYear');
    var url = "<?php echo base_url(); ?>leave_attendance/load_availableYears_in_timeSheet";
    var id = "TimeSheetID";
    var text = "YearValue";
    var minInputLength = 0;
    var placeholder = "Select Year";
    var multiple = false;
//        commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
    commonSelect2(selectYearSelector,url,id,text,minInputLength,placeholder,multiple);
    $('.select2-container').css("width","223px");

    selectEmployeeSelector.on('change',function(e){
        selectYearSelector.select2("destroy");
        var dependentEmployeeWhere = $(this).select2('data');
        commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
        $('.select2-container').css("width","223px");
    });

    selectYearSelector.on("change", function(e){
        var dependentWhere = $(this).select2('data');
        var selectMonthSelector = $('#selectMonth');
        var url = "<?php echo base_url(); ?>leave_attendance/load_availableMonths_in_timeSheet";
        var id = "TimeSheetID";
        var text = "MonthName";
        var minInputLength = 0;
        var placeholder = "Select Month";
        var multiple = false;
        commonSelect2Depend(selectMonthSelector,url,id,text,minInputLength,placeholder,multiple,dependentWhere);
        $('.select2-container').css("width","223px");
        selectMonthSelector.on("change", function(e){
            var monthData = $(this).select2('data');
            var yearData = selectYearSelector.select2('data');

            //Creating First Arrays from Objects
            var monthDataArray = $.map(monthData, function(value, index) {
                return [value];
            });
            var yearDataArray = $.map(yearData, function(value, index) {
                return [value];
            });
//                Creating New Array to send data to controller
            var data = [];
//                Pushing Data to Data Array.
            data.push(monthDataArray[1]);
            data.push(yearDataArray[1]);
//It Was There For Admin Before..
//            console.log(data);
            data.push(selectEmployeeSelector.val());

            console.log(data);
            var redirectURL = "<?php echo base_url(); ?>reports_con/view_timesheet_reports";
            $.redirect(redirectURL,{'selectedMonthYear':data},'POST');
        });
    });
    //Now Need to Get Available Months on Based of Year
});
</script>
