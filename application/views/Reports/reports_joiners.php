<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Human Resource Reports / Joiners & Leavers Report </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	

	<div class="right-contents">

		<div class="head">Joiners & Leavers Reports
		<span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
		</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee" style="width:160px;">
				<select style="width:160px;">
					<option>Select Month</option>
				</select>
				<select style="width:160px;">
					<option>Select Year</option>
				</select>

				<select style="width:160px;">
					<option>Select By</option>
				</select>

				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
                <!--<div id="chart_div" style="width: 900px; height: 500px;"></div>-->
			</div>
			<br class="clear">
			<hr>
        <div id="chart_div" style="width: 900px; height: 500px;"></div>
	<!-- tabuler data -->
        <div id="tablebar">
		<table class="table" cellspacing="0">
			<thead class="table-head">
				<td>Months</td>
				<td>No Of Absenties</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td>January</td>
					<td>5</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!--<div style="width: 50%; margin:20px auto;">
			
	</div>-->
		<!-- Menu left side  -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
 	</div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
  	</script>
    <script>
    	$(document).ready(function(){
		$('#tablebar').show();
		$('#chart_div').hide();
		$('#table').click(function(){
			$('#tablebar').show();
			$('#chart_div').hide();
		});
		$('#chart').click(function(){
			$('#chart_div').show();
			$('#tablebar').hide();
		});
	});
    </script>
  	<!-- leftside menu end -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Year', 'Joinners', 'Leavers'],
	<?php foreach($name as $nam){
		//echo "<br>".$nam->year; die;
		//$join = explode('-',$nam->joining_date);
		//$retir = explode('-',$nam->date);
		//print_r($join[0]);die;
		?>
    ['<?php echo $nam->year;?>',  <?php echo $nam->joiner;?>,<?php echo $nam->rtire;?>],<?php }?>
   /* ['2005',  1170,      460],
    ['2006',  660,       1120],
    ['2007',  1030,      540]*/
  ]);

  var options = {
    title: 'Company Performance',
    hAxis: {title: 'Year Wise Graph', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

  chart.draw(data, options);

}
    </script>
		</div>
	</div>
</div>

<!-- contents -->