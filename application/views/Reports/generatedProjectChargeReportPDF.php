<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
    @media print {
        #printButton {
            display: none;
        }
        a:link:after, a:visited:after {
            content: "" !important;
            display: none !important;
            background: black;
        }
    }
    body{
        background: #f8f8f8;
    }
    table th{
        font-size: 13px;
        font-weight:bold;
    }
    table td{
        font-size: 13px;
    }
    .report_msg{
        padding: 2em 1em;
        font-size: 18px;
        font-weight: bold;
        color: darkred;
        width:30%;
        margin:5em 12em;
        border: 1px solid #000066;
    }
</style>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">

        <h4 class="text-center"><?php echo (isset($reportHeading) && !empty($reportHeading))? $reportHeading:'Project Charge Report' ?></h4>

        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>Project Name</th>
                <th>Project Abbreviation</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            </thead>
            <tbody>
            <tr><td><?php echo $projectChargesReportData[0]->ProjectTitle; ?></td><td><?php echo $projectChargesReportData[0]->ProjectAbbreviation ?> </td><td><?php echo isset($Month)?$Month:''; ?></td><td>2015</td></tr>
            </tbody>
        </table>


        <table class="table table-hover table-condensed table-bordered">

            <thead>
            <tr>
                <th>Emp Code</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Monthly Salary</th>
                <th>%Charge</th>
                <th>No. of Days Worked</th>
                <th>Payable Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($projectChargesReportData) && !empty($projectChargesReportData)) {
                foreach($projectChargesReportData as $key=> $val){
                    echo "<tr><td>$val->EmployeeCode</td><td>$val->EmployeeName</td><td>$val->EmployeeDesignation</td><td>$val->MonthlySalary</td><td>$val->PercentCharged%</td><td>$val->NumberOfDays</td><td>$val->PayableAmount</td></tr>";
                }
            }
            ?>
           </tbody>
        </table>
    </div>
</div>
