<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
 <style>
 thead  th {
    font-size:15px;
 }
tbody td{
        font-size:14px;
    }
     h4 span.subHead{
         font-size: 10pt;
         display: block;
     }
 </style>   
 <div class="container-fluid">
    <div class="col-lg-8">
        <button type="button" style="margin-top:0.5em;" id="sendEmailButton" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#myModal">
            Send
        </button>
        <h4 class="text-center">Leave Attendance<span class="subHead">Customized Report</span></h4>
        <table class="table table-hover table-condensed table-bordered">
            <thead>
            <tr>
                <th>Payments/Month</th>
                <th>Total Leaves</th>
                <th>Total Absentees</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($bigResultArray) && is_array($bigResultArray)){
                foreach($bigResultArray as $key => $val){
                    echo "<tr><td>$key</td>";
                    echo isset($val['TotalLeaves'])?"<td>".$val['TotalLeaves']."</td>":"<td></td>";
                    echo isset($val['TotalAbsentees'])?"<td>".$val['TotalAbsentees']."</td>":"<td></td>";
                    echo "<tr>";
                }
            }
            ?>
            </tbody>
        </table>
            </div>
    </div>   

    <!-- Modal -->
<div hidden class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Reply To:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Attachment:</label>
                        <div class="col-sm-9">
                            <label  class="col-sm-2 control-label">work.pdf</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>

<!--//Scripts For Notifications..-->
<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script>
    $(document).ready(function(e){
        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>reports_con/sendGeneratedPDFReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax
        }); //End Of Button Clicked Function
    });
</script>

