<?php if(!empty($info)){
	$jan_amount=0;	$feb_amount=0;	$mar_amount=0;	$aprl_amount=0;
	$may_amount=0;	$jun_amount=0;	$july_amount=0;	$aug_amount=0;
	$sep_amount=0;	$oct_amount=0;	$nov_amount=0;	$dec_amount=0;

	foreach($info as $rec)
	{
		if($rec->month == 1){$amt=$rec->transaction_amount;$jan_amount+=$amt;}
		if($rec->month == 2){$amt=$rec->transaction_amount;$feb_amount+=$amt;}
		if($rec->month == 3){$amt=$rec->transaction_amount;$mar_amount+=$amt;}
		if($rec->month == 4){$amt=$rec->transaction_amount;$aprl_amount+=$amt;}
		if($rec->month == 5){$amt=$rec->transaction_amount;$may_amount+=$amt;}
		if($rec->month == 6){$amt=$rec->transaction_amount;$jun_amount+=$amt;}
		if($rec->month == 7){$amt=$rec->transaction_amount;$july_amount+=$amt;}
		if($rec->month == 8){$amt=$rec->transaction_amount;$aug_amount+=$amt;}
		if($rec->month == 9){$amt=$rec->transaction_amount;$sep_amount+=$amt;}
		if($rec->month == 10){$amt=$rec->transaction_amount;$oct_amount+=$amt;}
		if($rec->month == 11){$amt=$rec->transaction_amount;$nov_amount+=$amt;}
		if($rec->month == 12){$amt=$rec->transaction_amount;$dec_amount+=$amt;}


	}
	$jan_amount;	$feb_amount;	$mar_amount;	$aprl_amount;
	$may_amount;	$jun_amount;	$july_amount;	$aug_amount;
	$sep_amount;	$oct_amount;	$nov_amount;	$dec_amount;
}else{ echo "No Records Found !";}
?>
<!-- contents -->

<script src="assets/chart-plugin/Chart.js"></script>
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Reports / Pay Roll Reports / Salary Report </div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	
	<div class="right-contents">

		<div class="head">Salary Reports
		<span class="iconion">
			<a id="chart"><span class="fa fa-area-chart fa-lg"> </span>&nbsp;</a>
			<a id="table"><span class="fa fa-table fa-lg"></span></a>
			</span>
		</div>
		<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" name="" placeholder="Employee Name">
				<select>
					<option>Month</option>
				</select>
				<select>
					<option>Leave Type</option>
				</select>
				<div class="button-group">
					<button class="btn green">Show</button>
				</div>
			</div>
			
			<br class="clear">
			<hr>
	<div style="width: 50%; margin:20px auto;" id="chartbar">
			<canvas id="canvas" height="450" width="700"></canvas>
	</div>
	<!-- tabuler data -->
	<div id="tablebar">
		<table class="table" cellspacing="0">
			<thead class="table-head">
				<td>Months</td>
				<td>No Of Absenties</td>
			</thead>
			<tbody>
				<tr class="table-row">
					<td>January</td>
					<td>5</td>
				</tr>
			</tbody>
		</table>
	</div>
    <script src="<?php echo base_url() ?>assets/js/reports.js"></script>
		<!-- Menu left side  -->
	<div id="right-panel" class="panel">
		<?php $this->load->view('includes/reports_list_nav'); ?>
 	</div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
  	</script>
  	<!-- leftside menu end -->
	<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["January","February","March","April","May","June","July"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [
					<?php echo @$jan_amount;?>,
					<?php echo @$feb_amount;?>,
					<?php echo @$mar_amount;?>,
					<?php echo @$aprl_amount;?>,
					<?php echo @$may_amount;?>,
					<?php echo @$jun_amount;?>,
					<?php echo @$july_amount;?>,
					<?php echo @$aug_amount;?>,
					<?php echo @$sep_amount;?>,
					<?php echo @$oct_amount;?>,
					<?php echo @$nov_amount;?>,
					<?php echo @$dec_amount;?>
				]
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>
		</div>
	</div>
</div>
<!-- contents -->