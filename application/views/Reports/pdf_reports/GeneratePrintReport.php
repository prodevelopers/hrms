<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 1/27/2015
 * Time: 3:02 PM
 */
if(isset($SelectedCheckBoxes) and isset($ReportTableData)){
    if(strpos($SelectedCheckBoxes,'Salaries') !== false){
        $salaries = true;
    }else{
        $salaries = false;
    }
    if(strpos($SelectedCheckBoxes,'Allowances') !== false){
        $allowances = true;
    }else{
        $allowances = false;
    }
    if(strpos($SelectedCheckBoxes,'Expenses') !== false){
        $expenses = true;
    }else{
        $expenses = false;
    }
    if(strpos($SelectedCheckBoxes,'Deductions') !== false){
        $deductions = true;
    }else{
        $deductions = false;
    }
}
?>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">
        <h4 class="text-center"><?php echo isset($reportHeading)?$reportHeading:'Payroll Report'; ?></h4>
        <?php
        //This Section Will Execute If Employee Filter is Selected
        if (isset($employeeInfo) && !empty($employeeInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>E.Name</th>
                    <th>E.Code</th>
                    <th>Designation</th>
                    <th>Department</th>
                    <th>Year</th>
                </tr>
                <?php
                echo "<tr><td>" . $employeeInfo->EmployeeName . "</td><td>" . $employeeInfo->EmployeeCode . "</td><td>" . $employeeInfo->Designation . "</td><td>" . $employeeInfo->Department . "</td><td>" . $filteredYear . "</td></tr>";
                ?>
            </table>
        <?php } ?>

        <?php
        //This Section will execute if the Project filter is selected.
        if (isset($projectInfo) && !empty($projectInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>Project Name</th>
                    <th>Project Abbreviation</th>
                    <th>Year</th>
                </tr>
                <?php
                echo "<tr><td>" . $projectInfo->projectTitle . "</td><td>" . $projectInfo->ProjectAbbreviation . "</td><td>" . $filteredYear . "</td></tr>";
                ?>
            </table>
        <?php } ?>

        <?php
        //This Section will execute if the department filter is selected.
        if (isset($departmentInfo) && !empty($departmentInfo)) {
            ?>
            <table class="table table-condensed">
                <tr class="infor">
                    <th>Department</th>
                    <th>Year</th>
                </tr>
                <?php
                echo "<tr><td>" . $departmentInfo->DepartmentName . "</td><td>" . $filteredYear . "</td></tr>";
                ?>
            </table>
        <?php } ?>

        <table class="table table-hover table-condensed table-bordered">

            <thead>
            <tr>
                <th>Payments/Month</th>
                <?php echo $salaries? "<th>Salaries</th>":'' ?>
                <?php echo $allowances? "<th>Allowances</th>":'' ?>
                <?php echo $expenses? "<th>Expenses</th>":'' ?>
                <?php echo $deductions? "<th>Deductions</th>":'' ?>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($ReportTableData) && is_array($ReportTableData)){
                foreach($ReportTableData as $key=>$val){
                    echo "<tr><td>".$key."</td>";
                    echo $salaries? "<td>".(isset($val['Salaries'])?$val['Salaries']:'-')."</td>":'';
                    echo $allowances? "<td>".(isset($val['Allowances'])?$val['Allowances']:'-')."</td>":'';
                    echo $expenses? "<td>".(isset($val['Expenses'])?$val['Expenses']:'-')."</td>":'';
                    echo $deductions? "<td>".(isset($val['Deductions'])?$val['Deductions']:'-')."</td>":'';
                    //Lets Show The Row Total At The End..
                    //echo "<td>Total Here</td>";
                    echo "<td>";
                    $total = 0;
                    if($salaries){
                        $salariesRowTotal = isset($val['Salaries'])?$val['Salaries']:0;
                        $total += $salariesRowTotal;
                    }
                    if($allowances){
                        $allowancesRowTotal = isset($val['Allowances'])?$val['Allowances']:0;
                        $total += $allowancesRowTotal;
                    }
                    if($expenses){
                        $expensesRowTotal = isset($val['Expenses'])?$val['Expenses']:0;
                        $total += $expensesRowTotal;
                    }
                    if($deductions){
                        $deductionsRowTotal = isset($val['Deductions'])?$val['Deductions']:0;
                        $total += $deductionsRowTotal;
                    }
                    echo $total;
                    echo "</td></tr>";
                }
                echo "<tr><td><b>Total</b></td>";
                $superTotal = 0;
                if($salaries){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Salaries'])?$item['Salaries']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($allowances){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Allowances'])?$item['Allowances']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($expenses){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Expenses'])?$item['Expenses']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                if($deductions){
                    $sum = 0;
                    foreach ($ReportTableData as $item) {
                        $sum += isset($item['Deductions'])?$item['Deductions']:0;
                    }
                    echo "<td>".$sum."</td>";
                    $superTotal += $sum;
                }
                echo "<td><b>GrandTotal : ".$superTotal."</b></td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>