<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/7/2015
 * Time: 4:39 PM
 */
if(isset($SelectedCheckBoxes) and isset($ReportTableData)){
    if(strpos($SelectedCheckBoxes,'Salaries') !== false){
        $salaries = true;
    }else{
        $salaries = false;
    }
    if(strpos($SelectedCheckBoxes,'Allowances') !== false){
        $allowances = true;
    }else{
        $allowances = false;
    }
    if(strpos($SelectedCheckBoxes,'Expenses') !== false){
        $expenses = true;
    }else{
        $expenses = false;
    }
    if(strpos($SelectedCheckBoxes,'Deductions') !== false){
        $deductions = true;
    }else{
        $deductions = false;
    }
}
?>
<!doctype HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Employee Wise Payroll Report</title>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css"/>
<style>
    thead  th {
        font-size:15px;
    }
    .infor th{
        font-size: 14px;
    }
    tbody td{
        font-size:14px;
    }
    .detail td{
        font-size: 13px;
    }
</style>
</head>
<body>
<div class="container-fluid">
    <br class="clearfix"/>
    <div class="col-lg-8">

        <h4 class="text-center"><?php echo (isset($reportHeading) && !empty($reportHeading))?$reportHeading:'EmployeesWise Payroll Report'; ?></h4>

        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>EmployeeName</th>
                <th>Employee Code</th>
                <th>Designation</th>
                <th>Monthly Salary</th>
                <th>Salary Month and Year</th>
            </tr>
            </thead>
            <tbody>
        <?php
        //This Section Will Execute If Employee Filter is Selected

        if (isset($EmployeeInfo) && !empty($EmployeeInfo)) {
            ?>
                <?php
                echo "<tr><td>" . $EmployeeInfo->EmployeeName . "</td><td>" . $EmployeeInfo->EmployeeCode . "</td><td>" . $EmployeeInfo->Designation . "</td><td>" . ((isset($projectChargesReportData) && !empty($projectChargesReportData[0]->MonthlySalary))?$projectChargesReportData[0]->MonthlySalary:''). "</td><td>" .$Month.' - '.$filteredYear."</td></tr>";
                ?>
        <?php } ?>
            </tbody>
        </table>
        <table class="table table-condensed">
            <thead>
            <tr class="infor">
                <th>Project Name/Abbreviation</th>
                <th>Days Worked</th>
                <th>% Charges</th>
                <th>Payable Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //List The Projects For The Selected Employees IF Exist Any..
            if (isset($projectChargesReportData) && !empty($projectChargesReportData)) {
                foreach ($projectChargesReportData as $projectKey => $projectVal) {
                    echo "<tr><td>" . $projectVal->ProjectTitle . "(" . $projectVal->ProjectAbbreviation . ")</td><td>" . $projectVal->NumberOfDays . "</td><td>" .$projectVal->PercentCharged . "% </td><td>" . $projectVal->PayableAmount . "</td></tr>";
                }
            }
            ?>
            </tbody>
        </table> <!--End Of Employee Projects Payroll Details-->
    </div>
</div>



<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
</body>
</html>