<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/22/2015
 * Time: 11:58 AM
 */

$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->
<style type="text/css">
   .select2-results li{
        width: 300px;
        border-bottom:thin solid #ccc;
        height:20px;
    }
   /*  .select2TemplateImg { padding:0.2em 0;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}*/
    .select2TemplateImg p{ padding:0.2em 1em; font-size:8pt;}
   .select2-results .select2-result-label{font-size:10pt;}
</style>
<div class="contents-container">
    <div class="bredcrumb">Dashboard / Reports / Generate Project Charge Report </div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Generate Project Charge Report</div>
        <form>
            <div class="row">
                <h4>Program Name</h4>
                <input type="hidden" id="selectProgram">
            </div>
            <br class="clear">
            <div class="row">
                <h4>Project Name</h4>
                <input type="hidden" id="selectProject">
            </div>
            <br class="clear">

            <div class="row">
                <h4>Designation</h4>
                <input type="hidden" id="selectDesignation">
            </div>
            <div class="row">
                <h4>Report Heading</h4>
                <input type="text" id="reportHeading" placeholder="Heading For Report (Optional)">
            </div>
            <div class="row">
                <h4>Month</h4>
                <input type="hidden" id="selectMonth">
            </div>
            <div class="row">
                <h4>Year</h4>
                <input type="hidden" id="selectYear">
            </div>
        </form>
        <!-- button group -->
        <div class="row">
            <input type="button" class="btn green" value="Generate Report" id="generateReportButton">
            <input type="button" class="btn red" value="Clear" id="clearFormButton">
        </div>
    </div>
</div>
<!--End of Contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function () {

        //Loading Projects Filter..
        var selectProjectSelector = $('#selectProject');
        var url = "<?php echo base_url(); ?>payroll/loadAllAvailableProjects";
        var id = "ProjectID";
        var text = "ProjectTitle";
        var minInputLength = 0;
        var placeholder = "Select Project";
        var multiple = false;
        commonSelect2(selectProjectSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

          //Loading Program Filter Using Ajax..
        var selectProgramSelector = $('#selectProgram');
        var url = "<?php echo base_url(); ?>payroll/load_all_available_Program";
        var id = "PROGRAMID";
        var text = "ProgramName";
        var minInputLength = 0;
        var placeholder = "Select Program";
        var multiple = false;
        commonSelect2(selectProgramSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading Department Filter..
        var selectDesignationSelector = $('#selectDesignation');
        var url = "<?php echo base_url(); ?>reports_con/loadAllAvailableDesignations";
        var id = "DesignationID";
        var text = "Designation";
        var minInputLength = 0;
        var placeholder = "Select Designation";
        var multiple = false;
        commonSelect2(selectDesignationSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading All Available Years Filter..
        var selectYearSelector = $('#selectYear');
        var url = "<?php echo base_url(); ?>reports_con/loadAllAvailableYears";
        var id = "YearID";
        var text = "YearTitle";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
        commonSelect2(selectYearSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");

        //Loading Available Months Filter..
        var selectMonthSelector = $('#selectMonth');
        var url = "<?php echo base_url(); ?>reports_con/loadAllAvailableMonths";
        var id = "MonthID";
        var text = "MonthTitle";
        var minInputLength = 0;
        var placeholder = "Select Month";
        var multiple = false;
        commonSelect2(selectMonthSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width","250px");


        /***  Finally End Of All The Filters..***/
        $('#generateReportButton').on('click',function(e){
            //If Filters Are Selected Do The Filtering..
            //Project Filter
            var projectID = selectProjectSelector.val();

             //Program Filter
            var programID = selectProgramSelector.val();

            //Department Filter
            var DesignationID = selectDesignationSelector.val();

            //Year Filter
            var yearID = selectYearSelector.val();
            var monthID = selectMonthSelector.val();

            //Set The Heading for the report.
            var reportHeading = $('#reportHeading').val();

            //Do Stuff When Generate Button Is Clicked..

            //Don't Post If Project Or Month Have Not Been Selected
            if(projectID.length === 0){
                Parexons.notification('Please Select Project From Drop Down','warning');
                return false;
            }
            if(monthID.length === 0){
                Parexons.notification('Please Select Month From Drop Down','warning');
                return false;
            }

                var postURL = "<?php echo base_url(); ?>reports_con/projectChargeReport/generatedReport";
                var postData = {
                    ProjectID : projectID,
                    selectedMonthID : monthID
                };
                //Add Designation Filter If Selected Designation
                if(DesignationID.length > 0){
                    postData.designation = DesignationID;
                }

                //Add Program Filter If Selected Program
                if(programID.length > 0){
                    postData.Program = programID;
                }
                //Add Report Heading If Not Empty Text Box
                if(reportHeading.length > 0){
                    postData.reportHead = reportHeading;
                }
                //Add Year Filter If Not Selected Year.
                if(yearID.length > 0){
                    var yearText = selectYearSelector.select2('data');
                    postData.selectedYear = yearText.text;
                }
                $.redirect(postURL,postData,'POST');
        });

        //Need A Clear Button To Clear The Form When Pressed..
        $('#clearFormButton').on('click', function (e) {
            $('div.right-contents').find('form')[0].reset();
            $('.select2-container').select2('val', '');
        });

        //Page Messages

        //Page Messages.
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>
<!-- Menu left side -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/reports_list_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- leftside menu end -->