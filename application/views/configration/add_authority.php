<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

</style>
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"year","value":$('#year').val()});
            aoData.push({"name":"month","value":$('#month').val()});    
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}


function lookup(inputString) {
	
    if(inputString.length == 0) {
        $('#suggestions').hide();
		 $('#employee_inf0').hide();
	       location.reload();
    } else {
        $.post("config_site/autocomplete_approval_authority/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,designation_name,employee_id) {
    $('#id_input').val(thisValue);
	//$('#name').append(full_name);
	$('#desg').val(designation_name);
	$('#emp_id').val(employee_id);
  /* $('#approval_types').append(approval_types);
	
	 $('#todate').append(from_date)
	$('#fromdate').append(to_date);*/
	setTimeout("$('#suggestions').hide();", 200);
      //$('#suggestions').hide();
   <!--$('#employee_inf0').show();-->
 
	//setTimeout("$('#autoSuggestionsList').hide()", 200);
}   



</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Configuration / Add Authority</div> <!-- bredcrumb -->


 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Add Authority</div>

			

			<form action="config_site/add_authority" method="post">
				<div class="row">
					<h4>Employee Name</h4>
					<input type="text" name="name" id="id_input" class="serch" autocomplete="off" onkeyup="lookup(this.value)" required="required">
                    <input type="hidden" name="employee_id"  id="emp_id" />
				</div>
                  <div id="suggestions">
                        <div class="autoSuggestionsList_l" id="autoSuggestionsList">
                        </div>
                    </div>
				<br class="clear">
				<div class="row">
					<h4>Designation</h4>
                    <input type="text" name="designation_name" id="desg" readonly="readonly" />
					<?php //echo @form_dropdown('designation_name',$Designations,$designation_name);?>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Authorize for</h4>
					 <?php echo @form_multiselect('approval_types[]',$approval,$approval_types,'required="required"','required="required"');?>
				
                </div><br class="clear"><div class="row"><h4></h4><p style="font-size:11px; color:#900; ">Hold down the Ctrl button to select multiple options.</p></div>
				<!--<br class="clear">
				 <div class="row">
					<h4>Level</h4>
					<select>
						<option></option>
					</select>
				</div> -->
				<br class="clear">
				<div class="row">
					<h4>Status</h4>
					<select name="enabled">
						<option value="1">Enable</option>
						<option value="0">Disable</option>
					</select>
				</div>
			

			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add" class="btn green" value="Add Authority" />
					<!-- <a href="#"><button class="btn green">Add Authorigy</button></a>
					<button class="btn red">Delete</button> -->
				</div>
			</div>
</form>
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
      	<?php $this->load->view('includes/config_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->