<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	left: -22.4%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"approval_types","value":$('#approval_types').val()});
			aoData.push({"name":"designation_name","value":$('#Designations').val()});
					        
                
		}
                
	});

    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });


});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script><!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard / Approvals</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

<script>
 var navigation = responsiveNav(".nav-collapse");
</script>
	<div class="right-contents">

		<div class="head">Approvals</div>
        <?php echo form_open('');?>
        <input type="text" id="tableSearchBox" placeholder="Search Here" style="width: 30%;">
        	<?php echo @form_dropdown('approval_types',$approval,$approval_types,"class='resize' id='approval_types' onchange='this.form.submit()'"); ?>
       		<?php echo @form_dropdown('designation_name',$Designations,$designation_name,"class='resize' id='Designations' onchange='this.form.submit()'"); ?>
        
        <?php echo form_close();?>	
			  <table cellspacing="0" id="table_list" class="marge">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Authorise for</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-trash-o"></span></td>
				</thead>
			</table> 
            <div class="row">
					<a href="config_site/add_authority"><button class="btn green">Add Authority</button></a>
			</div> 	
	   </div>


	</div>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
	$( "#accordion1" ).accordion();
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
      	<?php $this->load->view('includes/config_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#approval_types, #Designations').select2();
    });
    </script>
    <!-- leftside menu end -->
