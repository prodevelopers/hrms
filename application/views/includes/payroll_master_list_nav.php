<div class="left-nav">
				<ul>
                    <li><a href="ml_payrol_site/view_allowance_type">Allowance Types</a></li>
                    <li><a href="ml_payrol_site/view_currency">Currency</a></li>
                    <li><a href="ml_payrol_site/view_deduction_head">Deduction Heads</a></li>
                    <li><a href="ml_payrol_site/view_expense">Expenses</a></li>
                    <li><a href="ml_payrol_site/view_increment_type">Increment Types</a></li>
					<li><a href="ml_payrol_site/view_payroll_ml">Paygrade</a></li>
					<li><a href="ml_payrol_site/view_pay_frequecy">Pay Frequency</a></li>
                    <li><a href="ml_payrol_site/view_payment_mode_type">Payment Mode Type</a></li>
                    <li><a href="ml_payrol_site/view_payment_type">Payment Type</a></li>
					<li><a href="ml_payrol_site/view_pay_rate">Payrate</a></li>
					<li><a href="ml_payrol_site/view_transaction">Transaction Types</a></li>
					<li><a href="ml_payrol_site/view_tax_slabs">Tax Slabs</a></li>

					<li><a href="ml_payrol_site/AddAjustmentHead">Adjustment Head</a></li>
					<li><a href="ml_payrol_site/AddAjustmentType">Adjustment Type</a></li>
				</ul>
			</div>