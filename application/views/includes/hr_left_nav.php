
<div class="left-nav nav-collapse">
		<ul style="width:220px;">
			<li><a href="human_resource/all_employee_list">Personnel Management</a></li>
			<li><a href="human_resource/view_job_assignment">Task Management</a></li>
			<li><a href="human_resource/performance_evaluation">Performance Evaluation</a></li>
			<li><a href="human_resource/view_position_management">Employment Management<i class="fa fa-caret-right menuorgan"></i></a>
                <ul style="margin-left:5px;margin-top:-277px;">
                    <li><a href="human_resource/view_position_management">Position Management</a></li>
                    <li><a href="human_resource/view_projects_management">Projects Allocation</a></li>
                    <li><a href="human_resource/view_employee_transfer">Employee Transfer</a></li>
                    <li><a href="human_resource/view_employee_extensions">Extensions</a></li>
                    <li><a href="human_resource/view_employee_rejoin">Re-Joining </a></li>
                    <li><a href="human_resource/view_employment_history">Employment History</a></li>
                </ul>
            </li>
			<li><a href="human_resource/view_retirement">Retirement/Separation<i class="fa fa-caret-right menuorgan"></i></a>
             <ul  style="margin-left:5px;margin-top:-277px;">
	            <li><a href="human_resource/view_retirement">Current Employees</a></li>
	             <li><a href="human_resource/retired_employees">Retired/Separated List</a></li>
            </ul>
            </li>
			<!--<li><a href="human_resource/benifit_management">Benefits & Entitlements</a></li>-->
            <li><a href="human_resource/annual_review">Annual Reviews</a></li>
            <li><a href="human_resource/disciplinary_action">Disciplinary Action</a></li>
            <li><a href="human_resource/all_appraisal_list">Appraisal / Review</a></li>
            <li><a href="human_resource/generate_report">Generate Report</a></li>
		</ul>
</div>
