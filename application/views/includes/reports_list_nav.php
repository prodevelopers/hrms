<div class="left-nav nav-collapse">
	<ul style="width:220px;">
		<li><a>Leave & Attendance Reports <i class="fa fa-caret-right menuorgan"></i></a>
			<ul style="top:10px; position: absolute;">
				<li><a href="reports_con/generate_employees_leave_attendance_report">Customized Report</a></li>
				<li><a href="reports_con/view_timesheet_reports">TimeSheet Report</a></li>
			</ul>
		</li>
		<li><a>Payroll Reports <i class="fa fa-caret-right menuorgan"></i></a>
			<ul style="top:65px; position: absolute;">
				<li id="kill"><a href="reports_con/generateSalaryReport">Customized Payroll Report</a></li>
				<li id="kill"><a href="reports_con/projectChargeReport">Project Charge Report</a></li>

			</ul>
		</li>
		<li><a>Human Resource Reports <i class="fa fa-caret-right menuorgan"></i></a>
			<ul style="top:100px; position: absolute;">
			<?php /* echo'	<li><a href="reports_con/view_joiner_leave_reports">Joiners And Leavers Report</a></li>
				<li><a href="reports_con/view_hr_district_reports">District Wise Report</a></li>
				<li><a href="reports_con/view_hr_skill_reports">Skills Wise Report</a></li>
				<li><a href="reports_con/view_hr_qualification_reports">Qualification Wise Report</a></li>
				<li><a href="reports_con/view_hr_experience_reports">Experience Wise Report</a></li>
				<li><a href="reports_con/view_hr_training_reports">Training Wise Report</a></li>
                 <li><a href="reports_con/view_progress_reports">Employee Progress Wsie Report</a></li>
				<li><a href="reports_con/staff">Staff Report</a></li>'*/ ?>
                <li><a href="reports_con/generate_report">Customized HR Report</a></li>
                <li id="kill"><a href="reports_con/employeeWiseHrReport">Program Wise Employees Report</a></li>
			</ul>
		</li>
       
	</ul>
</div>
<script>
	$("#kill ul").hide();
	$("#kill").hover(function() {
		$("#kill ul").show();
	}, function() {
		$("#kill ul").hide();
	});
</script>