<style type="text/css">
     .master-nav{
     float: left !important;
     width: 28%;
}
.master-nav ul{
     list-style: none;
}
.master-nav ul li a{
     background: #4D6684;
     display: block;
     font-size: 0.8em;
     line-height: 23px;
     border-bottom: solid 1px #fff;
     padding: 5px 20px;
     color: #fff;
     transition: all 0.4s;
}
.master-nav ul li a span{
     float: right;
    font-size: 1.1em;
    padding: 5px 8px;
}
.master-nav ul li a:hover{
     background: #5694BC;
     color: #fff;
}
#master{
     width:49.5%;
     float:left;
}
</style>
<div class="master-nav">
				<ul id="master">
        <!--<li><a href="hr_site/job_title">Job Title</a></li>-->
                    <li><a href="hr_site/task">Assign Task</a></li>
                    <li><a href="hr_site/branch">Branch</a></li>
                    <li><a href="hr_site/benefit_type"> Benefit Type</a></li>
                    <li><a href="hr_site/city">City/Village</a></li>
                    <li><a href="hr_site/designation">Designation</a></li>
                    <li><a href="hr_site/degree_level">Degree/Level</a></li>
                    <li><a href="hr_site/department">Department</a></li>
                    <li><a href="hr_site/district">District</a></li>
                    <li><a href="hr_site/employment_type">Employment Type</a></li>
                    <li><a href="hr_site/gender">Gender</a></li>
                    <li><a href="hr_site/insurance"> Insurance</a></li>
                    <li><a href="hr_site/job_category">Job Category</a></li>
                    <li><a href="hr_site/kpi"> KPI</a></li>
                    <li><a href="hr_site/marital"> Marital Status</a></li>
                    <li><a href="hr_site/month"> Month Master List</a></li>
                    <li><a href="hr_site/module"> Module List</a></li>
                    <li><a href="hr_site/nationality">Nationality</a></li>
                    <li><a href="hr_site/FinancialYear">Financial Year</a></li>
                </ul>
    <ul id="master" style="border-left:1px solid #fff;">
                     <li><a href="hr_site/organization_info"> Organisation Info </a></li>
                    <li><a href="hr_site/ProgramView">Program</a></li>
                    <li><a href="hr_site/project"> Project </a></li>
                    <li><a href="hr_site/province"> Province</a></li>
                    <li><a href="hr_site/posting_reason"> Posting Reason</a></li>
                    <li><a href="hr_site/posting_location"> Posting Location</a></li>
                    <li><a href="hr_site/relations">Relations</a></li>
                    <li><a href="hr_site/reporting"> Reporting </a></li>
                    <li><a href="hr_site/religion"> Religion Master List</a></li>
                    <li><a href="hr_site/skills">Skills</a></li>
                    <li><a href="hr_site/skill_levels">Skill Levels</a></li>
                    <li><a href="hr_site/shift"> Shift Time </a></li>
                    <li><a href="hr_site/separation"> Separation Type </a></li>
                    <li><a href="hr_site/status">Status Master List</a></li>
                    <li><a href="hr_site/appointment">Templates</a></li>
                    <li><a href="hr_site/user_group">User Group</a></li>
                    <li><a href="hr_site/year"> Year Master List</a></li>
                    <li><a href="hr_site/appraisal">Appraisal Terms</a></li>
		</ul>
			</div>