
<div class="left-nav">
<?php 
$photo        = @$employee->photograph; 
$employee_id  = @$employee->employee_id;
?>
		<div class="employee-name"><?php echo @$employee->full_name;?></div>
		<div class="user-pic">
        <?php if(!empty($employee->photograph)){?>
		<img src="upload/<?php echo $photo; ?>" width="100%" />
        <?php } else{?>
        <img src="upload/default.png" width="100%" />
        <?php } ?>
			<div class="upload-pic">
				Upload Photo
			</div>
		</div>
		<ul>
			<li><a href="human_resource/add_employee_info/<?php echo @$employee_id; ?>">Personal Info</a></li>
            <li><a href="human_resource/add_emp_employment/<?php echo @$employee_id; ?>">Employment Info</a></li>
            <li><a href="human_resource/add_emp_curr_contact/<?php echo @$employee_id; ?>">Contact Details</a></li>
			<li><a href="human_resource/add_emp_qualification/<?php echo @$employee_id; ?>">Qualification</a></li>
			<li><a href="human_resource/add_emp_experience/<?php echo @$employee_id; ?>">Experience</a></li>
			<li><a href="human_resource/add_emp_skill/<?php echo @$employee_id; ?>">Skills & Trainings</a></li>

			<li><a href="human_resource/add_emp_dependent/<?php echo @$employee_id; ?>">Dependents</a></li>
			<li><a href="human_resource/add_emp_pay_package/<?php echo @$employee_id; ?>">Pay Package</a></li>
			<li><a href="human_resource/add_emp_entitlement_increment/<?php echo @$employee_id; ?>">Entitlements</a></li>
			<li><a href="human_resource/add_emp_report_to/<?php echo @$employee_id; ?>">Report To</a></li>
			<li><a href="human_resource/add_emp_attachment/<?php echo @$employee_id; ?>">Attachments</a></li>
			<li><a href="human_resource/appointment/<?php echo @$employee_id; ?>">Appointment Letter</a></li>
			<li><a href="human_resource/coverLetter/<?php echo @$employee_id; ?>">Contract Letter</a></li>

		</ul>
	</div>
