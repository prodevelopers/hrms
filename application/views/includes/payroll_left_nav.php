<style>
.list_m{
	cursor:pointer;}
</style>
<script src="<?php echo base_url();?>assets/js/loading/jquery.loader.js"></script>
<link href="<?php echo base_url();?>assets/js/loading/jquery.loader.css" type="text/css" />
 <div class="left-nav nav-collapse">
		<ul style="width:220px;">
		   <li class="list_m"><a id="payroll_reg">Payroll Register</a></li>
		   <!--<li class="list_m"><a href="javascript: getsupport_trans()">Transactions</a></li>-->
           <li class="list_m"><a id="payroll_trans">Transactions</a></li>
           <li class="list_m"><a>Salary <i class="fa fa-caret-right menuorgan"></i></a>
           <ul style="top:70px; position: absolute;">
           <li class="list_m"><a id="payroll_paysal">Pay Salary</a></li>
           <li class="list_m"><a id="payroll_banktrans">Bank Transfer File</a></li>
           <li class="list_m"><a id="payroll_account">Account Management</a></li>
            </ul>
            </li>
			<li class="list_m"><a>Loans/Advance <i class="fa fa-caret-right menuorgan"></i></a>
           <ul  style="top:130px; position: absolute;">
            <li class="list_m"><a id="payroll_loanadv">Advances</a></li>
			<li class="list_m"><a id="payroll_lonadback">Advance Payback</a></li>
            </ul>
            </li>
            <li class="list_m"><a>Expense Management <i class="fa fa-caret-right menuorgan"></i></a>
				<ul  style="top:170px; position: absolute;">
					<li class="list_m"><a id="payroll_expclm">Expense Claim</a></li>
					<li class="list_m"><a id="payroll_expdis">Expense Disbursment</a></li>
				</ul>	
			</li>
			<li class="list_m"><a id="payroll_dedu">Deductions</a></li>
			<li class="list_m"><a href="<?php echo base_url();?>payroll/tax_adjustment" id="">Tax Adjustment</a></li>
			<li class="list_m"><a id="payroll_endser">End of Service</a></li>
			<li class="list_m"><a>Reports<i class="fa fa-caret-right menuorgan"></i>
            </a>
        <ul style="top:290px; position: absolute;">
		<!--<li class="list_m"><a id="payroll_report_exp">Expense Report</a></li>
		<li class="list_m"><a id="payroll_report_salary">Salary Report</a></li>
		<li id="kill"><a id="payroll_report_trans">Transactions Report</a></li>-->
		<li class="list_m"><a id="payroll_Staff_Salary">Staff Salary</a></li>
		<li class="list_m"><a id="payroll_Salary_Sheet">Payroll Sheet</a></li>
		</ul>
            </li>
            <li class="list_m"><a href="<?php echo base_url(); ?>payroll/generateSalaryReport">Generate Report</a></li>
		</ul> 
        </div>
        <script>
         $('#payroll_account').click(function(){
                className:"blue-with-image",
                 $.loader({content:getsupport_account()});
                //$.loader('setContent','');
            });

		$('#payroll_reg').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_reg()});
			//$.loader('setContent','');
			});

			$('#payroll_trans').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_trans()});
			//$.loader('setContent','');
			});
			
			$('#payroll_paybal').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_paybal()});
			//$.loader('setContent','');
			});
			
			$('#payroll_paysal').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_paysal()});
			//$.loader('setContent','');
			});
			
			$('#payroll_genslip').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_genslip()});
			//$.loader('setContent','');
			});
			
			$('#payroll_banktrans').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_banktrans()});
			//$.loader('setContent','');
			});
			
			$('#payroll_loanadv').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_loanadv()});
			//$.loader('setContent','');
			});
			
			$('#payroll_lonadback').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_lonadback()});
			//$.loader('setContent','');
			});
			
			$('#payroll_expclm').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_expclm()});
			//$.loader('setContent','');
			});
			
			$('#payroll_expdis').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_expdis()});
			//$.loader('setContent','');
			});
			
			$('#payroll_dedu').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_dedu()});
			//$.loader('setContent','');
			});
			
			$('#payroll_endser').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_endser()});
			//$.loader('setContent','');
			});
			
			$('#payroll_report_exp').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_report_exp()});
			//$.loader('setContent','');
			});
			
			$('#payroll_report_salary').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_report_salary()});
			//$.loader('setContent','');
			});
			
			$('#payroll_report_trans').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_report_trans()});
			//$.loader('setContent','');
			});
			
			$('#payroll_Staff_Salary').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_Staff_Salary()});
			//$.loader('setContent','');
			});
			
			$('#payroll_Salary_Sheet').click(function(){
			className:"blue-with-image",
			$.loader({content:getsupport_Salary_Sheet()});
			//$.loader('setContent','');
			});


         function getsupport_account()
         {
             document.payroll_account.submit() ;
         }
			
        function getsupport_reg()
		{
			 document.payroll_register.submit() ;
		}
		 function getsupport_trans()
		{
			 document.payroll_transaction.submit() ;
		}
		 function getsupport_paybal()
		{
			 document.payroll_paybalance.submit() ;
		}
		 function getsupport_paysal()
		{
			 document.payroll_paysalary.submit() ;
		}
		 function getsupport_genslip()
		{
			 document.payroll_genslip.submit() ;
		}
		 function getsupport_banktrans()
		{
			 document.payroll_banktransfer.submit() ;
		}
		 function getsupport_loanadv()
		{
			 document.payroll_loanadvance.submit() ;
		}
		 function getsupport_lonadback()
		{
			 document.payroll_loanadvback.submit() ;
		}
		
		 function getsupport_expclm()
		{
			 document.payroll_expclaim.submit() ;
		}
		 function getsupport_expdis()
		{
			 document.payroll_expdiburs.submit() ;
		}
		 function getsupport_dedu()
		{
			 document.payroll_deduction.submit() ;
		}
		 function getsupport_endser()
		{
			 document.payroll_endservice.submit() ;
		}
		 function getsupport_rep()
		{
			 document.payroll_report.submit() ;
		}
		
		 function getsupport_report_exp()
		{
			 document.expense_report.submit() ;
		}
		function getsupport_report_salary()
		{
			 document.salary_report.submit() ;
		}
		function getsupport_report_trans()
		{
			 document.trans_report.submit() ;
		}
		
		 function getsupport_Staff_Salary()
		{
			 document.payroll_staff_salary.submit() ;
		}
		 function getsupport_Salary_Sheet()
		{
			 document.payroll_salary_sheet.submit() ;
		}	
		
        </script>
        <div style="display:none;">
	<form name="payroll_register" action="payroll/payroll_register" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_transaction" action="payroll/pay_transaction" method="post">
    <input type="hidden" name="month" value="<?php echo date("M");?>" />
    </form>
    
    <form name="payroll_paybalance" action="payroll/salary_payment" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_paysalary" action="payroll/paysalary_time" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
     <input type="hidden" name="year" value="<?php echo date("Y");?>" />
    </form>
    
    
    
    <form name="payroll_genslip" action="payroll/salary_gen_slip" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_banktransfer" action="payroll/trans_banktransfer" method="post">
    <input type="hidden" name="month" value="<?php echo date("M");?>" />
    </form>
    
    
    
    
    
    <form name="payroll_loanadvance" action="payroll/advance_salary" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_loanadvback" action="payroll/advance_payback" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_expclaim" action="payroll/expense_mgt" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_expdiburs" action="payroll/expense_disbursment_view" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_deduction" action="payroll/deduction" method="post">
<!--    <input type="hidden" name="month" value="--><?php //echo date("m");?><!--" />-->
    </form>
    
    <form name="payroll_endservice" action="payroll/end_service" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_report" action="payroll/reports" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="expense_report" action="payroll/expense_reports" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="salary_report" action="payroll/salary_reports" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="trans_report" action="payroll/transactions_reports" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_staff_salary" action="payroll/staff_salary" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>
    
    <form name="payroll_salary_sheet" action="payroll/reviewsalary_time" method="post">
    <input type="hidden" name="month" value="<?php echo date("m");?>" />
    </form>

     <form name="payroll_account" action="payroll/account_mng" method="post">
         <input type="hidden" name="month" value="<?php echo date("m");?>" />
     </form>

     </div>