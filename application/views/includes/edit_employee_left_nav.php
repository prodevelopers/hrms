<div class="contents-container">

<div class="left-nav">
<?php 
$photo        = $employee->photograph; 
$employee_id  = $employee->employee_id;
?>
		<div class="employee-name"><?php echo (isset($employee->full_name) && !empty($employee->full_name))?$employee->full_name:''; ?></div>
		<div class="user-pic">
			<img src="<?php if(isset($photo) && !empty($photo)){if(empAvatarExist($photo) === TRUE){ echo base_url().'upload/'.$photo;}else{ echo empAvatarExist($photo);}} ?>" width="100%" />
			<div class="upload-pic">
				Upload
			</div>
		</div>
		<ul>
			<li><a href="human_resource/edit_employee_acct/<?php echo @$employee_id; ?>">Personal Info</a></li>
			<li><a href="human_resource/edit_emp_curr_contact/<?php echo @$employee_id; ?>">Contact Details</a></li>
			<li><a href="human_resource/edit_emp_qualification/<?php echo @$employee_id; ?>">Qualification</a></li>
			<li><a href="human_resource/edit_emp_experience/<?php echo @$employee_id; ?>">Experience</a></li>
			<li><a href="human_resource/edit_emp_skill/<?php echo @$employee_id; ?>">Skills & Trainings</a></li>
			<li><a href="human_resource/edit_emp_employment/<?php echo @$employee_id; ?>">Employment Info</a></li>
			<li><a href="human_resource/edit_emp_dependent/<?php echo @$employee_id; ?>">Dependents</a></li>
			<li><a href="human_resource/edit_emp_pay_package/<?php echo @$employee_id; ?>">Pay Package</a></li>
			<li><a href="human_resource/edit_emp_entitlement_increment/<?php echo @$employee_id; ?>">Entitlements</a></li>
			<li><a href="human_resource/edit_emp_report_to/<?php echo @$employee_id; ?>">Report To</a></li>
			<li><a href="human_resource/edit_emp_attachment/<?php echo @$employee_id; ?>">Attachments</a></li>
            <li><a href="human_resource/appointment/<?php echo @$employee_id; ?>">Appointment Letter</a></li>
            <li><a href="human_resource/coverLetter/<?php echo @$employee_id; ?>">Contract Letter</a></li>
		</ul>
	</div>