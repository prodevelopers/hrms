<br class="clear">
<footer>
    <script src="assets/js/modernizr.custom.js"></script>
    <script src="<?php echo base_url() ?>assets/select2/select2.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="assets/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/Parexons.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/customScripting.js"></script>
    


    <!-- navigation  -->
    <script>
        //  The function to change the class
        var changeClass = function (r, className1, className2) {
            var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
            if (regex.test(r.className)) {
                r.className = r.className.replace(regex, ' ' + className2 + ' ');
            }
            else {
                r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"), ' ' + className1 + ' ');
            }
            return r.className;
        };

        //  Creating our button in JS for smaller screens
        var menuElements = document.getElementById('menu');
        menuElements.insertAdjacentHTML('afterBegin', '<button type="button" id="menutoggle" class="navtoogle" aria-hidden="true"><i aria-hidden="true" class="icon-menu"> </i> Menu</button>');

        //  Toggle the class on click to show / hide the menu
        document.getElementById('menutoggle').onclick = function () {
            changeClass(this, 'navtoogle active', 'navtoogle');
        }

        // http://tympanus.net/codrops/2013/05/08/responsive-retina-ready-menu/comment-page-2/#comment-438918
        document.onclick = function (e) {
            var mobileButton = document.getElementById('menutoggle'),
                buttonStyle = mobileButton.currentStyle ? mobileButton.currentStyle.display : getComputedStyle(mobileButton, null).display;

            if (buttonStyle === 'block' && e.target !== mobileButton && new RegExp(' ' + 'active' + ' ').test(' ' + mobileButton.className + ' ')) {
                changeClass(mobileButton, 'navtoogle active', 'navtoogle');
            }
        }
    </script>
    <!--  navigation -->

    <!-- scroll -->
    <script>
        $(document).ready(function () {

            var nice = $("html").niceScroll();  // The document page (body)
            $("#div1").html($("#div1").html() + ' ' + nice.version);

            $("#boxscroll").niceScroll({cursorborder: "", cursorcolor: "#00F", boxzoom: true}); // First scrollable DIV

            $("#boxscroll2").niceScroll("#contentscroll2", {
                cursorcolor: "#F00",
                cursoropacitymax: 0.7,
                boxzoom: true,
                touchbehavior: true
            });  // Second scrollable DIV
            $("#boxframe").niceScroll("#boxscroll3", {
                cursorcolor: "#0F0",
                cursoropacitymax: 0.7,
                boxzoom: true,
                touchbehavior: true
            });  // This is an IFrame (iPad compatible)

            $("#boxscroll4").niceScroll("#boxscroll4 .wrapper", {boxzoom: true});  // hw acceleration enabled when using wrapper

            //-----------------------Menu shrinked setting------------------------//
            $('#right-panel-link').on('click',function(e){
               var navWidth =  $('#menu').width();
                if(navWidth>1230){
                    console.log('Menu Has Been Shrinked');
                    /*$('#first').css("background","red");
                     $('#second').css("background","green");
                     $('#third').css("background","yellow");*/

                    /*$('#first').css("width","15px;");
                    $('#second').css("width","15px");
                    $('#third').css("width","15px;");*/



                /*$first = '<div class="li" id="first">'
                   + ' <a href="user_site/view_user_management" style="height:55px; width:57px; padding-top:0.4em;">'
                   +' <span class="fa fa-user fa-2x" style="float:left;line-height:50px; padding-left:0.4em;"></span>'
                    +'<h4 style="height:20px; width:175px; top:3px; float:left;">Users Management</h4>'
                    +'</a>'
                    +'</div>';

                    $('$first').css("background","red");*/

                }else if(navWidth < 1230){
                    console.log('Nav Menu Its at Full Size.');
                }
            });
        });
    </script>
    <!-- scroll -->

</footer>


</body>
</html>