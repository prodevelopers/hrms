<style>
.count{
	color:#900;
	width:10px;
	height:22px;
	float:right;
	background:#060;
	color:#FFF;
	padding:0;
	margin:0;
	top:-5px;
	position:relative;
	
}
</style>
<div class="left-nav nav-collapse">
	<ul>
        <li><a href="dashboard_site/allowances_approvals">Allowances Approvals<span class="count"><?php if (empty($allowance)){echo "0";}else {echo $allowance;}?></span></a></li>
        <li><a href="dashboard_site/benefits_approvals">Benefits Approvals<span class="count"><?php if (empty($benefits)){echo "0";}else {echo $benefits;}?></span></a></li>
        <li><a href="dashboard_site/deductions">Deductions Approvals<span class="count"><?php if (empty($deduction)){echo "0";}else {echo $deduction;}?></span></a></li>
        <li><a href="dashboard_site/review_breached_disciplines">Discipline Reviews<span class="count"><?php if (empty($disciplineReview)){echo "0";}else {echo $disciplineReview;}?></span></a></li>
        <li><a href="dashboard_site/expense_claim">Expense Claim Approvals<span class="count"><?php if (empty($expense_claims)){echo "0";}else {echo $expense_claims;}?></span></a></li>
        <li><a href="dashboard_site/extend_approval">Extension Approvals<span class="count"><?php if (empty($contract)){echo "0";}else {echo $contract;}?></span></a></li>
        <li><a href="dashboard_site/EosApprovals">Eos Transactions Approvals<span class="count"><?php if (empty($eos)){echo"0";}else{echo $eos;}?></span></a></li>
        <li><a href="dashboard_site/increments_approvals">Increment Approvals<span class="count"><?php if (empty($increments)){echo "0";}else {echo $increments;}?></span></a></li>
        <li><a href="dashboard_site/applicants_approvals">Job Applicants Approvals<span class="count"><?php if (empty($ApplicantsReviews)){echo "0";}else {echo $ApplicantsReviews;}?></span></a></li>
        <li><a href="dashboard_site/leave_entitlement">Leave Entitlement Apr.<span class="count"><?php if (empty($entitlement)){echo "0";}else {echo $entitlement;}?></span></a></li>
        <li><a href="dashboard_site/leave_approval">Leave Application Apr.<span class="count"><?php if (empty($leave_approval)){echo"0";}else{echo $leave_approval;}?></span></a></li>
        <li><a href="dashboard_site/loan_approval">Loan & Advance Approval<span class="count"><?php if (empty($loan)){echo"0";}else{echo $loan;}?></span></a></li>
        <li><a href="dashboard_site/manual_timesheet_approval">M.Timesheet Approvals<span class="count"><?php if (empty($manualTimeSheet)){echo "0";}else {echo $manualTimeSheet;}?></span></a></li>
        <li><a href="dashboard_site/salary_approvals">New Salary Approvals<span class="count"><?php if (empty($salary)){echo "0";}else {echo $salary;}?></span></a></li>
        <li><a href="dashboard_site/posting_approval">Position Approvals<span class="count"><?php if (empty($position)){echo"0";}else{echo $position;}?></span></a></li>
        <li><a href="dashboard_site/posting_approval_new">Posting Approvals<span class="count"><?php if (empty($posting)){echo "0";}else {echo $posting;}?></span></a></li>
        <li><a href="dashboard_site/retirment_separation_approval">Retire/Separation Apr.<span class="count"><?php if (empty($separation)){echo "0";}else {echo $separation;}?></span></a></li>
		<li><a href="dashboard_site/approvals">Transactions Approvals<span class="count"><?php if (empty($count)){echo"0";}else{echo $count;}?></span></a></li>
     </ul>
</div>