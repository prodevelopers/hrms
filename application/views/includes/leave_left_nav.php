<?php
$loggedInEmployee = $this->session->userdata('employee_id');
?>
<div class="left-nav nav-collapse">
	<ul style="width:220px;">
    <li><a href="leave_attendance/view_attendance">Attendance Management<i class="fa fa-caret-right menuorgan"></i></a>
    <ul style="top:10px; position: absolute;">
             <li><a href="leave_attendance/view_attendance">Attendance</a></li>
			<li id="kill"><a href="leave_attendance/view_timesheet">Time Sheet<i class="fa fa-caret-right menuorgan"></i></a>
            <ul  style="top:10px; position: absolute;">
                    <li ><a href="leave_attendance/view_timesheet">Attendance Based TimeSheet</a></li>
                <li><a href="<?php echo base_url()?>leave_attendance/manual_timesheet">Manual Timesheet</a></li>
                <li><a href="leave_attendance/view_standard_timesheet">Monthly Accumulative Hourly Timesheet</a></li>
                    <!-- <li><a href="leave_attendance/view_acumulative">Hourly Accumulative Form Entry</a></li>  -->

            </ul>
	</li>
	  <li><a href="leave_attendance/view_over_time">Over Time</a></li>    </ul>
    </li>
    
    <li><a href="leave_attendance/view_leave_attandance">Leave Management<i class="fa fa-caret-right menuorgan"></i></a>
            <ul style="top:10px; position: absolute;">
            <li><a href="leave_attendance/apply_for_leave">Apply for Leave</a></li>
            <li><a href="leave_attendance/view_leave_attandance">Leave Applications</a></li>
            <li><a href="leave_attendance/view_leave_status">Staff on Leave</a></li>
            <li><a href="leave_attendance/view_leave_entitlement">Leave Entitlement</a></li>
            
            </ul>
    </li>
		</ul>
	</div>
    <script>
	$("#kill ul").hide();
	$("#kill").hover(function(){
		$("#kill ul").show();
		},function(){
		$("#kill ul").hide();	
	});
	</script>