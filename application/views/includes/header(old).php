<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <title>HR Management System</title>
        <base href="<?php echo base_url(); ?>">
        <link rel="stylesheet" type="text/css" href="assets/css/component.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
        <link rel="stylesheet" type="text/css" href="assets/multslect/multiple-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/form.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/data_picker/jquery-ui.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="assets/data_picker/jquery-1.10.2.js"></script>	
        <script src="assets/data_picker/jquery-ui.js"></script>
        <script src="assets/js/script.js"></script>	
        <script src="assets/js/script.js"></script>
        <script src="assets/multslect/jquery.multiple.select.js"></script>
    </head>
<body>
<style type="text/css">
	.dropdown-menu{
	position: relative;
	left: 70%;
}
.margino{
		padding-left: 2em;
		font-family: "Myriad Pro";
	}
	.margino .header {
		width: 200px;
		background: #4D6684;
		color: #fff;
		border:none;
	}
	.marginol{
		position: relative;
		left: 600px;
	}
	.margino ul{
		float: left;
	}
	.margino .menu li{
		float: left;
		width: 200px;
		background: #fff;
		border:none;
	}
	.margino .menu h4{
		font-size: 14px;
		float: left;
	}
	.margino .menu small{
		float: right;
		padding: 0.2em 0;
	}
	.margino .menu a{
		padding: 0.2em 0.5em;
		background: green;
		color: #fff;
	}
	.margino .menu a:hover{
		color: #f8f8f8;
	}
	.margino .menu p{
		float: left;
		width: 100%;
		padding: 0.2em;
		font-size: 13px;
		font-weight: bold;;
	}
	.margino .menu p:hover{
		background: #f8f8f8;
	}
	.margino ul ul{
		position: relative;
		left: -2px;
		top: 27px;
	}
	.margino ul ul li {
		float: left;
	}
	.margino h3 {
		font-size: 12px;
		padding: 0.5em;
	}

.ol{
  padding: 0;
  position: absolute;
  left: 0;
  /*width: 175px;*/
  height: 50px;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  display: none;
  opacity: 0;
  visibility: hidden;
  -webkit-transiton: opacity 0.2s;
  -moz-transition: opacity 0.2s;
  -ms-transition: opacity 0.2s;
  -o-transition: opacity 0.2s;
  -transition: opacity 0.2s;
}
/*ul li ol {
  
}*/
.li { 
  background: #333; 
  display: block; 
  /*width: 175px;*/
  color: #fff;
  font-size: 16px;
  float: left;
}
.li a{ 
  font-size: 16px;
}

.ol.li:hover{
  /*width: 180px;*/
  padding: 0;
  margin: 0;
  -webkit-transition:none;
  -o-transition:none;
  transition:none;
}
ul li:hover .ol {
  display: block;
  opacity: 1;
  visibility: visible;
  position: relative;
}
.li h4 {
  padding: 0;
  position: absolute;
  opacity: 0;
  left: 0;
  display: none;
}
.li:hover h4{
	display: block;
	font-weight: normal;
	background: #333;
	  opacity: 1;
	  visibility: visible;
	  position: relative;
	  padding: 0;
}
</style>
<header>
	<h1>HR Management System</h1>
	<div class="right-icons">
		<ul>
			<li><a href="#">
				<span class="fa fa-user"></span>
				<h4><?php echo $this->session->userdata('full_name'); ?></h4>
				<i class="fa fa-caret-down"></i>
			</a>
			<ul>
				<li><a href="#">My Account</a></li>
				<li><a href="login/log_out">Log Out</a></li>
			</ul>
			</li>
            <!--<li class="margino">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-users"></i>
                <div class="notify-count">7</div>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 7 messages
                	<a href="dashboard_site/approvals" style="margin-left:10px; font-size:12px; color:#fff; ">View All</a>	
                </li>
                <li>
                 /*inner menu: contains the actual data */
                <ul class="menu">
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                <li>
                <p>Leave Attendence</p>
                <a href="#">Action</a>
                <small class="fa fa-clock-o"> 5:00</small>
                </li>
                </ul>
                </ul>
            </li>-->
            </ul>


	</div>
</header>

<div class="nav-container clearfix">
	<nav id="menu" class="nav">					
		<ul>
			<li>
				<a href="dashboard_site/view_dashboard">
					<span class="icon">
						<i aria-hidden="true" class="fa fa-home"></i>
					</span>
					<span>Dashboard</span>
				</a>
			</li>
			<li>
				<a href="human_resource/all_employee_list">
					<span class="icon"> 
						<i aria-hidden="true" class="fa fa-users"></i>
					</span>
					<span>Human Resource</span>
				</a>
			</li>
			<li>
				<a href="leave_attendance/view_leave_attandance">
					<span class="icon">
						<i aria-hidden="true" class="fa fa-edit"></i>
					</span>
					<span>Leave & Attendance</span>
				</a>
			</li>
			<li>
				<a href="payroll/payroll_reg">
					<span class="icon">
						<i aria-hidden="true" class="fa fa-money"></i>
					</span>
					<span>PayRoll</span>
				</a>
			</li>
			<li>
				<a href="reports_con/view_absent_reports">
					<span class="icon">
						<i aria-hidden="true" class="fa fa-book"></i>
					</span>
					<span>Reports</span>
				</a>
			</li>
			<li>
				<a href="user_site/view_user_management">
					<span class="icon">
						<i aria-hidden="true" class="fa fa-gears"></i>
					</span>
					<span>Settings & Maintenance</span>
				</a>
				
					<div class="ol">
					<div class="li">
					<a href="user_site/view_user_management" style="height:55px; width:57px; padding-top:0.4em;">
						<span class="fa fa-user fa-2x" style="float:left;line-height:50px; padding-left:0.4em;"></span>
						<h4 style="height:20px; width:175px; top:3px; float:left;">Users Management</h4>										
					</a>
					</div>															
					<div class="li">
					<a href="config_site/notification" style="height:55px; width:57px; padding-top:0.4em;">
						<span class="fa fa-gear fa-2x" style="float:left;line-height:50px; padding-left:0.4em;"></span>
						<h4 style="height:20px; width:175px; top:3px; float:left; left:-57px;">Configuration</h4>										
					</a>					
					</div>
					<div class="li">
					<a href="#" style="height:55px; width:57px; padding-top:0.4em;">
						<span class="fa fa-hdd-o fa-2x" style="float:left;line-height:50px; padding-left:0.4em;"></span>
						<h4 style="height:20px; width:175px; top:3px; float:left; left:-114px;">Backup & Restore</h4>
					</a>
					</div>	
</div>	
			</li>
			
		</ul>
	</nav>
</div>

<!-- header ends -->

<br style="clear:both;">

						
