<?php /**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 3/26/2015
 * Time: 9:56 AM
 */ ?>

<div class="left-nav nav-collapse">
    <ul style="width:220px;">
        <li><a href="human_resource/job_advertisement">Job Advertisements</a></li>
        <li><a href="human_resource/available_applicants">Applicants</a></li>
        <li><a href="human_resource/InterviewList">Interview List</a></li>
    </ul>
</div>
