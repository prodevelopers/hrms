<?php
$loggedInEmployee = $this->session->userdata('employee_id');
if(isset($monthYear) && !empty($monthYear)){
	$Year = $monthYear[1];
	$Month = $monthYear[0];
	$MonthNumber =  date('m', strtotime($Month));
	$date = $Year.'-'.$MonthNumber.'-01';
	$end  = $Year.'-'.$MonthNumber.'-' . date('t', strtotime($date));
}else{
	$date = date('Y-m-01');
	$end = date('Y-m-') . date('t', strtotime($date));
}
$monthYear = explode('-',$date);
?>
<?php
$projectData = array(
	'hoursWorkedInProject' => array()
);
$timeSheetData = array(
	'Date' => array(),
	'Day' => array()
);
if(isset($TimeSheetViewData) && !empty($TimeSheetViewData)){
	$totalProjectsInMonth = array_count_values(array_column(json_decode(json_encode($TimeSheetViewData), true), 'ProjectID'));
	while (strtotime($date) <= strtotime($end)) {
		$day_num = date('d', strtotime($date));
		$day_name = date('l', strtotime($date));
		$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
		foreach ($TimeSheetViewData as $key => $value) {
			if (date('d', strtotime($value->WorkDate)) === $day_num) {
				if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
					$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
				}
				$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = $value->WorkHours;
				if(!array_key_exists('Total',$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
					$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] = 00;
				}
				$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] += intval($value->WorkHours);
			}else{
//            echo '<br />Date Not Matching'.$day_num;
				if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
					$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
				}
				if(isset($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
					if(!array_key_exists($monthYear[0].'-'.$monthYear[1].'-'.$day_num,$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
						$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = '00';
					}
				}
			}
			ksort($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]);
		}
		array_push($timeSheetData['Date'], $day_num);
		array_push($timeSheetData['Day'], $day_name);
	}
//Need to Add the Total Column in the End.
	$timeSheetData['Date']['Total'] = 'Total Hours Worked';
}else{
	$TimeSheetViewData = 'No Projects Assigned to You, So TimeSheet Can Not Be Enabled for You.';
}

//Now Need to Show the Employee Leave Data in Bottom
if(!empty($employeeTotalLeaves) && isset($employeeTotalLeaves)){
	$leaves_array = array_filter($employeeTotalLeaves, function($val){
		return (!empty($val->LeaveFrom) && !empty($val->LeaveTo));
	});
	foreach($leaves_array as $leave) {
		$range = array();
		foreach ($timeSheetData as $tsKey => $value) {
			if($tsKey != 'Day' && $tsKey != 'Date'){
				foreach($value as $vDates=>$vHours){
					//Now Change the Index
					try {
						$new_date = new DateTime($vDates);
						$from = new DateTime($leave->LeaveFrom);
						$to = new DateTime($leave->LeaveTo);
						if($new_date >= $from && $new_date <= $to) {
							$range[] = $vDates . '-'.$leave->HexColor;
							unset($timeSheetData[$tsKey][$vDates]);
						}
					} catch(Exception $e) {
					}
				}
				// changes
				foreach($range as $range_date) {
					if(!array_key_exists( $range_date , $timeSheetData[$tsKey])){
						$timeSheetData[$tsKey][$range_date] = '00';
					}
				}
				// resort
				ksort($timeSheetData[$tsKey]);
			}
		}
	}
}
//exit;
?>
<style type="text/css">
	#pagination
	{
		float:left;
		padding:5px;
		margin-top:15px;
	}
	#pagination a
	{
		padding:5px;
		background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color:#5D5D5E;
		font-size:14px;
		text-decoration:none;
		border-radius:5px;
		border:1px solid #CCC;
	}
	#pagination a:hover
	{
		border:1px solid #666;
	}
	.mytab tr:nth-child(odd) td{
		background-color:#ECECFF;
	}
	.mytab tr:nth-child(even) td{
		background-color:#F0F0E1;
	}
	.paginate_button
	{
		padding: 6px;
		background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color: #5D5D5E;
		font-size: 14px;
		text-decoration: none;
		border-radius: 5px;

		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		border: 1px solid #CCC;
		margin: 1px;
		cursor:pointer;
	}
	.paging_full_numbers"
	{
		margin-top:8px;
	}
	.dataTables_info
	{
		color:#3474D0;
		font-size:14px;

		margin:6px;
	}
	.paginate_active
	{
		padding: 6px;
		border: 1px solid #3474D0;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		color: #3474D0;
		font-weight: bold;
	}
	.dataTables_filter
	{
		float:right;
	}

	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
	.serch
	{
		width: 200px;
		height: 25px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		color: black;
		padding-left: 5px;
		border: 1px solid #999;
	}

	.designing{
		top:200px;
		right:200px;
	}
	#employee_inf0
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;
		display:none;
		alignment-adjust:central;
	}

	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
	.serch
	{
		width: 200px;
		height: 25px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		color: black;
		padding-left: 5px;
		border: 1px solid #999;
	}

	.designing{
		top:200px;
		right:200px;
	}
	#employee_inf0
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;
		display:none;
		alignment-adjust:central;
	}
	table {
		table-layout: fixed;
		width: 150%;
		*margin-left: -100px;/*ie7*/
	}
	td, th {
		vertical-align: top;
	}
	td.columnfirst, th.columnfirst {
		width: 100px;
		font-size:14px;
		font-weight: bold;
	}
	td.inner{
		font-size: 12pt;
		width: 20px;
	}
	.outer {position:relative;}
	div.inner {
		overflow-x:scroll;
		overflow-y:visible;
	}
	.inputCopyCat{
		-moz-appearance: textfield;
		-webkit-appearance: textfield;
		background-color: white;
		background-color: -moz-field;
		border: 1px solid darkgray;
		box-shadow: 1px 1px 1px 0 lightgray inset;
		font: -moz-field;
		font: -webkit-small-control;
		margin-top: 5px;
		padding: 2px 3px;
		width: 398px;
	}
</style>

<!--PLEASE Designer Dont Include Jquery inside View.. It Messes WIth other script Files. Jquery is already included in Head Section. No Need To Include Again.-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> -->
<script src="http://malsup.github.com/jquery.form.js"></script>
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Leave&Attendance / leave Details</div> <!-- bredcrumb -->

<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
<div class="right-contents">

	<!--Employee Personal Information Goes Here..A-->
	<table class="table table-approved">
		<thead class="table-head">
		<td colspan="2">Employee Information</td>
		</thead>
		<tbody>
		<tr class="table-row">
			<td> <h4>Employee Code</h4></td>
			<td><?php echo isset($employeInfo->employee_code) ? $employeInfo->employee_code : ''; ?></td>
		</tr>
		<tr class="table-row">
			<td><h4>Employee Name</h4></td>
			<td><?php echo isset($employeInfo->full_name) ? $employeInfo->full_name : ''; ?></td>
		</tr>
<!--		<tr class="table-row">
			<td><h4>New Leave Type</h4></td>
			<td><?php /*echo isset($emp->leave_type) ? $emp->leave_type : ''; */?></td>
		</tr>
		<tr class="table-row">
			<td><h4>Requested From Date</h4></td>
			<td id="requestFromDate"><?php /*echo date_format_helper(isset($emp->ApplicationFromDate) ? $emp->ApplicationFromDate : ''); */?>
				<input type="hidden" name="fromDate" id="fromDate" value="<?php /*echo (isset($emp->ApplicationFromDate) ? $emp->ApplicationFromDate : '');*/?>">
			</td>
		</tr>
		<tr class="table-row">
			<td> <h4>Requested To Date</h4></td>
			<td id="requestToDate"><?php /*echo date_format_helper(isset($emp->ApplicationToDate) ? $emp->ApplicationToDate : ''); */?>
				<input type="hidden" name="toDate" id="toDate" value="<?php /*echo (isset($emp->ApplicationToDate) ? $emp->ApplicationToDate : '');*/?>"></td>
		</tr>
		<tr class="table-row">
			<td> <h4>Total Leaves Requested</h4></td>
			<td><?php /*echo isset($emp->TotalDays) ? $emp->TotalDays : '' */?>
				<input type="hidden" name="totalDay" id="totalDay" value="<?php /*echo isset($emp->TotalDays) ? $emp->TotalDays : '' */?>"></td>
		</tr>-->

		</tbody>
	</table>
	<!--Employee Informtion Ends Here..A-->

	<table cellspacing="0" id="table_list" style="width:50%; float:left; margin-left:0.7em;">
		<thead class="table-head">
		<td colspan="5">Leave Details</td>
		</thead>
		<tr class="table-row">
			<th>Category</th>
			<th>Allocated</th>
			<th>Leaves Taken Yearly</th>
			<th>Balance</th>
			<!-- <th><b>Total Hours</b></th> -->
		</tr>
		<?php
		//     Now Need to Populate Table of EmployeeLeaves
		foreach($complete_profile as $LeaveTypes){
			echo '<tr class="table-row" style="text-align:center;">';
			echo '<td>'.$LeaveTypes->AvailableLeaveTypes.'</td><td>'.$LeaveTypes->TotalAllocatedLeaves.'</td><td>'.$LeaveTypes->YearTotal.'</td><td>'.$LeaveTypes->Balance.'</td>';
			//<td><b>88:00</b></td>
			echo '</tr>';
		}
		?>
	</table>
</div>
	
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->

<!-- contents -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="text/javascript">

	<?php if(is_admin($loggedInEmployee)){ ?>
	function  createTimeSheet(){
		$.ajax({
			url: "<?php echo base_url(); ?>leave_attendance/createTimeSheetForAllRemainingEmployees",
			type: "POST",
			success:function(output){
				var data = output.split('::');
				if(data[0] == 'OK'){
					Parexons.notification(data[1],data[2]);
				}else if(data[0] == 'FAIL'){
					Parexons.notification(data[1],data[2]);
				}
			}
		});
	}
	<?php } ?>

	$(document).ready(function(e){

		//if the loggedinUser/Employee is Admin
		var selectEmployee = $('#selectEmployee');
		var url = "<?php echo base_url(); ?>leave_attendance/loadAvailableEmployees";
		var id = "EmployeeID";
		var text = "EmployeeName";
		var minInputLength = 0;
		var placeholder = "Select Employee";
		var multiple = false;
		commonSelect2(selectEmployee,url,id,text,minInputLength,placeholder,multiple);
		$('.select2-container').css("width","223px");


//        Get the Selectors of the Page
		//Get Available Years First.
		//        Get the Selectors of the Page
		var selectYearSelector = $('#selectYear');
		var url = "<?php echo base_url(); ?>leave_attendance/load_availableYears_in_timeSheet";
		var id = "TimeSheetID";
		var text = "YearValue";
		var minInputLength = 0;
		var placeholder = "Select Year";
		var multiple = false;
//        commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
		commonSelect2(selectYearSelector,url,id,text,minInputLength,placeholder,multiple);
		$('.select2-container').css("width","223px");

		selectEmployee.on('change',function(e){
			selectYearSelector.select2("destroy");
			var dependentEmployeeWhere = $(this).select2('data');
			commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
			$('.select2-container').css("width","223px");
		});

		selectYearSelector.on("change", function(e){
			var dependentWhere = $(this).select2('data');
			var selectMonthSelector = $('#selectMonth');
			var url = "<?php echo base_url(); ?>leave_attendance/load_availableMonths_in_timeSheet";
			var id = "TimeSheetID";
			var text = "MonthName";
			var minInputLength = 0;
			var placeholder = "Select Month";
			var multiple = false;
			commonSelect2Depend(selectMonthSelector,url,id,text,minInputLength,placeholder,multiple,dependentWhere);
			$('.select2-container').css("width","223px");
			selectMonthSelector.on("change", function(e){
				var monthData = $(this).select2('data');
				var yearData = selectYearSelector.select2('data');

				//Creating First Arrays from Objects
				var monthDataArray = $.map(monthData, function(value, index) {
					return [value];
				});
				var yearDataArray = $.map(yearData, function(value, index) {
					return [value];
				});
//                Creating New Array to send data to controller
				var data = [];
//                Pushing Data to Data Array.
				data.push(monthDataArray[1]);
				data.push(yearDataArray[1]);
				<?php if(is_admin($loggedInEmployee)){ ?>
				data.push(selectEmployee.val());
				<?php } ?>
				console.log(data);
				var redirectURL = "<?php echo base_url(); ?>leave_attendance/manual_timesheet";
				$.redirect(redirectURL,{'selectedMonthYear':data},'POST');
			});
		});
		//Now Need to Get Available Months on Based of Year

		max = 1;
		//Now Need to Get Available Months on Based of Year
		$('tr.tableRow td.inner').on('click', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			$(this).attr('contentEditable', 'true');
			$(this).addClass('inputCopyCat');
			$(this).focus();
		}).on('keyup', function (e) {
			$tr = $(this).closest('tr');
			var sum = 0;
			$tr.find('td.inner').each(function () {
//                console.log(Number($(this).text()));
				sum += Number($(this).text());
			});
			$tr.find('.totalTD').text(sum);
		}).on('keydown',function(e){
			var editorSelector = $(this);
			check_charcount(editorSelector, max, e);
			function check_charcount(editorSelector, max, e)
			{
				var keyCode = e.which;
				if(keyCode !== 8 && keyCode !== 46 && keyCode !== 9 && keyCode !== 17)
				{
					// $('#'+content_id).text($('#'+content_id).text().substring(0, max));
					if(keyCode > 64 && keyCode < 91){
						e.preventDefault();
						e.stopPropagation();
						Parexons.notification('Letters Are Not Allowed','warning');
					}else if(editorSelector.text().length > max){
						e.preventDefault();
						e.stopPropagation();
						Parexons.notification('You can Not Add More Charactersa','warning');
					}
				}
			}
		});
//Function to Send Ajax Request to Server when Focus is Out.
		$('tr.tableRow td.inner').focusout(function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			$('td.inputCopyCat').removeAttr('contentEditable');
			$('td.inputCopyCat').removeClass('inputCopyCat');
			var hoursWorked = Number($(this).text());
			hoursWorked = hoursWorked ? hoursWorked : 0;
			var dateOfHoursWorked = $(this).attr('td-data');
			var projectID = $( 'td:first-child', $( this ).parents ( 'tr' ) ).attr('data-project');
			var postData = {
				hoursWorked: hoursWorked,
				dateOfHoursWorked: dateOfHoursWorked,
				projectID: projectID
			};
			<?php if(isset($employeeData) && !empty($employeeData)){

    echo "postData.EmployeeID = $employeeData->EmployeeID;";
}
?>
			$.ajax({
				url: "<?php echo base_url(); ?>leave_attendance/updateTimeSheetInfo",
				data: postData,
				type: "POST",
				success:function(output){
					var data = output.split("::");
					if(data[0] === "OK"){
						Parexons.notification(data[1],data[2]);
						//Now Need to Sum The Values in the Total if Record Return is Success...
						//console.log(sum);
					}else if(data[1] === "FAIL"){
						Parexons.notification(data[1],data[2]);
					}
				}
			});
		});
	});
	$(function(){
		var message_status = $("#status");
		$("td[contenteditable=true]").blur(function(){
			var field_userid = $(this).attr("id") ;
			var value = $(this).text() ;
			var data={
				field_userid: $(this).attr("id"),
				value: $(this).text()
			};
			$.ajax({
				url:'leave_attendance/update_timesheet',
				data:data,
				type: 'POST',
				success: function(output){
					console.log(output);
				}
			});
		});
	});
</script>
