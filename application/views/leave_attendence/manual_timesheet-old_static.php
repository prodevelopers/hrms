<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
</style>
<script>
      var navigation = responsiveNav(".nav-collapse");
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="http://malsup.github.com/jquery.form.js"></script> 
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Manual Time Sheet</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/leave_left_nav'); ?>
 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Manual Timesheet</div>
		<div class="filter">
			<h4>Filter</h4>
			<div class="row">
				<h4>Month</h4>
					<select style="float: left; width: 140px;">
						<option selected value="empty">Select Month</option>
						<option value="pension">July</option>
					</select>
				<h4>Year</h4>
					<select style="float: left; width: 140px;">
						<option selected value="empty">Select Year</option>
						<option value="pension">July</option>
					</select>
			</div>
		</div>
		<br class="clear">
		<table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em;">
				<thead class="table-head">
					<td>Color Code</td>
				</thead>
				<tbody>
					<tr class="table-row">
						<td>
							<ul class="color-legend">
								<li class="sun"><span>Sunday</span></li>
								<li class="casual"><span>Casual</span></li>
								<li class="sick"><span>Sick</span></li>
								<li class="annual"><span>Annual</span></li>
								<li class="holiday"><span>Holiday</span></li>
								<li class="compen"><span>Compensatory</span></li>
							</ul>
						</td>
					</tr>
                </tbody>
			</table>  
		<!-- <table cellspacing="0" id="table_list" style="width:30%; float:left; margin-left:0.7em;">
				<tr class="head">
						<td colspan="5" style="padding:0.2em 1em; text-align:center;">Select Week Ends</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Sunday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Monday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Tuesday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Wednesday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Thursday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Friday</td>
					</tr>
					<tr class="table-row">
						<td><input type="checkbox"></td>
						<td>Saturday</td>
					</tr>

			</table>	 -->
</div>

	</div>
 <div class="right-contents" style="width:98%; margin-right:0.5em;">
 	<table><thead><tr>
 	<th>Name:</th>
 	<th>Noor Malik</th>
 	<th>Project Implementation Period:</th>
 	<th>September 2014</th>
 	<th>Location:</th>
 	<th>Swat</th></tr></thead></table>
	<div class="table-responsive1">
	  <div style="width:150%">
		<div class="rowfirst">
			<div class="rowfirstinside head">Indicate Cost <br>Code(s) - 1/line</div>
			<div class="rowfirstinside">DRR 2</div>
			<div class="rowfirstinside">OGB</div>
			<div class="rowfirstinside">SDC</div>
			<div class="rowfirstinside">UNICEF SPSP</div>
			<div class="rowfirstinside">UNICEF WASH</div>
			<div class="rowfirstinside">SSFNS WHH</div>
			<div class="rowfirstinside">Total Work Hours</div>
			
		</div>
		<div class="rowdays">
			<div class="head">01<br>Mo
			</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">02<br>Tu</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">03<br>We</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">04<br>Th</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">05<br>Fr</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">06<br>Sa</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays sun">
			<div class="head">07<br>Su</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			</div>
		<div class="rowdays">
			<div class="head">08 <br> Mo</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">09<br>Tu</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">10 <br> We</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">11 <br> Th</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">12 <br> Fr</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">13<br>Sa</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays sun">
			<div class="head">14 <br>Su</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">15 <br> Mo</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">16<br>Tu</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">17 <br> We</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">18 <br> Th</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">19 <br> Fr</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">20<br>Sa</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays sun">
			<div class="head">21 <br>Su</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">22 <br> Mo</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">23<br>Tu</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">24 <br> We</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">25 <br> Th</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">26 <br> Fr</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">27<br>Sa</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays sun">
			<div class="head">28 <br>Su</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">29 <br> Mo</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">30<br>Tu</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowdays">
			<div class="head">31 <br> We</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
			<div class="rowdaysinside" contenteditable="true">12:00</div>
		</div>
		<div class="rowend">
			<div class="rowendinside head">Total <br>Hours <br class="clear"></div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
			<div class="rowendinside" contenteditable="true">00</div>
		</div>
	</div>
 </div>
<table cellspacing="0" id="table_list" style="width:70%; float:left; margin-left:0.7em;">
	<thead class="table-head">
		<td colspan="10">Leave Details</td>
	</thead>
 	<tr class="table-row">
 		<th>Category</th>
 		<th>Allocated</th>
 		<th>Current Month</th>
 		<th>Leaves Taken</th>
 		<th>Balance</th>
		<th><b>Total Hours</b></th>
 	</tr>
 	<tr class="table-row" style="text-align:center;">
 		<td>Sick Leave</td>
 		<td>4</td>
 		<td>December</td>
 		<td>5</td>
 		<td>2</td>
		<td><b>88:00</b></td>
 	</tr>
 	<tr class="table-row" style="text-align:center;">
 		<td>Casual Leave</td>
 		<td>4</td>
 		<td>December</td>
 		<td>5</td>
 		<td>2</td>
		<td><b>88:00</b></td>
 	</tr>
 	<tr class="table-row" style="text-align:center;">
 		<td>Holiday Leave</td>
 		<td>4</td>
 		<td>December</td>
 		<td>5</td>
 		<td>2</td>
		<td><b>88:00</b></td>
 	</tr>
 	<tr class="table-row" style="text-align:center;">
 		<td>Compensatory</td>
 		<td>4</td>
 		<td>December</td>
 		<td>5</td>
 		<td>2</td>
		<td><b>88:00</b></td>
 	</tr>
 	<tr class="table-row" style="text-align:center;">
 		<td>Annual Vac. Leave</td>
 		<td>4</td>
 		<td>December</td>
 		<td>5</td>
 		<td>2</td>
		<td><b>88:00</b></td>
 	</tr>
 </table>

 <table cellspacing="0" id="table_list" style="width:20%; float:left; margin-left:0.7em;">
	<thead class="table-head">
		<td colspan="2">Time Allocation</td>
	</thead>
 	<tr class="table-row">
 		<td>Name Of The Project</td>
 		<td>%</td>
 	</tr>
 	<tr class="table-row">
 		<td>Project 1</td>
 		<td>4</td>
 	</tr>
 	<tr class="table-row">
 		<td>Project 2</td>
 		<td>12</td>
 	</tr>
	<tr class="table-row">
 		<td>Project 3</td>
 		<td>43</td>
	</tr>
	<tr class="table-row">
 		<td>Project 4</td>
 		<td>11</td>
	</tr>
 	<tr class="table-row">
		<td><b>Total Hours</b></td>
		<td><b>17</b></td>
 	</tr>
 </table>
 <form action="">
 	<div class="row">
 		<h4>Submited by:</h4>
 		<input type="text" placeholder="Sender">
 	</div>
 	<br class="clear">
 	<div class="row">
 		<h4>Reviewed by:</h4>
 		<input type="text" placeholder="Reviewed By">
 	</div>
 	<br class="clear">
	<div class="row">
		<input type="submit" class="btn green" value="Submit">
	</div>
 </form>
 	
</div>


<!-- contents -->
<script type="text/javascript">
$(function(){  
	var message_status = $("#status");
	 $("td[contenteditable=true]").blur(function(){
	 var field_userid = $(this).attr("id") ;         
	 var value = $(this).text() ; 
	 var data={
	 	field_userid: $(this).attr("id"),
	 	value: $(this).text()
	 };
	 $.ajax({
	 	url:'leave_attendance/update_timesheet',
	 	data:data,
	 	type: 'POST',
	 	success: function(output){
	 		console.log(output);
	 	}
	 });
	 // $.post('leave_attendance/update_timesheet' , field_userid + "=" + value, function(data){ 
	 // if(data != '')              
	 // { 
	 // alert('jsdfkj');
	 //  message_status.show();                  
	 //  message_status.text(data);12                 
	 //  setTimeout(function(){message_status.hide()},3000);          
	 // }        
  // });  
 });     
});
</script>
