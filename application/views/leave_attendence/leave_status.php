<style type="text/css">
	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
select[name="status_title"],select[name="leave_type"]{
	width: 30%;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 1%;
	top:-13px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -89.3%;
	top: 20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -60px;
	left: -65px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>
<!-- contents -->

<div class="contents-container">

	 <!-- bredcrumb -->



 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"leave_type","value":$('#type').val()});
			aoData.push({"name":"status_title","value":$('#status').val()});
                
                
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_leave_status(id)
{
	window.open('leave_attendance/print_leave_status/'+id,"","width=800,height=600");
}
function print_all_leave_status(id)
{
	var id = '<?php echo isset($employee_id) ? $employee_id: ""; ?>';
	window.open('leave_attendance/print_all_leave_status/'+id,"","width=800,height=600");
}
$(function() {
	$( "#in" ).datepicker({dateFormat: "yy-mm-dd",maxDate: 0});
});



function lookup(inputString) {

	if(inputString.length == 0) {
		$('#suggestions').hide();
		$('#employee_inf0').hide();
		location.reload();
	} else {
		$.post("leave_attendance/autocomplete_time_sheet/", {queryString: ""+inputString+""}, function(data){
			if(data.length > 0) {
				$('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
				//console.log(data);
			}
		});
	}
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue, employee_id) {
	$('#id_input').val(thisValue);
	$('#emp_id').val(employee_id);
	setTimeout("$('#suggestions').hide();", 200);


	//setTimeout("$('#autoSuggestionsList').hide()", 200);
}



</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Staff On Leave</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Staff On Leave</div>
		<div class="notice info">
			<p>Important Note</p>
			<ul>
				<li>List Includes all the employees whose leaves are approved, and are currently on leave</li>
			</ul>
		</div>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
					<h4>Employee Name</h4>
					<input type="text" name="full_name" id="id_input" style="width:190px;"  placeholder="Search Here" autocomplete="off" onkeyup="lookup(this.value)">
					<input type="hidden" name="emp_id" id="emp_id" />
					<div id="suggestions">
						<div class="autoSuggestionsList_l" id="autoSuggestionsList">
						</div>
					</div>
					<h4>From Date</h4>
					<input type="text" id="datepicker" name="from_date" style="width:190px;">
					<h4>To Date</h4>
					<input type="text" id="date" name="to_date" style="width:190px;">
					<button class="btn green atm">Search</button>
			</div>
		<span class="fa fa-print move" style="cursor:pointer" onclick="return print_all_leave_status();"></span>
<?php echo form_close();?>
            <!--  <form action="leave_attendance/print_all_leave_status/<?php /*echo $this->input->post('leave_type');*/?>" method="post">

                <span class="fa fa-print move" style="cursor:pointer" onclick="return print_all_leave_status('<?php /*echo $this->input->post('leave_type');*/?>')"></span>
                <input type="hidden" name="st_type" id="klo" value="<?php /*echo $this->input->post('status_type');*/?>"/>
                  <input type="hidden" name="task_id" id="hjki" value="<?php /*echo $this->input->post('task_name');*/?>"/>
               -->

               
               
                 </form>
                       <table class="table"  cellspacing="0" width="100%">
                   <thead class="table-head">
                   <tr>
                   <td>Employee ID</td>
					<td>Employee Name</td>
                   <td>Emergency Contacts</td>
					<td>Leave Type</td>
                    <td>From</td>
                    <td>To</td>
                   <td>Total Days</td>
                   </tr>

<!--                   <!-- <th align="center"><span class="fa fa-pencil"></span></th>-->
<!--				    <th align="center"><span class="fa fa-print"></span></th>-->
					<!--<th width="92">Action</th>-->
                </thead>
                  
                     <?php if(!empty($leave_staff)){
					foreach($leave_staff as $leave_staff){?>
                    <tbody>
                    <tr class="table-row">
                    <td><?php echo $leave_staff->employee_code?></td>
                    <td><?php echo $leave_staff->full_name?></td>
                    <td><?php echo $leave_staff->mobile_num?></td>
                    <td><?php echo $leave_staff->leave_type?></td>
                    <td><?php echo date_format_helper($leave_staff->approved_from);?></td>
                    <td><?php echo date_format_helper($leave_staff->approved_to);?></td>
					<td><?php echo $leave_staff->no_of_paid_days + $leave_staff->no_of_un_paid_days;?></td>

                  <?php /*?>  <td><a href="leave_attendance/edit_leave_staff/<?php echo $leave_staff->employee_id?>"> <span class="fa fa-pencil"></span></a></td><?php */?>
<!--                    <td><a href="leave_attendance/print_leave_status/--><?php //echo $leave_staff->employee_id?><!--"><span class="fa fa-print" onclick="print_leave_status" style="cursor:pointer"></span></a></td>-->
                    </tbody>
                    <?php }}else{?>
                    <td colspan="10"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
                    <?php }?>
                    </tr>
                
                
         </table>
		          <div id="pagination">
            <ul>
            <?php echo $links; ?>
            </ul>
            </div>
            </div>
            </div>
            </div>
				<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
<!-- contents -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $( "#datepicker" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,


        onClose: function( selectedDate ) {
            $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
            
        }

    });
    $( "#date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function( selectedDate ) {
            $("#applyForLeaveFromDate").datepicker( "option", "maxDate", selectedDate );
        }
    });
</script>

