<!-- contents -->
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Leave&Attendance / Update Accumulative Time Sheet</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">
		<div class="head">Update Accumulative Time Sheet</div>
		<div class="form-left">
            <?php echo form_open(); ?>
            <div class="row2">
					<h4>Employee ID</h4>
				<input type="text" name="employee_code" value="<?php echo $employee->employee_code;?>" readonly="readonly"/>
				</div>
					<div class="row2">
					<h4>Employee Name</h4>
				<input type="text" name="full_name" value="<?php echo $employee->full_name;?>" readonly="readonly"/>
				</div>
                   <br class="clear">
				<div class="row2">
					<h4>Standard Hours</h4>
                      <input type="text" name="monthly_std_hrs" id="standardHours" value="<?php echo $employee->monthly_std_hrs;?>" readonly="readonly"/>
				</div>
                 <br class="clear">
				<div class="row2">
					<h4>Month</h4>
                     <?php 
					 $slctd_month = (isset($employee->ml_month_id) ? $employee->ml_month_id  : ''); 
					 echo form_dropdown('month',$month, $slctd_month);?>
				</div>
                 <br class="clear">
				<div class="row2">
					<h4>Year</h4>
                     <?php  
					 $slctd_month = (isset($employee->ml_year_id) ? $employee->ml_year_id  : ''); 
					  echo form_dropdown('year',$year, $slctd_month);?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Work Hours</h4>
					<input type="text" name="work_hours" id="workHours" value="<?php echo $employee->work_hours;?>">
				</div>
              
				<div class="row2">
					<h4>Leave Hours</h4>
					<input type="text" name="leave_hours" id="leaveHours" value="<?php echo $employee->leave_hours;?>">
				</div>
				
				<br class="clear">
				
				<!-- button group -->
			<div class="row2">
				<div class="button-group">
				<input type="submit" name="edit" value="Update" class="btn green">
				</div>
			</div>
				
			<?php echo form_close();?>
			</div>
		</div>
	</div>
<!-- contents -->
<script type="text/javascript">
    $(document).ready(function() {
        //Updated Code By SHH
        var totalStandardHours = parseInt($('#standardHours').val());
        $('#workHours, #leaveHours').on('change', function (e) {
            var checkVariable = parseInt($(this).val());
            if(!isNaN(checkVariable)){
                $(this).val(checkVariable);
            }else{
                Parexons.notification('Only Numeric Input Is Allowed.','error');
                $(this).val(0);
            }
            //if Input Number is Greater Then the Standard Hours Number Then Make it Equal to Standard Hours.. Cuz We Cant Go Beyond Standard Number..
            if(parseInt($(this).val()) > totalStandardHours){
                $(this).val(totalStandardHours);
            }

        });

    });
</script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->