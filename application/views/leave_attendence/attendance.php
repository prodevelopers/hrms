<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
<!--<script src="<?php /*echo base_url()*/?>assets/js/bootstrap.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/data_picker/jquery-ui.js"></script>
<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/css/bootstrap.css" type="text/css"/>-->
<link rel="stylesheet" href="<?php echo base_url()?>assets/data_picker/jquery-ui.css" type="text/css"/>

<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<?php
//var_dump($employee_id); ?>
<style type="text/css">
    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .mytab tr:nth-child(odd) td {
        background-color: #ECECFF;
    }

    .mytab tr:nth-child(even) td {
        background-color: #F0F0E1;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers
    {
        margin-top: 8px;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;

        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 100%;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 40px;
    }

    /*------------------------------Model Send Email CSS--------------------*/

     #loginmodal{width: 50%;}
    .close{float:right;padding-top:5px;padding-bottom: 5px;padding-left: 10px;padding-right: 10px;}
    #close-hover:hover{cursor: pointer}
    .self-row{width:100%;height:35px;float:left;margin-bottom: 10px;}
    .self-row-attac{width:100%;height:35px;float:left;margin-bottom: 10px;margin-top: 100px;}
    .Label-left{width:200px;line-height:40px;float:left;padding-left: 10px;}
    .right-sec{float:left;width:300px;height: 35px;margin-left:10px;}
    .right-sec .textfield{width:280px;height:35px;padding-left: 5px;padding:10px;font-family: arial, sans-serif}
    #modal-title-self{border-bottom: 1px solid #808080}

    .label-attac{float:left;width:100px;height: 35px;margin-top: 10px;}
    .textarea-self{padding:10px;}
    .below{margin-top: 5px;}

    /*------------------------------------End Model CSS----------------------------->*/
    @media (max-width: 1171px){
        input[type="text"]{
            width: 100% !important;
        }
        form .row input[type="text"]{
            width: 100% !important;

        }

    }

    @media (min-width: 800px) and (max-width: 940px){
        input[type="text"]{
            width: 100% !important;
        }


    }

</style>

<div class="contents-container">
    <div class="bredcrumb">Dashboard / Leave&Attendance / Attendance</div>
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

    <div class="right-contents">

        <div class="head">Attendance</div>
        <div class="notice info">
            <p>Instruction</p>
            <ul>
                <li>The Button <strong>SignOut All Employees</strong> will absent mark all the employees who have not yet marked their attendance in morning or checked out them selves.</li>
                <li>The Button <strong>Import Attendance From File</strong> imports attendance records from both XCEL file and CSV file.</li>
                <li>The Button <strong>PDF</strong> generates  attendance to pdf file.</li>
                <li>The Button <strong>Update Device Records</strong> gets attendance from fingerprint device.</li>
                <li>The Button <strong>Send Email</strong> sends attendance sheet to the provided email address.</li>
                <li>The Button <strong>CSV</strong> generates  attendance to csv formatted file.</li>
            </ul>
        </div>
			<div class="filter">
				<h4>Filter By</h4>
                <div class="row">
                 <div class="col-xs-11 col-sm-11" style="padding:0px;">
                <?php echo form_open();?>
                <input style="min-width: 100% !important; max-width: 100% !important; margin-left: 1%" type="text" id="input_id" class="form-controll" name="full_name" placeholder="Employee Name" class="serch" autocomplete="off" onkeyup="lookup(this.value)">
				<input type="hidden" name="emp_id" id="emp_id" />
				<div id="suggestions">
					<div class="autoSuggestionsList_l" id="autoSuggestionsList">
					</div>
				</div>
                 </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12" style="margin-top: 1%;">
                      <div class="col-xs-3" style="padding: 1%;padding-left: 1.3%">
                     <?php //  echo @form_dropdown('year',$year);?>
                     <select name="project_title" style="width:100%" id="assign">
                     <option value="">-- Select Project --</option>
                     <?php foreach($projects as $assign_task){?>
                     <option value="<?php echo $assign_task->project_id;?>"><?php echo $assign_task->project_title;?></option>
                     <?php }?>
                     </select>
                       </div>
                       <div class="col-xs-3" style="padding: 1%;padding-left: 4%">
                     <?php //  echo @form_dropdown('year',$year);?>
                     <select name="status_type" style="width:100%;" id="status">
                     <option value="">-- Select Status --</option>
                     <?php foreach($status as $attendance){?>
                     <option value="<?php echo $attendance->attendance_status_type_id;?>"><?php echo $attendance->status_type;?></option>
                     <?php }?>
                     </select>
                      </div>
                       <div class="col-xs-4" style="padding: 1%;padding-left: 4%">
                     <?php //  echo @form_dropdown('year',$year);?>
                     <select name="department_name" style="width:75%;" id="department">
                     <option value="">-- Select Department --</option>
                     <?php foreach($select_department as $department){?>
                     <option value="<?php echo $department->department_id;?>"><?php echo $department->department_name;?></option>
                     <?php }?>
                     </select>&nbsp;&nbsp;&nbsp;&nbsp;
                      <span class="fa fa-print " style="cursor:pointer" onclick="return print_all_attendance();"></span>
                      </div>
                    </div>
                     </div>

                <div class="row">
                    <div class="col-xs-12"  style="margin-top: 1%;">
                    <div class="col-xs-3">
                        <input type="text" id="in_date" name="in_date" placeholder=" From Date" style="min-width: 100% !important;max-width: 100% !important;">
                    </div>
                    <div class="col-xs-3"  style="padding-left: 3.3%">
                        <input type="text" id="out_date"name="out_date" placeholder=" To Date" style="min-width: 100% !important;max-width: 100% !important;">
                    </div>
                    <div class="col-xs-3"style="padding-left: 4%">
                         <button style="padding:1%;border-radius:1px;width: 100%" class="btn btn-defult">Search</button>
                    </div>
                    </div>
                </div>
                <div class="row col-xs-12">
                <div class="col-xs-12" style="padding-left: 2.3%;">
                    <input type="button" style="border-radius:1px;" id="signOutAllEmployees" class="btn green" value="Sign Out All Employees"/>
                    <input type="button" style="border-radius:1px;" id="importFromFile" class="btn green" value="Import Attendance From File"/>
                    <input type="button" style="border-radius:1px;" id="pdfButton" class="btn green" value="PDF" title="Click to Download as PDF !"/>
                    <input type="button" style="border-radius:1px;" id="AttendanceButton" class="btn green" value="Update Device Records" title="Click to Update Device !"/>

                   <!-- <a href="#loginmodal"><input type="button" id="modaltrigger " class="btn green" value="Send Email" title="Click to Send Email with PDF Attachment !" /></a>-->
                    <a href="#loginmodal"  style="border-radius:1px;" class="btn green" id="modaltrigger" title="Click to Send Email with PDF Attachment !">Send Email</a>
                    <?php if(isset($files)){?><a href="<?php echo base_url().@$files;?>">
                            <input type="button" id="CSVButton" class="btn green" value="CSV" title="Click to Download as PDF !"/></a>
                    <?php
                    }
                    else{
                        ?>
                    <a href="<?php echo base_url().@$files;?>"><input type="button" class="btn" style="background-color: darkgray;border-radius:1px;" disabled value="CSV" title="Click to Download as PDF !"/></a>
                    <?php }?>

                 </div>
                </div>
				<?php echo form_close();?>
                <form action="leave_attendance/print_all_attendance/" method="post">

                 </form>
            </div>
			<div class="table-responsive1">
			<table cellspacing="0" style="width:106%; ">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
                    <td>Project</td>
                    <td>Department</td>
					<td>Coordinates</td>
					<td>In Date</td>
					<td>In Time</td>
					<td>Out Date</td>
                    <td>Out Time</td>
					<td>This Day</td>
					<td>Total Hours</td>
                    <td id="age" title="It will show your Attendence status.">Status</td>
					<!--<td><span class="fa fa-print"></span></td>-->
				</thead>
                <tbody>
                <?php
                if (!empty($leave_attend)){
                foreach ($leave_attend as $attendance){
                $timestart = $attendance['in_time'];
                $timestop = $attendance['out_time'];
                if ($timestop !== '00:00:00') {
                    //Change It
                    $time1 = new DateTime($timestart);
                    $time2 = new DateTime($timestop);
                    $time_diff = $time1->diff($time2);

                    $total1 = $time_diff->format('%H:%I');
                    //End of Change
                } else {
                    $total1 = " <div class='tooltip-container'><i class='fa fa-info-circle' style='color:blue;'>
                    <span class='tooltip'>Time Out Missing</span>
                    </i></div>";
                }
                ?>
                <tr class="table-row">
                    <td><?php echo $attendance['employee_code']; ?></td>
                    <td><?php echo $attendance['full_name']; ?></td>
                    <td><?php echo $attendance['Abbreviation']; ?></td>
                    <td><?php echo $attendance['department_name']; ?></td>
                    <td><?php echo $attendance['la_lo']; ?></td>
                    <td><?php echo date("d-M-Y",strtotime($attendance['in_date'])); ?></td>
                    <td><?php echo ($attendance['in_time'] == '00:00:00') ? ' - ': date("H:i ",strtotime($attendance['in_time'])); ?></td>
                    <td><?php echo ($attendance['out_date'] == "0000-00-00") ? date("d-M-Y",strtotime($attendance['in_date'])) : date("d-M-Y",strtotime($attendance['out_date'])); ?></td>
                    <td><?php echo ($attendance['out_time'] == '00:00:00') ? ' - ': date("H:i ",strtotime($attendance['out_time'])); ?></td>
                    <td><?php echo @$attendance[0]; ?></td>
                    <td><?php echo $total1; ?></td>
                    <td><?php
                        $type = $attendance['status_type'];
                        if ($type == 'Absent') {
                            echo '<span style="color:#ff0d00;">' . $type . '</span>';
                        } else if ($type == 'Present') {
                            echo '<span style="color:#00CC00;">' . $type . '</span>';
                        } else if ($type == 'On Leave') {
                            echo '<span style="color:#0c12ff;">' . $type . '</span>';
                        } else {
                            echo '<span style="color:#cd6e00;">' . $type . '</span>';
                        }
                        ?></td>
                    <?php /*?>    <td><a href="leave_attendance/edit_leave_staff/<?php echo $leave_staff->employee_id?>"> <span class="fa fa-pencil"></span></a></td>
                    <td><a href="leave_attendance/print_leave_status/<?php echo $leave_staff->employee_id?>"><span class="fa fa-print" onclick="print_leave_status" style="cursor:pointer"></span></a
                    ><?php */
                    ?></tr>
                <tr>
                    <?php }
                    } else { ?>
                        <td colspan="10"><?php echo "<p style='color:red'>Sorry No Data Available</p>" ?></td>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="pagination">
            <ul>
                <?php echo $links; ?>
            </ul>
        </div>
    </div>

</div>
</div>
<!-- Gender dialog -->
<div id="viewImportFromFile" title="Import From File" style="display:none; width:600px;">
    <form action="<?php echo base_url(); ?>leave_attendance/importAttendanceFromFile" method="post" enctype="multipart/form-data">
        <div class="data row" >
            <div class="col-xs-12" style="width:100%;">

                    <input type="text" style="height: 30px; width:100%;margin-bottom: 5%"class="form-cotroll" id="startImportingDate" placeholder="Select Start Date" name="startImportingDate">
                    <input type="hidden" id="altStartImportingDate" name="altStartImportingDate" style="display: none;">

            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="text" style="height: 30px; width:100%;margin-bottom: 5%" id="endImportingDate" placeholder="Select End Date" name="endImportingDate">
                    <input type="hidden" id="altEndImportingDate" name="altEndImportingDate" style="display: none;">

                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="file" style="height: 30px; width:100%;margin-bottom: 5%"class="fileInput" id="importAttendanceFromFile" name="importAttendanceFromFile"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Import" id="importAttendanceFromFileButton" class="btn green addedto" name="add">

                </div>
            </div>
        </div>
    </form>
</div>


<!-- Modal -->
<div hidden class="modal fade" id="loginmodal" id="modaltrigger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="header">
        <div class="modal-content">
            <div class="modal-header-self">
                <!--<button type="button" class="close" id="close-hover" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 id="head_style">Email To:</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="mailForm">
                    <div class="self-row">
                        <label for="inputEmail3" class="Label-left">To</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="ToEmail" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="inputPassword3" class="Label-left">Reply To:</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="FromEmail" placeholder="Reply To Email (optional)">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="inputPassword3" class="Label-left">Subject:</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" value="Attached Generated Payroll PDF" name="MessageSubject">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="inputPassword3" class="Label-left">Message:</label>
                        <div class="right-sec">
                            <textarea  class="textarea-self" name="MessageBody" id="" cols="30" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="self-row-attac">
                        <label for="inputPassword3" class="Label-left">Attachment:</label>
                        <div class="right-sec">
                            <label  class="label-attac">work.pdf</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="sendMailWithAttachment" class="btn btn-primary send">Send</button>
                <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    </div>

<!---->
<style>
    .modal-footer{float: right;
        width: auto;
        height: auto;
        margin-right: 20px;
        margin-top: 5px;
        margin-bottom: 10px;}
</style>
<!-----------------------------------------end models----------------------------->

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
    $(document).ready(function () {


        //Function To Update Attendance Of All Employees .
        $('#AttendanceButton').on('click', function (e) {
            $.ajax({
                url: "<?php echo base_url(); ?>bio_auth/attendance",
                type: "POST",
                success:function(output){
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });

        //Function To SignOut All Employees if The Particular Button Is Pressed.
        $('#signOutAllEmployees').on('click', function (e) {
            $.ajax({
                url: "<?php echo base_url(); ?>leave_attendance/sign_out_all_employees",
                type: "POST",
                success:function(output){
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });
//Need To Have A Function to Import Attendance From File Also..


        $( "#viewImportFromFile" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $( "#importFromFile" ).click(function() {
            $("#viewImportFromFile").dialog( "open" );
        });
        $('#importAttendanceFromFileButton').on('click', function (e) {
            e.preventDefault();
            var importFromDate = $('#altStartImportingDate').val();
            var importToDate = $('#altEndImportingDate').val();
            if(importFromDate.length == 0 || importToDate.length == 0){
                Parexons.notification('You Must Select Both Dates For The Import Function To Work.','warning');
                return;
            }
            var file = $("#importAttendanceFromFile")[0].files[0];
            var formData = new FormData();
            formData.append('ImportFromDate', importFromDate);
            formData.append('ImportToDate', importToDate);
            formData.append('file', $('input[type="file"]')[0].files[0]);
            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>leave_attendance/importAttendanceFromFile',
                data:formData,
                processData: false,
                contentType: false,
                success: function(output){
                    var data = output.split("::");
                    if (data[0] == "OK") {
                        Parexons.notification(data[1], data[2]);
                    }
                    else if(data[0]=="FAIL") {
                        Parexons.notification(data[1], data[2]);
                    }
                }
            });
        });

        $("#startImportingDate").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "d M, yy",
            altField:"#altStartImportingDate",
            altFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#endImportingDate" ).datepicker( "option", "minDate", selectedDate );
            }
        });

        $("#endImportingDate").datepicker({
            changeMonth: true,
            changeYear:true,
            dateFormat: "d M, yy",
            altField:"#altEndImportingDate",
            altFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#startImportingDate" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });

    function doc_file() {
        var r = confirm('if you are upload this file again !\n\ please follow check out method');
        if (r == true) {
            return true;
        } else if (r == false) {
            return false;
        }
    }
    function print_attendence(id) {
        window.open('leave_attendance/print_attendence/' + id, "", "width=800,height=600");
    }
    function print_all_attendance(id) {
        var id = '<?php echo isset($employee_id) ? $employee_id: ""; ?>';
        window.open('leave_attendance/print_all_attendance/' + id, "", "width=800,height=600");
    }

    $(function () {
        $("#in").datepicker({dateFormat: "yy-mm-dd", maxDate: 0});
    });
    function lookup(inputString) {

        if (inputString.length == 0) {
            $('#suggestions').hide();
            $('#employee_inf0').hide();
            location.reload();
        } else {
            $.post("leave_attendance/autocomplete_attendence_name/", {queryString: "" + inputString + ""}, function (data) {
                if (data.length > 0) {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').show();
                    $('#autoSuggestionsList').html(data);
                }
            });
        }
    }
    function clear_div() {
        document.getElementById('#employee_inf0').innerHTML = "";
    }

    function fill(thisValue, employee_id) {
        $('#input_id').val(thisValue);
        $('#emp_id').val(employee_id);
        //$('#full_name').append(full_name);
        setTimeout("$('#suggestions').hide();", 200);
        //$('#suggestions').hide();
        $('#employee_inf0').show();

        //setTimeout("$('#autoSuggestionsList').hide()", 200);
    }

</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#assign,#status,#department').select2();
    })


    </script>
    <!-- leftside menu end -->
<script>
    $(document).ready(function(){
        $("#pdfButton").on('click', function(){
            var targetURL = '<?php echo base_url();?>leave_attendance/downloadPdfAttendanceReport';
            <?php if(isset($view) && !empty($view)){ ?>
            var postData = [];
            postData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            console.log(postData);
            $.ajax({
                url: targetURL,
                data:postData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "Download"){
                        //console.log(data[1]);
                        //document.location = <?php //echo base_url();?>data[1];
                    }
                }
            });
            <?php } ?>
        });

        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($view) && !empty($view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>leave_attendance/SendPDFAttendanceReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>

<script>
    $("#in_date").datepicker({dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear:true,
        yearRange:"<?php echo yearRang(); ?>"

    });
    $("#out_date").datepicker({dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear:true,
        yearRange:"<?php echo yearRang(); ?>"

    });


</script>

<!---------------Model Javascript--------------------------->
<script type="text/javascript">
    $(function(){
        $('#loginform').submit(function(e){
            return false;
        });

        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".close" });
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".btn-default" });
    });
</script>
<!--------------------------End Model--------------------------->
<style>
    .col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{float:left}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}.col-xs-pull-12{right:100%}.col-xs-pull-11{right:91.66666667%}.col-xs-pull-10{right:83.33333333%}.col-xs-pull-9{right:75%}.col-xs-pull-8{right:66.66666667%}.col-xs-pull-7{right:58.33333333%}.col-xs-pull-6{right:50%}.col-xs-pull-5{right:41.66666667%}.col-xs-pull-4{right:33.33333333%}.col-xs-pull-3{right:25%}.col-xs-pull-2{right:16.66666667%}.col-xs-pull-1{right:8.33333333%}.col-xs-pull-0{right:auto}.col-xs-push-12{left:100%}.col-xs-push-11{left:91.66666667%}.col-xs-push-10{left:83.33333333%}.col-xs-push-9{left:75%}.col-xs-push-8{left:66.66666667%}.col-xs-push-7{left:58.33333333%}.col-xs-push-6{left:50%}.col-xs-push-5{left:41.66666667%}.col-xs-push-4{left:33.33333333%}.col-xs-push-3{left:25%}.col-xs-push-2{left:16.66666667%}.col-xs-push-1{left:8.33333333%}.col-xs-push-0{left:auto}.col-xs-offset-12{margin-left:100%}.col-xs-offset-11{margin-left:91.66666667%}.col-xs-offset-10{margin-left:83.33333333%}.col-xs-offset-9{margin-left:75%}.col-xs-offset-8{margin-left:66.66666667%}.col-xs-offset-7{margin-left:58.33333333%}.col-xs-offset-6{margin-left:50%}.col-xs-offset-5{margin-left:41.66666667%}.col-xs-offset-4{margin-left:33.33333333%}.col-xs-offset-3{margin-left:25%}.col-xs-offset-2{margin-left:16.66666667%}.col-xs-offset-1{margin-left:8.33333333%}.col-xs-offset-0{margin-left:0}@media (min-width:768px){.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-11{width:91.66666667%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-8{width:66.66666667%}.col-sm-7{width:58.33333333%}.col-sm-6{width:50%}.col-sm-5{width:41.66666667%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}.col-sm-1{width:8.33333333%}.col-sm-pull-12{right:100%}.col-sm-pull-11{right:91.66666667%}.col-sm-pull-10{right:83.33333333%}.col-sm-pull-9{right:75%}.col-sm-pull-8{right:66.66666667%}.col-sm-pull-7{right:58.33333333%}.col-sm-pull-6{right:50%}.col-sm-pull-5{right:41.66666667%}.col-sm-pull-4{right:33.33333333%}.col-sm-pull-3{right:25%}.col-sm-pull-2{right:16.66666667%}.col-sm-pull-1{right:8.33333333%}.col-sm-pull-0{right:auto}.col-sm-push-12{left:100%}.col-sm-push-11{left:91.66666667%}.col-sm-push-10{left:83.33333333%}.col-sm-push-9{left:75%}.col-sm-push-8{left:66.66666667%}.col-sm-push-7{left:58.33333333%}.col-sm-push-6{left:50%}.col-sm-push-5{left:41.66666667%}.col-sm-push-4{left:33.33333333%}.col-sm-push-3{left:25%}.col-sm-push-2{left:16.66666667%}.col-sm-push-1{left:8.33333333%}.col-sm-push-0{left:auto}.col-sm-offset-12{margin-left:100%}.col-sm-offset-11{margin-left:91.66666667%}.col-sm-offset-10{margin-left:83.33333333%}.col-sm-offset-9{margin-left:75%}.col-sm-offset-8{margin-left:66.66666667%}.col-sm-offset-7{margin-left:58.33333333%}.col-sm-offset-6{margin-left:50%}.col-sm-offset-5{margin-left:41.66666667%}.col-sm-offset-4{margin-left:33.33333333%}.col-sm-offset-3{margin-left:25%}.col-sm-offset-2{margin-left:16.66666667%}.col-sm-offset-1{margin-left:8.33333333%}.col-sm-offset-0{margin-left:0}}@media (min-width:992px){.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9{float:left}.col-md-12{width:100%}.col-md-11{width:91.66666667%}.col-md-10{width:83.33333333%}.col-md-9{width:75%}.col-md-8{width:66.66666667%}.col-md-7{width:58.33333333%}.col-md-6{width:50%}.col-md-5{width:41.66666667%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}.col-md-1{width:8.33333333%}.col-md-pull-12{right:100%}.col-md-pull-11{right:91.66666667%}.col-md-pull-10{right:83.33333333%}.col-md-pull-9{right:75%}.col-md-pull-8{right:66.66666667%}.col-md-pull-7{right:58.33333333%}.col-md-pull-6{right:50%}.col-md-pull-5{right:41.66666667%}.col-md-pull-4{right:33.33333333%}.col-md-pull-3{right:25%}.col-md-pull-2{right:16.66666667%}.col-md-pull-1{right:8.33333333%}.col-md-pull-0{right:auto}.col-md-push-12{left:100%}.col-md-push-11{left:91.66666667%}.col-md-push-10{left:83.33333333%}.col-md-push-9{left:75%}.col-md-push-8{left:66.66666667%}.col-md-push-7{left:58.33333333%}.col-md-push-6{left:50%}.col-md-push-5{left:41.66666667%}.col-md-push-4{left:33.33333333%}.col-md-push-3{left:25%}.col-md-push-2{left:16.66666667%}.col-md-push-1{left:8.33333333%}.col-md-push-0{left:auto}.col-md-offset-12{margin-left:100%}.col-md-offset-11{margin-left:91.66666667%}.col-md-offset-10{margin-left:83.33333333%}.col-md-offset-9{margin-left:75%}.col-md-offset-8{margin-left:66.66666667%}.col-md-offset-7{margin-left:58.33333333%}.col-md-offset-6{margin-left:50%}.col-md-offset-5{margin-left:41.66666667%}.col-md-offset-4{margin-left:33.33333333%}.col-md-offset-3{margin-left:25%}.col-md-offset-2{margin-left:16.66666667%}.col-md-offset-1{margin-left:8.33333333%}.col-md-offset-0{margin-left:0}}@media (min-width:1200px){.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9{float:left}.col-lg-12{width:100%}.col-lg-11{width:91.66666667%}.col-lg-10{width:83.33333333%}.col-lg-9{width:75%}.col-lg-8{width:66.66666667%}.col-lg-7{width:58.33333333%}.col-lg-6{width:50%}.col-lg-5{width:41.66666667%}.col-lg-4{width:33.33333333%}.col-lg-3{width:25%}.col-lg-2{width:16.66666667%}.col-lg-1{width:8.33333333%}.col-lg-pull-12{right:100%}.col-lg-pull-11{right:91.66666667%}.col-lg-pull-10{right:83.33333333%}.col-lg-pull-9{right:75%}.col-lg-pull-8{right:66.66666667%}.col-lg-pull-7{right:58.33333333%}.col-lg-pull-6{right:50%}.col-lg-pull-5{right:41.66666667%}.col-lg-pull-4{right:33.33333333%}.col-lg-pull-3{right:25%}.col-lg-pull-2{right:16.66666667%}.col-lg-pull-1{right:8.33333333%}.col-lg-pull-0{right:auto}.col-lg-push-12{left:100%}.col-lg-push-11{left:91.66666667%}.col-lg-push-10{left:83.33333333%}.col-lg-push-9{left:75%}.col-lg-push-8{left:66.66666667%}.col-lg-push-7{left:58.33333333%}.col-lg-push-6{left:50%}.col-lg-push-5{left:41.66666667%}.col-lg-push-4{left:33.33333333%}.col-lg-push-3{left:25%}.col-lg-push-2{left:16.66666667%}.col-lg-push-1{left:8.33333333%}.col-lg-push-0{left:auto}.col-lg-offset-12{margin-left:100%}.col-lg-offset-11{margin-left:91.66666667%}.col-lg-offset-10{margin-left:83.33333333%}.col-lg-offset-9{margin-left:75%}.col-lg-offset-8{margin-left:66.66666667%}.col-lg-offset-7{margin-left:58.33333333%}.col-lg-offset-6{margin-left:50%}.col-lg-offset-5{margin-left:41.66666667%}.col-lg-offset-4{margin-left:33.33333333%}.col-lg-offset-3{margin-left:25%}.col-lg-offset-2{margin-left:16.66666667%}.col-lg-offset-1{margin-left:8.33333333%}.col-lg-offset-0{margin-left:0}}
    @media (max-width: 1171px){
        input[type="text"]{
            width: 100% !important;
        }
        form .row input[type="text"]{
            width: 100% !important;

        }

    }

    @media (min-width: 800px) and (max-width: 940px){
        input[type="text"]{
            width: 100% !important;
        }


    }


    @media (min-width: 520px) and (max-width: 799px){
        input[type="text"]{
            width: 100% !important;
        }
    }


    @media (max-width: 519px){
        input[type="text"]{
            width: 100% !important;
        }
        form .row input[type="text"]{
            width: 100% !important;

        }
    }


    @media (max-width: 463px){
        form .row input[type="text"]{
            width: 100% !important;

        }

    }
</style>