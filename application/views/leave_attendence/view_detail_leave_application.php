<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none ;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 48%;
	top: 60px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -89.3%;
	top: 20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -60px;
	left: -65px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}

</style>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Employee Leave Application Detail</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"leave_type","value":$('#type').val()});
                
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
</script>
	<div class="right-contents">

		<div class="head">Employee Leave Application Detail</div>
		<table class="table">
			<tbody>
				<tr class="table-row">
					<td>Employee ID:</td>
					<td><?php echo @$complete_profile->employee_code;?></td>
				</tr>
				<tr class="table-row">
					<td>Name:</td>
					<td><?php echo @$complete_profile->full_name;?></td>
				</tr>
				<tr class="table-row">
					<td>Leave Type:</td>
					<td><?php echo @$complete_profile->leave_type;?></td>
				</tr>

				<tr class="table-row">
					<td>From Date:</td>
					<td><?php echo @date_format_helper($complete_profile->from_date)?></td>
				</tr>
				<tr class="table-row">
					<td>To Date:</td>
					<td><?php echo @date_format_helper($complete_profile->to_date)?></td>

				</tr>

                    <tr class="table-row">
                        <td>Total Days:</td>
                        <td><?php if(!empty($complete_profile->total_days))  {echo $complete_profile->total_days;} else {echo "";}?></td>
                    </tr>


				<tr class="table-row">
					<td><?php if($complete_profile->status== 1){ echo"";}elseif($complete_profile->status== 2){echo "Approved Date:";}else{ echo"Declined Date:";} ?></td>
					<td><?php  echo @date_format_helper($complete_profile->approval_date);?></td>
				</tr>
				<tr class="table-row">
					<td><?php if($complete_profile->status== 1){ echo"";} elseif($complete_profile->status== 2){echo "Approved By:";}else{ echo"Declined By:";} ?></td>
					<td><?php echo @$complete_profile->name;?></td>
				</tr>
				<tr class="table-row">
					<td>Remarks:</td>
					<td><?php echo @$complete_profile->note;?> </td>
				</tr>
			</tbody>
		</table>
</div>


            </div>
            </div>

<script type="text/javascript">


	var emp_name=$("#name").val();
	$("#employee").append(emp_name);
	var emp_name=$("#code").val();
	$("#id").append(emp_name);
</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->