

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Users Management / Edit User</div> <!-- bredcrumb -->

	<div class="right-contents full">

	

		<div class="head">Update Staff On Leave</div>
			
			<form action="" method="post">
				<div class="row">
					<h4>Leave Type</h4>
                     <?php 
					 $selected = (isset($leave) ? $leave : '');
					 echo form_dropdown('leave_type',$leave_type, $selected); ?>
				<!--<select name="user_group">
						<option>Admin</option>
						<option>HRM</option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employee CODE</h4>
					<input type="text" name="employee_code" value="<?php echo $employee->employee_code;?>" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employee Name</h4>
					<input name="full_name" type="text" value="<?php echo $employee->full_name;?>" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row">
					<h4>From Date</h4>
					<input name="from_date" type="text" value="<?php echo $employee->from_date;?>">
				</div>
                <br class="clear">
				<div class="row">
					<h4>To Date</h4>
					<input name="to_date" type="text" value="<?php echo $employee->to_date;?>">
				</div>
                   <br class="clear">
				<div class="row">
					<h4>Contact Number</h4>
					<input name="mobile_num" type="text" value="<?php echo $employee->mobile_num;?>">
				<br class="clear">
				<div class="row1">
					<h4>Status</h4>
					<select name="status">
						<option value="1">Enable</option>
						<option value="0">Disable</option>
					</select>
				</div>
				<br class="clear">
				
		

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="add_employee_personal_info.php"><button class="btn green">Add</button></a>-->
                    <input type="submit" name="edit" value="Update" class="btn green">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

				</form>

		</div>

	</div>
	</div>
<!-- contents -->
