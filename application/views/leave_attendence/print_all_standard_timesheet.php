<base href="<?=base_url()?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var a=confirm("Are you sure want to Print");
	if(a==true)
	{
		window.print() ;
	}
});
</script>

<style type="text/css">
    body{
        background: #f8f8f8;
    }
    table th{
        font-size: 12px;
        font-weight:bold;
    }
    table td{
        font-size: 11px;
    }
    .shadow h5{
        display:inline-block;
        margin-left: 4em;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">

            <!-- print for present-->
            <div class="printing">
                <table class="table table-striped table-bordered table-condensed">
				<thead>
					<th>Employee ID</th>
					<th>Employee Name</th>
					<th>Month</th>
                    <th>Year</th>
                    <th>Work Hours</th>
                    <th>Leave Hours</th>
                    <th>Total Hours</th>
				</thead>
                    <?php if(!empty($present)) { ?>
                    <?php foreach($present as $presents):
					//echo "<pre>"; print_r($present); die;
					?>     
                <tr class="table-row">
                    <td class="no-apply"><?php echo $presents->employee_code;?></td>
					<td class="no-apply"><?php echo $presents->full_name;?></td>
					<td class="no-apply"><?php echo $presents->month;?></td>
					<td class="no-apply"><?php echo $presents->year;?></td>
					<td class="no-apply"><?php echo $presents->work_hours;?></td>
					<td class="no-apply"><?php echo $presents->leave_hours;?></td>
					<td class="no-apply"><?php echo $presents->monthly_std_hrs;?></td>
				</tr>

            <?php endforeach;}else{?>
						<td colspan="10"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
					<?php }?>
			</table>
			</div>
