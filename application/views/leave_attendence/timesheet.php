<?php if(!empty($time_sheet)){ foreach($time_sheet as $rec)
{$emp_name=$rec->full_name;}}else{ $emp_name="";}
?>
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
<!-- contents -->

<div class="contents-container">




 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $("#leav_add").hide();
    $("#hr_add").hide();
   
	

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,

        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"leave_type","value":$('#type').val()});
                
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_timesheet(id)
{
	window.open('leave_attendance/print_timesheet/'+id,"","width=800,height=600");
}
function print_all_record(id)
{
	window.open('leave_attendance/print_all_record/'+id,"","width=800,height=600");
}

function lookup(inputString) {
	
    if(inputString.length == 0) {
        $('#suggestions').hide();
		 $('#employee_inf0').hide();
	       location.reload();
    } else {
        $.post("leave_attendance/autocomplete_leave_enetitlement/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_id) {
    $('#id_input').val(thisValue);
	$('#emp_id').val(employee_id);
	setTimeout("$('#suggestions').hide();", 200);
      //$('#suggestions').hide();
   $('#employee_inf0').show();
 
	//setTimeout("$('#autoSuggestionsList').hide()", 200);
}
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Attendance Based Timesheet</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Attendance Based Timesheet</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
<div class="row2">
				<h4>Employee Name</h4>
				<input type="text" name="full_name" id="id_input" placeholder="Search Here"  autocomplete="off" onkeyup="lookup(this.value)">
				<input type="hidden" name="emp_id" id="emp_id" />
				<div id="suggestions">
					<div class="autoSuggestionsList_l" id="autoSuggestionsList">
					</div>
				</div>
</div>
			  <br class="clear">
				<div class="row">
					<h4>Month</h4>
                    <select name="month" class="row" required="required" id="month">
                     <option value="-1">-- Select month --</option>
                     <?php foreach($month as $mon){?>
                     <option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                     
                     <?php }?>
                     </select>
                     
				</div>
                <br class="clear">
				<div class="row">
					<h4>Year</h4>
                     <?php //  echo @form_dropdown('year',$year);?>
                     <select name="year" class="row" required id="year">
                     <option value="-1">-- Select Year --</option>
                     <?php foreach($year as $yer){?>
                     <option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                     <?php }?>
                     </select>
                    
				</div>
				<div class="button-group">
					<button class="btn green">Search</button>
					<!--<button class="btn gray">Reset</button>-->
				</div>
                <?php echo form_close();?>
			</div>
          <div class="row">
          <span id="emp_name"></span>
				<h4><?php echo @$emp_name; ?></h4>
			</div>

			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td>Project Name</td>
					<td>In Date</td>
                    <td>In Time</td>
					<td>Out Date</td>
                    <td>Out Time</td>
					<td>Total Hours</td>
                    <td>Status</td>
				</thead>
			       <?php if(!empty($time_sheet)){
					         $total = 0;
					foreach($time_sheet as $time_sheets){
						$count=count($time_sheet);
					$timestart = $time_sheets->in_time;
					$timestop = $time_sheets->out_time;

                        //Change It
                    $time1 = new DateTime($timestart);
					$time2 = new DateTime($timestop);
					$time_diff = $time1->diff($time2);

                    $total1 = $time_diff->format('%H:%I:%s');
                        //End of Change

					if ($total1 < 0) {
						$total1 += 24;
					}
						//print_r($final);
					//echo gmdate("h:i:s",$final);
				$emp_id=$time_sheets->employee_id;
				$month_id=$time_sheets->ml_month_id;
				$year_id=$time_sheets->ml_year_id;
				$emp_name=$time_sheets->full_name;
				$date1 = $time_sheets->in_date;
				?>
                    <tbody>
                    <td><?php echo $time_sheets->project_title;?></td>
                    <td><?php echo date_format_helper($time_sheets->in_date);?></td>
                    <td><?php echo date("h:i:s",strtotime($time_sheets->in_time))?></td>
                    <td><?php echo date_format_helper($time_sheets->Out_date);?></td>
                    <td><?php echo date("h:i:s",strtotime($time_sheets->out_time))?></td>
                    <td><?php echo $total1;?></td>
                    <?php  $total +=  $total1;?>

                     <td><?php echo $time_sheets->status_type;?></td>
                    <input type="hidden" name="name" id="emp_name" value="<?php echo $time_sheets->full_name; ?>" />
                    
                    <input type="hidden" name="name" id="emp_name" value="<?php echo $time_sheets->in_date; ?>" />
					<script>
                    	
                    </script>
                    </tbody>
                    <?php $sum = $total;}}else{?>
                    <td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
                    <?php }?>
                    <tr style="background:#333; color:#FFF;">
                	<td><span style="float:right; font-weight:bold;">Total Hours:</span></td>
                	<td><?php echo @$sum; ?></td>
                    </tr>
                    
			</table>
            
            <br class="clear" />
            
            <form action="leave_attendance/add_leave_earned" method="post" >
                
                    <input type="hidden" name="emp_id" id="emp_id" value="" />
                    <input type="hidden" name="leave_entitle" id="leave_id" value="<?php echo @$emp_id; ?>" />
                    <input type="hidden" name="leave_month" id="leave_id" value="<?php echo @$months; ?>" /> 
                    <input type="hidden" name="leave_year" id="leave_id" value="<?php echo @$years; ?>" />
                    <input type="hidden" name="indate" id="indate" value="<?php echo @$date1; ?>" />  
                     <br class="clear" />
                    <br class="clear" />
                   <?php /*?> <?php echo @form_dropdown('leave_type',$leave_type," id='type' onChange='this.form.submit() style='width:auto;'");?><?php */?>
                    <br class="clear" />
                    <br class="clear" />
					<h4>Leave Earned</h4>
					<input type="text" placeholder="Enter Earned leave...." style="width:auto" id="leave_earned" name="no_of_leaves" onkeyup="leave_submit()"> 
                     <input type="submit" class="btn green"  value="Submit" name="leav_add" id="leav_add" />
                     
                    <br class="clear" />
                    <br class="clear" />
                    </form>
                    <div>
                    <form action="leave_attendance/add_over_time" method="post">
                    <h4>Over Time</h4>
                     <input type="hidden" name="hr_emp_id" id="hr_emp_id" value="<?php echo @$emp_id; ?>" />
                    <input type="hidden" name="hr_month" id="hr_month" value="<?php echo @$months;?>" /> 
                    <input type="hidden" name="hr_year" id="hr_year" value="<?php echo @$years; ?>" />
                    <input type="hidden" name="indate" id="indate" value="<?php echo @$date1; ?>" /> 
					<input type="text" placeholder="Enter hours...." style="width:auto" id="sanction_hours" name="sanction_hours" onkeyup="hr_submit()" /> 
				    <input type="submit" class="btn green"  value="Submit" name="hr_add" id="hr_add" />
                <div class="row2">
				<div class="button-group">
                       
				</div>
			</div>
                </form>
				</div>
			</div>
		</div>


<!-- contents -->
<script type="text/javascript">
function leave_submit()
{	var earned= $("#leave_earned").val();
	if(earned.length > 0)
	{$("#leav_add").show();}
	else
	{$("#leav_add").hide();}
	}
	
function hr_submit()
{
	var earned= $("#sanction_hours").val();
	if(earned.length > 0)
	{$("#hr_add").show();}
	else
	{$("#hr_add").hide();}
}

//var emp_name=$("#name").val();
//$("#employee").append(emp_name);

   
/// Ajax Function
/*function get_emp_name()
{*/
	$(document).ready(function()
	{
		var emp_name=$("#emp_name").val();
		$("#emp").append(emp_name);
		//alert(emp_name);
		var emp = $('#emp_name123').val();
		//alert(emp);
		$.ajax({
			type: "POST",
			url: "leave_attendance/emp_name",
			data: {emp:emp},
			success: function(data){
				alert(data);
			$('#employee').html(data);	
			}
		});
	});
	
/*}*/
</script>


<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#month,#year').select2();
    });



    </script>
    <!-- leftside menu end -->
    <script>
    //////////// Notification Message ////////////////
    $(document).ready(function() {
    <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                echo "message = '".$message."';"; ?>
                var data = message.split("::");
                Parexons.notification(data[0], data[1]);
            <?php
            }
        }
    }

    ?>
    });
    </script>