<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<style>
    /*modal CSS*/
    .modal-demo {
        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
</style>
<?php
$loggedInEmployee = $this->session->userdata('employee_id');
if(isset($monthYear) && !empty($monthYear)){
    $Year = $monthYear[1];
    $Month = $monthYear[0];
    $MonthNumber =  date('m', strtotime($Month));
    $date = $Year.'-'.$MonthNumber.'-01';
    $end  = $Year.'-'.$MonthNumber.'-' . date('t', strtotime($date));
}else{
    $date = date('Y-m-01');
    $end = date('Y-m-') . date('t', strtotime($date));
}
$monthYear = explode('-',$date);
?>
<?php
$projectData = array(
    'hoursWorkedInProject' => array()
);
$timeSheetData = array(
    'Date' => array(),
    'Day' => array()
);
if(isset($TimeSheetViewData) && !empty($TimeSheetViewData)){
    $totalProjectsInMonth = array_count_values(array_column(json_decode(json_encode($TimeSheetViewData), true), 'ProjectID'));
    while (strtotime($date) <= strtotime($end)) {
        $day_num = date('d', strtotime($date));
        $day_name = date('l', strtotime($date));
        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        foreach ($TimeSheetViewData as $key => $value) {
            if (date('d', strtotime($value->WorkDate)) === $day_num) {
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = $value->WorkHours;
                if(!array_key_exists('Total',$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] = 00;
                }
                $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]['Total'] += intval($value->WorkHours);
            }else{
//            echo '<br />Date Not Matching'.$day_num;
                if(!array_key_exists( $value->ProjectID.'::'.$value->ProjectTitle , $timeSheetData )){
                    $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle] = array();
                }
                if(isset($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                    if(!array_key_exists($monthYear[0].'-'.$monthYear[1].'-'.$day_num,$timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle])){
                        $timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle][$monthYear[0].'-'.$monthYear[1].'-'.$day_num] = '00';
                    }
                }
            }
            ksort($timeSheetData[$value->ProjectID.'::'.$value->ProjectTitle]);
        }
        array_push($timeSheetData['Date'], $day_num);
        array_push($timeSheetData['Day'], $day_name);
    }
//Need to Add the Total Column in the End.
    $timeSheetData['Date']['Total'] = 'Total Hours Worked';
}else{
    $TimeSheetViewData = 'No Projects Assigned to You, So TimeSheet Can Not Be Enabled for You.';
}

//Now Need to Show the Employee Leaves In Time Sheet.
if(!empty($employeeTotalLeaves) && isset($employeeTotalLeaves)){
    $leaves_array = array_filter($employeeTotalLeaves, function($val){
        return (!empty($val->LeaveFrom) && !empty($val->LeaveTo));
    });
    foreach($leaves_array as $leave) {
        $range = array();
        foreach ($timeSheetData as $tsKey => $value) {
            if($tsKey != 'Day' && $tsKey != 'Date'){
                foreach($value as $vDates=>$vHours){
                    //Now Change the Index
                    try {
                        $new_date = new DateTime($vDates);
                        $from = new DateTime($leave->LeaveFrom);
                        $to = new DateTime($leave->LeaveTo);
                        if($new_date >= $from && $new_date <= $to) {
                            $range[] = $vDates . '-'.(empty($leave->HexColor) ? rand_color(): $leave->HexColor);
                            unset($timeSheetData[$tsKey][$vDates]);
                        }
                    } catch(Exception $e) {
                    }
                }
                // changes
                foreach($range as $range_date) {
                    if(!array_key_exists( $range_date , $timeSheetData[$tsKey])){
                        $timeSheetData[$tsKey][$range_date] = '00';
                    }
                }
                // resort
                ksort($timeSheetData[$tsKey]);
            }
        }
    }
}

if(isset($HolidaysData) && !empty($HolidaysData)){
    //Clear Any Empty Data or Inconsistent ones.
    $holidays = array_filter($HolidaysData, function($val){
        return (!empty($val->from_date) && !empty($val->to_date));
    });
    //Need To Work for Holidays.
    foreach($holidays as $holiday){
        $range = array();
        foreach ($timeSheetData as $tsKey => $value) {
            if($tsKey != 'Day' && $tsKey != 'Date'){
                foreach($value as $vDates=>$vHours){
                    //Now Change the Index
                    try {
                        $new_date = new DateTime($vDates);
                        $from = new DateTime($holiday->from_date);
                        $to = new DateTime($holiday->to_date);
                        if($new_date >= $from && $new_date <= $to) {
                            $range[] = $vDates . '-#dd2200';
                            unset($timeSheetData[$tsKey][$vDates]);
                        }
                    } catch(Exception $e) {
                    }
                }
                // changes
                foreach($range as $range_date) {
                    if(!array_key_exists( $range_date , $timeSheetData[$tsKey])){
                        $timeSheetData[$tsKey][$range_date] = '00';
                    }
                }
                // resort
                ksort($timeSheetData[$tsKey]);
            }
        }
    }
}

?>
<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	/*margin-top:8px;*/
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}
table {
    table-layout: fixed;
    width: 150%;
    *margin-left: -100px;/*ie7*/
}
td, th {
    vertical-align: top;
}
td.columnfirst, th.columnfirst {
    width: 100px;
    font-size:14px;
    font-weight: bold;
}
td.inner{
    font-size: 12pt;
    width: 20px;
}
.outer {position:relative;}
div.inner {
    overflow-x:scroll;
    overflow-y:visible;
}
.inputCopyCat{
    -moz-appearance: textfield;
    -webkit-appearance: textfield;
    background-color: white;
    background-color: -moz-field;
    border: 1px solid darkgray;
    box-shadow: 1px 1px 1px 0 lightgray inset;
    font: -moz-field;
    font: -webkit-small-control;
    margin-top: 5px;
    padding: 2px 3px;
    width: 398px;
}
.select2-results li{
    width: 300px;
    border-bottom:thin solid #ccc;
    height:60px;
}
.select2TemplateImg { padding:0.2em 0;}
.select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
.select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}

/*Custom User Info Box*/
.userInfoBox{
    box-shadow: 0 0 3px #666664;
    display: inline-block;
    margin-left: 20px;
    width: 400px;
}
.userInfoBox p{
    display: inline-block;
    min-width:150px;
}
</style>

<!--PLEASE Designer Dont Include Jquery inside View.. It Messes WIth other script Files. Jquery is already included in Head Section. No Need To Include Again.-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> -->
<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/css/bootstrap.css" type="text/css"/>-->
<script src="http://malsup.github.com/jquery.form.js"></script> 
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Leave&Attendance / Manual Time Sheet</div> <!-- bredcrumb -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">
        <div class="head">Mannual Timesheet</div>
        <div class="notice info">
            <p>Notes</p>
            <ul>
               <!-- TimeSheet Status -->
                <?php
                if(isset($TimeSheetViewData) && is_array($TimeSheetViewData) && !empty($TimeSheetViewData)){
                $TimeSheetStatus = $TimeSheetViewData[0]->TimeSheetStatus;
                if(is_numeric($TimeSheetStatus) && intval($TimeSheetStatus) === 0) {
                        $StatusMessage = '<font color="red">Your Timesheet is Currently Not Submitted for Review.</font>';
                }
                elseif(is_numeric($TimeSheetStatus) && intval($TimeSheetStatus) === 1){
                    $StatusMessage = '<font color="red">Your Timesheet is Submitted and is in pending for Review.</font>';
                }
                elseif(is_numeric($TimeSheetStatus) && intval($TimeSheetStatus) === 2){
                    $StatusMessage = '<font color="red">Your Timesheet has been Approved.</font>';
                }
                elseif(is_numeric($TimeSheetStatus) && intval($TimeSheetStatus) === 3){
                    $StatusMessage = '<font color="red">Your Timesheet has been Declined, Please Review and Fix It.</font>';
                }
                    echo '<li>'.$StatusMessage.'</li>';
                 }
                else {?>
                <li ><span style="color:red">Timesheet Is Not Set.</span></li>
                <?php }?>
            </ul>
        </div>
<?php if(is_admin($loggedInEmployee)){?> 
         <div class="table-responsive">
          <table cellspacing="0" id="table_list" style="margin-left:0.7em; width:99%">
                <thead class="table-head">
                    <td colspan="6">Employee</td>
                </thead>
                <tbody>
                    <tr class="table-row">
                       <td>
                           <h4>Select Employee</h4>
                       </td>
                       <td><input type="hidden" style="float: left; width: 140px;" id="selectEmployee"></td>
                        <td></td>
                        <td><input type="button" value="Generate Current Month TimeSheet" onclick="createTimeSheet()" class="btn green"></td>
                    </tr>
                </tbody>
            </table>
        </div>
<!--    <input type="button" class="btn green relocate" onclick="createTimeSheet()" value="Generate Current Month TimeSheet">-->
<?php } ?>
        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:99%">
                <thead class="table-head">
                <tr>
                    <td>Filter</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                    <tr class="table-row">
                       <td><h4>Year</h4></td>
                       <td><input type="hidden" style="width: 140px;" id="selectYear"></td>
                       <td><h4 style="padding-left: 6.5em;padding-top: 0.5em;">Month</h4></td>
                       <td><input type="hidden" style="width: 140px;" id="selectMonth"></td>
                    </tr>
                </tbody>
            </table>
        </div>    
		<br class="clear">
        <div class="table-responsive">
            <table cellspacing="0" id="table_list" style=" float:left; margin-left:0.7em; width:99%">
                <thead class="table-head">
                    <td>Color Code</td>
                <td></td>
                </thead>
                <tbody>
                    <tr class="table-row">
                        <td>
                            <ul class="color-legend">
                                <li style = "background: none repeat scroll 0 0 #f90; color:#ffffff;" ><span>Sunday</span></li>
                                <li style="background: none repeat scroll 0 0 #dd2200; color:#ffffff;"><span>Holiday</span></li>
                                <?php
                                if(isset($LeaveTypes) && !empty($LeaveTypes)){
                                    foreach($LeaveTypes as $leaveType){
                                        echo "<li style='background: none repeat scroll 0 0 ".$leaveType->LeaveTypeColor."; color:#ffffff;'><span>".$leaveType->LeaveType."</span></li>";
                                    }
                                }
                                ?>
                            </ul>
                        </td>
                        <td><button type="button" id="PdfButton" class="sendbtn">
                                PDF
                            </button>
                            <!-- <button type="button" id="CsvButton" class="btn btn-default btn-sm pull-right">
                                 CSV
                             </button>-->
                            <style>
                                .sendbtn{
                                    margin-right: 5px;
                                    float:right;
                                    display: inline-block;
                                    padding: 6px 12px;
                                    margin-bottom: 0px;
                                    font-size: 14px;
                                    font-weight: normal;
                                    line-height: 1.42857;
                                    text-align: center;
                                    white-space: nowrap;
                                    vertical-align: middle;
                                    cursor: pointer;
                                    -moz-user-select: none;
                                    background-image: none;
                                    border: 1px solid transparent;
                                    border-radius: 4px;}

                            </style>
                            <button type="button" id="sendEmailButton" class="sendbtn" data-toggle="modal" data-target="#myModal">
                                Send As Email Attachment
                            </button></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="userInfoBox">
            <div class="row"><p>Name : </p><strong><?php echo $employeeData->EmployeeName; ?></strong></div>
            <div class="row"><p>Employee Code :</p><strong><?php echo $employeeData->EmployeeCode; ?></strong></div>
            <div class="row"><p>Timesheet Month :</p><strong> <?php
                    if(isset($filterInfo)){
                        echo $filterInfo[0]." ".$filterInfo[1];
                    }else{
                        echo date("F Y");
                    }

                    ?></strong></div>
<!--            <div class="row">
                <p>Location :</p>
                <strong>Swat</strong>
            </div>-->

        </div>

        <!--    Automated TimeSheet Code-->
        <?php
        function ifColor($dates,$key){
          $attributes = '';
            if($dates === 'Total' && $key === 'Date'){
                $attributes = "rowspan='2' style = 'width:100px;'";
            }
            if($key !== 'Date' && $key !== 'Day' && $dates !== 'Total'){
                $leaveType = explode('-',$dates);
                if(date('l', strtotime($leaveType[0].'-'.$leaveType[1].'-'.$leaveType[2])) === 'Sunday'){
                    $attributes = "style='background:none repeat scroll 0 0 #f90;color:#fff;'";
                }elseif(isset($leaveType[3]) && !empty($leaveType[3])){
                    $attributes = "style='background:none repeat scroll 0 0 ".$leaveType[3].";color:#fff;'";
                }
            }
            return $attributes;
        }

        if(is_array($TimeSheetViewData) && !is_string($TimeSheetViewData)){
            echo "<div class='outer'><div class='inner'><table class=\"table-responsive1\">";
            echo "<tbody style=\"width:150%\">";

            foreach ($timeSheetData as $key => $value) {
                if($key == 'Day' || $key == 'Date'){
                    echo "<tr style='background: none repeat scroll 0 0 #4d6684;color:#fff;'>";
                }else{
                    echo "<tr class='tableRow'>";
                }
                if(strpos($key,"::")){
                    $key = explode("::",$key);
                    echo "<td class=\"columnfirst\" data-project='".$key[0]."'>".$key[1]."</td>";
                }else{
                    echo "<td class=\"columnfirst\">".$key."</td>";
                }
                foreach ($value as $vKey=>$valueData){
                    if($vKey !== 'Total'){
                        echo "<td ". ifColor($vKey,$key) ." class=\"inner\" td-data='".$vKey."'>";
                    }else{
                        echo "<td ". ifColor($vKey,$key) ." class=\"totalTD\" td-data='".$vKey."'>";
                    }
                    if($key === 'Day'){
                        print_r(substr($valueData, 0, 2));
                    }else{
                        print_r($valueData);
                    }
                    echo "</td>";
                }
                echo "</tr>";
            }
            echo "</table></div></div>";
        }else{
            echo '<div class="messageBox">'.$TimeSheetViewData.'</div>';
        }
        ?>


        <table cellspacing="0" id="table_list" style="width:50%; float:left; margin-left:0.7em;">
            <thead class="table-head">
            <td colspan="5">Leave Details</td>
            </thead>
            <tr class="table-row">
                <th>Category</th>
                <th>Allocated</th>
                <th>Leaves Taken Yearly</th>
                <th>Leaves Taken Monthly</th>
                <th>Balance</th>
                <!-- <th><b>Total Hours</b></th> -->
            </tr>
            <?php
            //     Now Need to Populate Table of EmployeeLeaves
            foreach($employeeTotalMonthlyAndYearly as $LeaveTypes){
                echo '<tr class="table-row" style="text-align:center;">';
                echo '<td>'.$LeaveTypes->AvailableLeaveTypes.'</td><td>'.$LeaveTypes->TotalAllocatedLeaves.'</td><td>'.$LeaveTypes->YearTotal.'</td><td>'.$LeaveTypes->MonthTotal.'</td><td>'.$LeaveTypes->Balance.'</td>';
                //<td><b>88:00</b></td>
                echo '</tr>';
            }
            ?>
        </table>
        <div class="row">
            <input type="button" value="Submit For Review" id="submitForReview" class="btn green"/>
        </div>
        <table cellspacing="0" id="table_list" style="width:20%; float:left; margin-left:0.7em; display:none;">
            <thead class="table-head">
            <tr>
                <td colspan="2">Time Allocation</td>
            </tr>
            </thead>
            <tr class="table-row">
                <td>Name Of The Project</td>
                <td>%</td>
            </tr>
            <tr class="table-row">
                <td>Project 1</td>
                <td>4</td>
            </tr>
            <tr class="table-row">
                <td>Project 2</td>
                <td>12</td>
            </tr>
            <tr class="table-row">
                <td>Project 3</td>
                <td>43</td>
            </tr>
            <tr class="table-row">
                <td>Project 4</td>
                <td>11</td>
            </tr>
            <tr class="table-row">
                <td><b>Total Hours</b></td>
                <td><b>17</b></td>
            </tr>
        </table>
</div>


    <!----------------------------Send Email Without Bootstrap libraries----------------------------->
    <div id="sendEmail" class="modal-demo" style="display: none;">
        <div class="top_header">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only"></span>
            </button>
            <h3 class="title">Email To</h3>
        </div>
        <form class="form-horizontal" id="mailForm">

            <div class="pop_row">
                <div class="left_side">
                    <h5>To</h5>
                </div>
                <div class="right_side">
                    <input type="email" class="right_txt" name="ToEmail" id="inputEmail3" placeholder="Email" style="width:240px;height:35px;">
                </div>
            </div>

            <div class="pop_row">
                <div class="left_side">
                    <h5>Reply To</h5>
                </div>
                <div class="right_side">
                    <input type="text" class="right_txt" name="FromEmail" placeholder="Reply To Email (optional)" style="width:240px;height:35px;">
                </div>
            </div>

            <div class="pop_row">
                <div class="left_side">
                    <h5>Subject</h5>
                </div>
                <div class="right_side">
                    <input type="text" class="right_txt" value="Attached Generated Payroll PDF" name="MessageSubject" style="width:240px;height:35px;">
                </div>
            </div>

            <div class="pop_row_txtarea">
                <div class="left_side">
                    <h5>Message</h5>
                </div>
                <div class="right_side">
                    <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
                </div>
            </div>

        <!--    <div class="pop_row">
                <div class="left_side">
                    <h5>Attachment</h5>
                </div>
                <div class="right_side">
                    <label>Work.pdf</label>
                </div>
            </div>-->

            <div class="btn-row">
                <input type="button" class="btn-pop" id="sendMailWithAttachment" value="Send">
            </div>
        </form>

    </div>
    <!------------------Script of the Email Btn without Libraies of Bootstrap-------->
    <script>
        $('#sendEmailButton').on('click', function(e){
            Custombox.open({
                target: '#sendEmail',
                effect: 'fadein'
            });
            e.preventDefault();
        });
    </script>

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- left side menu end -->
	</div>


<!-- contents -->

<!--<script src="<?php /*echo base_url()*/?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php /*echo base_url()*/?>assets/js/bootstrap.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/noty/packaged/jquery.noty.packaged.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Parexons.js"></script>
<script type="text/javascript">
var editable = true;
<?php if(isset($TimeSheetViewData) && is_array($TimeSheetViewData) && !empty($TimeSheetViewData)){
    $TimeSheetStatus = $TimeSheetViewData[0]->TimeSheetStatus;
    if(is_numeric($TimeSheetStatus) && (intval($TimeSheetStatus) === 0 || intval($TimeSheetStatus) === 3)){
        ?>
editable = true;
<?php
}else{
echo "editable = false;";
}
}
?>

    <?php if(is_admin($loggedInEmployee)){ ?>
    function  createTimeSheet(){
        $.ajax({
            url: "<?php echo base_url(); ?>leave_attendance/createTimeSheetForAllRemainingEmployees",
            type: "POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] == 'OK'){
                    Parexons.notification(data[1],data[2]);
                }else if(data[0] == 'FAIL'){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    }
    <?php } ?>
    $(document).ready(function(e){

        //if the loggedInUser/Employee is Admin
        /*
        * New Select Employee
        * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
        * */
        var selectEmployeeSelector = $('#selectEmployee');
        var url = "<?php echo base_url(); ?>leave_attendance/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
                return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
        $('.select2-container').css("width","100%");

//        Get the Selectors of the Page
        //Get Available Years First.
        //        Get the Selectors of the Page
        var selectYearSelector = $('#selectYear');
        var url = "<?php echo base_url(); ?>leave_attendance/load_availableYears_in_timeSheet";
        var id = "TimeSheetID";
        var text = "YearValue";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
//        commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
        commonSelect2(selectYearSelector,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css("width","223px");

        selectEmployeeSelector.on('change',function(e){
            selectYearSelector.select2("destroy");
            var dependentEmployeeWhere = $(this).select2('data');
            commonSelect2Depend(selectYearSelector,url,id,text,minInputLength,placeholder,multiple,dependentEmployeeWhere);
            $('.select2-container').css("width","223px");
        });

        selectYearSelector.on("change", function(e){
            var dependentWhere = $(this).select2('data');
            var selectMonthSelector = $('#selectMonth');
            var url = "<?php echo base_url(); ?>leave_attendance/load_availableMonths_in_timeSheet";
            var id = "TimeSheetID";
            var text = "MonthName";
            var minInputLength = 0;
            var placeholder = "Select Month";
            var multiple = false;
            commonSelect2Depend(selectMonthSelector,url,id,text,minInputLength,placeholder,multiple,dependentWhere);
            $('.select2-container').css("width","223px");
            selectMonthSelector.on("change", function(e){
                var monthData = $(this).select2('data');
                var yearData = selectYearSelector.select2('data');

                //Creating First Arrays from Objects
                var monthDataArray = $.map(monthData, function(value, index) {
                    return [value];
                });
                var yearDataArray = $.map(yearData, function(value, index) {
                    return [value];
                });
//                Creating New Array to send data to controller
                var data = [];
//                Pushing Data to Data Array.
                data.push(monthDataArray[1]);
                data.push(yearDataArray[1]);
                <?php if(is_admin($loggedInEmployee)){ ?>
                data.push(selectEmployeeSelector.val());
                <?php } ?>
                console.log(data);
                var redirectURL = "<?php echo base_url(); ?>leave_attendance/manual_timesheet";
                $.redirect(redirectURL,{'selectedMonthYear':data},'POST');
            });
        });

        //only If Editable Is True, Then The User Will Be Able To Edit the Data, OtherWise He/She is not allowed To Do The Editing.
        if(editable === true){
            //Now Need to Get Available Months on Based of Year
            max = 1;
            //Now Need to Get Available Months on Based of Year
            $('tr.tableRow td.inner').on('click', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $(this).attr('contentEditable', 'true');
                $(this).addClass('inputCopyCat');
                $(this).focus();
            }).on('keyup', function (e) {
                $tr = $(this).closest('tr');
                var sum = 0;
                $tr.find('td.inner').each(function () {
//                console.log(Number($(this).text()));
                    sum += Number($(this).text());
                });
                $tr.find('.totalTD').text(sum);
            }).on('keydown',function(e){
                var editorSelector = $(this);
                check_charcount(editorSelector, max, e);
                function check_charcount(editorSelector, max, e)
                {
                    var keyCode = e.which;
                    if(keyCode !== 8 && keyCode !== 46 && keyCode !== 9 && keyCode !== 17)
                    {
                        // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
                        if(keyCode > 64 && keyCode < 91){
                            e.preventDefault();
                            e.stopPropagation();
                            Parexons.notification('Letters Are Not Allowed','warning');
                        }else if(editorSelector.text().length > max){
                            e.preventDefault();
                            e.stopPropagation();
                            Parexons.notification('You can Not Add More Charactersa','warning');
                        }
                    }
                }
            });
//Function to Send Ajax Request to Server when Focus is Out.
            $('tr.tableRow td.inner').focusout(function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                $('td.inputCopyCat').removeAttr('contentEditable');
                $('td.inputCopyCat').removeClass('inputCopyCat');
                var hoursWorked = Number($(this).text());
                hoursWorked = hoursWorked ? hoursWorked : 0;
                var dateOfHoursWorked = $(this).attr('td-data');
                var projectID = $( 'td:first-child', $( this ).parents ( 'tr' ) ).attr('data-project');
                var postData = {
                    hoursWorked: hoursWorked,
                    dateOfHoursWorked: dateOfHoursWorked,
                    projectID: projectID
                };
                <?php if(isset($employeeData) && !empty($employeeData)){

        echo "postData.EmployeeID = $employeeData->EmployeeID;";
    }
    ?>
                $.ajax({
                    url: "<?php echo base_url(); ?>leave_attendance/updateTimeSheetInfo",
                    data: postData,
                    type: "POST",
                    success:function(output){
                        var data = output.split("::");
                        if(data[0] === "OK"){
                            Parexons.notification(data[1],data[2]);
                        }else if(data[1] === "FAIL"){
                            Parexons.notification(data[1],data[2]);
                        }
                    }
                });
            });
        }

        //Submit For Review Button..
        <?php if(isset($TimeSheetViewData) && is_array($TimeSheetViewData) && !empty($TimeSheetViewData)){
            $TimeSheetStatus = $TimeSheetViewData[0]->TimeSheetStatus;
            if(is_numeric($TimeSheetStatus) && intval($TimeSheetStatus) === 0){
                ?>
        $('#submitForReview').on('click',function(e){
            var postData = {
                timeSheetID : <?php echo $TimeSheetViewData[0]->TimeSheetID; ?>
            };
            $.ajax({
                url: "<?php echo base_url(); ?>leave_attendance/sendManualTimeSheetForApproval",
                data:postData,
                type: "POST",
                success: function (output) {
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        editable = false;
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
        <?php
            }elseif(intval($TimeSheetStatus) === 1){ ?>
        $('#submitForReview').on('click', function (e) {
            Parexons.notification('TimeSheet Already Been Sent For Approval', 'warning');
            return;
        });
        <?php
            }elseif(intval($TimeSheetStatus) === 2){
            ?>
        $('#submitForReview').on('click', function (e) {
            Parexons.notification('TimeSheet Has Already Been Approved', 'warning');
            return;
        });
        <?php

            }
        } ?>

    });

    //Don't Remember the Below Function Right Now for what purpose its been used, Will check it later thou... :)
$(function(){
	var message_status = $("#status");
	 $("td[contenteditable=true]").blur(function(){
	 var field_userid = $(this).attr("id") ;         
	 var value = $(this).text() ; 
	 var data={
	 	field_userid: $(this).attr("id"),
	 	value: $(this).text()
	 };
	 $.ajax({
	 	url:'leave_attendance/update_timesheet',
	 	data:data,
	 	type: 'POST',
	 	success: function(output){
	 		console.log(output);
	 	}
	 });
 });     
});
</script>
<!--- Script for PDF file ---->
<script>
    $(document).ready(function(){
        $("#PdfButton").on('click', function(){
            var targetURL = '<?php echo base_url();?>leave_attendance/downloadPdfTimeSheetReport';
            <?php if(isset($pdf_view) && !empty($pdf_view)){ ?>
            var postData = [];
            postData.push({name:"viewData",value:<?php echo json_encode($pdf_view); ?>});
            console.log(postData);
            $.ajax({
                url: targetURL,
                data:postData,
                type:"POST",
                success: function (output) {
                    /*var data = output.split("::");
                     if(data[0] === "Download"){

                     }*/
                }
            });
            <?php } ?>
        });

        $('#sendMailWithAttachment').on('click', function (e) {
            var formData = $('#mailForm').serializeArray();
            <?php if(isset($pdf_view) && !empty($pdf_view)){ ?>
            formData.push({name:"viewData",value:<?php echo json_encode($pdf_view); ?>});
            <?php } ?>
            var targetURL = '<?php echo base_url();?>leave_attendance/SendPDFEmployeeTimeSheetReport';
            $.ajax({
                url: targetURL,
                data:formData,
                type:"POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>
