<?php //include('includes/header.php'); ?>
<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
select[name="status_title"],select[name="leave_type"]{
	width: 30%;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	left: 98%;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -89.3%;
	top: 20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -60px;
	left: -65px;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
</style>
<!-- contents -->

<div class="contents-container">

	 <!-- bredcrumb -->



 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"month","value":$('#type').val()});
			aoData.push({"name":"year","value":$('#status').val()});
                
                
		}
                
	});
});

function lookup(inputString) {

	if(inputString.length == 0) {
		$('#suggestions').hide();
		//$('#employee_inf0').hide();
		location.reload();
	} else {
		$.post("leave_attendance/autocomplete_over_time/", {queryString: ""+inputString+""}, function(data){
			//console.log(data);
			if(data.length > 0) {
				$('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
			}
		});
	}
}

function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue,employee_id) {
	$('#id_input').val(thisValue);
	$('#emp_id').val(employee_id);


	setTimeout("$('#suggestions').hide();", 200);
	//$('#suggestions').hide();
	//$('#employee_inf0').show();

	//setTimeout("$('#autoSuggestionsList').hide()", 200);
}



$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_leave_status(id)
{
	window.open('leave_attendance/print_leave_status/'+id,"","width=800,height=600");
}
function print_all_leave_status(id)
{
	window.open('leave_attendance/print_all_leave_status/'+id,"","width=800,height=600");
}

</script>

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Timesheet</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Over Time</div>
<form action="leave_attendance/view_over_time" method="post">
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" style="width:220px;" name="full_name" id="id_input"  placeholder="Employee Name" class="serch" autocomplete="off" onkeyup="lookup(this.value)">
				<input type="hidden" name="emp_id" id="emp_id" />
				<div class="row"> <div id="suggestions">
						<div class="autoSuggestionsList_l" id="autoSuggestionsList">
						</div>
					</div>
				</div>
			        <select name="month" style="width:220px;float:left;margin-left: 10px;" required="required" id="month">
                     <option value="-1">-- Select month --</option>
                     <?php foreach($month as $mon){?>
                     <option value="<?php echo $mon->ml_month_id;?>"><?php echo $mon->month;?></option>
                     
                     <?php }?>
                     </select>
                     <select name="year" style="width:220px;float:left;margin-left: 10px;" required id="year">
                     <option value="-1">-- Select Year --</option>
                     <?php foreach($year as $yer){?>
                     <option value="<?php echo $yer->ml_year_id;?>"><?php echo $yer->year;?></option>
                     <?php }?>
                     </select>
				<div class="button-group" style="margin-left: 10px;" >
					<button class="btn green">Search</button>
				</div>
				</div>
				<br class="clear">
            </form>

			

			<!-- table -->
			<table cellspacing="0" cols="6">
				<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>Month</td>
                    <td>Year</td>
					<td>Calculated Hours</td>
                    <td>Sanctioned Hours</td>
				</thead>
                  <?php if(!empty($over_time)){
					foreach($over_time as $over_time){?>
                    <tbody>
                    <td><?php echo $over_time->employee_code?></td>
                    <td><?php echo $over_time->full_name?></td>
                    <td><?php echo $over_time->month?></td>
                    <td><?php echo $over_time->year?></td>
                    <td><?php echo $over_time->calculated_hours?></td>
                    <td><?php echo $over_time->sanction_hours?></td>
                    </tbody>
                    <?php }}else{?>
                    <td colspan="8"><?php echo "<p style='color:red'>No Data Available</p>"?></td>
                    <?php }?>
            
			</table>
                <div id="pagination">
            <ul>
            <?php echo $links; ?>
            </ul>
            </div>
		</div>

	</div>
<!-- contents -->

<?php //include('includes/footer.php'); ?>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#month,#year').select2();
    })


    </script>
    <!-- leftside menu end -->