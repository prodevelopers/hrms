<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
.move{
	position: relative;
	/*top: 45px;*/
    margin-left:20px;
}
.resize{
	width: 220px;
}
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	left: -33.3%;
	top: -31px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -60px;
	left: -8.4%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 40px;
}
select[name="month"], select[name="year"]{
	width: 20%;
}
</style>
<!-- contents -->

<!--<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance</div> --><!-- bredcrumb -->


 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
    <script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"year","value":$('#year').val()});
            aoData.push({"name":"month","value":$('#month').val()});    
		}
                
	});

    $('#Searchbyname').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });



    $('tbody').addClass("table-row");
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}
function print_all_standard_timesheet(id)
{
	window.open('leave_attendance/print_all_standard_timesheet/'+id,"","width=800,height=600");
}

</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Timesheet</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Hourly Accumulative Time Sheet</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>

                <input type="text" id="Searchbyname" placeholder="Search Employees" style="width: 30%;">

					<?php echo @form_dropdown('month',$month, $select_month, "class='resize' id='month' onchange='this.form.submit()'");?>

		        <?php echo @form_dropdown('year',$year, $select_year, "class='resize' id='year' onchange='this.form.submit()'");?>

                <span class="fa fa-print move" style="cursor:pointer;" onclick="return print_all_standard_timesheet('<?php echo $this->input->post('month')?>/<?php echo $this->input->post('month')?>/')">
                 </span>

			</div>

 <?php echo form_close();?>

		<!--	<div class="row">
				<h4>Employee Name</h4>
			</div>-->

			<!-- table -->
            
            	<table cellspacing="0" id="table_list">
				<thead class="table-head">
					<td>Employee Id</td>
					<td>Employee Name</td>
					<td>Month</td>
                    <td>Year</td>
                    <td>Work Hours</td>
					<td>Leave Hours</td>
                    <td>Total Hours</td>
					
				
				</thead>
                <tbody>
                </tbody>
                </table>

		<div class="row2">
			<a href="leave_attendance/view_acumulative"><button class= "btn green">Hourly Accumulative Form Entry</button></a>
		</div>
			<!--</table>-->

	</div>
	</div>

<!-- contents -->


<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

    $(document).ready(function(e){
        $('#year, #month').select2();
    });
    </script>
    <!-- leftside menu end -->