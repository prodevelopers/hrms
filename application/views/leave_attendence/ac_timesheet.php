
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

</style>
<!-- contents -->
<script type="text/javascript">
$(document).ready(function() {
    //Restrict The Input to Maximum Standard Hours.
var totalStandardHours = parseInt($('#standardHours').text());
    $('#workHours, #leaveHours').on('change', function (e) {
        var checkVariable = parseInt($(this).val());
        console.log(checkVariable);
        if(!isNaN(checkVariable)){
            $(this).val(checkVariable);
        }else{
            Parexons.notification('Only Numeric Input Is Allowed.','error');
            $(this).val(0);
        }
        //if Input Number is Greater Then the Standard Hours Number Then Make it Equal to Standard Hours.. Cuz We Cant Go Beyond Standard Number..
        if(parseInt($(this).val()) > totalStandardHours){
            $(this).val(totalStandardHours);
        }
    });

	$('#backbutton').on('click',function(){
        //We Need To Unset/Clear The Data in Session When BackButton Is Pressed..
        $.ajax({
            url: "<?php echo base_url(); ?>leave_attendance/clearHourlyTimeSheetSession",
            type: "GET",
            success:function(output){
                var data = output.split("::");
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    window.setTimeout(function(){location.reload()},1000);
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
	});


    <?php

     //Print Messages If Exist in the Session...

     if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
    var data = message.split("::");
    Parexons.notification(data[0],data[1]);
    <?php
    }
    }
}
?>

});

function lookup(inputString) {

    if(inputString.length == 0) {
        $('#suggestions').hide();
		 $('#employee_inf0').hide();
	       location.reload();
    } else {
        $.post("leave_attendance/autocomplete_time_sheet/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
				//console.log(data);
            }
        });
    }
}
function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue, employee_id) {
    $('#id_input').val(thisValue);
	$('#emp_id').val(employee_id);
	setTimeout("$('#suggestions').hide();", 200);
}



</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Accumulative Time Sheet Entry Form</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
<script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Accumulative Time Sheet Entry Form</div>
		<div class="form-left">
			<form action="leave_attendance/add_accumulative" method="post">
				<div class="row2">
					<h4>Employee Name</h4>
					<input type="text" name="id_input" id="id_input"  autocomplete="off" onkeyup="lookup(this.value)" required="required">
					<input type="hidden" name="emp_id" id="emp_id" />
					<div id="suggestions">
						<div class="autoSuggestionsList_l" id="autoSuggestionsList">
						</div>
					</div>

				</div>
                   <br class="clear">
				<div class="row2">
					<h4>Standard Hours</h4>
                     <?php echo @form_dropdown('monthly_std_hrs',$monthly_std_hrs,'',"disabled='disabled' id='standardHours'");?>
                      <?php //echo form_hidden('monthly_std_hrss',$monthly_std_hrs, '', "disabled='disabled'");?>
                      <input type="hidden" name="monthly_std_hrss" value="<?php echo @$standard->monthly_std_hrs_id; ?>" />
				</div>
                 <br class="clear">
				<div class="row2">
					<h4>Month</h4>
                     <?php  echo @form_dropdown('month',$month,'required = "required"','required = "required"');?>
				</div>
                 <br class="clear">
				<div class="row2">
					<h4>Year</h4>
                     <?php  echo @form_dropdown('year',$year,'required = "required"','required = "required"');?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4>Work Hours</h4>
					<input type="text" name="work_hours" required="required" id="workHours">
				</div>
              
				<div class="row2">
					<h4>Leave Hours</h4>
					<input type="text" name="leave_hours" required="required" id="leaveHours">
				</div>

				<div class="row2">
				<br class="clear">
				
				<!-- button group -->
			<div class="row2">
				<div class="button-group">
                    <input type="submit" class="btn green"  value="Submit" name="add"/>
				    <input type="button" value="Clear" id="backbutton" class="btn gray"  />
				</div>
			</div>
          
				
			</form>
			</div>

		</div>
            	<table cellspacing="0" >
				<thead class="table-head">
                <tr>
					<td>Employee Id</td>
					<td>Employee Name</td>
					<td>Month</td>
                    <td>Year</td>
                    <td>Work Hours</td>
					<td>Leave Hours</td>
                    <td>Total Hours</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
                </tr>
				</thead>
                <tbody>
                    <?php
                        $temporaryTableArray = array();
                    foreach($this->session->all_userdata() as $key=>$value){
                        if("AccumulativeTimeSheet-" === substr($key,0,22)){
                            $temporaryTableArray[$key] = $value;
                        }
                    }
                    foreach($temporaryTableArray as $key => $value){
                        /**
                         * Columns In Table Are Listed As:
                         * EmployeeID
                         * EmployeeName
                         * Month
                         * Year
                         * WorkHours
                         * LeaveHours
                         * TotalHours
                         * EditOption
                         */
//                        print_r($value);
                        echo "<tr><td>".$value['employeeID']."</td><td>".$value['employeeName']."</td><td>".$value['Month']."</td><td>".$value['Year']."</td><td>".$value['WorkHours']."</td><td>".$value['LeaveHours']."</td><td>".$value['TotalHours']->monthly_std_hrs."</td><td><a href='".base_url()."leave_attendance/edit_accumulative/".$value['employeeID']."/".$value['TimeSheetID']."'><span class='fa fa-pencil'></span></span></a></td></tr>";
                    }
                    ?>
				</tbody>
                </table>
			<!--</table>-->
       
	</div>
<!-- contents -->


<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->