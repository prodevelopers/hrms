<base href="<?=base_url()?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var a=confirm("Are you sure want to Print");
	if(a==true)
	{
		window.print() ;
	}
});
</script>

<style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	padding-left: 7em;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight: bold;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 5px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
}
</style>


<link rel="stylesheet" type="text/css" href="css/style.css">
			<table cellspacing="0" width="600" class="printing">
				<tr>
					<td colspan="2" style="padding: 1em 0;" class="head"><h3>Leave Attendence Status</h3></td>
				</tr>
                	<tr>
					<td width="200" class="apply" class="apply" class="apply" class="apply">Employee ID</td>
					<td class="no-apply"><?php echo @$print->employee_code;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Employee Name</td>
					<td class="no-apply"><?php echo @$print->full_name;?></td>
				</tr>
			
				
				<tr>
					<td width="200" class="apply" class="apply">Leave Type</td>
					<td class="no-apply"><?php echo @$print->leave_type;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">From Date</td>
					<td class="no-apply"><?php echo @$print->from_date;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">To Date</td>
					<td class="no-apply"><?php echo @$print->to_date;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Total Days</td>
					<td class="no-apply"><?php 
						$from = $print->from_date;
						$to = $print->to_date;
						$diff = abs(strtotime($from) - strtotime($to));
						$years = floor($diff / (365*60*60*24));
						$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
						$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
						
						if(!empty($years))
						{
							echo $years." Years, ".$months." Months, ".$days." Days";
						}
						else if(!empty($months))
						{
							echo $months." Months, ".$days." Days";
						}
						
						else if(!empty($months))
						{
							echo $days." Days";
						}
						//echo $years." Years, ".$months." Months, ".$days." Days";?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Emergency Contact No:</td>
					<td class="no-apply"><?php echo @$print->mobile_num;?></td>
				</tr>
                <tr>
					<td width="200" class="apply">Status:</td>
					<td class="no-apply"><?php echo @$print->status_title;?></td>
				</tr>
				
			</table>
		</div>
