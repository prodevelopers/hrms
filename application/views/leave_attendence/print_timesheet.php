<base href="<?=base_url()?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var a=confirm("Are you sure want to Print");
	if(a==true)
	{
		window.print() ;
	}
});
</script>

<style type="text/css">
.printing
	{
	border:thin solid #ccc;
}
.printing h3
{
	font-style: normal;
	text-align: center;
}
.printing .apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -1px;
	font-weight: bold;
}
.printing .no-apply
{
	padding:0.6em 0 0.6em 3em;
	border:thin solid #ccc;
	background: #fff;
	letter-spacing: -0.5px;
	font-weight: bold;
}
.head
{
	background: #4D6684;
	color: #fff;
	padding: 5px 0px 5px 20px;
	font-size: 0.9em;
	line-height: 25px;
}
</style>
<link rel="stylesheet" type="text/css" href="css/style.css">
			<table cellspacing="0" width="600" class="printing">
				<tr>
					<td colspan="2" style="padding: 0;" class="head"><h3>Timesheet</h3></td>
				</tr>
				<tr>
					<td colspan="2" style="padding:1em;" class="head"><h2><?php echo @$print->full_name;?></h2></td>
				</tr>
				<tr>
					<td width="200" class="apply">Project Name</td>
					<td class="no-apply"><?php echo @$print->project_title;?></td>
				</tr>
                <tr>
					<td width="200" class="apply">Activity Name</td>
					<td class="no-apply"><?php echo @$print->task_name;?></td>
				</tr>
					<td width="200" class="apply">In Time</td>
					<td class="no-apply"><?php echo @$print->in_time;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">In Date</td>
					<td class="no-apply"><?php echo @$print->in_date;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Out Time</td>
					<td class="no-apply"><?php echo @$print->out_time;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Out Date</td>
					<td class="no-apply"><?php echo @$print->out_date;?></td>
				</tr>
				<tr>
					<td width="200" class="apply">Total Hours</td>
					<td class="no-apply"><?php echo @$print->monthly_std_hrs;?></td>
				</tr>
			</table>
		</div>
