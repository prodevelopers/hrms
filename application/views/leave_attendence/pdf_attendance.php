<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/progress-bar.css">
<link rel="stylesheet" type="text/css" href="assets/select2/select2.css">
<link rel="stylesheet" type="text/css" href="assets/css/form.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/data_picker/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<style type="text/css">
.dropdown-menu{
    position: relative;
    left: 70%;
}
.margino{
    padding-left: 2em;
    font-family: "Myriad Pro";
}
.margino .header {
    width: 200px;
    background: #4D6684;
    color: #fff;
    border:none;
}
.marginol{
    position: relative;
    left: 600px;
}
.margino ul{
    float: left;
}
.margino .menu li{
    float: left;
    width: 200px;
    background: #fff;
    border:none;
}
.margino .menu h4{
    font-size: 14px;
    float: left;
}
.margino .menu small{
    float: right;
    padding: 0.2em 0;
}
.margino .menu a{
    padding: 0.2em 0.5em;
    background: green;
    color: #fff;
}
.margino .menu a:hover{
    color: #f8f8f8;
}
.margino .menu p{
    float: left;
    width: 100%;
    padding: 0.2em;
    font-size: 13px;
    font-weight: bold;;
}
.margino .menu p:hover{
    background: #f8f8f8;
}
.margino ul ul{
    position: relative;
    left: -2px;
    top: 27px;
}
.margino ul ul li {
    float: left;
}
.margino h3 {
    font-size: 12px;
    padding: 0.5em;
}

.ol{
    padding: 0;
    position: absolute;
    left: 0;
    /*width: 175px;*/
    height: 50px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    display: none;
    opacity: 0;
    visibility: hidden;
    -webkit-transiton: opacity 0.2s;
    -moz-transition: opacity 0.2s;
    -ms-transition: opacity 0.2s;
    -o-transition: opacity 0.2s;
    -transition: opacity 0.2s;
}
/*ul li ol {

}*/
.li {
    background: #333;
    display: block;
    /*width: 175px;*/
    color: #fff;
    font-size: 16px;
    float: left;
}
.li a{
    font-size: 16px;
}

.ol.li:hover{
    /*width: 180px;*/
    padding: 0;
    margin: 0;
    -webkit-transition:none;
    -o-transition:none;
    transition:none;
}
ul li:hover .ol {
    display: block;
    opacity: 1;
    visibility: visible;
    position: relative;
}
.li h4 {
    padding: 0;
    position: absolute;
    opacity: 0;
    left: 0;
    display: none;
}
.li:hover h4{
    display: block;
    font-weight: normal;
    background: #333;
    opacity: 1;
    visibility: visible;
    position: relative;
    padding: 0;
}




    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .mytab tr:nth-child(odd) td {
        background-color: #ECECFF;
    }

    .mytab tr:nth-child(even) td {
        background-color: #F0F0E1;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers

    "
    {
        margin-top: 8px
    ;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;

        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 40px;
    }
</style>
<div class="table-responsive1">
    <div><h4>Attendance List</h4></div>
			<table cellspacing="0" style="width:106%;">
				<thead>
                <tr>
					<td>Employee ID</td>
					<td>Employee Name</td>
                    <td>Project</td>
                    <td>Department</td>
					<td>Coordinates</td>
					<td>In Date</td>
					<td>In Time</td>
					<td>Out Date</td>
                    <td>Out Time</td>
					<td>This Day</td>
					<td>Total Hours</td>
                    <td id="age" title="It will show your Attendence status.">Status</td>
					<!--<td><span class="fa fa-print"></span></td>--></tr>
				</thead>
                <tbody>
                <?php
                if (!empty($leave_attend)){
                foreach ($leave_attend as $attendance){
                $timestart = $attendance['in_time'];
                $timestop = $attendance['out_time'];
                if ($timestop !== '00:00:00') {
                    //Change It
                    $time1 = new DateTime($timestart);
                    $time2 = new DateTime($timestop);
                    $time_diff = $time1->diff($time2);

                    $total1 = $time_diff->format('%H:%I');
                    //End of Change
                } else {
                    $total1 = " <div class='tooltip-container'><i class='fa fa-info-circle' style='color:blue;'>
                    <span class='tooltip'>Time Out Missing</span>
                    </i></div>";
                }
                ?>
                <tr class="table-row">
                    <td><?php echo $attendance['employee_code']; ?></td>
                    <td><?php echo $attendance['full_name']; ?></td>
                    <td><?php echo $attendance['Abbreviation']; ?></td>
                    <td><?php echo $attendance['department_name']; ?></td>
                    <td><?php echo $attendance['la_lo']; ?></td>
                    <td><?php echo date("d-M-Y",strtotime($attendance['in_date'])); ?></td>
                    <td><?php echo ($attendance['in_time'] == '00:00:00') ? ' - ': date("H:i ",strtotime($attendance['in_time'])); ?></td>
                    <td><?php echo ($attendance['out_date'] == "0000-00-00") ? date("d-M-Y",strtotime($attendance['in_date'])) : date("d-M-Y",strtotime($attendance['out_date'])); ?></td>
                    <td><?php echo ($attendance['out_time'] == '00:00:00') ? ' - ': date("H:i ",strtotime($attendance['out_time'])); ?></td>
                    <td><?php echo @$attendance[0]; ?></td>
                    <td><?php echo $total1; ?></td>
                    <td><?php
                        $type = $attendance['status_type'];
                        if ($type == 'Absent') {
                            echo '<span style="color:#ff0d00;">' . $type . '</span>';
                        } else if ($type == 'Present') {
                            echo '<span style="color:#00CC00;">' . $type . '</span>';
                        } else if ($type == 'On Leave') {
                            echo '<span style="color:#0c12ff;">' . $type . '</span>';
                        } else {
                            echo '<span style="color:#cd6e00;">' . $type . '</span>';
                        }
                        ?></td>
                    <?php /*?>    <td><a href="leave_attendance/edit_leave_staff/<?php echo $leave_staff->employee_id?>"> <span class="fa fa-pencil"></span></a></td>
                    <td><a href="leave_attendance/print_leave_status/<?php echo $leave_staff->employee_id?>"><span class="fa fa-print" onclick="print_leave_status" style="cursor:pointer"></span></a
                    ><?php */
                    ?></tr>
                <tr>
                    <?php }
                    } else { ?>
                        <td colspan="10"><?php echo "<p style='color:red'>Sorry No Data Available</p>" ?></td>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </div>


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
