<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;"><?php echo  $this->session->flashdata('alert');?></div>
<style type="text/css">
#autoSuggestionsList
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 
 display:none;
}
#autoSuggestionsList li
{
 list-style:none;
 cursor:pointer;
 padding:5px;
 font-size:14px;
 font-family:Arial;
 margin:2px;
 -moz-border-radius: 5px;
 border-radius: 5px;
}
#autoSuggestionsList li:hover
{
 border:1px solid #CCC;
}
.serch
{
 width: 200px;
 height: 25px;
 font-family: Arial, Helvetica, sans-serif;
 font-size: 14px;
 color: black;
 padding-left: 5px;
 border: 1px solid #999;
}

.designing{
	top:200px;
	right:200px;
	}
#employee_inf0
{
 width: 205px;
 overflow: hidden;
 border:1px solid #CCC;
 -moz-border-radius: 5px;
 border-radius: 5px;
 display:none;
 alignment-adjust:central;
}

#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php base_url();?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}
.paginate_button
{
	padding: 6px;
	background-image: url(<?php base_url();?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;

	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers"
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;

	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>
<!-- contents -->

<div class="contents-container">

<!--	<div class="bredcrumb">Dashboard / Leave&Attendance</div>--> <!-- bredcrumb -->

<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
   <!-- <script type="text/javascript" src="<?php /*echo base_url();*/?>assets/js/data-tables/jquery.js"></script>-->
<script type="application/javascript" src="<?php echo base_url();?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="application/javascript" src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
/*$(document).ready(function() {
	
	
	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"leave_type","value":$('#type').val()});
                
		}
                
	});
});*/
function lookup(inputString) {
	
    if(inputString.length == 0) {
        $('#suggestions').hide();
		 //$('#employee_inf0').hide();
	       location.reload();
    } else {
        $.post("leave_attendance/autocomplete_apply_name/", {queryString: ""+inputString+""}, function(data){
			//console.log(data);
            if(data.length > 0) {
                $('#suggestions').show();
				$('#autoSuggestionsList').show();
				$('#autoSuggestionsList').html(data);
            }
        });
    }
}

function clear_div()
{document.getElementById('#employee_inf0').innerHTML = "";}

function fill(thisValue, employee_id, obj) {
    $('#id_input').val(thisValue);
    $('#emp_id').val(employee_id);
    setTimeout("$('#suggestions').hide();", 200);
    //$('#suggestions').hide();
    //$('#employee_inf0').show();

    //Custom Code
    var employeeID = $(obj).attr('data-id');
    var data = {
        employeeID: employeeID
    };
    $.ajax({
        url: "<?php echo base_url(); ?>leave_attendance/selectLeavesForSelectedEmployee",
        data: data,
        type: "POST",
        dataType:"json",
        success: function (output) {
// console.log(output);
            if(output.length>0){
                $.each(output, function(i, e){
                    console.log(e);
                    $('table#table_list').append('<tr class="table-row"><td>' + e.Category + '</td><td>' + e.Allocated + '</td></tr>');
                });
            }
        }
    });
    //setTimeout("$('#autoSuggestionsList').hide()", 200);
}   



$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}


 

</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Update Leave Application</div> <!-- bredcrumb -->


	<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;"><?php echo  $this->session->flashdata('alert');?></div>
<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Update Leave Application</div>
		<div class="form-left">
          <?php //if ($employee >0){
                 //foreach($employee as $employee):?>
			<form action="" method="post">

                <br class="clear">
				<h4>Employee Name</h4>
				<input type="text" name="full_name" value="<?php echo $employee_id->full_name ;?>" readonly>
				<input type="hidden" name="emp_id" id="emp_id" />


				<br class="clear">
				<div class="row2">
					<h4>Leave Type</h4>
					<?php $selected = $user->ml_leave_type_id;?>
                     <?php echo @form_dropdown('leave_type',$leave_type,$selected,'required = "required"','required = "required"');?>
				</div>

				<br class="clear">
				   <div class="row2">
                     <h4>From</h4>
					<input type="text" id="applyForLeaveFromDate" name="from_date" onchange="mydata()" value="<?php echo date_format_helper($user->from_date);?>" required="required">
				 </div>
				<div class="row2">
					<h4>To</h4>
					<input type="text" id="applyForLeaveToDate" name="to_date" onchange="mydata()" value="<?php echo date_format_helper($user->to_date);?>" required="required">
				</div>
                <br class="clear">
                <div class="row2" id="half_d" style="display: none">
                    <h4>Half Day</h4>
                    <input type="radio" id="half_day"  name="half_day">
                </div>
				<br class="clear">
				<div class="row2">
					<h4>Total Days:</h4>
					<input type="text" name="total_days" id="tdays" value="<?php echo $user->total_days;?>">
                 
				</div>

				<br class="clear">

			<div class="row2">
	<h4 >Description:</h4>
					<textarea name="note" rows="8" cols="33"><?php echo $user->note;?></textarea>
				<br class="clear"></div>
				<!-- button group -->
			<div class="row2">
				<div class="button-group">

                       <input type="submit" class="btn green"  value="Update" name="edit" />
				</div>
			</div>
				
			</form>
			</div>

		</div>



<!-- contents -->

	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

	<script>

		function mydata(){
            $("#half_d").hide();
			var fromDate=$("#applyForLeaveFromDate").val();
			var toDate=$("#applyForLeaveToDate").val();
            var fromDateArray = fromDate.split('-');
            var toDateArray = toDate.split('-');
            var diff = new Date(Date.parse(toDateArray[2]+'-'+toDateArray[1]+'-'+toDateArray[0]) - Date.parse(fromDateArray[2]+'-'+fromDateArray[1]+'-'+fromDateArray[0]));
			var totalDays = diff / 1000 / 60 / 60 / 24;
			//Assign The Total Days to The TextField.
			$("#tdays").val(totalDays+1);
            var d=totalDays+1;
            var da=parseInt(d);

            if(da == 1){
                $("#half_d").show();
            }
            if($("#tdays").val() == 1){
                $('#half_day').prop('checked', false);
            }

        }
        $("#half_day").on('click',function(){


            if($('#half_day').is(':checked'))
            {

                $("#tdays").html('').val(0.5);
            }

        });
		/*$("#alert").delay(3000).fadeOut('slow');*/



	/*	$("#alert").delay(3000).fadeOut('slow');*/

		//alert(msg);
		//setTimeout(function(){ msg.hide() }, 3000);
	</script>
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });

        <!---------->
    $("#applyForLeaveFromDate").datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate){
            $("#applyForLeaveToDate").datepicker( "option", "minDate", selectedDate );
        }
    });
    $("#applyForLeaveToDate").datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate) {
            $("#applyForLeaveFromDate").datepicker( "option", "maxDate", selectedDate );
        }
    });
    </script>
    <!-- leftside menu end -->