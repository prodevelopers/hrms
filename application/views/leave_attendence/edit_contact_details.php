<!-- contents -->
<div class="contents-container">

	<div class="bredcrumb">Dashboard / Employees / Edit Employee / Personal Info</div> <!-- bredcrumb -->


	
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Current Address Details</div>
        <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Address<i>*</i></h4>
					<input type="text" name="address" value="<?php echo $curr_add->address; ?>">
				</div>

				<div class="row">
					<h4>City/Village</h4>
                    <?php 
					$selctd_city = (isset($curr_add->city_village) ? $curr_add->city_village : '');
					echo @form_dropdown('city_village', $city, $selctd_city); ?>
					<!--<select name="city_village">
						<option>Select City/Village</option>
					</select>-->
				</div>
				<div class="row">
					<h4>District (Domicile)</h4>
                    <?php 	
					$selctd_district = (isset($curr_add->District) ? $curr_add->District : '');
					echo @form_dropdown('District', $district, $selctd_district); ?>
					<!--<select name="District">
						<option>Select District</option>
					</select>-->
				</div>
				<div class="row">
					<h4>Province</h4>
                    <?php 
					$selctd_province = (isset($curr_add->province) ? $curr_add->province : '');
					echo @form_dropdown('province', $province, $selctd_province); ?>
				</div>

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="home_phone" value="<?php echo @$curr_add->home_phone; ?>">
				</div>

				<div class="row">
					<h4>Mobile</h4>
					<input type="text" name="mob_num" value="<?php echo @$curr_add->mob_num; ?>">
				</div>

				 <br class="clear"> 

				<div class="row">
					<h4>Email</h4>
					<input type="email" name="email_address" value="<?php echo @$curr_add->email_address; ?>">
				</div>
				<br class="clear">
				<div class="row">
					<h4><b>Work Contacts</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Work Telephone</h4>
					<input type="text" name="office_phone" value="<?php echo @$curr_add->office_phone; ?>">
				</div>

				<div class="row">
					<h4> Work Email</h4>
					<input type="email" name="official_email" value="<?php echo @$curr_add->official_email; ?>">
				</div>

				<h6>* Required Fields</h6>		

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_current" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
                <!--<input type="reset" value="Reset" class="btn gray" />-->
				</div>
			</div>
        <?php echo form_close();?>

		</div>
<div class="right-contents" style="margin-top:20px;">

		<div class="head">Permanent Address Details</div>
        <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Address<i>*</i></h4>
					<input type="text" name="address" value="<?php echo @$perm_add->address;?> ">
				</div>
				<div class="row">
					<h4>City/Village</h4>
                    <?php 
					$selctd_perm_city = (isset($perm_add->city_village) ? $perm_add->city_village : '');
					echo @form_dropdown('city_village', $city, $selctd_perm_city); ?>
					<!--<select name="city_village">
						<option>Select City/Village</option>
					</select>-->
				</div>
				<div class="row">
					<h4>District (Domicile)</h4>
                    <?php					
					$selctd_perm_dist = (isset($perm_add->district) ? $perm_add->district : ''); 
					echo @form_dropdown('district', $district, $selctd_perm_dist); ?>
					<!--<select name="district">
						<option>Select District</option>
					</select>-->
				</div>
				<div class="row">
					<h4>Province</h4>
                    <?php
					$selctd_perm_prov = (isset($perm_add->province) ? $perm_add->province : '');  
					echo @form_dropdown('province', $province, $selctd_perm_prov); ?>
					<!--<select name="province">
						<option>Select Province</option>
					</select>-->
				</div>
				<!-- <br class="clear"> -->

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="phone" value="<?php echo @$perm_add->phone; ?>">
				</div>

				<h6>* Required Fields</h6>	

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_permanant" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
        <?php echo form_close();?>

		</div>
		<div class="right-contents" style="margin-top:20px;">

		<div class="head">Emergency Contacts</div>
        <?php echo form_open('human_resource/edit_emp_curr_contact/'.$employee->employee_id);?>
            <input type="hidden" name="emp_id" value="<?php echo @$employee->employee_id;?>" />
				<div class="row">
					<h4>Name</h4>
					<input type="text" name="cotact_person_name" value="<?php echo @$emerg_add->cotact_person_name; ?>">
				</div>

				<div class="row">
					<h4>Relationship</h4>
                    <?php 
					$selct_relation = (isset($emerg_add->relationship) ? $emerg_add->relationship : '');
					echo @form_dropdown('relationship', $relation, $selct_relation); ?>
				</div>

				<div class="row">
					<h4>Home Telephone</h4>
					<input type="text" name="home_phone" value="<?php echo @$emerg_add->home_phone; ?>">
				</div>

				<div class="row">
					<h4>Mobile</h4>
					<input type="text" name="mobile_num" value="<?php echo @$emerg_add->mobile_num; ?>">
				</div>

				 <br class="clear"> 

				<div class="row">
					<h4>Work Telephone</h4>
					<input type="text" name="work_phone" value="<?php echo @$emerg_add->work_phone; ?>">
				</div>

			<!-- button group -->
			<br class="clear">
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add_emergency" value="Update" class="btn green" />
				<input type="button" value="Cancel" onclick="history.back();" class="btn gray" />
				</div>
			</div>
        <?php echo form_close();?>

			<?php /*?><table cellspacing="0" >
				<thead class="table-head secondry">
					<td>Name</td>
					<td>Relationship</td>
					<td>Home telephone</td>
					<td>Mobile</td>
					<td>Work telephone</td>
                    <td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($emrg_detail)){
					foreach($emrg_detail as $row){ ?>
				<tr class="table-row">
					<td><?php echo $row->cotact_person_name; ?></td>
					<td><?php echo $row->relation_name; ?></td>
					<td><?php echo $row->home_phone; ?></td>
					<td><?php echo $row->mobile_num; ?></td>
					<td><?php echo $row->work_phone; ?></td>
                    <td><a href="human_resource/edit_emp_single_curr_contact/<?php echo $employee->employee_id?>/<?php echo $row->emergency_contact_id?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="human_resource/send_2_trash_contct/<?php echo $employee->employee_id?>/<?php echo $row->emergency_contact_id?>"><span class="fa fa-trash-o"></span></a></td>
				</tr>
                <?php } } ?>
				
			</table><?php */?>
		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
    <?php $this->load->view('includes/edit_employee_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->