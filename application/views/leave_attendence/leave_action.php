

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Leave Action</div> <!-- bredcrumb -->

	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>


	<div class="right-contents">
	
		<div class="head">Leave Action</div>
			
<div class="form-left">
			<form action="leave_attendance/add_leave_action" method="post">
            	<br class="clear">
				<div class="row2">
					<h4>Leave Type</h4>
					<h4><i><?php echo $rec->leave_type;?></i></h4>
				</div>
				<br class="clear">
				<div class="row2" >
					<h4>Application</h4>
					<h4><i><?php echo $rec->note;?></i></h4>
				</div>
                <br class="clear">
				<div class="row2">
					<h4>From Date</h4>
					<h4><i><?php echo $rec->from_date;?></i></h4>
				</div>
                 <br class="clear">
				<div class="row2">
					<h4>To Date</h4>
					<h4><i><?php echo $rec->to_date;?></i></h4>
				</div>
				<br class="clear">
				
				<div class="row2">
					<h4>Paid Leaves</h4>
					<input type="text"  name="no_of_paid_days" >
				</div>
				<div class="row2">
					<h4>Un-paid Leaves</h4>
					<input type="text" name="no_of_un_paid_days" >
				</div>
				<div class="row2">
					<h4>Action</h4>
                    <?php echo form_dropdown('status_title', $status);?>
				</div>
				<br class="clear">
				<div class="row2">
					<h4 style="width:21%">Description:</h4>
					<textarea style="float:right;margin-right:4.6em;" rows="8" cols="35" name="remarks"></textarea>
					</div>
				<br class="clear">
				<!-- button group -->
			<div class="row2">
				<div class="button-group">
                     <input type="submit" class="btn green"  value="Action" name="add" />
				</div>
			</div>
			
			</form>
            	<?php //endforeach;?>
                <?php //} else { echo "<div class=\"result\">No record found..</div>";} ?>
			</div>
			<div class="form-right">
		<div class="head">Leave Summary</div>
        <div class="row2">
					<h5 class="headingfive">Employee ID</h5>
					<i class="italica"><?php echo $rec->employee_code;?></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Employee Name</h5>
                    	<i class="italica"><?php echo $rec->full_name;?></i>
                    </div>
                    <div class="row2">
                    	<h5 class="headingfive">Designation</h5>
                    	<i class="italica"><?php echo $rec->designation_name;?></i>
				</div>
                    <div class="row2">
                    	<h5 class="headingfive">Leave Entitlements</h5>
                    	<i class="italica"><?php echo $rec->no_of_leaves;?></i>
				</div>
				<div class="row2">
                    	<h5 class="headingfive">Leave Taken</h5>
                    	<i class="italica"><?php echo @$rec->total_days;?></i>
				</div>
			
			</div>
		</div>
		</div>

	</div>	
<!-- contents -->
<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->


<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       	<?php $this->load->view('includes/leave_left_nav.php'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->