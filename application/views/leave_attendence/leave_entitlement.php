<?php //include('includes/header.php'); ?>
<style type="text/css">
	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
	#pagination
	{
		float:left;
		padding:5px;
		margin-top:15px;
	}
	#pagination a
	{
		padding:5px;
		background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color:#5D5D5E;
		font-size:14px;
		text-decoration:none;
		border-radius:5px;
		border:1px solid #CCC;
	}
	#pagination a:hover
	{
		border:1px solid #666;
	}
	.mytab tr:nth-child(odd) td{
		background-color:#ECECFF;
	}
	.mytab tr:nth-child(even) td{
		background-color:#F0F0E1;
	}
	.paginate_button
	{
		padding: 6px;
		background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color: #5D5D5E;
		font-size: 14px;
		text-decoration: none;
		border-radius: 5px;

		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		border: 1px solid #CCC;
		margin: 1px;
		cursor:pointer;
	}
	.paging_full_numbers"
	{
	/*margin-top:8px;*/
	}
	.dataTables_info
	{
		color:#3474D0;
		font-size:14px;

		margin:6px;
	}
	.paginate_active
	{
		padding: 6px;
		border: 1px solid #3474D0;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		color: #3474D0;
		font-weight: bold;
	}
	.dataTables_filter
	{
		float:right;
	}
	.move{
		position: relative;
		left: 48%;
		top: 60px;
	}
	.resize{
		width: 220px;
	}
	.rehieght{
		position: relative;
		top: -5px;
	}
	.dataTables_length{
		position: relative;
		left: -89.3%;
		top: 20px;
		font-size: 1px;
	}
	.dataTables_length select{
		width: 60px;
	}

	.dataTables_filter{
		position: relative;
		top: -60px;
		left: -65px;
	}
	.dataTables_filter input{
		width: 180px;
	}
	.revert{
		margin-left: -15px;
	}
	.marge{
		margin-top: 40px;
	}
</style>
<!-- contents -->

<div class="contents-container">

	<!-- bredcrumb -->




	<script type="text/javascript" src="assets/js/data-tables/jquery.js"></script>
	<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript">

		function doc_file(){
			var r  =confirm('if you are upload this file again !\n\ please follow check out method');
			if(r == true){
				return true;
			}else if(r == false){
				return false;
			}
		}
		function print_leave_status(id)
		{
			window.open('leave_attendance/print_leave_status/'+id,"","width=800,height=600");
		}
		function print_all_leave_status(id)
		{
			window.open('leave_attendance/print_all_leave_status/'+id,"","width=800,height=600");
		}
		function lookup(inputString) {

			if(inputString.length == 0) {
				$('#suggestions').hide();
				$('#employee_inf0').hide();
				location.reload();
			} else {
				$.post("leave_attendance/autocomplete_leave_enetitlement/", {queryString: ""+inputString+""}, function(data){
					if(data.length > 0) {
						$('#suggestions').show();
						$('#autoSuggestionsList').show();
						$('#autoSuggestionsList').html(data);
					}
				});
			}
		}
		function clear_div()
		{document.getElementById('#employee_inf0').innerHTML = "";}

		function fill(thisValue,employee_id) {
			$('#id_input').val(thisValue);
			$('#emp_id').val(employee_id);
			setTimeout("$('#suggestions').hide();", 200);
			//$('#suggestions').hide();
			$('#employee_inf0').show();

			//setTimeout("$('#autoSuggestionsList').hide()", 200);
		}


	</script>

	<div class="contents-container">

		<div class="bredcrumb">Dashboard / Leave&Attendance / Leave Entitlement</div> <!-- bredcrumb -->
		<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>



		<div class="right-contents">

			<div class="head">Leave Entitlement</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>


				<?php echo form_open();?>
				<div class="row">
					<input type="text" name="full_name" id="id_input"  placeholder="Employee Name" class="serch" autocomplete="off" onkeyup="lookup(this.value)">
					<input type="hidden" name="emp_id" id="emp_id" />
					<button class="btn green">Search</button>
					<div class="row2"> <div id="suggestions">
							<div class="autoSuggestionsList_l" id="autoSuggestionsList">
							</div>
						</div>
					</div>
				</div>

				<?php echo form_close();?>
				<!-- table -->
				<table class="marge" cellspacing="0" width="100%">
					<thead class="table-head">
					<tr id="table-row">
						<td>Employee ID</td>
						<td>Employee Name</td>
						<td>Leave Entitlement</td>


						<td><span class="fa fa-eye"></span></td>
					</tr>
					</thead>
					<tbody>
					<?php if(!empty($leave_entitlement)){
					foreach($leave_entitlement as $leave_entitlement){
					?>
					<tbody>
					<tr class="table-row">
						<td><?php echo $leave_entitlement->employee_code;?></td>
						<td><?php echo $leave_entitlement->full_name;?></td>
						<td><?php echo $leave_entitlement->no_of_leaves;?></td>
						<!--<td><?php //echo $leave_entitlement->year;?></td>-->

						<td ><a href="leave_attendance/view_leave_entitlement_detail/<?php echo $leave_entitlement->employee_id;?>"><span class="fa fa-eye"></span></a></td>
					</tbody>
					<?php }}else{?>
						<td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
					<?php }?>
					</tr>
				</table>

				<div id="pagination">
					<ul>
						<?php echo $links; ?>
					</ul>
				</div>
			</div>

		</div>
		<!-- contents -->

		<!-- Menu left side  -->
		<div id="right-panel" class="panel">
			<?php $this->load->view('includes/leave_left_nav.php'); ?>
		</div>
		<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
		<script>
			$('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
			$('#close-panel-bt').click(function() {
				$.panelslider.close();
			});



		</script>
		<!-- leftside menu end -->
