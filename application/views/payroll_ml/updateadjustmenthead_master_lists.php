<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->


			<?php $this->load->view('includes/payroll_master_list_nav'); ?>

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<form action="ml_payrol_site/UpdateAjustmentHead" method="post">

				<div class="row">
					<h4><b>Adjustment Head</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Update Adjustment Head</h4>
					<input type="text" name="adjustmentTitle" value="<?php echo $AjustmentHead->adjustmentTitle;?>" required>
					<input type="hidden" name="adjustmentID" value="<?php echo $AjustmentHead->adjustmentID?>">
				</div>
			<div class="row">
				<h4>Adjustment Type</h4>
				<select name="IsLive" style="width: 299px;">

					<option value="1" <?php if($AjustmentHead->IsLive==1){ echo 'selected="selected"';}?>>Enable</option>

					<option value="0" <?php if($AjustmentHead->IsLive==0){ echo 'selected="selected"';}?>>Disable</option>

				</select>

			</div>


			<!-- button group -->
			<div class="row">
					<input type="submit" class="btn green"  value="Update" name="update" />
			</div>

			</form>
</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');

    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

