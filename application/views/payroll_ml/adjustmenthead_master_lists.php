


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->


			<?php $this->load->view('includes/payroll_master_list_nav'); ?>

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<form action="ml_payrol_site/AddAjustmentHead" method="post">

				<div class="row">
					<h4><b>Tax Adjustment Head</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Adjustment Head</h4>
					<input type="text" name="adjustmentTitle" required>
				</div>

				<!-- button group -->
			<div class="row">
					<input type="submit" class="btn green"  value="Add" name="add" />
			</div>

			</form>


			<!-- table -->

            	<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Adjustment Tax</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                   <?php if (@$AjustmentHead >0){
                  foreach($AjustmentHead as $AjustmentH):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo @$AjustmentH->adjustmentTitle;?></td>
                    <?php if(@$AjustmentH->IsLive == 1):?>
                    <td style="color: green"><?php echo "Enabled"; ?></td>
                    <?php else:?>
                    <td style="color: red"><?php echo "Disabled"?></td>
                    <?php endif;?>
		<td><a href="ml_payrol_site/UpdateAjustmentHead/<?php echo @$AjustmentH->adjustmentID?>" /><span class="fa fa-pencil"></span></a></td>
		<td><a href="ml_payrol_site/DeleteAjustmentHead/<?php echo @$AjustmentH->adjustmentID?>"/><span class="fa fa-trash-o" onclick="return confirm('Are You Sure')"></span></a></td>
				</tr>
					<?php endforeach; ?>
                 <?php } else { echo "<div class=\"result\">No Record Found..</div>";} ?>

			</table>




		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');

	<?php if(!empty($this->session->flashdata('msg'))){?>
	var message = "<?php echo $this->session->flashdata('msg')?>";
	setInterval(function(){ var data = message.split("::");
		Parexons.notification(data[0],data[1]); });
	var data = message.split("::");
	Parexons.notification(data[0],data[1]);
	<?php }?>
    $(document).ready(function(e){



    });
</script>

