<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->

	
			<?php $this->load->view('includes/payroll_master_list_nav.php'); ?>	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<form action="ml_payrol_site/add_payment_type" method="post">

				<div class="row">
					<h4><b>Payment Types</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Payment Types</h4>
					<input type="text" name="payment_type_title" required>
				</div>
				
				<!-- button group -->
			<div class="row">
                    
                       <input type="submit" class="btn green"  value="Add" name="add" />
					<!--<a href="add.php"><button class="btn green">Add Payment Mode</button></a>-->
			</div>
				
			</form>


			<!-- table -->
             <table cellspacing="0">
				<thead class="table-head">
					<td>Payment Mode</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                 <?php if ($data >0){ 
				 foreach($data as $payment_type):?>
				<tr class="table-row">
					
					<td><?php echo $payment_type->payment_type_title; ?></td>
                    <?php if($payment_type->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
                <td><a href="ml_payrol_site/edit_payment_type/<?php echo $payment_type->payment_type_id;?>"><span class="fa fa-pencil"></span></a></td>
			<td><a href="ml_payrol_site/trashed_payment_type/<?php echo $payment_type->payment_type_id;?>/<?php echo 'payment_type_id';?>" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
               
				<?php endforeach; ?>
                 <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
				
			</table>
            
            
            
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Payment Mode</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Payment Mode</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

