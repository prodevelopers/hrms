<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    .mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }
    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
    .move{
        position: relative;
        left: 48%;
        top: 60px;
    }
    .resize{
        width: 220px;
    }
    .rehieght{
        position: relative;
        top: -5px;
    }
    .dataTables_length{
        position: relative;
        font-size: 0px;
    }
    .dataTables_length select{
        width: 100px;
        float:right;
        margin-right:11px;
    }
    .fa-pencil:hover{cursor: pointer}
    .dataTables_filter{
        position: relative;
        top: -21px;
        left: -19%;
    }
    .dataTables_filter input{
        width: 180px;
    }
    .revert{
        margin-left: -15px;
    }
    .marge{
        margin-top: 8%;
    }

    /*tooltip css*/
    [title]{
        position:relative;

    }
    [title]:after{
        content:attr(title);
        color:#000000;
        font-family: arial, sans-serif;
        background:#b0fbf1;
        padding:6px;
        position:absolute;
        left:-9999px;
        border-radius: 5px;
        opacity:0;
        bottom:100%;
        white-space:nowrap;
        -webkit-transition:0.25s linear opacity;
    }
    [title]:hover:after {
        left: 5px;
        opacity: 1;

    }
    #table_list{width:auto important;}


    /*Add Employee Green Button*/
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }

    /*tooltip css*/
</style>

<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/payroll_master_list_nav'); ?>
    <div class="right-contents1">
        <div class="head">Tax Slabs <span id="financialYearHead">(2015 - 2016)</span>
            <div class="btn-right">
                <a href="<?php echo base_url(); ?>ml_payrol_site/add_tax_slabs" class="btn green">Add Tax Slab</a>
            </div></div>
        <!-- filter -->
        <div class="filter">
            <h4>Filter By
                <?php echo form_open();?>
                <?php echo form_dropdown('FinancialYear',$financialYears,'','id="FinancialYear"'); ?>
                <?php echo form_close();?>
            </h4>

        </div>
        <!-- table -->
        <div class="table-width">
        <table cellpadding="0" cellspacing="0" id="tax_slabs_list" class="marge">
                <thead class="table-head">
                <tr>
                <td>SlabID</td>
                <td>From</td>
                <td>To</td>
                <td>Fixed Amount Addition</td>
                <td>Rate of Tax %</td>
                <td width="140">Action</td>
                </tr>
                </thead>
            <tbody>
            </tbody>
        </table>
        </div>
    </div>
</div>
<!-- contents -->
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
    $(function(){
        oTable ='';
        var taxSlabsTableSelector = $('#tax_slabs_list');
        var url_DT = "<?php echo base_url(); ?>ml_payrol_site/list_tax_slabs";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "SlabID",
                "bVisible":false,
                "bSearchable":false
            },
            /* Annual Income From */ {
                "mData" : "AnnualIncomeFrom"
            },
            /* Annual Income To */ {
                "mData" : "AnnualIncomeTo"
            },
            /* Fixed Amount */ {
                "mData" : "FixedAmount"
            },
            /* Remarks */ {
                "mData" : "Remarks"
            },
            /* Action Buttons */ {
                "mData" : "ActionButtons"
            }
        ];
        var HiddenColumnID_DT = 'SlabID';
        var sDom_DT = '<"H"r>lt<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        var FinancialYear = $("#FinancialYear").val();
        if(FinancialYear === null || FinancialYear.length === 0){
            FinancialYear = 0;
        }
        var filters = 'aoData.push({"name":"FinancialYear","value":'+FinancialYear+'});';
        commonDataTablesFiltered(taxSlabsTableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);

        if($("#FinancialYear").val().length > 0){
            $("#financialYearHead").text("("+$("#FinancialYear option:selected").text()+")");
        }
        $("#FinancialYear").on("change",function(){
            location.reload();
        });
    });
</script>
<script>
    $("#alert").delay(3000).fadeOut('slow');

    $("body").on("click","#EditTaxSlab",function(){
        $.ajax({
            url:'<?php echo base_url() ?>ml_payrol_site/edit_tax_slabs',
        });
    });
    $(document).ready(function(e){
        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
        if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
        }
        ?>
    });
</script>