<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
    .mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }
    .move{
        position: relative;
        left: 48%;
        top: 60px;
    }
    .resize{
        width: 220px;
    }
    .rehieght{
        position: relative;
        top: -5px;
    }
    .dataTables_length{
        position: relative;
        font-size: 0px;
    }
    .marge{
        margin-top: 8%;
    }

    /*tooltip css*/
    [title]{
        position:relative;

    }
    [title]:after{
        content:attr(title);
        color:#000000;
        font-family: arial, sans-serif;
        background:#b0fbf1;
        padding:6px;
        position:absolute;
        left:-9999px;
        border-radius: 5px;
        opacity:0;
        bottom:100%;
        white-space:nowrap;
        -webkit-transition:0.25s linear opacity;
    }
    [title]:hover:after {
        left: 5px;
        opacity: 1;

    }


    /*Add Employee Green Button*/
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }
    .col-xs-3{
        float: left;
        width: 17%;
        margin: 1%;
    }
    /*tooltip css*/
</style>

<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/payroll_master_list_nav'); ?>
<!-- ---------------------Add TAX Slab --------------------------------------->
    <div class="right-contents1">
        <div class="head">Update Tax Slab
            <div class="btn-right">
                <a href="<?php echo base_url(); ?>ml_payrol_site/view_tax_slabs" class="btn green">Back</a>
            </div></div>
        <?php

        ?>
        <?php echo form_open("ml_payrol_site/update_taxSlab_info",'id="taxSlabForm"');?>
        <div class="filter" style="width: auto;border-bottom: 1px solid gray">
            <h4 >Financial Year</h4>
            <?php
            $selected_year = (isset($taxSlab->TaxYearID) ? $taxSlab->TaxYearID : '');
            echo form_dropdown('FinancialYear',$financialYears,$selected_year,'id="FinancialYear"'); ?>

            <div class="clear"></div>
        </div>
        <!----------Add more text field------>
        <div class="count taxSlabBox" >
            <p style="margin-left: 1%" >Tax Slabs</p>
            <div  class="formBox" style="float: left; max-width: 100% !important; min-width: 100% !important;">
                <div class="form-group">
                    <div class="col-xs-3">
                        <input type="hidden" name="hiddenID" id="hiddenID" value="<?php if(isset($taxSlab->TaxSlabID)){echo $taxSlab->TaxSlabID;} ?>">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="AnnualIncomeFrom" class="form-control" name="AnnualIncomeFrom" value="<?php if(isset($taxSlab->AnnualIncomeFrom)){echo $taxSlab->AnnualIncomeFrom;} ?>" onkeypress='return isNumberKey(event)'>
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="AnnualIncomeTo" class="form-control" name="AnnualIncomeTo" value="<?php if(isset($taxSlab->AnnualIncomeTo)){echo $taxSlab->AnnualIncomeTo;} ?>" onkeypress='return isNumberKey(event)'>
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="TaxRate" class="form-control TaxRate" name="TaxRate" value="<?php if(isset($taxSlab->TaxRate)){echo $taxSlab->TaxRate;} ?>" onkeyup="CheckIsVarchar(this);">
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="FixedAmount" class="form-control" name="FixedAmount" value="<?php if(isset($taxSlab->FixedAmount)){echo $taxSlab->FixedAmount;} ?>" onkeypress='return isNumberKey(event)'>
                    </div>

                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="Remarks" class="form-control" name="Remarks" value="<?php if(isset($taxSlab->Remarks)){echo $taxSlab->Remarks;} ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="filter" style="margin-left: 1%">
            <input type="submit" id="updateTaxSlab" value="Update Tax Slab" class="btn green" />
        </div>
        <?php echo form_close();?>
        </div>
</div>
<!-- contents -->
<script>
    $("#alert").delay(3000).fadeOut('slow');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode >38 && (charCode < 48 || charCode > 57));
    }
    function CheckIsVarchar(obj) {
        var inputValue = $(obj).val();
        if(inputValue.length !== 0){
            if(!$.isNumeric(inputValue) || inputValue > 100){
                Parexons.notification("Please Only Numeric Value And Less then 100 Allowed in TaxRate Field","warning");
                $(obj).val("");
            }
        }
    }
    // for insertion
    $("#updateTaxSlab").on("click",function(){
        var FinancialYear    =$("#FinancialYear").val();
        var AnnualIncomeFrom =$("#AnnualIncomeFrom").val();
        var AnnualIncomeTo   =$("#AnnualIncomeTo").val();
        var FixedAmount      =$("#FixedAmount").val();
        var TaxRate          =$("#TaxRate").val();
        var Remarks          =$("#Remarks").val();
        if(FinancialYear.length == ""){
            Parexons.notification("Please Select Financial Year From DropDown","error");
            return false;
        }
        if(AnnualIncomeFrom.length == 0){
            Parexons.notification("Please Fill Annual Income From Field","error");
            return false;
        }
        if(AnnualIncomeTo.length == 0){
            Parexons.notification("Please Fill Annual Income To Field","error");
            return false;
        }
        if(FixedAmount.length == 0){
            Parexons.notification("Please Fill FixedAmount Field","error");
            return false;
        }
        if(TaxRate.length == 0){
            Parexons.notification("Please Fill TaxRate Field","error");
            return false;
        }
        $(this).parents("form").submit();
    });




    $(function(){
        $("body").on("keyup",".TaxRate",function(){
            var AnnualIncomeFrom = $(this).parents("div.formBox").find("#AnnualIncomeFrom").val();
            var AnnualIncomeTo = $(this).parents("div.formBox").find("#AnnualIncomeTo").val();
            var TaxRate = $(this).val();
            var sub = AnnualIncomeTo - AnnualIncomeFrom;
            var total  =   sub * TaxRate;
            var percent=total/100;
            var FixedAmount = $(this).parents("div.formBox").find("#FixedAmount").val(percent);

        });
    });

    $(document).ready(function(e){
        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
        if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
        }
        ?>
    });
</script>
