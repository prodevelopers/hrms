
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/payroll_master_list_nav'); ?>	
	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>

		<form action="ml_payrol_site/add_currency" method="post">

				<div class="row">
					<h4><b>Currency</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Currency</h4>
					<input type="text" name="currency_name" required>
				</div>
				
				<!-- button group -->
			<div class="row">
					<input type="submit" class="btn green"  value="Add" name="add" />
			</div>
				
			</form>


			<!-- table -->
            <form id="active_currency">
            	<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Currency</td>
                    <td>Status</td>
                    <td>Active </td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                 <?php if ($data >0){
                  foreach($data as $currency):?>

				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->

					<td><?php echo $currency->currency_name?></td>
                    <?php if($currency->enabled == 1):?>
                    <td><?php echo "Enabled"; ?>
                        <input type="hidden" name="id" id="id" value="<?php echo $currency->ml_currency_id; ?>"></td>

                    <?php if($currency->active == 1){?>
                    <td><input type="radio" name="active" id="active" value="<?php echo $currency->ml_currency_id; ?>" class="active" checked></td>

                    <?php }else{?>
                    <td>
                        <input type="radio" name="active" id="active" value="<?php echo $currency->ml_currency_id; ?>" class="active" ></td>

                    <?php }?>
                    <?php else:?>
                    <td><?php echo "Disabled"?></td>
                    <input type="radio" name="active" id="active" class="active" disabled>
                    <?php endif;?>
		<td><a href="ml_payrol_site/edit_currency/<?php echo $currency->ml_currency_id?>" /><span class="fa fa-pencil"></span></a></td>
		<td><a href="ml_payrol_site/trashed_currency/<?php echo $currency->ml_currency_id?>/<?php echo 'ml_currency_id';?>" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
                  <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
			</table>
            </form>
            
            
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Currency</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Currency</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->

			

		</div>

	</div>
<!-- contents -->

<script>
	$("#alert").delay(3000).fadeOut('slow');
    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>

        $(".active").on('click',function(){
            var formData=$("#active_currency").serialize();
            $.ajax({
                url:"ml_payrol_site/active_currency",
                data:formData,
                type:"POST",
                success:function(result){
                    var data=result.split('::')
                    if(data[0] === "OK")
                    {
                        Parexons.notification(data[1],data[2]);
                        return;
                    }else
                    {
                        Parexons.notification(data[1],data[2]);
                        return;
                    }
                }
            });
        });
    });
</script>
