<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/payroll_master_list_nav'); ?>	
	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<form action="ml_payrol_site/add_deduction" method="post">

				<div class="row">
					<h4><b>Deduction Heads</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Deduction Heads</h4>
					<input type="text" name="deduction_type" required>
				</div>
				
				<!-- button group -->
			<div class="row">
					<input type="submit" class="btn green"  value="Add" name="add" />
			</div>
				
			</form>


			<!-- table -->
             	<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Deduction Heads</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                   <?php if ($data >0){
                   foreach($data as $deduction):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo $deduction->deduction_type ?></td>
                    <?php if($deduction->enabled == 1):?>
                    <td><?php echo "Enabled"; ?></td>
                    <?php else:?>
                    <td><?php echo "Disabled"?></td>
                    <?php endif;?>
 <td><a href="ml_payrol_site/edit_deduction/<?php echo $deduction->ml_deduction_type_id?>" /><span class="fa fa-pencil"></span></a></td>
<td><a href="ml_payrol_site/trashed_Deduction/<?php echo $deduction->ml_deduction_type_id?>/<?php echo 'ml_deduction_type_id'?>" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
			</table>
            
            
            
		<!--	<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Deduction Heads</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Deduction Heads</td>
					<td>20%</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

