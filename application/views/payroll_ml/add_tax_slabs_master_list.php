<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<style type="text/css">
    .mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }
    .move{
        position: relative;
        left: 48%;
        top: 60px;
    }
    .resize{
        width: 220px;
    }
    .rehieght{
        position: relative;
        top: -5px;
    }
    .dataTables_length{
        position: relative;
        font-size: 0px;
    }
    .marge{
        margin-top: 8%;
    }

    /*tooltip css*/
    [title]{
        position:relative;

    }
    [title]:after{
        content:attr(title);
        color:#000000;
        font-family: arial, sans-serif;
        background:#b0fbf1;
        padding:6px;
        position:absolute;
        left:-9999px;
        border-radius: 5px;
        opacity:0;
        bottom:100%;
        white-space:nowrap;
        -webkit-transition:0.25s linear opacity;
    }
    [title]:hover:after {
        left: 5px;
        opacity: 1;

    }


    /*Add Employee Green Button*/
    .btn-right {
        float: right;
        height: 25px;
        margin-top: -6px;
        width: auto;
    }
    .col-xs-3{
        float: left;
        width: 17%;
        margin: 1%;
    }
    /*tooltip css*/
</style>

<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / Payroll / Master Lists</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/payroll_master_list_nav'); ?>
<!-- ---------------------Add TAX Slab --------------------------------------->
    <div class="right-contents1">
        <div class="head">Add New Tax Slab
            <div class="btn-right">
                <a href="<?php echo base_url(); ?>ml_payrol_site/view_tax_slabs" class="btn green">Back</a>
            </div></div>
        <?php echo form_open("ml_payrol_site/add_taxSlab_info",'id="taxSlabForm"');?>
        <div class="filter" style="width: auto;border-bottom: 1px solid gray">
            <div class="notice info">
                <p>Instruction</p>
                <ul>
                    <li>Click <strong> ( <i class="fa fa-plus"></i> ) </strong> Icon to add more text fields.</li>
                    <li>Click <strong> ( <i class="fa fa-minus"></i> ) </strong> Icon to remove text fields.</li>
                </ul>
            </div>
            <h4 >Financial Year</h4>

            <?php echo form_dropdown('FinancialYear',$financialYears,'','id="FinancialYear"'); ?>

            <div class="clear"></div>
        </div>
        <!----------Add more text field------>
        <div class="count taxSlabBox" >
            <p style="margin-left: 1%" >Tax Slabs</p>
            <div  class="formBox" style="float: left; max-width: 100% !important; min-width: 100% !important;">
                <div class="form-group">
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="AnnualIncomeFrom" class="form-control" name="AnnualIncomeFrom[]" placeholder="Annual Income From" onkeypress='return isNumberKey(event)'>
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="AnnualIncomeTo" class="form-control" name="AnnualIncomeTo[]" placeholder="Annual Income To" onkeypress='return isNumberKey(event)'>
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" class="form-control TaxRate" name="TaxRate[]" placeholder="Tax Rate %" onkeyup="CheckIsVarchar(this);">
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="FixedAmount" class="form-control" name="FixedAmount[]" placeholder="Fixed Amount" onkeypress='return isNumberKey(event)'>
                    </div>
                    <div class="col-xs-3">
                        <input style="max-width:95% !important;min-width: 95% !important;" type="text" id="Remarks" class="form-control" name="Remarks[]" placeholder="Remarks">
                    </div>
                </div>
            </div>
            <div class="BtnBox" id="addButton" style="float:left; width: 0.1%; margin-left:-5%;">
                <div class="plusBtnDiv">
                    <span class="btn btn-default fa fa-plus"></span>
                </div>
            </div>
        </div>
        <div class="filter" style="margin-left: 1%">
            <input type="submit" name="addTaxSlabs" id="AddTaxSlab" value="Add Tax Slab" class="btn green" />
        </div>
        <?php echo form_close();?>
        </div>
</div>
<!-- contents -->
<script>
    $("#alert").delay(3000).fadeOut('slow');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        return !(charCode >38 && (charCode < 48 || charCode > 57));
    }

    //Function for add more text field
    var taxSlabAddButton = $("#addButton").find("span.fa-plus");
    taxSlabAddButton.on("click",function(){

        var totalItems = $("div.count").length;
        var newNumber = totalItems+1;
        var oldNumber = totalItems-1;
        console.log(totalItems);

        var taxSlabBox = '<div class="count taxSlabBox-'+totalItems+'">'
            + '<div class="formBox" style="float: left; width: 99.99%;">'
            + '<div class="form-group">'
            + '<div class="col-xs-3">'
            + '<input name="AnnualIncomeFrom[]" class="form-control" id="AnnualIncomeFrom" placeholder="Annual Income From" style="text-indent:3%;width:100%;height: 28px;" onkeypress="return isNumberKey(event)">'
            + '</input>'
            + '</div>'
            + '<div class="col-xs-3">'
            + '<input name="AnnualIncomeTo[]" class="form-control" id="AnnualIncomeTo" placeholder="Annual Income To" style="text-indent:3%;width:100%;height: 28px;" onkeypress="return isNumberKey(event)">'
            + '</input>'
            + '</div>'
            + '<div class="col-xs-3">'
            + '<input name="TaxRate[]" class="form-control TaxRate" placeholder="Tax Rate %" style="text-indent:3%;width:100%;height: 28px;" onkeyup="CheckIsVarchar(this);">'
            + '</input>'
            + '</div>'
            + '<div class="col-xs-3">'
            + '<input name="FixedAmount[]" class="form-control" id="FixedAmount" placeholder="Fixed Amount" style="text-indent:3%;width:100%;height: 28px;" onkeypress="return isNumberKey(event)">'
            + '</input>'
            + '</div>'
            + '<div class="col-xs-3">'
            + '<input name="Remarks[]" class="form-control" id="Remarks" placeholder="Remarks" style="text-indent:3%;width:100%;height: 28px;">'
            + '</input>'
            + '</div>'
            +  ' </div>'
            +  ' </div>'
            +  ' <div style="float:left; width: 0.1%; margin-left:-5%;" id="addButton" class="BtnBox">'
            +  ' <div class="plusBtnDiv">'
            +  ' <span class="btn btn-default fa fa-minus"></span>'
            +  ' </div>'
            +  ' </div>'
            +  ' </div>';

        if(totalItems == 1){
            $(".taxSlabBox").after(taxSlabBox);
        }else{
            $(".taxSlabBox-"+oldNumber).after(taxSlabBox);
        }
    });

    //End function for add more text field
    $("body").on("click","span.fa-minus",function(){
        //First Lets Check if it already exists in DB or not, If it does, then we also need to remove it from DB also.
        var parentDiv = $(this).parents("div.count");
        $(this).parents("div.count").remove();
    });

    function CheckIsVarchar(obj) {
        var inputValue = $(obj).val();
        if(inputValue.length !== 0){
            if(!$.isNumeric(inputValue) || inputValue > 100){
                Parexons.notification("Please Only Numeric Value And Less then 100 Allowed in TaxRate Field","warning");
                $(obj).val("");
            }
        }
    }
    // for insertion
    $("#AddTaxSlab").on("click",function(){
        var FinancialYear    =$("#FinancialYear").val();
        var AnnualIncomeFrom =$("#AnnualIncomeFrom").val();
        var AnnualIncomeTo   =$("#AnnualIncomeTo").val();
        var FixedAmount      =$("#FixedAmount").val();
        var TaxRate          =$("#TaxRate").val();
        var Remarks          =$("#Remarks").val();
        if(FinancialYear.length == ""){
            Parexons.notification("Please Select Financial Year From DropDown","error");
            return false;
        }
        if(AnnualIncomeFrom.length == 0){
            Parexons.notification("Please Fill Annual Income From Field","error");
            return false;
        }
        if(AnnualIncomeTo.length == 0){
            Parexons.notification("Please Fill Annual Income To Field","error");
            return false;
        }
        if(FixedAmount.length == 0){
            Parexons.notification("Please Fill FixedAmount Field","error");
            return false;
        }
        if(TaxRate.length == 0){
            Parexons.notification("Please Fill TaxRate Field","error");
            return false;
        }
        $(this).parents("form").submit();
    });


    $(function(){
        $("body").on("keyup",".TaxRate",function(){
            var AnnualIncomeFrom = $(this).parents("div.formBox").find("#AnnualIncomeFrom").val();
            var AnnualIncomeTo = $(this).parents("div.formBox").find("#AnnualIncomeTo").val();
            var TaxRate = $(this).val();
            var sub = AnnualIncomeTo - AnnualIncomeFrom;
            var total  =   sub * TaxRate;
            var percent=total/100;
            var FixedAmount = $(this).parents("div.formBox").find("#FixedAmount").val(percent);

        });
    });





    $(document).ready(function(e){
        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
        if(!empty($message) && isset($message)){
        echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
        }
        ?>
    });
</script>
