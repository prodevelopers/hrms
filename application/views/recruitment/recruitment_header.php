<!DOCTYPE html>
<html lang="en">
<?php
/*echo "<pre>";
    var_dump($this->session->userdata);
echo "</pre>";*/
//exit;
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Recruitment Model</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/recruitement.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/js/data-tables/dataTables.bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/data_tables/css/dataTables.responsive.css" rel="stylesheet">

    <!--    Script Tags-->
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>

    <!--    Bootstrap-->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jasny-bootstrap.min.js" type="text/javascript"></script>
    <!--    DataTables-->
    <script src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-tables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/data-tables/dataTables.responsive.js" type="text/javascript"></script>
    <!--    <script src="--><?php //echo base_url();?><!--assets/data_tables/js/dataTables.responsive.js" type="text/javascript"></script>-->
    <!--    Select 2 Library-->
    <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/select2/select2.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/select2/select2.min.js" type="text/javascript"></script>
    <!-- script references -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/noty/packaged/jquery.noty.packaged.min.js"></script>
    <!--    My Custom Defined Scripts-->
    <script src="<?php echo base_url(); ?>assets/js/Parexons.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/customScripting.js" type="text/javascript"></script>

        <!--Date Picker-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function(){
            $('input[type="text"]').keyup(function(event) {
                $(this).val(($(this).val().substr(0,1).toUpperCase())+($(this).val().substr(1)));
            });
        });
    </script>

    <style>
        .navbar-inverse{background-color: #5CB85C;}
        .navbar{min-height: 30px;}

    </style>
</head>

<body>

<!-- Navigation -->
<style>
    .navbar-bright{background:#632f53;height: 50px;}
    .navbar-default{background:#632f53;}
    #masthead{background: #6a6c5f}
    .navbar-nav.navbar-right:last-child{margin-right: 0px;}
    .tab_below {
        padding-top: 30px;
    }
    .tab-setting{margin-top: 20px;margin-left: 15px;margin-right: 15px;}

    .usr{margin-right: 10px;}
    .logout{padding-right: 20px;}
    ul li a:hover{background: none;}
    .nav .open > a, .nav .open > a:hover, .nav .open > a:focus{background: none;}
    .dropdown-menu{max-width: 170px important;}
    .drpdwn{margin-left: -90px;background: #d3d3d3}
 li:hover{cursor: pointer}
    ul li a:hover{cursor: pointer}
    .imgcircle{width:40px;height:30px;display: inline-block}
    .srch-style{width:220px;margin-top: 8px;}

    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav >
    .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{background: none;}
    .navbar-default .navbar-collapse, .navbar-default .navbar-form{border-color:#646464;}
    .navbar-default .navbar-nav > li > a{line-height: 30px;}
    .navbar-right{line-height: 38px;}
</style>


<style>
    .top-pic{width:30px;height:30px;border-radius: 5px;margin-top: -6px;}
    .usr{color:white;}
    .a-style{margin-top: -29px;}
    #drp_dwn_lst{
        background: lightgray;
        margin-left: 5px;
        margin-right: 5px;
        border-top-left-radius:5px;
        border-top-right-radius: 5px;
    }
    #drp_hover a:hover{background: none;}

    .checkbox a:hover{
        text-decoration: underline;
    }
</style>
<div class="custom">
<nav class="navbar navbar-default" style="margin-top: -50px;border:none;border-radius: 0px;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle colylapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="color:white;" href="#">Job Portal</a>
        </div>
<style>
    .hr_style{margin-bottom: 5px;margin-top: 5px;}
</style>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                            <?php
                            $ApplicantAvatar = empAvatarExist($this->session->userdata('applicantAvatar'));
                            ?>
                    <?php
                    if(LoggedIn() === TRUE){?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo ($ApplicantAvatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('applicantAvatar'): $ApplicantAvatar ?>"  class="top-pic"  id="img" width="30" height="30" style="display: inline-block"> <b class="caret"></b></a>

                            <ul class="dropdown-menu" id="drp_dwn_lst" role="menu" >

                                <li id="drp_hover"><a href="<?php echo base_url('recruitment/logout');?>" ><span class="fa fa-power-off" style="padding-right: 15px;"></span>Log Out</a></li><!--<hr class="hr_style">-->

                            </ul> <?php }?> <?php
                                if(LoggedIn() === FALSE){
                                    echo '<li style="border-top: 1px solid #646464;height:42px;" data-toggle="modal" data-target="#myModal"><a href="#about"><span class="fa fa-user fa-2x usr" ></span></a></li>';
                                }?>
                    </li>

                </ul>


            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <!--<button type="submit" class="btn btn-default">Submit</button>-->
            </form>
        </div><!-- /.navbar-collapse -->

    </div><!-- /.container-fluid -->
</nav>
</div>
<!---------------User Registration and login pages models----------------->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Login</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#login" data-toggle="tab">Login</a></li>
                    <li role="presentation"><a href="#createAccount" data-toggle="tab">Create Account</a></li>
                </ul>
                <div class="tab-content">
                    <div id="login" class="tab-pane active tab-setting">
                        <form class="form-horizontal tab_below" id="applicantLoginForm">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-3">Username</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" name="UserName" id="username" placeholder="Username">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-8">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="remember_me"> Remember me</label>
                                        <a style="color: #428bca" role="presentation" class="pull-right" data-toggle="modal" data-target=".forgotPassword" href="#">Forgot your password?</a>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="loginbutton" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                    </div>
                    <!--------------Registration Form------------------------------------------------>

                    <div id="createAccount" class="tab-pane tab-setting">
                        <form class="form-horizontal tab_below" id="registerForm">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-3">First Name</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" name="FirstName" id="inputEmail" placeholder="First Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">Last Name</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" name="LastName" id="inputPassword" placeholder="Last Name">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">User Name</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" name="UserName" id="UserName" placeholder="User Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" name="Password" id="Password" placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-3">Confirm Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" id="conf_pass" onkeyup="return valid()" placeholder="Confirm Password">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button"  id="Register" class="btn btn-primary">Register</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!---------------User Registration and login pages models----------------->
<div class="modal fade forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style="margin:10% auto;width: 50%;" id="modal_qualification">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="padding-top: 5%;padding-bottom: 5%;">
                    <form  id="forgotPassword_form">
                        <!----------when selected then form appears------>
                        <div class="row">
                            <div class="form-group">
                                <label for="Email" class="control-label col-xs-2 lbl">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" placeholder="Email" name="EmailAddress" id="EmailAddress">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer mfooter">
                    <div class="row" style="text-align: right;">
                        <!--<form id="test"><input type="text" name="tttt" value="dasdasda"> </form>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="forgotPasswordBtn">Send Password</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="masthead">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size: 50px">
                    <?php echo isset($pageTitle)?$pageTitle:''; ?>
                    <p class="lead"></p>
                </h1>
            </div>
        </div>
    </div><!-- /cont -->

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top_spacer">

                </div>
            </div>
        </div>
    </div>
</div>
