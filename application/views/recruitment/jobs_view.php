<?php $pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <!--/stories-->
                    <!-- filter -->
                    <div class="filter">

                        <?php echo form_open();?>
                        <h4>Filter By</h4>

                        <input type="hidden" name="selectDepartments" id="selectDepartments">
                        <?php echo form_close();?>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="jobCareersTable" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Advertisement ID</th>
                                        <th data-class="expand">Job Title</th>
                                        <th data-hide="phone,tablet">Department</th>
                                        <th data-hide="phone">Minimum Qualification</th>
                                        <th data-hide="phone">Salary</th>
                                        <th data-hide="phone">Age</th>
                                        <th data-hide="phone,tablet">Date of Expiry</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <hr>

                </div>
            </div>
        </div><!--/col-12-->
    </div>
</div>


<hr>
<script>
    var oTable;
    $(document).ready(function (e) {

        //Select department
        var LoadDepartment = $('#Department');
        var url = "<?php echo base_url(); ?>recruitment/loadDepartment";
        var id = "DepartmentID";
        var text = "DepartmentName";
        var minInputLength = 0;
        var placeholder = "Select Department";
        var multiple = false;
        commonSelect2(LoadDepartment,url,id,text,minInputLength,placeholder,multiple);

        ///////////////////////////Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        //Filter Selectors
        //Select department
        var selectDepartmentsSelector = $('#selectDepartments');
        var url = "<?php echo base_url(); ?>recruitment/loadDepartment";
        var id = "DepartmentID";
        var text = "DepartmentName";
        var minInputLength = 0;
        var placeholder = "Select Department";
        var multiple = false;
        commonSelect2(selectDepartmentsSelector,url,id,text,minInputLength,placeholder,multiple);
        // filter datatable
        selectDepartmentsSelector.on('change',function(e){
            oTable.fnDestroy();
            var filteredselectDepartments = $(this).val();
            var filters = 'aoData.push({"name":"filteredselectDepartments","value":'+$(this).val()+'});';
            commonDataTablesFiltered(tableSelector,url,aoColumns,sDom,HiddenColumnID,filters);
        });

        oTable = '';
        var tableSelector = $('#jobCareersTable');
        var url = "<?php echo base_url(); ?>recruitment/careers/list";
        var aoColumns = [
            /* ID */ {
                "mData": "AdvertisementID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /*Title*/
            {
                "mData" : "Title"
            },
            /* Department Name*/ {
                "mData" : "DepartmentName"
            },
            /* Min Qualification */ {
                "mData" : "MinQualification"
            },
            /* Salary Range */ {
                "mData" : "SalaryRange"
            },
            /* Age Limit */ {
                "mData" : "AgeLimit"
            },
            /* Expiry Date */ {
                "mData" : "ExpDate"
            },
            /* Status Button */ {
                "mData" : "detailsViewButton"
            }
        ];
        var HiddenColumnID = 'AdvertisementID';
        var sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone : 480
        };
        oTable = tableSelector.dataTable({
            sPaginationType: 'bootstrap',
            oLanguage : {
                sLengthMenu: '_MENU_ records per page'
            },
            "autoWidth" : false,
            "aoColumns":aoColumns,
            "bServerSide":true,
            "bProcessing":true,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom" : sDom,
            "sAjaxSource": url,
            "iDisplayLength": 25,
            "aLengthMenu": [[2, 25, 50, -1], [2, 25, 50, "All"]],
            'fnServerData' : function(sSource, aoData, fnCallback){
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': url,
                    'data': aoData,
                    'success': fnCallback
                }); //end of ajax
            },
            'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("data-id",aData[HiddenColumnID]);
                if(typeof RowCallBack !== "undefined"){
                    eval(RowCallBack);
                }
                responsiveHelper.createExpandIcon(nRow);
                return nRow;
            },
            fnPreDrawCallback: function () {
// Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableSelector, breakpointDefinition);
                }
            },
            fnDrawCallback : function (oSettings) {
// Respond to windows resize.
                responsiveHelper.respond();
                if(typeof DrawCallBack !== "undefined"){
                    eval(DrawCallBack);
                }
            }
        });

        //Script for Modal, When Clicked On Eye Then Load Data in View Modal.
        tableSelector.on('click','.detailView',function(e){
            //Load Job Details Page For This Particular Selected Job
            var advertisedJobID = $(this).closest('tr').attr('data-id');
            window.location.href = '<?php echo base_url() ?>recruitment/jobs_details_view/'+advertisedJobID;
        });

        //Table Filter
        $('.search').on('keyup',function(e){
            oTable.fnFilter($(this).val());
        });

        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>


    });
</script>
<style>
    .modal-backdrop{z-index: 0}
</style>

<style>
    .modal-header{
        background: #357EBD;
        border-top-right-radius:10px;
        border-top-left-radius:10px;
        color:white;
    }
    .modal-content{
        border-top-right-radius:10px;
        border-top-left-radius:10px;
    }
    .close{color:white;}
    .close:hover{color:white;}
    /** $glow-color: rgba( 255, 0, 0, 0.4); // red for errors **/
    /** $glow-color: rgba( 0, 255, 0, 0.4); // green for success **/
    /** $glow-color: rgba( 255, 200, 0, 0.4); // orange for warning **/
    .overlay {
        background:lightblue;
        border:1px solid lightblue;
        -moz-box-shadow: 1px 1px 150px rgba(255, 255, 255, 0.4), -1px -1px 150px rgba(255, 255, 255, 0.4), 0 0 0 9999px rgba(0, 0, 0, 0.5);
        -webkit-box-shadow: 1px 1px 150px rgba(255, 255, 255, 0.4), -1px -1px 150px rgba(255, 255, 255, 0.4), 0 0 0 9999px rgba(0, 0, 0, 0.5);
    }
    .overlay * {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    .blockquote {
        background: whitesmoke;
        border-left: 0.5em solid transparent;
        margin: 5px;
        padding: 0.5em;
        font-style: italic;
    }
    .blockquote.he {
        border-right: 0.5em solid darkmagenta;
    }
    .blockquote.we {
        border-left: 0.5em solid darkcyan;
    }
    .row-detail li {
        background: none repeat scroll 0 0 #f9f9f9;
        border-radius: 5px;
        display: block;
        margin-bottom: 2px;
        padding-left: 5px;
    }
</style>