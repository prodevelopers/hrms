<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <p class="pull-right">
                    Powered <i class="icon-heart-empty"></i> By
                    <a href="http://parexons.com/">Parexons</a></p>
            </div>
        </div>
    </div>
</footer>
<script>
    $(document).ready(function() {
        function valid() {
            if ($("#email").val()) {
                $("#warn").append("<p style='color: red'>plz enter email.....!</p>");
            }
            if ($('#Password').val() != $('#conf_pass').val()) {
                $("#warn").append("<p style='color: red'>Sorry Password Doesn't Match.....!</p>");
                return false;
            } else {
                $("#warn").hide();
                return true;
            }
        }

        /// Insertion for Applicant Register Using ajax //
        $("#login").on('click','#loginbutton',function(e){
            e.preventDefault();
            var formData = $('#applicantLoginForm').serialize();
            $.ajax({
                url: "<?php echo base_url()?>recruitment/applicantLogin/",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var splitCharacter = '>>>';
                    var data;
                    var topData;
                    if(output.indexOf(splitCharacter) > -1){
                        topData = output.split('>>>');
                        data = topData[0].split('::');
                    }else{
                        data = output.split('::');
                    }
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        $('#myModal').modal('hide');
                        $('#applicantLoginForm')[0].reset();
                        location.reload();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }); //End Of Adding Of Applicant Register

            /// User Login Using ajax //
        $("#createAccount").on('click','#Register',function(e){
            e.preventDefault();
            var formData = $('#registerForm').serialize();
            $.ajax({
                url: "<?php echo base_url()?>recruitment/RegisterApplicantAccount/",
                data:formData,
                type:"POST",
                success: function (output) {
                    //Output Here If Success.
                    var splitCharacter = '>>>';
                    var data;
                    var topData;
                    if(output.indexOf(splitCharacter) > -1){
                        topData = output.split('>>>');
                        data = topData[0].split('::');
                    }else{
                        data = output.split('::');
                    }
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        $('#myModal').modal('hide');
                        $('#registerForm')[0].reset();
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }); //End Of User Login
        /// Insertion for Applicant Register Using ajax //
        $(".forgotPassword").on('click','#forgotPasswordBtn',function(e){
            e.preventDefault();
//            var formData = $('#forgotPassword_form').serialize();
                var EmailAddress=$("#EmailAddress").val();
                if(EmailAddress.length==0)
                {
                    Parexons.notification("Mail Address Is Required!","error");
                    return;
                }
                //console.log(MailMessage);
                $.ajax({
                    url:"<?php echo base_url();?>recruitment/forgotPassword",
                    type:"POST",
                    data:{
                        EmailAddress:EmailAddress
                    },
                    success:function(data)
                    {
                        var SplitData=data.split("::");
                        if(SplitData[0]=='OK')
                        {
                            $(".forgotPassword").modal('hide');
                        }

                        Parexons.notification(SplitData[1],SplitData[2]);
                    }


                })
        }); //End Of Adding Of Applicant Register
    });
</script>
</body>
</html>