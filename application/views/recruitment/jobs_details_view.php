<?php
    $jobAdvertisementID = $this->uri->segment(3);
if (strpos($jobAdvertisementID,'#') !== false) {
    $jobAdvertisementID = explode('#',$jobAdvertisementID)[0];
}

//Redirect if Not a Numeric Value.
if(!is_numeric($jobAdvertisementID)){
    redirect('recruitment/careers');
}
$applicantID = $this->session->userdata('applicantID');
?>
<style>
    .below_body {
        height: auto;
        background: white;
        padding-bottom: 15px;
        border-bottom: 5px solid #6495ED;
    }

    .panel-primary {
        border: 1px solid #428BCA
    }
.nvdf{background: #F8F8F8}
    .b_bottom {
        border-bottom: 1px solid #ccccdd
    }

    .lmenu ul li span, i {
        padding-right: 10px;
    }

    .full-time {
        font-weight: bold;
        font-size: 16px;
    }

    .prvious {
        font-weight: normal;
        color: cornflowerblue
    }

    .requirements li {
        border: none;
        padding: 5px;
    }

    span h4 {
        font-weight: bold;
        font-size: 15px;
    }

    .details {
        padding-left: 15px;
    }

    .content {
        margin-top: 25px;
    }

    .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
        background: #4F94CF;
    }

    .tab-content {
        border-bottom: 1px solid #CCCCCC;
        border-left: 1px solid #CCCCCC;
        border-right: 1px solid #CCCCCC;
        margin-top: -20px;
    }

    .form {
        margin-top: 15px;
    }

    #pane1,#pane2,#pane3 {
        margin-left: 15px;
        margin-right: 15px;
    }

    .tab_below {
        padding-top: 30px;
    }
     .form-control .fileinput-filename{
         overflow: hidden;
         white-space: nowrap;
         text-overflow: ellipsis
     }
     .nav-custm{
         background: white;
         padding-top: 10px;
         border:none;
     }
     .modal-header{
         background: #4F94CF;
         color:white;
         border-top-right-radius:10px;
         border-top-left-radius:10px;
     }
    .modal-content{
        border-top-right-radius:10px;
        border-top-left-radius:10px;
    }
    /*-----------------Toggle collapse menu------------------------*/
    /* make sidebar nav vertical */
    @media (min-width: 768px) {
        .sidebar-nav .navbar .navbar-collapse {
            padding: 0;
            max-height: none;
        }
        .sidebar-nav .navbar ul {
            float: none;
        }
        .sidebar-nav .navbar ul:not {
            display: block;
        }
        .sidebar-nav .navbar li {
            float: none;
            display: block;
        }
        .sidebar-nav .navbar li a {
            padding-top: 10px;
            padding-bottom: 10px;
        }
    }


    /*-----------------Toggle collapse menu------------------------*/

</style>
<script>
    $(document).ready(function(){
        $('input[type="text"]').keyup(function(event) {
            $(this).val(($(this).val().substr(0,1).toUpperCase())+($(this).val().substr(1)));
        });
    });
</script>

<style>
    .navbar-inverse{background-color: #5CB85C;}
    .navbar{min-height: 30px;}
</style>
<!-- Navigation -->
<style>
    .navbar-bright{background:#632f53;height: 50px;}
    .navbar-default{background:#632f53;}
    #masthead{background: #6a6c5f}
    .navbar-nav.navbar-right:last-child{margin-right: 0px;}
    .tab_below {
        padding-top: 30px;
    }
    .tab-setting{margin-top: 20px;margin-left: 15px;margin-right: 15px;}

    .usr{margin-right: 10px;}
    .logout{padding-right: 20px;}
    ul li a:hover{background: none;}
    .nav .open > a, .nav .open > a:hover, .nav .open > a:focus{background: none;}
    .dropdown-menu{max-width: 170px important;}
    .drpdwn{margin-left: -90px;background: #d3d3d3}
    li:hover{cursor: pointer}
    ul li a:hover{cursor: pointer}
    .imgcircle{width:40px;height:30px;display: inline-block}
    .srch-style{width:220px;margin-top: 8px;}

    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav >
    .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{background: none;}
    .navbar-default .navbar-collapse, .navbar-default .navbar-form{border-color:#646464;}
    .navbar-default .navbar-nav > li > a{line-height: 30px;}
    .navbar-right{line-height: 38px;}
</style>


<style>
    .top-pic{width:30px;height:30px;border-radius: 5px;margin-top: -6px;}
    .usr{color:white;}
    .a-style{margin-top: -29px;}
    #drp_dwn_lst{
        background: lightgray;
        margin-left: 5px;
        margin-right: 5px;
        border-top-left-radius:5px;
        border-top-right-radius: 5px;
    }
    #drp_hover a:hover{background: none;}


</style>
<!--Modal CSS-->
<style>
    .lbl {
        margin-left: 15px;
    }
    .rside {
        margin-right: 15px;
    }
    .mdl {
        margin-bottom: -20px;
        overflow-x: none;
        overflow-y: none;
    }
</style>
<div class="container">
<div class="below_body">
<div class="row">
<div class="col-md-12">
<div class="row">

<!------------------------Left Menu----------------------------------------------->
<div class="col-md-3 lmenu" style="margin-left: 15px;margin-top: 25px;margin-right: 15px">
<div class="sidebar-nav">
<div class="navbar navbar-default nvdf" role="navigation" style="">
    <div class="navbar-header" style="background: #d3d3d3">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="visible-xs navbar-brand">Sidebar menu</span>
    </div>
<div class="navbar-collapse collapse sidebar-navbar-collapse nav-custm">
    <ul class="nav nav-pills nav-stacked " style="background: #ffffee;">
        <li role="presentation" class="active"><a href="#"><span class="fa fa-print"></span>Print</a></li>
<!--        <li role="presentation" class="b_bottom"><a href="#"><i class="fa fa-floppy-o"></i>Save</a></li>-->
        <li role="presentation" class="b_bottom" data-toggle="modal" data-target=".emailmodel">
            <a href="#"><span class="fa fa-envelope"></span>Email</a></li>
        <li role="presentation" class="b_bottom"><a href="#"><span class="fa fa-share"></span>Share</a></li>
<?php
if (LoggedIn() === TRUE) {
    ?>
    <li role="presentation" class="b_bottom" data-toggle="modal" data-target=".applyForJob" id="applyForJobListItem"><a href="#">
            <span class="fa fa-hand-o-right"></span>Apply For Job</a></li>
<?php } else { ?>
    <li role="presentation" class="b_bottom" data-toggle="modal" data-target="#myModal" id="loginError"><a href="#">
            <span class="fa fa-hand-o-right"></span>Apply For Job</a></li>
<?php } ?>
    </ul>
    </div>
    </div>
    </div>
</div>
<!---------------------------------Left Menu End-------------------------------------->
    <div class="modal fade emailmodel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content" style="margin:10% auto;width: 50%;" id="modal_qualification">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Email</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-top: 5%;">
                        <form  id="Email_form">
                            <!----------when selected then form appears------>
                            <div class="row">
                               <div class="form-group">
                                <label for="Email" class="control-label col-xs-2 lbl">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" placeholder="Email" id="MailAddress">
                                </div>
                                </div>
                            </div>

                            <div class="row"style="margin-top: 5%;margin-bottom: 5%;">
                                <div class="form-group">
                                    <label for="Message" class="control-label col-xs-2 lbl">Subjec</label>
                                    <div class="col-xs-8">
                                        <input type="text"  id="MailSubject"class="form-control" placeholder="Message...">
                                    </div>
                                </div>
                            </div>
                            <div class="row"style="margin-top: 5%;margin-bottom: 5%;">
                                <div class="form-group">
                                    <label for="Message" class="control-label col-xs-2 lbl">Message</label>
                                    <div class="col-xs-8">
                                        <textarea  id="MailMessage"class="form-control" placeholder="Message..."></textarea>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>

                    <div class="modal-footer mfooter">
                        <div class="row" style="text-align: right;">
                            <!--<form id="test"><input type="text" name="tttt" value="dasdasda"> </form>-->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" id="sendMessage">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!------------------- Modal For applay job--------------------->
<div class="modal fade applyForJob" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="col-md-12">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Apply For Job: <?php echo $result->ReferenceNo;?>, <?php echo $result->JobTitle;?>  </h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 ">
                           <p style="text-align: center"><img src="<?php echo base_url(); ?>assets/images/defaultAvatar.png"
                                 class="img-responsive img-thumbnail"></p>
                        </div>
                        <div class="col-lg-10 col-md-6 col-sm-12 col-xs-12">
                            <div class="tabbable">

                            <!--    <ul class="tabs">
                                    <li class="active" rel="tab1" id="a" >Water Quality&Supply Scheme</li>
                                    <li rel="tab2" id="b" disabled="true">DWSS</li>
                                    <li rel="tab3" id="c"> Feasibility</li>
                                    <li rel="tab4" id="d">O&M Comittee</li>
                                    <li rel="tab5" id="e"> Scheme Dimensions</li>
                                </ul>-->

                                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                                    <li class="active" id="Personal"><a href="#pane1" data-toggle="tab">Personal Information</a></li>
                                    <li id="Contact"><a href="#pane2" data-toggle="tab">Contact Details</a></li>
                                    <li id="qualification_tabe"><a href="#pane3" data-toggle="tab">Qualification</a></li>
                                    <li id="Experience"><a href="#pane4" data-toggle="tab">Experience</a></li>
                                    <li id="Skills_Fetch"><a href="#pane5" data-toggle="tab">Skills</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="pane1" class="tab-pane active">
                                        <form class="form-horizontal form">

                                            <div class="form-group tab_below">
                                                <label for="inputEmail" class="control-label col-xs-3">
                                                    Upload Image</label>

                                                <div class="col-xs-8">
                                                    <div class="fileinput fileinput-new input-group"
                                                         data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <span class="fa fa-file fileinput-exists"></span>
                                                            <span class="fileinput-filename" style="width:100px;"></span></div>
                                                        <span class="input-group-addon btn btn-default btn-file"><span
                                                                class="fileinput-new">Select file</span><span
                                                                class="fileinput-exists">Change</span><input type="file" name="applicantAvatar"></span>
                                                        <a href="#"
                                                           class="input-group-addon btn btn-default fileinput-exists"
                                                           data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label for="inputEmail" class="control-label col-xs-3">First Name</label>

                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="firstName" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Last Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="lastName" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Father Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="fatherName" placeholder="Father Name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">CNIC Number</label>

                                                <div class="col-xs-8">
                                                    <input type="text" id="input" class="form-control" name="nic1" style="width: 70px; margin-right: 5px; float: left;" maxlength="5" placeholder="" required="required">
                                                    <input type="text" id="input1" name="nic2" class="form-control" style="width: 110px; margin-right:5px; float: left;" maxlength="7" placeholder="" required="required">
                                                    <input type="text" id="input2" name="nic3" class="form-control" style="width: 35px;  margin-right:5px; float: left;" maxlength="1" placeholder=""  required="required">

                                                    <!--<div id="check_cnic"></div>--><!--<input type="text" class="form-control" id="cnic" placeholder="CNIC">-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Gender</label>

                                                <div class="col-xs-8">
                                                    <?php echo form_dropdown('selectGender',$genderType,'',"class='form-control' id='selectGender'"); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Upload Resume</label>

                                                <div class="col-xs-8">
                                                    <div class="fileinput fileinput-new input-group"
                                                         data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <span class="fa fa-file fileinput-exists"></span>
                                                            <span class="fileinput-filename" style="width:100px;"></span></div>
                                                        <span class="input-group-addon btn btn-default btn-file"><span
                                                                class="fileinput-new">Select file</span><span
                                                                class="fileinput-exists">Change</span><input type="file" name="applicantResume"></span>
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                 <!--                               <div class="form-group">
                                                    <label for="inputEmail" class="control-label col-xs-3">Skills</label>

                                                    <div class="col-xs-8">
                                                        <input type='hidden' name='selectSkill' id='selectSkill'/>
                                                        <input type='text' name='selectedSkill' id='selectedSkill' class='form-control' title="Selected Skills" readonly/>
                                                    </div>
                                                    </div>-->
                                            </div>

                                        </form>
                                        <div class="modal-footer">
                                            <a href="#pane2" data-toggle="tab"><button type="button" id="PersonalNext" class="btn btn-primary">Next</button></a>
                                        </div>

                                    </div>


                                    <div id="pane2" class="tab-pane">
                                        <form class="form-horizontal form">
                                            <div class="form-group tab_below">
                                                <label for="inputEmail" class="control-label col-xs-3">Current Address</label>

                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="currentAddress" placeholder="Current Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Permanent Address</label>

                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="permanentAddress" placeholder="Permanent Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Contact Number (Cell/Mobile)</label>

                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="contactNumber" placeholder="contactNumber">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Resident Phone (Tel)</label>

                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="homePhone" placeholder="Resident Phone Number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Official Email</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="officialEmail" placeholder="e-g jhon@example.com">
                                                </div>
                                            </div>
                                        </form>
                                        <div class="modal-footer">
                                            <a href="#pane3" data-toggle="tab"><button type="button" id="ContactNext" class="btn btn-primary">Next</button></a>
                                        </div>
                                    </div>
                                    <style>
                                        td p{text-align: center}
                                        .btnaply{
                                            float:right;
                                            margin-top: 10px;
                                            margin-left: 40px;}
                                        .tb-setting{margin-left: 15px;margin-right: 15px;}
                                    </style>

                                    <div id="pane3" class="tab-pane tb-setting">
                                        <div class="row">
                                            <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btnaply" data-toggle="modal" data-target=".apply">
                                                        Add Qualification
                                                    </button>
                                            </div>
                                        </div>
                                        <div class="table-responsive tab_below">
                                        <table class="table table-striped table-bordered table-hover tableq" id="qualification_view">
                                            <thead>
                                            <tr>
                                                <th >Qualification</th>
                                                <th>Type</th>
                                                <th>Institute</th>
                                                <th>Year</th>
                                                <th>Edit</th>
                                                <th>Trash</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                        </div>

                                        <div class="modal-footer">
                                            <a href="#pane4" data-toggle="tab"><button type="button" id="QualificationNext" class="btn btn-primary">Next</button></a>
                                        </div>
                                    </div>

                                    <!------------For Expereience--------------------------------------->

                                    <div id="pane4" class="tab-pane tb-setting" >
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-success btnaply" data-toggle="modal" data-target=".add_experience">
                                                    Add Experience
                                                </button>
                                            </div>
                                        </div>
                                        <div class="table-responsive tab_below">
                                            <table class="table table-striped table-bordered table-hover tableq" id="expe_view">
                                                <thead>
                                                <tr>
                                                    <th>Position</th>
                                                    <th>Company</th>
                                                    <th>From</th>
                                                    <th>Date</th>
                                                    <td id="edit">Edit</td>
                                                    <td id="trashed">View</td>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#pane5" data-toggle="tab"><button type="button" id="ExperienceNext" class="btn btn-primary">Next</button></a>
                                        </div>
                                    </div>

                                    <!-----------------------end of experience-------------------------->


                                    <!------------For Skill--------------------------------------->

                                    <div id="pane5" class="tab-pane tb-setting" >
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-success btnaply" data-toggle="modal" data-target=".add_Skill">
                                                    Add Skills
                                                </button>
                                            </div>
                                        </div>
                                        <div class="table-responsive tab_below">
                                            <table class="table table-striped table-bordered table-hover tableq" id="Skills_view">
                                                <thead>
                                                <tr>
                                                    <th >Skills</th>
                                                    <th>Level</th>
                                                    <td id="edit">Edit</td>
                                                    <td id="trashed">View</td>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" id="submitBtn">Submit & Apply</button>
                                        </div>
                                    </div>
                                    <!-----------------------end of Skill-------------------------->

                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.tabbable -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<!-------------------2nd Modal add experience form--------------------->
<div class="modal fade add_experience mdl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style="margin-top: -30px;" id="modal_qualification">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Experience</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" id="expereince_form">

                        <!----------when selected then form appears------>
                        <div class="form-group">
                            <label for="DegreeTitle" class="control-label col-xs-2 lbl">Position</label>
                            <div class="col-xs-8">
                                <input type="text" name="ExpPosition" class="form-control" id="Position" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Institute" class="control-label col-xs-2 lbl">Company</label>
                            <div class="col-xs-8">
                                <input type="text" name="Company" class="form-control" id="Company" placeholder="Company">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Specialization" class="control-label col-xs-2 lbl">From </label>
                            <div class="col-xs-8">
                                <input type="text" name="FromDate" class="form-control" id="FromDate" placeholder="From Date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="YearComp" class="control-label col-xs-2 lbl">To</label>
                            <div class="col-xs-8">
                                <input type="text" name="ToDate" class="form-control" id="ToDate" placeholder="To Date">
                            </div>
                        </div>


                    </form>
<script>
    $('#sandbox-container input').datepicker({
        keyboardNavigation: false
    });
</script>

                    <!--------script for datepickerssssssssss------------------->
<script>
    $(document).ready(function($) {
        $('#FromDate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            autoclose: true
        });
    });
</script>
                    <!--------end of script for datepickerssssssssss------------------->

                </div>

                <div class="modal-footer mfooter">
                    <div class="row" style="margin-right: 110px;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_experience">Save changes</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><!-------------------2nd Modal add Skill form--------------------->
<div class="modal fade add_Skill mdl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style="margin-top: -30px;" id="modal_qualification">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Skills</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form  id="Skills_form">
                        <!----------when selected then form appears------>
                        <div class="form-group">
                            <label for="DegreeTitle" class="control-label col-xs-2 lbl">Skills</label>
                                <div class="col-xs-3">
                                    <?php echo form_dropdown('skill_type_ids',$SKILLS,'','id="ASkillType" required="required" class="form-control"'); ?>
                                </div>

                                <label for="DegreeTitle" class="control-label col-xs-3 lbl">Level</label>
                                <div class="col-xs-3">
                                    <?php echo form_dropdown('level',$SKILLS_LEVELS,'','id="ASkillLevel" required="required" class="form-control"'); ?>
                                </div>

                            </div>
                        </form>
                     </div>

                <div class="modal-footer mfooter">
                    <div class="row" style="margin-right: 110px;">
                        <!--<form id="test"><input type="text" name="tttt" value="dasdasda"> </form>-->
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_Skill">Save changes</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-------------------apply entry Skills form finish--------------------->

<!----------------------Add Experience---------------------------->
<!-------------------2nd Modal apply entry form--------------------->
<div class="modal fade apply mdl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style="margin-top: -30px;" id="modal_qualification">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Qualification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" id="qualification_form">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2 lbl">Qualification</label>
                            <div class="col-xs-8">
                                <?php echo @form_dropdown('qualification_type_id', $qualification,'', 'required="required" id="qualificationType" class="form-control"'); ?>
                            </div>
                        </div>

                        <!----------when selected then form appears------>
                        <div class="form-group">
                            <label for="DegreeTitle" class="control-label col-xs-2 lbl">Degree</label>
                            <div class="col-xs-8">
                                <input type="text" name="DegreeTitle" class="form-control" id="Degree" placeholder="Degree">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Institute" class="control-label col-xs-2 lbl">Institute</label>
                            <div class="col-xs-8">
                                <input type="text" name="Institute" class="form-control" id="institute" placeholder="Institute">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Specialization" class="control-label col-xs-2 lbl">Specialization</label>
                            <div class="col-xs-8">
                                <input type="text" name="Specialization" class="form-control" id="specialization" placeholder="Specialization">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="YearComp" class="control-label col-xs-2 lbl">Year of Completion</label>
                            <div class="col-xs-8">
                                <input type="text" name="YearComp" class="form-control" id="year" placeholder="Year of Completion">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="GPA" class="control-label col-xs-2 lbl">GPA / DIVISION</label>
                            <div class="col-xs-8">
                                <input type="text" name="gpa" class="form-control" id="gpa" placeholder="GPA / DIVISION">
                            </div>
                        </div>


                    </form>

                </div>

                <div class="modal-footer mfooter">
                    <div class="row" style="margin-right: 110px;">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_qualification">Save changes</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-------------------apply entry form finish--------------------->

<!---------------------------end of expereience---------------------->


<div class="col-md-8">
    <!--<h2 class="details">Job Details</h2>-->
    <div class="row-fluid content">
        <div class="col-md-12">
            <?php echo (isset($result) && !empty($result->EmploymentType))? '<spanclass ="full-time">'.$result->EmploymentType.'/</span>':'';?>
            <?php echo (isset($result) && !empty($result->CityName))? '<span class="full-time">'. $result->CityName.'/</span>':'';?>
            <?php echo (isset($result) && !empty($result->AvailablePositions))? 'Available Position &nbsp;<span class="full-time">'. $result->AvailablePositions.'/</span>':'';?>
            <?php echo (isset($result) && !empty($result->DatePosted))? 'Posted On: <span class="full-time">'. $result->DatePosted.'/</span>':'';?>
            <a href="<?php echo base_url();?>recruitment/careers"><span class="prvious pull-right">  Back to Jobs Listing </span></a>
            <hr>


            <h3>Job Description</h3>

            <p>
                <?php echo $result->JobDescription; ?>
            </p>

            <p><strong>Requirements:</strong></p>
            <ul class="requirements">
                <?php echo (isset($result) && !empty($result->MinQualification)) ? '<li><strong>Qualification: &nbsp</strong>' . $result->MinQualification . '</li>' : '' ?>
                <?php echo (isset($result) && !empty($result->AgeLimit)) ? '<li><strong>Qualification: &nbsp</strong>' . $result->AgeLimit . '</li>' : '' ?>
                <?php echo (isset($result) && !empty($result->MinExperience)) ? '<li><strong>Experience: &nbsp</strong>' . $result->MinExperience . '</li>' : '' ?>
                <?php echo (isset($result) && !empty($result->TransportFacilities)) ? '<li><strong>Transport: &nbsp</strong>' . $$result->TransportFacilities . '</li>' : '' ?>
                <?php echo (isset($result) && !empty($result->SalaryLimit)) ? '<li><strong>Salary Rang: &nbsp</strong>' . $result->SalaryLimit . '</li>' : '' ?>
            </ul>




            <?php

            //Only If Skills Are Available, Then Should Skills Should Show In View.
            if (isset($result) && !empty($result->RequiredSkills)) {?>
                <hr>
            <?php
                echo "<p><strong>Required Skills:</strong></p>";
                echo '<ul class="requirements">';
                $requiredSkills = explode(',', $result->RequiredSkills);
                //                                print_r($requiredSkills);
                //Loop For Bullets
                foreach ($requiredSkills as $skillKey => $skillValue) {
                    echo "<li>$skillValue</li>";
                }
                echo "</ul>";
            } ?>




            <!--  <h3>JOB SUMMARY</h3>
            <hr>
              <div class="row-fluid" style="">
                  <div class="col-md-4">
                      <span><h4>Company</h4></span>
                      <span>IT Parexons</span>
                  </div>
                  <div class="col-md-4">
                      <span><h4>Location</h4></span>
                      <span>Peshawar,Deans Trade Center</span>
                  </div>
                  <div class="col-md-4">
                      <span><h4>Career Level</h4></span>
                      <span>Experienced</span>
                  </div>
                  <div class="col-md-4">
                      <span><h4>Job Type</h4></span>
                      <span>Full Time</span>
                  </div>
                  <div class="col-md-4">
                      <span><h4>Available Vacancies</h4></span>
                      <span>120</span>
                  </div>
                  <div class="col-md-4">
                      <span><h4>Salary</h4></span>
                      <span>25 to 30 Thousand</span>
                  </div>-->
              </div>
          </div>
      </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
<!---------------for edit qualification--------------------------------------------->
<!-- Modal -->
<div class="modal edit_qualification fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Qualification</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="update_form">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Qualification</label>
                        <div class="col-sm-8">

                            <?php echo @form_dropdown('qualification_type_id', $qualification,'', 'required="required" class="form-control" id="EqualificationType"'); ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Degree</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="EDegree" id="EDegree" placeholder="Type">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Institute</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="EInstitute" id="EInstitute" placeholder="Institute">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Year of Completion</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ECompYear" id="ECompYear" placeholder="Year of Completion">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Specialization</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ESpecialization" id="ESpecialization" placeholder="Specialization">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label"> GPA / DIVISION
                        </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="EGpa" id="EGpa" placeholder="GPA / DIVISION">
                            <input type="hidden" class="form-control" name="EQid" id="EQid" >
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div class="row" style="margin-right: 150px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update_changes">Update changes</button>
                </div>
            </div>
        </div>
    </div>
 </div>



<!---------------for edit qualification--------------------------------------------->
<!-- Modal -->
<div class="modal edit_Skills fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Skills</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="update_form_skills">
                    <div class="form-group">
                        <label for="DegreeTitle" class="control-label col-xs-2 lbl">Skills</label>
                        <div class="col-xs-3">
                            <?php echo form_dropdown('skill_type_ids',$SKILLS,'','id="ESkillType" class="form-control"'); ?>
                            <input type="hidden" class="form-control" name="SKID" id="SKID">
                        </div>

                        <label for="DegreeTitle" class="control-label col-xs-3 lbl">Experience In Year</label>
                        <div class="col-xs-3">
                            <select class="form-control" name="EDuration" id="EDuration">
                                <option selected> Select Year </option>
                                <option value="1">Beginner</option>
                                <option value="2">Intermediate</option>
                                <option value="3">Advanced</option>

                            </select>

                        </div>







</div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row" style="margin-right: 150px;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update_changes_skills">Update changes</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------For edit experience------------------------------------------->
<div class="modal edit_experience fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Experience</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="update_EXpform">
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Position</label>
                        <div class="col-sm-8">

                            <input type="text" class="form-control" name="EPosition" id="EPosition" placeholder="Position">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Company</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ECompany" id="ECompany" placeholder="Company">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">From </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="EFromDate" id="EFromDate" placeholder="From Date">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="EToDate" id="EToDate" placeholder="To Date">
                            <input type="hidden" class="form-control" name="Expid" id="Expid">
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="row" style="margin-right: 150px;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update_ExpChanges">Update changes</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!----------------end of experience---------------------------------->



<script>

    $(document).ready(function (e) {

//        $("body").on("click",'#loginError',function(){
//            Parexons.notification('To Apply For Job, Please Login/Register First','warning');
//            return false;
//        })

        $('#PersonalNext').on('click', function (e) {
            //Get All The Required Data From The Forms..
            //PersonalInformation
            if ($('input[name="applicantAvatar"]')[0].files.length > 0) {
                var ext = $('input[name="applicantAvatar"]').val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    Parexons.notification('Only GIF, PNG, JPG and JPEG files are allowed', 'warning');
                    return;
                }
            } else {
                Parexons.notification('No Image Has Been Selected For Upload.', 'warning');
//                return;
            }

            var applicantAvatar = $('input[name="applicantAvatar"]')[0].files[0];
            var FirstName = $('#firstName').val();
            var LastName = $('#lastName').val();
            var FatherName = $('#fatherName').val();
            var CNIC1 = $('#input').val();
            var CNIC2 = $('#input1').val();
            var CNIC3 = $('#input2').val();

            var Gender = $('#selectGender').val();

            if(FirstName.length === 0 || LastName.length === 0){
                Parexons.notification("Please Fill Complete Name","warning");
                return false;
            }

            if(CNIC1.length < 5 || CNIC2.length < 7 || CNIC3.length < 1){
                Parexons.notification("Pleae Enter Correct CNIC","error")
                return false;
            }

            if(Gender === null || Gender.length === 0){
                Parexons.notification("Gender Field is Required, Please Select Gender from Dropdown.","error")
                return false;
            }

            //Remove active from anyother Tab and Add it to Next Tab Pane it moves.
            $(".tabbable").find("li").removeClass("active");
            $(".tabbable ul li#Contact").addClass("active");
        });

        $("#ContactNext").on("click",function(){

            var contactNumber = $('#contactNumber').val();

            if(contactNumber.length < 6){
                Parexons.notification("Please Enter a valid Contact Number","error");
                return false;
            }

            fetch_qualification();

            //Remove active from anyother Tab and Add it to Next Tab Pane it moves.
            $(".tabbable").find("li").removeClass("active");
            $(".tabbable ul li#qualification_tabe").addClass("active");

        });

        $('input[name="applicantAvatar"]').on('change', function (e) {
//            console.log('working');
            var applicantAvatarFile = $(this)[0].files[0];
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                Parexons.notification('Only GIF, PNG, JPG and JPEG files are allowed', 'warning');
                $(this).val('');
                return;
            }
            if ($(this)[0].files.length > 0) {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(applicantAvatarFile);
                oFReader.onload = function (oFREvent) {
                        $('.img-thumbnail').attr('src', oFREvent.target.result);
                }
            }
        });
        //Apply For Job Button
        $('#applyForJobListItem').on('click',function(e){
            //Get Details For The Popup Modal
            var postData = {
                jobAdvertisementID: <?php echo $jobAdvertisementID; ?>
            };
            $.ajax({
                url:"<?php echo base_url(); ?>recruitment/get_applicant_details",
                data:postData,
                type:"POST",
                success:function(output){
                    try {
                        var json = JSON.parse(output);
                        $('#firstName').val(json.ApplicantFirstName);
                        $('#lastName').val(json.ApplicantLastName);
                        $('#fatherName').val(json.FatherName);
                        $('.img-thumbnail').attr('src',json.Avatar);
                        $('#selectGender').val(json.ApplicantGender);
                        $('#currentAddress').val(json.ApplicantCurrentAddress);
                        $('#permanentAddress').val(json.ApplicantPermanentAddress);
                        $('#contactNumber').val(json.ContactNo);
                        $('#homePhone').val(json.ResidentPhone);
                        $('#officialEmail').val(json.Email);

                        //Assigning the CNIC
                        var ApplicantCNIC = json.ApplicantCNIC;
                        if(ApplicantCNIC.length === 15){
                            var CNICSplit = ApplicantCNIC.split("-");
                            $("input[name='nic1']").val(CNICSplit[0]);
                            $("input[name='nic2']").val(CNICSplit[1]);
                            $("input[name='nic3']").val(CNICSplit[2]);
                        }
                    }
                    catch (e) {
                        var data = output.split('::');
                        if(data[0] === 'FAIL'){
                            Parexons.notification(data[1],data[2]);
                        }
                    }
                }
            });
            // code for selected skills
            /*The Selector for Selecting the Employee Skills*/

           /* var select2Selector = $('#selectSkill');
            var url = "<?php //echo base_url(); ?>recruitment/get_selected_skills";
            var id = "SkillID";
            var text = "SkillName";
            var minInputLength = 0;
            var placeholder = "Select Skills";
            var multiple = true;
            commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);
*/
            //$('.select2-container').css("width","99%");
            //
           /* $.ajax({
             url:"<?php echo base_url(); ?>recruitment/get_selected_skills",
             data:postData,
             type:"POST",
             success:function(result){
             // console.log(result);
             var Setjson = JSON.parse(result);

             $('#selectedSkill').val(Setjson.SkillName);

             }
             });
             */
        });

        /*The Selector for Selecting the Employee Skills*/
        var select2Selector = $('#selectSkill');
        var url = "<?php echo base_url(); ?>recruitment/load_all_available_skills";
        var id = "SkillID";
        var text = "SkillName";
        var minInputLength = 0;
        var placeholder = "Select Skills";
        var multiple = true;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css("width","99%");

        /*The Selector for Selecting the Employee Skills*/
        var select2Selector = $('#selectLevel');
        var url = "<?php echo base_url(); ?>recruitment/load_all_available_skills";
        var id = "SkillID";
        var text = "SkillName";
        var minInputLength = 0;
        var placeholder = "Select Skills";
        var multiple = true;
        commonSelect2(select2Selector,url,id,text,minInputLength,placeholder,multiple);
        $('.select2-container').css("width","99%");

        //DatePickers
        $('#FromDate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            autoclose: true
        });

        //DatePickers
        $('#ToDate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            autoclose: true
        });
        $('#EFromDate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            autoclose: true
        });

        //DatePickers
        $('#EToDate').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            autoclose: true
        });

        //For Submitting of Applicant Application

        $("#submitBtn").on("click",function(e){
            e.preventDefault();
            //Get All The Required Data From The Forms..
            //PersonalInformation
            if ($('input[name="applicantAvatar"]')[0].files.length > 0) {
                var ext = $('input[name="applicantAvatar"]').val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    Parexons.notification('Only GIF, PNG, JPG and JPEG files are allowed', 'warning');
                    return;
                }
            } else {
                Parexons.notification('No Image Has Been Selected For Upload.', 'warning');
//                return;
            }
            var applicantAvatar = $('input[name="applicantAvatar"]')[0].files[0];
            var FirstName = $('#firstName').val();
            var LastName = $('#lastName').val();
            var FatherName = $('#fatherName').val();
            var CNIC1 = $('#input').val();
            var CNIC2 = $('#input1').val();
            var CNIC3 = $('#input2').val();

            var Gender = $('#selectGender').val();
            //var Skill = $('#selectSkill').val();
            /* if(Skill.length < 1)
             {
             Parexons.notification('Please Select Skill', 'warning');

             }*/

            var applicantResume = $('input[name="applicantResume"]')[0].files[0];

            //From Second Tab Contact Details.
            var currentAddress = $('#currentAddress').val();
            var permanentAddress = $('#permanentAddress').val();
            var contactNumber = $('#contactNumber').val();
            var residentPhone = $('#homePhone').val();
            var officialMail = $('#officialEmail').val();


            var formData = new FormData();
            formData.append('firstName', FirstName);
            formData.append('lastName', LastName);
            formData.append('fatherName', FatherName);
            formData.append('nic1', CNIC1);
            formData.append('nic2', CNIC2);
            formData.append('nic3', CNIC3);
            formData.append('gender', Gender);
            formData.append('avatar', $('input[name="applicantAvatar"]')[0].files[0]);
            formData.append('resume', $('input[name="applicantResume"]')[0].files[0]);
            //formData.append('skill', Skill);

            //Second Tab Data To POST
            formData.append('currentAddress', currentAddress);
            formData.append('permanentAddress', permanentAddress);
            formData.append('contactNumber', contactNumber);
            formData.append('residentPhone', residentPhone);
            formData.append('officialMail', officialMail);
            //console.log(officialMail);
            formData.append('jobAdvertisementID',<?php echo $jobAdvertisementID; ?>);
            $.ajax({
                url:"<?php echo base_url(); ?>recruitment/apply_for_job",
                data:formData,
                type:"POST",
                processData: false,
                contentType: false,
                success: function (output) {
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        $("#Personal").removeClass("active");
                        $("#Contact").addClass("active");
                        $("#pane1").hide();
                        $("#pane2").show();
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });

    });

    $("#save_qualification").on('click',function(){
        var formData=$("#qualification_form").serialize();

        $.ajax({
            url:"<?php echo base_url()?>recruitment/add_qualification",
            data:formData,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    //$('#modal_qualification').hide();
                    $("#qualification_form")[0].reset();
                    $("#qualification_tabe").trigger('click');
                    $(".apply").modal('hide');
                    }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });


    $("#QualificationNext").on('click',function(){
        fetch_experience();
        $("#qualification_tabe").removeClass("active");
        $("#Experience").addClass("active");
    });

    function trashed_info(id)
    {
        if(!confirm("Are You Sure ?"))
        {
            return false;
        }else{
        var dataP=id;
        $.ajax({
            url:"<?php echo base_url()?>recruitment/trashed_qualification",
            data:{id:dataP},
            type:"POST",
            success:function(result){
                var data = result.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#qualification_tabe").trigger('click');

                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
        }
    }
    function edit_info(id)
    {
        var dataU=id;
        $.ajax({
            url:"<?php echo base_url()?>recruitment/update_qualification",
            data:{Uid:dataU},
            type:"POST",
            success:function(result){
                var json=JSON.parse(result);
                $("#EqualificationType").val(json.qualification_type_id);
                $("#EDegree").val(json.title);
                $("#qualificationType").val(json.qualification_type_id);
                $("#EInstitute").val(json.institute);
                $("#ECompYear").val(json.comp_year);
                $("#ESpecialization").val(json.specialization);
                $("#EGpa").val(json.gpa_division);
                $("#EQid").val(json.id);
            }
        });
    }

    $("#update_changes").on('click',function(){
        var formDataU=$("#update_form").serialize();

        $.ajax({
            url:"<?php echo base_url()?>recruitment/update_changes_qualification",
            data:formDataU,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#qualification_tabe").trigger('click');
                    $(".edit_qualification").modal('hide');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });
    $("#save_experience").on('click',function(){
        var formData=$("#expereince_form").serialize();

        $.ajax({
            url:"<?php echo base_url()?>recruitment/add_Experience",
            data:formData,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    //$('#modal_qualification').hide();
                    $("#expereince_form")[0].reset();
                    $("#Experience").trigger('click');
                    $(".add_experience").modal('hide');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    $("#ExperienceNext").on('click',function(){
        fetch_skill();
        $("#Experience").removeClass("active");
        $("#Skills_Fetch").addClass("active");
    });

    function trashed_expinfo(id)
    {
        if(!confirm("Are You Sure ?"))
        {
            return false;
        }else{
            var dataP=id;
            $.ajax({
                url:"<?php echo base_url()?>recruitment/trashed_exp",
                data:{id:dataP},
                type:"POST",
                success:function(result){
                    var data = result.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        $("#Experience").trigger('click');

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
    }
    function edit_expinfo(id)
    {
        var dataU=id;
        $.ajax({
            url:"<?php echo base_url()?>recruitment/update_exp",
            data:{Uid:dataU},
            type:"POST",
            success:function(res){
                //console.log(result); return;
                var set=JSON.parse(res);
                $("#EPosition").val(set.Exp_position);
                $("#ECompany").val(set.Company);
                $("#EFromDate").val(set.FromDate);
                $("#EToDate").val(set.ToDate);
                $("#Expid").val(set.id);
            }
        });
    }
    $("#update_ExpChanges").on('click',function(){
        var formDataU=$("#update_EXpform").serialize();

        $.ajax({
            url:"<?php echo base_url()?>recruitment/UpdateRec_Experience",
            data:formDataU,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#Experience").trigger('click');
                    $(".edit_experience").modal('hide');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    // Ajax Function to Fetch Skills Applicant //
    $("#Skills_Fetch").on('click',function(){
        fetch_skill();
    });

    // Ajax Function to Fetch Experience Applicant //
    $("#Experience").on('click',function(){
        fetch_experience();
    });

    // Ajax Function to Fetch Qualification Applicant //
    $("#qualification_tabe").on('click',function(){
        fetch_qualification();
    });
// END
    // Ajax Function To Add Applicant Skills
    $("#save_Skill").on('click',function(){
        var formData=$("#Skills_form").serialize();
        //console.log(formData); return;
        $.ajax({
            url:"<?php echo base_url()?>recruitment/add_Skill",
            data:formData,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    //$('#modal_qualification').hide();
                    $("#Skills_form")[0].reset();
                    $("#Skills_Fetch").trigger('click');
                    $(".add_Skill").modal('hide');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    //END

    // Ajax Function To Update Applicant Skills //

    function edit_info_Skills(id)
    {
        var dataS=id;
        $("#SKID").val(dataS);
        $.ajax({
            url:"<?php echo base_url()?>recruitment/update_Skills",
            data:{Sid:dataS},
            type:"POST",
            success:function(result){
                //console.log(result); return;
                var json=JSON.parse(result);
                $("#ESkillType").val(json.SID);
                $("#EDuration").val(json.SkillLevel);
            }
        });
    }

    $("#update_changes_skills").on('click',function(){
        var formDataU=$("#update_form_skills").serialize();

        $.ajax({
            url:"<?php echo base_url()?>recruitment/update_changes_Skills",
            data:formDataU,
            type:"POST",
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#Skills_Fetch").trigger('click');
                    $(".edit_Skills").modal('hide');
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });

    // END

    // Ajax Function To Trash Applicant Skills

    function trashed_Skill(id)
    {
        if(!confirm("Are You Sure ?"))
        {
            return false;
        }else{
            var dataP=id;
            $.ajax({
                url:"<?php echo base_url()?>recruitment/Skills_Trashed",
                data:{Skill_id:dataP},
                type:"POST",
                success:function(result){
                    var data = result.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        $("#Skills_Fetch").trigger('click');

                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        }
    }
    // END

    function fetch_qualification(){
        var applID = <?= (isset($applicantID) && !empty($applicantID))?$applicantID:"0" ?> ;

        var DataPost = {applicantID:applID};
        $.ajax({
            url:"<?php echo base_url()?>recruitment/fetch_qualification",
            data:DataPost,
            type:"POST",
            success:function(output){
                var json=JSON.parse(output);
                //var table=$("#qualification_view tbody tr");
                $('#qualification_view tbody tr').html('');
                $.each(json,function(i,item){
                    var $tr=$('<tr>').append(
                        $("<td>").append(item.title),
                        $("<td>").append(item.qualification_title),
                        $("<td>").append(item.institute),
                        $("<td>").append(item.comp_year),
                        $("<td>").append("<span class='fa fa-edit' data-toggle='modal' data-target='.edit_qualification' id='applyForJobListItem' onclick='edit_info("+item.id+");'></span>"),
                        $("<td>").append("<span class='fa fa-trash-o' onclick='return trashed_info("+item.id+");'></span>")
                    );
                    $('#qualification_view').append($tr);
                });
            }
        });
    }
    function fetch_experience(){
        var AppID = <?= (isset($applicantID) && !empty($applicantID))?$applicantID:"0" ?> ;
        var DataPosts = {ApplID:AppID};
        $.ajax({
            url:"<?php echo base_url()?>recruitment/fetch_Experience",
            data:DataPosts,
            type:"POST",
            success:function(result){
                //console.log(output);return;
                var data=JSON.parse(result);

                $('#expe_view tbody').html('');
                $.each(data,function(i,items){
                    var $t=$('<tr>').append(
                        $("<td>").append(items.Exp_position),
                        $("<td>").append(items.Company),
                        $("<td>").append(items.FromDate),
                        $("<td>").append(items.ToDate),
                        $("<td>").append("<span class='fa fa-edit' data-toggle='modal' data-target='.edit_experience' id='applyForJobListItem' onclick='edit_expinfo("+items.id+");'></span>"),
                        $("<td>").append("<span class='fa fa-trash-o' onclick='return trashed_expinfo("+items.id+");'></span>")
                    );
                    $('#expe_view tbody').append($t);
                });
            }
        });
    }
    function fetch_skill(){
        var applID = <?= (isset($applicantID) && !empty($applicantID))?$applicantID:"0" ?> ;
        var DataPost = {applicantID:applID};
        $.ajax({
            url:"<?php echo base_url()?>recruitment/fetch_skill",
            data:DataPost,
            type:"POST",
            success:function(output){
                var json=JSON.parse(output);
                //var table=$("#qualification_view tbody tr");
                $('#Skills_view tbody').html('');
                $.each(json,function(i,item){
                    var $tr=$('<tr>').append(
                        $("<td>").append(item.skill_name),
                        $("<td>").append(item.skill_level),
                        $("<td>").append("<span class='fa fa-edit' data-toggle='modal' data-target='.edit_Skills' id='applyForJobListItem' onclick='edit_info_Skills("+item.id+");'></span>"),
                        $("<td>").append("<span class='fa fa-trash-o' onclick='return trashed_Skill("+item.id+");'></span>")
                    );
                    $('#Skills_view tbody').append($tr);
                });
            }
        });
    }



</script>
<script>
$("#sendMessage").on('click',function(e){
    e.preventDefault();
    var MailAddress=$("#MailAddress").val();
    var MailSubject=$("#MailSubject").val();
    var MailMessage=$("#MailMessage").val();
    if(MailAddress.length==0)
    {
        Parexons.notification("Mail Address Is Required!","warning");
        return;
    }if(MailMessage.length==0)
    {
        Parexons.notification("Mail Body Is Required!","warning");
        return;
    }
    //console.log(MailMessage);
    $.ajax({
        url:"<?php echo base_url();?>recruitment/SendMail",
        type:"POST",
        data:{
            MailAddress:MailAddress,
            MailSubject:MailSubject,
            MailMessage:MailMessage,
            },
        success:function(data)
        {
            var SplitData=data.split("::");
            if(SplitData[0]=='OK')
            {
                Parexons.notification(SplitData[1],SplitData[2]);
                $(".emailmodel").modal('hide');
            }
        }


    })
});
    function check_cnic()
    {
        $(document).ready(function ()
        {
            var fisrt_part = $('#nic1').val();
            var second_part = $('#nic2').val();
            var third_part = $('#nic3').val();
            var cnic = fisrt_part +'-'+second_part +'-'+third_part;
            $.ajax({
                type: "POST",
                url: "human_resource/check_cnic_existApplicant",
                data: {cnic:cnic} ,
                success: function(data)
                {
                    $('#check_cnic').html(data);
                }
            });
        });
    }
</script>