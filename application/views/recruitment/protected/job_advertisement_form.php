<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<?php
//var_dump($employee_id); ?>
<style type="text/css">
    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .mytab tr:nth-child(odd) td {
        background-color: #ECECFF;
    }

    .mytab tr:nth-child(even) td {
        background-color: #F0F0E1;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers
    {
        margin-top: 8px;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;

        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 40px;
    }
    .select2TemplateImg { padding:0.2em 0; clear: both;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .select2-chosen div.select2TemplateImg img{ width: 24px; height: 24px;}
    p{
        margin:0 !important;
    }
    textarea { resize: none; }
    .select2-result-selectable{clear:both;}

    /*********************************************Model CSS*********************************************/
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }
    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        height:35px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
    #dataTableFilter{
        width: 22% !important;
    }


    /*************************************************Model css Finish******************************/

</style>
<style>
    .left_block_coments{
        width:100%;
        height: auto;
        float:left;
    }
    .right_lbl .textfield{width:96%;}
    .right_lbl_coments{
        width:470px;
        height: auto;
        float:left;
    }
    .left_lbl_coments{
        width: 24%;
        float: left;
        line-height: 35px;
        padding-left: 5px;
    }
    .coments_row{
        float: left;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
    }
</style>
<!---for Dialogoe---->
<style>
    .tip {
        width: 0px;
        height: 0px;
        position: absolute;
        background: transparent;
        border: 10px solid #ccc;
    }

    .tip-up {
        top: -25px; /* Same as body margin top + bordere */
        left: 10px;
        border-right-color: transparent;
        border-left-color: transparent;
        border-top-color: transparent;
    }

    .tip-down {
        bottom: -25px;
        left: 10px;
        border-right-color: transparent;
        border-left-color: transparent;
        border-bottom-color: transparent;
    }

    .tip-left {
        top: 10px;
        left: -25px;
        border-top-color: transparent;
        border-left-color: transparent;
        border-bottom-color: transparent;
    }

    .tip-right {
        top: 10px;
        right: -25px;
        border-top-color: transparent;
        border-right-color: transparent;
        border-bottom-color: transparent;
    }

    .dialogbox .body {
        position: relative;
        max-width: 100%;
        height: auto;
        /* margin: 20px 10px;*/
        padding: 5px;
        background-color: #DADADA;
        border-radius: 3px;
        border: 5px solid #ccc;
    }

    .body .message {
        min-height: 30px;
        border-radius: 3px;
        font-family: Arial;
        font-size: 14px;
        line-height: 1.5;
        color: #797979;
    }

</style>

<!------ contents -->

<div class="contents-container">
    <div class="bredcrumb">Dashboard / Human Resource / Job Advertisement </div> <!-- Bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Job Advertisement
        <div class="btn-right">
            <a href="#AdvertJobModal"  class="btn green below right" id="modaltrigger" title="Click to Add New Advertisement !">New Job Advertisement</a>
            </div>
        </div>

        <!-- filter -->
        <div class="filter">
            <h4>Filter By</h4>
            <?php echo form_open();?>
            <input type="text" placeholder="Search Reference Number Here" id="dataTableFilter">
            <!--<input type="text" id="tableSearchBox" placeholder="Search Employees" style="width: 30%;">-->
            <?php //echo @form_dropdown('department', $department, $slct_dept, "class='resize' id='dept' onChange='this.form.submit()'"); ?>
            <?php //echo @form_dropdown('designation', $designation, $design, "class='resize' id='design' onChange='this.form.submit()'"); ?>
            <input type="hidden" name="selectDepartments" id="selectDepartments">
            <input type="hidden" name="selectMonth" id="selectMonth">
            <input type="hidden" name="selectYear" id="selectYear">
            <input type="hidden" name="selectJobAdOpen" id="selectJobAdOpen">
            <?php echo form_close();?>
        </div>

        <!-- table -->

        <div class="table_area">
        <table cellpadding="0" cellspacing="0" id="reportedAdvertTable"  class="marge ">
            <div class="table-width">
                <thead class="table-head">

                <td>advertisementID</td>
                <td>Reference #</td>
                <td>Job Title</td>
                <td>Department</td>
                <td>Positions</td>
                <td>Applicants</td>
                <!--<td>Description</td>-->
                <!--<td>minQualification</td>
                <td>Age Limit</td>
                <td>Salary Range</td>-->
                <td>Date Posted</td>
                <td>Expiry Date</td>
                <td><span title="click to close">Advertise Status</span></td>
                <td width="180">Edit &nbsp; &nbsp;|&nbsp; &nbsp;View &nbsp;|&nbsp;Applicants
                </td>
                </thead>
            </div>
            <tbody>
            </tbody>
        </table>
</div>
        <!-- button group -->

    </div>
</div>
<!-- contents -->
</div>

        </div>

<!-- Gender dialog -->

<!-- Modal for Add -->
<!--div hidden class="modal-demo" id="AdvertJobModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


        <div class="top_header">
            <h3 class="title">Add Advertisment</h3>
        </div>


                <form class="form-horizontal" id="reportForm">

                    <div class="self-row">
                        <label for="JobTitle" class="Label-left">Job Title</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="JobTitle" id="JobTitle" placeholder="Job Title">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Department" class="Label-left">Department</label>
                        <div class="right-sec">
                            <input type="hidden" name="Department" id="Department">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Description" class="Label-left">Description</label>
                        <div class="right-sec">
                            <textarea name="Description" id="Description" placeholder="Description of Advertisement" rows="11"></textarea>
                        </div>
                        <br>
                    </div>
                    <div class="self-row">
                        <label for="MinimumQualification" class="Label-left">Minimum Qualification</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="MinimumQualification" id="MinimumQualification" placeholder="Minimum Qualification">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryStartRange" class="Label-left">Salary Start Range</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="SalaryStartRange" id="SalaryStartRange" placeholder="Salary Range From">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryEndRange" class="Label-left">Salary End Range</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="SalaryEndRange" id="SalaryEndRange" placeholder="Salary Range To">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeStartLimit" class="Label-left">Age Start Limit</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="AgeStartLimit" id="AgeStartLimit" placeholder="Age Limit From">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeEndLimit" class="Label-left">Age End Limit</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="AgeEndLimit" id="AgeEndLimit" placeholder="Age Limit To">
                        </div>
                    </div>

                    <div class="self-row">
                        <label for="AvailablePositions" class="Label-left">Available Positions</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="AvailablePositions" id="AvailablePositions" placeholder="Available Positions">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="DateOfExpiry" class="Label-left">Date of Expiry</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" id="dateOfOccurrence"  name="DateOfExpiry">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="refrence_no" class="Label-left">Job Reference #</label>
                        <div class="right-sec">
                            <span id="refrence_v"></span>
                            <input type="hidden" class="textfield" id="refrence_no"  name="refrence_no" readonly>
                        </div>
                    </div>

                </form>

            <div class="modal-footer">
                <button type="button" id="submitForm" class="btn btn-primary send">Submit </button>
                <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
            </div>


</div>-->


<!----Modal For edit --->
<!--<div id="EditModal" class="modal-demo" style="display: none;">
        <div class="top_header">
            <h3 class="title">Edit Job Advertisment</h3>
        </div>

                <form class="form-horizontal" id="reportFormEdit">

                    <div class="self-row">
                        <label for="JobTitle" class="Label-left">Job Title</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="EJobTitle" id="EJobTitle" placeholder="Job Title">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Department" class="Label-left">Department</label>
                        <div class="right-sec">
                            <input type="hidden" name="EDepartment" id="EDepartment">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Description" class="Label-left">Description</label>
                        <div class="right-sec">
                            <textarea name="EDescription" id="EDescription" placeholder="Description of Advertisement" rows="11"></textarea>
                        </div>
                        <br>
                    </div>
                    <div class="self-row">
                        <label for="MinimumQualification" class="Label-left">Minimum Qualification</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="EMinimumQualification" id="EMinimumQualification" placeholder="Minimum Qualification">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryStartRange" class="Label-left">Salary Start Range</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="ESalaryStartRange" id="ESalaryStartRange" placeholder="Salary Range From">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryEndRange" class="Label-left">Salary End Range</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="ESalaryEndRange" id="ESalaryEndRange" placeholder="Salary Range To">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeStartLimit" class="Label-left">Age Start Limit</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="EAgeStartLimit" id="EAgeStartLimit" placeholder="Age Limit From">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeEndLimit" class="Label-left">Age End Limit</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="EAgeEndLimit" id="EAgeEndLimit" placeholder="Age Limit To">
                        </div>
                    </div>

                    <div class="self-row">
                        <label for="AvailablePositions" class="Label-left">Available Positions</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" name="EAvailablePositions" id="EAvailablePositions" placeholder="Available Positions">
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="DateOfExpiry" class="Label-left">Date of Expiry</label>
                        <div class="right-sec">
                            <input type="text" class="textfield" id="EDateOfExpiry"  name="EDateOfExpiry">
                            <input type="hidden" class="textfield" id="EAdvertiseID"  name="EAdvertiseID">
                        </div>
                    </div>

                </form>

            <div class="modal-footer">
                <button type="button" id="submitEditForm" class="btn btn-primary send">Submit </button>
                <button type="button" class="btn btn-default close" data-dismiss="modal" onclick="Custombox.close();">Close</button>
             </div>


</div>-->
<!---------------------Maybe Final for Add Advertisment details------------------>
<div id="AdvertJobModal" class="modal-demo view_model" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Add Advertisement</h3>
    </div>
    <form class="form-horizontal" id="reportForm">
        <div class="container_view" style="float:left;width:990px;height:auto;">
            <div class="right_sec">
                <div class="left_block">
                    <div class="left_lbl">
                        <h5>Job Title</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="JobTitle" id="JobTitle" placeholder="Job Title">
                    </div>

                    <div class="left_lbl">
                        <h5>Department</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="hidden" name="Department" id="Department">
                    </div>

                    <div class="left_lbl">
                        <h5>Description</h5>
                    </div>
                    <div class="right_lbl">
                        <textarea name="Description" id="Description" placeholder="Description of Advertisement" style="wodth:180px;height: 96px;"></textarea>
                    </div>

                    <div class="left_lbl" style="margin-top: 60px;">
                        <h5>Minimum Qualification</h5>
                    </div>
                    <div class="right_lbl" style="margin-top: 60px;">
                        <input type="text" class="textfield" name="MinimumQualification" id="MinimumQualification" placeholder="Minimum Qualification">
                    </div>

                    <div class="left_lbl">
                        <h5>Salary Start Range</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="SalaryStartRange" id="SalaryStartRange" placeholder="Salary Range From">
                    </div>

                </div>

                <div class="left_block" >
                    <div class="left_lbl">
                        <h5>Salary End Range</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="SalaryEndRange" id="SalaryEndRange" placeholder="Salary Range To">
                    </div>

                    <div class="left_lbl">
                        <h5>Age Start Limit</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="AgeStartLimit" id="AgeStartLimit" placeholder="Age Limit From">
                    </div>

                    <div class="left_lbl">
                        <h5>Age End Limit</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="AgeEndLimit" id="AgeEndLimit" placeholder="Age Limit To">
                    </div>

                    <div class="left_lbl">
                        <h5>Available Positions</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" name="AvailablePositions" id="AvailablePositions" placeholder="Available Positions">
                    </div>

                    <div class="left_lbl">
                        <h5>Date of Expiry</h5>
                    </div>
                    <div class="right_lbl">
                        <input type="text" class="textfield" id="dateOfOccurrence"  name="DateOfExpiry">
                    </div>

                    <div class="left_lbl">
                        <h5>Job Reference Number</h5>
                    </div>
                    <div class="right_lbl">
                        <span id="refrence_v"></span>
                        <input type="hidden" class="textfield" id="refrence_no"  name="refrence_no" readonly>
                    </div>

                </div>




            </div>
            <style>
                .btn_rights{
                    float:right;
                    width:auto;
                    height: 35px;
                    background-color: #6AAD6A;
                    margin-right: 35px;
                    color:white;
                }
                .btn_rights:hover{
                    cursor:pointer;
                }
            </style>

            <div class="btn_row" style="float:left;width:100px;height:30px;margin-top: 250px;">
                <button type="button" id="submitForm" class="btn btn-primary send">Submit </button>
            </div>

    </form>
</div>
</div>
<!---------------------------------end of final -------------------------->

<!-----------Model for View---------------->

<!---------------------Maybe Final for Edit job details------------------>
<div id="EditModal" class="modal-demo view_model" style="display: none;">
<div class="top_header">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only"></span>
    </button>
    <h3 class="title">Report Details</h3>
</div>
<form class="form-horizontal" id="reportFormEdit">
<div class="container_view" style="float:left;width:990px;height:auto;">
<div class="right_sec">
<div class="left_block">
    <div class="left_lbl">
        <h5>Job Title</h5>
    </div>
    <div class="right_lbl">
            <input type="text" class="textfield" name="EJobTitle" id="EJobTitle" placeholder="Job Title">
    </div>

    <div class="left_lbl">
        <h5>Department</h5>
    </div>
    <div class="right_lbl">
        <input type="hidden" name="EDepartment" id="EDepartment">
    </div>

    <div class="left_lbl">
        <h5>Description</h5>
    </div>
    <div class="right_lbl">
        <textarea name="EDescription" id="EDescription" placeholder="Description of Advertisement" style="width:180px;height:90px;"></textarea>
    </div>

    <div class="left_lbl" style="margin-top: 60px;">
        <h5>Minimum Qualification</h5>
    </div>
    <div class="right_lbl" style="margin-top: 60px;">
        <input type="text" class="textfield" name="EMinimumQualification" id="EMinimumQualification" placeholder="Minimum Qualification">
    </div>

    <div class="left_lbl">
        <h5>Salary Start Range</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" name="ESalaryStartRange" id="ESalaryStartRange" placeholder="Salary Range From">
    </div>

</div>

<div class="left_block" >
    <div class="left_lbl">
        <h5>Salary End Range</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" name="ESalaryEndRange" id="ESalaryEndRange" placeholder="Salary Range To">
    </div>

    <div class="left_lbl">
        <h5>Age Start Limit</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" name="EAgeStartLimit" id="EAgeStartLimit" placeholder="Age Limit From">
    </div>

    <div class="left_lbl">
        <h5>Age End Limit</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" name="EAgeEndLimit" id="EAgeEndLimit" placeholder="Age Limit To">
    </div>

    <div class="left_lbl">
        <h5>Available Positions</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" name="EAvailablePositions" id="EAvailablePositions" placeholder="Available Positions">
    </div>

    <div class="left_lbl">
        <h5>Date of Expiry</h5>
    </div>
    <div class="right_lbl">
        <input type="text" class="textfield" id="EDateOfExpiry"  name="EDateOfExpiry">
        <input type="hidden" class="textfield" id="EAdvertiseID"  name="EAdvertiseID">
    </div>

</div>




</div>
<style>
    .btn_rights{
        float:right;
        width:auto;
        height: 35px;
        background-color: #6AAD6A;
        margin-right: 35px;
        color:white;
    }
    .btn_rights:hover{
        cursor:pointer;
    }
</style>

<div class="btn_row" style="float:left;width:100px;height:30px;margin-top: 250px;">
    <button type="button" id="submitEditForm" class="btn btn-primary send">Submit </button>
</div>

</form>
</div>
</div>
<!---------------------------------end of final -------------------------->

<div id="AdvertViewDetails" class="modal-demo" style="display: none;">

    <div class="top_header">
        <!--<button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
            </button>-->
        <h3 class="title">View Job Advertisment</h3>
    </div>
                <form class="form-horizontal" id="reportFormView">

                    <div class="self-row">
                        <label for="JobTitle" class="Label-left">Job Title</label>
                        <div class="right-sec">
                            <span name="EJobTitle" id="VJobTitle" placeholder="Job Title"></span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Department" class="Label-left">Department</label>
                        <div class="right-sec">
                            <span  name="EDepartment" id="VDepartment"> <span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="Description" class="Label-left">Description</label>
                        <div class="right-sec">
                            <span name="EDescription" id="VDescription"></span>
                            <!--                            <input type="text" class="textfield"  placeholder="Discipline Breached">-->
                        </div>
                        <br>
                    </div>
                    <div class="self-row">
                        <label for="MinimumQualification" class="Label-left">Minimum Qualification</label>
                        <div class="right-sec">
                            <span name="EMinimumQualification" id="VMinimumQualification"></span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryStartRange" class="Label-left">Salary Start Range</label>
                        <div class="right-sec">
                            <span name="ESalaryStartRange" id="VSalaryStartRange" > <span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="SalaryEndRange" class="Label-left">Salary End Range</label>
                        <div class="right-sec">
                            <span  name="ESalaryEndRange" id="VSalaryEndRange" ></span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeStartLimit" class="Label-left">Age Start Limit</label>
                        <div class="right-sec">
                            <span name="EAgeStartLimit" id="VAgeStartLimit"> <span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="AgeEndLimit" class="Label-left">Age End Limit</label>
                        <div class="right-sec">
                            <span name="EAgeEndLimit" id="VAgeEndLimit"> <span>
                        </div>
                    </div>

                    <div class="self-row">
                        <label for="AvailablePositions" class="Label-left">Available Positions</label>
                        <div class="right-sec">
                            <span name="EAvailablePositions" id="VAvailablePositions"> </span>
                        </div>
                    </div>
                    <div class="self-row">
                        <label for="DateOfExpiry" class="Label-left">Date of Expiry</label>
                        <div class="right-sec">
                            <span id="VDateOfExpiry" > </span>
                        </div>
                    </div>

                </form>




</div>
<!----End--->
<style>
    .modal-footer{float: right;
        width: auto;
        height: auto;
        margin-right: 20px;
        margin-top: 5px;
        margin-bottom: 10px;}
</style>
<!---->

<!-----------------------------------------end models----------------------------->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/recruitment_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>
<!-- left side menu End -->

<!---------------Model Javascript--------------------------->
<script type="text/javascript">
    $(function(){
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".close" });
        $('#modaltrigger').leanModal({ top: 110, overlay: 0.45, closeButton: ".btn-default" });

    });
</script>
<!--------------------------End Model--------------------------->

<!--------------------------Data Tables--------------------------->
<script type="text/javascript">
    var oTable;
$(document).ready(function(e){
    //Select department
    var LoadDepartment = $('#Department');
    var url = "<?php echo base_url(); ?>human_resource/loadDepartment";
    var id = "DepartmentID";
    var text = "DepartmentName";
    var minInputLength = 0;
    var placeholder = "Select Department";
    var multiple = false;
    commonSelect2(LoadDepartment,url,id,text,minInputLength,placeholder,multiple);

    ///////////////////////////Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //Filter Selectors
    //Select department
    var selectDepartmentsSelector = $('#selectDepartments');
    var url = "<?php echo base_url(); ?>human_resource/loadDepartment";
    var id = "DepartmentID";
    var text = "DepartmentName";
    var minInputLength = 0;
    var placeholder = "Select Department";
    var multiple = false;
    commonSelect2(selectDepartmentsSelector,url,id,text,minInputLength,placeholder,multiple);
    // filter datatable
    selectDepartmentsSelector.on('change',function(e){
        oTable.fnDestroy();
        var filteredselectDepartments = $(this).val();
        var filters = 'aoData.push({"name":"filteredselectDepartments","value":'+$(this).val()+'});';
        commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });


    //Check Job Ad Type if Close or open Using Ajax //
    var selectJobAdTypeSelector = $('#selectJobAdOpen');
    var url = "<?php echo base_url(); ?>human_resource/JobAdCloseOpen";
    var id = "jobAdID";
    var text = "TITLE";
    var minInputLength = 0;
    var placeholder = "Select Job Status";
    var multiple = false;
    commonSelect2(selectJobAdTypeSelector,url,id,text,minInputLength,placeholder,multiple);
    // filter datatable
    selectJobAdTypeSelector.on('change',function(e){
        oTable.fnDestroy();
        var filteredselectjobstatus = $(this).val();
        var filters = 'aoData.push({"name":"filteredselectjobstatus","value":'+$(this).val()+'});';
        commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });


    //Select Month
    var selectMonth = $('#selectMonth');
    var url = "<?php echo base_url(); ?>human_resource/loadMonth";
    var id = "MonthID";
    var text = "MonthName";
    var minInputLength = 0;
    var placeholder = "Select Month";
    var multiple = false;
    commonSelect2(selectMonth,url,id,text,minInputLength,placeholder,multiple);
    // filter datatable
    selectMonth.on('change',function(e){
        oTable.fnDestroy();
        var filteredselectMonth = $(this).val();
        var filters = 'aoData.push({"name":"filteredselectMonth","value":'+$(this).val()+'});';
        commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });

//Select Year
    var selectYear = $('#selectYear');
    var url = "<?php echo base_url(); ?>human_resource/loadYear";
    var id = "YearName";
    var text = "YearName";
    var minInputLength = 0;
    var placeholder = "Select Year";
    var multiple = false;
    commonSelect2(selectYear,url,id,text,minInputLength,placeholder,multiple);
    // filter datatable
    selectYear.on('change',function(e){
        oTable.fnDestroy();
        var filteredselectYear = $(this).val();
        var filters = 'aoData.push({"name":"filteredselectYear","value":'+$(this).val()+'});';
        commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });

    //Filter the SearchBox Of Table.
    $('#dataTableFilter').on('keyup',function(e){
        oTable.fnFilter($(this).val());
    });
    ///////////////////////End Of Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\

    $('#submitForm').on('click',function(e){
        e.preventDefault();

        //Need To Post Form Using Ajax When Button Clicked.
        var formData = $('#reportForm').serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/advertise_submit_form",
            type: "POST",
            data:formData,
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#reportForm")[0].reset();
                    $(".modal-demo").hide();
                    Custombox.close();
                    oTable.fnClearTable();
                    oTable.fnDraw();
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
        var post='ref';
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/advertise_reference_no",
            type: "POST",
            data:{id:post},
            success:function(output){
                var ref=parseInt(output);
                var IncRef =ref + 1;
                $("#refrence_v").html('').append(IncRef);
                $("#refrence_no").html('').val(IncRef);
            }
        });

    });

// For edit Form submitting

    $('#submitEditForm').on('click',function(e){
        e.preventDefault();

        //Need To Post Form Using Ajax When Button Clicked.
        var formData = $('#reportFormEdit').serialize();
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/advertise_update_record",
            type: "POST",
            data:formData,
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    $("#reportFormEdit")[0].reset();
                    Custombox.close();
                    oTable.fnClearTable();
                    oTable.fnDraw();
                    //$('#reportedAdvertTable').reload();
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });
    //

//Need To Display the list of All The Reported/Violated Disciplines
    oTable ='';
    var tableSelector = $('#reportedAdvertTable');
    var url_DT = "<?php echo base_url(); ?>human_resource/advertise_submit_form/list";
    var aoColumns_DT = [
        /* ID */ {
            "mData": "advertisementID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
       /*Job Refrenece No */ {
            "mData" : "ReferenceNo"
        },/*Job Title */ {
            "mData" : "Title"
        },
        /* DepTitle */ {
            "mData" : "DepTitle"
        },
        /* availPositions */ {
            "mData" : "availPositions"
        },
        /* availPositions */ {
            "mData" : "applieds"
        },
        /* Description  {
            "mData" : "Description"
        },*/
        /* minQualification */ /*{
            "mData" : "minQualification"
        },
        *//* Shows The Status Of minAgeRange *//* {
            "mData" : "AgeRange"
        },
        *//* Shows The Status Of salary Range *//* {
            "mData" : "salaryRange"
        }
        ,*/
        /* Shows The Status Of The datePosted */ {
            "mData" : "datePosted"
        }
        ,
        /* Shows The Status Of expiryDate */ {
            "mData" : "expiryDate"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "closeView"
        },
        /* Shows The Status Of The Report */ {
            "mData" : "ViewEditActionButtons"
        }
    ];
    var HiddenColumnID_DT = 'advertisementID';
    var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
    //End Of DataTables
///
    $(document).on('click','.AdvertEdit', function(e){

        /////
        var selectEDepartment = $('#EDepartment');
        var url = "<?php echo base_url(); ?>human_resource/loadDepartment";
        var tDataValues = {
            id: "DepartmentID",
            text: "DepartmentName"
        };
        var minInputLength = 0;
        var placeholder = "Select Department";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <p> " + e.text + "</p></div>";
        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEDepartment,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
        ///////

        var AdvertID = $(this).closest('tr').attr('data-id');
        //Getting Form Selectors For Specific Modal To Assign Values
        var modalSelector = $('#EditModal');
        var selectedTitle = modalSelector.find('#EJobTitle');
        //var selectedDepartment = modalSelector.find('#EDepartment');
        var selectedDescription = modalSelector.find('#EDescription');
        var selectedQualification = modalSelector.find('#EMinimumQualification');
        var selectedSalaryStartRange = modalSelector.find('#ESalaryStartRange');
        var selectedSalaryEndRange = modalSelector.find('#ESalaryEndRange');
        var selectedAgeStartLimit = modalSelector.find('#EAgeStartLimit');
        var selectedAgeEndLimit = modalSelector.find('#EAgeEndLimit');
        var selectedAvailablePositions = modalSelector.find('#EAvailablePositions');
        var selectedDateOfExpiry = modalSelector.find('#EDateOfExpiry');
        var selectEDepartment= modalSelector.find('#EDepartment');
        var AdvertiseID= modalSelector.find('#EAdvertiseID');
        //console.log(AdvertID);
        var postData = {
            AdvertID: AdvertID
        };
        $.ajax({
            url:"<?php echo base_url(); ?>human_resource/edit_advert_details/edit",
            type: "POST",
            data: postData,
            success:function(output){
                try
                {
                    //console.log(output); return;
                    //If Result is In JSON then Show The Edit Fields In Modal.
                    var json = JSON.parse(output);

                    selectedTitle.val(json.Title);
                    AdvertiseID.val(AdvertID);
                    selectedDescription.val(json.Description);
                    selectedQualification.val(json.minQualification);
                    selectedSalaryStartRange.val(json.salaryStartRange);
                   selectedSalaryEndRange.val(json.salaryEndRange);
                    selectedAgeStartLimit.val(json.minAgeRange);
                   selectedAgeEndLimit.val(json.maxAgeRange);
                    selectedAvailablePositions.val(json.availPositions);
                    selectedDateOfExpiry.val(json.expiryDate);
                    selectEDepartment.select2('data', {id:json.department_id, text:json.DepTitle});
                }
                catch(e)
                {
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }
        });
        Custombox.open({
            target: '#EditModal',
            effect: 'fadein'
        });
        e.preventDefault();

    });

    ///


    $(document).on('click','.AdvertViewDetails', function(e){

        var AdvertID = $(this).closest('tr').attr('data-id');
        var modalSelector = $('#AdvertViewDetails');
        var selectedTitle = modalSelector.find('#VJobTitle');
        //var selectedDepartment = modalSelector.find('#EDepartment');
        var selectedDescription = modalSelector.find('#VDescription');
        var selectedQualification = modalSelector.find('#VMinimumQualification');
        var selectedSalaryStartRange = modalSelector.find('#VSalaryStartRange');
        var selectedSalaryEndRange = modalSelector.find('#VSalaryEndRange');
        var selectedAgeStartLimit = modalSelector.find('#VAgeStartLimit');
        var selectedAgeEndLimit = modalSelector.find('#VAgeEndLimit');
        var selectedAvailablePositions = modalSelector.find('#VAvailablePositions');
        var selectedDateOfExpiry = modalSelector.find('#VDateOfExpiry');
        var selectedDepartment= modalSelector.find('#VDepartment');

        var postData = {
            AdvertID: AdvertID
        };
        $.ajax({
            url:"<?php echo base_url(); ?>human_resource/edit_advert_details/edit",
            type: "POST",
            data: postData,
            success:function(output){
                try
                {
                    //console.log(output); return;
                    //If Result is In JSON then Show The Edit Fields In Modal.
                    var json = JSON.parse(output);
                    //$('#AdvertViewDetails').empty();
                    selectedTitle.html('').append(json.Title);
                    //selectedDepartment.append(json.DepTitle);
                    selectedDescription.html('').append(json.Description);
                    selectedQualification.html('').append(json.minQualification);
                    selectedSalaryStartRange.html('').append(json.salaryStartRange);
                    selectedSalaryEndRange.html('').append(json.salaryEndRange);
                    selectedAgeStartLimit.html('').append(json.minAgeRange);
                    selectedAgeEndLimit.html('').append(json.maxAgeRange);
                    selectedAvailablePositions.html('').append(json.availPositions);
                    selectedDateOfExpiry.html('').append(json.expiryDate);
                    selectedDepartment.html('').append(json.DepTitle);
                }catch(e)
                {
                    var data = output.split('::');
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] === "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }
        });
        //console.log(AdvertID);
        Custombox.open({
            target: '#AdvertViewDetails',
            effect: 'fadein'
        });
        e.preventDefault();

    });

    //DatePickers
    $('#dateOfOccurrence').datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });

    //DatePickers
    $('#EDateOfExpiry').datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });


    //Defining Width For All Selectors Of the Filters
    $('.select2-container').css("width","180px");
    //Adding table-row Class to DataTables For Better look
    $('tbody').addClass("table-row");



    $(document).on('click','.CloseJob', function(e){
        if(confirm("Are You Sure to Close This Job ?")){
        var formData = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/advertise_close",
            type: "POST",
            data:{ID:formData},
            success:function(output){
                var data = output.split('::');
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                    oTable.fnClearTable();
                    oTable.fnDraw();
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });}
    });

    $("#modaltrigger").on('click',function(){
        var post='ref';
        $.ajax({
            url: "<?php echo base_url(); ?>human_resource/advertise_reference_no",
            type: "POST",
            data:{id:post},
            success:function(output){
                var ref=parseInt(output);
                var IncRef =ref + 1;
                $("#refrence_v").html('').append(IncRef);
                $("#refrence_no").html('').val(IncRef);
            }
        });
});
});
</script>

