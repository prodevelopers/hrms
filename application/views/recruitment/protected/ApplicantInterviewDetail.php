<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<!--------------Custome css for model-------->
<style>
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }
    .grey{
        background: #808080;
        color:white;
    }
    #feedback{
        width:48%;
    }
</style>


<style type="text/css">
tbody td:nth-child(even) {
    background: #f8f8f8;
    font-style: italic;
}
tbody td:nth-child(odd) {
    background: #f1f1f1;
    font-weight: bold;
}
</style>
<script type="text/javascript">
/// Function For Print Report
	function print_report(id)
	{
		window.open('human_resource/emp_profile_print_report/'+id, "", "width=800,height=600");
	}
</script>
<!-- contents -->
<?php
//employee ID from the Segment
$employeeIDFromSegment = $this->uri->segment(3);
?>
<div class="contents-container">
	<div class="bredcrumb">Dashboard / Human Resource / Applicant Information View</div>
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
<style>
    .imgsize{width:auto;height:150px;}
    .nobg{background: none;}
    .btn-top{
        background: #6AAD6A;
        color:white;
        padding: 8px;
        margin-left: 35px;
    }
    .sendbtn{width:30px;height:50px;background: #6AAD6A;padding: 7px;color:white;}
</style>

	<div class="right-contents">

			<table cellspacing="0">
				<thead class="table-head class">
					<td colspan="5">Applicant Information</td>
				</thead>
				<tbody>
				<tr class="table-row">
					<td rowspan="4" width="150px"><?php $ApplicantAvatar = empAvatarExist($Applicant_info->avatar);?><img src="<?php echo ($ApplicantAvatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$Applicant_info->avatar: $ApplicantAvatar ?>" class="img-responsive img imgsize" > <b class="caret"></b></td>
					<td>Applicant Name</td>
                    <td><?php echo $Applicant_info->fullName;?></td>
                    <td>Father Name</td>
                    <td><?php echo $Applicant_info->fatherName;?></td>

				</tr>
				<tr class="table-row">
                    <td>Contact Number</td>
                    <td><?php echo $Applicant_info->contactNo;?></td>
                    <td>Telephone Number</td>
                    <td><?php echo $Applicant_info->residentPhone;?></td>
				</tr>	
				<tr class="table-row">
                    <td>CNIC</td>
                    <td><?php echo $Applicant_info->CNIC;?></td>
                    <td>Applicant Email</td>
                    <td><?php echo $Applicant_info->officialEmailAddress;?></td>
				</tr>
                <tr class="table-row">
                    <td>Permanent Address</td>
                    <td><?php echo $Applicant_info->permanentAddress;?></td>
                    <td>Current Address</td>
                    <td><?php echo $Applicant_info->currentAddress;?></td>
                </tr>
				</tbody>	
			</table>
        <table class="table marge" cellspacing="0" width="100%">
            <thead class="table-head">
            <td>Degree / Diploma / Certificate </td>
            <td>Title </td>
            <td>Specialization</td>
            <td>GPA/Score</td>
            <td>Institute</td>
            <td>Year Of Completion</td>
            </thead>
            <?php if(!empty($qualificationInfo)){
                foreach($qualificationInfo as $qualification){
                    ?>
                    <tbody>
                    <tr class="table-row">
                        <td><?php echo $qualification->qualification_title?></td>
                        <td><?php echo $qualification->title?></td>
                        <td><?php echo $qualification->specialization?></td>
                        <td><?php echo $qualification->gpa_division?></td>
                        <td><?php echo $qualification->institute;?></td>
                        <td><?php echo $qualification->comp_year;?></td>
                    </tr>
                    </tbody>
                <?php }}?>
        </table>

        <table class="table marge" cellspacing="0" width="100%">
            <thead class="table-head">
            <td>Applicant Skill </td>
            <td>Skill Level</td>
            <td>Date Added</td>

            </thead>
            <?php if(!empty($skills)){
                foreach($skills as $skillsApplicant){
                    ?>
                    <tbody>
                    <tr class="table-row">
                        <td><?php echo $skillsApplicant->skill_name?></td>
                        <td><?php echo $skillsApplicant->skill_level?></td>
                        <td><?php echo date_format_helper($skillsApplicant->dateAdded);?></td>
                    </tr>
                    </tbody>
                <?php }}?>
        </table>
        <table class="table marge" cellspacing="0" width="100%">
            <thead class="table-head">
            <td>Applied for Job</td>
            <td>Status Type</td>
            <td>Status</td>
            <td>Applied On</td>
            <td>Job Expiry Date</td>
            <td>Action</td>

            </thead>
            <?php if(!empty($appliedJob)){
                foreach($appliedJob as $Job){
                    ?>

                    <tbody>
                    <tr class="table-row" <?php echo isset($Job->appAppliedJobID)?'data-id="'.$Job->appAppliedJobID.'"':'' ?>>
                        <td><?php echo $Job->title?></td>
                        <td><?php echo $Job->statusTitle;?></td>
                        <td><?php echo $Job->Title;?></td>
                        <td><?php echo date_format_helper($Job->dateAppliedOn);?></td>
                        <td><?php echo date_format_helper($Job->expiryDate);?></td>
                        <td><?php
                            if($Job->JobAD_ID == 2)
                            {
                                echo '<input type="button" class="grey btn" name="shortListBtn" id="shortListBtn" title="Job Closed !"  value="Short List">';
                            }
                            elseif($Job->applicantStatusTypeID == 1)
                            {
                                echo '<input type="button" class="green btn" name="shortListBtn" id="shortListBtn" value="Short List">';
                            }else{
                                echo '<input type="button" class="red btn" name="shortListBtn" id="shortListBtn" title ="Shortlisted For Job" value="ShortListed" disabled="disabled">';
                            }
                            ?></td>
                    </tr>
                    </tbody>
                <?php }}?>
        </table>
        <table class="table marge" cellspacing="0" width="100%">
            <thead class="table-head">
            <td>Experience Title</td>
            <td>Company</td>
            <td>From Date</td>
            <td>To Date</td>
            <td>Date Created</td>

            </thead>
            <?php if(!empty($AppExperience)){
                foreach($AppExperience as $Experience){
                    ?>

                    <tbody>
                    <tr class="table-row">
                        <td><?php echo $Experience->Exp_position?></td>
                        <td><?php echo $Experience->Company;?></td>
                        <td><?php echo date_format_helper($Experience->FromDate);?></td>
                        <td><?php echo date_format_helper($Experience->ToDate);?></td>
                        <td><?php echo date_format_helper($Experience->DateCreated) ;?></td>
                    </tr>
                    </tbody>
                <?php }}?>
        </table>

        <div class="form-left">
            <?php //if ($employee >0){
            //foreach($employee as $employee):?>
            <?php if(isset($Present) && !empty($Present)){?>
            <form action="human_resource/ApplicantFeedbackInterview" method="post">
                <div class="row2">
                    <br class="clear">
                    <h4>FeedBack/Score</h4>
                <textarea name="feedback" id="feedback" style="color: #000000" ><?php if(!empty($feedback)){echo $feedback;}?></textarea>
                    <input type="hidden" name="id" value="<?php echo $this->uri->segment(3);?>">
                </div>

                <br class="clear">
                <div class="row2">
                    <h4>Score</h4>
                    <select name="Score" id="score" required="required">
                        <option>Select Score</option>
                        <option value="">Select Score</option>
                        <option value="1/10">1/10</option>
                        <option value="2/10">2/10</option>
                        <option value="3/10">3/10</option>
                        <option value="4/10">4/10</option>
                        <option value="5/10">5/10</option>
                        <option value="6/10">6/10</option>
                        <option value="7/10">7/10</option>
                        <option value="8/10">8/10</option>
                        <option value="9/10">9/10</option>
                        <option value="10/10">10/10</option>

                    </select>
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Attendance Status</h4>
                    <?php
                        $totalPresents = array(
                            0 => 'Absent',
                            1 => 'Present'
                        );
                    ?>

                    <select name="present" id="present" required="required">

                        <?php
                        foreach($totalPresents as $key=>$val){
                            echo '<option value ="'.$key.'" '.((isset($Present) && intval($key)===intval($Present))?"selected='selected'":"").'> '.$val.' </option> ';
                        }
                        ?>

                    </select>
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Selection Status</h4>
                    <?php if(isset($stat) && !empty($stat) && $stat == 1){echo form_dropdown('InterviewStatus',$status,$stat, "id='leavetype' disabled='disabled'");} else{echo form_dropdown('InterviewStatus',$status,$stat, "id='leavetype' required='required'"," required='required'");}?>
                </div>

                <!-- button group -->
                <div class="row2">
                    <div class="button-group">
                        <input type="hidden" value="update" name="update" />
                        <?php if(isset($stat) && !empty($stat) && $stat == 1){?>
                        <input type="submit" class="btn green"  value="Update" name="update" disabled="disabled" style="background: darkgray" title="Candidate is Selected !"/>
                        <?php }else{?>
                            <input type="submit" class="btn green"  value="Update" name="update" />
                        <?php }?>
                    </div>
                </div>

            </form>
            <?php }else{?>



            <form action="human_resource/ApplicantFeedbackInterview" method="post">
                <div class="row2">
                    <br class="clear">
                    <h4>FeedBack/Score</h4>
                <textarea name="feedback" id="feedback" required="required"></textarea>
                    <input type="hidden" name="id" value="<?php echo $this->uri->segment(3);?>">
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Score</h4>
                    <select name="Score" id="score" required="required">
                        <option value="">Select Score</option>
                        <option value="1/10">1/10</option>
                        <option value="2/10">2/10</option>
                        <option value="3/10">3/10</option>
                        <option value="4/10">4/10</option>
                        <option value="5/10">5/10</option>
                        <option value="6/10">6/10</option>
                        <option value="7/10">7/10</option>
                        <option value="8/10">8/10</option>
                        <option value="9/10">9/10</option>
                        <option value="10/10">10/10</option>

                    </select>
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Status</h4>
                    <select name="present" id="present" required="required">
                        <option value="">Select Status</option>
                        <option value="1">Present</option>
                        <option value="0">Absent</option>

                    </select>
                </div>
                <br class="clear">
                <div class="row2">
                    <h4>Selection Status</h4>
                    <?php echo form_dropdown('InterviewStatus',$status,'', "id='leavetype' required='required'"," required='required'");?>
                </div>


                <!-- button group -->
                <div class="row2">
                    <div class="button-group">
                        <input type="submit" class="btn green"  value="Submit" name="add" />
                    </div>
                </div>

            </form>
            <?php }?>
        </div> <? //End Of Form ?>

    </div>

	<!-- Menu left side  -->
    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/recruitment_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
    <script>
        $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
        $('#close-panel-bt').click(function() {
            $.panelslider.close();
        });
    </script>
    <!-- leftside menu end -->

        <!------------------Script of the Email Btn without Libraies of Bootstrap-------->
        <!-------------------------Edit Model Finish------------------------->
        <!------------------Script of the Email Btn without Libraies of Bootstrap-------->
        <script>
            $('#sendEmailButton').on('click', function(e){
                Custombox.open({
                    target: '#sendEmail',
                    effect: 'fadein'
                });
                e.preventDefault();
            });

            // Ajax Function for Applicant Shortlisted //
            $(document).ready(function(e){
               $('#shortListBtn').on('click', function (e) {
                   var applicantAppliedJobID = $(this).closest('tr').attr('data-id');
                   var postData ={
                       ID:applicantAppliedJobID
                   };
                   $.ajax({
                       url: "<?php echo base_url()?>human_resource/shortListApplicant/",
                       data:postData,
                       type:"POST",
                       success: function (output) {
                           //Output Here If Success.
                           var splitCharacter = '>>>';
                           var data;
                           var topData;
                           if(output.indexOf(splitCharacter) > -1){
                               topData = output.split('>>>');
                               data = topData[0].split('::');
                           }else{
                               data = output.split('::');
                           }
                           if(data[0] === "OK"){
                               Parexons.notification(data[1],data[2]);
                               location.reload();
                           }else if(data[0] === "FAIL"){
                               Parexons.notification(data[1],data[2]);
                           }
                       }
                   });
               });
            }); // End of Applicant Shortlisted Function
        </script>
