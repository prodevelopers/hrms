<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<?php
//var_dump($employee_id); ?>
<style type="text/css">
    #autoSuggestionsList
    {
        width: 205px;
        overflow: hidden;
        border:1px solid #CCC;
        -moz-border-radius: 5px;
        border-radius: 5px;

        display:none;
    }
    #autoSuggestionsList li
    {
        list-style:none;
        cursor:pointer;
        padding:5px;
        font-size:14px;
        font-family:Arial;
        margin:2px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    #autoSuggestionsList li:hover
    {
        border:1px solid #CCC;
    }
    #pagination {
        float: left;
        padding: 5px;
        margin-top: 15px;
    }

    #pagination a {
        padding: 5px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        border: 1px solid #CCC;
    }

    #pagination a:hover {
        border: 1px solid #666;
    }

    .mytab tr:nth-child(odd) td {
        background-color: #ECECFF;
    }

    .mytab tr:nth-child(even) td {
        background-color: #F0F0E1;
    }

    .paginate_button {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor: pointer;
    }

    .paging_full_numbers
    {
        margin-top: 8px;
    }
    .dataTables_info {
        color: #3474D0;
        font-size: 14px;

        margin: 6px;
    }

    .paginate_active {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        color: #3474D0;
        font-weight: bold;
    }

    .dataTables_filter {
        float: right;
    }

    select[name="task_name"], select[name="status_type"], select[name="department_name"] {
        width: 30%;
    }

    .move {
        position: relative;
        left: 16%;
        top: 60px;
    }

    .resize {
        width: 220px;
    }

    .rehieght {
        position: relative;
        top: -5px;
    }

    .dataTables_length {
        position: relative;
        top: -40px;
        left: 3%;
        font-size: 1px;
    }

    .dataTables_length select {
        width: 60px;
    }

    .dataTables_filter {
        position: relative;
        top: -67px;
        left: -20%;
    }

    .dataTables_filter input {
        width: 180px;
    }

    .revert {
        margin-left: -15px;
    }

    .marge {
        margin-top: 40px;
    }
    .select2TemplateImg { padding:0.2em 0; clear: both;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .select2-chosen div.select2TemplateImg img{ width: 24px; height: 24px;}
    p{
        margin:0 !important;
    }
    textarea {
        resize: none;
         }

    .select2-result-selectable{clear:both;}
    #dataTableFilter{
        width: 22% !important;
    }
    /************BTN*********************/
.btns_row{
    float:right;
    width:380px;
    height:30px;
}
     .btn{
         float:left;
        /* margin-right:325px;*/
         background: indianred;
         padding-left:10px;
         padding-right:10px;
         padding-top:6px;
         padding-bottom:6px;
         color:white;
     }

    /***********************model css**********************************************/
    /*modal CSS*/
    .modal-demo {

        float:left;
        background-color: #FFF;
        padding-bottom: 15px;
        border: 1px solid #000;
        border-radius: 10px;
        box-shadow: 0 8px 6px -6px black;
        display: none;
        text-align: left;
        width: 600px;


    }
    .title {
        border-bottom: 1px solid #ccc;
        font-size: 18px;
        line-height: 18px;
        padding: 10px 20px 15px;
    }

    h1, h2, h4 {
        font-family: "Dosis",sans-serif;
    }
    .text{
        padding: 0 20px 20px;
    }
    .text , .title{
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }
    button.close {
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
        border: 0 none;
        cursor: pointer;
        padding: 0;
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
    }

    .close {
        color: #ffffff;
        float: right;
        font-size: 21px;
        font-weight: 700;
        line-height: 1;
        text-shadow: 0 1px 0 #fff;
    }

    .pop_row{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .pop_row_txtarea{
        float:left;
        width:100%;
        height:auto;
        margin-bottom: 7px;
    }
    .left_side{
        float:left;
        width:40%;
        height:auto;
    }
    .left_side h5{
        line-height: 35px;
        padding-left: 20px;
    }
    .right_side{
        float:left;
        width:60%;
        height:auto;
    }
    .right_txt{
        width:240px important;
        height:auto important;
    }
    .top_header{
        float:left;
        width:100%;
        height:45px;
        background-color: #4f94cf;
        margin-bottom: 10px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        border-bottom: 2px solid rosybrown
    }
    .top_header h3{
        color:#ffffff
    }
    .right_txt_area{
        width:240px;
        height:100px;
        padding:7px;
    }
    .btn-row{
        width:auto;
        float:right;
        margin-right:-15px;
    }
    .btn-pop{
        float:right;
        /*width:80px;*/
        padding: 5px 10px 5px 10px;
        background-color: #6AAD6A;
        color:white;
        margin-right: 120px;
    }
    .btn-pop:hover{
        cursor: pointer
    }
    .view_model{
        width:1000px;
    }
    .left_photo{
        float:left;
        width:100px;
        height:100px;
        margin-right: 10px;
        margin-top: 10px;
        border: 1px solid #d3d3d3
    }
    .right_sec{
        float:left;
        width:820px;
        height:auto;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .left_lbl h5{
        line-height: 35px;
    }
    .left_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-right: 5px;
        margin-bottom:10px;
        padding-left: 5px;
    }
    .right_lbl{
        float:left;
        width:190px;
        height:35px;
        margin-bottom:10px
    }
    .right_lbl{
        line-height: 35px;
        padding-left: 5px;
    }
    .left_block{
        width:400px;
        height:auto;
        float:left;
    }
    div.text form{
        display: inline-block;
    }

    .modal-footer{
        border-top: @gray-lighter solid 1px;
        text-align: right;
    }

</style>

<!-- contents -->

<div class="contents-container">
    <div class="bredcrumb">Dashboard / Human Resource / Job Advertisement </div> <!-- Bread Crumbs -->
    <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
    <div class="right-contents">
        <div class="head">Available Applicants</div>

        <!-- filter -->
        <div class="filter" style="float:left;">
            <h4>Filter By</h4>
            <?php echo form_open();?>
            <input type="text" placeholder="Search Candidate Here" id="dataTableFilter">
            <!-- Job Filter-->
            <!--<input type="hidden" name="selectAvailableJob" id="selectAvailableJob">-->
            <input type="hidden" name="selectApplicantType" id="selectApplicantType">
            <div class="btns_row" style="">
            <input type="button" name="" id="FilterResetButton" class="btn" value="Reset">
            </div>
            <?php echo form_close();?>
        </div>

        <div class="clear"> </div>
        <!-- table -->
        <div class="btns_row" style="display: none" id="interview">
            <input type="button" name="" id="InterviewButton" class="btn green" data-toggle="modal" style="background-color: #008000" value="Call For Interview">
        </div>

        <table cellpadding="0" cellspacing="0" id="reportedBreachesTable" class="marge ">
            <div class="table-width">
                <thead class="table-head">
                <td>advertisementID</td>
                <td>ApplicantID</td>
                <td>Select</td>
                <td>Full Name</td>
                <td>Contact no</td>
                <td>Candidate</td>
                <td>Applied Job</td>
                <td>Email</td>
                <td>Date Applied</td>
                <td width="140"> Resume/CV </td>
                <td width="100">Details</td>
                </thead>
            </div>
            <tbody>
            </tbody>
        </table>

        <!-- button group -->

    </div>
</div>
<!-- contents -->
</div>

        </div>

<!-- Gender dialog -->
<style>

</style>
<!-- Modal -->
<!----------------------------Send Email Without Bootstrap libraries----------------------------->
<div id="sendEmail" class="modal-demo call_email" style="display: none;">
    <div class="top_header">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only"></span>
        </button>
        <h3 class="title">Email To</h3>
    </div>
    <form class="form-horizontal" id="InterviewCall">

        <div class="pop_row">
            <div class="left_side">
                <h5>To</h5>
            </div>
            <div class="right_side">
                <textarea  name="ToEmail" id="ToEmail" readonly>
                </textarea>
                <input type="hidden" class="right_txt" name="ApplicantID" id="ApplicantID" style="width:240px;height:35px;">
                <input type="hidden" class="right_txt" name="AdvertID" id="AdvertID" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row">
            <div class="left_side">
                <h5>Reply To</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" name="FromEmail" placeholder="Reply To Email (optional)" style="width:240px;height:35px;">
            </div>
        </div>


        <div class="pop_row">
            <div class="left_side">
                <h5>Interview Date</h5>
            </div>
            <div class="right_side">
                <input type="text" class="right_txt" placeholder="Date" name="InterviewDate" id="date" style="width:240px;height:35px;">
            </div>
        </div>

        <div class="pop_row_txtarea">
            <div class="left_side">
                <h5>Message</h5>
            </div>
            <div class="right_side">
                <textarea  class="right_txt_area" name="MessageBody" id=""></textarea>
            </div>
        </div>


        <div class="btn-row">
            <input type="button" class="btn-pop" id="sendMailForInterview" value="Send">
        </div>
    </form>

</div>

<!----Modal For edit --->


<!----End--->
<style>
    .modal-footer{float: right;
        width: auto;
        height: auto;
        margin-right: 20px;
        margin-top: 5px;
        margin-bottom: 10px;}
</style>
<!---->

<!-----------------------------------------end models----------------------------->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/recruitment_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design, #project').select2();
    });
</script>


<!--------------------------Data Tables--------------------------->
<script type="text/javascript">

var oTable;
$(document).ready(function(e){

    // Function for Select All Shortlisted //

    //End ///
    ///////////////////////////Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    /*
     * New Select Employee
     * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
     * */

    //Filter Selectors

    var jobFilterSelector =  $('#selectAvailableJob');
    var url = "<?php echo base_url(); ?>recruitment/load_all_available_advertisedJobs/";
    var id = "AdvertisedJobID";
    var text = "JobTitle";
    var minInputLength = 0;
    var placeholder = "Select Job";
    var multiple = false;
    commonSelect2(jobFilterSelector,url,id,text,minInputLength,placeholder,multiple);

    //Applicant Status Type
    var applicantStatusFilter =  $('#selectApplicantType');
    var url = "<?php echo base_url(); ?>recruitment/load_all_available_applicant_status/";
    var id = "ID";
    var text = "TEXT";
    var minInputLength = 0;
    var placeholder = "Select Applicant Status";
    var multiple = false;
    commonSelect2(applicantStatusFilter,url,id,text,minInputLength,placeholder,multiple);
    //As This Field Is Dependent Over the First Filter..
   /* if(jobFilterSelector.val().length === 0){
        applicantStatusFilter.select2('disable');
    }else{
        applicantStatusFilter.select2('enable');
    }*/
//Need To Display the list of All The Reported/Violated Disciplines
    oTable ='';
    var tableSelector = $('#reportedBreachesTable');
    var url_DT = "<?php echo base_url(); ?>human_resource/load_available_Advert_applicants/<?php echo $this->uri->segment(3);?>";
    var aoColumns_DT = [
        /* ID */ {
            "mData": "AdvertisementID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },{
            "mData": "applicantID",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
       /*Job Title */ {
            "mData": "CheckBoxShortList",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },/*Job Title */ {
            "mData" : "Name"
        },
        /* DepTitle */ {
            "mData" : "contactNo"
        },
        /* availPositions */ {
            "mData" : "ApplicantStatus",
            "bVisible": false,
            "bSortable": false,
            "bSearchable": false
        },
        {
            "mData" : "Job_Title"
        },{
            "mData" : "email"
        },
        /* Shows The Status Of salary Range */
        {
            "mData" : "AppliedDate"
        },
        /* Shows The Status Of The datePosted */
        {
            "mData" : "attachment"
        },
        {
            "mData" : "ViewApplicantDetails"
        }

    ];
    var HiddenColumnID_DT = 'applicantID';
    var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
    commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
    //End Of DataTables

    jobFilterSelector.on('change',function(e){
        //Applicant Job Wise Filter..
        oTable.fnDestroy();
        var filterCandidates = $(this).val();
        var filters = 'aoData.push({"name":"filterApplicantJob","value":'+filterCandidates+'});';
        var filteredStatus_aoColumns_DT = [
            /* ID */
            {
                "mData": "AdvertisementID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },{
                "mData": "applicantID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /*Check Box */ {
                "mData" : "CheckBoxShortList",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },/*Job Title */ {
                "mData" : "Name"
            },
            /* Contact No */ {
                "mData" : "contactNo"
            },
            /* Applicant Status */ {
                "mData" : "ApplicantStatus"
            },
            {
                "mData" : "Job_Title"
            },{
                "mData" : "email"
            },
            /* Date Applicant Applied On */
            {
                "mData" : "AppliedDate"
            },
            /* Applicant Resume Attachment Should Should Here */
            {
                "mData" : "attachment"
            },
            {
                "mData" : "ViewApplicantDetails"
            }

        ];
        commonDataTablesFiltered(tableSelector,url_DT,filteredStatus_aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
        applicantStatusFilter.select2('enable');
    });

    applicantStatusFilter.on('change',function(e){
        //Applicant Status Wise Filter
        oTable.fnDestroy();
        var filteredApplicantStatus = $(this).val();
        var filters = 'aoData.push({"name":"filterApplicantStatus","value":'+filteredApplicantStatus+'}); aoData.push({"name":"filterApplicantJob","value":'+$('#selectAvailableJob').val()+'});';
        var filteredStatus_aoColumns_DT = [
            /* ID */ {
                "mData": "AdvertisementID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },{
                "mData": "applicantID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },/*Check Box */ {
                "mData" : "CheckBoxShortList",
                "bSortable": false
            },
            /*Job Title */ {
                "mData" : "Name"
            },
            /* Contact No */ {
                "mData" : "contactNo"
            },
            /* Applicant Status */ {
                "mData" : "ApplicantStatus"
            },
            {
                "mData" : "Job_Title"
            },{
                "mData" : "email"
            },
            /* Date Applicant Applied On */
            {
                "mData" : "AppliedDate"
            },
            /* Applicant Resume Attachment Should Should Here */
            {
                "mData" : "attachment"
            },
            {
                "mData" : "ViewApplicantDetails"
            }

        ];
        commonDataTablesFiltered(tableSelector,url_DT,filteredStatus_aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });

    //Filter the SearchBox Of Table.
    $('#dataTableFilter').on('keyup',function(e){
        oTable.fnFilter($(this).val());
    });

    ////Code For Filter
    $('#candidateFilter').on('change', function (e) {
        oTable.fnDestroy();
        var filterCandidates = $('#candidateFilter').val();
        var filters = 'aoData.push({"name":"filterCandidates","value":'+filterCandidates+'});';
        var filteredStatus_aoColumns_DT = [
            /* ID */ {
                "mData": "AdvertisementID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },{
                "mData": "applicantID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /*Job Title */ {
                "mData" : "Name"
            },
            /* Contact No */ {
                "mData" : "contactNo"
            },
            /* Applicant Status */ {
                "mData" : "ApplicantStatus"
            },
            {
                "mData" : "Job_Title"
            },{
                "mData" : "email"
            },
            /* Date Applicant Applied On */
            {
                "mData" : "AppliedDate"
            },
            /* Applicant Resume Attachment Should Should Here */
            {
                "mData" : "attachment"
            },
            {
                "mData" : "ViewApplicantDetails"
            }

        ];
        commonDataTablesFiltered(tableSelector,url_DT,filteredStatus_aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
    });


    //Defining Width For All Selectors Of the Filters
    $('.select2-container').css("width","223px");
    $('tbody').addClass("table-row");


    //Filters Reset Button
    $('#FilterResetButton').on('click', function (e) {
        applicantStatusFilter.select2('disable');
        jobFilterSelector.select2('val', '');
        applicantStatusFilter.select2('val', '');
        oTable.fnDestroy();
        commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
    });
});
    $(document).on('click','.AdvertViewDetails', function(e){
        var ApplicantID = $(this).closest('tr').attr('data-id');
        /*var postData = {
            ApplicantIDs: ApplicantID
        };*/
        window.location ="<?php echo base_url(); ?>human_resource/applicantDetails/"+ApplicantID;
            /*    $.ajax({
            url:"<?php //echo base_url(); ?>human_resource/applicantDetails/list",
            type: "POST",
            data: postData,
            success:function(output){}
    });*/
    });
    $(document).on('click','#checkbox_select', function(e){
        var ApplicantID = $(this).closest('tr').attr('data-id');
        if( $(".checkbox_select").is(':checked')) {
            var id = $("#checkbox_select").val();
            if (id.length > 0) {
                $("#interview").show();
            }


        } else {
            $("#interview").hide();
        }

        console.log(ApplicantID);

    });

    $(document).on('click','#interview', function(e){
        var ApplicantID = $("#checkbox_select").closest('tr').attr('data-id');
        var advertiseID= $("#AdvertisementID").val();
        var ids= $('input[type=checkbox]:checked').map(function(_, el) {
            return $(el).val();
        }).get();


    $.ajax({
        url: "<?php echo base_url()?>human_resource/ShorlistedApplicantEmail",
        data:{ids:ids},
        type:"POST",
        success: function (output) {
            var json=JSON.parse(output);
            //console.log(json);
         $("#ToEmail").val(json.Email);
         $("#ApplicantID").val(json.ApplicantID);
         $("#AdvertID").val(advertiseID);
        }
    });
    });
    $('#sendMailForInterview').on('click', function (e) {
        var formData = $('#InterviewCall').serializeArray();
        //console.log(formData); return;
        var targetURL = '<?php echo base_url();?>human_resource/SendInterviewCall';
        $.ajax({
            url: targetURL,
            data:formData,
            type:"POST",
            success: function (output) {
                var data = output.split("::");
                if(data[0] === "OK"){
                    Parexons.notification(data[1],data[2]);
                }else if(data[0] === "FAIL"){
                    Parexons.notification(data[1],data[2]);
                }
            }
        });
    });
   // <!-----------------------bton script------------------->

    $('#InterviewButton').on('click', function(e){
        Custombox.open({
            target: '#sendEmail',
            effect: 'fadein'
        });
        e.preventDefault();
    });
    $( "#date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true


    });
</script>



