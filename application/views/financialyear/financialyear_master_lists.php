
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource /Master List/ Financial Year</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_master_list_nav'); ?>

	<div class="right-contents" style="width:70%">

		<div class="head">Financial Year</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
			<form action="hr_site/FinancialYear" method="post">

				<!--<div class="row">
					<h4><b>Appraisal</b></h4>
				</div>-->
				<br class="clear">

					<table>
					<!--<td><h4>Financial Year Title</h4></td>
					<td><input type="text" name="FinancialYear" value="" required></td>-->


					<tr>
						<td><h4>From Year</h4></td>
						<td><?php echo form_dropdown('FromYearID', $FinancialYear,'','required="required"');?></td>

						<td><h4>To Year</h4></td>
						<td><?php echo form_dropdown('ToYearID', $FinancialYear,'','required="required"');?></td>

					</tr>
					<tr>

						<td><h4>From Month</h4></td>
						<td><?php echo form_dropdown('FromMonthID', $FinancialMonth,'','required="required"');?></td>

						<td><h4>To Month</h4></td>
						<td><?php echo form_dropdown('ToMonthID', $FinancialMonth,'','required="required"');?></td>
				</tr>


				</table>
				<!-- button group -->
				<div class="row">
					<input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
				</div>

			</form>


			<!-- table -->


			<table cellspacing="0">
				<thead class="table-head">

				<td>Financial Year</td>
				<td>From Year</td>
				<td>To Year</td>
				<td>From Month</td>
				<td>To Month</td>
<!--				<td>Status</td>-->
				<td><span class="fa fa-pencil"></span></td>
<!--				<td><span class="fa fa-trash-o"></span></td>-->
				</thead>
				<?php if (!empty($FYDATA)){?>
					<?php foreach($FYDATA as $FinancailYear):?>
						<tr class="table-row">

							<td><?php echo $FinancailYear->FinancialYear; ?></td>
							<td><?php echo $FinancailYear->YearFrom; ?></td>
							<td><?php echo $FinancailYear->YearTo; ?></td>
							<td><?php echo $FinancailYear->MonthFrom; ?></td>
							<td><?php echo $FinancailYear->MonthTo; ?></td>

							<td><a href="hr_site/UpdateFinancialYear/<?php echo $FinancailYear->MainID;?>"><span class="fa fa-pencil"></span></a></td>
<!--							<td><a href="hr_site/DeleteAppraisal/--><?php //echo $AppraisalTitle->AppraisalId;?><!--" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>-->
						</tr>

					<?php endforeach; ?>
				<?php } else { echo "<div class=\"result\">No record found..</div>";} ?>

			</table>

		</div>



	</div>

</div>
<!-- contents -->

<script>
	$("#alert").delay(3000).fadeOut('slow');

	<?php if(!empty($this->session->flashdata('msg'))){?>
	var message = "<?php echo $this->session->flashdata('msg')?>";
	setInterval(function(){
		var data = message.split("::");
		Parexons.notification(data[0],data[1]);
	});

	<?php }?>

	$(document).ready(function(e){


	});

</script>
