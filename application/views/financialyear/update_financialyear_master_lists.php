
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource /Master List/ Financial Year</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/hr_master_list_nav'); ?>

	<div class="right-contents" style="width:70%">

		<div class="head">Financial Year</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
			<form action="hr_site/UpdateFinancialYear" method="post">

				<!--<div class="row">
					<h4><b>Appraisal</b></h4>
				</div>-->
				<br class="clear">

			<table>
				<!--<tr>
					<td><h4>Financial Year Title</h4></td>
					<td><input type="text" name="FinancialYear" value="<?php /*echo $FYDATA->FinancialYear*/?>" required></td>
				</tr>-->
				<tr>
					<td><h4>From Year</h4></td>
					<td><?php echo form_dropdown('FromYearID', @$FinancialYear,$FYDATA->FYearID,'required="required"');?></td>

					<td><h4>To Year</h4></td>
					<td><?php echo form_dropdown('ToYearID', @$FinancialYear, $FYDATA->ToYearID,'required="required"');?></td>
				</tr>
				<tr>
					<td><h4>From Month</h4></td>
					<td><?php echo form_dropdown('FromMonthID', @$FinancialMonth, $FYDATA->FromMonthID,'required="required"');?></td>

					<td><h4>To Month</h4></td>
					<td><?php echo form_dropdown('ToMonthID', @$FinancialMonth, $FYDATA->ToMonthID,'required="required"');?></td>
				</tr>
			</table>
				<!-- button group -->
				<div class="row">
					<input type="hidden" name="MainID" value="<?php echo @$FYDATA->MainID?>">
					<input type="submit" name="update" value="Update" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
				</div>

			</form>

</div>



	</div>

</div>
<!-- contents -->

<script>
	$("#alert").delay(3000).fadeOut('slow');

	<?php if(!empty($this->session->flashdata('msg'))){?>
	var message = "<?php echo $this->session->flashdata('msg')?>";
	setInterval(function(){
		var data = message.split("::");
		Parexons.notification(data[0],data[1]);
	});

	<?php }?>

	$(document).ready(function(e){


	});

</script>
