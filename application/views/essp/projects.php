    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
 						</div>
 						<div class="col-lg-7">
 							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
 					<!--<form class="form-inline" role="form">
					  <div class="form-group col-xs-4">
					    <div class="input-group">
					      <div class="input-group-addon">Filter :</div>
					      <select name="" id="" class="form-control">
					      	<option value="">Select Project</option>
					      	<option value=""></option>
					      </select>
					    </div>
					  </div>
					  <div class="form-group col-xs-2">
 						  <select name="" id="" class="form-control">
					      	<option value="">Select Task</option>
					      	<option value=""></option>
					      </select>					    
					  </div>
					  <div class="form-group col-xs-2">
 						<select name="" id="" class="form-control">
					      	<option value="">Select Status</option>
					      	<option value=""></option>
					      </select>					   
					  </div>
					  <div class="form-group col-lg-1">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-default" style="background:#049ad9; color:#fff;"><span class="fa fa-search"> </span> Search</button>
					    </div>
					  </div>
					</form>-->
 				</div>
 				</div>
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Tasks / Projects</div>
					  <div class="panel-body">
					  	<div class="table-responsive">
						<table class="table table-striped">
						 		<tr>
						 		<th>Project Name</th>
						 		<th>Task</th>
						 		<th>Start Date</th>
						 		<th>End Date</th>
						 		<th>Status</th>
						 	</tr>
                            <?php if(!empty($task_history)){
                                foreach($task_history as $task){?>
                                    <tr>
                                        <?php /*?><td><?php echo $task->employee_code;?></td>
                	<td><?php echo $task->full_name;?></td>
                	<td><?php echo $task->designation_name;?></td><?php */?>
                                        <td><?php echo $task->project_title;?></td>
                                        <td><?php echo $task->task_name;?></td>
                                        <td><?php echo $task->start_date;?></td>
                                        <td><?php echo $task->completion_date?></td>
                                        <td><?php echo $task->status;?></td>
                                        <!--<td><?php //if(!empty($task->app_emp_name)){echo $task->app_emp_name;}else{echo "Not Approved";}?></td>
                	<td><?php //echo $task->status;?></td>
                	<td><?php //echo $task->status_title;?></td>-->
                                    </tr>
                                <?php } }else{ ?>
                                <tr><td colspan="5"><?php echo "No Record Found ... !";?></td></tr>
                            <?php } ?>
						 	<!--<tr>
						 		<td>Web Development</td>
						 		<td>Development</td>
						 		<td>16-Oct-2014</td>
						 		<td>19-Dec-2014</td>
						 		<td>
						 		<div class="progress">
								  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								    <span class="sr-only">60% Complete (success)</span>
								  </div>
								</div>
								</td>
						 	</tr>-->
						</table>
						</div>
					  </div>
					</div>
				
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
  