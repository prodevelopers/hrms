    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left_side">
 				<div class="picture">
 						<div class="col-lg-5">
                            <?php
                            $employeeAvatar = empAvatarExist($personal_info->thumbnail);
                            ?>
							<img src="<?php echo ($employeeAvatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$personal_info->thumbnail : $employeeAvatar ?>"   width="100" height="100" class="img-responsive">
 						</div>
 						<div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 right-side">
 				<div class="col-lg-8 col-md-7 col-xs-12 col-sm-7">
					<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Projects Progress</div>
					  <div class="panel-body">
					  		<!--<div id="canvas-holder" style="width:60%; margin:0 auto;">
								<canvas id="chart-area" width="50%" height="50%"/>
							</div>-->

                          <!--------------------BAR CHART-------------------------------------------->
                          <div style="width: 100%">
                              <canvas id="canvas" height="30%" width="40%"></canvas>
                          </div>

                          <!--------------------BAR CHART END-------------------------------------------->

					  </div>
					</div>

		
 				</div>
 				<div class="col-lg-4 col-md-5 col-xs-12 col-sm-5">
 				<div class="row">
 					<div class="col-lg-12 collapse-top">
	 					<div class="row">
	 					<div class="input-group">
	 						
	 					</div>
	 					<button type="button"  style="background:#049ad9; text-align:left;" class="btn btn-primary col-lg-12 col-md-12 col-xs-12" data-toggle="collapse" data-target="#demo" aria-expanded="true" aria-controls="demo">
						  Notifications <span class="badge" style="margin-left:2em;"><?php if(!empty($leave_notify)){echo count($leave_notify);}?></span><span class="badge pull-right"><i class="fa fa-angle-down"></i></span>
						</button>
	 					</div>
						<div class="row">
							<div id="demo" class="collapse in text-justify doi col-md-5">
									<div class="notification">
                                        <?php if(!empty($leave_notify)){

                                            foreach($leave_notify as $notify){

                                                if($notify->notification_detail_link == "Expense Claim"){?>
                                            <h5>Expense Claim
                                                <span class="pull-right"><a href="<?php echo base_url();?>essp/expense_claim" style="background:#4D6684;" class="btn btn-xs btn-primary">View Details</a></span>
                                            </h5>
                                            <?php break; }}?>
                                       <?php $i=0; foreach($leave_notify as $notify){
                                                $i++;
                                                if($notify->notification_detail_link == "Expense Claim"){?>
                                        <div style="color: #008000; background-color: #d3d3d3">
                                        <p style="color: #008000;"><?php echo $notify->notification_text;?></p>
										<p style="color: #008000"><?php echo date_format_helper($notify->created_date);?></p>
                                        </div>
                                            <?php  if($i == 5) break; }}}?>
                                        <hr/>
									</div>
									<div class="notification">

                                        <?php if(!empty($leave_notify)){
                                            foreach($leave_notify as $notify){ if($notify->notification_detail_link == "Leave Application"){?>
                                            <h5>Leave Application
                                                <span class="pull-right"><a href="<?php echo base_url();?>essp/apply_leave" style="background:#4D6684;" class="btn btn-xs btn-primary">View Details</a></span>
                                            </h5>
                                            <?php break; }}?>
                                           <?php $j=0; foreach($leave_notify as $notify){
                                                $j++;
                                                if($notify->notification_detail_link == "Leave Application"){?>
                                        <div style="color: #008000; background-color: #d3d3d3">
                                                <p style="color: #008000"><?php echo $notify->notification_text;?></p>
                                                <p style="color: #008000"><?php echo date_format_helper($notify->created_date);?></p>
                                           </div>
                                            <?php if($j == 5) break; }}}?>
									</div>
									<div class="notification">

                                        <?php if(!empty($leave_notify)){
                                            foreach($leave_notify as $notify){ if($notify->notification_detail_link == "Advance Request"){?>
                                                <h5>Advance Request
                                                    <span class="pull-right"><a href="<?php echo base_url();?>essp/advance_claim" style="background:#4D6684;" class="btn btn-xs btn-primary">View Details</a></span>
                                                </h5>
                                                <?php break; }}?>
                                                <?php $k=0; foreach($leave_notify as $notify){
                                                $k++;
                                                if($notify->notification_detail_link == "Advance Request"){?>
                                              <div style="color: #008000; background-color: #d3d3d3">
                                                    <p style="color: #008000"><?php echo $notify->notification_text;?></p>
                                                    <p style="color: #008000"><?php echo date_format_helper($notify->created_date);?></p>
                                               </div>
                                                <?php if($k == 5) break; }}}?>
									</div>


							</div>
						</div>
	 				</div>
 				</div>
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
		<script src="<?php echo base_url();?>assets/essp/js/Chart.js"></script>
		<script>
		var polarData = [
				{
					value: 100,
					color:"#F7464A",
					highlight: "#FF5A5E",
					label: "Web Designing (Completed)"
				},
				{
					value: 95,
					color: "#46BFBD",
					highlight: "#5AD3D1",
					label: "Web Development (Progress...)"
				},
				{
					value: 100,
					color: "#FDB45C",
					highlight: "#FFC870",
					label: "Photoshop (Progress...)"
				},
				{
					value: 99,
					color: "#949FB1",
					highlight: "#A8B3C5",
					label: "Dreamweaver (Progress...)"
				},
				{
					value: 100,
					color: "#4D5360",
					highlight: "#616774",
					label: "Dreamweaver (Completed)"
				}

			];

			window.onload = function(){
				var ctx = document.getElementById("chart-area").getContext("2d");
				window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
					responsive:true
				});
			};
		</script>


        <!-----------------------Script for Bar Chart------------------------------------------>
        <script>
            var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

            var barChartData = {
                labels : [<?php if(!empty($progress)){
        foreach($progress as $project_progress){
        echo '"'.$project_progress->project_title.'",';
        }}?>],
                datasets : [
                    {
                        fillColor : "rgba(220,220,220,0.5)",
                        strokeColor : "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data : [<?php if(!empty($progress)){
        foreach($progress as $project_progress){
        echo $project_progress->AvgProgress.',';
        }}?>]
                    },

                ]

            };
            window.onload = function(){
                var ctx = document.getElementById("canvas").getContext("2d");
                window.myBar = new Chart(ctx).Bar(barChartData, {
                    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> %",
                    responsive : true
                });
            }


        </script>

        <!-------------End of bar Chart------------------->
        <script src="<?php echo base_url();?>assets/chart-plugin/Chart.js"></script>
  
