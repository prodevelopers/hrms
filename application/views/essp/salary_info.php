    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
 						</div>
                    <div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
            <?php echo form_open(); ?>
 				<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
 					
					  <div class="form-group col-xs-4">
					    <div class="input-group">
                        
					      <div class="input-group-addon">Filter :</div>
					 <?php
                     echo form_dropdown('month',$months,$selc_m,"id='month' class='form-control' onchange='this.form.submit()'")?>
           	   
					    </div>
					  </div>
					  <div class="form-group col-xs-2">
 						<?php 
						echo form_dropdown('year',$years,$selc_y,"id='month' class='form-control' onchange='this.form.submit()'")?>				    
					  </div>
                     
					  <div class="form-group col-xs-2">
 						<!--<select name="" id="" class="form-control">
					      	<option value="">Select Status</option>
					      	<option value=""></option>
					      </select>	-->				   
					  </div>
					  <div class="form-group col-lg-1">
                      
					    <div class="col-sm-offset-2 col-sm-10">
					      <!--<button type="submit" class="btn btn-default" style="background:#049ad9; color:#fff;"><span class="fa fa-search"> </span> Search</button>-->
					    </div>
					  </div>
                       <?php echo form_close(); ?>
					<!--</form>-->
 				</div>
 				</div>
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Salary Information</div>
					  <div class="panel-body">
					  	<div class="table-responsive">
						<table class="table table-striped">
						 	<tr>
						 		<th>Month / Year</th>
						 		<th>Base Salary</th>
						 		<th>Paid Salary</th>
								<th>Transaction Date</th>
						 		<th>Print Pay Slip</th>
						 	</tr>
                            <?php if(!empty($info)){?>
                            <?php foreach($info as $salary_rec){?>
						 	<tr>
						 		<td><?php echo $salary_rec->month." / ".$salary_rec->year;?></td>
						 		<td><?php echo $salary_rec->base_salary;?></td>
					<td><?php echo $salary_rec->transaction_amount;?></td>
								<td> <?php
									$trans_date=$salary_rec->transaction_date;
									$date_format=date("d-m-Y",strtotime($trans_date));
									echo $date_format;?></td>
						 		<td><a href="slip/<?php echo $salary_rec->employee_id;?>/<?php echo $salary_rec->salary_month;?>//<?php echo $salary_rec->salary_year;?>" class="btn btn-default btn-xs"><span class="fa fa-print	"> </span> Pay Slip</a></td>
						 	</tr>
							<?php }}else {echo'<tr>'. "<span style='color:red'>sorry no record found !</span>".'</tr>';}?>
						 	
						</table>
						</div>
					  </div>
					</div>
				
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
  