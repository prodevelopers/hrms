    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
						</div>
 						<div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
            <?php echo form_open();?>
 				<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
                    
 					
					  <div class="form-group col-xs-4">
					    <div class="input-group">
					      <div class="input-group-addon">Filter :</div>
					      <input type="text" name="full_name" class="form-control" placeholder="Search Here" required> 
				
					    </div>			
                        
					  </div> 
                     <div class="form-group col-xs-3">
                  <div class="input-group">
					
                     <?php echo @form_dropdown('year',$year,'',"class='form-control' required");?>
                    
                     </div>
                     </div>
                 <div class="form-group col-xs-3">
                  <div class="input-group">
                     <?php echo @form_dropdown('month',$month,'',"class='form-control' required");?>
                 
				</div>
                </div>
                  <div class="button-group">
					<button class="btn green" style="background:#049ad9; color:#fff;">Search</button>
					<!--<button class="btn gray">Reset</button>-->
				</div>
                     
                     
					<?php echo form_close(); ?>
 				</div>
 				</div>
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Timesheet</div>
					  <div class="panel-body">
					  	<div class="table-responsive">
						<table cellspacing="0" class="table">
				<tr>
					<td>Project Name</td>
					<td>Activity Name</td>
                 <!--   <td>Name</td>-->
					<td>In Date</td>
                    <td>In Time</td>
					<td>Out Date</td>
                    <td>Out Time</td>
                    <!--<td>month</td>
                    <td>year</td>-->
					<td>Total Hours</td>
                    <td>Status</td>
				</tr>
			       <?php if(!empty($time_sheet)){
					         $total = 0;
					foreach($time_sheet as $time_sheets){
						$count=count($time_sheet);
					$timestart = strtotime($time_sheets->in_time);
					$timestop = strtotime($time_sheets->out_time); 
					$time_diff = $timestart - $timestop ; 
					$show = strtotime($time_diff);
					$time_m=date('m',$show);
					$time=date('h',$show);
					$final=$time.":".$time_m;
					if ($final < 0) {
					  $final += 24;
					}
			
				?>
                    <tbody>
                    <td><?php echo $time_sheets->project_title;?></td>
                    <!--<td><span id="emp_name"><?php // echo //$time_sheet->full_name;?></span></td>-->
                    <td><?php echo $time_sheets->task_name?></td>
                    <td><?php echo $time_sheets->in_date?></td>
                    <td><?php echo $time_sheets->in_time?></td>
                    <td><?php echo $time_sheets->Out_date?></td>
                    <td><?php echo $time_sheets->out_time?></td>
                   <!-- <td><?php //echo $time_sheet->in_month?></td>
                    <td><?php //echo $time_sheet->in_year?></td>-->
                    <td><?php echo @gmdate("h:i:s", $final);?></td>
                    <?php  $total +=  @gmdate("h:i:s", $final);?>
                     <td><?php echo $time_sheets->status_type;?></td>
                   
                    
                    <input type="hidden" name="name" id="emp_name" value="<?php echo $time_sheets->in_date; ?>" />
					
                    </tbody>
                    <?php $sum = $total;}}else{?>
                    <td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
                    <?php }?>
                    <tr >
                	<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                
                    <td style="background:#333; color:#FFF;"><span style="float:right; font-weight:bold;">Total Hours:</span></td>
                	<td style="background:#333; color:#FFF;"><?php echo @$sum; ?></td>
                    </tr>
                    
			</table>
						</div>
					  </div>
					</div>
				
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
  