    <?php  $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                   <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
 						<div class="col-lg-5">
                            <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
						</div>
 						<div class="col-lg-7">
							<p><?php if(!empty($design->designation_name)){ echo $personal_info->full_name;}?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;}else{echo "";}?></span></p>
						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php  $this->load->view('essp/includes/leftmenu.php');?>
 			</div>
 			<?php //print_r($leaves);?>
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
				<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;"><?php echo  $this->session->flashdata('alert');?></div>
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Apply For Leave</div>
					  <div class="panel-body">
                      
					  	<form action="<?php echo base_url();?>essp/add_apply_for_leave" method="post" class="form-horizontal" role="form">
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Employee Name</label>
						    <div class="col-sm-4">
						      <input type="text" class="form-control" id="inputEmail3" name="full_name" value="<?php echo @$employee->full_name;?>" readonly="readonly">
						    </div>
						  </div>

						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Leave Type</label>
						    <div class="col-sm-4">
                            <?php echo form_dropdown('leave_type', $leave_type,'',"class='form-control' required")?>
						     	
						    </div>
						  </div>
						   <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">From</label>
						    <div class="col-sm-4">
						      <input type="text" name="from_date" class="form-control" id="applyForLeaveFromDate" placeholder="From Date" onchange="mydata()">
						    </div>
						  </div>
						   <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">To</label>
						    <div class="col-sm-4">
						      <input type="text"  name="to_date" class="form-control" id="applyForLeaveToDate" placeholder="To Date" onchange="mydata()">
						    </div>
						  </div>
                            <br class="clear">
                            <div class="form-group" id="half_d" style="display: none">
                                <label for="half_day" class="col-sm-3 control-label">Half Day</label>
                                <input type="radio" id="half_day"  name="half_day">
                            </div>
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Total Days</label>
						    <div class="col-sm-4">
						      <input type="text" class="form-control" name="total_days" id="tdays" placeholder="Total Days">
						    </div>
						  </div>	
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
						    <div class="col-sm-4">
						     	<textarea name="note" id="" cols="30" rows="05" class="form-control"></textarea>
						    </div>
						  </div>		
						  <div class="form-group">
						    <div class="col-sm-offset-3 col-sm-4">
						      <!--<button type="submit" class="btn" style="background:#049ad9; color:#fff;">Apply</button>-->
                              <input type="hidden" name="emp_id" value="<?php echo $this->session->userdata('employee_id') ?>" />
                              <input type="submit" class="btn" style="background:#049ad9; color:#fff;" value="Submit" name="add" />
						    </div>
						  </div>
						</form>
					  </div>

					</div>
                <div class="panel panel-default" style="margin-top:1em;">
                    <div class="panel-heading" style="background:#049ad9; color:#fff;">Leave Application Records</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Leave Type</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Total Days</th>
                                    <th>Status</th>
                                    <th>Application Date</th>
                                    <!--<th>Edit</th>
                                    <th>View</th>-->
                                </tr>
                                <?php if(!empty($leave_attend)){
                                    foreach($leave_attend as $leave){
                                        ?>
                                        <tbody>
                                        <tr class="table-row">
                                            <td><?php echo $leave->employee_code?></td>
                                            <td><?php echo $leave->full_name?></td>
                                            <td><?php echo $leave->leave_type?></td>

                                            <td><?php echo date_format_helper($leave->from_date);?></td>
                                            <td><?php echo date_format_helper($leave->to_date);?></td>
                                             <td><?php echo $leave->total_days;?></td>
                                            <td><?php echo $leave->status_title?></td>
                                            <td><?php echo date_format_helper($leave->application_date);?></td>
                                        <!--    <td align="center"><?php /*if($leave->status_title=="Approved"){*/?><span class="fa fa-pencil" aria-disabled="true" title="Already Approved "></span><?php /*} elseif($leave->status_title=="Declined"){*/?><span class="fa fa-pencil" aria-disabled="true" title="You are Not allowed to Edit...Application is Declined "></span><?php /*}else{*/?>
                                                <a href="leave_attendance/edit_leave_application/<?php /*echo $leave->application_id;*/?>/<?php /*echo $leave->employee_id;*/?>/<?php /*echo $leave->leave_approval_id;*/?>"><span class="fa fa-pencil"></span></a><?php /*}*/?></td>
                                            <td align="center">
                                                <a href="leave_attendance/view_employee_leave/<?php /*echo $leave->application_id;*/?>"><span class="fa fa-eye"></span></a></td>
                                        </tr>-->
                                        </tbody>
                                    <?php }}else{?>
                                    <td colspan="8"><?php echo "<p style='color:red'>Sorry No Data Available</p>"?></td>
                                <?php }?>
                            </table>
                            <div>
                                <ul>
                                    <?php echo $links;?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

				</div>

 	</div>
 	</div>
 	<?php  $this->load->view('essp/includes/footer');?>

	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

    <script>
        function mydata(){
            $("#half_d").hide();
            var fromDate=$("#applyForLeaveFromDate").val();
            var toDate=$("#applyForLeaveToDate").val();
            if(fromDate.length === 0 || toDate.length === 0){
                return;
            }
            var fromDateArray = fromDate.split('-');
            var toDateArray = toDate.split('-');
            var diff = new Date(Date.parse(toDateArray[2]+'-'+toDateArray[1]+'-'+toDateArray[0]) - Date.parse(fromDateArray[2]+'-'+fromDateArray[1]+'-'+fromDateArray[0]));
            var totalDays = diff / 1000 / 60 / 60 / 24;
            //Assign The Total Days to The TextField.
            $("#tdays").val(totalDays+1);
            var d=totalDays+1;
            var da=parseInt(d);

            if(da == 1){
                $("#half_d").show();
            }
            if($("#tdays").val() == 1){
                $('#half_day').prop('checked', false);
            }

        }
        $("#half_day").on('click',function(){


            if($('#half_day').is(':checked'))
            {

                $("#tdays").html('').val(0.5);
            }

        });
    </script>

	<script>
		$("#alert").delay(3000).fadeOut('slow');
		$( "#applyForLeaveFromDate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
			changeMonth: true,
			changeYear: true,
			onClose: function( selectedDate ) {
				$( "#applyForLeaveToDate" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$( "#applyForLeaveToDate" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
			changeMonth: true,
			changeYear: true,
			onClose: function( selectedDate ) {
				$( "#applyForLeaveFromDate" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
        $(function(){
            $("#short, #full").change(function(){
                $("#applyForLeaveFromTime, #applyForLeaveToTime, #applyForLeaveFromDate, #applyForLeaveToDate,#tdays").val("").attr("disabled",true);
                if($("#short").is(":checked")){
                    $("#applyForLeaveFromTime").removeAttr("disabled");
                    $("#applyForLeaveToTime").removeAttr("disabled");
                }
                else if($("#full").is(":checked")){
                    $("#applyForLeaveFromDate").removeAttr("disabled");
                    $("#applyForLeaveToDate").removeAttr("disabled");
                    $("#tdays").removeAttr("disabled");
                }
            });
        });
	</script>
