    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
						</div>
 						<div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;}else{echo "";}?></span></p>
						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Advance Request</div>
					<div id="flash" ><?php echo $this->session->flashdata('message');?></div>
					  <div class="panel-body">
					  	<form action="loan_advance_pay" method="post" class="form-horizontal" role="form">
						  <div class="form-group">
		 <label for="inputEmail3" class="col-sm-3 control-label">Employee Name</label>
						    <div class="col-sm-4">
		 <input type="input" name="name" class="form-control" id="inputEmail3" placeholder="Employee Name" value="<?php echo $employee->full_name;?>" readonly="readonly">
         <input type="hidden" name="emp_id" id="inputEmail3"value="<?php echo $employee->employee_id;?>" readonly="readonly">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Payment Type</label>
						    <div class="col-sm-4">
					<?php
                  echo form_dropdown('payment_type',$payment_type,$payment_t," class='form-control' required='required'")?>
						    </div>
						  </div>

						   <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Amount</label>
						    <div class="col-sm-4">
						      <input type="input" name="payment_amount" class="form-control" id="inputEmail3" placeholder="Enter Amount" required="required">
						    </div>
						  </div>
						 	 <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Month</label>
						    <div class="col-sm-4">
						     	<?php
                              echo form_dropdown('month',$months,$selc_m," class='form-control' required='required'")?>
						    </div>
						  </div>
						   <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Year</label>
						    <div class="col-sm-4">
						     	<?php 
						      echo form_dropdown('year',$years,$selc_y,"class='form-control' required='required'")?>
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-sm-offset-3 col-sm-4">
						      <input type="submit" class="btn" style="background:#049ad9; color:#fff;" value="Advance Request"/>
						    </div>
						  </div>
						</form>
					  </div>
					</div>
                <div class="panel panel-default" style="margin-top:1em;">
                    <div class="panel-heading" style="background:#049ad9; color:#fff;">Advances Information</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Request Date</th>
                                    <th>Due month</th>
                                    <th>Advance Type</th>
                                    <th>Amount</th>
                                    <th>Balance</th>
                                    <th>Request Status</th>
                                    <th>Transaction Status</th>
                                    <th>Paid Back</th>
                                </tr>
                          <?php if(!empty($info)){?>
                                    <?php foreach($info as $adv_rec){
                                        $status=$adv_rec->status;

                                        $adv_paid=$adv_rec->paid;?>
                                        <tr>
                                            <td><?php echo date_format_helper($adv_rec->date_created);?></td>
                                            <td><?php echo $adv_rec->month." / ".$adv_rec->year;?></td>
                                            <td><?php echo $adv_rec->payment_type_title;?></td>
                                            <td><?php echo $adv_rec->amount;?></td>
                                            <td><?php echo $adv_rec->balance;?></td>
                                            <td><?php
                                                if($status == 2)
                                                {echo "<span style='color:green'>Approved</span>";}
                                                elseif($status == 1){echo "<span style='color:red'>Pending</span>";}
                                                elseif($status == 3){echo "<span style='color:red'>Decline</span>";}
                                                ?>
                                            </td>
                                            <td><?php if($adv_rec->status == 2 && $adv_rec->paid == 1 && $adv_rec->trans_status==2){echo "<span style='color: green;'>Approved</span>";}?></td>
                                            <td><?php if($adv_rec->status == 2 && $adv_rec->paid_back == 1)
                                                {echo "<span style='color:#181818 ;'>Paid back</span>";}else{echo " ";}?>
                                            </td>

                                        </tr>
                                    <?php }}else {echo'<tr>'. "<span style='color:red'>sorry no record found !</span>".'</tr>';}?>

                            </table>
                        </div>
                    </div>
                </div>
                <div id="container">
                    <ul>
                        <?php echo $links;?>
                    </ul>
                </div>
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
	<script>
		$(function() {
			$('#flash').delay(500).fadeIn('slow', function() {
				$(this).delay(2500).fadeOut('slow');
			});
		});
	</script>