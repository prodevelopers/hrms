   <style>
	   table.innerTable{
		   width: 100%;
	   }
	   table.innerTable>tbody>tr>td{
		   font-weight: normal !important;
	   }
   </style>

    <?php //$this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize" style="background:#fff; height:auto;">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php
                    $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));
                    ?>
 						<div class="col-lg-5">
                            <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
 						</div>
 						<div class="col-lg-7">
 							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
 						</div>
 				</div>
 				<!-- profile <?php echo base_url();?>assets/essp/img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Personal Information</div>
					  <div class="panel-body">
					 	<div class="col-lg-12">
					 		<div class="table-reponsive">
					 			<table class="table table-responsive" id="listing">
					 				<tr>
					 					<td rowspan="4">
                                            <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="150" height="150">
					 					</td>
					 					<td style="font-weight:bold;">Employee Name</td>
					 					<td style="font-weight:normal;"><?php if(isset($personal_info)){ echo $personal_info->full_name;}else{ echo "No Record Found";} ?></td>
					 					<td style="font-weight:bold;">Employee Id</td>
					 					<td style="font-weight:normal;"><?php if(isset($personal_info)){echo $personal_info->employee_code;}else{echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Father Name</td>
					 					<td><?php if(isset($personal_info)){echo $personal_info->father_name;}else{ echo "No Record Found";} ?></td>
					 					<td>CNIC</td>
					 					<td><?php if(isset($personal_info)){echo $personal_info->CNIC;}else{ echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Martial Status</td>
					 					<td><?php if(!empty($marital)){ echo $marital->marital_status_title;}else{ echo"No Marital Status Founded.";} ?></td>
					 					<td>Date Of Birth</td>
					 					<td><?php if(isset($personal_info)){echo date_format_helper($personal_info->date_of_birth);}else{ echo "No Date Of Birth Found.";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Nationality</td>
					 					<td><?php if(!empty($nat)){ echo $nat->nationality;}else{ echo"No Nationality Founded.";} ?></td>
					 					<td>Religion</td>
					 					<td><?php if(!empty($reg)){ echo $reg->religion_title;}else{ echo"No Religon Founded.";} ?></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Contact Information (Current)</h4></td>
					 				</tr>
					 				<tr>
					 					<td>Current Address</td>
					 					<td><?php if(isset($cont)){ echo $cont->address;}else{ echo "No Record Found";} ?></td>
					 					<td>Telephone</td>
					 					<td><?php if(isset($cont)){echo $cont->home_phone;}else{ echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Mobile	</td>
					 					<td><?php if(isset($cont)){echo $cont->mob_num;}else{ echo "No Record Found";} ?></td>
					 					<td>Email</td>
					 					<td><?php if(isset($cont)){echo $cont->email_address;}else{ echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Work Telephone</td>
					 					<td><?php if(isset($cont)){echo $cont->office_phone;}else{echo "No Record Found";}?></td>
					 					<td>Work Email</td>			
					 					<td><?php if(isset($cont)){echo $cont->official_email;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Contact Information (Permanent)</h4></td>
					 				</tr>
					 				<tr>
					 					<td>Permanent Address</td>
					 					<td><?php if(isset($contact)){echo $contact->address;}else{echo "No Record Found";} ?></td>
					 					<td>Telephone</td>		
					 					<td><?php if(isset($contact)){echo $contact->phone;}else{echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>City</td>
					 					<td><?php if(isset($contact)){echo $contact->city_name;}else{echo "No Record Found";} ?></td>
					 					<td>District</td>
					 					<td><?php if(isset($contact)){echo $contact->district_name;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Province</td>
					 					<td><?php if(isset($contact)){echo $contact->province_name;}else{echo "No Record Found";} ?></td>
					 					<td></td>			
					 					<td></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Contact Information (Emergency)</h4></td>
					 				</tr>
					 				<tr>
					 					<td>Emergency Contacts</td>
					 					<td><?php if(isset($emr)){echo $emr->cotact_person_name;}else{echo "No Record Found";}?></td>
					 					<td>Telephone</td>		
					 					<td><?php if(isset($emr)){echo $emr->home_phone;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Mobile</td>
					 					<td><?php if(isset($emr) && !empty($emr)){echo $emr->mobile_num;}else{echo "No Record Found";}?></td>
					 					<td>Relationship</td>
					 					<td><?php if(isset($jn_emr) && !empty($jn_emr)){echo $jn_emr->relation_name;}else{echo " No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Educational Information</h4></td>
					 				</tr>			
					 				<tr>
					 					<td>Degree</td>
					 					<td><?php if(isset($qua)){echo $qua->qualification;}else{echo "No Record Found";}?></td>
					 					<td>Institute</td>
					 					<td><?php if(isset($qua)){echo $qua->institute;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Specialization</td>
					 					<td><?php if(isset($qua)){echo $qua->major_specialization;}else{echo "No Record Found";} ?></td>
					 					<td>GPA/Score</td>
					 					<td><?php if(isset($qua)){echo $qua->gpa_score;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Year Of Completion</td>
					 					<td><?php if(isset($qua)){ echo date_format_helper($qua->year);}else{echo "No Record Found";}?></td>
					 					<td></td>
					 					<td></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Experiance</h4></td>
					 				</tr>			
					 				<tr>
					 					<td>Job Title</td>
					 					<td>Organization</td>
					 					<!--<td>Employment Type</td>-->
					 					<td>Start Date</td>
					 					<td>End Date</td>
					 					<td>Total Experience</td>

					 				</tr>
                                    <?php if(!empty($exp)){
                                        $count=count($exp);
                                        $no=1;
                                        foreach($exp as $experience){?>
                                  <tr>
                                      <td><?php echo $experience->designation_name;?></td>
                                      <td><?php echo $experience->organization; ?></td>
                                      <!--<td><?php /*echo $design->employment_type; */?></td>-->
                                      <td><?php echo date_format_helper($experience->from_date); ?></td>
                                      <td><?php echo date_format_helper($experience->to_date); ?></td>
                                      <td>
                                          <?php
                                          $from_date=$experience->from_date;
                                          $to_date=$experience->to_date;
                                          $exp_start_date= new DateTime($from_date);
                                          $exp_to_date= new DateTime($to_date);

                                            if(!empty($from_date)) {
                                                if($count == $no){
                                              $interval = $exp_start_date->diff($exp_to_date);
                                              echo  $total_current_exp=$interval->y ." Year(s), ".$interval->m." Month(s)";
                                          }}else{echo "";}?>
                                      </td>
                                    </tr>
                                    <?php $no++;}} ?>
					 				<tr>
					 					<td colspan="5"><h4>Skills</h4></td>
					 				</tr>
                                    <tr>
                                        <th>Skill</th>
                                        <th>Level</th>
                                    </tr>
                                    <?php if(!empty($jn_skl)){
                                        foreach($jn_skl as $skills_level){?>
					 				<tr>

					 					<td><?php if(isset($skills_level)){echo $skills_level->skill_name;}else{echo "No Record Found";}?></td>

					 					<td><?php if(isset($skills_level)){echo $skills_level->skill_level;}else{echo "No Record Found";} ?></td>
					 				</tr>
                                    <?php }}?>
					 				<tr>
					 					<td colspan="5"><h4>Employement Information</h4></td>
					 				</tr>			
					 				<tr>
					 					<td>Current Job Title</td>
					 					<td><?php  if(isset($design)){echo $design->designation_name;}else{echo "No Record Found";}?></td>
					 					<td>Job Specification</td>		
					 					<td><?php  if(isset($eee1)){echo $eee1->job_specifications;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Employment Type</td>
					 					<td><?php if(isset($design)){echo $design->employment_type;}else{echo "No Record Found";} ?></td>
					 					<td>Job Category</td>		
					 					<td><?php if(isset($we2)){echo $we2->job_category_name;}else{echo "No Record Found";} ?></td>
					 				</tr>
					 				<tr>
					 					<td>Department</td>
					 					<td><?php if(isset($dd) && !empty($dd)){echo $dd->department_name;}else{echo "No Record Found";}?></td>
					 					<td>City</td>		
					 					<td><?php if(isset($per4)){echo $per4->city_name;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Location</td>
					 					<td><?php if(isset($pp2)){echo $pp2->posting_location;}else{echo "No Record Found";}?></td>
					 					<td>Work Shift</td>		
					 					<td><?php if(isset($pp3)){echo $pp3->shift_name;}else{echo "No Record Found";}?></td>
					 				</tr>
					 				<tr>
					 					<td>Contract Expiry Date</td>
					 					<td><?php if(isset($we)){echo date_format_helper($we->contract_expiry_date);}else{echo "No Record Found";} ?></td>
					 					<td></td>		
					 					<td></td>
					 				</tr>
					 				<tr>
					 					<td colspan="5"><h4>Dependents</h4></td>
					 				</tr>
									<tr>
										<td>Name</td>
					                    <td><?php if(isset($depend)){echo $depend->dependent_name;}else{echo "No Record Found";}?></td>
										<td style="none;">Relationship</td>
										<td><?php  if(isset($depend)){echo $depend->relation_name;}else{echo "No Record Found";}?></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><?php  if(isset($depend)){echo date_format_helper($depend->date_of_birth);}else{echo "NO Record Found";} ?></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
					 					<td colspan="5"><h4>Pay Package</h4></td>
					 				</tr>
					 				<tr>
									<td>Pay Grade</td>
									<td><?php if(isset($pay)){echo $pay->pay_grade;}else{echo "No Record Found";}?></td>
									<td>Pay Frequency</td>
									<td><?php if(isset($pay)){echo $pay->pay_frequency;}else{echo "No Record Found";}?></td>
								</tr>
								<tr>
									<td>Currency</td>
									<td><?php if(isset($pay)){echo $pay->currency_name;}else{echo "No Record Found";}?></td>
									<td>Payrate</td>
									<td><?php if(isset($pay)){echo $pay->payrate;}else{echo "No Record Found";}?></td>
								</tr>
									<tr>
										<td colspan="5"><h4>Entitlements</h4></td>
									</tr>
									<tr>
										<td colspan="2"><h4>Increments</h4></td>
									</tr>
									<tr><td colspan="5">
										<table class="innerTable" width="100%">
											<thead><tr><th>Increment Amount</th><th>Increment Type</th><th>Date Effective</th></tr></thead>
											<tbody>
											<?php if(isset($incrementsData) && !empty($incrementsData) && is_array($incrementsData)){
												foreach($incrementsData as $increment){
													echo "<tr><td>$increment->Amount</td><td>$increment->Type</td><td>$increment->EffectiveDate</td></tr>";
												}
											}else{
												echo '<tr><td colspan="2">No Record Found</td></tr>';
											}
											?>
											</tbody>
										</table>
										</td>
									</tr>
									<tr>
										<td colspan="2"><h4>Benefits</h4></td>
									</tr>
									<tr>
										<td colspan="5">
											<table class="innerTable" width="100%">
												<thead>
												<tr>
													<th>Benefit Type</th>
													<th>Status</th>
												</tr>
												</thead>
												<tbody>

												<?php
												if(isset($benefitsData) && !empty($benefitsData) && is_array($benefitsData)){
													foreach($benefitsData as $benefit){
														echo "<tr><td>$benefit->Benefit</td><td>$benefit->Status</td></tr>";
													}
												}else{
													echo '<tr><td colspan="2">No Record Found</td></tr>';
												}
												?>

												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2"><h4>Allowances</h4></td>
									</tr>
									<tr>
										<td colspan="5">
											<table class="innerTable" width="100%">
												<thead>
												<tr>
													<th>Benefit Type</th>
													<th>Amount</th>
													<th>Status</th>
												</tr>
												</thead>
												<tbody>
												<?php
												if(isset($allowanceData) && !empty($allowanceData) && is_array($allowanceData)){
													foreach($allowanceData as $allowance){
														echo "<tr><td>$allowance->Allowance</td><td>$allowance->Amount</td><td>$allowance->Status</td></tr>";
													}
												}else{
													echo '<tr><td colspan="3">No Record Found</td></tr>';
												}
												?>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2"><h4>Entitled Leaves</h4></td>
									</tr>
									<tr>
										<td colspan="5">
											<table class="innerTable" width="100%">
												<thead>
												<tr>
													<th>Leave Type</th>
													<th>Allocated Leaves</th>
													<th>Entitled Leaves</th>
													<th>Status</th>
												</tr>
												</thead>
												<tbody>
												<?php
												if(isset($leavesEntitlementsData) && !empty($leavesEntitlementsData) && is_array($leavesEntitlementsData)){
													foreach($leavesEntitlementsData as $leave){
														echo "<tr><td>$leave->Type</td><td>$leave->AllocatedLeaves</td><td>$leave->EntitledLeaves</td><td>$leave->Status</td></tr>";
													}
												}else{
													echo '<tr><td colspan="4">No Record Found</td></tr>';
												}
												?>
												</tbody>
											</table>
										</td>
									</tr>

								<tr>
					 				<td colspan="5"><h4>Report To</h4></td>
					 			</tr>
					 			<tr>
								<td colspan="2"><h4>Supervisors</h4></td></tr>
									<tr><td colspan="5">
											<table class="innerTable" width="100%">
												<thead><tr><td>Supervisor Name</td><td>Report Type</td><td>Status</td></tr></thead>
												<tbody>
												<?php
												if(isset($reportingDetails['supervisors']) && is_array($reportingDetails['supervisors']) && !empty($reportingDetails['supervisors'])){
													foreach($reportingDetails['supervisors'] as $supervisor){
														echo "<tr><td>".$supervisor['SupervisorName']."</td><td>".$supervisor['ReportingType']."</td><td>".$supervisor['Status']."</td></tr>";
													}
												}else{
													echo "<tr><td colspan='3'> No Supervisors Currently Assigned </td></tr>";
												}
												?>

												</tbody>
											</table>
										</td></tr>
									<tr>
										<td colspan="2">
											<h4>SubOrdinates</h4>
										</td></tr>
									<tr><td colspan="5">
											<table class="innerTable">
												<thead><tr><td>Subordinate Name</td><td>Report Type</td><td>Status</td></tr></thead>
												<tbody>
												<?php
												if(isset($reportingDetails['subordinates']) && is_array($reportingDetails['subordinates']) && !empty($reportingDetails['subordinates'])){
													foreach($reportingDetails['subordinates'] as $subordinate){
														echo "<tr><td>".$subordinate['SubordinateName']."</td><td>".$subordinate['ReportingType']."</td><td>".$subordinate['Status']."</td></tr>";
													}
												}else{
													echo "<tr><td colspan='3'> No Subordinates Currently Assigned </td></tr>";
												}
												?>

												</tbody>
											</table>
										</td>
									</tr>
									</table>
					  </div>
					</div>
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
  
