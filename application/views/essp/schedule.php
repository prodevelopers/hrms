    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
						</div>
 						<div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				<!--<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
 					<form class="form-inline" role="form">
					  <div class="form-group col-xs-4">
					    <div class="input-group">
					      <div class="input-group-addon">Filter :</div>
					      <input type="text" class="form-control" placeholder="Search Here">
					    </div>
					  </div>  
					  <div class="form-group col-xs-2">
 						<select name="" id="" class="form-control">
					      	<option value="">Select Project</option>
					      	<option value=""></option>
					      </select>					   
					  </div>
					  <div class="form-group col-xs-2">
 						  <select name="" id="" class="form-control">
					      	<option value="">Select Department</option>
					      	<option value=""></option>
					      </select>					    
					  </div>
					  <div class="form-group col-lg-2">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-default" style="background:#049ad9; color:#fff;"><span class="fa fa-search"> </span> Search</button>
					    </div>
					  </div>
					</form>
 				</div>
 				</div>-->
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Schedule</div>
					  <div class="panel-body">
					  	<div class="table-responsive">
						<table class="table table-striped">
						 	<tr>
						 		<th>Employee Name</th>
						 		<th>Project</th>
						 		<th>Department</th>
						 		<th>Task/Project</th>
						 		<th>Start date</th>
						 		<th>Est. Completion Date</th>
						 	</tr>
						 	<tr>

						 		<td><?php if(!empty($employee)){ echo $employee->full_name;} else{echo "No Name Found";}?></td>
						 		<td><?php if(!empty($employee)){ echo $employee->project_title;} else{echo "No Name Found";}?></td>
						 		<td><?php if(!empty($employee)){ echo $employee->department_name;} else{echo "No Name Found";}?></td>
						 		<td><?php if(!empty($employee)){ echo $employee->task_name;} else{echo "No Name Found";}?></td>
						 		<td><?php if(!empty($employee)){ echo $employee->start_date;} else{echo "No Name Found";}?></td>
						 		<td><?php if(!empty($employee)){ echo $employee->completion_date;} else{echo "No Name Found";}?></td>
						 	</tr>

						</table>
						</div>
					  </div>
					</div>
				
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
  