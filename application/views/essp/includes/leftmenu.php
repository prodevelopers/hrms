			<style>

			</style>
			<div class="row left-menu">
 					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 						<ul class="nav nav-pills nav-stacked lo" role="tablist">
						  <li><a href="<?php echo base_url();?>essp/index"><span class="fa fa-home fa-lg"> </span> Home</a></li>
						  <li><a href="<?php echo base_url();?>essp/personal_info"><span class="fa fa-file fa-sm"> </span> Personal Information</a></li>
                            <li><a data-toggle="modal" data-target="#myModal"><span class="fa fa-clock-o fa-lg"> </span> Register Attendence</a></li>
						  <li><a href="<?php echo base_url();?>essp/salary_info"><span class="fa fa-money fa-sm"> </span> Salary Information</a></li>
						  <li><a href="<?php echo base_url();?>essp/progress"><span class="fa fa-line-chart fa-sm"> </span> Performance Evaluation</a></li>
						  <li><a href="<?php echo base_url();?>essp/apply_leave"><span class="fa fa-file-text fa-sm"> </span> Apply for Leave</a></li>
						  <li><a href="<?php echo base_url();?>essp/projects"><span class="fa fa-tasks fa-sm"> </span> Tasks / Projects</a></li>
						  <li><a href="<?php echo base_url();?>essp/expense_claim"><span class="fa fa-money fa-sm"> </span> Expense Claim</a></li>
							<li><a href="<?php echo base_url();?>essp/advance_claim"><span class="fa fa-money fa-sm"> </span> Advance Request</a></li>
						   <li><a href="<?php echo base_url();?>essp/schedule"><span class="fa fa-times-circle-o fa-lg"> </span> Schedule</a></li>
						  <li><a href="<?php echo base_url();?>essp/timesheet"><span class="fa fa-table fa-lg"> </span> Timesheet</a></li>
<!--						  <li><a href="go_to_list"><span class="fa fa-list fa-sm"> </span> Goto List</a></li>-->
<!--						  <li><a href="phone_book"><span class="fa fa-book fa-sm"> </span> Phone Book</a></li>-->
						  <li><a href="<?php echo base_url();?>essp/downloads"><span class="fa fa-download fa-sm"> </span> Downloads</a></li>
						</ul>
 					</div>
 				</div>
 			