<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Employee self service portal (ESSP)</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/essp/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/essp/css/custom.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/essp/font-awesome-4.1.0/css/font-awesome.min.css">
         <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/data_picker/jquery-ui.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/select2/select2.css">
        <script src="<?php echo base_url(); ?>assets/essp/js/jquery-1.11.1.min.js"></script>
         <script src="<?php echo base_url();?>assets/data_picker/jquery-ui.js"></script>
        <script type="application/javascript" src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
        <link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" type="text/css">

    </head>
    <body> 
<div class="container">
    	<!-- start of Navigaiton -->

    		<nav class="navbar navbar-fixed-top" role="navigation" style="background:#049ad9;">
			<div class="navbar-header">
			<button type="button" style="background:#fff;" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">
				<span class="sr-only" style="background:#ccc;">Toggle Navigation</span>
				<span class="icon-bar" style="background:#ccc;"></span>
				<span class="icon-bar" style="background:#ccc;"></span>
				<span class="icon-bar" style="background:#ccc;"></span>
			</button>
			<a  class="navbar-brand"><span class="branding">Employee Self Service Portal (ESSP)</span></a>
			</div>
			<div class="navbar-collapse collapse pull-right">
				<ul class="nav navbar-nav">
				<li class="dropdown doit" style="background:none;">
                    <?php
                    $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));
                    ?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background:none; color:#fff;"><img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="30" height="30"> <?php echo $this->session->userdata('full_name');?> <i class="caret"></i>
							<ul class="dropdown-menu">
								<li><a href="">Settings</a></li>
								<li><a data-toggle="modal" data-target="#up_profile">Update Profile</a></li>
								<li><a href="<?php echo base_url();?>login/log_out">Log out</a></li>
							</ul>
						</a>
					</li>
				</ul>
			</div>
        </nav>
    </div>