<!-- Modal -->
<?php $this->load->view('essp/update_profile') ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background:#049ad9; color:#fff;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>Today is: <b><?php echo date('D'); ?></b></p>

                <p>Current Time is: <b><?php echo date('h:i:s'); ?></b> & Current Date is:
                    <b><?php echo date('d-m-Y'); ?></b></p>
                <?php if (isset($disable->in_date) && ($disable->employee_id))
                {
                ?>
                <p style="margin-bottom:2em;"><a onclick="submit_form()" id="submit1" style="color:#fff;" class="btn btn-primary btn-lg disabled" data-dismiss="modal">DayIn Time</a>
                    <?php } else { ?>
                        <a onclick="submit_form()" id="submit1" style="color:#fff;" class="btn btn-primary btn-lg"
                           data-dismiss="modal">DayIn Time</a>
                    <?php } ?>
                    <?php if (isset($enable->out_date) && ($enable->employee_id))
                    {
                        ?>
                        <a onclick="submit_form1()" style="color:#fff; margin-left:2em;"
                           class="btn btn-primary btn-lg disabled" data-dismiss="modal">DayOut Time</a>
                    <?php } else { ?>
                    <a onclick="submit_form1()" style="color:#fff; margin-left:2em;" class="btn btn-primary btn-lg"
                       data-dismiss="modal">DayOut Time</a></p>
            <?php } ?>
            </div>
            <div class="modal-footer" style="background:#049ad9; color:#fff;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/essp/js/bootstrap.js"></script>
<script>
    $('#myCollapsible').collapse({
        toggle: false
    });


    function submit_form() {
        document.form1.submit();
    }
    $('#myCollapsible').collapse({
        toggle: false
    });

    function submit_form1() {
        document.form2.submit();
    }
    $('#myCollapsible').collapse({
        toggle: false
    });

</script>
<form name="form1" action="Register_attendance" method="post" style="display:none">
    <input type="hidden" name="in_time" value="<?php echo date('h:i:s'); ?>"/>
    <input type="hidden" name="in_date" value="<?php echo date('Y-m-d'); ?>"/>
    <input type="hidden" name="year" value="<?php echo date("Y"); ?>"/>
</form>
<form name="form2" action="dayout_attendance" method="post" style="display:none">
    <input type="hidden" name="out_time" value="<?php echo date('h:i:s'); ?>"/>
    <input type="hidden" name="out_date" value="<?php echo date('Y-m-d'); ?>"/>
</form>
<script src="<?php echo base_url() ?>assets/select2/select2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/noty/packaged/jquery.noty.packaged.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/customScripting.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/Parexons.js" type="text/javascript"></script>
</body>
</html>