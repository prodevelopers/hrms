    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container " style="background:#fff; min-height:100%; overflow:hidden;">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
 						</div>
 						<div class="col-lg-7">
 							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 right-side">
 				<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
                        <!--<form class="form-inline" role="form">
                        !--  <div class="form-group col-xs-4">
                              !--<div class="input-group">
                               <!--<div class="input-group-addon">Filter :</div>
                                   <select name="" id="" class="form-control">
                                       <option value="">Select Project</option>
                                       <option value="">Web Development</option>
                                   </select>
                               </div>
                             </div>
                             <div class="form-group col-xs-2">
                                  <select name="" id="" class="form-control">
                                     <option value="">Select Year</option>
                                     <option value="">2014</option>
                                 </select>
                             </div>
                          <div class="form-group col-xs-2">
                             <select name="" id="" class="form-control">
                                  <option value="">Select Status</option>
                                  <option value="">Completed</option>
                                  <option value="">In Progress</option>
                              </select>
                          </div>
                         <div class="form-group col-lg-1">
                           <div class="col-sm-offset-2 col-sm-10">
                             <button type="submit" class="btn btn-default" style="background:#049ad9; color:#fff;"><span class="fa fa-search"> </span> Search</button>
                           </div>
                         </div>
                       </form>-->
 				</div>
 				</div>
 				<div class="col-lg-12 col-md-11 col-xs-12 col-sm-11">
					<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Performance Evaluation</div>
					  <div class="panel-body">
					  		<div style="width: 50%">
								<canvas id="canvas" height="70%" width="60%"></canvas>
							</div>
					  </div>
					</div>
 				</div>
 				<div class="col-lg-12 col-md-11 col-xs-12 col-sm-11">
					<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Performance Evaluation</div>
					  <div class="panel-body">
					  	<div class="table-responsive">
						<table class="table table-striped">
						 	<tr>
						 		<th>Project Name</th>
						 		<th>Start Date</th>
						 		<th>End Date</th>
						 		<th>Progress</th>
						 	</tr>
                            <?php if(empty($name2)){echo "No Record Founded";} else {foreach($name2 as $nam){?>
						 	<tr>

						 		<td><?php echo $nam->project_title;?></td>
						 		<td><?php echo $nam->start_date;?></td>
						 		<td><?php echo $nam->completion_date;?></td>
						 		<td>
						 		<div class="progress">
								  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $nam->percent_achieved;?>%">
								    <span class="sr-only">%</span>
								  </div>
								</div>
								</td>
						 	</tr>
                            <?php }}?>

						</table>
						</div>
					  </div>
					</div>
 				</div>
				
 	</div>
 	</div>
 	</div>

 	<?php $this->load->view('essp/includes/footer');?>
		<script src="<?php echo base_url();?>assets/essp/js/Chart.js"></script>
		<script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : [<?php if(empty($name)){echo "No Task Assign";}else {foreach($name as $nam){  echo '"';echo $nam->kpi;echo '",'; }}?>],
		datasets : [
			{
				fillColor : "rgba(11,166,225,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php foreach($name as $nam){
					 echo '"';echo $nam->percent_achieved; echo '",'; }?>],
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>
  
