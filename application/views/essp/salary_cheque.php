<script>
function print_table(id) {
     var printContents = document.getElementById(id).innerHTML;
     var originalContents = document.body.innerHTML;
	 
     document.body.innerHTML = printContents;
	 
     window.print();

     document.body.innerHTML = originalContents;
}</script>

<span><input type="button"  name="print" onClick="print_table('print_area')" value="Print"></button></span>
<div id="print_area">
<style type="text/css">
*{
	padding: 0;
	margin:0;
}
.head{
	font-family:"calibri";
	background:#005b96;
	padding: 0.5em;
	color:#fff;
	font-weight: bold;
	font-size: 18px;
}
.cheque{
	width:720px;
	min-height: 6em;
	font-family:"calibri";
	overflow: hidden;
	border: 1px solid #e1e1e1;
	-webkit-box-shadow:0 0 2px rgba(0,0,0,0.4);
	margin-left:30px;	
}
.green {
    background: none repeat scroll 0 0 #6aad6a;
    color: #fff;
}
.btn {
    border: medium none;
    cursor: pointer;
    float: left;
    margin-right: 5px;
    padding: 5px 8px;
}
.beauty h4{
	font-size:13px;
	font-weight:normal;
}
.beauty td{
	font-size: 13px;
}
.beauty{
	width:23%;
	margin:1em 0.5em;
	float:left;
}
.head1{
	background: #4D6684;
	color: #fff;
	font-weight:500;
	font-size: 0.9em;
	line-height: 25px;
}
.br{
	margin: 0.1em 0.1em 0.3em 0.1em;
	width: 99%;
	background: rgba(0,0,0,0.1);
	float: left;
}
.net{
	height: 30px;
	width:76%;
	float: left;
	margin: 0.5em 0;
}
.net p{
	float: right;
	font-size: 14px;
	font-weight: bold;
	color:#005b96;
}
.total{
	height: 30px;
	width: 20%;
	float: left;
	margin: 0.5em 0;
}
.total p{
	font-size: 14px;
	font-style: italic;
	padding-left: 0.6em;
	text-decoration: underline;;
}
.td_style{
	width:171px;
	height:38px;
	font-weight:bold;	
	}
</style>
	<div id="print_area">
		<div class="right-contents">
			<div class="head" style="font-size:1em; font-weight:bold;">PayRoll Record <?php echo @$details->month.", ".@$details->year; ?>
				<!--<input type="button"  name="print" onClick="print_table('print_area')" class="btn green" style="float:right" value="Print">--></div>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="2" style="text-align:left;">Employee Infomation</th>
				</tr>
				<tr class="table-row">
					<td>Name</td>
					<td><h4><?php echo @$details->full_name;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Employee Code</td>
					<td><h4><?php echo @$details->employee_code;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Designation</td>
					<td><h4><?php echo @$details->designation_name;?></h4></td>
				</tr>
				<tr class="table-row">
					<td>Pay Grade</td>
					<td><h4><?php echo @$details->pay_grade;?></h4></td>
				</tr>

			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Salary Info</th>
				</tr>
				<tr >
					<td>Base Salary</td>
					<td><h4><?php echo @$details->base_salary;?></h4></td>
				</tr>
				<tr>
					<td>Deducations</td>

					<td><h4><?php if(!empty($ded_trans->transaction_amount)){echo @$ded_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>
				<tr>
					<td>Allowances</td>

					<td><h4><?php
							if(!empty($allw_trans->transaction_amount)){
								echo @$allw_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>
				<tr>
					<td>Expenses</td>

					<td><h4><?php
							if(!empty($exp_trans->transaction_amount)){
								echo @$exp_trans->transaction_amount;}else{echo "0";}?></h4></td>
				</tr>
				<tr>
					<td>Advances</td>

                    <td><h4><?php
                            if(!empty($advance_trans->advance_trans_amount))
                            {
                                echo @$advance_trans->advance_trans_amount;}else{echo "0";}?>
                        </h4></td>
				</tr>
				<tr>
					<td>Last Arrears</td>

					<td><h4><?php if(!empty($last_salary->arears))
							{
								$last_arrears=$last_salary->arears;
								echo $last_arrears;}else{$last_arrears=0;echo $last_arrears;}?>
						</h4></td>
				</tr>
				<tr>
					<td>Total Paid</td>

					<td><h4><?php echo @$salary_info->transaction_amount;?></h4></td>
				</tr>


			</table>

			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Balance Details</th>
				</tr>
				<tr class="table-row">
					<td>Balance </td>
					<td><h4>
							<?php echo @$salary_info->arears;?></h4></td>
				</tr>

			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8">Deductions</th>
				</tr>
				<tr class="table-row">
					<?php
					$ded_type=@$ded_trans->ded_rectp; $ded_amt=@$ded_trans->ded_recmnt;
					?>
					<td>
						<?php $explode_dedtyp=explode(",",$ded_type);
						$explode_dedamt=explode(",",$ded_amt);
						?>

						<?php foreach($explode_dedtyp as $ded_typ)
						{
							echo $ded_typ."<br />";

						}?>
					</td>
					<td><h4>
							<?php foreach($explode_dedamt as $ded_mt)
							{
								echo $ded_mt."<br />";

							}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total <!--Deductions--> </td>
					<td><h4><?php
							$deduction_amount=@$ded_trans->total_ded_amount;
							echo $deduction_amount;?></h4></td></tr>
			</table>

			<br class="clear"/>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Allowances</th>
				</tr>

				<tr>
					<?php
					$alw_rectp=@$allw_trans->alw_rectp; $alw_recm=@$allw_trans->alw_recm;
					?>
					<td>
						<?php $explode_alw_rectp=explode(",",$alw_rectp);
						$explode_alw_recm=explode(",",$alw_recm);
						?>

						<?php foreach($explode_alw_rectp as $alw_typ)
						{
							echo $alw_typ."<br />";
						}?>
					</td>
					<td><h4>
							<?php foreach($explode_alw_recm as $alw_mt)
							{
								echo $alw_mt."<br />";
							}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total</td>
					<td><h4><?php
							$allowance_amount=@$allw_trans->total_alw_recm;
							echo $allowance_amount;?></h4></td></tr>

			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Expenses</th>
				</tr>

				<tr>
					<?php
					$exp_rectp=@$exp_trans->exp_rectp; $exp_recm=@$exp_trans->exp_recm;
					?>
					<td>
						<?php $explode_exp_rectp=explode(",",$exp_rectp);
						$explode_exp_recm=explode(",",$exp_recm);
						?>

						<?php foreach($explode_exp_rectp as $expense_type)
						{
							echo $expense_type."<br />";
						}?>
					</td>
					<td><h4>
							<?php foreach($explode_exp_recm as $expense_amt)
							{
								echo $expense_amt."<br />";
							}
							?></h4></td>
				</tr>
				<tr>
					<td class="headingfive">Total </td>
					<td><h4><?php
							$exp_amount=@$exp_trans->total_exp_recm;
							echo $exp_amount;?></h4></td></tr>


			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Advances</th>
				</tr>
				<tr>
					<?php
					$advance_rectp=@$advance_trans->loan_rectp; $advance_recm=@$advance_trans->loan_recm;
					?>
					<td>
                        <?php $explode_advance_rectp=explode(",",$advance_rectp);
                        $explode_advance_recm=explode(",",$advance_recm);
                        ?>

                        <?php foreach($explode_advance_rectp as $advance_type)
                        {
                            if(!empty($advance_type))
                            {
                                echo $advance_type."<br />";
                            }elseif(!empty($advance_trans->advance_trans_amount)){ echo "Balance"."<br />";}
                        }?>
					</td>
					<td><h4>
                            <?php foreach($explode_advance_recm as $advance_amt)
                                if(!empty($advance_amt))
                                {
                                    echo $advance_amt."<br />";
                                }
                                elseif(!empty($advance_trans->advance_trans_amount))
                                {echo $advance_trans->advance_trans_amount;}
                            ?></h4></td>
				</tr>
                <tr>
                    <td class="headingfive">Total </td>
                    <td><h4>
                            <?php
                            $advance_amount=@$advance_trans->total_advamount;
                            if(!empty($advance_amount)){
                                echo $advance_amount;}
                            elseif(!empty($advance_trans->advance_trans_amount)){
                                echo $advance_trans->advance_trans_amount;
                            }?></h4>
                    </td></tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Payment Mode </th>
				</tr>

				<?php if(!empty($payment_mode->account_id)){?>
					<tr >
						<td>Account Title</td>
						<td><h4><?php
								echo $payment_mode->account_title;?></h4></td>
					</tr>
					<tr >
						<td>Account Number</td>
						<td><h4><?php
								echo $payment_mode->account_no;?></h4></td>
					</tr>
					<tr >
						<td>Bank</td>
						<td><h4><?php
								echo $payment_mode->bank_name;?></h4></td>
					</tr>
					<tr >
						<td>Branch</td>
						<td><h4><?php
								echo $payment_mode->branch;?></h4></td>
					</tr>
				<?php }if(!empty($payment_mode->cash_id)){?>
					<tr >
						<td>Received By</td>
						<td><h4><?php
								echo $payment_mode->received_by;?></h4></td>
					</tr>
					<tr >
						<td>Remarks</td>
						<td><h4><?php
								echo $payment_mode->remarks;?></h4></td>
					</tr>
				<?php }elseif(!empty($payment_mode->cheque_id)){?>
					<tr >
						<td>Cheque No</td>
						<td><h4><?php
								echo $payment_mode->cheque_no;?></h4></td>
					</tr>
					<tr >
						<td>Bank</td>
						<td><h4><?php
								echo $payment_mode->chk_bank;?></h4></td>
					</tr>
				<?php }?>


			</table>


			<br class="clear">
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Transaction Details</th>
				</tr>
				<tr >
					<td>Transaction Date</td>
					<td><h4><?php
							$date=$details->transaction_date;
							$new=date("d-m-Y",strtotime($date));
							echo @$new;?></h4></td>
				</tr>
				<tr >
					<td>Transaction ID</td>
					<td><h4><?php echo @$details->transaction_id;?></h4></td>
				</tr>
				<tr >
					<td>Reviewed By</td>
					<?php if($details->status==2){?>
					<td><h4><?php echo @$details->name;?></h4></td>
				</tr>
				<tr>
					<td>Approved Date</td>
					<td><h4> <?php
							$date=$details->ap_date;
							if(!empty($date)){
								$new=date("d-m-Y",strtotime($date));
								echo @$new;}else{echo "";}}?></h4></td>
				</tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0">
				<tr class="head">
					<th colspan="2" style="text-align:left;">Previous Salaries Record of <?php echo @$details->year; ?></th>
				</tr>
				<?php if(!empty($salaries)){
					foreach($salaries as $salary){
						$month=$salary->month;
						$current_year=$salary->year;
						$salary_amount=$salary->transaction_amount;
						if($month=="Jan")
						{$jan_salary=$salary_amount;}
						if($month=="Feb")
						{$feb_salary=$salary_amount;}
						if($month=="Mar")
						{$mar_salary=$salary_amount;}
						if($month=="Apr")
						{$apr_salary=$salary_amount;}
						if($month=="May")
						{$may_salary=$salary_amount;}
						if($month=="Jun")
						{$jun_salary=$salary_amount;}
						if($month=="Jul")
						{$jul_salary=$salary_amount;}
						if($month=="Aug")
						{$aug_salary=$salary_amount;}
						if($month=="Sep")
						{$sep_salary=$salary_amount;}
						if($month=="Oct")
						{$oct_salary=$salary_amount;}
						if($month=="Nov")
						{$nov_salary=$salary_amount;}
						if($month=="Dec")
						{$dec_salarys=$salary_amount;}
					}}else{echo "Sorry no previous salary !";}

				?>
				<tr >
					<td>Jan / Feb</td>
					<td><h4><?php if(!empty($jan_salary)){echo @$jan_salary;}
							else{echo "0";}?> /
							<?php if(!empty($feb_salary)){echo @$feb_salary;}
							else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Mar / Apr</td>
					<td><h4><?php if(!empty($mar_salary)){echo @$mar_salary;}
							else{echo "0";}?> /
							<?php if(!empty($apr_salary)){echo @$apr_salary;}
							else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>May / Jun</td>
					<td><h4><?php if(!empty($may_salary)){echo @$may_salary;}
							else{ echo "0";}?> /
							<?php if(!empty($jun_salary)){echo @$jun_salary;}
							else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Jul / Aug</td>
					<td><h4><?php if(!empty($jul_salary)){echo @$jul_salary;}
							else{ echo "0";}?> /
							<?php if(!empty($aug_salary)){echo @$aug_salary;}
							else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Sep / Oct</td>
					<td><h4><?php if(!empty($sep_salary)){echo @$sep_salary;}
							else{ echo "0";}?> /
							<?php if(!empty($oct_salary)){echo @$oct_salary;}
							else{ echo "0";}?></h4></td>
				</tr>
				<tr >
					<td>Nov / Dec</td>
					<td><h4><?php if(!empty($nov_salary)){echo @$nov_salary;}
							else{ echo "0";}?> /
							<?php if(!empty($dec_salarys)){echo @$dec_salarys;}
							else{ echo "0";}?></h4></td>
				</tr>
			</table>
			<table class="table beauty" cellpadding="0" cellspacing="0" style="width:45%">
				<tr class="head">
					<th colspan="8" style="text-align:left;">Total Amount of <?php echo @$details->month." ".$details->year; ?></th>
				</tr>
				<tr >
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><?php echo @$details->salary_date;?></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>Net Paid</td>
					<td><?php /*?><?php
					$base_salary=$details->base_salary;
					$deduction_amount;
					///$loan_advance;
					$expense_amount;
					$alwnc_amount;
					$payable=$base_salary+$expense_amount+$alwnc_amount-$loan_advance-$deduction_amount;
					//$payable=$base_salary+$expense_amount+$alwnc_amount-$deduction_amount;
					 ?><?php */?>
						<h4><?php echo "RS: ".$salary_info->transaction_amount." /-";?></h4></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>