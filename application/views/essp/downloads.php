    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize" style="background:#fff;">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side" style="">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
 						</div>
 						<div class="col-lg-7">
 							<p><?php echo $employee->full_name;?><br><span>Web Developer</span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>

 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">

				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Downloads HRMS Application</div>
					  <div class="panel-body">

					  	<div class="row downloads">
						  <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                              <a href="<?php echo base_url();?>assets/Apps/Parexons.apk">
                                  <img src="<?php echo base_url();?>assets/img/Parexon_apps.png" alt="" width="70%" height="70%"><br>
                                  <p style="color:#00F">Download Attendce App</p>
                              </a>
						  </div>
						</div>
					  </div>
					</div>
                <div class="panel panel-default" style="margin-top:1em;">
                    <div class="panel-heading" style="background:#049ad9; color:#fff;">Downloads Files</div>
                    <div class="panel-body">

                        <div class="row downloads">
                            <div class="col-md-12">
                                <?php if(!empty($download)){
                                    foreach($download as $load){
                                        $explod = explode('.', $load->attached_file);
                                        //echo "<pre>"; print_r($explod);die;?>
                                        <?php if($explod[1] == 'pdf'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2"  data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                    <img src="<?php echo base_url();?>assets/img/pdf.jpg" alt="" width="70%" height="70%"><br>
                                                <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                </div>
                                            </a>
                                        <?php }else if($explod[1] == 'docx'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                <img src="<?php echo base_url();?>assets/img/ms.jpg" alt="" width="70%" height="70%"><br>
                                                    <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                </div>
                                            </a>
                                        <?php }else if($explod[1] == 'xls' || $explod[1] == 'xlsx'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                    <img src="<?php echo base_url();?>assets/img/excel.jpg" alt="" width="70%" height="70%"><br>
                                                    <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                 </div>
                                            </a>
                                        <?php }else if($explod[1] == 'png'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                    <img src="<?php echo base_url();?>assets/img/Preview-icon.png" alt="" width="70%" height="70%"><br>
                                                    <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                </div>
                                            </a>
                                        <?php }else if($explod[1] == 'jpg'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                    <img src="<?php echo base_url();?>assets/img/Preview-icon.png" alt="" width="70%" height="70%"><br>
                                                    <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                </div>
                                            </a>
                                        <?php }else if($explod[1] == 'gif'){?>
                                            <a href="download_app/<?php echo $load->attached_file ?>" target="new">
                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" data-toggle="tooltip" data-placement="top" title="<?php echo $load->remarks ?>">
                                                    <img src="<?php echo base_url();?>assets/img/Preview-icon.png" alt="" width="70%" height="70%"><br>
                                                    <p><?php echo substr($load->remarks,0,8); ?>...</p>
                                                </div>
                                            </a>
                                        <?php }else {?>
<!--                                            <a href="download_app/--><?php //echo $load->attached_file ?><!--" target="new">-->
<!--                                                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2" style="background: green;">-->
<!--                                                <img src=--><?php //base_url();?><!--upload/Download_Files/--><?php //echo $load->attached_file ?><!--" alt="" width="20%" height="20%"><br>-->
<!--                                                <p>--><?php //echo $load->remarks ?><!--</p>-->
<!--                                                    </div>-->
<!--                                            </a>-->
                                        <?php } ?>
                                        </li>
                                    <?php } } ?>

                            </div>







                        </div>
                    </div>
                </div>
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>