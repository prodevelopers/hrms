    <?php $this->load->view('essp/includes/header');?>

 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
 						<div class="col-lg-5">
 							<img src="<?php echo base_url();?>upload/Thumb_Nails/<?php echo $this->session->userdata('thumbnail');?>" class="img img-responsive">
 						</div>
 						<div class="col-lg-7">
 							<p><?php echo $employee->full_name;?><br><span>Web Developer</span></p>
 						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				<div class="row">
 					<div class="col-lg-12" style="margin:1em 0 .5em 0;">
                        <?php echo form_open('');?>
					  <div class="form-group col-xs-4">
					    <div class="input-group">
					      <div class="input-group-addon">Filter :</div>
                            <?php echo form_dropdown('full_name',$exp,$full_name,"class='resize' id='full_name' onchange='this.form.submit()'");?>
					    </div>
					  </div>  
					  <div class="form-group col-xs-2">
                          <?php echo form_dropdown('department_name',$status,$department_name,"class='resize' id='department_name' onchange='this.form.submit()'");?>
					  </div>

					  <div class="form-group col-lg-2">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-default" style="background:#049ad9; color:#fff;"><span class="fa fa-search"> </span> Search</button>
					    </div>
					  </div>
                        <?php echo form_close();?>
 				</div>
 				</div>
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Goto List</div>
					  <div class="panel-body">
					  	<div class="table-responsive">

						<table class="table table-striped" id="table_list">
                            <thead class="table-head">
                        <tr>

						 		<th>Employee Name</th>
						 		<th>Designation</th>
						 		<th>Department</th>
						 		<th>Responsible For</th>
						 		<th>Email</th>
						 		<th>Phone No</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>

						</table>
						</div>
					  </div>
					</div>
				
				</div>
 	</div>
 	</div>
 	<?php $this->load->view('essp/includes/footer');?>
    <script type="application/javascript" src="<?php echo base_url();?>assets/js/data-tables/jquery.js"></script>
    <script type="application/javascript" src="<?php echo base_url();?>assets/js/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            var oTable = $('#table_list').dataTable( {
                "bProcessing": true,
                "bPaginate" :true,
                "sPaginationType": "full_numbers",
                "bServerSide": true,
                "sAjaxSource": window.location+"/list",
                "bDestroy":true,
                "sServerMethod": "POST",
                "aaSorting": [[ 0, "asc" ]],
                "fnServerParams": function (aoData, fnCallBack){
                    aoData.push({"name":"full_name","value":$('#full_name').val()});
                    aoData.push({"name":"department_name","value":$('#department_name').val()});
                }

            });
            $(".filter1").change(function(e) {
                oTable.fnDraw();
            });
        });



        function doc_file(){
            var r  =confirm('if you are upload this file again !\n\ please follow check out method');
            if(r == true){
                return true;
            }else if(r == false){
                return false;
            }
        }

        function confirm()
        {
            alert('Are You Sure to Delete Record ...?');
        }
    </script>