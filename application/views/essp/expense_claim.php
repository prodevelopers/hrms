   <style>
       #pagination
       {
           float:left;
           padding:5px;
           margin-top:15px;
       }
       #pagination a
       {
           padding:5px;
           background-image:url(<?php echo base_url();?>assets/images/top_menu_bg.png);
           color:#5D5D5E;
           font-size:14px;
           text-decoration:none;
           border-radius:5px;
           border:1px solid #CCC;
       }
       #pagination a:hover
       {
           border:1px solid #666;
       }
       .paginate_button
       {
           padding: 6px;
           background-image: url(<?php echo base_url();?>assets/images/top_menu_bg.png);
           color: #5D5D5E;
           font-size: 14px;
           text-decoration: none;
           border-radius: 5px;
           -webkit-border-radius:5px;
           -moz-border-radius:5px;
           border: 1px solid #CCC;
           margin: 1px;
           cursor:pointer;
       }
       .paging_full_numbers
       {
           margin-top:8px;
       }
   </style>
    <?php $this->load->view('essp/includes/header');?>
 	<!-- End Of Header -->
 	<div class="container resize">
 	<div class="row resize">
 			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left_side">
 				<div class="picture">
                    <?php $employeeavatar = empAvatarExist($this->session->userdata('thumbnail'));?>
                    <div class="col-lg-5">
                        <img src="<?php echo ($employeeavatar === TRUE)? base_url('upload/Thumb_Nails').'/'.$this->session->userdata('thumbnail'): $employeeavatar ?>" id="img" width="100" height="100">
						</div>
 						<div class="col-lg-7">
							<p><?php echo $employee->full_name;?><br><span><?php  if(isset($design) && !empty($design)){echo $design->designation_name;;}else{echo "";}?></span></p>
						</div>
 				</div>
 				<!-- profile img and name ends here -->
 				<?php $this->load->view('essp/includes/leftmenu');?>
 			</div>
 			
 			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 right-side">
 				
				<div class="panel panel-default" style="margin-top:1em;">
					<div class="panel-heading" style="background:#049ad9; color:#fff;">Expense Claim</div>
					<div id="flash" ><?php echo $this->session->flashdata('msg');?></div>
					  <div class="panel-body">
					  	<form action="add_expense_records" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
						  <div class="form-group">
		 <label for="inputEmail3" class="col-sm-3 control-label">Employee Name</label>
						    <div class="col-sm-4">
		 <input type="input" name="name" class="form-control" id="inputEmail3" placeholder="Employee Name" value="<?php echo $employee->full_name;?>" readonly="readonly">
        <input type="hidden" name="emp_id" id="inputEmail3"value="<?php echo $employee->employee_id;?>" readonly="readonly">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Expense Type</label>
						    <div class="col-sm-4">
					<?php
                  echo form_dropdown('expense_type',$expense,$expense_t," class='form-control' required='required'")?>
						    </div>
						  </div>
						   <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Expense Date</label>
						    <div class="col-sm-4">
                            <div class="input-group">
								<!--<script>
									$(function(){$("#expdate").datepicker({dateFormat:'yy-mm-dd'})});
								</script>-->
						      <input type="input" class="form-control" id="expdate" placeholder="Effective date" name="expdate"  required="required"/>
                              <label class="input-group-addon"><img src="<?php echo base_url();?>assets/icons/data.png" /></label>
						    	</div>
                            </div>
						  </div>
						  <div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Amount</label>
						    <div class="col-sm-4">
						      <input type="input" name="expamount" class="form-control" id="inputEmail3" placeholder="Enter Amount" required="required">
						    </div>
						  </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Attachment (optional)</label>
                                <div class="col-sm-4">
                                    <input type="file" name="receipt" id="receipt"/>
                                </div>
                            </div>

						  <div class="form-group">
						    <div class="col-sm-offset-3 col-sm-4">
						      <input type="submit" class="btn" style="background:#049ad9; color:#fff;" value="Claim Expense"/>
						    </div>

						  </div>
						</form>

					  </div>

					</div>
                <div class="panel panel-default" style="margin-top:1em;">
                    <div class="panel-heading" style="background:#049ad9; color:#fff;">Expense Claims Information</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Expense Date</th>
                                    <th>Claim Date</th>
                                    <th>Expense Type</th>
                                    <th>Amount</th>
                                    <th>Attachments</th>
                                    <th>Approval Status</th>
                                    <th>Paid</th>
                                </tr>
                                <?php if(!empty($info)){?>
                                    <?php foreach($info as $exp_rec){
                                        $status=$exp_rec->status;
                                        $file=$exp_rec->file_name;
                                        $exp_paid=$exp_rec->paid;?>
                                        <tr>
                                            <td><?php echo date_format_helper($exp_rec->expense_date);?></td>
                                            <td><?php echo date_format_helper($exp_rec->date_created);?></td>
                                            <td><?php echo $exp_rec->expense_type_title;?></td>
                                            <td><?php echo $exp_rec->amount;?></td>
                                            <td><?php if($file)
                                                {?><a href="<?php echo base_url();?>upload/expense_receipts/<?php echo $file;?>"><span class="fa fa-paperclip" style="color: #000000"></span></a>
                                                <?php }else{echo "n/a";}?></td>
                                            <td> <?php
                                                if($status == 2)
                                                {echo "<span style='color:green'>Approved</span>";}
                                                elseif($status == 3){echo "<span style='color:red'>Declined</span>";}
                                                else{echo "<span style='color:red'>Pending</span>";}?></td>
                                            <td><?php if($status == 2 && $exp_paid==1)
                                                {echo "<span style='color:green ;'>YES</span>";}
                                                else{echo "<span style='color:#181818 ;'>NO</span>";}?>
                                            </td>
                                        </tr>
                                    <?php }}else {echo'<tr>'. "<span style='color:red'>sorry no record found !</span>".'</tr>';}?>

                            </table>
                        </div>
                    </div>
                </div>
                <div id="container">
                    <ul>
                        <?php echo $links;?>
                    </ul>
                </div>
				</div>


 	</div>

 	</div>

 	<?php $this->load->view('essp/includes/footer');?>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$(function() {
			$( "#expdate" ).datepicker({dateFormat:'dd-mm-yy'});
		});
	</script>
	<script>
		$(function() {
			$('#flash').delay(500).fadeIn('slow', function() {
				$(this).delay(2500).fadeOut('slow');
			});
		});
	</script>