<?php
$employeePersonalInformation = getEmpPersonalInformation($this->data['EmployeeID']);
?>
<style type="text/css">

    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }

    .paginate_button
    {
        padding: 6px;
        background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }

    input[readonly].default-cursor {
        cursor: pointer;
    }
    .input-group .form-control{
        z-index: 0;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url()?>assets/essp/css/bootstrap.vertical-tabs.css">
<div class="modal fade" id="up_profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background:#049ad9; color:#fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Profile</h4>
      </div>
      <div class="modal-body">
         <div class="col-xs-4" style="border-right:thin solid #ccc;"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">
            <li class="active"><a href="#home" data-toggle="tab">Personal Information</a></li>
            <li><a href="#contact" data-toggle="tab">Contact Information</a></li>
            <li><a href="#education" data-toggle="tab">Education Information</a></li>
            <li><a href="#experiance" data-toggle="tab">Experience Information</a></li>
            <li><a href="#skills" data-toggle="tab">Skills Information</a></li>
          </ul>
        </div>

        <div class="col-xs-8">
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="home">
            <form class="form-horizontal" id="personalInformationForm">
                <div class="form-group">
                <label class="col-sm-4 control-label">Employee Name</label>
                <div class="col-sm-6">
                  <input type="text" name="employeeName" class="form-control" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeName:''; ?>" placeholder="Employee Name">
                </div>
              </div>
               <div class="form-group">
                <label class="col-sm-4 control-label">Father Name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="FatherName" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeFatherName:''; ?>" placeholder="Father Name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">CNIC</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->CNIC:''; ?>" name="CNIC" placeholder="CNIC">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Martial Status</label>
                <div class="col-sm-6">
                    <input type="hidden" id="maritalStatusSelector" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->MaritalStatusID.','.$employeePersonalInformation->MaritalStatusText:''; ?>" name="MaritalStatus">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Date Of Birth</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="dobDatePicker" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->EmployeeDOB)?date("d-m-Y",strtotime($employeePersonalInformation->EmployeeDOB)):'';?>" placeholder="Date Of Birth">
                  <input type="hidden" id="dobDatePickerAlt" name="DOB" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeDOB:'';?>" placeholder="Date Of Birth">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Nationality</label>
                <div class="col-sm-6">
<!--                  <input type="text" class="form-control" name="Nationality" placeholder="Nationality">-->
                    <input type="hidden" name="Nationality" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->NationalityID.','.$employeePersonalInformation->NationalityText:''; ?>" id="nationalitySelector">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Religion</label>
                <div class="col-sm-6">
<!--                  <input type="text" class="form-control" name="Religion" placeholder="Religion">-->
                    <input type="hidden" id="religionSelector" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ReligionID.','.$employeePersonalInformation->ReligionText:''; ?>" name="Religion">
                </div>
              </div>
            </form>
            </div>
            <!-- End -->
            <div class="tab-pane" id="contact">
              <ul class="nav nav-tabs" role="tablist" id="myTab">
              <li role="presentation" class="active"><a href="#Current" aria-controls="Current" role="tab" data-toggle="tab">Current Info</a></li>
              <li role="presentation"><a href="#Permanent" aria-controls="Permanent" role="tab" data-toggle="tab">Permanent Info</a></li>
              <li role="presentation"><a href="#Emergency" aria-controls="Emergency" role="tab" data-toggle="tab">Emergency Info</a></li>
            </ul>

            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="Current">
              <br>
                <form class="form-horizontal" id="currentContactForm">
                    <input type="hidden" name="contactFormType" value="current">
              <div class="form-group">
                <label class="col-sm-4 control-label">Current Address</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="currentAddress" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeAddress:''; ?>" placeholder="Current Address">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">City</label>
                <div class="col-sm-6">
<!--                  <input type="text" class="form-control" name="currentCity" value="" id="districtSelector" placeholder="Current Address">-->
                  <input type="hidden" name="currentCity" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->ccCityVillageID)?$employeePersonalInformation->ccCityVillageID.','.$employeePersonalInformation->ccCityVillageText:'' ?>" class="citySelector">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">District</label>
                <div class="col-sm-6">
<!--                  <input type="text" class="form-control" name="currentCity" value="" id="districtSelector" placeholder="Current Address">-->
                  <input type="hidden" name="currentDistrict" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->ccDistrictID)?$employeePersonalInformation->ccDistrictID.','.$employeePersonalInformation->ccDistrictName:'' ?>" class="districtSelector">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Telephone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="telephone" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeHomePhone:''; ?>" placeholder="Telephone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Mobile</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="mobile" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeMobilePhone:''; ?>" placeholder="Mobile">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ccEmployeeEmail:'' ?>" name="email" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Work Telephone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="workPhone" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmployeeWorkPhone:''; ?>" placeholder="Work Telephone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Work Email</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ccOfficialEmail:''; ?>" name="workEmail" placeholder="Work Email">
                </div>
              </div>
            </form>
            </div>
              <!-- End -->
              <div role="tabpanel" class="tab-pane" id="Permanent">
              <br> 
              <form class="form-horizontal" id="permanentContactForm">
                  <input type="hidden" name="contactFormType" value="permanent">
              <div class="form-group">
                <label class="col-sm-4 control-label">Permanent Address</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="permanentAddress" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->pcEmployeeAddress:'' ?>" placeholder="Permanent Address">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Telephone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="telephone" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->pcEmployeePhone:'' ?>" placeholder="Telephone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">City</label>
                <div class="col-sm-6">
                  <input type="text" name="city" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->pcCityVillageID)?$employeePersonalInformation->pcCityVillageID.','.$employeePersonalInformation->pcCityVillageText:'' ?>" class="citySelector">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">District</label>
                <div class="col-sm-6">
<!--                  <input type="text" class="form-control" name="district" value="--><?php //echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->pcDistrictID)?$employeePersonalInformation->pcDistrictID.','.$employeePersonalInformation->pcDistrictName:'' ?><!--" placeholder="District">-->
                  <input type="hidden" name="district" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->pcDistrictID)?$employeePersonalInformation->pcDistrictID.','.$employeePersonalInformation->pcDistrictName:'' ?>" class="districtSelector">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Province</label>
                <div class="col-sm-6">
                  <input type="text" id="provinceSelector" name="permanentProvince" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->pcProvinceID)?$employeePersonalInformation->pcProvinceID.','.$employeePersonalInformation->pcProvinceName:'' ?>" class="provinceSelector">
                </div>
              </div>
            </form>
              </div>
              <!-- End -->
              <div role="tabpanel" class="tab-pane" id="Emergency">
                 <br> 
              <form class="form-horizontal">
                  <input type="hidden" name="contactFormType" value="emergency">
              <div class="form-group">
                <label class="col-sm-4 control-label">Person Name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="emergencyContactPersonName" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->EmergencyContactPersonName:'' ?>" placeholder="Emergency Person Name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Home Telephone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="homeTelephone" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ecHomePhone:'' ?>" placeholder="Home Telephone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Work Telephone</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="workTelephone" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ecWorkPhone:'' ?>" placeholder="Home Telephone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Mobile</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="ecMobile" value="<?php echo isset($employeePersonalInformation)?$employeePersonalInformation->ecMobilePhone:'' ?>" placeholder="Mobile">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Relationship</label>
                <div class="col-sm-6">
                  <input type="text" id="relationShipSelector" name="relationship" value="<?php echo isset($employeePersonalInformation) && !empty($employeePersonalInformation->ecPersonRelationID)?$employeePersonalInformation->ecPersonRelationID.','.$employeePersonalInformation->ecPersonRelationName:'' ?>">
                </div>
              </div>
            </form>
              </div>
            </div>
            </div>
            <!-- End -->
<script type="text/javascript">
         $(document).ready(function(){
          $(".diploma-field").hide();
          $(".degree-field").show();
          $(".certificate-field").hide();
             $("#qualificationTypeSelector").change(function(){
            $( "select option:selected").each(function(){
                if($(this).attr("value") == "degree"){
                    $(".degree-field").show();
                    $(".certificate-field").hide();
                    $(".diploma-field").hide();
                }
                if($(this).attr("value") == "diploma"){
                    $(".degree-field").hide();
                    $(".certificate-field").hide();
                    $(".diploma-field").show();
                }
                if($(this).attr("value") == "certificate"){
                    $(".degree-field").hide();
                    $(".certificate-field").show();
                    $(".diploma-field").hide();
                }
            });
        }).change();
    });
</script>
            <div class="tab-pane" id="education">
            <form class="form-horizontal" id="educationForm">
                <input type="hidden" id="qualificationIDHiddenField" name="qualification">
             <div class="form-group">
                <label class="col-sm-4 control-label">Select Qualification</label>
                <div class="col-sm-6">
                  <select name="qualificationType" id="qualificationTypeSelector" class="form-control">
                    <option value="degree" selected="selected">Degree</option>
                    <option value="diploma">Diploma</option>
                    <option value="certificate">Certificate</option>
                  </select>
                </div>
              </div>
              <div class="form-group degree-field">
                <label class="col-sm-4 control-label">Degree/Level</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="degreeLevel" placeholder="e-g PHD, BSE, etc">
                </div>
              </div>
              <div class="form-group degree-field">
                <label class="col-sm-4 control-label">Institute</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="degreeInstitute" placeholder="Institute">
                </div>
              </div>
              <div class="form-group degree-field">
                <label class="col-sm-4 control-label">Specialization</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="degreeSpecialization" placeholder="Specialization">
                </div>
              </div>
              <div class="form-group degree-field">
                <label class="col-sm-4 control-label">GPA/Score</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="degreeScore" placeholder="GPA/Score">
                </div>
              </div>
              <div class="form-group degree-field">
                <label class="col-sm-4 control-label">Year Of Completion</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="degreeYearOfCompletion" placeholder="Year Of Completion">
                </div>
              </div>
                <!-- Diploma -->
              <div class="form-group diploma-field">
                <label class="col-sm-4 control-label">Diploma</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="diploma" placeholder="Diploma">
                </div>
              </div>
              <div class="form-group diploma-field">
                <label class="col-sm-4 control-label">Institute</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="diplomaInstitute" placeholder="Institute">
                </div>
              </div>

              <div class="form-group diploma-field">
                <label class="col-sm-4 control-label">Duration</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="diplomaDuration" placeholder="Duration">
                </div>
              </div>

              <div class="form-group diploma-field">
                <label class="col-sm-4 control-label">Year</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="diplomaYear" placeholder="Year">
                </div>
              </div>
              <!-- Cetificate -->
              <div class="form-group certificate-field">
                <label class="col-sm-4 control-label">Certificate</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="certificate" placeholder="Certificate">
                </div>
              </div>
                <div class="form-group certificate-field">
                <label class="col-sm-4 control-label">Institute</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="certificateInstitute" placeholder="Institute">
                </div>
              </div>

              <div class="form-group certificate-field">
                <label class="col-sm-4 control-label">Year</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="certificateYear" placeholder="Year">
                </div>
              </div>
            <!-- end of certificate -->
                <div class="form-group">
                    <div class="col-sm-10">
                        <a style="cursor: pointer;" id="educationFormSubmitBtn" class="btn btn-primary btn-xs pull-right">Save</a>
                    </div>
                </div>
            </form>
                <div class="clearfix"></div>
                <br>
                <div class="table-resposive">
                    <table class="table table-bordered table-condensed table-hover" id="educationListTable">
                        <thead>
                        <th>QualificationID</th>
                        <th>Type</th>
                        <th>Institute</th>
                        <th>GPA/Score</th>
                        <th>Year</th>
                        <td class="text-center"><a href=""><span class="fa fa-pencil"></span></a></td>
                        <td class="text-center"><a href=""><span class="fa fa-trash"></span></a></td>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End -->
            <div class="tab-pane" id="experiance">
              <form class="form-horizontal" id="expForm">
                  <input type="hidden" name="exp">
              <div class="form-group">
                <label class="col-sm-4 control-label">Job Title</label>
                <div class="col-sm-6 input-group">
                  <input type="hidden" id="jobTitleSelector" name="jobTitle" placeholder="Job Title">
                    <label class="input-group-addon" for=""><a href="" data-toggle="modal" data-target="#jobtitle"><span class="fa fa-plus"></span></a></label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Organization</label>
                <div class="col-sm-6 input-group">
                  <input type="text" class="form-control" name="organization" placeholder="Organization">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Employment Type</label>
                <div class="col-sm-6 input-group">
                  <input type="hidden" name="employmentType" id="employmentTypeSelector" placeholder="Employment Type">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Start Date</label>
                <div class="col-sm-6 input-group">
                  <input type="text" class="form-control" id="employmentStartDate" placeholder="Employment Start Date" readonly="readonly">
                  <input type="hidden" id="employmentStartDateAlt" name="startDate">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">End Date</label>
                <div class="col-sm-6 input-group">
                  <input type="text" class="form-control" id="employmentEndDate" placeholder="Employment End Date" readonly="readonly">
                  <input type="hidden" id="employmentEndDateAlt" name="endDate">
                </div>
              </div>
                  <div class="form-group">
                      <div class="col-sm-10">
                          <a style="cursor: pointer;" id="expFormSubmitBtn" class="btn btn-primary btn-xs pull-right">Save</a>
                      </div>
                  </div>
            </form>
                <div class="clearfix"></div>
                <br>
                <div class="table-resposive">
                    <table class="table table-bordered table-condensed table-hover" id="expListTable">
                        <thead>
                        <th>EXP</th>
                        <th>Job</th>
                        <th>Organization</th>
                        <th>Type</th>
                        <td class="text-center"><a href=""><span class="fa fa-pencil"></span></a></td>
                        <td class="text-center"><a href=""><span class="fa fa-trash"></span></a></td>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="skills">
              <form class="form-horizontal" id="skillForm">
                  <input type="hidden" name="skill">
              <div class="form-group">
                <label class="col-sm-4 control-label">Skill Name</label>
                <div class="col-sm-6  input-group">
                  <input type="hidden" id="skillSelector" name="skillName" placeholder="Skill Name">
                    <label class="input-group-addon" for=""><a data-toggle="modal" data-target="#skillNameModal"><span class="fa fa-plus"></span></a></label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Level</label>
                <div class="col-sm-6">
                     <input type="hidden" id="skillLevel" name="SkillLevel" placeholder="Skill Level">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-10">
                  <a style="cursor: pointer;" id="skillFormSubmitBtn" class="btn btn-primary btn-xs pull-right">Save</a>
                </div>
              </div>
            </form> 
            <div class="clearfix"></div>
            <br>
              <div class="table-resposive">
                <table class="table table-bordered table-condensed table-hover" id="skillsListTable">
                  <thead>
                    <th>SkillID</th>
                    <th>Skills</th>
                    <th>Level</th>
                    <td class="text-center"><a href=""><span class="fa fa-pencil"></span></a></td>
                    <td class="text-center"><a href=""><span class="fa fa-trash"></span></a></td>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div> 
          </div>
        </div>
       <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveButton">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!--adding job title-->
<div class="modal fade" id="jobtitle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Job Title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="addJobTitleForm">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Job Title</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="jobTitle" id="jobTitle" placeholder="Job Title">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="jobTitleSaveBtn" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<!--end job title-->

<!--adding Skill Name-->
<div class="modal fade" id="skillNameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Skill</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="addJobTitleForm">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Skill Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="skillName" id="skillNameTxtBox" placeholder="Skill Name">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="skillNameSaveBtn" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<!--end job title-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.maskInput.js"></script>
<script>
    //Need To Do The Magic Here, For Checking and Updating Employee Info..
    var oTable;
    $(document).ready(function(e){
        oTable = '';
        //Need To Do The Magic Here, For Checking and Updating Employee Info..
        var formData;
        $(document).ready(function(e){
            var currentMenu = '';
            formData = {};
            $('#saveButton').on('click', function (e) {
                e.preventDefault();
                var currentActiveDivSelector =  $('div.col-xs-8 div.tab-content div.active');
                var currentActiveDivID = currentActiveDivSelector.attr('id');
                if(currentActiveDivID === 'home'){
                    formData = currentActiveDivSelector.find('#personalInformationForm').serializeArray();
                    formData.push({name:'form',value:'personalInformation'});
                }else if(currentActiveDivID === 'education'){
                    formData = currentActiveDivSelector.find('#educationForm').serializeArray();
                    formData.push({name:'form',value:'education'});
                }else if(currentActiveDivID === 'contact'){
                    var subActiveDiv = currentActiveDivSelector.find('div.active');
                    formData = subActiveDiv.find('form').serializeArray();
                    formData.push({name:'form',value:'contact'});
                }
                $.ajax({
                    url: "<?php echo base_url(); ?>essp/update_personal_information",
                    type: "POST",
                    data: formData,
                    success: function(output){
                        var data = output.split('::');
                        if(data[0] === "OK"){
                            Parexons.notification(data[1],data[2]);
                        }else if(data[0] === "FAIL"){
                            Parexons.notification(data[1],data[2]);
                        }
                    }
                });
            });
        });


        //Load All The Selectors..
        //Loading Marital Status Selctor
        var maritalStatusSelector = $('#maritalStatusSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_marital_statuses";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Marital Status";
        var multiple = false;
        commonSelect2(maritalStatusSelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Nationality Selector
        var nationalitySelector = $('#nationalitySelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_nationalities";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Nationality";
        var multiple = false;
        commonSelect2(nationalitySelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Religion Selector
        var religionSelector = $('#religionSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_religion";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Religion";
        var multiple = false;
        commonSelect2(religionSelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Religion Selector
        var districtSelector = $('.districtSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_districts";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select District";
        var multiple = false;
        commonSelect2(districtSelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Religion Selector
        var citySelector = $('.citySelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_cities";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select District";
        var multiple = false;
        commonSelect2(citySelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Provinces
        var citySelector = $('.provinceSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_provinces";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select District";
        var multiple = false;
        commonSelect2(citySelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Relations
        var citySelector = $('#relationShipSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_relations";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Relation";
        var multiple = false;
        commonSelect2(citySelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Skills
        var skillSelector = $('#skillSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_skills";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Skill";
        var multiple = false;
        commonSelect2(skillSelector,url,id,text,minInputLength,placeholder,multiple);

         //Loading Skills
        var skillLevel = $('#skillLevel');
        var url = "<?php echo base_url(); ?>essp/load_all_available_skill_level";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Skill Level";
        var multiple = false;
        commonSelect2(skillLevel,url,id,text,minInputLength,placeholder,multiple);

        //Loading Jobs
        var jobTitleSelector = $('#jobTitleSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_jobs";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Skill";
        var multiple = false;
        commonSelect2(jobTitleSelector,url,id,text,minInputLength,placeholder,multiple);

        //Loading Employment Types
        var employmentTypeSelector = $('#employmentTypeSelector');
        var url = "<?php echo base_url(); ?>essp/load_all_available_employment_types";
        var id = "ID";
        var text = "TEXT";
        var minInputLength = 0;
        var placeholder = "Select Skill";
        var multiple = false;
        commonSelect2(employmentTypeSelector,url,id,text,minInputLength,placeholder,multiple);



        /**
         * Skill Form, An Employee can Have Multiple Skills So We Need Data Tables
         * First We Need To Do the Form Section, As We Need To Save Data For Employee..
         */

        // 1) Add/Update Skill Form
        var skillFromSubmitBtn = $('#skillFormSubmitBtn');
        skillFromSubmitBtn.on('click',function(e){
            var skillFormSelector = $('#skillForm');
            var skillFormData = skillFormSelector.serializeArray();
            var skillName = skillFormSelector.find('input[name="skillName"]').val();
            var SkillLevel = skillFormSelector.find('input[name="skillLevel"]').val();
            if(skillName.length === 0){
                Parexons.notification('Skill Name Field Can Not Be Left Empty','error');
                return;
            }
            if(SkillLevel == ""){
                Parexons.notification('You Must Skill level','error');
                return;
            }
            $.ajax({
                url: "<?php echo base_url(); ?>essp/updateSkillInfo/save",
                data: skillFormData,
                type: "POST",
                success: function(output){
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        skillListDataTable.dataTable().fnDestroy();
                        commonDataTables(skillListDataTable,url,aoColumns,sDom,HiddenColumnID);
                        skillListDataTable.css('width','100%');
                        skillFormSelector[0].reset();
                        skillSelector.select2('data', null);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });

        });

        // 2) Skill List DataTable
        var skillListDataTable = $('#skillsListTable');
        var url = "<?php echo base_url(); ?>essp/employee_skills_DT";
        var aoColumns = [
            /* ID */ {
                "mData": "SkillID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Skill Name */ {
                "mData": "Skill"
            },
            /* Experience In Years */ {
                "mData": "Level"
            },
            /* Edit Update Pencil Icon */ {
                "mData": "EditUpdate"
            },
            /* Trash Box Icon */ {
                "mData": "Trash"
            }
        ];
        var HiddenColumnID = 'SkillID';
        var sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(skillListDataTable,url,aoColumns,sDom,HiddenColumnID);
         //Little Functions For icons in The DataTables
        skillListDataTable.on('click','.trashBox',function (e) {
            var skillID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateSkillInfo/trash",
                data:{skill:skillID},
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        //If Record Is Success, We Need To Refresh The Table To Remove That Data From DataTable.
                        oTable.fnDestroy();
                        commonDataTables(skillListDataTable,url,aoColumns,sDom,HiddenColumnID);
                        skillListDataTable.css('width','100%');
                    }else{
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
        skillListDataTable.on('click','.editUpdatePencil',function (e) {
            var skillID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateSkillInfo/edit",
                data:{skill:skillID},
                type:"POST",
                dataType:"json",
                success:function(data){
                    //Load The Data in to The Form Fields..
//                    console.log(data['Experience']);
                    var skillFormSelector = $('#skillForm');
                    //skillFormSelector.find('input[name="skillLevel"]').val(data.ml_skill_level_id);
                    skillFormSelector.find('input[name="skill"]').val(data.SkillID);
                    skillFormSelector.find('input[name="skillName"]').select2("data", { id: data.SkillTypeID, text: data.SkillName });
                    skillFormSelector.find('input[name="SkillLevel"]').select2("data", { id: data.ml_skill_level_id, text: data.Level });
                }
            });
        });


        /**
         * Experience Form
         */

        // 1) Add/Update Experience Form
            var expFormSubmitBtn = $('#expFormSubmitBtn');
            expFormSubmitBtn.on('click', function (e) {
            var expFormSelector = $('#expForm');
            var expFormData = expFormSelector.serializeArray();
            var jobTitle = expFormSelector.find('input[name="jobTitle"]').val();
            var organization = expFormSelector.find('input[name="organization"]').val();
            if(jobTitle.length === 0){
                Parexons.notification('Job Title Field Can Not Be Left Empty','error');
                return;
            }
            if(organization.length === 0){
                Parexons.notification('Organization Field Can Not Be Left Empty','error');
                return;
            }
            $.ajax({
                url: "<?php echo base_url(); ?>essp/updateExperienceInfo/save",
                data: expFormData,
                type: "POST",
                success: function(output){
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        expListTable.dataTable().fnDestroy();
                        commonDataTables(expListTable,expUrl,exp_aoColumns,exp_sDom,expHiddenColumnID);
                        expListTable.css('width','100%');
                        expFormSelector[0].reset();
                        jobTitleSelector.select2('data', null);
                        employmentTypeSelector.select2('data', null);
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax Call
        });

        // 2) Experience List DataTable
        var expListTable = $('#expListTable');
        var expUrl = "<?php echo base_url(); ?>essp/employee_experiences_DT";
        var exp_aoColumns = [
            /* ID */ {
                "mData": "ExpID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Skill Name */ {
                "mData": "EmployeeJobName"
            },
            /* Experience In Years */ {
                "mData": "Organization"
            },
            /* Edit Update Pencil Icon */ {
                "mData": "EmploymentType"
            },
            /* Edit Update Pencil Icon */  {
                "mData": "EditUpdate"
            },
            /* Trash Box Icon */ {
                "mData": "Trash"
            }
        ];
        var expHiddenColumnID = 'ExpID';
        var exp_sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(expListTable,expUrl,exp_aoColumns,exp_sDom,expHiddenColumnID);
         //Little Functions For icons in The DataTables
        expListTable.on('click','.trashBox',function (e) {
            var expID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateExperienceInfo/trash",
                data:{exp:expID},
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        //If Record Is Success, We Need To Refresh The Table To Remove That Data From DataTable.
                        oTable.fnDestroy();
                        commonDataTables(expListTable,expUrl,exp_aoColumns,exp_sDom,expHiddenColumnID);
                        expListTable.css('width','100%');
                    }else{
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
        expListTable.on('click','.editUpdatePencil',function (e) {
            var expID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateExperienceInfo/edit",
                data:{exp:expID},
                type:"POST",
                dataType:"json",
                success:function(data){
                    //Load The Data in to The Form Fields..
//                    console.log(data['Experience']);
                    var expFormSelector = $('#expForm');
                    expFormSelector.find('input[name="organization"]').val(data.Organization);
                    expFormSelector.find('input[name="exp"]').val(data.ExpID);
                    expFormSelector.find('input[name="startDate"]').val(data.EmploymentFromDate);
                    expFormSelector.find('input[name="endDate"]').val(data.EmploymentToDate);
                    expFormSelector.find('input[name="jobTitle"]').select2("data", { id: data.JobID, text: data.JobTitle });
                    expFormSelector.find('input[name="employmentType"]').select2("data", { id: data.EmploymentTypeID, text: data.EmploymentTypeText });
                    //Need To Show Dates To User Also
                    $('#employmentStartDate').val(data.EmploymentFromDateUsr);
                    $('#employmentEndDate').val(data.EmploymentToDateUsr);
                }
            });
        });



        /**
         * Education Form
         */

        // 1) Add/Update Experience Form
            var educationFormSubmitBtn = $('#educationFormSubmitBtn');
            educationFormSubmitBtn.on('click', function (e) {
            var educationSelector = $('#educationForm');
            var qualificationType = $('#qualificationTypeSelector').val();

                //Now Lets Do The Checks That If The Fields Are Properly Entered..
            if(qualificationType === 'degree'){
                var degreeLevel = educationSelector.find('input[name="degreeLevel"]').val();
                var degreeInstitute = educationSelector.find('input[name="degreeInstitute"]').val();
                var degreeSpecialization = educationSelector.find('input[name="degreeSpecialization"]').val();
                var degreeScore = educationSelector.find('input[name="degreeScore"]').val();
                var degreeYearOfCompletion = educationSelector.find('input[name="degreeYearOfCompletion"]').val();
                //Checks For degree Qualification
                if(degreeLevel.length === 0){
                    Parexons.notification('Degree/Level Field Can Not Be Left Empty','error');
                    return;
                }
                if(degreeInstitute.length === 0){
                    Parexons.notification('Institute Field Can Not Be Left Empty','error');
                    return;
                }
                if(degreeScore.length === 0){
                    Parexons.notification('GPA/Score Field Can Not Be Left Empty','error');
                    return;
                }
                if(degreeYearOfCompletion.length === 0){
                    Parexons.notification('Year Of Completion Field Can Not Be Left Empty','error');
                    return;
                }

                //As We Have Got All The Work Done Here, We Will Make The Ajax Call From Here...
                var postData = {
                    degree:degreeLevel,
                    qualificationType:'degree',
                    institute:degreeInstitute,
                    specialization:degreeSpecialization,
                    score:degreeScore,
                    completionYear:degreeYearOfCompletion
                };
            }else if(qualificationType === 'diploma'){
                //Getting All The Required Form Data For the Diploma
                var diploma = educationSelector.find('input[name="diploma"]').val();
                var diplomaInstitute = educationSelector.find('input[name="diplomaInstitute"]').val();
                var diplomaDuration = educationSelector.find('input[name="diplomaDuration"]').val();
                var diplomaYear = educationSelector.find('input[name="diplomaYear"]').val();
                //Checks For diploma Qualification
                if(diploma.length === 0){
                    Parexons.notification('Diploma Field Can Not Be Left Empty','error');
                    return;
                }
                //As We Have Got All The Work Done Here, We Will Make The Ajax Call From Here...
                var postData = {
                    diploma:diploma,
                    qualificationType:'diploma',
                    institute:diplomaInstitute,
                    diplomaDuration:diplomaDuration,
                    completionYear:diplomaYear
                };
            }else if(qualificationType === 'certificate'){
                //Getting All The Required Form Data For the Certificate
                var certificate = educationSelector.find('input[name="certificate"]').val();
                var certificateInstitute = educationSelector.find('input[name="certificateInstitute"]').val();
                var certificateYear = educationSelector.find('input[name="certificateYear"]').val();
                //Checks For Certificate Qualification
                if(certificate.length === 0){
                    Parexons.notification('Cetificate Field Can Not Be Left Empty','error');
                    return;
                }
                if(certificateInstitute.length === 0){
                    Parexons.notification('Institute Field Can Not Be Left Empty','error');
                    return;
                }
                if(certificateYear.length === 0){
                    Parexons.notification('Year Field Can Not Be Left Empty','error');
                    return;
                }

                //As We Have Got All The Work Done Here, We Will Make The Ajax Call From Here...
                var postData = {
                    certificate:certificate,
                    qualificationType:'certificate',
                    institute:certificateInstitute,
                    completionYear:certificateYear
                };
            }
                var qualificationID = $('#qualificationIDHiddenField').val();
                if(qualificationID.length > 0){
                    postData.qualification = qualificationID;
                }
                console.log(qualificationID);

            $.ajax({
                url: "<?php echo base_url(); ?>essp/updateEducationInfo/save",
                data: postData,
                type: "POST",
                success: function(output){
                    var data = output.split("::");
                    if(data[0] === "OK"){
                        Parexons.notification(data[1],data[2]);
                        educationListTable.dataTable().fnDestroy();
                        commonDataTables(educationListTable,educationUrl,education_aoColumns,education_sDom,educationHiddenColumnID);
                        educationListTable.css('width','100%');
                        educationSelector[0].reset();
                        $('#qualificationIDHiddenField').val('');
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }); //End Of Ajax Call
        });

        // 2) Experience List DataTable
        var educationListTable = $('#educationListTable');
        var educationUrl = "<?php echo base_url(); ?>essp/employee_qualification_DT";
        var education_aoColumns = [
            /* ID */ {
                "mData": "QualificationID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Type */ {
                "mData": "QualificationTitle"
            },
            /* Institute */ {
                "mData": "Institute"
            },
            /* GPA / Score */ {
                "mData": "Score"
            },
            /* Completion Year */  {
                "mData": "Year"
            },
            /* Edit Update Pencil Icon */ {
                "mData": "EditUpdate"
            },
            /* Trash Box Icon */ {
                "mData": "Trash"
            }
        ];
        var educationHiddenColumnID = 'QualificationID';
        var education_sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(educationListTable,educationUrl,education_aoColumns,education_sDom,educationHiddenColumnID);
         //Little Functions For icons in The DataTables
        educationListTable.on('click','.trashBox',function (e) {
            var educationID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateEducationInfo/trash",
                data:{qualification:educationID},
                type:"POST",
                success:function(output){
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        //If Record Is Success, We Need To Refresh The Table To Remove That Data From DataTable.
                        oTable.fnDestroy();
                        commonDataTables(educationListTable,educationUrl,education_aoColumns,education_sDom,educationHiddenColumnID);
                        educationListTable.css('width','100%');
                    }else{
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
        educationListTable.on('click','.editUpdatePencil',function (e) {
            var educationID = $(this).closest('tr').attr('data-id');
            //As We Have the SkillID, So We Will Trash The The Record On Button Press
            $.ajax({
                url:"<?php echo base_url(); ?>essp/updateEducationInfo/edit",
                data:{qualification:educationID},
                type:"POST",
                dataType:"json",
                success:function(data){
//                    Lets Get The Data And Assigned Data in the to the variables
                    var DegreeLevel = data.DegreeLevel,
                        Institute = data.Institute,
                        QualificationID = data.QualificationID,
                        Score = data.Score,
                        Specialization = data.Specialization,
                        Duration = data.Duration,
                        QualificationTitle = data.QualificationTitle,
                        Year = data.Year;

                    var educationFormSelector = $('#educationForm');
                    $('#qualificationTypeSelector').val(QualificationTitle.toLowerCase()).change();
                    educationFormSelector.find('input[name="qualification"]').val(QualificationID);

                    if(QualificationTitle === 'Degree'){
                        //Populate the Form Regarding Degree Fields
                        educationFormSelector.find('input[name="degreeLevel"]').val(DegreeLevel);
                        educationFormSelector.find('input[name="degreeInstitute"]').val(Institute);
                        educationFormSelector.find('input[name="degreeSpecialization"]').val(Specialization);
                        educationFormSelector.find('input[name="degreeScore"]').val(Score);
                        educationFormSelector.find('input[name="degreeYearOfCompletion"]').val(Year);
                    }else if(QualificationTitle === 'Diploma'){
                        //Populate the Form Regarding Diploma Fields
                        educationFormSelector.find('input[name="diploma"]').val(DegreeLevel);
                        educationFormSelector.find('input[name="diplomaInstitute"]').val(Institute);
                        educationFormSelector.find('input[name="diplomaDuration"]').val(Duration);
                        educationFormSelector.find('input[name="diplomaYear"]').val(Year);
                    }else if(QualificationTitle === 'Certificate'){
                        //Populate the Form Regarding Certificate Fields
                        educationFormSelector.find('input[name="certificate"]').val(DegreeLevel);
                        educationFormSelector.find('input[name="certificateInstitute"]').val(Institute);
                        educationFormSelector.find('input[name="certificateYear"]').val(Year);
                    }
                }
            });
        });



        //Loading DatePickers
        $( "#dobDatePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            altField: "#dobDatePickerAlt",
            altFormat: "yy-mm-dd"
        });

        //Employments Start And End Dates.
        $( "#employmentStartDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            altField: "#employmentStartDateAlt",
            altFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#employmentEndDate" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#employmentEndDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            altField: "#employmentEndDateAlt",
            altFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#employmentStartDate" ).datepicker( "option", "maxDate", selectedDate );
            }
        });


        //Extra Little Modals Work Below
        $('#jobTitleSaveBtn').on('click',function(e){
            var jobTitle = $('#jobTitle').val();
            if(jobTitle.length === 0){
                Parexons.notification('Job Title Field Can Not Be left Empty','error');
                return;
            }
            var postData = {
                jobTitle: jobTitle
            };

            $.ajax({
                url:"<?php echo base_url(); ?>essp/add_job_title",
                data: postData,
                type:"POST",
                success: function (output) {
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        $('#jobTitleSelector').select2("data",{id:data[3],text:jobTitle});
                        $('#jobtitle').modal('hide');
                        $('#jobTitle').val('');
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            })
        });


        //Extra Little Modals Work Below
        $('#skillNameSaveBtn').on('click',function(e){
            var skillName = $('#skillNameTxtBox').val();
            if(skillName.length === 0){
                Parexons.notification('Skill Name Field Can Not Be left Empty','error');
                return;
            }
            var postData = {
                skillName: skillName
            };

            $.ajax({
                url:"<?php echo base_url(); ?>essp/add_skill_name",
                data: postData,
                type:"POST",
                success: function (output) {
                    var data = output.split('::');
                    if(data[0] === 'OK'){
                        Parexons.notification(data[1],data[2]);
                        $('#skillSelector').select2("data",{id:data[3],text:skillName});
                        $('#skillNameModal').modal('hide');
                        $('#skillNameTxtBox').val('');
                    }else if(data[0] === 'FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            })
        });

        //resize all the selectors
        $('.select2-container').css("width","223px");
        //MASK For CNIC
        $("input[name='CNIC']").mask("99999-9999999-9");
    });
</script>