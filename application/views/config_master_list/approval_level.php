

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Configuration / Master Lists</div> <!-- bredcrumb -->


	

	<div class="right-contents">

		<div class="head">Master Lists</div>

			<?php $this->load->view('includes/config_master_list_nav.php'); ?>	
			

			<div class="right-list">
				<form action="site/add_approval" method="post">

				<div class="row">
					<h4><b>Approval Level</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Approval Level</h4>
					<input type="text" name="approval_level">
				</div>
				
				<!-- button group -->
			<div class="row">
					 <input type="submit" class="btn green" value="Add" name="add" />
			</div>
				
			</form>


			<!-- table -->
	   	<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Approval Level</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                  <?php foreach($approval as $al):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo $al->approval_level?></td>
					<td><a href="site/edit_approval_level/<?php echo $al->approval_level_id?>" /><span class="fa fa-pencil"></span></a></td>
					<td><a href="site/delete_approval_level/<?php echo $al->approval_level_id?>" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
			</table>
			</div>

			

		</div>

	</div>
<!-- contents -->


