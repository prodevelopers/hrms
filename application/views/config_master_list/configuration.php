

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Configuration</div> <!-- bredcrumb -->


 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Notification Configuration</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Designation</option>
				</select>
				<select>
					<option>Notification</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>

			

			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Designation</td>
					<td>Notification Type</td>
					<td>Status</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</thead>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Manager</td>
					<td>Leave Approval</td>
					<td>Active</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Manager</td>
					<td>Leave Approval</td>
					<td>Active</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Manager</td>
					<td>Leave Approval</td>
					<td>Active</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Manager</td>
					<td>Leave Approval</td>
					<td>Active</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Manager</td>
					<td>Leave Approval</td>
					<td>Active</td>
					<td align="center"><span class="fa fa-pencil"></span></td>
				</tr>
			</table>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="new_notification.php"><button class="btn green">New Notification</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->


