<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>


<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Master Lists / Configuration Type</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/leave_master_list_nav'); ?>	

	<div class="right-contents1">

		<div class="head">Configuration Type</div>



		<form action="<?php echo base_url();?>leave_site/AddConfigurationTypes" method="post">

				<div class="row">
					<h4><b>Configuration Types</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Configuration Type</h4>
					<input type="text" name="config" />
				</div>
				
				<!-- button group -->
			<div class="row">
				<input type="submit" class="btn green"  value="Add" name="add" />
			</div>
				
			</form>


			<!-- table -->
             <table cellspacing="0">
				<thead class="table-head">

					<td>Configuration Type</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				 </thead>
                 <?php if(isset($Config)){
                  foreach($Config as $Configuration): ?>
				  <tr class="table-row">
					<td><?php echo $Configuration->ConfigTypes;?></td>
                    <?php if($Configuration->enabled == 1):?>
                    <td><?php echo "Enabled"; ?></td>
                    <?php else:?>
                    <td><?php echo "Disabled"?></td>
                    <?php endif;?>
					<td><a href="leave_site/EditConfigurationTypes/<?php echo $Configuration->ConfigID?>" /><span class="fa fa-pencil"></span></a></td>
					<td><a href="leave_site/trashedConfiguration/<?php echo $Configuration->ConfigID?>/<?php echo 'ConfigID';?>" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
                           <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
			</table>


			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');

    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

