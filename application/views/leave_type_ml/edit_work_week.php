<?php //include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource / Master List / Work Week </div> <!-- bredcrumb -->


		<?php $this->load->view('includes/leave_master_list_nav'); ?>	
	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

 				<form action="" method="post">
               <br class="clear">
				<div class="row">
					<h4><b>Day</b></h4>
				</div>
                <div class="row">
					<input type="text" readonly disabled="disabled" value="<?=(isset($user) && !empty($user))?$user->work_week:""; ?>">
                </div>
					<br class="clear">
				<div class="row">
					<h4><b>Work Week</b></h4>
				</div>
                <div class="row">
                	<select name="working_day">
						<option>Full Day</option>
						<option>Half Day</option>
						<option>Weekend</option>
					</select>
                </div>
                
				<!-- button group -->
                 <br class="clear">
			<div class="row">
            <input type="submit" name="edit" value="Update" class="btn green" />
				
			</div>
			</form>
			</div>
		</div>
<!-- contents -->

<?php //include('includes/footer.php'); ?>
