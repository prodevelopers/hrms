<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Master Lists</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/leave_master_list_nav.php'); ?>	
	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php // echo  $this->session->flashdata('msg');?></div>


		<form action="leave_site/add_work_week" method="post">

				<div class="row">
					<h4><b>Work Week</b></h4>
				</div>
					<br class="clear">
					<div class="row">
						<h4>Add Work Week</h4>
						<input type="text" name="work_week"  required="required"/>
					</div>
					<br class="clear">
					<div class="row">
						<h4><b>Working Day</b></h4>
					

						<select name="working_day" required="required">
							<option>Full Day</option>
							<option>Half Day</option>
							<option>Weekend</option>
						</select>
					</div>

					<div class="row">
						<input type="submit" name="add" value="Submit" class="btn green" />
						</div>
			</form>
<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Days</td>
                    <td>Working Hours</td>
					<td><span class="fa fa-pencil"></span></td>
					
				</thead>
                  <?php if($data > 0){
                   foreach($data as $work_week):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo @$work_week->work_week?></td>
                    <td><?php echo @$work_week->working_day?></td>
		<td><a href="leave_site/edit_work_week/<?php echo $work_week->work_week_id?>" /><span class="fa fa-pencil"></span></a></td>
				</tr>
				<?php endforeach;?>
                 <?php } else { echo "<div  class=\"result\" style='color:red;'>Sorry No record found..</div>";} ?>
			</table>

			

			

		</div>

	</div>
<!-- contents -->

<?php //include('includes/footer.php'); ?>
<script>
	$("#alert").delay(3000).fadeOut('slow');

    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>