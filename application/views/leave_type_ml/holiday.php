<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Master Lists</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/leave_master_list_nav'); ?>	

	<div class="right-contents1">

		<div class="head">Master Lists</div>

			

				<form action="leave_site/add_holiday" method="post">
<?php //echo "<pre>"; print_r($approval);?>
		  <div class="row">
					<h4><b>Holidays</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Holidays</h4>
                    <input type="text" name="holiday_type" required>
				</div>
                <br class="clear">
				<div class="row">
					<h4>From Date</h4>
                    <input type="text" id="from_date" name="from_date">
				</div>
                  <br class="clear">
				<div class="row">
					<h4>To Date</h4>
                    <input type="text" id="to_date" name="to_date">
				</div>


				
				<!-- button group -->
			<div class="row">
					<input type="submit" class="btn green"  value="Add" name="add" />
			</div>
				
			</form>

		<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Holidays</td>
                    <td>From Date</td>
                    <td>To Date</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                  <?php if($data > 0){
                   foreach($data as $holiday):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo $holiday->holiday_type?></td>
                    <td><?php echo date_format_helper($holiday->from_date);?></td>
                    <td><?php echo date_format_helper($holiday->to_date);?></td>
                    <?php if($holiday->enabled == 1):?>
                    <td><?php echo "Enabled"; ?></td>
                    <?php else:?>
                    <td><?php echo "Disabled"?></td>
                    <?php endif;?>
		<td><a href="leave_site/edit_holiday/<?php echo $holiday->ml_holiday_id?>" /><span class="fa fa-pencil"></span></a></td>
		<td><a href="leave_site/trashed_holiday/<?php echo $holiday->ml_holiday_id?>/<?php echo 'ml_holiday_id';?>" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
                 <?php } else { echo "<div  class=\"result\">No record found..</div>";} ?>
			</table>

	  </div>

			


	</div>
<!-- contents -->

<script>
  $(function() {
    $( "#from_date,#to_date" ).datepicker({dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true

    });

  });



  $(document).ready(function(e){

      //Load Page Messages
      <?php if(!empty($pageMessages) && is_array($pageMessages)){
      echo "var message;";
      foreach($pageMessages as $key=>$message){
          if(!empty($message) && isset($message)){
                  echo "message = '".$message."';"; ?>
      var data = message.split("::");
      Parexons.notification(data[0],data[1]);
      <?php
      }
      }
  }
  ?>
  });

  </script>