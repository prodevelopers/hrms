

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php  $this->load->view('includes/hr_master_list_nav'); ?>
	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>

        <div class="right-list">
				<form action="" method="post">

				<div class="row">
					<h4><b>Project</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Project Name</h4>
					<input type="text" name="project_title" value="<?php if(isset($user->project_title)){echo $user->project_title;} ?>">
				</div>
					<div class="row">
						<h4>Project Abbreviation</h4>
						<input type="text" name="Abbreviation" value="<?php if(isset($user->Abbreviation)){echo $user->Abbreviation;} ?>">
					</div>
					<br class="clear">
					<div class="row">
						<h4>Project Start Date</h4>
						<input type="text" name="project_start_date" id="project_start_date" value="<?php if(isset($user->project_start_date)){echo date_format_helper($user->project_start_date);} ?>">
					</div>
					<div class="row">
						<h4>Project Completion Date</h4>
						<input type="text" name="project_end_date" id="project_end_date" value="<?php if(isset($user->project_end_date)){echo date_format_helper($user->project_end_date);} ?>">
					</div>
					<br class="clear">
					<div class="row">
						<h4>Donor</h4>
						<input type="text" name="donor" value="<?php if(isset($user->donor)){echo $user->donor;} ?>">
					</div>
					<div class="row">
						<h4>Project Duration</h4>
						<input type="text" name="duration" value="<?php if(isset($user->duration)){echo $user->duration;} ?>">
					</div>
					<br class="clear">
					<div class="row">
						<h4>Project Status</h4>
						<!--<input type="text" name="project_status"
							   value="<?php /*if(isset($user->project_status)){echo $user->project_status;} */?>">-->
						<select name="project_status_type_id">
							<option value="1"<?php if(isset($user->project_status_type_id)){if($user->project_status_type_id == 1){echo "selected='selected'";}}?>>Started</option>
							<option value="1"<?php if(isset($user->project_status_type_id)){if($user->project_status_type_id == 2){echo "selected='selected'";}}?>>In Progress</option>
							<option value="1"<?php if(isset($user->project_status_type_id)){if($user->project_status_type_id == 3){echo "selected='selected'";}}?>>Stopped</option>
							<option value="1"<?php if(isset($user->project_status_type_id)){if($user->project_status_type_id == 4){echo "selected='selected'";}}?>>Completed</option>
						</select>
					</div>
                    <br class="clear">
					<div class="row">
						<h4>Program</h4>
                        <?php
                        $SelectedProgram =(isset($user->ProgramID) ? $user->ProgramID : '');
                        //var_dump($SelectedProgram);return;
                        echo form_dropdown('Program',$Program,$SelectedProgram,'required="required"','required="required"'); ?>

                    </div>
				<div class="row">
					<h4>Type</h4>
                <select name="enabled">
                  <option value="1"<?php if(isset($user->enabled)){if($user->enabled == 1){echo "selected='selected'";}}?>>Enabled</option>
                    <option value="0"<?php if(isset($user->enabled)){if($user->enabled == 0){echo "selected='selected'";}}?>>Disabled</option>
                    
                    </select>
                </div>
				<!-- button group -->
			<div class="row">
           <input type="submit" name="edit" value="Update" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>


			<!-- table -->
            
            
             
			
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->
			</div>

			

		</div>

	</div>
<!-- contents -->
<!-- Menu left side  -->
<div id="right-panel" class="panel">
    <?php $this->load->view('includes/hr_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#dept, #design').select2();
    });
    $( "#project_start_date,#project_end_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true
    });
</script>
<!-- leftside menu end -->


