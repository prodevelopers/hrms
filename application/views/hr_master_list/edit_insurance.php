<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 12/27/2014
 * Time: 12:22 PM
 */
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Configuration / Edit Insurance Type</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_master_list_nav'); ?>

<div class="right-contents" style="width:70%">
    <div class="head">Master Lists</div>
    <div class="right-content">
        <form action="" method="post">
            <div class="row">
                <h4><b>Insurance Type</b></h4>
            </div>
            <br class="clear">
            <div class="row">
                <h4>Update Type</h4>
                <input type="text" name="insuranceType" id="insuranceType" value="<?php echo isset($selectedInsurance) ? $selectedInsurance->InsuranceTypeName:''?>">
            </div>
            <div class="row">
                <select name="enabled" id="enableStatus">
                    <option value="1"<?php if(isset($user->enabled)){if($user->enabled == 1){echo "selected='selected'";}}?>>Enabled</option>
                    <option value="0"<?php if(isset($user->enabled)){if($user->enabled == 0){echo "selected='selected'";}}?>>Disabled</option>

                </select>
            </div>
            <!-- button group -->
            <div class="row">
                <input type="button" name="edit" value="Update" id="updateButton" class="btn green" />
                <!--<a href="add.php"><button class="btn green">Add</button></a>-->
            </div>

        </form>
        <!-- table -->
    </div>



</div>

</div>
<!-- contents -->
<script>
    $(document).ready(function (e) {
       $('#updateButton').on('click', function () {
           var formData = {
               insuranceType: $('#insuranceType').val(),
               insuranceID: '<?php echo $selectedInsurance->InsuranceTypeID; ?>',
               enabled: $('#enableStatus').val()
           };
           $.ajax({
                url: "<?php echo base_url(); ?>hr_site/update_insurance_type",
               data: formData,
               type: "POST",
               success:function(output){
                   var data = output.split('::');
                   if(data[0] == "OK"){
                       Parexons.notification(data[1],data[2]);
                       window.location = '<?php echo base_url(); ?>hr_site/insurance';
                   }else if(data[0] == "FAIL"){
                       Parexons.notification(data[1],data[2]);
                   }
               }
           });
       });
    });
</script>


