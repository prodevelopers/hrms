<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
<?php $this->load->view('includes/hr_master_list_nav'); ?>	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>
		<div class="right-content">
				<form action="hr_site/add_employment_type" method="post">

				<div class="row">
					<h4><b>Employment Type</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Employment Type</h4>
					<input type="text" name="employment_type" value="" required>
				</div>
				
				<!-- button group -->
			<div class="row">
						<input type="submit" class="btn green" value="Add" name="add" />
			</div>
				
			</form>


			<!-- table -->
	 	<table cellspacing="0">
				<thead class="table-head">
				<!--	<td><input type="checkbox"></td>-->
					<td>Employment Type</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if (!empty($data)){?>
                  <?php foreach($data as $employment):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo $employment->employment_type;?></td>
                     <?php if($employment->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
					<td><a href="hr_site/edit_employement_type/<?php echo $employment->employment_type_id;?>" /><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_employee/<?php echo $employment->employment_type_id;?>/<?php echo 'employment_type_id';?>" onclick="return confirm('Are You Sure.....?')"/><span class="fa fa-trash-o"></span></td>
				</tr>
				<?php endforeach;?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
			</table>
			</div>

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
</script>

