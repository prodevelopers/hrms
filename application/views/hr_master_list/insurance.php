<div class="contents-container">

    <div class="bredcrumb">Dashboard / Insurance</div>
    <!-- bredcrumb -->

    <?php $this->load->view('includes/hr_master_list_nav'); ?>

    <div class="right-contents" style="width:70%">

        <div class="head">Master Lists</div>

        <div class="right-content">
            <form action="hr_site/add_branch" method="post">

                <div class="row">
                    <h4><b>Manage Insurance Types</b></h4>
                </div>
                <br class="clear">

                <div class="row">
                    <h4>Insurance Type</h4>
                    <input type="text" name="insurance" value="" id="insuranceType" required>
                </div>

                <!-- button group -->
                <div class="row">
                    <input type="button" name="add" value="Add" id="addButton" class="btn green"/>
                    <!--<a href="add.php"><button class="btn green">Add</button></a>-->
                </div>

            </form>


            <!-- table -->
            <table cellspacing="0">
                <thead class="table-head">
                <td>Insurance Type</td>
                <td>Status</td>
                <td>Edit</span></td>
                <td>Delete</td>
                </thead>

                <?php
                if(!empty($InsuranceTypes)){
                foreach ($InsuranceTypes as $key => $value) {
                    echo "<tr class='table-row' data-id='$value->InsuranceTypeID'>";
                    echo "<td>$value->InsuranceTypeName</td>";
                    echo "<td>" . ($value->Status == 1 ? "Enabled" : "Disabled") . "</td>";
                    echo "<td><a style='cursor: pointer' class='editInsurance'><span class=\"fa fa-pencil\"></span></a></td>";
                    echo "<td><a style='cursor: pointer' class='trashInsuranceType'><span class=\"fa fa-trash-o\"></span></a></td>";

                } }else{}?>
            </table>
        </div>

    </div>

</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.redirect.js"></script>
<script>
    $(document).ready(function (e) {
        $('#addButton').on('click', function (e) {
            var insuranceType = $('#insuranceType').val();
            //We First Need To Check If the data Do Not Already Exist in the Database for the Same Name.
            if (insuranceType.trim()) {
                console.log(insuranceType);
                var addInsuranceTypeURL = "<?php echo base_url(); ?>hr_site/addInsuranceType";
                var postData = {
                    insuranceType: insuranceType
                };
                $.ajax({
                    url: addInsuranceTypeURL,
                    type: 'POST',
                    data: postData,
                    success: function (output) {
                        var data = output.split('::');
                        if (data[0] == 'OK') {
                            Parexons.notification(data[1], data[2]);
                            $.redirect(addInsuranceTypeURL, postData, 'POST');
                        } else if (data[0] == 'FAIL') {
                            Parexons.notification(data[1], data[2]);
                        }
                    }
                });
            }
        });
        $('.editInsurance').on('click', function (output) {
            var insuranceID = $(this).parents('tr').attr('data-id');
            if(insuranceID.length>0){
                var editURL = '<?php echo base_url(); ?>hr_site/edit_insurance_type';
                var data = {
                    insuranceID: insuranceID
                };
                $.redirect(editURL,data,'POST');
            }
        });
        $('.trashInsuranceType').on('click', function (output) {
            var insuranceID = $(this).parents('tr').attr('data-id');
            if(insuranceID.length>0){
                var deleteURL = '<?php echo base_url(); ?>hr_site/trash_insurance_type';
                var data = {
                    insuranceID: insuranceID
                };
                $.ajax({
                    url:deleteURL,
                    data:data,
                    type:"POST",
                    success:function(output){
                        var data = output.split("::");
                        if(data[0] == "OK"){
                            Parexons.notification(data[1],data[2]);
                            location.reload();
                        }else if(data[0] == "FAIL"){
                            Parexons.notification(data[1],data[2]);
                        }
                    }
                });
            }
        });
    });
</script>