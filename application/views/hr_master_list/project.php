<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_master_list_nav'); ?>	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>
                <div class="right-content">
				<form action="hr_site/add_project" method="post">

				<div class="row">
					<h4><b>Project</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Project Name</h4>
					<input type="text" name="project_title" value="" required>
				</div>

					<div class="row">
						<h4>Project Abbreviation</h4>
						<input type="text" name="Abbreviation" value="" required>
					</div>
					<br class="clear">
					<div class="row">
						<h4>Project Start Date</h4>
						<input type="text" name="project_start_date" id="project_start_date" required>
					</div>

					<div class="row">
						<h4>Project End Date</h4>
						<input type="text" name="project_end_date" id="project_end_date" required>
					</div><br class="clear">
					<div class="row">
						<h4>Donor</h4>
						<input type="text" name="donor" value="" required>
					</div>

					<div class="row">
						<h4>Project Duration</h4>
						<input type="text" name="duration" value="" required>
					</div>
					<br class="clear">
					<div class="row">
						<h4>Project Status</h4>
						<select name="project_status">
							<option value=" " >--Select Status--</option>
							<option value="1"> Started </option>
							<option value="2"> In Progress </option>
							<option value="3"> Stopped </option>
							<option value="4"> Completed </option>
						</select>

					</div>

                    <br class="clear">
                    <div class="row">
                        <h4>Program</h4>
                        <?php
                        echo @form_dropdown('Program',$Program,'','id="drop_title" required="required"','required="required",required="required"'); ?>

                    </div>
				
				<!-- button group -->
			<div class="row">
            <input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>


			<!-- table -->
            
            
             <table cellspacing="0">
				<thead class="table-head">
					
					<td>Project</td>
					<td>Program</td>
                    <td>Abbreviation</td>
					<td>Project Start Date</td>
					<td>Project End Date</td>
					<td>Donor</td>
					<td>Duration</td>
					<td>Project Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if (!empty($data)){?>
                 <?php foreach($data as $project):?>
				<tr class="table-row">
					
					<td><?php echo $project->project_title; ?></td>
					<td><?php echo $project->Program; ?></td>
					<td><?php echo $project->Abbreviation; ?></td>
					<td><?php echo date_format_helper($project->project_start_date); ?></td>
					<td><?php echo date_format_helper($project->project_end_date); ?></td>
					<td><?php echo $project->donor; ?></td>
					<td><?php echo $project->duration; ?></td>
                      <td><?php echo $project->status_type_title; ?></td>
					<td><a href="hr_site/edit_project/<?php echo $project->project_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_project/<?php echo $project->project_id;?>/<?php echo 'project_id';?>" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach; ?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
				
			</table>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->
			</div>

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');

    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    });
    $( "#project_start_date,#project_end_date" ).datepicker({ dateFormat: "dd-mm-yy", timeFormat: "HH:MM",
        changeMonth: true,
        changeYear: true,
        yearRange: "<?php echo yearRang(); ?>"

    });
</script>

