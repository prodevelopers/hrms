<?php //include('includes/header.php'); ?>
<?php
if(isset($data) && $data != NULL && !empty($data)){

    $organizationID = $data[0]->organization_id;
    $name = $data[0]->org_name;
    $phone = $data[0]->phone;
    $city = $data[0]->city;
    $country = $data[0]->country;
    $email = $data[0]->email;
    $mobile = $data[0]->mobile;
    $address = $data[0]->address;
    $website = $data[0]->website;
}else{
    $organizationID = $name = $phone = $email = $mobile = $address = $city = $country = $website = '';
}
?>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_master_list_nav'); ?>	

	<div class="right-contents" style="width:70%">

		<div class="head">Add Organisation Info</div>
			<form action="<?php base_url() ?>hr_site/create_update_organization" method="post" id="organizationForm">
				<div class="row">
					<h4>Organisation Name</h4>
					<input type="text" value="<?php echo $name?>" name="o_name" id="o_name">
				</div>
				<br class="clear">
				<div class="row">
					<h4>City</h4>
					<input type="text" value="<?php echo $city?>" name="o_city" id="o_city">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Country</h4>
					<input type="text" value="<?php echo $country?>" name="o_country" id="o_country">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Phone</h4>
					<input type="text" value="<?php echo $phone?>" name="o_phone" id="o_phone">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Email</h4>
					<input type="text" value="<?php echo $email?>" name="o_email" id="o_email">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Website</h4>
					<input type="text" value="<?php echo $website?>" name="o_website" id="o_website">
				</div>
				<br class="clear">
				<div class="row">
					<h4>Address</h4>
					<textarea name="o_address" id="o_address"><?php echo $address?></textarea>
				</div>
                <div class="row">
                    <h4>Attendance Type</h4>
                    <?php
                    $slctd_attend = (isset($data[0]->ConfigID) ? $data[0]->ConfigID  : '');
                    echo form_dropdown('ConfigTypes', $attendance,$slctd_attend,'required="required"');?>
                </div>
			</form>
			<div class="row">
				<div class="button-group">
					<a id="saveButton"><button class="btn green">Save Info</button></a>
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#saveButton').on('click', function (e) {
            var formD = $('#organizationForm').serialize();
            $.ajax({
                url: '<?php base_url() ?>hr_site/create_update_organization',
                data:formD,
                type:'POST',
                success: function (output) {
                    var data = output.split('::');
                    if(data[0]=='OK'){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0]=='FAIL'){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            }) ;
        });
    });
</script>

<?php //include('includes/footer.php'); ?>
