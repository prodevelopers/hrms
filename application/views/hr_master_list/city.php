<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

			<?php $this->load->view('includes/hr_master_list_nav'); ?>	
	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>
		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>

			

			<div class="right-content">
				<form action="hr_site/add_city" method="post">
				<div class="row">
					<h4><b>City / Village</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add City / Village</h4>
					<input type="text" name="city_name" value="" required>
				</div>
				
				<!-- button group -->
			<div class="row">
              <input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>


			<!-- table -->
            
             <table cellspacing="0">
				<thead class="table-head">
					
					<td>City / Village</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if (!empty($data)){?>
                 <?php foreach($data as $city):?>
				<tr class="table-row">
					
					<td><?php echo $city->city_name; ?></td>
                    <?php if($city->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
					<td><a href="hr_site/edit_city/<?php echo $city->city_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_city/<?php echo $city->city_id;?>/<?php echo 'city_id';?>" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach; ?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
				
			</table>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>City / Village</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>City / Village</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->
			</div>

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');

    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    })


</script>

