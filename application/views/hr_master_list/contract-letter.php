<?php
if(isset($organizationData)){
    $orgName = $organizationData->org_name;
    $orgAddress = $organizationData->address;
    $orgCity = $organizationData->city;
    $orgCountry = $organizationData->country;
    $orgPhone = $organizationData->phone;
    $orgEmail = $organizationData->email;
    $orgWebsite = $organizationData->website;
}
else{
    $orgName = 'Company Name';
    $orgAddress = 'Company Address';
    $orgCity = 'City';
    $orgCountry = 'Country';
    $orgPhone = 'Company Phone';
    $orgEmail = 'Company Email';
    $orgWebsite = 'Company Website';
}
if(isset($employeeData) && !empty($employeeData)){
    $eFullName = $employeeData->FullName;
    $eAddress = $employeeData->Address;
    $ePosition = $employeeData->Position;
    $eStartDate = $employeeData->StartDate;
    $eBaseSalary = $employeeData->BaseSalary;
}else{
    $eFullName = 'Employee Name';
    $eAddress = 'Employee Address';
    $ePosition = 'Employee Position';
    $eStartDate = 'Employee Join Date';
    $eBaseSalary = 'Employee Base Salary';
}
?>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
<style>
    @media print {
        #printButton {
            display: none;
        }
    }
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
body{
	background: #f8f8f8;
}
.invoice-header {
  margin: 0 0 10px;
  border-bottom: 1px dashed #B6B6B6;
  display: inline-block;
}
.row{
}

.box {
  display: block;
  z-index: 1999;
  position: relative;
  border: 1px solid #f8f8f8;
  box-shadow: 0 0 4px #D8D8D8;
  background: transparent;
  margin-bottom: 20px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
}
.box-content {
  position: relative;
  -webkit-border-radius: 0 0 3px 3px;
  -moz-border-radius: 0 0 3px 3px;
  border-radius: 0 0 3px 3px;
  padding: 15px;
overflow:hidden;
min-height:200px;
  background: #FCFCFC;
}
 .box-content {
  width: 100%;
  left: 0;
  top: 0;
}
.text-body p{
	line-height:26px;
	text-align: justify;
}
</style>
<div class="row">
	<div class="col-xs-9">
		<div class="box">
			<div class="box-content">
				<div class="col-sm-offset-1 col-xs-10">
					<address>
						<strong><?php echo $eFullName; ?></strong><br>
						<?php echo $eAddress; ?><br>
						Peshawar KPK Pakistan<br>
						<abbr title="">Date:</abbr><?php echo date('l jS \of F Y');?><br><br>
						<strong>Subject </strong> Appointment letter<br>
					</address>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-offset-1 col-xs-10"><b>Dear </b><?php echo $eFullName; ?></div>
				<div class="clearfix"></div>
				<br>
				<div class="col-sm-offset-1 col-xs-10 text-body">
<!--					<p>I am writing to confirm my offer of a position at as a. The hours will be per week daily.
					This position is offered subject to satisfactory reference and pre-employment checks and 
					completion of the six-month probationary period during which time your performance will 
					be reviewed.</p>
					<p>This is a permanent position and you will therefore be entitled to all staff benefits.  
					Your starting date will be . You will be paid at a rate of per hour. Your salary will be
					paid directly into your bank account on the of each month. 
					You will be entitled to days holiday per year pro-rata, plus Bank Holidays.
					The Holiday year runs from Jan 1st - Dec 31st.</p>
					<p>Please find enclosed clearance forms which I would be grateful if you could
					complete and return to me as soon as possible.
					We are all looking forward to working with you and hope you will soon feel part of the team. If you have any questions, please contact me.</p>-->
                    <?php echo $letterData->LetterText; ?>
                </div>
				<div class="col-sm-offset-1 col-xs-10 text-right">
				<br>
					<strong>Your Sincerely</strong><br>
					<abbr><?php echo $loggedEmployee->FullName; ?></abbr><br>
					<p>&nbsp;</p>
				</div>
                <button id="printButton" onClick="window.print()">Print</button>
			</div>
		</div>
	</div>
</div>
