<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/hr_master_list_nav'); ?>

    <div class="right-contents" style="width:70%">

        <div class="head">Master Lists</div>



        <div class="right-content">
				<form action="hr_site/add_job_title" method="post">
				<div class="row">
					<h4><b>Job Title</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Job Title</h4>
					<input type="text" name="job_title" value="" required>
				</div>
				
				<!-- button group -->
			<div class="row">
            <input type="submit" class="btn green" value="Add" name="add" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>

				<style type="text/css">
				.result{
					position:relative;
					top:105px;
					left:20px;
					font-size:18px;
					width:20%;
					height:100px;
					}
					</style>
			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<!--<td><input type="checkbox"></td>-->
					<td>Job Title</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                 <?php if ($data >0){?>
                <?php foreach($data as $job_title):?>
				<tr class="table-row">
					<!--<td><input type="checkbox"></td>-->
					<td><?php echo $job_title->job_title; ?></td>
                     <?php if($job_title->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
					<td><a href="hr_site/edit_job_title/<?php echo $job_title->ml_job_title_id; ?>" /><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_job_title/<?php echo $job_title->ml_job_title_id; ?>/<?php echo 'ml_job_title_id';?>" onclick="return confirm('Are You Sure.....?')" /><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach;?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
			</table>
			</div>

			

		</div>

	</div>
<!-- contents -->


