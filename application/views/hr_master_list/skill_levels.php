<script>
    $(document).ready(function(){
        <?php $msg=$this->session->flashdata('msg');
        if(isset($msg) && !empty($msg)){?>
        $("#msg").show().delay(3000).fadeOut();
        <?php }?>
    });
</script>
<div class="contents-container">
<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
    <?php $this->load->view('includes/hr_master_list_nav'); ?>
<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>
<div style="display: none;" id="msg"><?php echo $this->session->flashdata('msg');?></div>
    <div class="right-content">
				<form action="hr_site/add_skill_level" method="post">
				<div class="row">
					<h4><b>Skill Level</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Skill Level</h4>
					<input type="text" name="level" required="required">
				</div>
				<!-- button group -->
			<div class="row">
				<input type="submit" name="submit" value="Add" class="btn green">
			</div>
				
			</form>
<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					<!--<td><input type="checkbox"></td>-->
					<td>Level</td>
					<td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if(!empty($data)){
                    foreach($data as $rec){?>
				<tr class="table-row">
			<!--		<td><input type="checkbox" value="<?php /*echo $rec->level_id;*/?>"></td>-->
					<td><?php echo $rec->skill_level;?></td>
					<td><?php if(isset($rec->enabled) && $rec->enabled == 1){echo "Enabled";}else{echo "Disabled";}?></td>
					<td><a href="hr_site/edit_skill_level/<?php echo $rec->level_id;?>" ><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_skill_level/<?php echo $rec->level_id;?>/<?php echo "ml_skill_level"; ?>" ><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php }}?>
			</table>
			</div>
</div>
    </div>


