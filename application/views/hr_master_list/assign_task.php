<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->
	<?php $this->load->view('includes/hr_master_list_nav'); ?>	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
				<form action="hr_site/add_task" method="post">

				<div class="row">
					<h4><b>Assign Task</b></h4>
				</div>
                    <br class="clear">
                    <div class="row">
                        <h4>Project</h4>
                       <select name="project" required="required">
                           <option value="">
                                -- Select Project --
                           </option>
                           <?php foreach($project as $projects){?>
                           <option value="<?php echo $projects->project_id; ?>">
                               <?php echo $projects->project_title; ?>
                           </option>
                           <?php }?>
                       </select>
                    </div>

                    <br class="clear">
				<div class="row">
					<h4>Add Task</h4>
					<input type="text" name="task_name" value="" required>
				</div>
				
				<!-- button group -->
			<div class="row">
            <input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>

			
			<!-- table -->
			<table cellspacing="0">
				<thead class="table-head">
					
					<td>Task</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                 <?php if ($data >0){ 
				 foreach($data as $asign_task):?>
				<tr class="table-row">
					
					<td><?php echo $asign_task->task_name; ?></td>
                    <?php if($asign_task->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
					<td><a href="hr_site/edit_task/<?php echo $asign_task->ml_assign_task_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_task/<?php echo $asign_task->ml_assign_task_id;?>/<?php echo 'ml_assign_task_id';?>" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
               
				<?php endforeach; ?>
                 <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
				
			</table>
			</div>

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');
    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    })
</script>

