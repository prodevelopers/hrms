<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Human Resource</div> <!-- bredcrumb -->

<?php $this->load->view('includes/hr_master_list_nav'); ?>	

	<div class="right-contents" style="width:70%">

		<div class="head">Master Lists</div>

		<div id="alert" style="background-color: red; color: #ffffff; text-align: center; font-weight: bold;">
			<?php //echo  $this->session->flashdata('msg');?></div>


		<div class="right-content">
				<form action="hr_site/add_gender" method="post">

				<div class="row">
					<h4><b>Gender</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Add Gender</h4>
					<input type="text" name="gender_type_title" value="" required>
				</div>
				
				<!-- button group -->
			<div class="row">
            <input type="submit" name="add" value="Add" class="btn green" />
					<!--<a href="add.php"><button class="btn green">Add</button></a>-->
			</div>
				
			</form>


			<!-- table -->
            
            <table cellspacing="0">
				<thead class="table-head">
					
					<td>Gender</td>
                    <td>Status</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
                <?php if (!empty($data)){?>
                 <?php foreach($data as $gender):?>
				<tr class="table-row">
					
					<td><?php echo $gender->gender_type_title; ?></td>
                    <?php if($gender->enabled == 1): ?>
                <td><?php echo "Enabled"; ?></td>
                <?php else : ?>
                <td><?php echo "Disabled"; ?></td>
                <?php endif;?>
					<td><a href="hr_site/edit_gender/<?php echo $gender->gender_type_id;?>"><span class="fa fa-pencil"></span></a></td>
					<td><a href="hr_site/trashed_gender/<?php echo $gender->gender_type_id;?>/<?php echo 'gender_type_id';?>" onclick="return confirm('Are You Sure.....?')"><span class="fa fa-trash-o"></span></a></td>
				</tr>
				<?php endforeach; ?>
                <?php } else { echo "<div class=\"result\">No record found..</div>";} ?>
				
			</table>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td><input type="checkbox"></td>
					<td>Gender</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</thead>
				<tr class="table-row">
					<td><input type="checkbox"></td>
					<td>Gender</td>
					<td><span class="fa fa-pencil"></span></td>
					<td><span class="fa fa-trash-o"></span></td>
				</tr>
				
			</table>-->
			</div>

			

		</div>

	</div>
<!-- contents -->
<script>
	$("#alert").delay(3000).fadeOut('slow');



    $(document).ready(function(e){

        //Load Page Messages
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
        echo "var message;";
        foreach($pageMessages as $key=>$message){
            if(!empty($message) && isset($message)){
                    echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
    })
</script>

