

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Group Privilegies</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Group Restrication</div>
        
			<table cellspacing="0">
				<thead class="table-head">
					<td>Module</td>
					<td>View</td>
					<td>Modify</td>
					<td>Create New</td>
					<td>Print</td>
                    <td>Edit</td> 
				</thead>
                
                <form action="" method="post">
                <?php if (!empty($join)){?>
			<?php foreach($join as $record):
		//echo "<pre>"; print_r($data); die;
			?>
            
				<tr class="table-row">
					<td><?php echo $record->module_name; ?></td>
					<td><input type="checkbox" name="view_only"  value="1"<?php if($record->view_only == '1')
			echo "checked='checked' ";?> ></td>
					<td><input type="checkbox" name="modify" value="1"<?php if($record->modify == '1'){echo "checked='checked' ";} ?> ></td>
					<td><input type="checkbox" name="create_new_record"  value="1"<?php if($record->create_new_record == '1'){echo "checked='checked' "; }?>></td>
					<td><input type="checkbox" name="print"  value="1"<?php if($record->print == '1'){echo "checked='checked' ";}?>></td>
                  
                    <td><a href="user_site/update_priviligies/<?php echo $record->privilege_id;?>"><span class="fa fa-pencil"></span></a></td>
				</tr>
               
              <?php endforeach;?> 
            <?php } else { echo " <span style='color:#F00'>No record found..</span>";}?> 
             </form>
				<!--<tr class="table-row">
					<td>Leave & Attendence</td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
				</tr>
				<tr class="table-row">
					<td>HR Master List</td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>
					<td><input type="checkbox"></td>-->
			</table>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                
             
					<!--<a href="#"><button class="btn green">Save Changes</button></a>-->
					<!-- <button class="btn red">Delete</button> -->
                    
				</div>
			</div>    

		</div>
       

	</div>
<!-- contents -->

    <div id="right-panel" class="panel">
    <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->