

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / User Privilegies</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	
	<div class="right-contents">
		<div class="head">User Restrication</div>
        <form action="" method="post">
			<table cellspacing="0">
			
				<tr class="table-row">
					<td width="120">View Only</td>
					<td><input type="checkbox" name="view_only" value="1" <?php if($join->view_only == '1')
			echo "checked='checked' ";?>></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="table-row">
					<td width="120">Modify</td>
					<td width="120"><input type="checkbox" name="modify" value="1" <?php if($join->modify == '1')
			echo "checked='checked' ";?>></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="table-row">
					<td width="120">Create New</td>
					<td><input type="checkbox" name="create_new_record" value="1" <?php if($join->create_new_record == '1')
			echo "checked='checked' ";?>></td>
					<td></td>
					<td></td>
				</tr><tr class="table-row">
					<td width="120">Print</td>
					<td><input type="checkbox" name="print" value="1" <?php if($join->print == '1')
			echo "checked='checked' ";?>></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="edit" value="Save Changes" class="btn green" />
					<!--<a href="#"><button class=""></button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
            </form>

		</div>

	</div>
<!-- contents -->

    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
