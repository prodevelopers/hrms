<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
.dataTables_length{
	position: relative;
	top: -20px;
	/*font-size: 1px;*/
	font-size:0.3px;
	/*left: 92.5%;*/
}

.dataTables_filter{
	position: relative;
	top: -20px;
	left: -28em;
}
#pagination a
{
	padding:5px;
	background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter{
    position: relative;
    top: -21px;
    left: -19%;
}
.marge{
    margin-top: 40px;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var oTable = $('#table_list').dataTable({
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sDom" : '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>',
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"user_name","value":$('#user_name').val()});
			aoData.push({"name":"user_group_title","value":$('#user_group_title').val()});
		}
	});

    $('#tableSearchBox').keyup(function(){
        oTable.fnFilter( $(this).val() );
    });
	$('tbody').addClass('table-row');
});

$(".filter1").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Users Management</div> <!-- bredcrumb -->

 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

	<div class="right-contents">

		<div class="head">Users Management</div>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
                <?php echo form_open();?>
                <input type="text" id="tableSearchBox" placeholder="Search Employees" style="width: 30%;">
                <?php echo form_dropdown('user_group_title',$user_group_title,$group,"style='width:220px;' class='select_47 filter1' id='user_group_title' onChange='this.form.submit()'");?>
                <?php echo form_close();?>
            </div>



			<!-- table -->
			<table cellpadding="0" cellspacing="0" id="table_list"  class="marge">
                   <thead class="table-head">
                   <tr id="table-row">
                    <td width="20%">Employee Code</td>   
					<td width="20%">Employee Name</td>
					<td width="20%">User Name</td>
					<td width="20%">User Role</td>
					<td width="20%">Status</td>
					<td ><span class="fa fa-pencil"></span></td>
                    </tr>
                </thead>
                    <tbody>
                    </tbody>
               </table>

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="human_resource/add_employee_acct"><button class="btn green">Add User</button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
			
            
		</div>

	</div>
<!-- contents -->

<!-- Menu left side  -->
    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    $(document).ready(function(e){
        $('#user_group_title').select2();
    });
    </script>

    <!-- leftside menu end -->