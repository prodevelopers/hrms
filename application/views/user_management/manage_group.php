<?php /**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 1/5/2015
 * Time: 9:49 AM
 *
 */
?>
<style>
    .move {
        position: relative;
        left: 48%;
        top: 60px;
    }

    select[name="employee_code"], select[name="user_group_title"] {
        width: 220px;
    }

    .dataTables_length {
        position: relative;
        top: -20px;
        font-size: 1px;
    }

    .dataTables_filter {
        position: relative;
        top: -21px;
        left: -10.6%;
    }

    .marge {
        margin-top: 60px;
    }
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }

    .paginate_button
    {
        padding: 6px;
        background-image: url(assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
</style>
<!-- contents -->
<div class="contents-container">
    <div class="bredcrumb">Dashboard / User Group Management</div>
  <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
   <div class="right-contents">
        <div class="head">User Group Management</div>
        <!-- filter -->
        <?php echo form_open(); ?>
        <div class="filter">
            <div>
                <h4>Filter By</h4>
                <input type="text" placeholder="User Name" style="width: 200px;">
            <?php //echo form_dropdown('employee_code',$employee_code); ?>
                <!--<h4>Module Name: </h4>-->
                <input type="hidden" id="selectModule" name="selectModule">
            <?php //echo form_dropdown('user_group_title',$user_group_title); ?>
             <!--<h4>User Group: </h4>-->
                <input type="hidden" id="selectUserGroup" name="selectUserGroup">
            </div>
            <!-- <div class="button-group">
                <button class="btn green">Search</button>
                <button class="btn gray">Reset</button>
            </div> -->
        </div>
        <?php echo form_close(); ?>
        <!-- table -->

            <table cellspacing="0" id="listUsers">
                <thead class="table-head">
                <td>Employee ID</td>
                <td>Employee Code</td>
                <td>Employee Name</td>
                <td>User Group</td>
                <td>User Name</td>
                </thead>
                <tbody></tbody>
            </table>
        <a href="user_site/user_group_add_pri/<?php //echo $record->user_group_id;?>">
            <button class="btn green" style="margin-left: 1em;">Manage Priviliges</button>
        </a>
    </div>
    <!-- button group
    <div class="row">
        <div class="button-group">
            <a href="user_group.php"><button class="btn green">Create New</button></a>
             <button class="btn red">Delete</button>
        </div> -->
</div>
</div>
</div>
<!-- contents -->
<!--<script type="text/javascript" src="js/jquery-ui.js"></script>-->
<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
    var oTable;
$(document).ready(function(e){
    oTable = '';
    //First Lets Show the Selectors..
    var selectModuleSelector = $('#selectModule');
    var url = "<?php echo base_url(); ?>user_site/loadAvailableModules";
    var id = "ModuleID";
    var text = "ModuleName";
    var minInputLength = 0;
    var placeholder = "Select Module";
    var multiple = false;
    commonSelect2(selectModuleSelector, url, id, text, minInputLength, placeholder, multiple);

    //First Lets Show the Selectors..
    var selectUserSelector = $('#selectUserGroup');
    var url = "<?php echo base_url(); ?>user_site/loadAllAvailableGroups";
    var id = "UserGroupID";
    var text = "UserGroupName";
    var minInputLength = 0;
    var placeholder = "Select User Group";
    var multiple = false;
    commonSelect2(selectUserSelector, url, id, text, minInputLength, placeholder, multiple);

    ///Functions To Work When Some Change Comes To Selectors..
    selectModuleSelector.on('change', function (e) {
        $('#selectUserGroup').select2('destroy');
        var dependentGroupNameWhere = $(this).select2('data');
        commonSelect2Depend(selectUserSelector,url,id,text,minInputLength,placeholder,multiple,dependentGroupNameWhere);
        $('.select2-container').css("width","223px");
        $('.select2-container').css("display","inline-block");
    });
    selectUserSelector.on('change', function (e) {
        //Load DataTables When Some Value is Selected..
        var dataTablesSelector = $('#listUsers');
        var url = "<?php echo base_url(); ?>user_site/listUsers_DT";
        var aoColumns = [
            /* ID */ {
                "mData": "EmployeeID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Employee Name */ {
                "mData" : "EmployeeCode"
            },
            /* Employee Name */ {
                "mData" : "EmployeeName"
            },
            /* Designation */ {
                "mData" : "EmployeeUserGroup"
            },
            /* Project */ {
                "mData" : "EmployeeUserName"
            }
        ];
        var UserGroupID = e.val;
        var HiddenColumnID = 'EmployeeID';
        var sDom = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        var filters = 'aoData.push({"name":"userGroup","value":'+JSON.stringify(UserGroupID)+'});  aoData.push({"name":"appModule","value":$("#selectExperience").val()});';
        commonDataTablesFiltered(dataTablesSelector,url,aoColumns,sDom,HiddenColumnID,filters);
    });

    //Select2 Common With For All..
    $('.select2-container').css("width","223px");
});
</script>

    <div id="right-panel" class="panel">
        <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->