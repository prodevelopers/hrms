<style>
	.move{
	position: relative;
	left: 48%;
	top: 60px;
}
select[name="employee_code"],select[name="user_group_title"]{
	width: 220px;
}

.dataTables_length{
	position: relative;
	top: -20px;
	font-size: 1px;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -10.6%;
}

.marge{
	margin-top: 60px;
}
</style>
<!-- contents -->
<div class="contents-container">
<div class="bredcrumb">Dashboard / User Group Management</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">User Group Management</div>
			<!-- filter -->
            
        <?php echo form_open(); ?>
			<div class="filter">
				<!--<h4>Filter By</h4>
				<input type="text" placeholder="User Name" class="resize">-->
                <?php   //echo form_dropdown('employee_code',$employee_code); ?>
				<!--<select>
					<option>Group ID</option>
				</select>-->
                 <?php   //echo form_dropdown('user_group_title',$user_group_title); ?>
                 
				<!--<select>
					<option>Group Name</option>
				</select>-->

				<!-- <div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div> -->
			</div>
        <?php echo form_close(); ?>
			<!-- table -->
            
			<div id="accordion" class="marge">
				<h4 class="head"><?php
					echo "Admin";
					?></h4>
				<table cellspacing="0">
					<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>User Group</td>
					<td>User Name</td>
					</thead>
			<?php if (!empty($join)){$countadm=0;?>
			<?php foreach($join as $record):
					if($record->user_group_title=="Admin"){
			?>
				<tr class="table-row">
               
					<td><?php  echo $record->employee_code;?></td>
                    <td><?php  echo $record->full_name;?></td>
                    <td><?php  echo $record->user_group_title;?></td>
                    <td><?php  echo $record->user_name;?></td>

				</tr>


              <?php }endforeach;?>
            <?php } else { echo " <span style='color:#F00'>No record found..</span>";}?>

			</table>

				<h4 class="head"><?php
					echo "HRM";
					?></h4>
				<table cellspacing="0">
					<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>User Group</td>
					<td>User Name</td>
					</thead>
					<?php if (!empty($join)){$countadm=0;?>
						<?php foreach($join as $record):
							if($record->user_group_title=="HRM"){
								?>
								<tr class="table-row">

									<td><?php  echo $record->employee_code;?></td>
									<td><?php  echo $record->full_name;?></td>
									<td><?php  echo $record->user_group_title;?></td>
									<td><?php  echo $record->user_name;?></td>
								</tr>


							<?php }endforeach;?>
					<?php } else { echo " <span style='color:#F00'>No record found..</span>";}?>

				</table>

				<h4 class="head"><?php
					echo "Pay Roll";
					?></h4>
				<table cellspacing="0">
					<thead class="table-head">
					<td>Employee Code</td>
					<td>Employee Name</td>
					<td>User Group</td>
					<td>User Name</td>
					</thead>
					<?php if (!empty($join)){$countadm=0;?>
						<?php foreach($join as $record):
							if($record->user_group_title=="Pay Roll"){
								?>
								<tr class="table-row">

									<td><?php  echo $record->employee_code;?></td>
									<td><?php  echo $record->full_name;?></td>
									<td><?php  echo $record->user_group_title;?></td>
									<td><?php  echo $record->user_name;?></td>
								</tr>


							<?php }endforeach;?>
					<?php } else { echo " <span style='color:#F00'>No record found..</span>";}?>

				</table>

			</div>
		<a href="user_site/user_group_add_pri/<?php //echo $record->user_group_id;?>"><button class="btn green">Add Privilages</button></a>

			</div>
            
			<!-- button group
			<div class="row">
				<div class="button-group">
					<a href="user_group.php"><button class="btn green">Create New</button></a>
					 <button class="btn red">Delete</button>
				</div> -->
			</div>
		</div>
	</div>
<!-- contents -->
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
</script>

    <div id="right-panel" class="panel">
<?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->
