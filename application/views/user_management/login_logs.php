
<style>
    #pagination
    {
        float:left;
        padding:5px;
        margin-top:15px;
    }
    #pagination a
    {
        padding:5px;
        background-image:url(assets/images/top_menu_bg.png);
        color:#5D5D5E;
        font-size:14px;
        text-decoration:none;
        border-radius:5px;
        border:1px solid #CCC;
    }
    #pagination a:hover
    {
        border:1px solid #666;
    }
    /*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
    .paginate_button
    {
        padding: 6px;
        background-image: url(assets/images/top_menu_bg.png);
        color: #5D5D5E;
        font-size: 14px;
        text-decoration: none;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border: 1px solid #CCC;
        margin: 1px;
        cursor:pointer;
    }
    .paging_full_numbers
    {
        margin-top:8px;
    }
    .dataTables_info
    {
        color:#3474D0;
        font-size:14px;
        margin:6px;
    }
    .paginate_active
    {
        padding: 6px;
        border: 1px solid #3474D0;
        border-radius: 5px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        color: #3474D0;
        font-weight: bold;
    }
    .dataTables_filter
    {
        float:right;
    }
</style>
<!-- contents -->
<div class="contents-container">
<div class="bredcrumb">Dashboard / Users Management</div>
<!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">

		<div class="head">Users Login Logs</div>
        <!-- filter -->
        <form action="">
        <div class="filter">
            <div class="row2">
                <h4>Employee Name/Code</h4>
                <input type="text" placeholder="Search Employee Name/Code" id="searchUserLogs" style="width:200px;"/>
            </div>
            <div class="row2">
                <h4>Select Date</h4>
                <input type="hidden" id="selectYear"/>
                <input type="hidden" id="selectMonth"/>
                <input type="hidden" id="selectDate"/>
                <input type="button" class="green btn" value="Filter" id="filterButton">
            </div>
        </div>
       </form>
        <table class="table" id="listUsersLoginLogs" cellspacing="0">
            <thead>
                   <tr class="table-head boldness">
                        <td>Employee ID</td>
                        <td>Employee Name</td>
                       <td>Employee Code</td>
                        <td>Login Time</td>
                        <td>Logout Time</td>
                   </tr>
            </thead>
            <tbody></tbody>
            </table>
		</div>
	</div>
<!-- contents -->
<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.redirect.js"></script>
<script>
    var oTable;
    $(document).ready(function (e) {
        oTable = '';
       //DataTables Data To Show In Tables.
        var selectorTableUsersLoginLogs =  $('#listUsersLoginLogs');
        var url_DT = "<?php echo base_url(); ?>user_site/user_logs/login_logs_DT";
        var aoColumns_DT = [
            /* ID */ {
                "mData": "EmployeeID",
                "bVisible": false,
                "bSortable": false,
                "bSearchable": false
            },
            /* Employee Name */ {
                "mData" : "EmployeeName"
            },
            /* Employee Code */ {
                "mData" : "EmployeeCode"
            },
            /* LoginDate */ {
                "mData" : "LoginDate"
            },
            /* LogoutDate */ {
                "mData" : "LogoutDate"
            }
        ];
        var HiddenColumnID_DT = 'EmployeeID';
        var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
        commonDataTables(selectorTableUsersLoginLogs,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
        // Sort immediately with columns 0 and 1
        oTable.fnSort( [ [3,'desc'] ] );

        /**
         * Lets Work on Filters Of the The DataTables
         */
        //Employee Name and Employee Code Filter
        $("#searchUserLogs").keyup(function() {
            oTable.fnFilter(this.value);
        });

        //Year/Month/Date Filter
            //Using Select2 Selector for this DateFilter..
        //Year Selector
        var select2SYearelector = $('#selectYear');
        var url = "<?php echo base_url(); ?>user_site/selectAvailableLoggedInYear";
        var id = "Year";
        var text = "Year";
        var minInputLength = 0;
        var placeholder = "Select Year";
        var multiple = false;
        commonSelect2(select2SYearelector,url,id,text,minInputLength,placeholder,multiple);

        //Year Selector
        var select2MonthSelector = $('#selectMonth');
        var url = "<?php echo base_url(); ?>user_site/selectAvailableLoggedInMonths";
        var id = "Months";
        var text = "Months";
        var minInputLength = 0;
        var placeholder = "Select Month";
        var multiple = true;
        commonSelect2(select2MonthSelector,url,id,text,minInputLength,placeholder,multiple);

        //Year Selector
        var select2DateSelector = $('#selectDate');
        var url = "<?php echo base_url(); ?>user_site/selectAvailableLoggedInDates";
        var id = "Dates";
        var text = "Dates";
        var minInputLength = 0;
        var placeholder = "Select Date";
        var multiple = false;
        commonSelect2(select2DateSelector,url,id,text,minInputLength,placeholder,multiple);

        //Set the Width For the Select2-Container..
        $('.select2-container').css("width","223px");

        //As We Are Done With The Selectors Now We Need to Have A Go Button To Trigger the Action..
        $('#filterButton').on('click', function (output) {
            //Performing Some Action If Button is Pressed.
            //First We Need To Check if Any of The Filters Are Available.
            var selectedYears = $('#selectYear').select2('data');
            var selectedMonths = $('#selectMonth').select2('data');
            var selectedDate = $('#selectDate').select2('data');

            //Return the Function If No Filter Is Selected..
            if(selectedYears === null && selectedDate === null && selectedMonths.length == 0){
                Parexons.notification('You Must Select a Single Filter to Filter Logs','warning');
                return;
            }

            //As Now This Below Function Will Only Executes If Any of the Filters have Any Values.
             //We Will First Gonna Destroy the Existing DataTables.

            //As Old DataTables Instance Has Been Destroyed, So Creating New Instance With Filters..
            var filters = '';
            if(selectedYears !== null){
                filters += ' aoData.push({"name":"selectedYear","value":"'+selectedYears.id+'"});';
            }
            if(selectedDate !== null){
                filters += ' aoData.push({"name":"selectedDate","value":'+selectedDate.id+'});';
            }
            if(selectedMonths.length > 0){
                filters += ' aoData.push({"name":"selectedMonth","value":\''+JSON.stringify(selectedMonths)+'\'});';
            }
            console.log(JSON.stringify(selectedMonths));
            //Now As Filters Are Set, Destroy And Recreate The Table.. :)
            oTable.fnDestroy();
            commonDataTablesFiltered(selectorTableUsersLoginLogs,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
        });
    });
</script>

    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->