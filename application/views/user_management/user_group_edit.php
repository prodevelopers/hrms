<!-- contents -->

<div class="contents-container">

    <div class="bredcrumb">Dashboard / User Privilegies</div>
    <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>

    <div class="right-contents">
        <div class="head">User Restriction</div>
        <?php echo form_open(); ?>
        <table cellspacing="0">
            <thead class="table-head">
            <td colspan="4"> Group Restriction<?php //echo $data->user_group_title; ?></td>
            </thead>
            <tr class="table-row">
                <td width="120">Select Group</td>
                <td width="100"><input type="hidden" name="selectUserGroup" id="selectUserGroup"></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="table-row">
                <td width="120">Select Module</td>
                <td width="100"><input type="hidden" name="selectAppModule" id="selectAppModule"></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="table-row">
                <td width="120">View Only</td>
                <td><input type="checkbox" name="view" value="1"<?php if (isset($data->view_only)) {
                        if ($data->view_only == '1') {
                            echo "checked='checked' ";
                        }
                    } ?>></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="table-row">
                <td width="120">Modify</td>
                <td width="120"><input type="checkbox" name="modify" value="1" <?php if (isset($data->modify)) {
                        if ($data->modify == '1') {
                            echo "checked='checked' ";
                        }
                    } ?>></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="table-row">
                <td width="120">Create New</td>
                <td><input type="checkbox" name="create"
                           value="1" <?php if (isset($data->create_new_record)) {
                        if ($data->create_new_record == '1') {
                            echo "checked='checked' ";
                        }
                    } ?>></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="table-row">
                <td width="120">Print</td>
                <td><input type="checkbox" name="print" value="1" <?php if (isset($data->print)) {
                        if ($data->print == '1') {
                            echo "checked='checked' ";
                        }
                    } ?>></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <!-- button group -->
<!--        <div class="row">
            <div class="button-group">
                <input type="button" name="add" value="Save Changes" class="btn green"/>
            </div>
        </div>-->
        </form>
    </div>
</div>
<!-- contents -->
<script>
    $(document).ready(function (e) {
        //Load Page Selectors
        var userGroupSelector = $('#selectUserGroup');
        var url = "<?php echo base_url(); ?>user_site/user_group_add_pri/selectingGroup";
        var id = "UserGroupID";
        var text = "UserGroupName";
        var minInputLength = 0;
        var placeholder = "Select User Group";
        var multiple = false;
        commonSelect2(userGroupSelector, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width", "223px");
        userGroupSelector.on('change', function (e) {
            var GroupID = e.val;
            var ModuleID = $('#selectAppModule').val();
            if(ModuleID.length == 0){
                return;
            }
            var postData = {
                Module: ModuleID,
                Group:GroupID
            };
            $.ajax({
                url: "<?php echo base_url(); ?>user_site/user_group_add_pri/loadCheckBoxes",
                data: postData,
                type: "POST",
                dataType:"json",
                success: function (output) {
                    if(output.hasOwnProperty("msg")){
                        console.log(output);
                        var data = output.msg.split("::");
                        if(data[0] == "FAIL"){
                            Parexons.notification(data[1],data[2]);
                            $("input[type='checkbox']").prop('checked',false);
                            return;
                        }
                    }
                    //CheckBox For Print
                    if(output.PPrint == "1"){
                        $("input[name='print']").prop('checked', true);
                    }else if(output.PPrint == "0"){
                        $("input[name='print']").prop('checked', false);
                    }
                    //CheckBox For Create
                    if(output.PCreate == "1"){
                        $("input[name='create']").prop('checked', true);
                    }else if(output.PCreate == "0"){
                        $("input[name='create']").prop('checked', false);
                    }
                    //CheckBox For View
                    if(output.PView == "1"){
                        $("input[name='view']").prop('checked', true);
                    }else if(output.PView == "0"){
                        $("input[name='view']").prop('checked', false);
                    }
                    //CheckBox For Modify
                    if(output.PModify == "1"){
                        $("input[name='modify']").prop('checked', true);
                    }else if(output.PModify == "0"){
                        $("input[name='modify']").prop('checked', false);
                    }
                }
            });
        });

        //selecting Application Module..
        var selectAppModule = $('#selectAppModule');
        var url = "<?php echo base_url(); ?>user_site/user_group_add_pri/selectingModule";
        var id = "ModuleID";
        var text = "ModuleName";
        var minInputLength = 0;
        var placeholder = "Select App Module";
        var multiple = false;
        commonSelect2(selectAppModule, url, id, text, minInputLength, placeholder, multiple);
        $('.select2-container').css("width", "223px");
        selectAppModule.on('change', function (e) {
            var ModuleID = e.val;
            var GroupID = $('#selectUserGroup').val();
            if(GroupID.length == 0){
                return;
            }
            var postData = {
                Module: ModuleID,
                Group:GroupID
            };
            $.ajax({
                url: "<?php echo base_url(); ?>user_site/user_group_add_pri/loadCheckBoxes",
                data: postData,
                type: "POST",
                dataType:"json",
                success: function (output) {
                    if(output.hasOwnProperty("msg")){
                        var data = output.msg.split("::");
                        if(data[0] == "FAIL"){
                            Parexons.notification(data[1],data[2]);
                            $("input[type='checkbox']").prop('checked',false);
                            return;
                        }
                    }
                    //CheckBox For Print
                    if(output.PPrint == "1"){
                        $("input[name='print']").prop('checked', true);
                    }else if(output.PPrint == "0"){
                        $("input[name='print']").prop('checked', false);
                    }
                    //CheckBox For Create
                    if(output.PCreate == "1"){
                        $("input[name='create']").prop('checked', true);
                    }else if(output.PCreate == "0"){
                        $("input[name='create']").prop('checked', false);
                    }
                    //CheckBox For View
                    if(output.PView == "1"){
                        $("input[name='view']").prop('checked', true);
                    }else if(output.PView == "0"){
                        $("input[name='view']").prop('checked', false);
                    }
                    //CheckBox For Modify
                    if(output.PModify == "1"){
                        $("input[name='modify']").prop('checked', true);
                    }else if(output.PModify == "0"){
                        $("input[name='modify']").prop('checked', false);
                    }
                }
            });
        });

        //Now Need To Work on CheckBoxes Ajax. To Update Record When CheckBox value is Changed.
        $("input[type='checkbox']").on('change',function(e){
            var GroupID = $('#selectUserGroup').val();
            var ModuleID = $('#selectAppModule').val();
            if(GroupID.length == 0 || ModuleID.length == 0){
                Parexons.notification('You Must Select Both Group and Module','error');
                $(this).prop('checked',false);
                return;
            }
            var checkBoxName = $(this).attr('name');
            var postData = {
                Module: ModuleID,
                Group:GroupID
            };
            if($(this).is(":checked")){
                postData[checkBoxName] = 1;
            }else{
                postData[checkBoxName] = 0;
            }
            $.ajax({
               url: "<?php echo base_url(); ?>user_site/user_group_add_pri/updatePrivilege",
                data:postData,
                type: "POST",
                success: function (output) {
                    var data = output.split("::");
                    if(data[0] == "OK"){
                        Parexons.notification(data[1],data[2]);
                    }else if(data[0] == "FAIL"){
                        Parexons.notification(data[1],data[2]);
                    }
                }
            });
        });
    });
</script>

    <div id="right-panel" class="panel">
 <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->