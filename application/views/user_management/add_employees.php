<!------------------------------Models without Bootstrap css and js------------------------------------------>
<link rel="stylesheet" href="<?php echo base_url()?>assets/jqueryModals/style-popup.css" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryModals/jquery.leanModal.min.js"></script>
<!-------------------------------------Model Library End------------------------------------------------------>
<?php
$pageMessages = array();
$pageMessages['msg'] = $this->session->flashdata('msg');
?>
	<!-- hide show -->
<script type="text/javascript">
	$(document).ready(function(){

        /* New Select Employee
        * In This Employee Code and Employee Image Will Also Be Shown With Employee Name..
        * */
        var selectEmployeeSelector = $('#selectEmployee');
        var url = "<?php echo base_url(); ?>user_site/loadAvailableEmployees";
        var tDataValues = {
            id: "EmployeeID",
            text: "EmployeeName",
            avatar:'EmployeeAvatar',
            employeeCode:'EmployeeCode'
        };
        var minInputLength = 0;
        var placeholder = "Select Employee";
        var baseURL = "<?php echo base_url(); ?>";
        var templateLayoutFunc = function format(e) {
            if (!e.id) return e.text;
            return "<div class='select2TemplateImg'><span class='helper'></span> <img src='" + e.avatar + "'/><p> " + e.text + "</p><p>" + e.employeeCode + "</p></div>";

        };
        var templateLayout = templateLayoutFunc.toString();
        commonSelect2Templating(selectEmployeeSelector,url,tDataValues,minInputLength,placeholder,baseURL,templateLayout);
        $('.select2-container').css("width","30%");

        //Page Notifications If Any..
        <?php if(!empty($pageMessages) && is_array($pageMessages)){
echo "var message;";
foreach($pageMessages as $key=>$message){
    if(!empty($message) && isset($message)){
            echo "message = '".$message."';"; ?>
        var data = message.split("::");
        Parexons.notification(data[0],data[1]);
        <?php
        }
        }
    }
    ?>
	});
/// To Check Employee Code Already Exist
 function check_emp_code(){
   $(document).ready(function () {
    var employee_code = $('#emp_code').val();
    $.ajax({
      type: "POST",
      url: "human_resource/check_employee_code",
      data: {employee_id:employee_code} ,
      success: function(data) {
      $('#check_emp').html(data); 
  }
});
});  
 }
/// Function for Password Confirmation
function valid()
{
	if($('#pass').val() != $('#conf_pass').val())
	{
		alert("Sorry Password Doesn't Match .....!");
		return false;
	}else {
		return true;
	}
	if($('#pass').val() != $('#conf_pass').val())
	{
		alert("Sorry Password Doesn't Match .....!");
		return false;
	}
}
</script>
		<script type="text/javascript">
			function check_cnic()
			{
				$(document).ready(function ()
				{
					var fisrt_part = $('#nic1').val();
					var second_part = $('#nic2').val();
					var third_part = $('#nic3').val();
					var cnic = fisrt_part +'-'+second_part +'-'+third_part;
					$.ajax({
						type: "POST",
						url: "human_resource/check_cnic_exist",
						data: {cnic:cnic} ,
						success: function(data)
						{
							$('#check_cnic').html(data);
						}
					});
				});
			}
			/// To Forward Focus to Next Textfield
			$(document).ready(function()
			{
				$('input').keyup(function()
				{
					if(this.value.length == $(this).attr("maxlength"))
					{
						$(this).next().focus();
					}
				});
			});
		</script>

<style type="text/css">
	#autoSuggestionsList
	{
		width: 205px;
		overflow: hidden;
		border:1px solid #CCC;
		-moz-border-radius: 5px;
		border-radius: 5px;

		display:none;
	}
	#autoSuggestionsList li
	{
		list-style:none;
		cursor:pointer;
		padding:5px;
		font-size:14px;
		font-family:Arial;
		margin:2px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}
	#autoSuggestionsList li:hover
	{
		border:1px solid #CCC;
	}
div.activeToolTip
{
float:left;
 display:block;
 visibility:visible;
 position:relative;
 left:-3em;
 top:0.6em;
 background-color:rgba(0,0,0,0.2);
 font-family: Verdana, Arial, Helvetica, sans-serif;
 font-size: 0.75em;
 line-height: 30px;
 font-weight:bold;
 color: #000;
 text-align:center;
 border:1px solid black;
 filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);
 width:200px;
 height:30px;
}
div.idleToolTip
{
 display:none;
 visibility:hidden;
}

    .select2-results li{
        width: 300px;
        border-bottom:thin solid #ccc;
        height:60px;
    }
    .select2TemplateImg { padding:0.2em 0;}
    .select2TemplateImg img{ width: 50px; height: 50px; float:left;padding:0 0.5em 0 0;}
    .select2TemplateImg p{ padding:0.2em 1em; font-size:12px;}
    .resize{width: 220px;}
	#pagination
	{
		float:left;
		padding:5px;
		margin-top:15px;
	}
	.dataTables_length{
		position: relative;
		top: -20px;
		/*font-size: 1px;*/
		font-size:0.3px;
		/*left: 92.5%;*/
	}

	#pagination a
	{
		padding:5px;
		background-image:url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color:#5D5D5E;
		font-size:14px;
		text-decoration:none;
		border-radius:5px;
		border:1px solid #CCC;
	}
	#pagination a:hover
	{
		border:1px solid #666;
	}
	/*.mytab tr:nth-child(odd) td{
        background-color:#ECECFF;
    }
    .mytab tr:nth-child(even) td{
        background-color:#F0F0E1;
    }*/
	.paginate_button
	{
		padding: 6px;
		background-image: url(<?php echo base_url(); ?>assets/images/top_menu_bg.png);
		color: #5D5D5E;
		font-size: 14px;
		text-decoration: none;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		border: 1px solid #CCC;
		margin: 1px;
		cursor:pointer;
	}
	.paging_full_numbers
	{
		margin-top:8px;
	}
	.dataTables_info
	{
		color:#3474D0;
		font-size:14px;
		margin:6px;
	}
	.paginate_active
	{
		padding: 6px;
		border: 1px solid #3474D0;
		border-radius: 5px;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		color: #3474D0;
		font-weight: bold;
	}

	/*********************************************Model CSS*********************************************/
	/*modal CSS*/
	.modal-demo {

		float:left;
		background-color: #FFF;
		padding-bottom: 15px;
		border: 1px solid #000;
		border-radius: 10px;
		box-shadow: 0 8px 6px -6px black;
		display: none;
		text-align: left;
		width: 600px;


	}
	.title {
		border-bottom: 1px solid #ccc;
		font-size: 18px;
		line-height: 18px;
		padding: 10px 20px 15px;
	}
	h1, h2, h4 {
		font-family: "Dosis",sans-serif;
	}
	.text{
		padding: 0 20px 20px;
	}
	.text , .title{
		color: #333;
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
		line-height: 1.42857;
	}
	button.close {
		background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
		border: 0 none;
		cursor: pointer;
		padding: 0;
	}
	.close {
		position: absolute;
		right: 15px;
		top: 15px;
	}

	.close {
		color: #ffffff;
		float: right;
		font-size: 21px;
		font-weight: 700;
		line-height: 1;
		text-shadow: 0 1px 0 #fff;
	}

	.pop_row{
		float:left;
		width:100%;
		height:auto;
		margin-bottom: 7px;
	}
	.pop_row_txtarea{
		float:left;
		width:100%;
		height:auto;
		margin-bottom: 7px;
	}
	.left_side{
		float:left;
		width:40%;
		height:auto;
	}
	.left_side h5{
		line-height: 35px;
		padding-left: 20px;
	}
	.right_side{
		float:left;
		width:60%;
		height:auto;
	}
	.right_txt{
		width:240px important;
		height:auto important;
	}
	.top_header{
		float:left;
		width:100%;
		height:45px;
		background-color: #4f94cf;
		margin-bottom: 10px;
		border-top-right-radius: 10px;
		border-top-left-radius: 10px;
		border-bottom: 2px solid rosybrown
	}
	.top_header h3{
		color:#ffffff
	}
	.right_txt_area{
		width:240px;
		height:100px;
		padding:7px;
	}
	.btn-row{
		width:auto;
		float:right;
		margin-right:-15px;
	}
	.btn-pop{
		float:right;
		/*width:80px;*/
		height:35px;
		background-color: #6AAD6A;
		color:white;
		margin-right: 120px;
	}
	.btn-pop:hover{
		cursor: pointer
	}
	.view_model{
		width:600px;
	}
	.left_photo{
		float:left;
		width:100px;
		height:100px;
		margin-right: 10px;
		margin-top: 10px;
		border: 1px solid #d3d3d3
	}
	.right_sec{
		float:left;
		width:820px;
		height:auto;
		margin-right: 10px;
		margin-bottom: 10px;
	}
	.left_lbl h5{
		line-height: 35px;
	}
	.left_lbl{
		float:left;
		width:190px;
		height:35px;
		margin-right: 5px;
		margin-bottom:10px;
		padding-left: 5px;
	}
	.right_lbl{
		float:left;
		width:190px;
		height:35px;
		margin-bottom:10px
	}
	.right_lbl{
		line-height: 35px;
		padding-left: 5px;
	}
	.left_block{
		width:400px;
		height:auto;
		float:left;
	}
	div.text form{
		display: inline-block;
	}

	.modal-footer{
		border-top: @gray-lighter solid 1px;
		text-align: right;
	}
	#dataTableFilter{
		width: 22% !important;
	}
	.container_view .btn:hover{
		cursor:pointer;
		opacity: 0.99;
		border-top: 1px solid red;

	}
	.container_view .userGroup{
		width: 100%;

	}

</style>
<!-- contents -->

<div class="contents-container">
	<div class="bredcrumb">Dashboard / Employees / Add Employee User Account</div> <!-- bredcrumb -->
	<a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Add Employee User Account</div>
			<div class="row">
				<div class="col-md-12" style="padding: 0.5%;">
					<div class="notice info">
						<p>Instruction</p>
						<ul>
							<li>Employee must have an Active Employment.</li>
							<li>Employee must not have user account.</li>
						</ul>
					</div>
				</div>
				<!-- table -->
				<div class=" col-md-12">
				<table cellpadding="0" cellspacing="0" id="table_list">
					<thead class="table-head">
					<tr id="table-row">
						<td width="20%" id="hiddenEmployeeID">HiddenID</td>
						<td width="20%">Employee Code</td>
						<td width="20%">Employee Name</td>
						<td width="20%">Father Name</td>
						<td width="20%">Contact#</td>
						<td width="20%">Position</td>
						<td width="20%">Create</td>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
			</div>
		</div>

	</div>
<!-- contents -->
<!---------------------Create user Account Model------------------>
<div id="EditModal" class="modal-demo view_model" style="display: none;">
	<div class="top_header">
		<button type="button" class="close" onclick="Custombox.close();">
			<span>&times;</span><span class="sr-only"></span>
		</button>
		<h3 class="title">Create Login Details</h3>
	</div>
		<div class="container_view" style="width:600px;height:auto;">
			    <div class="row">
					<?php echo form_open_multipart(); ?>
					<table style="width: 100%;">
<!--						<tr>-->
<!--							<td ><h4>Select Employee</h4></td>-->
<!--							<td><input style="width:100%;" type="hidden" id="selectEmployee" name="emp_id"></td>-->
<!---->
<!--						</tr>-->
						<input type="hidden" id="hiddenEmployeeID" name="emp_id">
						<tr>
							<td><h4>User Name (Email)</h4></td>
							<td><input style="width: 100%;" type="email"  name="employee_email" placeholder="Email Address" required/></td>

						</tr>
						<tr>
							<td><h4>User Group</h4></td>
							<td><?php echo @form_dropdown('user_group', $user_group,array(),'class="userGroup"'); ?></td>
						</tr>
						<tr>
							<td><h4>Password</h4></td>
							<td><input style="width: 100%;" type="password" name="password" id="pass" placeholder="Password" required></td>

						</tr>
						<tr>
							<td><h4>Confirm Password</h4></td>
							<td><input style="width: 100%;" type="password" name="confrim_password" id="conf_pass" placeholder="confirm Password" required></td>

						</tr>
						<tr>
							<td><h4>Status</h4></td>
							<td><select name="status" style="width: 100%;" required>
									<option value="1">Enable</option>
									<option value="0">Disable</option>
								</select>
							</td>

						</tr>
					</table>
				</div>
				<!-- button group -->
				<hr class="clear">
				<div class="row">
					<div class="pull-right" style="padding-right: 1%;">
						<input type="button" value="Cancel"  class="btn gray" onclick="Custombox.close();" />
						<input type="submit" class="btn green" name="continue" value="Create" onclick="return valid()">
					</div>
				</div>


				<?php echo form_close(); ?>
			</div>
</div>
		</div>
<!-- Menu left side  -->
<div id="right-panel" class="panel">
	<?php $this->load->view('includes/user_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
	$('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
	$('#close-panel-bt').click(function() {
		$.panelslider.close();
	});
	$(document).ready(function(e){
		$('#user_group_title').select2();
	});
</script>

<!-- leftside menu end -->
<script>
	$("#alert").delay(2000).fadeOut('slow');
	</script>

<div id="right-panel" class="panel">
    <?php $this->load->view('includes/user_left_nav'); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
<script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
        $.panelslider.close();
    });

</script>
	<script type="application/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript">
		var oTable;
		$(document).ready(function(e){
			//Select department
			var LoadDepartment = $('#Department');
			var url = "<?php echo base_url(); ?>human_resource/loadDepartment";
			var id = "DepartmentID";
			var text = "DepartmentName";
			var minInputLength = 0;
			var placeholder = "Select Department";
			var multiple = false;
			commonSelect2(LoadDepartment,url,id,text,minInputLength,placeholder,multiple);

			///////////////////////////Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
			//Filter Selectors
			//Select department
			var selectDepartmentsSelector = $('#selectDepartments');
			var url = "<?php echo base_url(); ?>human_resource/loadDepartment";
			var id = "DepartmentID";
			var text = "DepartmentName";
			var minInputLength = 0;
			var placeholder = "Select Department";
			var multiple = false;
			commonSelect2(selectDepartmentsSelector,url,id,text,minInputLength,placeholder,multiple);
			// filter datatable
			selectDepartmentsSelector.on('change',function(e){
				oTable.fnDestroy();
				var filteredselectDepartments = $(this).val();
				var filters = 'aoData.push({"name":"filteredselectDepartments","value":'+$(this).val()+'});';
				commonDataTablesFiltered(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT,filters);
			});

			//Filter the SearchBox Of Table.
			$('#dataTableFilter').on('keyup',function(e){
				oTable.fnFilter($(this).val());
			});
			///////////////////////End Of Page Selectors\\\\\\\\\\\\\\\\\\\\\\\\\\

//Need To Display the list of All The Reported/Violated Disciplines
			oTable ='';
			var tableSelector = $('#table_list');
			var url_DT = "<?php echo base_url(); ?>user_site/user_list/list";
			var aoColumns_DT = [
				/* ID */ {
					"mData": "employee_id",
					"bVisible": false,
					"bSortable": false,
					"bSearchable": false
				},
				/*employee_code No */ {
					"mData" : "employee_code"
				},/*User full_name */ {
					"mData" : "full_name"
				},
				/* user_name */ {
					"mData" : "father_name"
				},
				/* Contact# */ {
					"mData" : "mob_num"
				},
				/* availPositions */ {
					"mData" : "Position"
				},
				/* Shows The Status Of The Report */ {
					"mData" : "ViewEditActionButtons"
				}
			];
			var HiddenColumnID_DT = 'employee_id';
			var sDom_DT = '<"H"r>t<"F"<"row"<"col-xs-6" i> <"col-xs-6" p>>>';
			commonDataTables(tableSelector,url_DT,aoColumns_DT,sDom_DT,HiddenColumnID_DT);
			//End Of DataTables
///
			$(document).on('click','.AddUserAccount', function(e){

				var employeeID = $(this).parents("tr").attr("data-id");
				var modal = $("#EditModal");
				modal.find("input#hiddenEmployeeID").val(employeeID);
//
// $.ajax({
//					url: "<?php //echo base_url(); ?>//User_site/add_employee_user_account",
//					data: {employeeID:employeeID}
//				});
				Custombox.open({
					target: '#EditModal',
					effect: 'fadein'

				});
				e.preventDefault();

			});

			//DatePickers
			$('#dateOfOccurrence').datepicker({
				dateFormat: "dd-mm-yy",
				changeMonth: true,
				changeYear: true
			});

			//DatePickers
			$('#EDateOfExpiry').datepicker({
				dateFormat: "dd-mm-yy",
				changeMonth: true,
				changeYear: true
			});


			//Defining Width For All Selectors Of the Filters
			$('.select2-container').css("width","180px");
			//Adding table-row Class to DataTables For Better look
			$('tbody').addClass("table-row");

			$("#modaltrigger").on('click',function(){
				var post='ref';
				$.ajax({
					url: "<?php echo base_url(); ?>human_resource/advertise_reference_no",
					type: "POST",
					data:{id:post},
					success:function(output){
						var ref=parseInt(output);
						var IncRef =ref + 1;
						$("#refrence_v").html('').append(IncRef);
						$("#refrence_no").html('').val(IncRef);
					}
				});
			});
		});
	</script>