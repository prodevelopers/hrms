<style>
.rehieght{
	position: relative;
	top: -5px;
}
.dataTables_length{
	position: relative;
	top: -20px;
	font-size: 1px;
}
.dataTables_length select{
	width: 60px;
}

.dataTables_filter{
	position: relative;
	top: -21px;
	left: -10.6%;
}
.dataTables_filter input{
	width: 180px;
}
.revert{
	margin-left: -15px;
}
.marge{
	margin-top: 60px;
}
</style>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Group Privilegies</div> <!-- bredcrumb -->
 <a id="right-panel-link" href="#right-panel" ><span style="background:#fff; padding:0.2em 0.3em; border:1px solid #ccc;" class="fa fa-bars fa-2x"></span></a>
	<div class="right-contents">
		<div class="head">Group Privilegies</div>

			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Group Name">
				<!--<select>
					<option>Group ID</option>
				</select>-->
                <?php   echo form_dropdown('employee_code',$employee_code); ?>
				<!--<select>
					<option>Group Name</option>
				</select>-->
				 <?php   echo form_dropdown('user_group_title',$user_group_title); ?>
				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>

			
			<!-- table -->
			<div id="accordion"  class="marge">
            <?php if (!empty($join)){?>
			<?php foreach($join as $record):
		//echo "<pre>"; print_r($data); die;
			?>
			<h4 class="head"><?php echo $record->user_group_title; ?></h4>
			<div>
			<table cellspacing="0"
				<thead class="table-head">
					<td>Module</td>
					<td>View</td>
					<td>Modify</td>
					<td>Create New</td>	
					<td>Trash</td>
					<td>Print</td> 
				</thead>
				<tr class="table-row">
					<td><?php echo $record->module_name; ?></td>
					<td><input type="checkbox" name="view_only" checked disabled value="1"<?php if(isset($record->view_only)){if($record->view_only == '1'){echo "checked='checked' ";} }?>></td>
					<td><input type="checkbox" name="modify" checked disabled value="1"<?php if(isset($record->modify)){if($record->modify == '1'){echo "checked='checked' ";} }?>></td><!--<input type="checkbox" checked disabled>-->
					<td><input type="checkbox" name="create_new_record" checked disabled value="1"<?php if(isset($record->create_new_record)){if($record->create_new_record == '1'){echo "checked='checked' ";} }?>></td>
                    <td><input type="checkbox" name="trashed" checked disabled value="1"<?php if(isset($record->trashed)){if($record->trashed == '1'){echo "checked='checked' ";} }?>></td>
					<td><input type="checkbox" name="print" checked disabled value="1"<?php if(isset($record->print)){if($record->print == '1'){echo "checked='checked' ";} }?>></td>
				</tr>
				
			
			</table>
			<div class="row">
				<div class="button-group">

					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
			</div>
            
              <?php endforeach;?> 
            <?php } else { echo " <span style='color:#F00'>No record found..</span>";}?>


			</div>
			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="user_site/group_edit_priviligies"><button class="btn green">Update Privilegies</button></a>
					<!--<a href="hr_site/user_group"><button class="btn green">Create New</button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

		</div>

	</div>
<!-- contents -->
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#accordion" ).accordion();
</script>

    <div id="right-panel" class="panel">
       <?php $this->load->view('includes/user_left_nav'); ?>
    </div>
    <script src="<?php echo base_url() ?>assets/js/jquery.panelslider.js"></script>
    <script>
    $('#right-panel-link').panelslider({side: 'left', clickClose: false, duration: 200 });
    $('#close-panel-bt').click(function() {
      $.panelslider.close();
    });
    </script>
    <!-- leftside menu end -->