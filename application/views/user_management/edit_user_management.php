<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
function valid()
{
	if($('#pass').val() != $('#confirm').val())
	{
		alert("Sorry Password Does Not Match !");
		return false;
	}else {
		return true;
	}
}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Users Management / Edit User</div> <!-- bredcrumb -->

	<div class="right-contents full">

	

		<div class="head">Edit User</div>
			<?php //print_r($data); ?>
			<form action="" method="post">
				<div class="row">
					<h4>User Role</h4>
                     <?php   
					$selctd_group = (isset($data->user_group_id) ? $data->user_group_id : '');
					
					 echo form_dropdown('user_group_title',$user_group_title,$selctd_group); ?>
				<!--<select name="user_group">
						<option>Admin</option>
						<option>HRM</option>
					</select>-->
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employee Code</h4>
					<input type="text" name="employee_code" value="<?php echo $data->employee_code;?>" readonly="readonly" >
				</div>
				<br class="clear">
				<div class="row">
					<h4>Employee Name</h4>
					<input name="full_name" type="text" value="<?php echo $data->full_name;?>" readonly="readonly">
				</div>
				<br class="clear">
				<div class="row">
					<h4>User Name</h4>
					<input name="user_name" type="text" value="<?php echo $data->user_name;?>">
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Update Password</h4>
					<input type="password" name="password" id="pass" value="" >
				</div>
              
				<br class="clear">
				<div class="row login">
					<h4>Confirm Password</h4>
					<input type="password" name="confirm" id="confirm">
				</div>
				<br class="clear">
				<div class="row login">
					<h4>Status</h4>
					<select name="enabled">
						<option value="1">Enable</option>
						<option value="0">Disable</option>
					</select>
				</div>
				<br class="clear">
				
		

			<!-- button group -->
			<div class="row">
				<div class="button-group">
					<!--<a href="add_employee_personal_info.php"><button class="btn green">Add</button></a>-->
                    <input type="submit" name="edit" value="Update" class="btn green" onClick="return valid()">
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>

				</form>

		</div>

	</div>
<!-- contents -->
