<html>
	<head>
		<title>Error 404</title>
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	</head>
	<style type="text/css">
		body{
			background:url(<?php echo base_url()?>assets/images/four.jpg);
			background-size:cover;
			font-family: 'Oswald', sans-serif;
		}
		#error{
			margin:6em 0 6em 4em;
			width:30%;
			min-height:200px;
			overflow:hidden;
			float:left;
		}
		h1{ font-size:45px; line-height:2.4em;
			-webkit-transform:rotate(5deg);
			-moz-transform:rotate(5deg);
			-ms-transform:rotate(5deg);
			-o-transform:rotate(5deg);
			transform:rotate(5deg);
		}
		span{
			font-size:4em;
			color:#fff;
		}
		#error p{ line-height:0; letter-spacing:0.75em;padding-top: 1em; font-size:20px;}
		#back{ float:left; width:50%; position:relative;
			top:0;
			margin:7em 0 2em 7em;}
		#back p{
			line-height:1em;
			
			font-size:4em;
			letter-spacing:0;
			color:#fff;
			font-weight:bold;
		}
		#back span{
			 font-size: 20px;
			 color:#ccc;
		}
		#back #a{
		text-align:left;
		font-size:24px;
	
		}
		#a a{
				color:yellow;
		}
	</style>
	<body>
	<div id="back">
			<p>Human Resource <br> <span>Management System</span></p>
			<p id="a"><a href="<?php echo base_url()?>dashboard_site/view_dashboard">Home</a></p>
		</div>
		<div id="error">
			<h1>Error
				<br> <span>404</span>
			</h1>
			<p>Page Not Found</p>
		</div>
		
		<br>
		<br>
		<br>
		<br>
		<p style="width:100%;float:left;text-align:center; margin-top:6em;color:#fff; font-size:0.8em; letter-spacing:0">Parexons IT Solutions &  Services</p>
	</body>
</html>