<?php

/*require_once __DIR__ . '/db_connect_app.php';

$db = new DB_CONNECT();*/


$response = array();

//$_POST['employee_id'] = "1";
//$_POST['in_time'] = "3:4";
//$_POST['in_date'] = "2012-4-4";
//$_POST['out_time'] = "3:7";
//$_POST['out_date'] = "2012-4-4";
//$_POST['attendence_status_type'] = "1";
//$_POST['remarks'] = "helo";
//$_POST['ml_month_id'] = "1";
//$_POST['ml_year_id'] = "2015";
//$_POST['work_week_id'] = "Monday";
//$_POST['latitude'] = "324";
//$_POST['longitude'] = "4555";


if (  isset($_POST['employee_id'])
    && isset($_POST['in_time']) && isset($_POST['in_date'])&& isset($_POST['out_time'])&& isset($_POST['out_date'])
    && isset($_POST['attendence_status_type'])&& isset($_POST['remarks'])&& isset($_POST['ml_year_id'])
    && isset($_POST['ml_month_id'])&& isset($_POST['work_week_id'])&& isset($_POST['latitude'])
    && isset($_POST['longitude']))
{
// 
    $employee_id = $_POST['employee_id'];
    $in_time = $_POST['in_time'];
    $in_date = $_POST['in_date'];
    $out_time = $_POST['out_time'];
    $out_date = $_POST['out_date'];
    $attendance_status_type = $_POST['attendence_status_type'];
    $remarks = $_POST['remarks'];
    $ml_month_id=$_POST['ml_month_id'];
    $ml_year_id=$_POST['ml_year_id'];
    $work_week_id=$_POST['work_week_id'];
    $latitude=$_POST['latitude'];
    $longitude=$_POST['longitude'];
//   
//   
//  
//   try {
    $this->load->model('common_model');

    //First We need To Check If Record Already Exist
    $attTable = 'attendence';
    $selectData = array('COUNT(1) AS TotalRecordsFound',false);
    $where = array(
        'employee_id' => $employee_id,
        'in_date' =>$in_date
    );
    $countResult = $this->common_model->select_fields_where($attTable,$selectData,$where,TRUE);
    if(isset($countResult) && $countResult->TotalRecordsFound > 0){
        $response["success"] = 0;
        $response["message"] = "Today's Attendance Is Already Been Registered.";
        echo json_encode($response);
        exit;
    }
    //Select The Required Fields, Year And Work Week ID's
    //Working For Year
    $yearTable = 'ml_year';
    $selectData = 'ml_year_id';
    $where = array(
        'year' => $ml_year_id,
        'trashed' => 0
    );
    $yearIDResult = $this->common_model->select_fields_where($yearTable,$selectData,$where,TRUE);
    if(isset($yearIDResult) && $yearIDResult->ml_year_id > 0){
        $ml_year_id = $yearIDResult->ml_year_id;
    }else{
        $response["success"] = 0;
        $response["message"] = "Year Is Not Defined In Database, Please Contact System Administrator for Further Assistance";
        echo json_encode($response);
        exit;
    }
    //Working For Work Week
    $workWeekTable = 'work_week';
    $selectData = 'work_week_id';
    $where = array(
        'work_week' => $work_week_id,
        'trashed' => 0
    );
    $workWeekResult = $this->common_model->select_fields_where($workWeekTable,$selectData,$where,TRUE);
//    print_r($workWeekResult);
    if(isset($workWeekResult) && $workWeekResult->work_week_id > 0){
        $work_week_id = $workWeekResult->work_week_id;
    }else{
        $response["success"] = 0;
        $response["message"] = "Work Week Is Not Defined In Database, Please Contact System Administrator for Further Assistance";
        echo json_encode($response);
        exit;
    }
    //This Code Is To Insert Data
    $table = 'attendence';
    $insertData = array(
        'employee_id' => $employee_id,
        'in_time' => $in_time,
        'in_date' => $in_date,
        'out_time' =>$out_time,
        'out_date' => $out_date,
        'attendance_status_type' => $attendance_status_type,
        'ml_month_id' => $ml_month_id,
        'ml_year_id' => $ml_year_id,
        'work_week_id' => $work_week_id,
        'remarks' => $remarks,
        'latitude' => $latitude,
        'longitude' => $longitude
    );
    $insertedRowId = $this->common_model->insert_record($table,$insertData);

    if (isset($insertedRowId) && $insertedRowId > 0) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Attendence successfully inserted.";
        echo json_encode($response);
    } else
    {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";

        // echoing JSON response
        echo json_encode($response);
    }
}
else
{
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>