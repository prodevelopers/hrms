<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
//$_POST['empcode'] ="E-02";
// check for required fields
if (isset($_POST['empcode']) ) {
    $employee_code = $_POST['empcode'];
    $result = $this->db->query("SELECT * FROM  employee WHERE employee_code ='".$employee_code."'");

    // check if row inserted or not

    if ($result->result_array()) {
        $result = $result->result_array();
        // successfully inserted into database
         if($result!=null){
             //Response COde.
             $response["success"] = 1;
             $response["found"] = 1;
             $response["message"] = "Record Found";
//        Setting Employees Records Data
        $response["id"] = $result[0]["employee_id"];
        $response["employee_code"] = $result[0]["employee_code"];
        $response["full_name"] = $result[0]["full_name"];
        $response["father_name"] = $result[0]["father_name"];
        $response["CNIC"] = $result[0]["CNIC"];

          echo json_encode($response);
         }
       
        // echoing JSON response
          
    }
    else {
        $response["success"] = 1;
        $response["found"] = 0;
        $response["message"] = "Record not Found";
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>
