<?php //include('includes/header.php'); ?>

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Apply For Leave</div> <!-- bredcrumb -->

	<?php  $this->load->view('includes/leave_left_nav');
	//echo "<pre>";
	//print_r($employee);
	
	
	 ?>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Apply For Leave</div>

			

			<?php //foreach($employee as $employee):?>

			<form action="site/add_leave_action" method="post">
				<div class="row">
					<h4>Employee ID</h4>
					<h4><i><?php echo $employee->employee_code; ?></i></h4>
				</div>
				<div class="row">
					<h4>Employee Name</h4>
					<h4><i><?php echo $employee->full_name; ?></i></h4>
				</div>
				<div class="row">
					<h4>Leave Entitlements</h4>
					<h4><i><?php echo $employee->no_of_leaves; ?></i></h4>
				</div>
				<div class="row">
					<h4>Remaining</h4>
					<h4><i>02</i></h4>
				</div>
				<div class="row">
					<h4>Project/Task</h4>
					<h4><i>HR Management System</i></h4>
				</div>
                <?php //endforeach;?>
				<!--<div class="row">
					<h4>Progress</h4>
					<h4>
						<div class="meter orange">
							<span style="width: 75.3%"></span>
						</div>
					</h4>
				</div>-->
				<br class="clear">
				<div class="row">
					<h4>Leave Type</h4>
                    <?php echo form_dropdown('leave_type',$leave_type);?>
					<!--<select>
						<option></option>
					</select>-->
				</div>
				<br class="clear">
				<!--<div class="row">
					<h4>Duration</h4>
					<select>
						<option></option>
					</select>
				</div>-->
				<br class="clear">
				<div class="row">
                <script>
                $(function()
				{
					$("#date").datepicker({dateFormat: 'dd-mm-yy'});
					});
                </script>
					<h4>From</h4>
					<input type="text" id="date" name="from">
				</div>
				<br class="clear">
				<div class="row">
                 <script>
                $(function()
				{
					$("#c").datepicker({dateFormat: 'dd-mm-yy'});
					});
                </script>
					<h4>To</h4>
					<input type="text" id="c" name="to">
				</div>
				<br class="clear">
				<!--<div class="row">
					<h4>Approve From</h4>
					<select>
						<option></option>
					</select>
				</div>-->
				<br class="clear">
				<div class="row" style="width:90%">
					<h4 style="width:21%">Note</h4>
					<textarea name="note"></textarea>
				<br class="clear">
				</div>
				<br class="clear">
				<!-- button group -->
			<div class="row">
				<div class="button-group">
                <input type="submit" name="add" value="Apply" class="btn green" />
					<!--<a href="#"><button class="btn green">Apply</button></a>-->
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
				
			</form>

		</div>

	</div>
<!-- contents -->

<?php //include('includes/footer.php'); ?>
