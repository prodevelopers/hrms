<style>
#pagination
{
	float:left;
	padding:5px;
	margin-top:15px;
}
#pagination a
{
	padding:5px;
	background-image:url(assets/images/top_menu_bg.png);
	color:#5D5D5E;
	font-size:14px;
	text-decoration:none;
	border-radius:5px;
	border:1px solid #CCC;
}
#pagination a:hover
{
	border:1px solid #666;
}
/*.mytab tr:nth-child(odd) td{
	background-color:#ECECFF;
}
.mytab tr:nth-child(even) td{
	background-color:#F0F0E1;
}*/
.paginate_button
{
	padding: 6px;
	background-image: url(assets/images/top_menu_bg.png);
	color: #5D5D5E;
	font-size: 14px;
	text-decoration: none;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border: 1px solid #CCC;
	margin: 1px;
	cursor:pointer;
}
.paging_full_numbers
{
	margin-top:8px;
}
.dataTables_info
{
	color:#3474D0;
	font-size:14px;
	margin:6px;
}
.paginate_active
{
	padding: 6px;
	border: 1px solid #3474D0;
	border-radius: 5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	color: #3474D0;
	font-weight: bold;
}
.dataTables_filter
{
	float:right;
}
</style>

<script type="application/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var oTable = $('#table_list').dataTable( {
		
		"bProcessing": true,
		"bPaginate" :true,
		"sPaginationType": "full_numbers",
		"bServerSide": true,
        "sAjaxSource": window.location+"/list",
		"bDestroy":true,
		"sServerMethod": "POST",
		"aaSorting": [[ 0, "asc" ]],
		"fnServerParams": function (aoData, fnCallBack){
 			aoData.push({"name":"leave_type","value":$('#leave_type').val()});
					        
                
		}
                
	});
});

$(".filter").change(function(e) {
    oTable.fnDraw();
});

function doc_file(){
    var r  =confirm('if you are upload this file again !\n\ please follow check out method');
if(r == true){
   return true;
}else if(r == false){
return false;
}
}

function confirm()
{
	alert('Are You Sure to Delete Record ...?');
}
</script>
<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Leave Status</div> <!-- bredcrumb -->

	<?php  $this->load->view('includes/leave_left_nav'); ?>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Leave Status</div>

            <?php echo form_open('');?>
			<!-- filter -->
			<div class="filter">
				<h4>Filter By</h4>
				<!--<input type="text" placeholder="Employee Name">-->
                <?php echo form_dropdown('leave_type',$leave_type,$uname,"class='select_47' filter id='leave_type' onchange='this.form.submit()'");?>
			<!--<div class="filter">
				<h4>Filter By</h4>
				<input type="text" placeholder="Employee Name">
				<select>
					<option>Leave Type</option>
				</select>
				<select>
					<option>Month</option>
				</select>

				<div class="button-group">
					<button class="btn green">Search</button>
					<button class="btn gray">Reset</button>
				</div>
			</div>-->

			<!-- button group -->
			<!-- <div class="row">
				<div class="button-group">
					<a href="add-employee.html"><button class="btn green">Add</button></a>
					<button class="btn red">Delete</button>
				</div>
			</div> -->

			<!-- table -->
            <table class="table" id="table_list" cellspacing="0" width="100%">
                   <thead class="table-head">
                   <tr id="table-row">
                       <!--<td><input type="checkbox"></td>-->
                    <td  align="left">Employee Code</td>   
					<td align="left">Employee Name</td>
					<td  align="left">Leave Type</td>
					<td width="15%"  align="left">From</td>
					<td width="15%"  align="left">To</td>
                    <td  align="left">Emergency Contacts</td>
					<td align="left">Status</td>
					
					
                           
                    </tr>
                </thead>
                    <tbody>
                    </tbody>
         </table>
         <?php echo form_close();?>
			<!--<table cellspacing="0">
				<thead class="table-head">
					<td>Employee ID</td>
					<td>Employee Name</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>Emergency Contacts</td>
					<td>Status</td>
				</thead>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>+92 300 568 45 54</td>
					<td>Pending</td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>+92 300 568 45 54</td>
					<td>Pending</td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>+92 300 568 45 54</td>
					<td>Pending</td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>+92 300 568 45 54</td>
					<td>Pending</td>
				</tr>
				<tr class="table-row">
					<td>035</td>
					<td>Employee</td>
					<td>Leave Type</td>
					<td>Duration</td>
					<td>From</td>
					<td>To</td>
					<td>+92 300 568 45 54</td>
					<td>Pending</td>
				</tr>
			</table>-->

		</div>

	</div>
<!-- contents -->

<?php //include('includes/footer.php'); ?>
