

<!-- contents -->

<div class="contents-container">

	<div class="bredcrumb">Dashboard / Leave&Attendance / Leave Action</div> <!-- bredcrumb -->

	<?php $this->load->view('includes/leave_left_nav') ?>

 <script>
      var navigation = responsiveNav(".nav-collapse");
    </script>
	<div class="right-contents">

		<div class="head">Leave Action</div>

			<?php //echo print_r($rec);
			//foreach ($rec as $data):?>

			<form>
				<div class="row">
					<h4>Employee ID</h4>
					<h4><i><?php echo $rec->employee_code; ?></i></h4>
				</div>
				<div class="row">
					<h4>Employee Name</h4>
					<h4><i><?php echo $rec->full_name; ?></i></h4>
				</div>
				
				<div class="row">
					<h4><b>Leave Info</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Leave Entitlements</h4>
					<h4><i><?php echo $rec->no_of_leaves; ?></i></h4>
				</div>
				<div class="row">
					<h4>Remaining</h4>
					<h4><i>02</i></h4>
				</div>
				
				<div class="row">
					<h4>Leave Type</h4>
					<h4><i><?php echo $rec->leave_type; ?></i></h4>
				</div>
				<div class="row">
					<h4>Duration</h4>
					<h4><i><?php echo($rec->to)-($rec->from); ?></i></h4>
				</div>
				<div class="row">
					<h4>From</h4>
					<h4><i><?php echo $rec->from; ?></i></h4>
				</div>
				<div class="row">
					<h4>To</h4>
					<h4><i><?php echo $rec->to; ?></i></h4>
				</div>
				<div class="row">
					<h4><b>Project/Task Info</b></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Project/Task</h4>
					<h4><i>HR Management System</i></h4>
				</div>
				<div class="row">
					<h4>Progress</h4>
					<h4>
						<div class="meter orange">
							<span style="width: 75.3%"></span>
						</div>
					</h4>
				</div>
				<br class="clear">
				<div class="row" style="width:90%">
					<h4 style="width:21%">Application</h4>
					<h4><i><?php echo $rec->note; ?></i></h4>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Action</h4>
					<select name="status">
                    	<option><?php echo $rec->status; ?></option>
						<option value="Approve">Approve</option>
						<option value="Reject">Reject</option>
					</select>
				</div>
				<br class="clear">
				<div class="row">
					<h4>Remarks</h4>
					<textarea><?php echo $rec->remark; ?></textarea>
				</div>
				<br class="clear">
				<!-- button group -->
			<div class="row">
				<div class="button-group">
					<a href="#"><button class="btn green">Submit</button></a>
					<!-- <button class="btn red">Delete</button> -->
				</div>
			</div>
				
			</form>
<?php //endforeach;?>
		</div>

	</div>
<!-- contents -->


