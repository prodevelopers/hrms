<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Syed Haider Hassan
 * Date: 3/17/13
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * @property common_model $common_model It resides all the methods which can be used in most of the controllers.
 * @class MY_Controller
 */
class MY_Controller extends CI_Controller{
    public $data = array();
    function __construct() {
        parent::__construct();
        //Loading Common Files Among All Controllers
        $this->load->model('common_model');
        $this->load->helper('extra_helper');
//        $this->load->model('users_management/login_check');

        $this->data['errors']=array();
        $this->data['site_name']=array();
        $this->data['errorPage_401'] = 'errorPages/error_401';
        $this->data['errorPage_403'] = 'errorPages/error_403';
        $this->data['errorPage_404'] = 'errorPages/error_404';
        $this->data['errorPage_500'] = 'errorPages/error_500';
        $this->data['empCodeFormat'] = 'E-xxx';
        $this->data['defaultMinYearDatePicker'] = 'c-50:c+10';
        $this->data['mailConfiguration'] = array(
            'protocol' => 'sendmail',
            'mailpath' => '/usr/sbin/sendmail',
            'wordwrap' => 'TRUE',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );

        if(loginCheckBool() === TRUE){
            $this->data['EmployeeID'] = $this->session->userdata('employee_id');
            $this->data['EmployeeName'] = $this->session->userdata('full_name');
            $this->data['UserProfileImage'] = $this->session->userdata('thumbnail');
            $this->data['dbCurrentDateTime'] = date("Y-m-d H:i:s");
            $this->data['dbCurrentDate'] = date("Y-m-d");
        }
        $this->data['noReply']="no-reply@parexons.com";
        $this->data['Company']="Parexons";
    }

}
