<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payroll_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->helper('extra_helper');
		date_default_timezone_set("Asia/Karachi");
	}
	//  Login function //

	// End of Login function //
	/// ********************************************* *** **** ****** ***** **** *********************************************///
	/// ============================================= Pay Roll Module Start Here ============================================///
	/// ********************************************* *** **** ****** ***** **** ********************************************///
	public function payroll_view_count()
	{
	$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,salary.base_salary,SUM(DISTINCT allowance.allowance_amount) as alwc_amount,SUM(DISTINCT deduction.deduction_amount) as ded_amount,SUM(DISTINCT loan_advances.amount) as lon_amount,((salary.base_salary+SUM(DISTINCT allowance.allowance_amount))-SUM(DISTINCT deduction.deduction_amount)-SUM(DISTINCT loan_advances.amount)) as payable
	,(transaction.calender_month),(transaction.calender_year),employment.employee_id')
	->from('employee')
	->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('position_management','position_management.employement_id=employment.employment_id')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
	->join('salary','salary.employement_id=employment.employment_id')
	
	->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
	 ->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id','LEFT')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
	->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
	->where('transaction.status',2)
    ->where('position_management.current',1)
	->group_by('salary.salary_id');

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	
	//return $this->datatables->generate();
	}
	
	public function payroll_view($limit,$page = NULL)
	{
		
	//$month=$this->input->post('month');	
	//$name=$this->input->post('name');
	$this->db->select(
                 'employment.employment_id,employee.employee_id,employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,SUM(DISTINCT CASE WHEN allowance.status=2 THEN allowance.allowance_amount ELSE 0 END) as allowance_amnt,SUM(DISTINCT CASE WHEN expense_claims.status=2 THEN expense_claims.amount ELSE 0 END) as  exp_amount,salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advance_payment.transaction_id > 0)THEN loan_advances.amount ELSE 0 END) as loan_amount,employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,(deduction.month) as ded_month,(deduction.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,salary_payment.salary_payment_id,ml_status.status_title,salary_payment.transaction_id,allowance.pay_frequency,deduction.deduction_frequency,transaction.transaction_date,(ml_month.month) as sal_month,(ml_year.year) as sal_year,(salary_payment.transaction_amount) as paid_salary')
	->from('employee')
	->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')	
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')
    ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
	->where('transaction.status',2)
    ->where('position_management.current',1)
	->order_by('transaction.transaction_id','desc')

	->group_by('transaction.transaction_id');
	//$this->db->where('salary_payment.transaction_id = 2');
	$this->db->limit($limit,$page);
	 if($name=$this->input->post('name'))
		{
			$this->db->like('employee.full_name',$name);
		}
	 if($month=$this->input->post('month'))
		{
			$this->db->where('salary_payment.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->db->where('salary_payment.year',$year);
		}
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	
	//return $this->datatables->generate();
	}
	public function tax_adjustment_view($limit,$page = NULL)
	{

		//$month=$this->input->post('month');
		//$name=$this->input->post('name');
		$this->db->select(
			'employment.employment_id,
			employee.employee_id,
			employee.employee_code,
			employee.full_name,
			ml_tax_slab.SlabNo,
			ml_financial_year.FinancialYear,
			employee_tax_statement.AnnualSalary,
			employee_tax_statement.MonthlySalary,
			employee_tax_statement.MontlySalaryTaxAmount,
			employee_tax_statement.TaxAreas,
			employee_tax_statement.TaxAdjustment,
			employee_tax_statement.TaxPayable')
			->from('employee')
			->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
     		->join('employee_tax_statement','employment.employment_id = employee_tax_statement.EmploymentID')
		    ->join('ml_tax_slab','employee_tax_statement.TaxSlabID = ml_tax_slab.TaxSlabID')
		    ->join('ml_financial_year','ml_tax_slab.TaxYearID = ml_financial_year.FinancialYearID');
		//$this->db->where('salary_payment.transaction_id = 2');
		$this->db->limit($limit,$page);
		if($name=$this->input->post('name'))
		{
			$this->db->where('employee.full_name',$name);
		}
//		if($month=$this->input->post('month'))
//		{
//			$this->db->where('salary_payment.month',$month);
//		}
		if($year=$this->input->post('year'))
		{
			$this->db->where('ml_financial_year.FinancialYearID',$year);
		}
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		//return $this->datatables->generate();
	}
	
	//Function for Payrollsalary Details
	public function payroll_salary_details($where)
	{
	$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,SUM(DISTINCT CASE WHEN allowance.status=2 THEN allowance.allowance_amount ELSE 0 END) as allowance_amnt,SUM(DISTINCT CASE WHEN expense_claims.status=2 THEN expense_claims.amount ELSE 0 END) as  exp_amount,salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advance_payment.transaction_id > 0)THEN loan_advances.amount ELSE 0 END) as loan_amount,employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,(deduction.month) as ded_month,(deduction.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,salary_payment.salary_payment_id,ST1.status,salary_payment.transaction_id,,allowance.pay_frequency,deduction.deduction_frequency')
	->from('employee')
	->where($where)
	->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')	
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employee_id=employee.employee_id','LEFT OUTER')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT OUTER')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT OUTER')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT OUTER')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT OUTER')
	//->WHERE('ml_pay_frequency.pay_frequency','Monthly')
	
	->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
        ->where('position_management.current',1);
	//->like('employee.full_name',$emp_name)
	//->like('salary_payment.month',$selc_month)
	//->like('salary_payment.year',$selc_year)
	//->group_by('salary_payment.salary_payment_id');

	
	//$this->db->limit($limit,$start);
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->row();
		}
	
	}
	
	
	//Function for Payroll Register Details
	public function payroll_regadvance_trans($id = NULL,$month = NULL,$year = NULL,$salary_trans_date = NULL)
	{

		$this->db->select('(transaction.transaction_amount) as advance_trans_amount,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as loan_recm,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
		SUM(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as total_advamount,
		(transaction.transaction_id) as adv_trans_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as payback_advance_id')
		->from('employee')
		->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
	    ->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
	   ->join
	   ('loan_advance_payback','loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id','LEFT')
		->join('transaction','transaction.transaction_id=loan_advance_payback.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',3)
            ->where('transaction.transaction_date',$salary_trans_date)
        ->where('loan_advance_payback.month',$month)
        ->where('loan_advance_payback.year',$year);
		
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	public function payroll_regexp_trans($id = NULL,$month= NULL,$year= NULL,$salary_trans_date = NULL)
	{
		$this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as exp_recm,
		SUM(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as total_exp_recm,
		(transaction.transaction_id) as exp_trans_id')
		->from('employee')
		->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
	    ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
		->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
	    ->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
		->join('transaction','transaction.transaction_id=expense_disbursment.transaction_id','LEFT')
        ->where('transaction.employment_id',$id)
        ->where('transaction.transaction_type',4)
        ->where('transaction.transaction_date',$salary_trans_date);
		
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	
	public function payroll_regallw_trans($id = NULL ,$month= NULL,$year= NULL,$salary_trans_date = NULL)
	{

		$this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
		SUM(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as total_alw_recm,
		(transaction.transaction_id) as alw_trans_id')
		->from('employee')

		->join('employment','employee.employee_id=employment.employee_id','LEFT')
	    ->join('allowance','employment.employment_id=allowance.employment_id','LEFT')
         ->join('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id','LEFT')
	    ->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
		->join('transaction','transaction.transaction_id=allowance_payment.transaction_id','LEFT')
        ->where('transaction.employment_id',$id)
        ->where('transaction.transaction_type',2)
        ->where('transaction.transaction_date',$salary_trans_date);
		
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	
	public function payroll_regded_trans($id = NULL,$month= NULL,$year= NULL,$salary_trans_date = NULL)
	{
        $this->db->select('deduction_processing.transaction_id,transaction.transaction_date,SUM(DISTINCT transaction.transaction_amount) as transaction_amount,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as total_ded_amount,
		(transaction.transaction_id) as ded_trans_id')
		->from('employee')

		->join('employment','employee.employee_id=employment.employee_id','LEFT')
	    ->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
		->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
	    ->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
		->join('transaction','transaction.transaction_id=deduction_processing.transaction_id','LEFT')
        ->where('transaction.employment_id',$id)
        ->where('transaction.transaction_type',1)
        ->or_where('transaction.transaction_type',8)
        ->where('transaction.transaction_date',$salary_trans_date);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	
	public function payment_mode_details($payment_mode_id)
	{
		$this->db->select("emp_bank_account.account_no,emp_bank_account.account_title,
		emp_bank_account.bank_name,emp_bank_account.branch,emp_bank_account.branch_code,payment_mode.account_id,payment_mode.cash_id,
		payment_mode.cheque_id,cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,
		cash_payment.received_by,cash_payment.remarks")
		->from('payment_mode')
		->join('emp_bank_account','emp_bank_account.bank_account_id=payment_mode.account_id','LEFT')
		->join('cheque_payments','cheque_payments.cheque_payment_id=payment_mode.cheque_id','LEFT')
		->join('cash_payment','cash_payment.cash_payment_id=payment_mode.cash_id','LEFT')
		->where('payment_mode.payment_mode_id',$payment_mode_id);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	
	public function payroll_reg_details($where,$month =NULL,$year =NULL,$salary_trans_date =NULL)
	{
	$this->db->select(
"employee.employee_id,employee.employee_code,employee.full_name,
ml_designations.designation_name,ml_pay_grade.pay_grade,
ml_month.month,ml_year.year,salary.base_salary,
(CASE WHEN (transaction.transaction_type=5 AND transaction.transaction_date=$salary_trans_date) THEN transaction.transaction_amount ELSE 0 END) as salary_trans,
salary_payment.transaction_amount,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,
salary_payment.salary_payment_id,ST1.transaction_date,ST1.status,
(ST1.approval_date) as ap_date,ST1.transaction_date,ST1.transaction_id,
ST1.approval_date,(Select full_name from employee where employee_id=ST1.approved_by) as name,
ml_status.status_title,salary_payment.salary_id")
	->from('employee')

	->where('transaction.transaction_date',$salary_trans_date)
    ->join('employment','employee.employee_id=employment.employee_id')
	->join('transaction','transaction.employment_id=employment.employment_id')

	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
   ->join('transaction ST1','salary_payment.transaction_id=ST1.transaction_id','LEFT')	
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_status','ST1.status=ml_status.status_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
	->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
    ->where($where)
    ->where('position_management.current',1);
	//->where('salary_payment.month',$month)
	//->where('salary_payment.year',$year)
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->row();
		}
	}


	public function month_salaries($where_emp)
	{
		$this->db->select('transaction.transaction_date,transaction.transaction_amount,
		salary_payment.arears,transaction.payment_mode,salary_payment.month,
		salary_payment.year,salary_payment.salary_payment_id,transaction.transaction_id')
		->from('employee')
		//->where('salary_payment.year',$year)
		->join('employment','employee.employee_id=employment.employee_id','LEFT')
	    ->join('salary','employment.employment_id=salary.employement_id','LEFT')
	    ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
		->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
        ->where($where_emp)
        ->where('transaction.status',2);
		$query=$this->db->get();
		if($query->num_rows() > 0){
            return $query->row();
        }
	}
    public function month_salaries_review($where_emp)
    {
        $this->db->select('transaction.transaction_date,transaction.transaction_amount,
		salary_payment.arears,transaction.payment_mode,salary_payment.month,
		salary_payment.year,salary_payment.salary_payment_id,transaction.transaction_id')
            ->from('employee')
            ->where($where_emp)

            //->where('salary_payment.year',$year)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('salary','employment.employment_id=salary.employement_id','LEFT')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
            ->where('transaction.status',3);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

	public function fail_review_info($where)
	{
		$this->db->select('salary_payment.salary_payment_id,fail_reviews.remarks,fail_reviews.review_id')
			->from('fail_reviews')
			->where($where)
			->join('salary_payment','fail_reviews.salary_payment_id=salary_payment.salary_payment_id','INNER');

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}

	public function review_transaction_request($salary_transaction_id,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id,$review_id)
	{
		$this->db->trans_start();
		$this->db->where('transaction_id',$salary_transaction_id);
		$query=$this->db->update('transaction',array('status'=>1));
		if(!empty($alw_trans_id))
		{
			$this->db->where('transaction_id',$alw_trans_id);
			$query=$this->db->update('transaction',array('status'=>1));
		}
		if(!empty($ded_trans_id))
		{
			$this->db->where('transaction_id',$ded_trans_id);
			$query=$this->db->update('transaction',array('status'=>1));
		}
		if(!empty($adv_trans_id))
		{
			$this->db->where('transaction_id',$adv_trans_id);
			$query=$this->db->update('transaction',array('status'=>1));
			$this->db->where('transaction_id',$adv_trans_id);
			$query=$this->db->update('loan_advance_payback',array('review_status'=>0));
		}
		if(!empty($exp_trans_id))
		{
			$this->db->where('transaction_id',$exp_trans_id);
			$query=$this->db->update('transaction',array('status'=>1));
			$this->db->where('transaction_id',$exp_trans_id);
			$query=$this->db->update('expense_disbursment',array('review_status'=>0));
		}
		$this->db->where('review_id',$review_id);
		$query=$this->db->update('fail_reviews',array('status'=>0));
		if($this->db->trans_status()=== FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();

	}
	
	public function year_salaries($where_emp,$year)
	{
		$this->db->select('(CASE WHEN transaction.status=2 THEN salary_payment.transaction_amount ELSE 0 END) as salary_amount,
		ml_year.year,ml_month.month,(salary_payment.month) as sal_month,salary_payment.transaction_amount,
		salary_payment.transaction_id')
		->from('employee')
		->where($where_emp)
		->where('transaction.status',2)
		->where('salary_payment.year',$year)
		->join('employment','employee.employee_id=employment.employee_id','LEFT')
	    ->join('salary','employment.employment_id=salary.employement_id','LEFT')
	    ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
		->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
		->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	    ->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
		->group_by('salary_payment.salary_payment_id');
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->result();}
		
		
	}
	// Function for employe names and id drop down //
	public  function employee_names() {
  		$this->db->select('full_name,employee_id');
  		$this->db->distinct();
  		$query = $this->db->get('employee');
  		if($query->num_rows() > 0) {
   		$data[''] = '-- Select Employee --';
   		foreach($query->result() as $row) {
    	        $data[$row->employee_id] = $row->full_name;
   	 	}
   		return $data;
 		}
	}
	
	// End of Function for employe names and id drop down //
	
	// Function for pay grade title and id drop down //
	public  function pay_grade() {
  		$this->db->select('pay_grade_title,pay_grade_id');
  		$this->db->distinct();
  		$query = $this->db->get('master_list_pay_grade');
  		if($query->num_rows() > 0) {
   		$data[''] = '-- Select Grade --';
   		foreach($query->result() as $row) {
    	        $data[$row->pay_grade_id] = $row->pay_grade_title;
   	 	}
   		return $data;
 		}
	}
	
	// End of Function for pay grade title and id drop down //
	
	
	
	// End of Function for Employee type  and id drop down //
	// Function for deduction title and id drop down //
	public  function allowance_type() {
  		$this->db->select('allowance_type,ml_allowance_type_id');
  		$this->db->distinct();
  		$query = $this->db->get('ml_allowance_type');
  		if($query->num_rows() > 0) {
   		$data[''] = '-- Select Allowance Type --';
   		foreach($query->result() as $row) {
    	        $data[$row->ml_allowance_type_id] = $row->allowance_type;
   	 	}
   		return $data;
 		}
	}
	
	// End of Function for deduction title and id drop down //
	
	
	// Function for deduction frequency and id drop down //
	public  function deduction_frequency() {
  		$this->db->select('deduction_id,deduction_frequency');
  		$this->db->distinct();
  		$query = $this->db->get('deduction');
  		if($query->num_rows() > 0) {
   		$data[''] = '-- Select Deduction Frequency --';
   		foreach($query->result() as $row) {
    	        $data[$row->deduction_id] = $row->deduction_frequency;
   	 	}
   		return $data;
 		}
	}
	
	// End of Function for deduction frequency and id drop down //
	/////////////////////check Deduction record for duplication/////////////////
	public function check_deduction_record($employee_id,$ded_type,$ded_freq= NULL)
	{
		$this->db->select('employment_id,ml_deduction_type_id,deduction_frequency');
		$this->db->from('deduction');
		$this->db->where('employment_id',$employee_id);
		$this->db->where('ml_deduction_type_id',$ded_type);
		//$this->db->where('deduction_frequency',$ded_freq);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{ return $query->row();}
	}
	//////////////////// END /////////////////////////////////////////////
	////////////// Function for insert deduction /////////////////
	public function add_deduction_records_monthly($deduction_monthly)
	{
		$this->db->trans_start();
		$query=$this->db->insert('deduction',$deduction_monthly);

		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End ADD deduction
	
	////////////// Function for insert deduction /////////////////
	public function add_deduction_records($deduction)
	{
		$this->db->trans_start();
		$query=$this->db->insert('deduction',$deduction);

		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End ADD deduction
	
	// Function for Add new record of deduction //
	//public function insert_records_deduction($insert)
//	{
//		$insertrec = $this->db->insert('deduction', $insert);
//			return $insertrec;
//	}
	
	// Function for Add new record of alowance //
	public function insert_records_allowance($insert)
	{
		$insertrec = $this->db->insert('allowance', $insert);
			return $insertrec;
	}
	
	// Function for deduction records autocomplete //
	
	public function get_autocomplete()
	{
	$name=$this->input->post('queryString');
	$query=$this->db->query("select * from employee
INNER JOIN employment on employment.employee_id=employee.employee_id
INNER JOIN posting on posting.employement_id=employment.employment_id
INNER JOIN position_management on position_management.employement_id=employment.employment_id
INNER JOIN ml_department on ml_department.department_id=posting.ml_department_id
INNER JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE position_management.current=1 AND full_name like '$name%'");
	
	return $query;
	}

	// End
	
	// Function for loan/advacne records autofill //
	
	public function get_autofill_loanad($loan_advance_id)
	{
	$query=$this->db->query("select 
employee.employee_id,employee.employee_code,employee.full_name,loan_advances.amount,
ml_designations.designation_name,ml_department.department_name,ml_payment_type.payment_type_title,
ml_month.ml_month_id,ml_year.ml_year_id,ml_month.month,ml_year.year,loan_advances.loan_advance_id,loan_advances.balance
FROM employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN loan_advances on loan_advances.employment_id=employment.employment_id

LEFT JOIN ml_payment_type on ml_payment_type.payment_type_id=loan_advances.payment_type
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
LEFT JOIN ml_month on loan_advances.month=ml_month.ml_month_id
LEFT JOIN ml_year on loan_advances.year=ml_year.ml_year_id
WHERE loan_advances.loan_advance_id = $loan_advance_id");
	if($query->num_rows()>0)
	{
		return $query->row();
		
	}
	}

	// End
	
	/// Auto complete for deduction /////
	public function get_autocomplete_deduction()
	{
	$name=$this->input->post('queryString');
	$query=$this->db->query("select employee.employee_id,employee.employee_code,employee.full_name,deduction.deduction_amount,ml_designations.designation_name,ml_department.department_name,ml_deduction_type.deduction_type 
from employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN salary on employment.employment_id=salary.employement_id
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
LEFT JOIN deduction on employment.employment_id=deduction.employment_id
LEFT JOIN ml_deduction_type on ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id
LEFT JOIN deduction_processing on deduction_processing.deduction_id=deduction.deduction_id
WHERE employment.current=1 AND position_management.current=1 AND position_management.status=2 AND salary.status=2 AND full_name like '$name%'
 GROUP BY employee.full_name");
	
	return $query;
	} 
	//end
	/// Auto complete for loan advance pay back /////
	public function get_autocomplete_loan_advance()
	{
	$name=$this->input->post('queryString');
	$query=$this->db->query("select employee.employee_code,employee.employee_id,employee.full_name,ml_designations.designation_name,ml_department.department_name,loan_advances.amount,loan_advances.loan_advance_id
	from employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN salary on employment.employment_id=salary.employement_id
LEFT JOIN loan_advances on loan_advances.employment_id=employment.employment_id
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE employment.current=1 AND position_management.current=1 AND position_management.status=2 AND salary.status=2 AND full_name like '$name%'
group by employee.employee_id");
	//$this->db->like('full_name',$this->input->post('queryString'),'both');
	return $query;
	}
	// END
	
	/// Auto complete for expense /////
	public function get_autocomplete_expense()
	{
	$name=$this->input->post('queryString');
	$query=$this->db->query("select  employee.employee_id,employee.employee_code,employee.full_name,expense_claims.amount,ml_designations.designation_name,ml_department.department_name,ml_expense_type.expense_type_title
FROM employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN salary on employment.employment_id=salary.employement_id
LEFT JOIN expense_claims on expense_claims.employment_id=employment.employment_id
LEFT JOIN ml_expense_type on ml_expense_type.ml_expense_type_id=expense_claims.expense_type
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE employment.current=1 AND position_management.current=1 AND position_management.status=2  AND salary.status=2 AND full_name like '$name%'
group by employee.employee_id");

	return $query;
	}
	// END
	
	/// Auto complete for loan advance pay back /////
	public function get_autocomplete_expenseback($exp_id)
	{
	//$name=$this->input->post('queryString');
	$query=$this->db->query("select *,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year 
	from employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN expense_claims on expense_claims.employment_id=employment.employment_id
LEFT JOIN ml_month on expense_claims.month=ml_month.ml_month_id
LEFT JOIN ml_year on expense_claims.year=ml_year.ml_year_id
LEFT JOIN ml_expense_type on ml_expense_type.ml_expense_type_id=expense_claims.expense_type
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE position_management.current=1 AND expense_claim_id = $exp_id");
	if($query->num_rows()>0)
	{
		return $query->row();
		
	}
	
	}
	// END
	/// Auto complete for loan advance pay back /////
	public function get_autocomplete_paysalary()
	{
	$name=$this->input->post('queryString');
	$query=$this->db->query("select employee.full_name,employee.employee_code,employee.CNIC,employee.employee_id,ml_designations.designation_name,ml_department.department_name,salary.base_salary,salary.salary_id
	 from employee
INNER JOIN employment on employment.employee_id=employee.employee_id
INNER JOIN salary on salary.employement_id=employment.employment_id
INNER JOIN posting on posting.employement_id=employment.employment_id
INNER JOIN position_management on position_management.employement_id=employment.employment_id
INNER JOIN ml_department on ml_department.department_id=posting.ml_department_id
INNER JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE position_management.current=1 AND full_name like '$name%'");
	//$this->db->like('full_name',$this->input->post('queryString'),'both');
	return $query;
	}
	// END
	
	
	// Function for deduction data view //
	public function deduction_count()
	{
		$query=$this->db->select('deduction.deduction_id')
	->from('deduction');

	$query=$this->db->get();
	if($query)
	{
		return $query->result();
	}
	}

	/// Auto complete of employees for accounts /////
	public function get_autocomplete_employees()
	{
		$name=$this->input->post('queryString');
		$query=$this->db->query("select employee.full_name,employee.employee_code,employee.CNIC,employee.employee_id,ml_designations.designation_name,ml_department.department_name,salary.base_salary
FROM employee
LEFT JOIN employment on employment.employee_id=employee.employee_id
LEFT JOIN salary on salary.employement_id=employment.employment_id
LEFT JOIN posting on posting.employement_id=employment.employment_id
LEFT JOIN position_management on position_management.employement_id=employment.employment_id
LEFT JOIN ml_department on ml_department.department_id=posting.ml_department_id
LEFT JOIN ml_designations on ml_designations.ml_designation_id=position_management.ml_designation_id
WHERE position_management.current=1 AND employment.current=1  AND full_name like '$name%'
GROUP BY employment.employee_id");
		//$this->db->like('full_name',$this->input->post('queryString'),'both');
		return $query;
	}
	// END
	
	public function deduction_view($limit,$page = NULL)
	{
	$query=$this->db->select('employee.employee_code,employee.full_name,salary.base_salary,
	ml_deduction_type.deduction_type,ml_pay_frequency.pay_frequency,deduction.deduction_amount,
	deduction.date_created,ml_status.status_title,deduction.deduction_id,deduction.status,
	(Select full_name from employee where employee_id=deduction.approved_by) as name,
	deduction.date_approved')
	->from('employee')
	->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
	->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
	->join('salary','salary.employement_id=employment.employment_id','LEFT')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT')
	->join('ml_month','ml_month.ml_month_id=deduction.month','LEFT')
	->join('ml_year','ml_year.ml_year_id=deduction.year','LEFT')
	->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
	->join('ml_status','ml_status.status_id=deduction.status')
	->group_by('deduction.deduction_id');
	$this->db->limit($limit,$page);

		if($name=$this->input->post('name'))
		{
			$this->db->like('employee.full_name',$name);
		}
	   /*if($month=$this->input->post('month'))
		{
			$this->db->where('deduction.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->db->where('deduction.year',$year);
		}*/
		if($ded_type=$this->input->post('ded_type'))
		{
			$this->db->where('deduction.ml_deduction_type_id',$ded_type);
		}
		if($frequency=$this->input->post('frequency'))
		{
			$this->db->where('deduction.deduction_frequency',$frequency);
		}
	$query=$this->db->get();
	if($query)
	{
		return $query->result();
	}
	}
	
	// End
	
	// Function for select deduction data for editing//
	public function deduction_data_fetch($where)
	{
		$this->db->select('deduction.deduction_id,employee.full_name,ml_deduction_type.deduction_type,ml_deduction_type.ml_deduction_type_id,deduction.deduction_frequency,deduction.deduction_amount,ml_month.ml_month_id,ml_month.month,ml_year.ml_year_id,ml_year.year,deduction.status,employee.employee_id,deduction.deduction_id,ml_pay_frequency.pay_frequency')
		->where($where)
		->from('employee')
		->join('employment','employee.employee_id=employment.employee_id','LEFT')
        ->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
		->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
		->join('deduction_processing','deduction_processing.deduction_id=deduction.deduction_id','LEFT')
		->join('ml_pay_frequency','deduction.deduction_frequency=ml_pay_frequency.ml_pay_frequency_id','LEFT')
		->join('ml_month','ml_month.ml_month_id=deduction_processing.month','LEFT')
		->join('ml_year','ml_year.ml_year_id=deduction_processing.year','LEFT');
		$query=$this->db->get();
		if($query->num_rows() > 0){
		return $query->row();}
		
	}
	// End
	///////////// check expense records///////////////////////
	public function check_expense_records($emp_id,$exp_type,$check_month,$check_year= NULL)
	{
		$this->db->select('employee_id,expense_type,month,year');
		$this->db->from('expense_claims');
		$this->db->where('employee_id',$emp_id);
		$this->db->where('expense_type',$exp_type);
		$this->db->where('month',$check_month);
		$this->db->where('month',$check_month);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{ return $query->row();}
	}
	
	///////////////////add new records expense/////////////////
	
	public function expense_new_record($tbl,$data)
	{	
	
		$this->db->trans_start();
		$query=$this->db->insert($tbl,$data);
		$expense_id=$this->db->insert_id();
		/*$query=$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$this->input->post('expamount'),
		'transaction_type'=>4,
		'employee_id'=>$this->input->post('emp_id')
		));
		$transaction_id=$this->db->insert_id();*/
		/*$query=$this->db->insert('expense_payment',array(
		'expense_id'=>$expense_id,
		'month'=>$month,
		'year'=>$year
		//'transaction_id'=>$transaction_id
		));*/
		
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	
	
	//END 
	
	//// Function for expense transactions
	public function expense_transaction_model($emp_id,$amount,$exp_id)
	{
		$this->db->trans_start();
		$query=$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$amount,
		'transaction_type'=>4,
		'employment_id'=>$emp_id
		));
		$transaction_id=$this->db->insert_id();
		$this->db->where(array('expense_id'=>$exp_id));
		$query=$this->db->update('expense_payment',array(
		'transaction_id'=>$transaction_id
		));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
	}
	// END
	
// Function for expense data view //
	public function expense_count()
	{
	$this->db->select('expense_claims.expense_claim_id')
	->from('expense_claims');

		$query=$this->db->get();
	    if($query->num_rows()>0)
	{
		return $query->result();
		
	}
	//return $this->datatables->generate();
	}
	
	public function expense_view($limit,$page = NULL)
	{
	$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
	ml_expense_type.expense_type_title,expense_claims.amount,expense_claims.expense_date,
	expense_claims.status,(expense_claims.expense_claim_id) as exp_id,
	expense_claims.expense_claim_id,employee.employee_id,expense_disbursment.expense_id,
	(expense_disbursment.month) as exp_dis_month,(expense_disbursment.year) as exp_dis_year,
	(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,
	expense_disbursment.transaction_id,(expense_disbursment.expense_id) as exp_dis_id,expense_claims.paid,
	expense_claims.file_name')
	->from('employee')
        ->join('employment','employment.employee_id=employee.employee_id','LEFT')
        ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
		->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
		->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
		->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id')
	  
	  ->join('ml_month','ml_month.ml_month_id=expense_claims.month','LEFT')
	  ->join('ml_year','ml_year.ml_year_id=expense_claims.year','LEFT')
	  ->join('ml_status','ml_status.status_id=expense_claims.status','LEFT')
	  ->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
	  ->limit($limit,$page)
	  ->order_by('expense_claims.expense_claim_id','desc')
        ->where('position_management.current',1)
	  ->group_by('expense_claims.expense_claim_id');

	
	if($month=$this->input->post('month'))
		{
			$this->db->where('expense_claims.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->db->where('expense_claims.year',$year);
		}
		if($exp_type=$this->input->post('exp_type'))
		{
			$this->db->where('expense_claims.expense_type',$exp_type);
		}
		$query=$this->db->get();
	    if($query->num_rows()>0)
	{
		return $query->result();
		
	}
	//return $this->datatables->generate();
	}
	
	// End
	
	public function payroll_expense_print($where)
	{
	$this->db->select('employee.employee_code,employee.full_name,
	ml_designations.designation_name,ml_pay_grade.pay_grade,salary.base_salary,
	ml_expense_type.expense_type_title,expense_claims.amount,expense_claims.expense_date,
	ml_status.status_title,(expense_claims.expense_claim_id) as exp_id,
	expense_claims.expense_claim_id,employee.employee_id,expense_claims.paid,expense_claims.month,expense_claims.year')
	->from('employee')
    ->join('employment','employment.employee_id=employee.employee_id','LEFT')
    ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
	->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
	->join('salary','salary.employement_id=employment.employment_id','LEFT')
	->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id','LEFT')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=expense_claims.month')
	->join('ml_year','ml_year.ml_year_id=expense_claims.year')
	->join('ml_status','ml_status.status_id=expense_claims.status')
	->where($where)
    ->where('position_management.current',1);

		$query=$this->db->get();
	    if($query->num_rows()>0)
	{
		return $query->row();
		
	}
	//return $this->datatables->generate();
	}
	
	// End
	
	// Function for expense disbursment data view //
	
	public function expense_disbursment_view()
	{
	$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
	ml_expense_type.expense_type_title,transaction.transaction_amount,
	(Select full_name from employee where employee_id=expense_claims.approved_by) as name,
	date_format(transaction.transaction_date,"%d-%m-%Y")',false)
	->from('employee')
    ->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
    ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
    ->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','INNER')
    ->join('transaction','transaction.transaction_id=expense_disbursment.transaction_id','LEFT')
	->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
	->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=expense_disbursment.month','LEFT')
	->join('ml_year','ml_year.ml_year_id=expense_disbursment.year','LEFT')
        ->where('position_management.current',1)
        ->group_by('expense_disbursment.transaction_id');

	
	//$this->datatables->order('expense_disbursment.expense_dis_id','desc');
	if($month=$this->input->post('month'))
		{
			$this->datatables->where('expense_disbursment.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->datatables->where('expense_disbursment.year',$year);
		}
		if($exp_type=$this->input->post('exp_type'))
		{
			$this->datatables->where('expense_claims.expense_type',$exp_type);
		}
	
	return $this->datatables->generate();
	}
	
	// End
	
	// Function for expension edit data fetch //
	
	public function expense_data_fetch($where)
	{
		$this->db->select('employee.full_name,ml_expense_type.expense_type_title,expense_claims.expense_type,expense_claims.amount,expense_claims.expense_date,expense_claims.status,expense_claims.expense_claim_id')
	->from('employee')
	->where($where)
    ->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('expense_claims','employment.employment_id=expense_claims.employment_id')
	//->join('ml_employment_type','ml_employment_type.employment_type_id=expense_claims.employment_type_id')
	->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type');
	$query=$this->db->get();
	return $query->row();
	}
	// End
	
	
	////////////// Function for insert Allowance /////////////////
	public function add_allowance_records($allowances,$month,$year)
	{
		$this->db->trans_start();
		$query=$this->db->insert('allowance',$allowances);
		$allowance_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$this->input->post('allowance_amount'),
		'transaction_type'=>2,
		'employment_id'=>$this->input->post('emp_id')
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('allowance_payment',array(
		'transaction_id'=>$trans_id,
		'allowance_id'=>$allowance_id,
		'month'=>$month,
		'year'=>$year
		));
		
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End ADD Allowance
	
	// Function for Allowance list records//
	
	public function allowance_view()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,salary.base_salary,ml_month.month,ml_year.year,ml_allowance_type.allowance_type,allowance.effective_date,allowance.allowance_amount,ml_status.status_title,allowance.allowance_id,employee.employee_id')
		->from('employee')
		->unset_column('employee.employee_id')
        ->join('employment','employment.employee_id=employee.employee_id')
       ->join('allowance','employment.employment_id=allowance.employment_id')
		->join('position_management','position_management.employement_id=employment.employment_id')
		->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
		->join('salary','salary.employement_id=employment.employment_id')
		->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id')
		->join('ml_month','ml_month.ml_month_id=allowance_payment.month')
		->join('ml_year','ml_year.ml_year_id=allowance_payment.year')
		->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id')
		->join('ml_status','ml_status.status_id=allowance.status')
        ->where('position_management.current',1);
		
		$this->datatables->edit_column('ml_status.status_title','<a href="payroll/allowance_status_update/$1/$2">$1</a>',			'ml_status.status_title,allowance.allowance_id');
		
		$this->datatables->edit_column('allowance.allowance_id',
		'<a href="payroll/edit_allowance/$1"><span class="fa fa-pencil"></span></a>','allowance.allowance_id');
		if($month=$this->input->post('month'))
		{
			$this->datatables->where('allowance_payment.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->datatables->where('allowance_payment.year',$year);
		}
		if($alw_type=$this->input->post('alw_type'))
		{
			$this->datatables->where('allowance.ml_allowance_type_id',$alw_type);
		}
		
		return $this->datatables->generate();
	}
	// End

	// Function for select allowance data for editing//
	public function allowance_data_fetch($where)
	{
		$this->db->select('allowance.allowance_id,employee.full_name,ml_allowance_type.ml_allowance_type_id,ml_pay_frequency.ml_pay_frequency_id,allowance.allowance_amount, ml_month.ml_month_id,ml_month.month,ml_year.ml_year_id,ml_year.year,allowance.effective_date,allowance.status,employee.employee_id,allowance.allowance_id')
		->where($where)
		->from('employee')
        ->join('employment','employment.employee_id=employee.employee_id')
		->join('allowance','employment.employment_id=allowance.employment_id')
		->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id')
		->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=allowance.pay_frequency')
		->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id')
		->join('ml_month','ml_month.ml_month_id=allowance_payment.month')
		->join('ml_year','ml_year.ml_year_id=allowance_payment.year');
		$query=$this->db->get();
		return $query->row();
		
	}
	// End
	
	// Function for increment data view //
	
	public function increment_view()
	{
	$this->datatables->select('employee.employee_code,employee.full_name,increments.date_effective,salary.base_salary,ml_increment_type.increment_type,increments.increment_amount,ml_month.month,ml_year.year,ml_status.status_title,employment.employee_id,increments.increment_id')
	->from('increments')
	//->unset_column('employee.employee_id')
	//->unset_column('increments.increment_id')
	->unset_column('employment.employee_id')
	->join('employee','employee.employee_id=increments.employee_id')
	->join('ml_increment_type','ml_increment_type.increment_type_id=increments.increment_type_id')
	->join('employment','employment.employee_id=employee.employee_id')
	->join('salary','salary.employement_id=employment.employment_id')
	->join('increments_processing','increments_processing.increment_id=increments.increment_id')
	->join('ml_month','ml_month.ml_month_id=increments_processing.month')
	->join('ml_year','ml_year.ml_year_id=increments_processing.year')
	->join('ml_status','ml_status.status_id=increments.status');
	//
	
	$this->datatables->edit_column('ml_status.status_title','<a href="payroll/increment_status_update/$1/$2">$1</a>','ml_status.status_title,increments.increment_id');
	
	$this->datatables->edit_column('increments.increment_id',
	'<a href="payroll/edit_increment/$1"><span class="fa fa-pencil"></span></a>',
	'increments.increment_id');
	
	if($month=$this->input->post('month'))
		{
			$this->datatables->where('increments_processing.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->datatables->where('increments_processing.year',$year);
		}
		if($incr_type=$this->input->post('incr_type'))
		{
			$this->datatables->where('increments.increment_type_id',$incr_type);
		}
	
	return $this->datatables->generate();
	}
	// End
	
	// Function for increment edit data fetch //
	
	public function increment_data_fetch($where)
	{
		$this->db->select('employee.full_name,ml_increment_type.increment_type_id,increments.increment_amount,increments.date_effective,increments.status,increments.increment_id')
	->from('increments')
	->where($where)
	->join('employee','employee.employee_id=increments.employee_id')
	->join('ml_increment_type','ml_increment_type.increment_type_id=increments.increment_type_id');
	$query=$this->db->get();
	return $query->row();
	}
	// End
	
	////////////// Function for insert increments /////////////////
	public function add_increments_records($increments,$month,$year)
	{
		$this->db->trans_start();
		$query=$this->db->insert('increments',$increments);
		$increments_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'employee_id'=>$this->input->post('emp_id')
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('increments_processing',array(
		'transaction_id'=>$trans_id,
		'increment_id'=>$increments_id,
		'month'=>$month,
		'year'=>$year
		));
		
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End ADD increments
	/////////// check advance request 
	public function check_advances($emp_id = NULL,$advance_type = NULL,$advance_month = NULL,$advance_year = NULL)
	{
		$this->db->select('employee.employee_id,payment_type,month,year');
		$this->db->from('employee');
        $this->db->join('employment','employment.employee_id = employee.employee_id AND employment.current = 1 AND employment.trashed = 0','inner');
        $this->db->join('loan_advances','loan_advances.employment_id = employment.employment_id','inner');

		$this->db->where('employment.employment_id',$emp_id);
		$this->db->where('payment_type',$advance_type);
		$this->db->where('month',$advance_month);
		$this->db->where('year',$advance_year);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{ return $query->row();}
	}
	// advance request insertion records //
	public function loan_advance_claim($loan_advance = NULL,$month= NULL,$year = NULL,$emp_id= NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END 
	
	
	// Function for loan and advance with transacton query //
	
	public function loan_advance_account($loan_advance = NULL,$month = NULL,$year = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();
		$query=$this->db->insert('loan_advance_payment',array(
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End
	
	// Transaction for cash //
	public function loan_advances_payment($loan_advance = NULL,$month = NULL,$year = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();
		
		$query=$this->db->insert('loan_advance_payment',array(
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END 
	
	// Transaction for cheque //
	public function loan_advance_cheque($loan_advance = NULL,$month = NULL,$year = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();
		
		$query=$this->db->insert('loan_advance_payment',array(
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		// END 
		
// Function For Loan and Advance view records //
///// check 
//////////////// Functions for loan advance transactions /////////////////////////

public function loan_advance_account_trans($loan_id = NULL,$payment_account = NULL,$month = NULL,$year = NULL,$emp_id = NULL,$account_id = NULL)
	{
		$this->db->trans_start();
		/*$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();*/
		if(!empty($account_id)){$acc_id=$account_id;}
		else{
		$query=$this->db->insert('emp_bank_account',$payment_account);
		$acc_id=$this->db->insert_id();}
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'account_id'			 =>$acc_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$this->input->post('payment_amount'),
		'transaction_type'=>3,
		'employment_id'=>$emp_id
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('loan_advance_id'=>$loan_advance_id));
		$query=$this->db->insert('loan_advance_payment',array(
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
		$this->db->where(array('loan_advance_id'=>$loan_id));
		$this->db->update('loan_advances',array('paid'=>1));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End
	
	// Transaction for cash //
	public function loan_advance_cash_trans($loan_id = NULL,$payment_cash = NULL,$month = NULL,$year = NULL,$emp_id = NULL)
	{
		$this->db->trans_start();
		/*$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();*/
		$query=$this->db->insert('cash_payment',$payment_cash);
		$cash_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cash_id'			     =>$cash_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$this->input->post('payment_amount'),
		'transaction_type'=>3,
		'employment_id'=>$emp_id
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('loan_advance_id'=>$loan_advance_id));
		$query=$this->db->insert('loan_advance_payment',array(
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
        $this->db->where(array('loan_advance_id'=>$loan_id));
        $this->db->update('loan_advances',array('paid'=>1));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END 
	
	// Transaction for cheque //
	public function loan_advance_cheque_trans($loan_id = NULL,$payment_cheque = NULL,$month = NULL,$year = NULL,$emp_id = NULL)
	{
		$this->db->trans_start();
		/*$query=$this->db->insert('loan_advances',$loan_advance);
		$loan_id=$this->db->insert_id();*/
		$query=$this->db->insert('cheque_payments',$payment_cheque);
		$cheque_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cheque_id'			 =>$cheque_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$this->input->post('payment_amount'),
		'transaction_type'=>3,
		'employment_id'=>$emp_id
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('loan_advance_id'=>$loan_advance_id));
		$query=$this->db->insert('loan_advance_payment',array(
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_id,
		 'month'=>$month,
		'year'=>$year
		));
        $this->db->where(array('loan_advance_id'=>$loan_id));
        $this->db->update('loan_advances',array('paid'=>1));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		// END 



//////////////////////// END of Functions for loan advance transactions /////////////////////////
	public function loan_advacne_count()
	{
	
		$where=array('loan_advances.trashed'=>0);
	$this->db->select('loan_advances.loan_advance_id,employee.employee_id')
	->from('employee')
	->join('employment','employee.employee_id=employment.employee_id','LEFT')
    ->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')
    ->where($where);
        $query=$this->db->get();
		if($query->num_rows() > 0)
		{ return $query->result();
		}
	}
		
	public function loan_advacne_view($limit = NULL,$page = NULL)
	{
		//$month=$this->input->post('month');
		//$year=$this->input->post('year');
		//$adnc_type=$this->input->post('adnc_type');
		$name=$this->input->post('name');
		$where=array('loan_advances.trashed'=>0);
	$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
	ml_payment_type.payment_type_title,ml_month.month,ml_year.year,loan_advances.amount,
	ml_status.status_title,(loan_advances.loan_advance_id) as loanadvances_id,
	loan_advances.loan_advance_id,employee.employee_id,
	loan_advances.date_created,loan_advances.status,
	(loan_advances.month) as loan_month,(loan_advances.year) as loan_year,
	loan_advances.paid,loan_advances.paid_back,(loan_advance_payment.transaction_id) as advance_trans_id,
	(transaction.status) as trans_status,loan_advances.balance')
	->from('employee')

    ->join('employment','employment.employee_id=employee.employee_id','LEFT')

	->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')

	->join('position_management','position_management.employement_id=employment.employment_id','LEFT')

	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')

	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')

    ->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id AND transaction.employment_id=employment.employment_id','LEFT')

    ->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')

	//->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=loan_advances.month','LEFT')

	->join('ml_year','ml_year.ml_year_id=loan_advances.year','LEFT')

	->join('ml_status','ml_status.status_id=loan_advances.status','LEFT')

	//->like('loan_advance_payment.month',$month)
	//->like('loan_advance_payment.year',$year)
	//->like('loan_advances.payment_type',$adnc_type)
    ->where($where)
    ->where('position_management.current',1)
	->like('employee.full_name',$name)
	->order_by('loan_advances.loan_advance_id','desc')
	->limit($limit,$page);
	$this->db->group_by('loan_advances.loan_advance_id');
	
	if($month=$this->input->post('month'))
	{
		$this->db->where('loan_advances.month',$month);
	}
	
	if($year=$this->input->post('year'))
	{
		$this->db->where('loan_advances.year',$year);
	}
	
	if($adnc_type=$this->input->post('adnc_type'))
	{
		$this->db->where('loan_advances.payment_type',$adnc_type);
	}
	
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{ return $query->result();
		}
	
	}
	// END
	
	//////////// view for loan paid 
	public function loan_advacnes_paid()
	{
		//$where=array('loan_advances.trashed'=>0);
	$this->datatables->select('employee.employee_code,employee.full_name,
	ml_designations.designation_name,ml_payment_type.payment_type_title,

	(Select full_name from employee where employee_id=loan_advances.approved_by) as name ,
	transaction.transaction_amount,date_format(transaction.transaction_date,"%d-%m-%Y") as date',false)
	->from('employee')
    ->join('employment','employment.employee_id=employee.employee_id','LEFT')
    ->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')

	//->join('transaction','transaction.employee_id=loan_advances.employment_id=employment.employment_id')
	->join('transaction','loan_advance_payback.transaction_id=transaction.transaction_id','LEFT')
	->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
	//->join('employment','employment.employee_id=employee.employee_id','LEFT')
	->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=loan_advance_payback.month','LEFT')
	->join('ml_year','ml_year.ml_year_id=loan_advance_payback.year','LEFT')
        ->where('position_management.current',1);
	//->join('ml_status','ml_status.status_id=loan_advances.status');
	$this->db->group_by('loan_advance_payback.loan_adv_payback_id','LEFT');
		
		 if($month=$this->input->post('month'))
		{
			$this->datatables->where('loan_advance_payback.month',$month);
		}
		if($year=$this->input->post('year'))
		{
			$this->datatables->where('loan_advance_payback.year',$year);
		}
		if($adnc_type=$this->input->post('adnc_type'))
		{
			$this->datatables->where('loan_advances.payment_type',$adnc_type);
		}
        
	return $this->datatables->generate();
	}
	// END
	
	public function loan_advance_rec($id = NULL)
	{
		$this->db->select('loan_advances.loan_advance_id,loan_advances.amount,
            employee.employee_code,employee.employee_id,employee.full_name,
            ml_designations.designation_name,ml_department.department_name,
            ml_payment_type.payment_type_title,ml_month.month,ml_year.year,loan_advances.balance')
		->from('employee')
        ->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
		->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')

		->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
	    ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
		->join('posting','posting.employement_id=employment.employment_id','LEFT')
		->join('ml_department','ml_department.department_id=posting.ml_department_id','LEFT')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
		//->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
		->join('ml_month','loan_advances.month=ml_month.ml_month_id','LEFT')
		->join('ml_year','loan_advances.year=ml_year.ml_year_id','LEFT')
		->where('loan_advances.loan_advance_id',$id)
            ->where('position_management.current',1);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	// Function for loan advance salary Print
	public function advanceloan_details($where = NULL)
	{
	$this->db->select('employee.full_name, ml_department.department_name,
	ml_designations.designation_name,loan_advances.loan_advance_id,
	loan_advances.amount,ml_payment_mode_types.payment_mode_type,
	ml_payment_mode_types.payment_mode_type,ml_payment_type.payment_type_title,
	transaction.transaction_date,ml_status.status_title,loan_advances.paid,
	loan_advances.paid_back,ml_month.month,ml_year.year')
	->from('employee')
        ->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
		->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')

		->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
		->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
		->join('posting','posting.employement_id=employment.employment_id','LEFT')
		->join('ml_department','ml_department.department_id=posting.ml_department_id','LEFT')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
		->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id','LEFT')
		->join('payment_mode','payment_mode.payment_mode_id=transaction.payment_mode','LEFT')
		//->join('cash_payment','cash_payment.cash_payment_id=payment_mode.cash_id')
		->join('ml_payment_mode_types','ml_payment_mode_types.payment_mode_type_id=payment_mode.payment_mode_type_id','LEFT')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
		->join('ml_status','ml_status.status_id=loan_advances.status','LEFT')
		->join('ml_month','ml_month.ml_month_id=loan_advances.month','LEFT')
		->join('ml_year','ml_year.ml_year_id=loan_advances.year','LEFT')
        ->where('position_management.current',1)
        ->where($where);
		$query=$this->db->get();
		return $query->row();
	
	
	}
	
	
	// END
	
	/////////////////////////// Function for fetch data for Edit loan and advance page ////////////////////////////////////
	
	public function check_payment_mode($id = NULL)
	{
		$where=array('loan_advances.loan_advance_id'=>$id);
		$this->db->select('loan_advances.loan_advance_id,ml_payment_mode_types.payment_mode_type_id,ml_payment_mode_types.payment_mode_type,payment_mode.payment_mode_type_id,payment_mode.payment_mode_id,transaction.transaction_id,loan_advance_payment.loan_advance_payment_id,payment_mode.payment_mode_type_id')
		->from('loan_advancesS')
		->where($where)
		//->join('employee','employee.employee_id=loan_advances.employment_id=employment.employment_id')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
		->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id')
		->join('payment_mode','payment_mode.payment_mode_id=transaction.payment_mode','LEFT')
		->join('ml_payment_mode_types','ml_payment_mode_types.payment_mode_type_id=payment_mode.payment_mode_type_id');
		//$this->db->where($where);
		$query=$this->db->get();
		return $query->row();
	}
	
	public function loan_advance_fetch($id = NULL)
	{
		$where=array('loan_advances.loan_advance_id'=>$id);
		$this->db->select('employee.employee_id,employee.full_name,
		loan_advances.loan_advance_id,loan_advances.amount,
		ml_payment_type.payment_type_id,ml_month.month,ml_year.year,loan_advances.month,
		loan_advances.year,ml_payment_type.payment_type_id,
		ml_payment_type.payment_type_title')
		->from('employee')
		->join('employment','employee.employee_id=employment.employee_id','LEFT')
		->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
		->join('ml_month','loan_advance_payment.month=ml_month.ml_month_id','LEFT')
		->join('ml_year','loan_advance_payment.year=ml_year.ml_year_id','LEFT')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
		return $query->row();
		}
	}
	
	public function loan_advance_fetch_account($id = NULL)
	{
		$where=array('loan_advances.loan_advance_id'=>$id);
		$query=$this->db->select('employee.full_name,loan_advances.loan_advance_id,loan_advances.amount,payment_mode.payment_mode_type_id,payment_mode.payment_mode_id,payment_mode.account_id,ml_payment_type.payment_type_id,emp_bank_account.bank_account_id,emp_bank_account.account_no,emp_bank_account.account_title,emp_bank_account.bank_name,emp_bank_account.branch,transaction.transaction_id,loan_advance_payment.loan_advance_payment_id,ml_status.status_id')
		->from('loan_advances')
		->join('employee','employee.employee_id=loan_advances.employment_id=employment.employment_id')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
		->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id')
		->join('payment_mode','payment_mode.payment_mode_id=transaction.payment_mode')
		->join('emp_bank_account','emp_bank_account.bank_account_id=payment_mode.account_id')
		->join('ml_payment_mode_types','ml_payment_mode_types.payment_mode_type_id=payment_mode.payment_mode_type_id')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type')
		//->join('transaction','transaction.employee_id=loan_advances.employment_id=employment.employment_id')
		//->join('loan_advance_payment','loan_advance_payment.transaction_id=transaction.transaction_id')
		->join('ml_status','ml_status.status_id=loan_advances.status');
		$this->db->where($where);
		$query=$this->db->get();
		return $query->row();
	}
	
	public function loan_advance_fetch_cheque($id = NULL)
	{
		$where=array('loan_advances.loan_advance_id'=>$id);
		$query=$this->db->select('employee.full_name,loan_advances.loan_advance_id,loan_advances.amount,payment_mode.payment_mode_type_id,payment_mode.payment_mode_id,payment_mode.cheque_id,ml_payment_type.payment_type_id,cheque_payments.cheque_payment_id,cheque_payments.cheque_no,cheque_payments.bank_name,transaction.transaction_id,loan_advance_payment.loan_advance_payment_id,ml_status.status_id')
		->from('loan_advances')
		->join('employee','employee.employee_id=loan_advances.employment_id=employment.employment_id')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
		->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id')
		->join('payment_mode','payment_mode.payment_mode_id=transaction.payment_mode')
		->join('ml_payment_mode_types','ml_payment_mode_types.payment_mode_type_id=payment_mode.payment_mode_type_id')
		->join('cheque_payments','cheque_payments.cheque_payment_id=payment_mode.cheque_id')
		->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type')
		//->join('transaction','transaction.employee_id=loan_advances.employment_id=employment.employment_id')
		//->join('loan_advance_payment','loan_advance_payment.transaction_id=transaction.transaction_id')
		->join('ml_status','ml_status.status_id=loan_advances.status');
		//$this->db->distinct('loan_advances.loan_advance_id');
		$this->db->where($where);
		$query=$this->db->get();
		return $query->row();
	}
	
	public function update_status($table,$where,$data){
            $this->db->where($where);
            $query = $this->db->update($table,$data);
	       if($query){
	        return true;
		}else {
		return false;
		}
	}
	
	///////////////////////////////////////////// End
	
	////////////////// Cluster Functions for Updation Loan and Advances  ////////////////////////////////////
	
		// UPDATION Transaction for cash //////////////////////////////
		public function loan_advance_cash_update($loan_advance_up = NULL,$loan_advance_id = NULL,$payment_cash_up = NULL,$cash_payment_id = NULL,
		$payment_mode_id = NULL,$transaction_id = NULL,$loan_advance_payment_id = NULL)
		{
			$this->db->trans_start();
			$this->db->where($loan_advance_id);
			$query=$this->db->update('loan_advances',$loan_advance_up);
			$this->db->where($cash_payment_id);
			$query=$this->db->update('cash_payment',$payment_cash_up);

			if ($this->db->trans_status() === FALSE)
			{
				 $this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			return $query;
			$this->db->trans_complete();
			} 
			//END 
			
			// UPDATION Transaction for account ///////////////////////////
			public function loan_advance_account_update($loan_advance_up = NULL,$loan_advance_id = NULL,$payment_account_up = NULL,$account_payment_id = NULL,$payment_mode_id = NULL,$transaction_id = NULL,$loan_advance_payment_id = NULL)
		{
			$this->db->trans_start();
			$this->db->where($loan_advance_id);
			$query=$this->db->update('loan_advances',$loan_advance_up);
			$this->db->where($account_payment_id);
			$query=$this->db->update('emp_bank_account',$payment_account_up);
			
			if ($this->db->trans_status() === FALSE)
			{
				 $this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			return $query;
			$this->db->trans_complete();
			}
		// END
		
		// UPDATION Transaction for account ///////////////////////////
			public function loan_advance_cheque_update($loan_advance_up = NULL,$loan_advance_id = NULL,$payment_cheque_up = NULL,$cheque_payment_id = NULL,$payment_mode_id = NULL,$transaction_id = NULL,$loan_advance_payment_id = NULL)
		{
			$this->db->trans_start();
			$this->db->where($loan_advance_id);
			$query=$this->db->update('loan_advances',$loan_advance_up);
			$this->db->where($cheque_payment_id);
			$query=$this->db->update('cheque_payments',$payment_cheque_up);
			$this->db->where($payment_mode_id);
			$query=$this->db->update('payment_mode',array(
			'payment_mode_type_id'	=>$this->input->post('payment_mode'),
			'cheque_id'			     =>$this->input->post('cheque_payment_id')
			));
			$this->db->where($transaction_id);
			$this->db->update('transaction',array(
			'payment_mode'=>$this->input->post('payment_mode_id'),
			'calender_year'=>date('Y'),
			'calender_month'=>date('M'),
			'transaction_date'=>date('Y-m-d'),
			'transaction_time'=>date('h:i:s')
			
			));
			$this->db->where($loan_advance_payment_id);
			$query=$this->db->update('loan_advance_payment',array(
			'transaction_id'=>$this->input->post('transaction_id'),
			'loan_advance_id'=>$this->input->post('loan_advance_id')
			));
			if ($this->db->trans_status() === FALSE)
			{
				 $this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			return $query;
			$this->db->trans_complete();
			}
			
	// END 
	
	
	// Function for Expense Disbursment with transacton query //
	
	    		/// Function for expense disurtion through account ///
	public function expense_disburment_account($payment_account = NULL,$exp_id = NULL,$month = NULL,$year = NULL,$paid_amount = NULL,$employmentID = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('emp_bank_account',$payment_account);
		$acc_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'account_id'			 =>$acc_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_type'=>6,
		'transaction_amount'=>$paid_amount,
		'transaction_time'=>date('H:i:s'),
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$trans_id,
		'expense_id'=>$exp_id,
		//'paid_amount'=>$paid_amount,
		'month'=>$month,
		'year'=>$year
		));
		$paid=array('paid'=>1);
		$this->db->where('expense_claim_id',$exp_id);
		$query=$this->db->update('expense_claims',$paid);
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End account
	
	/// Function for expense disurtion through cash ///

	/**
	 * @param $payment_cash
	 * @param $exp_id
	 * @param $month
	 * @param $year
	 * @param $paid_amount
	 * @return mixed
	 * @param $paid
	 */
	public function expense_disburment_cash($payment_cash = NULL,$exp_id = NULL,$month = NULL,$year = NULL,$paid_amount = NULL,$employmentID = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('cash_payment',$payment_cash);
		$cash_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cash_id'			     =>$cash_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_type'=>6,
		'transaction_amount'=>$paid_amount,
		'transaction_time'=>date('H:i:s'),
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$trans_id,
		'expense_id'=>$exp_id,
		//'paid_amount'=>$paid_amount,
		'month'=>$month,
		'year'=>$year
		));
		$paid=array('paid'=>1);
		$this->db->where('expense_claim_id',$exp_id);
		$query=$this->db->update('expense_claims',$paid);
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END cash
	
	/// Function for expense disurtion through cheque ///
	public function expense_disburment_cheque($payment_cheque,$exp_id,$month,$year,$paid_amount,$employmentID)
	{
		$this->db->trans_start();
		$query=$this->db->insert('cheque_payments',$payment_cheque);
		$cheque_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cheque_id'			 =>$cheque_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_type'=>6,
		'transaction_amount'=>$paid_amount,
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$trans_id,
		'expense_id'=>$exp_id,
		//'paid_amount'=>$paid_amount,
		'month'=>$month,
		'year'=>$year
		));
		$paid=array('paid'=>1);
		$this->db->where('expense_claim_id',$exp_id);
		$query=$this->db->update('expense_claims',$paid);
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		// END cheque
		
		
	////////////////// End of Cluster Functions for Updation Loan and Advances  //////////////////////////////
	
	//////////////////  Functions for Pay Salary to Employee  //////////////////////////////////////////////////
	
	////// Function for cheque////////
	public function salary_pay_model($salary_id = NULL,$pay_amount = NULL,$month = NULL,$year = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('salary_payment',array(
		'salary_id'=>$salary_id,
		'month'=>$month,
		'year'=>$year,
		'transaction_amount'=>$pay_amount));
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		///// END
	
/////////////////////////////////////salary transaction////////////////////////
	
	
	////// Function for cheque////////
	public function salary_trans_cheque($salary_id ,$payment_cheque,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$deductible_amount)
	{
        $employment_id = get_employment_from_employeeID($this->input->post('emp_id'));
		$this->db->trans_start();
		$query=$this->db->insert('cheque_payments',$payment_cheque);
		$cheque_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cheque_id'			 =>$cheque_id
		));
		$paymenmod_id=$this->db->insert_id();
		///////////// for deduction
		if($deduction_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$deduction_amount,
		'transaction_type'=>1,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_ded=$this->db->insert_id();
		foreach($ded_ids as $ded_idd){
		$this->db->insert('deduction_processing',array(
		'transaction_id'	=>$transaction_id_ded,
		'deduction_id'  	=>$ded_idd,
		'month'				=>$month,
		'year'				=>$year
		));
		}
		}
        // code for absentee DEDUCTION
        if($deductible_amount > 0)
        {
            $this->db->insert('deduction',array(
                'employment_id'=>$employment_id,
                'ml_deduction_type_id'=>6,
                'deduction_amount'=>$deductible_amount,
                'month'=>$month,
                'year'=>$year,
                'deduction_frequency'=>3,
                'created_by'=>$this->data['EmployeeID'],
                'date_created'=>date("Y-m-d"),
                'approved_by'=>$this->data['EmployeeID'],
                'date_approved'=>date("Y-m-d"),
                'status'=>2,
                'trashed'=>0
            ));
            $deduction_id=$this->db->insert_id();
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$deductible_amount,
                'transaction_type'=>8,
                'employment_id'=>$employment_id
            ));
            $transaction_id_ded=$this->db->insert_id();
            $this->db->insert('deduction_processing', array(
                'transaction_id' => $transaction_id_ded,
                'deduction_id' => $deduction_id,
                'month' => $month,
                'year' => $year
            ));

        }
		////////////// For Allowance
		if($allowance_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$allowance_amount,
		'transaction_type'=>2,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_alw=$this->db->insert_id();
		foreach($alw_ids as $alw_idd){
		$this->db->insert('allowance_payment',array(
		'transaction_id'	=>$transaction_id_alw,
		'allowance_id'  	=>$alw_idd,
		'month'				=>$month,
		'year'				=>$year
		));
		}
		}
		//////////// for expense
		if($expense_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$expense_amount,
		'transaction_type'=>4,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_exp=$this->db->insert_id();
		//$count=0;
		foreach($exp_ids as $exp_idd){
		$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$transaction_id_exp,
		'expense_id'    =>$exp_idd,
		'month'         =>$month,
		'year'          =>$year
		));
		//$count++;
		$this->db->where('expense_claim_id',$exp_idd);
		$query=$this->db->update('expense_claims',array('paid'=>1));
		}
		}
		if($advance_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$advance_amount,
		'transaction_type'=>3,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_advance=$this->db->insert_id();
		foreach($advance_id as $loan_ids){
		$this->db->insert('loan_advance_payback',array(
		'transaction_id'=>$transaction_id_advance,
		'loan_advance_id'=>$loan_ids,
		'month'=>$month,
		'year'=>$year
		));
		$this->db->where(array('loan_advance_id'=>$loan_ids));
		$this->db->update('loan_advances',array('paid_back'=>1));
		}
		}
		if(!empty($partial_pay))
		{$salary_amount=$partial_pay;
		$balance=$arears;
		}
		else
		{$salary_amount=$pay_amount;
		$balance=0;
		}
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$salary_amount,
		'transaction_type'=>5,
		'employment_id'=>$this->input->post('emp_id')
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('salary_payment_id'=>$salary_payment_id));
		$query=$this->db->insert('salary_payment',array(
		'salary_id'			=>$salary_id,
		'month'				=>$month,
		'year'				=>$year,
		'transaction_id'	=>$trans_id,
		'transaction_amount'=>$salary_amount,
		'arears'			=>$balance));
		/*if(!empty($prvarears))
		{$paid_arrears=$prvarears;}
		else{$paid_arrears=0;}
		$this->db->where('salary_payment_id',$salary_payment_id);
		$query=$this->db->update('salary_payment',array('paid_arears'=>$paid_arrears));*/
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		///// END
		
		/// Function for account ///
	public function salary_trans_account($account_id,$salary_id,$payment_account,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$total_adv_amount,$advance_balance,$remain_balance,$deductible_amount)
	{
        $employment_id = get_employment_from_employeeID($this->input->post('emp_id'));
		$this->db->trans_start();
		
		if(!empty($account_id)){$acc_id=$account_id;}
		else{
		$query=$this->db->insert('emp_bank_account',$payment_account);
		$acc_id=$this->db->insert_id();
        }
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'account_id'			 =>$acc_id
		));
		$paymenmod_id=$this->db->insert_id();
		if($deduction_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$deduction_amount,
		'transaction_type'=>1,
		'employment_id'=>$employment_id
		));
		$transaction_id_ded=$this->db->insert_id();
		foreach($ded_ids as $ded_idd){
		$this->db->insert('deduction_processing',array(
		'transaction_id'	=>$transaction_id_ded,
		'deduction_id'  	=>$ded_idd,
		'month'				=>$month,
		'year'				=>$year
		));
		}
		}
        // code for absentee DEDUCTION
        if($deductible_amount > 0)
        {
            $this->db->insert('deduction',array(
                'employment_id'=>$employment_id,
                'ml_deduction_type_id'=>6,
                'deduction_amount'=>$deductible_amount,
                'month'=>$month,
                'year'=>$year,
                'deduction_frequency'=>3,
                'created_by'=>$this->data['EmployeeID'],
                'date_created'=>date("Y-m-d"),
                'approved_by'=>$this->data['EmployeeID'],
                'date_approved'=>date("Y-m-d"),
                'status'=>2,
                'trashed'=>0
            ));
            $deduction_id=$this->db->insert_id();
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$deductible_amount,
                'transaction_type'=>8,
                'employment_id'=>$employment_id
            ));
            $transaction_id_ded=$this->db->insert_id();
             $this->db->insert('deduction_processing', array(
                'transaction_id' => $transaction_id_ded,
                'deduction_id' => $deduction_id,
                'month' => $month,
                'year' => $year
            ));

        }
		////////////// For Allowance
		if($allowance_amount > 0){
			$this->db->insert('transaction',array(
			'calender_year'=>date('Y'),
			'calender_month'=>date('M'),
			'transaction_date'=>date('Y-m-d'),
			'transaction_time'=>date('H:i:s'),
			'transaction_amount'=>$allowance_amount,
			'transaction_type'=>2,
			'employment_id'=>$employment_id
			));
			$transaction_id_alw=$this->db->insert_id();
			foreach($alw_ids as $alw_idd){
			$this->db->insert('allowance_payment',array(
			'transaction_id'	=>$transaction_id_alw,
			'allowance_id'  	=>$alw_idd,
			'month'				=>$month,
			'year'				=>$year
				));
			}
		}
		//////////// for expense
		if($expense_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$expense_amount,
		'transaction_type'=>4,
		'employment_id'=>$employment_id
		));
		$transaction_id_exp=$this->db->insert_id();
		//return $exp_ids;
		foreach($exp_ids as $exp_idd){
		$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$transaction_id_exp,
		'expense_id'    =>$exp_idd,
		'month'         =>$month,
		'year'          =>$year
		));
		//$count++;
		$this->db->where('expense_claim_id',$exp_idd);
		$query=$this->db->update('expense_claims',array('paid'=>1));
		}
		
		}
		if($advance_amount > 0){
            foreach($advance_id as $loan_ids) {
                if($advance_amount == $total_adv_amount)
                {
                    $this->db->where(array('loan_advance_id'=>$loan_ids));
                    $this->db->update('loan_advances',array('balance'=>0,'paid_back'=>1));
                }

            if($total_adv_amount > $advance_amount)
            {
                    $balance_ad= $total_adv_amount - $advance_amount;
                    $this->db->where(array('loan_advance_id' => $loan_ids));
                    $this->db->update('loan_advances',array('balance'=>$balance_ad,'paid_back' => 0));

            }
            }

		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$advance_amount,
		'transaction_type'=>3,
		'employment_id'=>$employment_id
		));
		$transaction_id_advance=$this->db->insert_id();
		foreach($advance_id as $loan_ids){
		$this->db->insert('loan_advance_payback',array(
		'transaction_id'=>$transaction_id_advance,
		'loan_advance_id'=>$loan_ids,
		'month'=>$month,
		'year'=>$year
		));
		//$this->db->where(array('loan_advance_id'=>$loan_ids));
		//$this->db->update('loan_advances',array('paid_back'=>1));
		}
		}
		if(!empty($partial_pay))
		{$salary_amount=$partial_pay;
		$balance=$arears;
		}
		else
		{$salary_amount=$pay_amount;
		$balance=0;
		}
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$salary_amount,
		'transaction_type'=>5,
		'employment_id'=>$employment_id
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('salary_payment_id'=>$salary_payment_id));
		$query=$this->db->insert('salary_payment',array(
		'salary_id'			=>$salary_id,
		'month'				=>$month,
		'year'				=>$year,
		'transaction_id'	=>$trans_id,
		'transaction_amount'=>$salary_amount,
		'arears'			=>$balance));
		/*if(!empty($prvarears))
		{$paid_arrears=$prvarears;}
		else{$paid_arrears=0;}
		$this->db->where('salary_payment_id',$salary_payment_id);
		$query=$this->db->update('salary_payment',array('paid_arears'=>$paid_arrears));*/
		
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End account
	
	/// Function for  cash ///
	public function salary_trans_cash($salary_id,$payment_cash,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$deductible_amount)
	{
        $employment_id = get_employment_from_employeeID($this->input->post('emp_id'));
		$this->db->trans_start();
		$query=$this->db->insert('cash_payment',$payment_cash);
		$cash_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cash_id'			     =>$cash_id
		));
		$paymenmod_id=$this->db->insert_id();
		if($deduction_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$deduction_amount,
		'transaction_type'=>1,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_ded=$this->db->insert_id();
		foreach($ded_ids as $ded_idd){
		$this->db->insert('deduction_processing',array(
		'transaction_id'	=>$transaction_id_ded,
		'deduction_id'  	=>$ded_idd,
		'month'				=>$month,
		'year'				=>$year
		));
		}
		}
        // code for absentee DEDUCTION
        if($deductible_amount > 0)
        {
            $this->db->insert('deduction',array(
                'employment_id'=>$employment_id,
                'ml_deduction_type_id'=>6,
                'deduction_amount'=>$deductible_amount,
                'month'=>$month,
                'year'=>$year,
                'deduction_frequency'=>3,
                'created_by'=>$this->data['EmployeeID'],
                'date_created'=>date("Y-m-d"),
                'approved_by'=>$this->data['EmployeeID'],
                'date_approved'=>date("Y-m-d"),
                'status'=>2,
                'trashed'=>0
            ));
            $deduction_id=$this->db->insert_id();
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$deductible_amount,
                'transaction_type'=>8,
                'employment_id'=>$employment_id
            ));
            $transaction_id_ded=$this->db->insert_id();
            $this->db->insert('deduction_processing', array(
                'transaction_id' => $transaction_id_ded,
                'deduction_id' => $deduction_id,
                'month' => $month,
                'year' => $year
            ));

        }
		////////////// For Allowance
		if($allowance_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$allowance_amount,
		'transaction_type'=>2,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_alw=$this->db->insert_id();
		foreach($alw_ids as $alw_idd){
		$this->db->insert('allowance_payment',array(
		'transaction_id'	=>$transaction_id_alw,
		'allowance_id'  	=>$alw_idd,
		'month'				=>$month,
		'year'				=>$year
		));
		}
		}
		//////////// for expense
		if($expense_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$expense_amount,
		'transaction_type'=>4,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_exp=$this->db->insert_id();
		foreach($exp_ids as $exp_idd){
		$this->db->insert('expense_disbursment',array(
		'transaction_id'=>$transaction_id_exp,
		'expense_id'    =>$exp_idd,
		'month'         =>$month,
		'year'          =>$year
		));
		//$count++;
		$this->db->where('expense_claim_id',$exp_idd);
		$query=$this->db->update('expense_claims',array('paid'=>1));
		}
		}
		if($advance_amount > 0){
		$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$advance_amount,
		'transaction_type'=>3,
		'employment_id'=>$this->input->post('emp_id')
		));
		$transaction_id_advance=$this->db->insert_id();
		foreach($advance_id as $loan_ids){
		$this->db->insert('loan_advance_payback',array(
		'transaction_id'=>$transaction_id_advance,
		'loan_advance_id'=>$loan_ids,
		'month'=>$month,
		'year'=>$year
		));
		$this->db->where(array('loan_advance_id'=>$loan_ids));
		$this->db->update('loan_advances',array('paid_back'=>1));
		}
		}
		if(!empty($partial_pay))
		{$salary_amount=$partial_pay;
		$balance=$arears;
		}
		else
		{$salary_amount=$pay_amount;
		$balance=0;
		}
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$salary_amount,
		'transaction_type'=>5,
		'employment_id'=>$this->input->post('emp_id')
		));
		$trans_id=$this->db->insert_id();
		//$this->db->where(array('salary_payment_id'=>$salary_payment_id));
		$query=$this->db->insert('salary_payment',array(
		'salary_id'			=>$salary_id,
		'month'				=>$month,
		'year'				=>$year,
		'transaction_id'	=>$trans_id,
		'transaction_amount'=>$salary_amount,
		'arears'			=>$balance));
		/*if(!empty($prvarears))
		{$paid_arrears=$prvarears;}
		else{$paid_arrears=0;}
		$this->db->where('salary_payment_id',$salary_payment_id);
		$query=$this->db->update('salary_payment',array('paid_arears'=>$paid_arrears));*/
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END cash
	///////////////////END Functions for Pay Salary to Employee////////////////////////////////
	
	//////////////////  Functions for Functions for loan and advance pay back  //////////////////////////////////////////////////
	
	////// Function for cheque////////
	public function loan_advance_payback_cheque($payment_cheque,$pay_amount,$loan_advance_id,$month,$year,$total_amount, $employmentID = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('cheque_payments',$payment_cheque);
		$cheque_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cheque_id'			 =>$cheque_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$pay_amount,
        'transaction_type'=>3,
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('loan_advance_payback',array(
		'payment_mode_id'=>$paymenmod_id,
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_advance_id,
		'paid_amount'=>$pay_amount,
		'month'=>$month,
		'year'=>$year
		));
        if($pay_amount < $total_amount)
        {
            $balance=$total_amount - $pay_amount;
            $this->db->where(array('loan_advance_id'=>$loan_advance_id));
            $this->db->update('loan_advances',array('paid_back'=>0,'balance'=>$balance));
        }
        if($pay_amount == $total_amount)
        {
            $balance=0;
            $this->db->where(array('loan_advance_id'=>$loan_advance_id));
            $this->db->update('loan_advances',array('paid_back'=>1,'balance'=>$balance));
        }
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
		///// END
		
			/// Function for account ///
	public function loan_advance_payback_account($payment_account,$pay_amount,$loan_advance_id,$month,$year,$account_id,$total_amount,$employmentID = NULL)
	{
		$this->db->trans_start();
		if(!empty($account_id)){$acc_id=$account_id;}
		else{
		$query=$this->db->insert('emp_bank_account',$payment_account);
		$acc_id=$this->db->insert_id();}
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'account_id'			 =>$acc_id
		));
		$paymenmod_id=$this->db->insert_id();
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$pay_amount,
        'transaction_type'=>3,
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('loan_advance_payback',array(
		'payment_mode_id'=>$paymenmod_id,
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_advance_id,
		'paid_amount'=>$pay_amount,
		'month'=>$month,
		'year'=>$year
		));
        if($pay_amount < $total_amount)
        {
        $balance=$total_amount - $pay_amount;
		$this->db->where(array('loan_advance_id'=>$loan_advance_id));
		$this->db->update('loan_advances',array('paid_back'=>0,'balance'=>$balance));
        }
        if($pay_amount == $total_amount)
        {
            $balance=0;
            $this->db->where(array('loan_advance_id'=>$loan_advance_id));
            $this->db->update('loan_advances',array('paid_back'=>1,'balance'=>$balance));
        }
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	// End account
	
	/// Function for  cash ///
	public function loan_advance_payback_cash($payment_cash,$pay_amount,$loan_advance_id,$month,$year,$total_amount,$employmentID = NULL)
	{
		$this->db->trans_start();
		$query=$this->db->insert('cash_payment',$payment_cash);
		$cash_id=$this->db->insert_id();
		$query=$this->db->insert('payment_mode',array(
		'payment_mode_type_id'	=>$this->input->post('payment_mode'),
		'cash_id'			     =>$cash_id
		));
		$paymenmod_id=$this->db->insert_id();
		
		$this->db->insert('transaction',array(
		'payment_mode'=>$paymenmod_id,
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('H:i:s'),
		'transaction_amount'=>$pay_amount,
        'transaction_type'=>3,
		'employment_id'=>$employmentID
		));
		$trans_id=$this->db->insert_id();
		$query=$this->db->insert('loan_advance_payback',array(
		'payment_mode_id'=>$paymenmod_id,
		'transaction_id'=>$trans_id,
		'loan_advance_id'=>$loan_advance_id,
		'paid_amount'=>$pay_amount,
		'month'=>$month,
		'year'=>$year
		));
        if($pay_amount < $total_amount)
        {
            $balance=$total_amount - $pay_amount;
            $this->db->where(array('loan_advance_id'=>$loan_advance_id));
            $this->db->update('loan_advances',array('paid_back'=>0,'balance'=>$balance));
        }
        if($pay_amount == $total_amount)
        {
            $balance=0;
            $this->db->where(array('loan_advance_id'=>$loan_advance_id));
            $this->db->update('loan_advances',array('paid_back'=>1,'balance'=>$balance));
        }
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		} 
	// END cash
	
	///////////////////END Functions for loan and advance pay back////////////////////////////////
	
	//////////////////////Function for salary payments ////////////////////////////////////////
	
	public function salarypayment_view()
	{
	$this->datatables->select('employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT deduction.deduction_amount),((salary.base_salary)+SUM(DISTINCT allowance.allowance_amount)+SUM(DISTINCT expense_claims.amount)-SUM(DISTINCT deduction.deduction_amount)-SUM(DISTINCT loan_advances.amount)) as payable,salary_payment.transaction_amount,employment.employee_id')
	->from('employee')
	->join('employment','employment.employee_id=employee.employee_id')
	->join('transaction','transaction.employment_id=employment.employment_id')
	->join('salary','salary.employement_id=employment.employment_id')	
	->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id')
	->join('position_management','position_management.employement_id=employment.employment_id')
	->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employment_id=employment.employment_id','LEFT OUTER' AND 'deduction.deduction_amount','0')
	->join('deduction_processing','deduction_processing.deduction_id=deduction.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id')
	->join('expense_claims','expense_claims.employment_id=employment.employment_id','LEFT')
	->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=salary_payment.month')
	->join('ml_year','ml_year.ml_year_id=salary_payment.year')
	//->where('salary_payment.month','deduction_processing.month')
	->WHERE('transaction.transaction_type',5)
        ->where('position_management.current',1)
	->group_by('salary_payment.salary_payment_id','salary_payment.month');
	//->group_by('salary_payment.month');
	$this->datatables->edit_column('employment.employee_id',
	'<a href="payroll/pay_remain_salary/$1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="assets/img/icon-black.png" width="20" height="20"></a>
	&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;<span onclick="return print_report($1);" class="fa fa-print" style="cursor:pointer"></span>
	&nbsp;| &nbsp;<a href="payroll/payroll_reg_detail/$1"><span class="fa fa-eye"></span></a>',
	'employment.employee_id');
	//$this->datatables->edit_column('employment.employee_id','<a href="site/payroll_register/$1">$2</a>','employment.employee_id');
	if($months = $this->input->post('month'))
	{
		$this->datatables->where('salary_payment.month',$months);
	}
	return $this->datatables->generate();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// TEST/////////////////////////////////////////////////
	public function salarypayment_count()
	{
		$this->db->select('*');
		$this->db->from('salary_payment');
		$query=$this->db->get();
		if($query->num_rows()>0)
		{ return $query->result();}
	}
		
	public function salarypayment_test($limit = NULL,$start = NULL,$emp_name = NULL,$selc_month = NULL,$selc_year = NULL) /// this is 1st query and function
		{
			
		$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.paid=0) THEN expense_claims.amount ELSE 0 END) as exp_amount,salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0)THEN loan_advances.amount ELSE 0 END) as loan_amount,employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,(deduction.month) as ded_month,(deduction.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,salary_payment.salary_payment_id,ml_status.status_title,salary_payment.transaction_id,allowance.pay_frequency,deduction.deduction_frequency')
	->from('employee')
	->join('employment','employee.employee_id=employment.employee_id','LEFT')
	->join('salary','employment.employment_id=salary.employement_id','LEFT')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')	
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employee_id=employee.employee_id','LEFT OUTER')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT OUTER')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT OUTER')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT OUTER')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT OUTER')
    ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
    ->where('position_management.current',1)
	->limit($limit,$start)
	->like('employee.full_name',$emp_name)
	->like('salary_payment.month',$selc_month)
	->like('salary_payment.year',$selc_year)
	->group_by('salary_payment.salary_payment_id');
	
	//$this->db->limit(10,$start);
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		} 
	///////////////// function for status salary payment ///////////////
		public function ml_status_payment()
		{
		$this->db->select('salary_payment.salary_payment_id,ml_status.status_title')
		->from('salary_payment')
		//->where(array('salary_payment_id'=>$id))
		->join('ml_status','salary_payment.status=ml_status.status_id');
		$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		}
/////////////////////////////////////// salary payment data for treansaction of an employee/////////////////////

public function salarypayment_trans($id) /// this is 1st query and function
		{
$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,SUM(DISTINCT CASE WHEN allowance.status=2 THEN allowance.allowance_amount ELSE 0 END) as allowance_amnt,SUM(DISTINCT CASE WHEN expense_claims.status=2 THEN expense_claims.amount ELSE 0 END) as  exp_amount,salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advance_payment.transaction_id > 0)THEN loan_advances.amount ELSE 0 END) as loan_amount,employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,(deduction.month) as ded_month,(deduction.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,salary_payment.salary_payment_id,ml_status.status_title,salary_payment.transaction_id,allowance.pay_frequency,deduction.deduction_frequency')
	->from('employee')
	->where('employee.employee_id',$id)
	->join('employment','employee.employee_id=employment.employee_id')
	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')	
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employee_id=employee.employee_id','LEFT OUTER')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT OUTER')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT OUTER')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT OUTER')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT OUTER')
	//->WHERE('ml_pay_frequency.pay_frequency','Monthly')
	
	->join('ml_month','salary_payment.month=ml_month.ml_month_id')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id')
    ->where('position_management.current',1);
	//->like('employee.full_name',$emp_name)
	//->like('salary_payment.month',$selc_month)
	//->like('salary_payment.year',$selc_year)
	//->group_by('salary_payment.salary_payment_id');
	
	//$this->db->limit($limit,$start);
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->row();
		}
		
		}


	/////////////////////////////////////END TEST//////////////////////////////////////////////
	
	public function pay_remain_salary_model($salary_where)
	{
		$this->db->select('employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,transaction.calender_month,salary.base_salary,salary_payment.transaction_amount,allowance.allowance_amount,deduction.deduction_amount,expense_claims.amount,(loan_advances.amount) as loan,((salary.base_salary+allowance.allowance_amount+expense_claims.amount)-(deduction.deduction_amount)-(loan_advances.amount)) as payable')
		->from('employee')
		->where($salary_where)
		->join('transaction','transaction.employee_id=employee.employee_id')
		->join('employment','employment.employee_id=employee.employee_id')
		->join('salary','salary.employement_id=employment.employment_id')
		->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id')
		->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	    ->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id')
		->join('position_management','position_management.employement_id=employment.employment_id')
		->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
		->join('deduction','deduction.employee_id=employee.employee_id','LEFT')
		->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
		->join('expense_claims','expense_claims.employee_id=employee.employee_id','LEFT')
        ->where('position_management.current',1);
		$query=$this->db->get();
		
		
		if($query->num_rows>0)
		{
			return $query->row();
		}
		return false;
	
		}
	///////////////////////END Function for salary payments//////////////////////////////////////
	
	/////////////////////////// Function for Transactions View page  ////////////////////////////
	
	public function paytransactions_count()
	{
		$this->db->select('employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,transaction.calender_month,transaction.calender_year,ml_transaction_types.transaction_type,SUM(DISTINCT transaction.transaction_amount) as trans_amount,
	transaction.transaction_id')
	->from('employee')
    ->join('employment','employment.employee_id=employee.employee_id')
    ->join('transaction','transaction.employment_id=employment.employment_id')

	->join('position_management','position_management.employement_id=employment.employment_id')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('salary','salary.employement_id=employment.employment_id')
	->join('ml_transaction_types','ml_transaction_types.ml_transaction_type_id=transaction.transaction_type')
     ->where('transaction.transaction_type !=1 AND transaction.transaction_type !=2')
      ->where('position_management.current',1)
	->group_by('transaction.transaction_id','transaction.transaction_type');

	$query=$this->db->get();
	if($query->num_rows()>0)
	{return $query->result();}
	}
	
	public function paytransactions_view($limit,$page = NULL)
	{
		$months=$this->input->post('month');
		$name=$this->input->post('name');
		$this->db->select('employee.employee_code,employee.full_name,
		ml_pay_grade.pay_grade,transaction.calender_month,transaction.calender_year,
		transaction.transaction_date,ml_transaction_types.transaction_type,
		SUM(DISTINCT transaction.transaction_amount) as trans_amount,transaction.transaction_amount,
	transaction.transaction_id,ml_status.status_title,transaction.status,transaction.employment_id,
	transaction.transaction_date,(transaction.transaction_type) AS trans_type_id')
	->from('employee')
	//->order_by('transaction.transaction_id','DESC')
	//->join('employee','employee.employee_id=transaction.employee_id')
    ->join('employment','employment.employee_id=employee.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('transaction','transaction.employment_id=employment.employment_id')

	->join('position_management','position_management.employement_id=employment.employment_id')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('salary','salary.employement_id=employment.employment_id')
	->join('ml_transaction_types','ml_transaction_types.ml_transaction_type_id=transaction.transaction_type')
	->join('ml_status','ml_status.status_id=transaction.status')
	->like('transaction.calender_month',$months)
	->like('employee.full_name',$name)
	->where('transaction.transaction_type !=1 AND transaction.transaction_type !=2')
    ->where('position_management.current',1)
	->order_by('transaction.transaction_id','desc')
	->group_by('transaction.transaction_id','transaction.transaction_type')
	->limit($limit,$page);
	
	
	if($months=$this->input->post('month'))
	{
		$this->db->where('transaction.calender_month',$months);
	}
	if($years=$this->input->post('year'))
	{
		$this->db->where('transaction.calender_year',$years);
	}
	if($name=$this->input->post('emp_name'))
	{
		$this->db->like('employee.full_name',$name);
	}
	if($code=$this->input->post('code'))
	{
		$this->db->like('employee.employee_code',$code);
	}
	/*$this->db->edit_column('transaction.transaction_id',
	'<span onclick="return print_report($1);" class="fa fa-print" style="cursor:pointer"></span></a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;<a href="payroll/transaction_details/$1"><span class="fa fa-eye"></span></a>',
	'transaction.transaction_id');*/
	
	//return $this->datatables->generate();
	$query=$this->db->get();
	if($query->num_rows()>0)
	{return $query->result();}
	}
	
	public function transaction_view_details($where)
	{
		$this->db->select(					'employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_designations.designation_name,transaction.calender_month,ml_transaction_types.transaction_type,transaction.transaction_amount,	transaction.transaction_id,transaction.transaction_date,ml_status.status_title,transaction.approval_date,(Select full_name from employee where employee_id=transaction.approved_by) as name')
	->from('employee')
    ->join('employment','employment.employee_id=employee.employee_id')
	->join('transaction','transaction.employment_id=employment.employment_id')
	->join('ml_status','transaction.status=ml_status.status_id','LEFT')

	->join('position_management','position_management.employement_id=employment.employment_id')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('salary','salary.employement_id=employment.employment_id')
	->join('ml_transaction_types','ml_transaction_types.ml_transaction_type_id=transaction.transaction_type')
	->where($where)
     ->where('position_management.current',1);
	$query=$this->db->get();
	return $query->row();
	}
	
	//////////////END Function for Transactions View page//////////////////////////////////////////
	
	/////////////Function for End Of Servuce ///////////////////////////////
	
	public function end_service_view($month = NULL, $year = NULL)
	{

		$where=array(
		'employment.do_eos_settlement'=>0//,
		//'employment.settlement_payment_status'=>0
		);
		$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,
		transaction.transaction_date,salary.base_salary,
		transaction.transaction_amount,
		SUM(DISTINCT CASE WHEN(deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
		SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount WHEN (allowance.status=2 AND allowance.month='.$month.' AND allowance.year='.$year.') THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
		SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month='.$month.' AND expense_claims.year='.$year.' AND expense_claims.paid=0) THEN expense_claims.amount ELSE 0 END) as exp_amount,
		SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month ='.$month.' AND loan_advance_payment.year ='.$year.' AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance ELSE 0 END) as loan_amount,
		employment.employment_id,employee.employee_id,contract.contract_start_date,(CASE WHEN(contract.extension = 1) THEN EXCT.e_end_date ELSE contract.contract_expiry_date END) AS contract_expiry_date,employment.settlement_payment_status')
		->from('employee')
		///->distinct('employment.employee_id')
		//->unset_column('employment.employment_id')
		->join('employment','employment.employee_id=employee.employee_id AND employment.trashed = 0 AND employment.current = 0')
        ->join('contract','contract.employment_id=employment.employment_id')
        ->join('employee_contract_extensions EXCT','contract.contract_id=EXCT.contract_id','LEFT')
        ->join('transaction','transaction.employment_id=employment.employment_id')
		->join('salary','salary.employement_id=employment.employment_id')
		->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id')
		->join('position_management','position_management.employement_id=employment.employment_id')
		->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
		->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
		->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
		->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
		->join('expense_claims','expense_claims.employment_id=employment.employment_id','LEFT')
		->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
		->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
		->join('transaction advt','advt.transaction_id=loan_advance_payment.transaction_id','LEFT')
		->where($where)
        ->where('position_management.current',0)
        ->order_by('employment.employment_id','desc');
		$this->db->group_by('employment.employment_id');
		if($name=$this->input->post('name'))
		{
			$this->db->where('employee.employee_id',$name);
		}
        if($month=$this->input->post('month')) {
            $this->db->where('MONTH(contract_expiry_date)', $month);
        }
		$query=$this->db->get();
		if($query)
		{
			return $query->result();
		}
    }
	/// END

    /////////////Function for End Of Service payment  ///////////////////////////////

    public function end_of_service_pay_view($employment_id = NULL, $month = NULL, $year = NULL)
    {
        $where=array(
            'employment.employment_id'=>$employment_id,
            'employment.current'=>0/*,
            'employment.do_eos_settlement'=>0,
            'settlement_payment_status'=>0*/
        );
        $this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,
		transaction.transaction_date,salary.base_salary,
		transaction.transaction_amount,
		SUM(DISTINCT CASE WHEN(deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
		group_concat(DISTINCT CASE WHEN(deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_id ELSE 0 END) as ded_ids,
		SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount WHEN (allowance.status=2 AND allowance.month='.$month.' AND allowance.year='.$year.') THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
		group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_id WHEN (allowance.status=2 AND allowance.month='.$month.' AND allowance.year='.$year.') THEN allowance.allowance_id ELSE NULL END) as  allowance_ids,
		SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month='.$month.' AND expense_claims.year='.$year.' AND expense_claims.paid=0) THEN expense_claims.amount ELSE 0 END) as exp_amount,
		group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month='.$month.' AND expense_claims.year='.$year.' AND expense_claims.paid=0)  THEN expense_claims.expense_claim_id ELSE NULL END) as  exp_ids,
		SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month ='.$month.' AND loan_advance_payment.year ='.$year.' AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance ELSE 0 END) as loan_amount,
		group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month ='.$month.' AND loan_advance_payment.year ='.$year.' AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.loan_advance_id WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.loan_advance_id ELSE 0 END) as loan_ids,
        group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN ml_deduction_type.deduction_type WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month='.$month.') THEN ml_deduction_type.deduction_type WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month='.$month.' AND deduction.year='.$year.') THEN ml_deduction_type.deduction_type ELSE NULL END) as ded_rectp,
        group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month='.$month.') THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month='.$month.' AND deduction.year='.$year.') THEN deduction.deduction_amount ELSE NULL END) as ded_recm,
        group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN ml_allowance_type.allowance_type
        WHEN (allowance.status=2 AND allowance.month='.$month.' AND allowance.year='.$year.') THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
        group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
        WHEN (allowance.status=2 AND allowance.month='.$month.' AND allowance.year='.$year.') THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
        group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month='.$month.' AND expense_claims.year='.$year.' AND expense_claims.paid=0) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
        group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month='.$month.' AND expense_claims.year='.$year.' AND expense_claims.paid=0) THEN expense_claims.amount ELSE NULL END) as exp_recm,
        group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month ='.$month.' AND loan_advance_payment.year ='.$year.' AND loan_advances.paid_back=0) THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
        group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month ='.$month.' AND loan_advance_payment.year ='.$year.' AND loan_advances.paid_back=0 AND advt.status=2) THEN loan_advances.amount  ELSE NULL END) as loan_recm,
        SUM(DISTINCT CASE  WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance
 ELSE 0 END) as balance_loan_amount,
		employment.employment_id,employee.employee_id,contract.contract_start_date,(CASE WHEN(contract.extension = 1) THEN EXCT.e_end_date ELSE contract.contract_expiry_date END) AS contract_expiry_date,salary.salary_id')
            ->from('employee')
            ///->distinct('employment.employee_id')
            //->unset_column('employment.employment_id')
            ->join('employment','employment.employee_id=employee.employee_id AND employment.trashed = 0')
            ->join('contract','contract.employment_id=employment.employment_id')
            ->join('employee_contract_extensions EXCT','contract.contract_id=EXCT.contract_id','LEFT')
            ->join('transaction','transaction.employment_id=employment.employment_id')
            ->join('salary','salary.employement_id=employment.employment_id')
            ->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id')
            ->join('position_management','position_management.employement_id=employment.employment_id')
            ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
            ->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
            ->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
            ->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
            ->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
            ->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
            ->join('expense_claims','expense_claims.employment_id=employment.employment_id','LEFT')
            ->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
            ->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
            ->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
            ->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
            ->join('transaction advt','advt.transaction_id=loan_advance_payment.transaction_id','LEFT')
            ->where($where)
            ->where('position_management.current',1)
            ->order_by('employment.employment_id','desc');
        $this->db->group_by('employment.employment_id');
        if($name=$this->input->post('name'))
        {
            $this->db->like('employee.full_name',$name);
        }
        $query=$this->db->get();
        if($query)
        {
            return $query->row();
        }
        /*$this->datatables->edit_column('employee.employee_id',
        '<a href="payroll/eos_settlement/$2">EOS Settelment</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="payroll/payroll_reg_detail/$2"><span class="fa fa-eye"></span></a>',
        'employee.employee_id,employment.employment_id');*/

        //$this->datatables->edit_column('employment.employee_id','<a href="site/payroll_register/$1">$2</a>','employment.employee_id');

        //return $this->datatables->generate();
    }
    /// END
    /// Function for payment End of service ///
    public function salary_trans_eos($employment_id,$payable,$ded_amount,$account_id,$exp_id,$alw_id,$adv_id,$month,$year,$exp_amount,$alw_amount,$adv_amount,$salary_id)
    {
        $this->db->trans_start();

        $query=$this->db->insert('payment_mode',array(
            'payment_mode_type_id'	=>3,
            'account_id'			 =>$account_id
        ));
        $paymenmod_id=$this->db->insert_id();
        if($ded_amount > 0){
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$ded_amount,
                'transaction_type'=>7,
                'employment_id'=>$employment_id
            ));
            /*$transaction_id_ded=$this->db->insert_id();
            foreach($ded_ids as $ded_idd){
                $this->db->insert('deduction_processing',array(
                    'transaction_id'	=>$transaction_id_ded,
                    'deduction_id'  	=>$ded_idd,
                    'month'				=>$month,
                    'year'				=>$year
                ));
            }*/
        }
        ////////////// For Allowance
        if($alw_amount > 0){
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$alw_amount,
                'transaction_type'=>2,
                'employment_id'=>$employment_id
            ));
            $transaction_id_alw=$this->db->insert_id();
            foreach($alw_id as $alw_idd){
                $this->db->insert('allowance_payment',array(
                    'transaction_id'	=>$transaction_id_alw,
                    'allowance_id'  	=>$alw_idd,
                    'month'				=>$month,
                    'year'				=>$year
                ));
            }
        }
        //////////// for expense
        if($exp_amount > 0){
            $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$exp_amount,
                'transaction_type'=>4,
                'employment_id'=>$employment_id
            ));
            $transaction_id_exp=$this->db->insert_id();
            //return $exp_ids;
            foreach($exp_id as $exp_idd){
                $this->db->insert('expense_disbursment',array(
                    'transaction_id'=>$transaction_id_exp,
                    'expense_id'    =>$exp_idd,
                    'month'         =>$month,
                    'year'          =>$year
                ));
                //$count++;
                $this->db->where('expense_claim_id',$exp_idd);
                $query=$this->db->update('expense_claims',array('paid'=>1));
            }

        }
        if($adv_amount > 0){
            foreach($adv_id as $loan_ids) {
                     $this->db->where(array('loan_advance_id'=>$loan_ids));
                    $this->db->update('loan_advances',array('balance'=>0,'paid_back'=>1));
            }
             $this->db->insert('transaction',array(
                'calender_year'=>date('Y'),
                'calender_month'=>date('M'),
                'transaction_date'=>date('Y-m-d'),
                'transaction_time'=>date('H:i:s'),
                'transaction_amount'=>$adv_amount,
                'transaction_type'=>3,
                'employment_id'=>$employment_id
            ));
            $transaction_id_advance=$this->db->insert_id();
            foreach($adv_id as $loan_ids){
                $this->db->insert('loan_advance_payback',array(
                    'transaction_id'=>$transaction_id_advance,
                    'loan_advance_id'=>$loan_ids,
                    'month'=>$month,
                    'year'=>$year
                ));
                //$this->db->where(array('loan_advance_id'=>$loan_ids));
                //$this->db->update('loan_advances',array('paid_back'=>1));
            }
        }

        $this->db->insert('transaction',array(
            'payment_mode'=>$paymenmod_id,
            'calender_year'=>date('Y'),
            'calender_month'=>date('M'),
            'transaction_date'=>date('Y-m-d'),
            'transaction_time'=>date('H:i:s'),
            'transaction_amount'=>$payable,
            'transaction_type'=>5,
            'employment_id'=>$employment_id
        ));
        $trans_id=$this->db->insert_id();
        //$this->db->where(array('salary_payment_id'=>$salary_payment_id));
        $query=$this->db->insert('salary_payment',array(
            'salary_id'			=>$salary_id,
            'month'				=>$month,
            'year'				=>$year,
            'transaction_id'	=>$trans_id,
            'transaction_amount'=>$payable));

        $query=$this->db->insert('eos',array(
            'employment_id'     =>$employment_id,
            'total_deductions'	=>$ded_amount,
            'transaction_id'	=>$trans_id,
            'date_created'		=>date("Y-m-d"),
            'created_by'	    =>$employment_id,
            'paid_amount'       =>$payable));

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
        return $query;
        $this->db->trans_complete();
    }
// End account

    ///////////////////////// Function for EOS settelment ///////////////////////
        public function eos_payment_model($employment_id,$employment_id,$employment_id)
        {

        }

    public function get_eossettelment($emp_id)
	{
		$this->db->select('employee.employee_code,employee.full_name,salary.base_salary,salary_payment.transaction_amount,deduction.deduction_amount,employment.employee_id')
	->from('employee')
		->join('transaction','transaction.employee_id=employee.employee_id')
		->join('employment','employment.employee_id=employee.employee_id')
		->join('salary','salary.employement_id=employment.employment_id')
		->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id')
		->join('position_management','position_management.employement_id=employment.employment_id')
		->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
		->join('deduction','deduction.employee_id=employee.employee_id')
		->join('allowance','allowance.employment_id=employment.employment_id')
		->join('expense_claims','expense_claims.employee_id=employee.employee_id')
            ->where('position_management.current',1);
		$this->db->where('employment.employment_id',$emp_id);
		$query=$this->db->get();
		if($query->num_rows>0)
		{
			$query->row();
			return $query;
		}
		
	}
	
	/////////////////// END
	//public function filter_month()
//	{
//		$this->db->select('ml_month.month')
//		->from('allowance')
//		->join('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id')
//		->join('ml_month','allowance_payment.month=ml_month.ml_month_id');
//		$query=$this->db->get();
//		if($query->num_rows() > 0)
//		{
//			return $query->row();
//		}
//	}
//	public function filter_year()
//	{
//		$this->db->select('ml_year.year')
//		->from('allowance')
//		->join('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id')
//		->join('ml_year','allowance_payment.year=ml_year.ml_year_id');
//		$query=$this->db->get();
//		if($query->num_rows() > 0)
//		{
//			return $query->result();
//		}
//	}

	public function update_trash($table,$where,$data)
	{
		$this->db->where($where);
		$query=$this->db->update($table,$data);
		if($query)
		{return true;}
		else{return false;}
	}
	public function master_list_data($table,$option,$key,$value)
	{
		$this->db->select('*');
		$query=$this->db->get($table);
		if($query->num_rows > 0)
	{
		foreach($query->result() as $row)
		{
			$data['']=$option;
			$data[$row->$key]= $row->$value;
		}
		return $data;
	}
			
	}
	
	public function ml_month_datatable($table,$key,$value)
	{
		$this->db->select('*');
		$query=$this->db->get($table);
		if($query->num_rows > 0)
	{
		foreach($query->result() as $row)
		{
			$data['']='Select Month';
			$data[$row->$key]= $row->$value;
		}
		return $data;
			
	}
		
	}
	
	public function ml_year_datatable($table,$key,$value)
	{
		$this->db->select('*');
		$query=$this->db->get($table);
		if($query->num_rows > 0)
	{
		foreach($query->result() as $row)
		{
			$data['']='Select Year';
			$data[$row->$key]= $row->$value;
		}
		return $data;
			
	}
		
	}
	/////////////////////////////////////Master list Month ///////////////////////////////////////////////
	public function ml_month()
	{
		$this->db->select('*');
		$this->db->from('ml_month');
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->result();	
	}
		
		
	}
	///// WHERE MONTH
	
	public function ml_month_where($month)
	{
		$where=array('ml_month_id'=>$month);
		$this->db->select('month');
		$this->db->from('ml_month');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->row();	
	}
		
		
	}
	/////////////////////////////////////Master list Year ///////////////////////////////////////////////
	public function ml_year()
	{
		$this->db->select('*');
		$this->db->from('ml_year');
        $this->db->order_by('ml_year.ml_year_id','DESC');
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->result();	
	}
		
	}
	
	///////////// WHERE YEAR
	public function ml_year_where($year)
	{
		$where=array('ml_year_id'=>$year);
		$this->db->select('year');
		$this->db->from('ml_year');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->row();	
	}
		
	}
//////////////////////Employee list for pay salary //////////////////////////////////

/////////// check salary payment of emplye that already paid //////////////////////
public function check_salary_payment($month,$year)
{
	$this->db->select('salary_payment.month,salary_payment.year,salary_payment.salary_id')
	->from('salary_payment')
	->join('salary','salary_payment.salary_id=salary.salary_id')
	->where('salary_payment.month',$month)
	->where('salary_payment.year',$year);
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{return $query->result();}
	
}
///////////////
public function salary_paid_employees($month = NULL,$year = NULL)
	{
		$month = intval($month);
		$year = intval($year);
		$wher=array('salary_payment.month'=> $month,
		'salary_payment.year'=>$year);
		$this->db->select('salary.salary_id')
		->from('salary')
		->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
		->where($wher);
		//->where('salary_payment.year',$year)
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->result();}
		
	}
public function employee_view_salary($ids= NULL,$month= NULL,$year= NULL,$project= NULL)
	{
        $year_Y=$this->db->query("select year from ml_year where ml_year_id=$year");
        $result=$year_Y->row();
        $rec_Y=$result->year;
        if($month >= 1 && $month <= 9){
            $monthn="0".$month;
        	$yr_mt=$rec_Y.$monthn;
        }
        else
        {$yr_mt=$rec_Y.$month;}
     if($project == '' || $project== NULL){
//		 $project= 'NULL';
		 $where= '(`salary`.`status` =2 AND `position_management`.`current` =1 AND `position_management`.`status` =2 AND `salary`.`salary_id` NOT IN ('.$ids.'))
		 AND (CASE WHEN extension = 0 THEN '.$yr_mt.' BETWEEN
EXTRACT(YEAR_MONTH FROM contract_start_date)
AND
EXTRACT(YEAR_MONTH FROM contract_expiry_date) ELSE '.$yr_mt.' BETWEEN
EXTRACT(YEAR_MONTH FROM e_start_date)
AND
EXTRACT(YEAR_MONTH FROM e_end_date) END)';
	 }else{
		 $where= '`salary`.`status` =2 AND `position_management`.`current` =1 AND `position_management`.`status` =2 AND  `salary`.`salary_id` NOT IN ('.$ids.') AND `employee_project`.`project_id` ='.$project.' AND (CASE WHEN extension = 0 THEN '.$yr_mt.' BETWEEN
EXTRACT(YEAR_MONTH FROM contract_start_date)
AND
EXTRACT(YEAR_MONTH FROM contract_expiry_date) ELSE '.$yr_mt.' BETWEEN
EXTRACT(YEAR_MONTH FROM e_start_date)
AND
EXTRACT(YEAR_MONTH FROM e_end_date) END)';
	 }
	$emp_name=$this->input->post('emp_name');
	$query=$this->db->query("SELECT `employee`.`employee_id`, `employee`.`employee_code`, `employee`.`full_name`, `ml_pay_grade`.`pay_grade`, `salary`.`base_salary`,
  SUM(DISTINCT Inc.`increment_amount`) AS Increment,
SUM(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$month) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$month AND deduction.year=$year) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$month AND expense_claims.year=$year AND expense_claims.paid=0) THEN expense_claims.amount ELSE 0 END) as exp_amount,
SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$month AND loan_advance_payment.year =$year AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance ELSE 0 END) as loan_amount,
`employment`.`employee_id`, `loan_advances`.`loan_advance_id`, (loan_advance_payback.loan_advance_id) as loan_payback_id, `expense_claims`.`expense_claim_id`,(deduction.month) as ded_month, (deduction.year) as ded_year,
(expense_claims.month) as exp_month, (expense_claims.year) as exp_year, (allowance_payment.month) as alwnc_month, (allowance_payment.year) as alwnc_year, (loan_advance_payment.month) as alonad_month, (loan_advance_payment.year) as alonad_year,
`ml_status`.`status_title`, `allowance`.`pay_frequency`, `deduction`.`deduction_frequency`,(loan_advance_payback.month)  as back_month,(loan_advance_payback.year) as back_year,(expense_disbursment.expense_id) as exp_dis_id,loan_advances.paid,
loan_advances.paid_back,`ml_projects`.`project_title`
	
 FROM (`employee`)
 INNER JOIN `employment` ON `employee`.`employee_id`=`employment`.`employee_id` AND employment.current = 1 AND employment.trashed = 0
 INNER JOIN contract  ON contract.`employment_id`=employment.`employment_id`
 LEFT JOIN employee_contract_extensions  ON employee_contract_extensions.`contract_id`=contract.`contract_id`
 LEFT JOIN `employee_project` ON `employment`.`employment_id`=`employee_project`.`employment_id`
 LEFT JOIN `ml_projects` ON `employee_project`.`project_id`=`ml_projects`.`project_id`
 INNER JOIN `salary` ON `employment`.`employment_id`=`salary`.`employement_id`
 LEFT JOIN `salary_payment` ON `salary`.`salary_id`=`salary_payment`.`salary_id` 
 LEFT JOIN `ml_status` ON `salary`.`status`=`ml_status`.`status_id` 
 LEFT JOIN `position_management` ON `employment`.`employment_id`=`position_management`.`employement_id` 
 LEFT JOIN `ml_pay_grade` ON `ml_pay_grade`.`ml_pay_grade_id`=`position_management`.`ml_pay_grade_id` 
 LEFT OUTER JOIN `deduction` ON `deduction`.`employment_id`=`employment`.`employment_id`
 LEFT OUTER JOIN `ml_pay_frequency` ON `ml_pay_frequency`.`ml_pay_frequency_id`=`deduction`.`deduction_frequency`
 LEFT OUTER JOIN `allowance` ON `allowance`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `allowance_payment` ON `allowance_payment`.`allowance_id`=`allowance`.`allowance_id`
 LEFT OUTER JOIN `expense_claims` ON `employment`.`employment_id`=`expense_claims`.`employment_id`
 LEFT OUTER JOIN `expense_disbursment` ON `expense_disbursment`.`expense_id`=`expense_claims`.`expense_claim_id`
 LEFT JOIN `loan_advances` ON `loan_advances`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `loan_advance_payment` ON `loan_advance_payment`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 LEFT JOIN `transaction` `advt` ON `advt`.`transaction_id`=`loan_advance_payment`.`transaction_id`
 LEFT JOIN increments Inc ON Inc.employment_id = `employment`.`employment_id` AND Inc.status = 2 AND Inc.trashed = 0 AND CURDATE() >= Inc.date_effective
 LEFT OUTER JOIN `loan_advance_payback` ON `loan_advance_payback`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 WHERE ".$where." AND employment.current=1 GROUP BY `salary`.`salary_id`");
	//return $this->db->last_query();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
	 
	}
	
	/////////////////// Employee Salary info for payment /////////////////////////////
	public function salary_employee_data($test,$months= NULL,$year= NULL)
	{
		$this->db->select("employee.employee_id,employee.employee_code,employee.full_name,ml_month.month,ml_year.year,salary.base_salary,
		SUM(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$months) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$months AND deduction.year=$year) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
		group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_id WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$months) THEN deduction.deduction_id WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$months AND deduction.year=$year) THEN deduction.deduction_id ELSE NULL END) as  ded_ids,
		group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_id WHEN (allowance.status=2 AND allowance.month=$months AND allowance.year=$year) THEN allowance.allowance_id ELSE NULL END) as  allowance_ids,
		SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$months AND allowance.year=$year) THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$months AND expense_claims.year=$year AND expense_claims.paid=0) THEN expense_claims.amount ELSE 0 END) as exp_amount,
group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$months AND expense_claims.year=$year AND expense_claims.paid=0)  THEN expense_claims.expense_claim_id ELSE NULL END) as  exp_ids,
SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$months AND loan_advance_payment.year =$year AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance
 ELSE 0 END) as loan_amount,
 SUM(DISTINCT CASE  WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.balance
 ELSE 0 END) as balance_loan_amount,
group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$months AND loan_advance_payment.year =$year AND loan_advances.paid_back=0 AND advt.status=2)THEN loan_advances.loan_advance_id WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.paid_back=0 AND advt.status=2 AND loan_advances.balance > 0)THEN loan_advances.loan_advance_id ELSE NULL END) as loan_ids,
employment.employee_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,loan_advances.loan_advance_id,
(loan_advance_payback.loan_advance_id) as loan_payback_id,(deduction.month) as ded_month,(deduction.year) as ded_year,deduction.deduction_frequency,allowance.pay_frequency,
(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,ml_deduction_type.deduction_type,salary.salary_id,ml_allowance_type.allowance_type,ml_expense_type.expense_type_title,ml_payment_type.payment_type_title,
group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN ml_deduction_type.deduction_type WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$months) THEN ml_deduction_type.deduction_type WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$months AND deduction.year=$year) THEN ml_deduction_type.deduction_type ELSE NULL END) as ded_rectp,
group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$months) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$months AND deduction.year=$year) THEN deduction.deduction_amount ELSE NULL END) as ded_recm,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN ml_allowance_type.allowance_type
WHEN (allowance.status=2 AND allowance.month=$months AND allowance.year=$year) THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$months AND allowance.year=$year) THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$months AND expense_claims.year=$year AND expense_claims.paid=0) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
group_concat(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$months AND expense_claims.year=$year AND expense_claims.paid=0) THEN expense_claims.amount ELSE NULL END) as exp_recm,
group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$months AND loan_advance_payment.year =$year AND loan_advances.paid_back=0) THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
group_concat(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$months AND loan_advance_payment.year =$year AND loan_advances.paid_back=0 AND advt.status=2) THEN loan_advances.amount  ELSE NULL END) as loan_recm,(expense_disbursment.month) as exp_dis_month,(expense_disbursment.year) as exp_dis_year")
	->from('employee,ml_month,ml_year')
	->WHERE('employee.employee_id',$test)
	->join('employment','employment.employee_id=employee.employee_id')
	->join('salary','salary.employement_id=employment.employment_id')
	->join('loan_advances','loan_advances.employment_id=employment.employment_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('transaction advt','advt.transaction_id=loan_advance_payment.transaction_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
	->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id','LEFT')
	->join('expense_claims','expense_claims.employment_id=employment.employment_id','LEFT')
	->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
	->join('deduction','deduction.employment_id=employment.employment_id','LEFT')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
	//->join('ml_month','deduction_processing.month=ml_month.ml_month_id')
	//->join('ml_year','deduction_processing.year=ml_year.ml_year_id')
	->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
    ->WHERE('employment.current',1);
	//$this->db->where($where_data);
	$query=$this->db->get();
	if($query->num_rows>0)
	{
		return $query->row();
	}
	}
	/*public function last_salary($id)
	{
		$this->db->select('salary_payment.salary_payment_id,salary_payment.transaction_amount,ml_month.month,ml_year.year,salary_payment.arears')
		->from('salary_payment')
		->join('ml_month','salary_payment.month=ml_month.ml_month_id','left')
		->join('ml_year','salary_payment.year=ml_year.ml_year_id','left')
	    ->join('transaction','salary_payment.transaction_id=transaction.transaction_id','left')
		->join('employee','transaction.employee_id=employee.employee_id','left')
		->where('employee.employee_id',$id)
		->order_by('salary_payment_id','desc');
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}*/
    public function last_salary($id)
    {
        $this->db->select('salary_payment.salary_payment_id,salary_payment.transaction_amount,ml_month.month,ml_year.year,salary_payment.arears')
            ->from('employee')
            ->join('employment','employment.employee_id=employee.employee_id','left')
            ->join('salary','salary.employement_id=employment.employment_id','left')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','left')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','left')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','left')
            ->join('transaction','salary_payment.transaction_id=transaction.transaction_id','left')
             ->where('employment.employment_id',$id)
            ->where('transaction.status',2)
            ->order_by('salary_payment_id','desc')
            ->limit(1,1);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }
	
	public function account_info($id)
	{
		$this->db->select('bank_account_id,account_no,account_title,bank_name,branch,branch_code')
		->from('emp_bank_account')
		->where('employee_id',$id)
		->where('active',1);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	public function employement_id($where)
	{
		$this->db->select('employment_id');
		$this->db->from('employment');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->row();	
	}
	}
	public function fetch_salary_id($em_id)
	{	
		$id=array('employement_id'=>$em_id);
		$this->db->select('salary_id');
		$this->db->from('salary');
		$this->db->where($id);
		$query=$this->db->get();
		if($query->num_rows>0)
	{
		return $query->row();	
	}
	}
	
////////////////////////END

////////////////////// Function for salary info for generating slip /////////////////

public function salarypayment_slip_count()
	{
		$this->db->select('salary_payment.salary_payment_id');
		$this->db->from('salary_payment')
		->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')
		->WHERE('transaction.status',2);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{ return $query->result();}
	}

	public function salarypayment_slips($limit,$start = NULL,$emp_name = NULL,$selc_month = NULL,$selc_year = NULL) /// this is 1st query and function
		{
			$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,ml_pay_grade.pay_grade,ml_month.month,ml_year.year,salary.base_salary,SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,SUM(DISTINCT CASE WHEN allowance.status=2 THEN allowance.allowance_amount ELSE 0 END) as allowance_amnt,SUM(DISTINCT CASE WHEN expense_claims.status=2 THEN expense_claims.amount ELSE 0 END) as  exp_amount,salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advance_payment.transaction_id > 0)THEN loan_advances.amount ELSE 0 END) as loan_amount,employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,(deduction.month) as ded_month,(deduction.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,salary_payment.salary_payment_id,ml_status.status_title,ml_status.status_id,salary_payment.transaction_id,allowance.pay_frequency,deduction.deduction_frequency,salary_payment.salary_payment_id,ml_month.ml_month_id,ml_year.ml_year_id,transaction.transaction_date')
	->from('employee')
	->join('employment','employee.employee_id=employment.employee_id')
	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')	
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
	->join('deduction','deduction.employee_id=employee.employee_id','LEFT OUTER')
	->join('ml_pay_frequency','ml_pay_frequency.ml_pay_frequency_id=deduction.deduction_frequency','LEFT OUTER')
	//->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT OUTER')
	->join('allowance','allowance.employment_id=employment.employment_id','LEFT OUTER')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT OUTER')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT OUTER')
	->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT OUTER')
	//->WHERE('ml_pay_frequency.pay_frequency','Monthly')
	
	->join('ml_month','salary_payment.month=ml_month.ml_month_id')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id')
	->like('employee.full_name',$emp_name)
	->like('salary_payment.month',$selc_month)
	->like('salary_payment.year',$selc_year)
	->group_by('salary_payment.salary_payment_id')
	->WHERE('transaction.status',2);
	//->where('salary_payment.salary_id=salary.salary_id');
	$this->db->limit($limit,$start);
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
		} 
		
		public function salarypayment_slip_emp($where2)
		{
		$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,employee.cnic,ml_month.month,ml_year.year,
salary.base_salary,ml_status.status_title,transaction.transaction_date,
payment_mode.payment_mode_type_id,cash_payment.received_by,cash_payment.remarks,
emp_bank_account.account_no,emp_bank_account.bank_name,emp_bank_account.branch,
cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,ml_month.month,
ml_year.year,transaction.transaction_amount,salary_payment.arears')
	->from('employee')
	->where($where2)
	->where('transaction.status',2)
	->join('employment','employee.employee_id=employment.employee_id')
	->join('salary','employment.employment_id=salary.employement_id','LEFT')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')
	->join('payment_mode','transaction.payment_mode=payment_mode.payment_mode_id','LEFT')
	->join('cash_payment','payment_mode.cash_id=cash_payment.cash_payment_id','LEFT')
	->join('emp_bank_account','payment_mode.account_id=emp_bank_account.bank_account_id','LEFT')
	->join('cheque_payments','payment_mode.cheque_id=cheque_payments.cheque_payment_id','LEFT')
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id')
	->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT');
		$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->row();
		}
		
		} 
///////////END

/////////////////////////////////////// Salary Transaction Transfer Bank/////////////
 	public function transaction_info_count()
	{
		$this->db->select('employee.full_name,emp_bank_account.account_no,emp_bank_account.account_title,transaction.calender_month,transaction.transaction_amount,transaction.transaction_date')
	->from('employee')
	//->join('employee','transaction.employee_id=employee.employee_id')
	->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id')
	->join('payment_mode','transaction.payment_mode=payment_mode.payment_mode_id')
	->join('emp_bank_account','payment_mode.account_id=emp_bank_account.bank_account_id')
	->where('transaction.status',2);
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}
	
	public function transaction_info($limit = NULL,$page = NULL)
	{
		$this->db->select('employee.full_name,emp_bank_account.bank_name,emp_bank_account.branch,emp_bank_account.branch_code,emp_bank_account.branch_code,emp_bank_account.account_no,emp_bank_account.account_title,transaction.calender_month,transaction.transaction_amount,transaction.transaction_date')
	->from('employee')
	->join('employment','employee.employee_id=employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
     ->join('salary','employment.employment_id=salary.employement_id')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id')
	->join('payment_mode','transaction.payment_mode=payment_mode.payment_mode_id')
	->join('emp_bank_account','payment_mode.account_id=emp_bank_account.bank_account_id')
	->where('transaction.status',2);
	if(!empty($limit)){
	$this->db->limit($limit,$page);
	}
	
	if($month=$this->input->post('month'))
	{
		$this->db->where('transaction.calender_month',$month);
	}
	if($year=$this->input->post('year'))
	{
		$this->db->where('transaction.calender_year',$year);
	}
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}

///////////////////////////////////////END///////////////////////////////////////

////////////////////////////////////// REPORTS OF PAYROLL ////////////////////////////
	public function expense_report_model()
	{
		$this->db->select('ml_month.ml_month_id,expense_claims.amount,expense_claims.month')
	->from('expense_claims')
	//->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT')
	//->join('transaction','transaction.transaction_id=expense_payment.transaction_id','LEFT')
	->join('ml_month','ml_month.ml_month_id=expense_claims.month','LEFT')
	->where('expense_claims.status',2)
	->where('expense_claims.paid',1)
	->where('expense_claims.year',17);
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}
	
	public function salary_report()
	{
	$this->db->select('salary_payment.month,salary_payment.transaction_amount')
	->from('salary')
	->join('salary_payment','salary_payment.salary_id=salary.salary_id','LEFT')
	->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
	//->join('ml_month','ml_month.ml_month_id=salary_payment.month','LEFT')
	->where('salary_payment.status',2)
	->where('transaction.status',2);
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}
	
	public function transactions_reports()
	{
		$this->db->select('transaction_amount,calender_month')
	->from('transaction')
	->where('transaction.status',2);
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}
	
	public function employee_count($project= NULL)
	{
		$query=$this->db->query("SELECT `employee_project`.`project_id`
FROM (`employee`)
LEFT JOIN `employment` ON `employment`.`employee_id`=`employee`.`employee_id`
LEFT JOIN `salary` ON `salary`.`employement_id`=`employment`.`employment_id`
LEFT JOIN `employee_project` ON `employment`.`employment_id`=`employee_project`.`employment_id`
LEFT JOIN `ml_projects` ON `employee_project`.`project_id`=`ml_projects`.`project_id`
LEFT JOIN `position_management` ON `position_management`.`employement_id`=`employment`.`employment_id`
LEFT JOIN `ml_designations` ON `ml_designations`.`ml_designation_id`=`position_management`.`ml_designation_id`
LEFT JOIN `ml_pay_grade` ON `ml_pay_grade`.`ml_pay_grade_id`=`position_management`.`ml_pay_grade_id`
WHERE `salary`.`status` = 2 AND `employment`.`current` = 1 AND `employee_project`.`current` = 1

group by `employee_project`.`employee_project_id`");
		if($query->num_rows() > 0)
		{return $query->result();}
	}
	/////////// Function for staff salaries
	public function staff_sallaries($limit,$page = NULL,$project = NULL,$name = NULL)
	{
		$this->db->select('employee.full_name,ml_designations.designation_name,
		ml_pay_grade.pay_grade,salary.base_salary,ml_projects.project_title,ml_currency.currency_name')
		->from('employee')
		->join('employment','employment.employee_id=employee.employee_id','LEFT')
	    ->join('salary','salary.employement_id=employment.employment_id','INNER')
		->join('ml_currency','salary.ml_currency=ml_currency.ml_currency_id','LEFT')
		->join('employee_project','employment.employment_id=employee_project.employment_id','INNER')
		->join('ml_projects','employee_project.project_id=ml_projects.project_id','LEFT')
		->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
	->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
	->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id','LEFT')
	->limit($limit,$page)
	->where('employment.current',1)
	->where('employee_project.current',1)
	->order_by('employee_project.employee_project_id','desc')
     ->group_by('employee_project.employment_id');

	if($project)
	{$this->db->where('employee_project.project_id',$project);}
	if($name)
	{
      $this->db->where('employee.employee_id',$name);
    }
	$query=$this->db->get();
	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	}
		////// function for staff salary print
	public function staff_sallaries_print($project = NULL,$name = NULL)
	{
		$this->db->select('employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,salary.base_salary,ml_projects.project_title')
			->from('employee')
			->join('employment','employment.employee_id=employee.employee_id','LEFT')
			->join('salary','salary.employement_id=employment.employment_id','INNER')
			->join('employee_project','employment.employment_id=employee_project.employment_id','LEFT')
			->join('ml_projects','employee_project.project_id=ml_projects.project_id','INNER')
			->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
			->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
			->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id','LEFT')
			->where('employment.current',1)
            ->where('employee_project.current',1)
            ->order_by('employee_project.employee_project_id','desc')
			->group_by('employee_project.employment_id');

		if($project)
		{$this->db->where('employee_project.project_id',$project);}
		if($name)
		{$this->db->like('employee.full_name',$name);}
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	//// end
	public function count_payrollsheet($month,$year)
	{	$month=intval($month);
		$year=intval($year);
		$query=$this->db->query("SELECT employee_project.employment_id
		FROM (`employee`) 
 LEFT JOIN `employment` ON `employee`.`employee_id`=`employment`.`employee_id` 
 LEFT JOIN `employee_project` ON `employment`.`employment_id`=`employee_project`.`employment_id`
 LEFT JOIN `ml_projects` ON `employee_project`.`project_id`=`ml_projects`.`project_id`
 LEFT JOIN `salary` ON `employment`.`employment_id`=`salary`.`employement_id` 
 LEFT JOIN `salary_payment` ON `salary`.`salary_id`=`salary_payment`.`salary_id`
WHERE `salary`.`status` =2
AND `salary_payment`.`month`='$month'
AND `salary_payment`.`year`='$year'
AND `employee_project`.`current` = 1
GROUP BY `employee_project`.`project_id`");

 if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
	}
	/////////// Function for payroll_sheet
	public function payroll_sheet($limit = NULL,$page = NULL,$month,$year,$name = NULL,$code,$project =NULL)
	{

	//$emp_name=$this->input->post('emp_name');
	$query=$this->db->query("SELECT `employee`.`employee_id`, `employee`.`employee_code`, `employee`.`full_name`, `ml_pay_grade`.`pay_grade`,`ml_designations`.`designation_name`, `salary`.`base_salary`,
SUM(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$month) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$month AND deduction.year=$year) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1 AND allowance.ml_allowance_type_id=3) THEN allowance.allowance_amount WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year AND allowance.ml_allowance_type_id=3) THEN allowance.allowance_amount ELSE 0 END) as allowance_utilities,
SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$month AND expense_claims.year=$year AND expense_claims.paid=1) THEN expense_claims.amount ELSE 0 END) as exp_amount,
SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$month AND loan_advance_payment.year =$year AND loan_advances.paid_back=1)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advance_payback.month =$month AND loan_advance_payback.year =$year AND advt.status=2)THEN advt.transaction_amount ELSE 0 END) as loan_amount,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN allowance.allowance_amount  ELSE NULL END) as grp_allow_amnt,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN ml_allowance_type.allowance_type
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN ml_allowance_type.allowance_type  ELSE NULL END) as grp_allow_type,
(CASE WHEN (salary_payment.month=$month AND salary_payment.year=$year) THEN salary_payment.arears ELSE 0 END) as salary_arears,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=4) THEN deduction.deduction_amount ELSE 0 END) as tax_ded,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=5) THEN deduction.deduction_amount ELSE 0 END) as eobi_ded,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=6) THEN deduction.deduction_amount ELSE 0 END) as other_ded,
`employment`.`employee_id`, `loan_advances`.`loan_advance_id`, (loan_advance_payback.loan_advance_id) as loan_payback_id, `expense_claims`.`expense_claim_id`,
(deduction.month) as ded_month, (deduction.year) as ded_year, (expense_claims.month) as exp_month, (expense_claims.year) as exp_year,
(allowance_payment.month) as alwnc_month, (allowance_payment.year) as alwnc_year, (loan_advance_payment.month) as alonad_month, (loan_advance_payment.year) as alonad_year,
`ml_status`.`status_title`, `allowance`.`pay_frequency`, `deduction`.`deduction_frequency`,(loan_advance_payback.month)  as back_month,(loan_advance_payback.year) as back_year,
(expense_disbursment.expense_id) as exp_dis_id,loan_advances.paid,loan_advances.paid_back,salary_payment.transaction_amount
	
 FROM (`employee`)
 LEFT JOIN `employment` ON `employee`.`employee_id`=`employment`.`employee_id` 

 LEFT JOIN `employee_project` ON `employment`.`employment_id`=`employee_project`.`employment_id`
 LEFT JOIN `ml_projects` ON `employee_project`.`project_id`=`ml_projects`.`project_id`
 LEFT JOIN `salary` ON `employment`.`employment_id`=`salary`.`employement_id` 
 LEFT JOIN `salary_payment` ON `salary`.`salary_id`=`salary_payment`.`salary_id` 
 LEFT JOIN `ml_status` ON `salary`.`status`=`ml_status`.`status_id`
 LEFT JOIN `position_management` ON `employment`.`employment_id`=`position_management`.`employement_id`
 LEFT JOIN `ml_pay_grade` ON `ml_pay_grade`.`ml_pay_grade_id`=`position_management`.`ml_pay_grade_id`
 LEFT JOIN `ml_designations` ON `ml_designations`.`ml_designation_id`=`position_management`.`ml_designation_id`
 LEFT JOIN `deduction` ON `deduction`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `ml_pay_frequency` ON `ml_pay_frequency`.`ml_pay_frequency_id`=`deduction`.`deduction_frequency`
 LEFT JOIN `allowance` ON `allowance`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `ml_allowance_type` ON `allowance`.`ml_allowance_type_id`=`ml_allowance_type`.`ml_allowance_type_id`
 LEFT JOIN `allowance_payment` ON `allowance_payment`.`allowance_id`=`allowance`.`allowance_id`
 LEFT JOIN `expense_claims` ON `employment`.`employment_id`=`expense_claims`.`employment_id`
 LEFT JOIN `expense_disbursment` ON `expense_disbursment`.`expense_id`=`expense_claims`.`expense_claim_id`
 LEFT JOIN `loan_advances` ON `loan_advances`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `loan_advance_payment` ON `loan_advance_payment`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 LEFT JOIN `loan_advance_payback` ON `loan_advance_payback`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 LEFT JOIN `transaction` `advt` ON `advt`.`transaction_id`=`loan_advance_payback`.`transaction_id`

 WHERE `salary`.`status` =2
 AND `position_management`.`current` =1
 AND `position_management`.`status` =2
 AND `employee_project`.`current` = 1
 AND `salary_payment`.`month`='$month'
 AND `salary_payment`.`year`='$year'
 AND `employee`.`employee_id` like '%$name%'
 AND `employee`.`employee_code` like '$code%'
 AND `employee_project`.`project_id` like '$project%'
 GROUP BY `salary`.`salary_id`
 LIMIT $limit OFFSET $page");
 /*if($project)
 {$this->db->where('assign_job.project_id',$project);}*/
	//return $this->db->last_query();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
	}
	/////////// Function for payroll_sheet print
	public function payroll_sheet_print($month,$year,$name = NULL,$code = NULL,$project =NULL)
	{

		//$emp_name=$this->input->post('emp_name');
		$query=$this->db->query("SELECT `employee`.`employee_id`, `employee`.`employee_code`, `employee`.`full_name`, `ml_pay_grade`.`pay_grade`,`ml_designations`.`designation_name`, `salary`.`base_salary`,
SUM(DISTINCT CASE WHEN (deduction.status=2 AND deduction.deduction_frequency=1) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=2 AND deduction.month=$month) THEN deduction.deduction_amount WHEN (deduction.status=2 AND deduction.deduction_frequency=3 AND deduction.month=$month AND deduction.year=$year) THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN allowance.allowance_amount  ELSE 0 END) as allowance_amnt,
SUM(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1 AND allowance.ml_allowance_type_id=3) THEN allowance.allowance_amount WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year AND allowance.ml_allowance_type_id=3) THEN allowance.allowance_amount ELSE 0 END) as allowance_utilities,
SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_claims.month=$month AND expense_claims.year=$year AND expense_claims.paid=1) THEN expense_claims.amount ELSE 0 END) as exp_amount,
SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advances.month =$month AND loan_advance_payment.year =$year AND loan_advances.paid_back=1)THEN loan_advances.amount WHEN (loan_advances.status=2 AND loan_advances.paid=1 AND loan_advance_payback.month =$month AND loan_advance_payback.year =$year AND advt.status=2)THEN advt.transaction_amount ELSE 0 END) as loan_amount,
(CASE WHEN (salary_payment.month=$month AND salary_payment.year=$year) THEN salary_payment.arears ELSE 0 END) as salary_arears,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=4) THEN deduction.deduction_amount ELSE 0 END) as tax_ded,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=5) THEN deduction.deduction_amount ELSE 0 END) as eobi_ded,
(CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id=6) THEN deduction.deduction_amount ELSE 0 END) as other_ded,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN allowance.allowance_amount
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN allowance.allowance_amount  ELSE NULL END) as grp_allow_amnt,
group_concat(DISTINCT CASE WHEN (allowance.status=2 AND allowance.pay_frequency=1) THEN ml_allowance_type.allowance_type
WHEN (allowance.status=2 AND allowance.month=$month AND allowance.year=$year) THEN ml_allowance_type.allowance_type  ELSE NULL END) as grp_allow_type,
`employment`.`employee_id`, `loan_advances`.`loan_advance_id`, (loan_advance_payback.loan_advance_id) as loan_payback_id, `expense_claims`.`expense_claim_id`,
(deduction.month) as ded_month, (deduction.year) as ded_year, (expense_claims.month) as exp_month, (expense_claims.year) as exp_year,
(allowance_payment.month) as alwnc_month, (allowance_payment.year) as alwnc_year, (loan_advance_payment.month) as alonad_month, (loan_advance_payment.year) as alonad_year,
`ml_status`.`status_title`, `allowance`.`pay_frequency`, `deduction`.`deduction_frequency`,(loan_advance_payback.month)  as back_month,(loan_advance_payback.year) as back_year,
(expense_disbursment.expense_id) as exp_dis_id,loan_advances.paid,loan_advances.paid_back,salary_payment.transaction_amount

 FROM (`employee`)
 LEFT JOIN `employment` ON `employee`.`employee_id`=`employment`.`employee_id`
 LEFT JOIN `employee_project` ON `employment`.`employment_id`=`employee_project`.`employment_id`
 LEFT JOIN `ml_projects` ON `employee_project`.`project_id`=`ml_projects`.`project_id`
 LEFT JOIN `salary` ON `employment`.`employment_id`=`salary`.`employement_id`
 LEFT JOIN `salary_payment` ON `salary`.`salary_id`=`salary_payment`.`salary_id`
 LEFT JOIN `ml_status` ON `salary`.`status`=`ml_status`.`status_id`
 LEFT JOIN `position_management` ON `employment`.`employment_id`=`position_management`.`employement_id`
 LEFT JOIN `ml_pay_grade` ON `ml_pay_grade`.`ml_pay_grade_id`=`position_management`.`ml_pay_grade_id`
LEFT JOIN `ml_designations` ON `ml_designations`.`ml_designation_id`=`position_management`.`ml_designation_id`
 LEFT OUTER JOIN `deduction` ON `deduction`.`employment_id`=`employment`.`employment_id`
 LEFT OUTER JOIN `ml_pay_frequency` ON `ml_pay_frequency`.`ml_pay_frequency_id`=`deduction`.`deduction_frequency`
 LEFT OUTER JOIN `allowance` ON `allowance`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `allowance_payment` ON `allowance_payment`.`allowance_id`=`allowance`.`allowance_id`
 LEFT JOIN `ml_allowance_type` ON `allowance`.`ml_allowance_type_id`=`ml_allowance_type`.`ml_allowance_type_id`
 LEFT OUTER JOIN `expense_claims` ON `employment.employment_id`.`employee_id`=`expense_claims`.`employment_id`
 LEFT OUTER JOIN `expense_disbursment` ON `expense_disbursment`.`expense_id`=`expense_claims`.`expense_claim_id`
 LEFT JOIN `loan_advances` ON `loan_advances`.`employment_id`=`employment`.`employment_id`
 LEFT JOIN `loan_advance_payment` ON `loan_advance_payment`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 LEFT OUTER JOIN `loan_advance_payback` ON `loan_advance_payback`.`loan_advance_id`=`loan_advances`.`loan_advance_id`
 LEFT JOIN `transaction` `advt` ON `advt`.`transaction_id`=`loan_advance_payback`.`transaction_id`

 WHERE `salary`.`status` =2
 AND `position_management`.`current` =1
 AND `position_management`.`status` =2
 AND `employee_project`.`current` = 1
 AND `salary_payment`.`month`='$month'
 AND `salary_payment`.`year`='$year'
 AND `employee`.`full_name` like '%$name%'
 AND `employee`.`employee_code` like '$code%'
 AND `employee_project`.`project_id` like '$project%'
 GROUP BY `salary`.`salary_id`");
		/*if($project)
         {$this->db->where('assign_job.project_id',$project);}*/
		//return $this->db->last_query();
		if($query->num_rows()>0)
		{
			return $query->result();
		}

	}
	public function employees_account($name = NULL,$bank = NULL,$branch = NULL,$active = NULL)
	{
		$this->db->select('employee.full_name,employee.employee_code,
		emp_bank_account.bank_account_id,emp_bank_account.account_no,
		emp_bank_account.account_title,emp_bank_account.bank_name,
		emp_bank_account.branch,emp_bank_account.branch_code,emp_bank_account.employee_id,emp_bank_account.bank_account_id,
		emp_bank_account.active')
			->from('emp_bank_account')
			->join('employee','emp_bank_account.employee_id=employee.employee_id','inner')
			->order_by('emp_bank_account.active','asc');
		if($name)
		{
			$this->db->like('employee.full_name',$name);
		}
		if($bank)
		{
			$this->db->like('emp_bank_account.bank_name',$bank);
		}
		if($branch)
		{
			$this->db->like('emp_bank_account.branch',$branch);
		}
		if($active)
		{
			$this->db->like('emp_bank_account.active',$active);
		}

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	public function update_account_activation($data,$account_id,$employee_id)
	{
		$this->db->trans_start();
		$this->db->where(array(
			'employee_id'=>$employee_id
		));
		$query=$this->db->update('emp_bank_account',array('active'=>2));
		$this->db->where(array(
			'bank_account_id'=>$account_id,
			'employee_id'=>$employee_id
		));
		$query=$this->db->update('emp_bank_account',$data);

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
	}
//////////////////////////////////////////END////////////////////////////////////////	
	/// ********************************************* *** **** ****** *** **** *********************************************///
	/// ============================================= Pay Roll Module End Here ============================================///
	/// ********************************************* *** **** ****** *** **** ********************************************///
	public function leave_attend()
	{
		$this->datatables->select('la.employee_name,la.leave_type,la.duration,la.status,la.employee_id,la.leave_id')
		->from('leave_attendance la')
		->unset_column('la.leave_id');
		
        
               
        if($leave_type = $this->input->post('leave_type'))
		{
		 	$this->datatables->where('la.leave_type',$leave_type);
		}
		if($duration = $this->input->post('duration'))
		{
			$this->datatables->where('la.duration',$duration);
		}
		
			$this->datatables->edit_column('la.employee_id',
			'<a href="site/edit_user/$1"><span class="fa fa-eye"></span></a>
			&nbsp;  
			<a href="site/leave_act/$1"><span class="btn green" style="float:right; margin-right:140px;">Action</span></a>',
			'la.employee_id');
		//}
       
		return $this->datatables->generate();
	}

	public function user_r()
	{
		$this->datatables->select('emp.employee_name,emp.username,emp.email,emp.user_role,emp.status,emp.employee_id')
		->from('user_management emp');
		
        $this->datatables->edit_column('emp.employee_name',
		'<a href="site/edit_user/$1">$2</a>',
		'emp.employee_id,emp.employee_name');
		
        $this->datatables->edit_column('emp.username',
		'<a href="site/edit_user/$1">$2</a>',
		'emp.employee_id,emp.username');
		
        $this->datatables->edit_column('emp.email',
		'<a href="site/edit_user/$1">$2</a>',
		'emp.employee_id,emp.email');
		
        $this->datatables->edit_column('emp.user_role',
		'<a href="site/edit_user/$1">$2</a>',
		'emp.employee_id,emp.user_role');
		
        $this->datatables->edit_column('emp.status',
		'<a href="site/edit_user/$1">$2</a>',
		'emp.employee_id,check_status(emp.status)');
		//$this->datatables->edit_column('user.user_account_id', '$1','check_status(user.login_status)', 'user.user_account_id');
               
        if($user_role = $this->input->post('user_role'))
		{
		 	$this->datatables->where('emp.user_role',$user_role);
		}
		if($username = $this->input->post('username'))
		{
			$this->datatables->where('emp.username',$username);
		}
		/*if($this->session->userdata('user_role') == 'Admin')
		{*/
			$this->datatables->edit_column('emp.employee_id',
			'<a href="site/edit_user/$1"><span class="fa fa-pencil"></span></a>
			&nbsp; &nbsp;<a href="site/delete_user/$1" onclick ="return confirm()"><span class="fa fa-eye"></span></a>',
			'emp.employee_id');
		//}
       
		return $this->datatables->generate();
	}
		
/*		public function create_new_user(){
		
			$insert = $this->db->insert('user_management', $new_member_insert_data);
			return $insert;
	}*/
	public function drop_down3() 
	 {
  		$rows = $this->db->select('leave_type')
		->from('leave_attendance')
		->get()
		->result();
		$drop_down = array('' => '-- Select Leave Type --');
		foreach($rows as $row)
		{
			$drop_down[$row->leave_type] = $row->leave_type;
			}
  		return $drop_down;
	}
	public function drop_down4() 
	 {
  		$rows = $this->db->select('duration')
		->from('leave_attendance')
		->get()
		->result();
		$drop_down = array('' => '-- Select Duration --');
		foreach($rows as $row)
		{
			$drop_down[$row->duration] = $row->duration;
			}
  		return $drop_down;
	}
	 public function drop_down() 
	 {
  		$rows = $this->db->select('username')
		->from('user_management')
		->get()
		->result();
		$drop_down = array('' => '-- Select User Name --');
		foreach($rows as $row)
		{
			$drop_down[$row->username] = $row->username;
			}
  		return $drop_down;
	}
		 public function drop_down2() 
	 {
  		$rows = $this->db->select('user_role')
		->from('user_management')
		->get()
		->result();
		$drop_down2 = array('' => '-- Select User Role --');
		foreach($rows as $row)
		{
			$drop_down2[$row->user_role] = $row->user_role;
			}
  		return $drop_down2;
	}
        public  function user_dropdown() {
  		$this->db->select('*');
  		$this->db->distinct();
  		$query = $this->db->get('user');
  		if($query->num_rows() > 0) {
   		$data[''] = '--Select Option--';
   		foreach($query->result() as $row) {
    	        $data[$row->user_id] = $row->user_name;
   	 	}
   		return $data;
 		}
	}
        
        
        public  function html_select_box($table,$key,$value) {
  		$this->db->select('*');
//  		$this->db->distinct();
  		$query = $this->db->get($table);
  		if($query->num_rows() > 0) {
   		$data[''] = '--Select Option--';
   		foreach($query->result() as $row) {
    	        $data[$row->$key] = $row->$value;
   	 	}
   		return $data;
 		}
	}
        
        
        public  function html_selectbox($table,$option,$key,$value,$where) {
  		$this->db->select('*');
		$this->db->where($where);
  		$query = $this->db->get($table);
  		if($query->num_rows() > 0) {
			
   		$data[''] = $option;
   		foreach($query->result() as $row) {
    	        $data[$row->$key] = $row->$value;
   	 	}
   		return $data;
 		}
		
	}

	public  function html_selectbox_desc($table,$option,$key,$value,$where) {
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($key,"desc");
		$query = $this->db->get($table);
		if($query->num_rows() > 0) {

			$data[''] = $option;
			foreach($query->result() as $row) {
				$data[$row->$key] = $row->$value;
			}
			return $data;
		}

	}
        

        public function get_dept_dropdown(){
                $query = $this->db->get('department');
                if($query->num_rows() > 0) {
   		return  $query;
 		}
   	}
	public function search_username($where)
	{
		return $query = $this->db->get_where('user_management', array('employee_name'=> $where))->result();
		}

       
        public function fetch_user($id){
            $this->db->select('*')->from('user');
            $this->db->where('dept_id',$id);
            $query = $this->db->get();
            if($query->num_rows > 0){
                foreach($query->result() as $row)
                {
                $data[$row->user_id] = $row->user_name;
                }
             return $data;
            }else{
             return array();
            }
        }

        
        public function do_upload($file_name) {
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|png|gif';
            $config['max_size'] = '10000000000';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload($file_name)){
             return false;
            }else{
             $data = array('upload_data' => $this->upload->data());
             return $data;
            }
	}
         
         
        public function upload_pic($image) {
            $config['upload_path'] = 'images/';
            $config['allowed_types'] = 'jpg|png|gif';
            $config['max_size'] = '10000000000';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload($image)){
             return false;
            }else{
             $data = array('upload_data' => $this->upload->data());
             return $data;
            }
	}
        

        public function get_row_by_id($table,$where){
            $this->db->select('*')->from($table);
            $this->db->where($where);
            $query = $this->db->get();
                if($query->num_rows() > 0){
                 return $query->row();
                }
	}
        
        public function get_row_by_where($table,$where){
            $this->db->select('*')->from($table);
            $this->db->where($where);
            $query = $this->db->get();
                if($query->num_rows() > 0){
                 return $query->row();
                }
	}
        
        
        

        public function get_num_rows_by_where($table,$where){
            $this->db->select('*')->from($table);
            $this->db->where($where);
            $query = $this->db->get();
                if($query->num_rows() > 0){
                return $query->num_rows();
                }
	}

	public function create_new_record($table,$data){
            $query = $this->db->insert($table, $data);
                if($query){
                return true;
                } else {
                return false;
                }
	}
	

        public function insert_data_get_last_id($table,$data){
            $query = $this->db->insert($table, $data);
            return $this->db->insert_id();
	}


        public function get_last_id() {
           return $this->db->insert_id();
        }
       
        public function delete_row_by_where($table,$where){
            $this->db->where($where);
            $query = $this->db->delete($table);
                if($query){
                return true;
                }
        }
		
             
        public function update_record($table,$where,$data){
            $this->db->where($where);
            $query = $this->db->update($table,$data);
	       if($query){
	        return true;
		}else {
		return false;
		}
	}

        public  function get_all($table){
	    $query = $this->db->get($table);
                if($query->num_rows() > 0){
                return $query->result();
                }
       }

        public function get_all_by_where($table,$where){
	    $this->db->select('*');
	    $this->db->where($where);
	    $query = $this->db->get($table);
		if($query->num_rows() > 0){
		return $query->result();
		}
	}

	
        public  function count_by_where($table,$where){
            $this->db->where($where);
            $query = $this->db->get($table);
               if($query->num_rows() > 0){
		return $query->num_rows();
		}
        }
        
        public function redirect_to($location = NULL){
            if($location != ''){
               redirect($location); 
            }    
        }
       
        public function get_employee_list($where = NULL){
                $this->datatables
		->select('emp.employee_id,emp.full_name,job.job_title,designation.designation_name,catagory.job_category_name,user.user_account_id,user.login_status')
		->from('employee emp')
                ->unset_column('emp.employee_id')
                ->join('emp_experience exp','exp.employee_id=emp.employee_id','inner')  
                ->join('master_list_job_title job','job.job_id=exp.job_id','inner')
                ->join('master_list_designations designation','designation.designation_id=exp.designation_id','inner')
                ->join('employment emplment','emplment.employee_id=emp.employee_id','inner')  
                ->join('master_list_job_category catagory','catagory.job_category_id=emplment.category_id','inner')
                ->join('user_account user','user.employee_id=emp.employee_id','inner')
                ->where($where);
                $this->datatables->edit_column('emp.full_name','<a href="site/view_emp_detail/$1">$2</a>','emp.employee_id,emp.full_name');
                $this->datatables->edit_column('job.job_title','<a href="site/view_emp_detail/$1">$2</a>','emp.employee_id,job.job_title');
                $this->datatables->edit_column('designation.designation_name','<a href="site/view_emp_detail/$1">$2</a>','emp.employee_id,designation.designation_name');
                 $this->datatables->edit_column('catagory.job_category_name','<a href="site/view_emp_detail/$1">$2</a>','emp.employee_id,catagory.job_category_name');
                $this->datatables->edit_column('user.user_account_id','<a href="site/view_emp_detail/$1">$2</a>','emp.employee_id,check_status(user.login_status)');
//                $this->datatables->edit_column('user.user_account_id', '$1','check_status(user.login_status)', 'user.user_account_id');
               
                if($job_id = $this->input->post('job_title')){
		 $this->datatables->where('job.job_id',$job_id);
		}
		if($designation_id = $this->input->post('designation')){
		$this->datatables->where('designation.designation_id',$designation_id);
		}
                if($category_id= $this->input->post('category_id')){
		$this->datatables->where('catagory.job_category_id',$category_id);
		}

		return $this->datatables->generate();
        }

   
        public function view_emp_detail($where = NULL){
               $this->db->select('doc.doc_id,doc.user_id,doc.dept_id,doc.cat_id,dev.version_id,doc.doc_name,doc.doc_file,doc.doc_descp
                ,doc.date_created,doc.date_modified,dev.doc_version_file_name,dev.modified_by,dev.doc_keywords,dev.version,dev.total_version,dev.sendto_trash,dev.check_status,dev.doc_version_desc_log,
                ,permn.doc_id,permn.user_id,permn.permission_id,dept.dept_id,dept.dept_name,cat.cat_id,cat.cat_name,u.user_id,u.user_name,u.user_type');
                $this->db->limit(1);
                $this->db->from('employee emp');
                $this->db->unset_column('emp.employee_id');
                $this->db->join('emp_experience exp','exp.employee_id=emp.employee_id','inner'); 
                $this->db->join('master_list_job_title job','job.job_id=exp.job_id','inner');
                $this->db->join('master_list_designations designation','designation.designation_id=exp.designation_id','inner');
                $this->db->join('employment emplment','emplment.employee_id=emp.employee_id','inner');
                $this->db->join('master_list_job_category catagory','catagory.job_category_id=emplment.category_id','inner');
                $this->db->join('user_account user','user.employee_id=emp.employee_id','inner');
                $this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows > 0){
		return $query->row();
                 }
 
        }
        
}

?>