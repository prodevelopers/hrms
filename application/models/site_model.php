<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	
	
	
	//********************************* ***** *****************************************//
	//================================= reports =========================================//
	///******************************** ***** ****************************************//
		//********************************* ***** *****************************************//
	//================================= reports =========================================//
	///******************************** ***** ****************************************//
	
		///// Function for Absenties Report Queries ////
	
	public function absenties()
	{
		$this->db->select('ml_attendance_status_type.status_type,ml_month.month,employee.employee_id,(count(attendence.attendance_status_type)) as counter')
		->from('attendence')
		->join('employee','employee.employee_id = attendence.employee_id')
		->join('ml_attendance_status_type','attendence.attendance_status_type =  ml_attendance_status_type.attendance_status_type_id')
		->join('ml_month','attendence.ml_month_id =  ml_month.ml_month_id')
		
		->where('attendence.attendance_status_type = 2');
		$this->db->group_by('ml_month.ml_month_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	//// Function for Leave Report Queries ////
	
	public function leave_report()
	  {
		$this->db->select(' ml_leave_type.leave_type,ml_month.month,employee.employee_id,(count(leave_application.ml_leave_type_id)) as counter')
		->from('leave_application')
		->join('employee','employee.employee_id = leave_application.employee_id')
		->join('ml_leave_type','leave_application.ml_leave_type_id =  ml_leave_type.ml_leave_type_id')
		->join('attendence','employee.employee_id =  attendence.employee_id')
		->join('ml_month','attendence.ml_month_id =  ml_month.ml_month_id');
		//->where('attendence.attendance_status_type = 2');*/
		$this->db->group_by('ml_month.ml_month_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	
	public function district_view()
	{
		$this->db->select('ml_district.district_name,employee.employee_id,(count(current_contacts.District)) as counter')
		->from('employee')
		->join('current_contacts','employee.employee_id = current_contacts.employee_id')
		->join('ml_district','current_contacts.District = ml_district.district_id');
		$this->db->group_by('District');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////
	public function skill_report_view()
	{
		$this->db->select('ml_skill_type.skill_name,employee.employee_id,(count(emp_skills.ml_skill_type_id)) as counter')
		->from('employee')
		->join('employment','employment.employee_id = employee.employee_id')
		->join('emp_skills','employment.employment_id = emp_skills.employment_id')
		->join('ml_skill_type','emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id');
		$this->db->group_by('ml_skill_type_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////
	public function qualification_report_view()
	{
		$this->db->select('ml_qualification_type.qualification_title,employee.employee_id,(count(qualification.qualification_type_id)) as counter')
		->from('employee')
		->join('qualification','employee.employee_id = qualification.employee_id')
		->join('ml_qualification_type','qualification.qualification_type_id = ml_qualification_type.qualification_type_id');
		$this->db->group_by('qualification.qualification_type_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function training_report_view()
	{
		$this->db->select('training.date,training.training_name,count(training.training_name) as tol,employee.employee_id')
		->from('employee')
		->join('training','employee.employee_id = training.employee_id');
		$this->db->group_by('training.training_name');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function experience_report_view()
	{
		$this->db->select('emp_skills.experience_in_years,count(emp_skills.employment_id) as tol,employee.employee_id')
		->from('employee')
		->join('employment','employee.employee_id = employment.employee_id')
		->join('emp_skills','employment.employment_id = emp_skills.employment_id');
		$this->db->group_by('emp_skills.experience_in_years');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////
	public function joiner_leaver_report()
	{
		$this->db->select('YEAR(employment.joining_date) as year,count(employment.employment_id) as joiner,YEAR(seperation_management.date) as sep_year,count(seperation_management.seperation_id) as rtire,employee.employee_id')
		->from('employee')
		->join('employment','employee.employee_id = employment.employee_id')
		->join('seperation_management','employee.employee_id = seperation_management.employee_id');
		$this->db->group_by('year');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
public function employee_progress()
	{
		$this->db->select('performance_evaluation.ml_kpi_type_id,ml_kpi.kpi,(count(ml_kpi.ml_kpi_id)) as counter')
		->from('performance_evaluation')
		->join('ml_kpi','performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id');
		//->join('ml_district','current_contacts.District = ml_district.district_id');
		$this->db->group_by('ml_kpi.ml_kpi_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

////////////////////////////////// Function for pagination Staff Report /////////////////////////////


	public function staff_report_count()
	{
		$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,qualification.qualification,emp_experience.to_date, emp_experience.from_date')

			->from('employee')

			->join('employment', 'employment.employment_id = employee.employee_id','inner')

			->join('position_management', 'employment.employment_id = position_management.employement_id','inner')

			->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','inner')

			->join('qualification', 'employee.employee_id = qualification.employee_id','inner')

			->join('emp_experience', 'employee.employee_id = emp_experience.employee_id','inner');


		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
	/// END

	///////////////////////////////////////////// Function for Staff Report in Site Model  ////////////////////////////////////////

	public function staff_report($limit = NULL, $start = NULL)
	{
		$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,qualification.qualification,(emp_experience.to_date) AS to_d,(emp_experience.from_date) AS frm_d')

			->from('employee')

			->join('employment', 'employment.employment_id = employee.employee_id','inner')

			->join('position_management', 'employment.employment_id = position_management.employement_id','inner')

			->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','inner')

			->join('qualification', 'employee.employee_id = qualification.employee_id','inner')

			->join('emp_experience', 'employee.employee_id = emp_experience.employee_id','inner')
			->limit($limit,$start)
			->group_by('employee.employee_id');

		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		//return $this->datatables->generate();


	}
	///// END
	
	//********************************* ********* *****************************************//
	//================================= DASHBOARD =========================================//
	///******************************** ********* ****************************************//

    public function separation_retir()
    {
        $this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_status.status_title,seperation_management.seperation_id,employee.employee_id')

            ->from('seperation_management')

            ->join('employee', 'employee.employee_id = seperation_management.employee_id')

            ->join('employment', 'employment.employment_id = employee.employee_id')

            ->join('position_management', 'employment.employment_id = position_management.employement_id')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')

            ->join('ml_status', 'seperation_management.status = ml_status.status_id')

            ->where('seperation_management.status = 1')
            ->group_by('seperation_management.seperation_id');
        $this->datatables->edit_column('seperation_management.seperation_id',
            '<a href="dashboard_site/action_separation_retir/$1/$2"><button class= "btn green">Action</button></a>',
            'seperation_management.seperation_id,employee.employee_id');

        if($approval = $this->input->post('status_title'))
        {
            $this->datatables->where('ml_status.status_id', $approval);
        }
        return $this->datatables->generate();


    }
    public function get_separation_retir($where)
    {
        $this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_separation_type.separation_type,ml_posting_reason_list.posting_reason,ml_status.status_title,seperation_management.seperation_id,employee.employee_id")

            ->from('seperation_management')

            ->join('employee', 'employee.employee_id = seperation_management.employee_id')

            ->join('employment', 'employment.employment_id = employee.employee_id')

            ->join('position_management', 'employment.employment_id = position_management.employement_id')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')

            ->join('ml_separation_type', 'seperation_management.ml_seperation_type = ml_separation_type.separation_type_id')

            ->join('ml_posting_reason_list', 'seperation_management.reason = ml_posting_reason_list.posting_reason_list_id')

            ->join('ml_status', 'seperation_management.status = ml_status.status_id')


            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }


    //////////////////////////function for laeve entitlement /////////////////////////////
    /**
     * @param $where
     * @return mixed
     * @fuction Deprecated Model Method
     */
/*public function posting()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_branch.branch_name,ml_department.department_name,ml_city.city_name,ml_posting_location_list.posting_location,ml_shift.shift_name,ml_status.status_title,posting.posting_id,employee.employee_id')
		
		->from('posting')
		
		->join('employment', 'employment.employment_id = posting.employement_id','left')
		
		->join('employee', 'employee.employee_id = employment.employee_id','left')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id','left')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','left')
		
		->join('ml_branch', 'posting.ml_branch_id = ml_branch.branch_id','left')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id','left')
		
		->join('ml_city', 'posting.ml_city_id = ml_city.city_id','left')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id','left')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id','left')
		
		->join('ml_status', 'posting.status = ml_status.status_id','left')
		
		->where('posting.status = 1 AND employee.enrolled = 1 AND posting.trashed = 0 AND posting.current = 1');
		//->group_by('posting.posting_id');
		$this->datatables->edit_column('posting.posting_id',
				'<a href="dashboard_site/action_posting_new/$1/$2"><button class= "btn green">Action</button></a>',
				'posting.posting_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
	
	public function get_posting_get($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_branch.branch_name,ml_department.department_name,ml_city.city_name,ml_posting_location_list.posting_location,ml_shift.shift_name,ml_status.status_title,posting.transfer_remarks,posting.posting_id,employee.employee_id")
		
		->from('posting')
		
		->join('employment', 'employment.employment_id = posting.employement_id')
		
		->join('employee', 'employee.employee_id = employment.employee_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_branch', 'posting.ml_branch_id = ml_branch.branch_id')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id')
		
		->join('ml_city', 'posting.ml_city_id = ml_city.city_id')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id')
		
		->join('ml_status', 'posting.status = ml_status.status_id')
		
		->where($where);
			
		$query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
///////////////////////////new work /////////////////////////////////////////////	
	public function deductions()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_deduction_type.deduction_type,deduction.deduction_amount,ml_status.status_title,deduction.deduction_id,employee.employee_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0')

		->join('deduction', 'employment.employment_id  = deduction.employment_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_deduction_type', 'deduction.ml_deduction_type_id = ml_deduction_type.ml_deduction_type_id')
		
		->join('ml_status', 'deduction.status = ml_status.status_id')
		
		->where('deduction.status = 1')
		->group_by('deduction.deduction_id');
		$this->datatables->edit_column('deduction.deduction_id',
				'<a href="dashboard_site/action_deductions/$1/$2"><button class= "btn green">Action</button></a>',
				'deduction.deduction_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function get_deduction_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_deduction_type.deduction_type,deduction.deduction_amount,ml_status.status_title,deduction.note,deduction.deduction_id,employee.employee_id")
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id')
		
		->join('deduction', 'employment.employment_id = deduction.employment_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_deduction_type', 'deduction.ml_deduction_type_id = ml_deduction_type.ml_deduction_type_id')
		
		->join('ml_status', 'deduction.status = ml_status.status_id')
		
		->where($where);
			
		$query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method
     */
/*	public function expense_claim()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,expense_claims.amount,ml_month.month,ml_year.year,ml_status.status_title,expense_claims.expense_claim_id,employee.employee_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id')

        ->join('expense_claims', 'employment.employment_id = expense_claims.employment_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_month', 'expense_claims.month = ml_month.ml_month_id')
		
		->join('ml_year', 'expense_claims.year = ml_year.ml_year_id')
		
		->join('ml_status', 'expense_claims.status = ml_status.status_id')
		
		->where('expense_claims.status = 1')
		->group_by('expense_claims.expense_claim_id');
		$this->datatables->edit_column('expense_claims.expense_claim_id',
				'<a href="dashboard_site/action_expense_claim/$1/$2"><button class= "btn green">Action</button></a>',
				'expense_claims.expense_claim_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
	//////////////////////////////////////////////////////////////////////////////////////////////////
	public function get_expense_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,expense_claims.amount,ml_expense_type.expense_type_title,ml_month.month,ml_year.year,ml_status.status_title,expense_claims.expense_claim_id,employee.employee_id,expense_claims.note")
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0')

        ->join('expense_claims', 'employment.employment_id = expense_claims.employment_id')
            ->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_month', 'expense_claims.month = ml_month.ml_month_id')
		
		->join('ml_year', 'expense_claims.year = ml_year.ml_year_id')
		
		->join('ml_status', 'expense_claims.status = ml_status.status_id')
		
		->where($where);
			
		$query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}

	//////////////////////////////////////////end of new work//////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $where
     * @return mixed
     * @function leave_entitlement
     */
//		public function leave_entitlement()
//	{
//		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_leave_type.leave_type,ml_status.status_title,leave_entitlement.leave_entitlement_id,employee.employee_id')
//
//		->from('leave_entitlement')
//
//        ->join('employment', 'employment.employment_id = leave_entitlement.employment_id AND current = 1 AND employment.trashed = 0')
//
//		->join('employee', 'employee.employee_id = employment.employee_id')
//
//		->join('position_management', 'employment.employment_id = position_management.employement_id')
//
//		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
//
//		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id')
//
//		->join('ml_status', 'leave_entitlement.status = ml_status.status_id')
//
//		->where('leave_entitlement.status = 1 AND leave_entitlement.trashed = 0')
//		->group_by('leave_entitlement.leave_entitlement_id');
//		$this->datatables->edit_column('leave_entitlement.leave_entitlement_id',
//				'<a href="dashboard_site/action_entitlement_approval/$1/$2"><button class= "btn green">Action</button></a>',
//				'leave_entitlement.leave_entitlement_id,employee.employee_id');
//
//			if($approval = $this->input->post('status_title'))
//			{
//				$this->datatables->where('ml_status.status_id', $approval);
//			}
//		return $this->datatables->generate();
//
//
//	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	public function get_entitlement_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_leave_type.leave_type,ml_status.status_title,leave_entitlement.leave_entitlement_id,leave_entitlement.no_of_leaves_allocated,leave_entitlement.note,employee.employee_id")
		
		->from('leave_entitlement')

        ->join('employment', 'employment.employment_id = leave_entitlement.employment_id')

       ->join('employee', 'employee.employee_id = employment.employee_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id')
		
		->where($where);
			
		$query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	
	//////////////////////////function for transcation approval/////////////////////
	
	
	public function get_edit_goto_list($where)
	{
	$this->db->select('employee.full_name, ml_designations.designation_name, ml_department.department_name, resposible, current_contacts.official_email, current_contacts.office_phone')
	
	->from('goto_list')
	
	->join('employee', 'goto_list.employee_id = employee.employee_id')
	
	->join('current_contacts', 'current_contacts.employee_id = goto_list.employee_id')
	
	->join('ml_department', 'ml_department.department_id = goto_list.ml_department_id')
	
	
	
	->join('ml_designations', 'goto_list.ml_desigination_id = ml_designations.ml_designation_id')
	
	->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	
}


public function goto_list()
	{
	$this->datatables->select('employee.full_name, ml_designations.designation_name, ml_department.department_name, resposible, current_contacts.official_email, current_contacts.office_phone, goto_list.goto_list_id')
	
	->from('goto_list')
	
	->join('employee', 'goto_list.employee_id = employee.employee_id')
	
	->join('ml_department', 'ml_department.department_id = goto_list.ml_department_id')
	
	->join('current_contacts', 'current_contacts.employee_id = goto_list.employee_id')
	
	->join('ml_designations', 'goto_list.ml_desigination_id = ml_designations.ml_designation_id');
	
	$this->datatables->edit_column('goto_list.goto_list_id',
	'<a href="dashboard_site/edit_goto_list/$1"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;
	<a href="dashboard_site/delete_goto_list/$1"><span class="fa fa-trash-o"></span></a>',
	'goto_list.goto_list_id');
	
	if($approval = $this->input->post('full_name'))
			{
				$this->datatables->where('employee.employee_id', $approval);
			}
			if($exp = $this->input->post('department_name'))
			{
				$this->datatables->where('ml_department.department_id', $exp);
			}
	
	 return $this->datatables->generate();
	
	
}
	
			public function get_autocomplete_goto_list()
	{
		$name = $this->input->post('queryString');
		
		$this->db->select("full_name, employee_code, ml_designations.designation_name,employee.employee_id,ml_designations.ml_designation_id, ml_department.department_id,ml_department.department_name")
		->from('employee')
        ->join('employment','employee.employee_id = employment.employee_id')	
		->join('position_management', 'employment.employment_id = position_management.employement_id') 		
		->join('posting','employment.employment_id = posting.employement_id')
		->join('ml_designations','position_management.ml_designation_id = ml_designations.ml_designation_id')
		->join('ml_department','posting.ml_department_id = ml_department.department_id')
		->like('full_name',$name ,'after')
		->WHERE('employment.current = 1')
		 ->group_by('employee.employee_id');
		//$this->db->like('full_name', $n, 'after');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	
	public function staff_activity_schedule()
	{
	$this->datatables->select('employee.full_name,ml_projects.project_title,ml_department.department_name,ml_assign_task.task_name,start_date,completion_date,assign_job.status')
	
	->from('employee')

    ->join('employment', 'employee.employee_id = employment.employee_id','inner')

	->join('assign_job', 'assign_job.employment_id = employment.employment_id','inner')

	->join('posting', 'posting.employement_id = employment.employment_id','inner')
	
	->join('ml_assign_task', 'ml_assign_task.ml_assign_task_id = assign_job.ml_assign_task_id','inner')
	
	->join('ml_projects', 'ml_projects.project_id = assign_job.project_id','inner')
	
	->join('ml_department', 'ml_department.department_id = posting.ml_department_id','inner')
	
	->join('position_management', 'employment.employment_id = position_management.employement_id','inner')
	
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','inner')

    ->group_by('assign_job.assign_job_id');
	

	
			if($approval = $this->input->post('project_title'))
			{
				$this->datatables->where('ml_projects.project_id', $approval);
			}
			if($exp = $this->input->post('department_name'))
			{
				$this->datatables->where('ml_department.department_id', $exp);
			}
	 return $this->datatables->generate();
	
	
}
    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method
     */
/*	public function get_loan()
	{
	$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_payment_type.payment_type_title,loan_advances.amount,ml_status.status_title,loan_advances.loan_advance_id,employee.employee_id')
	
	->from('employee')

    ->join('employment', 'employment.employee_id = employee.employee_id AND employment.current = 1 AND employment.trashed= 0')
	
	->join('loan_advances', 'employment.employment_id = loan_advances.employment_id')

	->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
	
	->join('ml_status', 'loan_advances.status = ml_status.status_id')
	
	->join('ml_payment_type', 'loan_advances.payment_type = ml_payment_type.payment_type_id')
	
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
	
	->where('loan_advances.status = 1')
	->group_by('loan_advances.loan_advance_id');
	
	$this->datatables->edit_column('loan_advances.loan_advance_id',
			'<a href="dashboard_site/action_loan_approval/$1/$2"><button class= "btn green">Action</button></a>',
			'loan_advances.loan_advance_id,employee.employee_id');
			
		if($approval = $this->input->post('status_title'))
		{
			$this->datatables->where('ml_status.status_id', $approval);
		}
	return $this->datatables->generate();
	
	
	}*/
//////////////////////////////////////////////////////////////////////////////////

	public function get_loan_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,
		ml_designations.designation_name,ml_payment_type.payment_type_title,
		loan_advances.amount,ml_status.status_title,loan_advances.note,
		loan_advances.loan_advance_id,employee.employee_id,salary.base_salary")
		
		->from('employee')

        ->join('employment', 'employment.employee_id = employee.employee_id AND employment.current = 1 AND employment.trashed = 0')
		
		->join('loan_advances', 'employment.employment_id = loan_advances.employment_id')
		

		->join('salary', 'employment.employment_id = salary.employement_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_status', 'loan_advances.status = ml_status.status_id')
		
		->join('ml_payment_type', 'loan_advances.payment_type = ml_payment_type.payment_type_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where($where)
		->where('salary.status',2);

		$query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
/////////////////////////////////////////////////////////////////////////////

//Deprecated..
//localhost/projects/HR/dashboard_site/posting_approval
/*
	public function get_position()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,ml_status.status_title,position_management.position_id,employment.employment_id,employee.employee_id')
		
		->from('position_management')
		
		->join('employment', 'employment.employment_id = position_management.employement_id ')
		
		->join('employee', 'employee.employee_id = employment.employee_id')
		
		->join('ml_status', 'position_management.status = ml_status.status_id')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where('position_management.status = 1 AND position_management.current = 1 AND employment.current = 1 AND employment.trashed=0');
		
		$this->datatables->edit_column('position_management.position_id',
				'<a href="dashboard_site/action_posting_approval/$1/$2/$3"><button class= "btn green">Action</button></a>',
				'position_management.position_id,employment.employment_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
//////////////////////////////////////////////////////////////////////////////////

	public function get_position_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,position_management.job_specifications,ml_status.status_title,position_management.note,position_management.position_id,employment.employment_id")
		
		->from('position_management')
		
		->join('employment', 'employment.employment_id = position_management.employement_id')
		
		->join('employee', 'employee.employee_id = employment.employee_id')
		
		->join('ml_status', 'position_management.status = ml_status.status_id')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where($where);

		
		$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}

    public function get_current_position($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_pay_grade.pay_grade,position_management.job_specifications,ml_status.status_title,position_management.note,position_management.position_id,employment.employment_id")

		->from('position_management')

		->join('employment', 'employment.employment_id = position_management.employement_id','left')

		->join('employee', 'employee.employee_id = employment.employee_id','left')

		->join('ml_status', 'position_management.status = ml_status.status_id','left')

		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id','left')

		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','left')
        ->where($where)
		->where('position_management.current',1)
		->where('position_management.status',2);



		$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
		
	////////////////////////// Function For Phone Book ////////////////////////
	public function phone_book()
	{
		$this->datatables->select('full_name, designation_name, project_title, department_name, mob_num, official_email')
		
		->from('employee')
		
		->join('current_contacts','employee.employee_id=current_contacts.employee_id','INNER')
		
		->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'INNER')
	    
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'LEFT')
	    
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'LEFT')
		
		->join('posting', 'posting.employement_id = employment.employment_id', 'left')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'left')
		
		->join('assign_job','employment.employment_id = assign_job.employment_id','left')
		
		->join('ml_projects','ml_projects.project_id=assign_job.project_id','left')
		->group_by('employee.employee_id');
		
		return $this->datatables->generate();
	}
////////////////////function for skill inventory ///////////////////////////////
	public function skills_inventory()
	{
	$this->datatables->select('employee.full_name,ml_designations.designation_name,ml_department.department_name,ml_projects.project_title,ml_skill_type.skill_name,emp_skills.experience_in_years,employee.employee_id')
	
	->from('emp_skills')
	

	
	->join('employment', 'employment.employee_id = emp_skills.employment_id')

    ->join('employee', 'employee.employee_id = employment.employee_id')
	
	->join('posting', 'posting.employement_id = employment.employee_id')
	
	->join('assign_job', 'assign_job.employment_id = employment.employment_id')
	
	->join('ml_projects', 'ml_projects.project_id = assign_job.project_id')
	
	->join('ml_department', 'ml_department.department_id = posting.ml_department_id')
	
	->join('position_management', 'employment.employment_id = position_management.employement_id')
	
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
	
	->join('ml_skill_type', 'ml_skill_type.skill_type_id = emp_skills.ml_skill_type_id');
	
	$this->datatables->edit_column('employee.employee_id','<a href="dashboard_site/view_skills/$1"><span class="fa fa-eye"></span></a>','employee.employee_id');
	
			if($approval = $this->input->post('skill_name'))
			{
				$this->datatables->where('ml_skill_type.skill_type_id', $approval);
			}
			if($exp = $this->input->post('experience_in_years'))
			{
				$this->datatables->where('emp_skills.experience_in_years', $exp);
			}
	 return $this->datatables->generate();
	
	
}
//////////////////////////////////////////////////////////////////////////////////////////
public function view_skills($where)
	{
		$this->db->select('ml_skill_type.skill_name,emp_skills.experience_in_years')
		
		->from('employee')
			
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')
		
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
	
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		
		->where($where)
		->group_by('employee.employee_id');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}

	////////////////////////////function for Increment approval /////////////////////////
	public function increments_approvals()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,increment_amount,ml_increment_type.increment_type,ml_status.status_title,increments.increment_id,employee.employee_id')
		
		->from('increments')

        ->join('employment', 'increments.employment_id = employment.employment_id')

        ->join('employee', 'employee.employee_id = employment.employee_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id')
		
		->join('ml_status', 'increments.status = ml_status.status_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where('increments.status = 1 AND increments.trashed = 0 AND employment.current = 1')
		->group_by('increments.increment_id');
		
		//$this->datatables->edit_column('salary.base_salary','$1','Rs:salary.base_salary');
		
		$this->datatables->edit_column('increments.increment_id',
				'<a href="dashboard_site/action_increment_approvals/$1/$2"><button class= "btn green">Action</button></a>',
				'increments.increment_id,employee.employee_id');
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}

/////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////function for increment get ////////////////////////////

	public function get_increment_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,increment_amount,ml_increment_type.increment_type,ml_status.status_title,increments.note,increments.increment_id,employee.employee_id")
		
		->from('increments')

        ->join('employment', 'increments.employment_id = employment.employment_id')

        ->join('employee', 'employment.employee_id = employee.employee_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id')
		
		->join('ml_status', 'increments.status = ml_status.status_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where($where);
		
		//->group_by('employee.employee_id')	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
		public function get_increment_repeat($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,increment_amount,ml_increment_type.increment_type,ml_status.status_title,increments.increment_id,employee.employee_id")
		
		->from('increments')
		
		->join('employment', 'employment.employment_id = increments.employment_id')
		
		->join('employee', 'employment.employee_id = employee.employee_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id')
		
		->join('ml_status', 'increments.status = ml_status.status_id')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where($where);
		
		//->group_by('employee.employee_id')	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	public function get_increment_approvals_repeat($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,increment_amount,ml_increment_type.increment_type,ml_status.status_title,increments.increment_id,employee.employee_id")
		
		->from('increments')

        ->join('employment', 'employment.employment_id = increments.employment_id', 'inner')

		->join('employee', 'employment.employee_id = employee.employee_id')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id')
		
		->join('ml_status', 'increments.status = ml_status.status_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where);
		
		//->group_by('employee.employee_id')	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	
	
	////////////////////////////function for salary approval /////////////////////////
    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method
     */
/*	public function salary_approvals()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,salary.base_salary,ml_status.status_title,salary.salary_id')
		->from('salary')
		
		->join('employment', 'employment.employment_id = salary.employement_id')
		
		->join('ml_status', 'salary.status = ml_status.status_id')
		
		->join('employee', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where('salary.status = 1 AND employee.enrolled = 1 AND employment.current = 1');
		
		//$this->datatables->edit_column('salary.base_salary','$1','Rs:salary.base_salary');
		
		$this->datatables->edit_column('salary.salary_id',
				'<a href="dashboard_site/action_salary_approvals/$1"><button class= "btn green">Action</button></a>',
				'salary.salary_id');
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
	//////////////////////////////////////////////////////////////////////////////
	
	public function get_salary_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,salary.base_salary,ml_status.status_title,salary.note,salary.salary_id")
		->from('salary')
		
		->join('employment', 'employment.employment_id = salary.employement_id')
		
		->join('ml_status', 'salary.status = ml_status.status_id')
		
		->join('employee', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where);
			
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	
	
	
	//////////////////////////function for transcation approval/////////////////////
    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method
     */
/*	public function benefits_approvals()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
		ml_benefit_type.benefit_type_title,
		ml_status.status_title,benefits.emp_benefit_id,employee.employee_id')
		
		->from('employee')
        ->join('employment', 'employee.employee_id = employment.employee_id', 'left')

		->join('benefits', 'benefits.employment_id = employment.employment_id AND benefits.trashed = 0')

		->join('ml_status', 'benefits.status = ml_status.status_id')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id')
		
		//->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where('benefits.status = 1 AND employment.current = 1 AND employee.enrolled = 1')
		->group_by('benefits.emp_benefit_id');
		
		$this->datatables->edit_column('benefits.emp_benefit_id',
				'<a href="dashboard_site/action_benefits_approvals/$1/$2"><button class= "btn green">Action</button></a>',
				'benefits.emp_benefit_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
	
	
	//////////////////////////////////////////////////////////////////////////
	
	public function get_benefits_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,
		ml_benefit_type.benefit_type_title,ml_status.status_title,benefits.note,benefits.emp_benefit_id")
		
		->from('employee')
            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

		->join('benefits', 'benefits.employment_id = employment.employment_id')
		
		->join('ml_status', 'benefits.status = ml_status.status_id')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id')
		
		//->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id')
		

		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where);
		
		//->group_by('employee.employee_id')
			
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	
	
	public function get_benefits($where)
	{
		$this->db->select("*")
		
		->from('benefits')
		
		->join('ml_status', 'benefits.status = ml_status.status_id')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id')
		
		//->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id')
		
		->where($where);
		
		//->group_by('employee.employee_id')
			
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	
	
	//////////////////////////function for transcation approval/////////////////////
	
	public function allowances_approvals()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_allowance_type.allowance_type,allowance.allowance_amount,ml_status.status_title,allowance.allowance_id,employee.employee_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('allowance', 'allowance.employment_id = employment.employment_id AND allowance.trashed = 0')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id')
		
		->join('ml_status', 'allowance.status = ml_status.status_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where('allowance.status = 1 AND employment.current = 1 AND employee.enrolled = 1')
		->group_by('allowance.allowance_id');
		
		$this->datatables->edit_column('allowance.allowance_id',
				'<a href="dashboard_site/action_allowances_approvals/$1/$2"><button class= "btn green">Action</button></a>',
				'allowance.allowance_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	
	
	public function get_allowances_approvals($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,allowance_amount,ml_allowance_type.allowance_type,ml_status.status_title,allowance.note,allowance.allowance_id")
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

		->join('allowance', 'allowance.employment_id = employment.employment_id')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id')
		
		->join('ml_status', 'allowance.status = ml_status.status_id')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where)
		
		->group_by('allowance.allowance_id');	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	public function get_allowances_approvals_repeat($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,allowance_amount,ml_allowance_type.allowance_type,ml_status.status_title,allowance.allowance_id")
		
		->from('employee')

       ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('allowance', 'allowance.employment_id = employment.employment_id')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id')
		
		->join('ml_status', 'allowance.status = ml_status.status_id')

		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where)
		
		->group_by('allowance.allowance_id');	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	
	
	
	
	////////////////////////////end of function///////////////////////////////////

	///////////////////////salary review /////////////////////////
	public function month_salaries($where_emp)
	{
		$this->db->select('(transaction.transaction_date) AS trans_date,transaction.transaction_amount,
		salary_payment.arears,transaction.payment_mode,transaction.transaction_id,salary_payment.salary_payment_id')
			->from('employee')
			//->where('transaction.status',2)
			//->where('salary_payment.year',$year)
			->join('employment','employee.employee_id=employment.employee_id','LEFT')
			->join('salary','employment.employment_id=salary.employement_id','LEFT')
			->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
			->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
            ->where($where_emp);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}

	public function payroll_regded_trans($id,$month,$year,$salary_trans_date)
	{
		$this->db->select('deduction_processing.transaction_id,
		transaction.transaction_date,transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as total_ded_amount,
		(transaction.transaction_id) as ded_trans_id')
			->from('employee')
			->join('employment','employee.employee_id=employment.employee_id','LEFT')
			->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
			->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
			->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
			->join('transaction','transaction.transaction_id=deduction_processing.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',1)
            ->where('transaction.transaction_date',$salary_trans_date);

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}

	public function payroll_regallw_trans($id,$month,$year,$salary_trans_date)
	{
        $employment_id = get_employment_from_employeeID($id);
		$this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
		SUM(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as total_alw_recm,
		(transaction.transaction_id) as alw_trans_id,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_id  ELSE NULL END) as alw_ids')
			->from('employee')
			->join('employment','employee.employee_id=employment.employee_id','LEFT')
			->join('allowance','employment.employment_id=allowance.employment_id','LEFT')
			->join('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id','LEFT')
			->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
			->join('transaction','transaction.transaction_id=allowance_payment.transaction_id','LEFT')
            ->where('transaction.employment_id',$employment_id)
            ->where('transaction.transaction_type',2)
            ->where('transaction.transaction_date',$salary_trans_date);

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}

	public function payroll_regexp_trans($id,$month,$year,$salary_trans_date)
	{
        $employment_id = get_employment_from_employeeID($id);
		$this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as exp_recm,
		SUM(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as total_exp_recm,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.expense_claim_id  ELSE NULL END) as exp_ids,
		(transaction.transaction_id) as exp_trans_id')
			->from('employee')
			->join('employment','employee.employee_id=employment.employee_id','LEFT')
			->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
			->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
			->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
			->join('transaction','transaction.transaction_id=expense_disbursment.transaction_id','LEFT')
            ->where('transaction.employment_id',$employment_id)
            ->where('transaction.transaction_type',4)
            ->where('transaction.transaction_date',$salary_trans_date);

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}
	public function payroll_regadvance_trans($id,$month,$year,$salary_trans_date)
	{
        $employment_id = get_employment_from_employeeID($id);
		$this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id) THEN loan_advances.amount ELSE NULL END) as loan_recm,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id) THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
		SUM(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id) THEN loan_advances.amount ELSE NULL END) as total_advamount,
		(transaction.transaction_id) as adv_trans_id,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id) THEN loan_advances.loan_advance_id  ELSE NULL END) as adv_ids')
			->from('employee')
			->join('employment','employee.employee_id=employment.employee_id','LEFT')
			->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')
			->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
			->join('loan_advance_payback','loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id','LEFT')
			->join('transaction','transaction.transaction_id=loan_advance_payback.transaction_id','LEFT')
            ->where('transaction.employment_id',$employment_id)
            ->where('transaction.transaction_type',3)
            ->where('transaction.transaction_date',$salary_trans_date);

		$query=$this->db->get();
		if($query->num_rows() > 0)
		{return $query->row();}
	}

	public function payroll_reg_details($where,$month =NULL,$year =NULL,$salary_trans_date =NULL)
	{
		$this->db->select(
			"employee.employee_id,employee.employee_code,employee.full_name,
ml_designations.designation_name,ml_pay_grade.pay_grade,
ml_month.month,ml_year.year,salary.base_salary,
(CASE WHEN (transaction.transaction_type=5 AND transaction.transaction_date=$salary_trans_date) THEN transaction.transaction_amount ELSE 0 END) as salary_trans,
salary_payment.transaction_amount,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,
salary_payment.salary_payment_id,ST1.transaction_date,ST1.status,
(ST1.approval_date) as ap_date,ST1.transaction_date,ST1.transaction_id,
ST1.approval_date,(Select full_name from employee where employee_id=ST1.approved_by) as name,
ml_status.status_title,salary_payment.salary_id")
			->from('transaction')
            ->join('employment','transaction.employment_id=employment.employment_id')
			->join('employee','employment.employee_id=employee.employee_id')
			->join('salary','employment.employment_id=salary.employement_id')
			->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
			->join('transaction ST1','salary_payment.transaction_id=ST1.transaction_id','LEFT')
			->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
			->join('ml_status','ST1.status=ml_status.status_id','LEFT')
			->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
			->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
			->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
			->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
            ->where($where)
            ->where('transaction.transaction_date',$salary_trans_date);
		//->where('salary_payment.month',$month)
		//->where('salary_payment.year',$year)
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}
	}
	public function fail_review($review_data,$transaction_data,$transaction_id,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id,$exp_id,$adv_id)
	{
		$this->db->trans_start();
		$this->db->where('transaction_id',$transaction_id);
		$query=$this->db->update('transaction',$transaction_data);
		if(!empty($alw_trans_id))
		{
		$this->db->where('transaction_id',$alw_trans_id);
		$query=$this->db->update('transaction',$transaction_data);
		}
		if(!empty($ded_trans_id))
		{
			$this->db->where('transaction_id',$ded_trans_id);
			$query=$this->db->update('transaction',$transaction_data);
		}
		if(!empty($adv_trans_id))
		{
			$this->db->where('transaction_id',$adv_trans_id);
			$query=$this->db->update('transaction',$transaction_data);
			$this->db->where('transaction_id',$adv_trans_id);
			$query=$this->db->update('loan_advance_payback',array('review_status'=>1));
		}
		if(!empty($exp_trans_id))
		{
			$this->db->where('transaction_id',$exp_trans_id);
			$query=$this->db->update('transaction',$transaction_data);
			$this->db->where('transaction_id',$exp_trans_id);
			$query=$this->db->update('expense_disbursment',array('review_status'=>1));
		}
		$query=$this->db->insert('fail_reviews',$review_data);
		if($this->db->trans_status()=== FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();

	}

	public function salary_transaction_approval($salary_trans_id,$approval_data,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id)
	{
		$this->db->trans_start();
		$this->db->where('transaction_id',$salary_trans_id);
		$query=$this->db->update('transaction',$approval_data);
		if(!empty($alw_trans_id))
		{
			$this->db->where('transaction_id',$alw_trans_id);
			$query=$this->db->update('transaction',$approval_data);
		}
		if(!empty($ded_trans_id))
		{
			$this->db->where('transaction_id',$ded_trans_id);
			$query=$this->db->update('transaction',$approval_data);
		}
		if(!empty($adv_trans_id))
		{
			$this->db->where('transaction_id',$adv_trans_id);
			$query=$this->db->update('transaction',$approval_data);
		}
		if(!empty($exp_trans_id))
		{
			$this->db->where('transaction_id',$exp_trans_id);
			$query=$this->db->update('transaction',$approval_data);

		}
		if($this->db->trans_status()=== FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();

	}
	////////////////////End////////////////////////////////////////
    public function eos_transaction_approval($employment_id,$salary_tran_id,$approval_data,$alw_trans_id,$ded_transaction_id,$adv_trans_id,$exp_trans_id)
    {
        $this->db->trans_start();
        $this->db->where('transaction_id',$salary_tran_id);
        $query=$this->db->update('transaction',$approval_data);
        if(!empty($alw_trans_id))
        {
            $this->db->where('transaction_id',$alw_trans_id);
            $query=$this->db->update('transaction',$approval_data);
        }
        if(!empty($ded_transaction_id))
        {
            $this->db->where('transaction_id',$ded_transaction_id);
            $query=$this->db->update('transaction',$approval_data);
        }
        if(!empty($adv_trans_id))
        {
            $this->db->where('transaction_id',$adv_trans_id);
            $query=$this->db->update('transaction',$approval_data);
        }
        if(!empty($exp_trans_id))
        {
            $this->db->where('transaction_id',$exp_trans_id);
            $query=$this->db->update('transaction',$approval_data);

        }
        if(!empty($employment_id))
        {
            $data=array('settlement_payment_status'=>1);
            $this->db->where('employment_id',$employment_id);
            $query=$this->db->update('employment',$data);
            $status=array('status'=>2);
            $this->db->where('employment_id',$employment_id);
            $query=$this->db->update('eos',$status);


        }
        if($this->db->trans_status()=== FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
        return $query;
        $this->db->trans_complete();

    }
    ////////////////////End////////////////////////////////////////

// Function for EOS salary details
    public function eos_salary_details($where,$month =NULL,$year =NULL,$salary_trans_date =NULL)
    {
                $this->db->select(
                    "employee.employee_id,employee.employee_code,employee.full_name,
        ml_designations.designation_name,ml_pay_grade.pay_grade,
        ml_month.month,ml_year.year,salary.base_salary,
        (CASE WHEN (transaction.transaction_type=5 AND transaction.transaction_date=$salary_trans_date) THEN transaction.transaction_amount ELSE 0 END) as salary_trans,
        salary_payment.transaction_amount,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,
        salary_payment.salary_payment_id,ST1.transaction_date,ST1.status,
        (ST1.approval_date) as ap_date,ST1.transaction_date,ST1.transaction_id,
        ST1.approval_date,(Select full_name from employee where employee_id=ST1.approved_by) as name,
        ml_status.status_title,salary_payment.salary_id,contract.contract_start_date,(CASE WHEN(contract.extension = 1) THEN EXCT.e_end_date ELSE contract.contract_expiry_date END) AS contract_expiry_date,eos.eos_id")
            ->from('transaction')
            ->join('employment','transaction.employment_id=employment.employment_id')
            ->join('eos','employment.employment_id=eos.employment_id')
            ->join('contract','contract.employment_id=employment.employment_id')
           ->join('employee_contract_extensions EXCT','contract.contract_id=EXCT.contract_id','LEFT')
            ->join('employee','employment.employee_id=employee.employee_id')
            ->join('salary','employment.employment_id=salary.employement_id')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction ST1','salary_payment.transaction_id=ST1.transaction_id','LEFT')
            ->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
            ->join('ml_status','ST1.status=ml_status.status_id','LEFT')
            ->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
            ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
            ->where($where)
            ->where('transaction.transaction_date',$salary_trans_date);
        //->where('salary_payment.month',$month)
        //->where('salary_payment.year',$year)
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }
    }

    public function eos_ded_trans($id = NULL,$month = NULL,$year = NULL,$salary_trans_date = NULL)
    {
        $this->db->select('
		transaction.transaction_date,transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount  ELSE NULL END) as total_ded_amount,
		SUM(DISTINCT CASE WHEN(deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount ELSE 0 END) as refund_ded_amount,
		(transaction.transaction_id) as RefDed_trans_id')
            ->from('employee,transaction')
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
            ->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
            //->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
            //->join('transaction','transaction.transaction_id=deduction_processing.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',7);
            //->where('transaction.transaction_date',$salary_trans_date);

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }
    public function eos_refundDed_trans($id = NULL)
    {
        $this->db->select('

		group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN(deduction.status=2 AND deduction.ml_deduction_type_id = 3 OR deduction.ml_deduction_type_id = 5) THEN deduction.deduction_amount ELSE 0 END) as refund_ded_amount')
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
            ->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
           ->where('deduction.employment_id',$id);
             $query=$this->db->get();
             if($query->num_rows() > 0)
             {return $query->row();}
    }
// End
	/////////////////////////function for contract approvals/////////////
    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method
     */
/*	public function get_extend_contract_app()
	{
		$this->db->select('employee_code, full_name, designation_name, IFNULL(e_start_date,contract_start_date) as strt_date, IFNULL(e_end_date,contract_expiry_date) as end_date, status_title, contract.contract_id, employee.employee_id',false)
			->from('contract')
			->join('employment','contract.employment_id = employment.employment_id','inner')
			->join('employee','employment.employee_id = employee.employee_id','inner')
			->join('employee_contract_extensions','contract.contract_id = employee_contract_extensions.contract_id AND extension = 1','left')
			->join('position_management', 'employment.employment_id = position_management.employement_id','LEFT')
			->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','LEFT')
			->join('ml_status','contract.status = ml_status.status_id','inner')
			->group_by('contract.contract_id')
			->where('contract.status = 1 ');
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}*/
    public function g_extend_contract_app($where)
    {
        $this->db->select('employee_code, full_name, designation_name, IFNULL(e_start_date,contract_start_date) as strt_date, IFNULL(e_end_date,contract_expiry_date) as end_date, status_title, contract.contract_id, (CASE WHEN contract.extension = 0 THEN contract.comments ELSE employee_contract_extensions.comments END) as com, employee.employee_id',false)
            ->from('contract')
            ->join('employment','contract.employment_id = employment.employment_id','inner')
            ->join('employee','employment.employee_id = employee.employee_id','inner')
            ->join('employee_contract_extensions','contract.contract_id = employee_contract_extensions.contract_id AND extension = 1','left')
            ->join('position_management', 'employment.employment_id = position_management.employement_id','LEFT')
            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','LEFT')
            ->join('ml_status','contract.status = ml_status.status_id','inner')
            ->group_by('contract.employment_id')
            ->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }

    }

	/////////////////////////function for Applicants approvals/////////////

	public function get_selected_applicants_app()
	{
		$this->db->select('JAdv.referenceNo, App.fullName, JAdv.title,ApJob.dateAppliedOn,ShtInt.shtintr_id',false)
			->from('applicants App')
			->join('applicant_applied_jobs ApJob','ApJob.applicantID = App.applicantID','inner')
			->join('job_advertisement JAdv','JAdv.advertisementID = ApJob.jobAdvertisementID','inner')
			->join('shortlisted_interview ShtInt', 'ShtInt.applicantID = App.applicantID','LEFT')
			->where('ShtInt.selected_approval = 1 ');
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->result();
			}

	}
    public function g_selected_applicants_app($where = NULL)
    {
        $this->db->select('App.applicantID,JAdv.referenceNo, App.fullName, JAdv.title,ApJob.dateAppliedOn,ShtInt.shtintr_id,ShtInt.Score,ShtInt.feedback',false)
            ->from('applicants App')
            ->join('applicant_applied_jobs ApJob','ApJob.applicantID = App.applicantID','inner')
            ->join('job_advertisement JAdv','JAdv.advertisementID = ApJob.jobAdvertisementID','inner')
            ->join('shortlisted_interview ShtInt', 'ShtInt.applicantID = App.applicantID','LEFT')
            ->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }

    }

	
	//////////////////////////function for transcation approval/////////////////////
	
	public function get_transcation()
	{
		$this->db->select('(transaction.transaction_id) as trans_id,employee.employee_code,
		employee.full_name,ml_designations.designation_name,ml_transaction_types.transaction_type,
		transaction.transaction_amount,ml_status.status_title,transaction.transaction_id,employment.employment_id,employee.employee_id,
		(transaction.transaction_type) as id,salary_payment.month AS salary_month,salary_payment.year,salary_payment.salary_payment_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id  AND employment.trashed = 0','INNER')

        ->join('transaction', 'employment.employment_id  = transaction.employment_id','INNER')

        ->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id','LEFT')

		->join('position_management', 'employment.employment_id = position_management.employement_id','LEFT')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','LEFT')
		
		->join('ml_transaction_types', 'transaction.transaction_type = ml_transaction_types.ml_transaction_type_id','LEFT')
		
		->join('ml_status', 'transaction.status = ml_status.status_id','LEFT')
		
		->where('transaction.status = 1 AND employment.current = 1')
            ->order_by('transaction.transaction_id','desc')
		->group_by('transaction.transaction_id');
		/*$this->datatables->edit_column('transaction.transaction_id',
				'<a href="dashboard_site/action_transcation_approval/$1/$2/$3"><button class= "btn green">Action</button></a>',
				'transaction.transaction_id,employee.employee_id,id');*/
				
			if($approval = $this->input->post('status_title'))
			{
				$this->db->where('ml_status.status_id', $approval);
			}
		//return $this->datatables->generate();
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
    /**
     * @param $where
     * @return mixed
     * @Method Deprecated Model Method for EOS Approval
     */
/*    public function get_eos_transcation()
    {
        $this->db->select('(transaction.transaction_id) as trans_id,employee.employee_code,
		employee.full_name,ml_designations.designation_name,ml_transaction_types.transaction_type,
		transaction.transaction_amount,ml_status.status_title,transaction.transaction_id,employment.employment_id,employee.employee_id,
		(transaction.transaction_type) as id,salary_payment.month as salary_month,salary_payment.year as salary_year,eos.eos_id')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id  AND employment.trashed = 0','INNER')

            ->join('eos', 'eos.employment_id = employment.employment_id','INNER')

            ->join('transaction', 'employment.employment_id  = transaction.employment_id','INNER')

            ->join('salary_payment','salary_payment.transaction_id=transaction.transaction_id','left')

            ->join('position_management', 'employment.employment_id = position_management.employement_id','LEFT')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','LEFT')

            ->join('ml_transaction_types', 'transaction.transaction_type = ml_transaction_types.ml_transaction_type_id','LEFT')

            ->join('ml_status', 'transaction.status = ml_status.status_id','LEFT')

            ->where('transaction.status = 1 AND employment.current = 0')
            ->group_by('transaction.transaction_id');
      if($approval = $this->input->post('status_title'))
        {
            $this->db->where('ml_status.status_id', $approval);
        }
        //return $this->datatables->generate();
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }


    }*/
	////////////////////////////end of function///////////////////////////////////
	
public function get_transcation_approval($where)
{
	$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,ml_transaction_types.transaction_type,transaction_amount,ml_status.status_title,transaction.calender_month,transaction.note,transaction.transaction_id,employee.employee_id")
	->from('employee')
    ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
	->join('transaction', 'transaction.employment_id = employment.employment_id')
	->join('ml_transaction_types', 'transaction.transaction_type = ml_transaction_types.ml_transaction_type_id')
	->join('ml_status', 'transaction.status = ml_status.status_id')

	->join('position_management', 'employment.employment_id = position_management.employement_id')
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
	->where($where);
	//->group_by('employee.employee_id')	
	 $query = $this->db->get();
    	if($query->num_rows() > 0)
		{
         	return $query->row();
         }
}


	
	
	public function get_transcation_history($where)
	{
		$this->db->select("*")
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employee.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
		
		->join('transaction', 'transaction.employment_id= employment.employment_id')
		
		->join('ml_transaction_types', 'transaction.transaction_type = ml_transaction_types.ml_transaction_type_id')
		
		->join('salary', 'employment.employment_id = salary.employement_id')
		
		->where($where);
		
		//->group_by('employee.employee_id');	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	
		
	public function get_transcation_history_repeat($where)
	{
		$this->db->select("*")
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employee.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
		
		->join('transaction', 'transaction.employment_id = employment.employment_id')
		
		->join('ml_transaction_types', 'transaction.transaction_type = ml_transaction_types.ml_transaction_type_id')
		

		
		->join('salary', 'employment.employment_id = salary.employement_id')
		
		->join('ml_status', 'transaction.status = ml_status.status_id')
		
		->where($where);
		
		//->group_by('employee.employee_id');	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	///////////////////////function for leave approvals /////////////////////////////

    /**
     * @param $where
     * @return mixed
     * @method deprecated Model Method
     */
/*	public function leave_approval()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,leave_type,date_format(leave_application.from_date,"%d-%m-%Y"),date_format(leave_application.to_date,"%d-%m-%Y"),leave_application.total_days,ml_status.status_title,leave_approval.leave_approval_id,leave_application.application_id,employee.employee_id',false)

		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')

		->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','INNER')

		->join('leave_application','leave_approval.leave_application_id = leave_application.application_id','INNER')

		->join('ml_leave_type','ml_leave_type.ml_leave_type_id = leave_application.ml_leave_type_id','LEFT')

		->join('ml_status', 'leave_approval.status = ml_status.status_id','LEFT')
		
		//->join('employment', 'employee.employee_id = employment.employee_id','LEFT')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id','LEFT')

		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id','LEFT')
		
		->where('leave_approval.status = 1 AND employment.current = 1 AND employee.trashed = 0 AND employee.enrolled = 1')
		->group_by('leave_application.application_id');
		$this->datatables->edit_column('leave_approval.leave_approval_id',
				'<a href="dashboard_site/action_leave_approval/$1/$2"><button class= "btn green">Action</button></a>',
				'leave_approval.leave_approval_id,employee.employee_id');
				
			if($approval = $this->input->post('status_title'))
			{
				$this->datatables->where('ml_status.status_id', $approval);
			}
		return $this->datatables->generate();
		
		
	}*/
	/////////////////END OF FFUNCTION ///////////////////////////////////////////////
	
	public function get_leave_approval($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,no_of_leaves_allocated,ml_leave_type.leave_type,no_of_paid_days,no_of_un_paid_days,ml_status.status_title,leave_approval.remarks,leave_approval.leave_approval_id")
		
		->from('employee')
        ->join('employment', 'employment.employee_id = employee.employee_id')
		->join('leave_approval', 'leave_approval.employment_id = employment.employment_id')
		->join('leave_entitlement', 'leave_entitlement.employment_id = employment.employment_id')
		->join('position_management', 'position_management.employement_id = employment.employment_id')
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id')
		->join('ml_status', 'leave_approval.status = ml_status.status_id')
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id')
		
		->where($where);
		
		//->group_by('employee.employee_id')	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->row();
			}
	}
	public function get_leave_approval_repeat($where)
	{
		$this->db->select("employee.employee_code,employee.full_name,ml_designations.designation_name,leave_approval.approval_date,approved_from,no_of_paid_days,no_of_un_paid_days,approved_to,ml_status.status_title,leave_approval.leave_approval_id")
		
		->from('employee')
		
		->join('leave_approval', 'leave_approval.employment_id = employment.employment_id')
		
		->join('ml_status', 'leave_approval.status = ml_status.status_id')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where);
		
		//->group_by('employee.employee_id')	
		
		 $query = $this->db->get();
		 
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	
	
	
	///////////////////function for trashed table ///////////////////////////////////
	public function all_trashed()
	{
		$this->datatables->select('table_id_name,record_id,trash_date,table_name,employee.full_name,trash_store_id,record_id')
		
		//->unset('record_id')
		
		->from('trash_store')

		->join('employee', 'trash_store.trashed_by = employee.employee_id');
	
	
			$this->datatables->edit_column('trash_store_id',
				'<a href="dashboard_site/recover_trash/$1/$2">Recover From Trash
				&nbsp;&nbsp;|&nbsp;<a href="dashboard_site/view/$1"><span class="fa fa-eye"></span>&nbsp;&nbsp;|&nbsp;<a href="dashboard_site/delete_parmanent/$1"><span class="fa fa-trash-o"></span></a>',
				'trash_store_id,record_id');
				if($exp = $this->input->post('record_id'))
			{
				$this->datatables->where('trash_store.record_id', $exp);
			}
		
	 	return $this->datatables->generate();
	}
	
	
	//********************************* ************ *****************************************//
	//================================= CONFIGRATION =========================================//
	///******************************** ************ ****************************************//

	////////////////////////FUNCTION FOR NOTIFICATION /////////////////////////////////////////
	public function configration()
	{
		$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
		ml_approval_types.approval_types,ml_approval_authorities.enabled,ml_approval_authorities.approval_auth_id,
		employee.employee_id')
		
		->from('employee')
		
		->join('ml_approval_authorities', 'ml_approval_authorities.employee_id = employee.employee_id')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_approval_types', 'ml_approval_types.ml_approval_types_id = ml_approval_authorities.ml_approval_type_id')
		
		->where('ml_approval_authorities.trashed = 0');
		
		//->group_by('employee.employee_id')
		$this->datatables->edit_column('ml_approval_authorities.enabled','$1','check_status(ml_approval_authorities.enabled)');
		
		$this->datatables->edit_column('ml_approval_authorities.approval_auth_id',
		'<a href="config_site/edit_authrity/$1"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="config_site/delete_authrity/$1"><span class="fa fa-trash-o"></span></a>',
		'ml_approval_authorities.approval_auth_id');
		
		if($approval = $this->input->post('approval_types'))
		{
			$this->datatables->where('ml_approval_types.ml_approval_types_id', $approval);
		}
		
		if($Designations = $this->input->post('designation_name'))
		{
			$this->datatables->where('ml_designations.ml_designation_id', $Designations);
		}		
		return $this->datatables->generate();
	}
	
	public function edit_configration($where)
	{
		$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_designations.ml_designation_id,ml_approval_types.approval_types,ml_approval_types.ml_approval_types_id,ml_approval_authorities.enabled,ml_approval_authorities.approval_auth_id,employee.employee_id')
		
		->from('employee')
		
		->join('ml_approval_authorities', 'ml_approval_authorities.employee_id = employee.employee_id')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_approval_types', 'ml_approval_types.ml_approval_types_id = ml_approval_authorities.ml_approval_type_id')
		
		->where($where);
		//->group_by('employee.employee_id')	
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
 /////////////////////////////////////////// Function for Auto Complete Time Sheet ////////////////////////////////
	 /// Auto complete for hourly time sheet /////
	public function get_autocomplete_approval_authority()
	{
		$name = $this->input->post('queryString');$query = $this->db->query("SELECT full_name, employee_code, ml_designations.designation_name,employee.employee_id
		FROM employee
        INNER JOIN employment ON employee.employee_id=employment.employment_id	
		INNER JOIN position_management ON employment.employment_id=position_management.employement_id 		
		INNER JOIN ml_designations ON position_management.position_id=ml_designations.ml_designation_id
		WHERE full_name like '$name%'");
		//$this->db->like('full_name', $n, 'after');
	
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END	
	public function config_new_record($tbl,$data,$enbl)
	{
		$this->db->insert();
	}
//********************************* ***** ********* *****************************************//
//================================= USER MANAGEMENT =========================================//
///******************************** ***** ********* ****************************************//	

/////////////////////////////////////////FUNCTION FOR VIEW GROUP PRIVILIGIES //////////////////////
	public function view_group_priviligies()
	{
		$this->db->select('user_groups_privileges.user_group_id,user_groups_privileges.privilege_id,user_groups_privileges.ml_module_list_id,user_groups_privileges.trashed,ml_user_groups.user_group_title,privileges.view_only,privileges.modify,privileges.create_new_record,privileges.print,ml_module_list.module_name,user_groups_privileges.user_group_privilege_id')
		
		->from('user_groups_privileges')
		
		->join('ml_user_groups','ml_user_groups.user_group_id=user_groups_privileges.user_group_id','inner')
		
		->join('ml_module_list','ml_module_list.ml_module_id=user_groups_privileges.ml_module_list_id','inner')
		
		->join('privileges','privileges.user_privilege_id=user_groups_privileges.privilege_id','inner');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
/////////////////////////END OF FUNCTION /////////////////////////////////////////////////////
////////////////////FUNCTION FOR GROUP JOIN BY WHERE /////////////////////////////////////////
	public function group_information($where)
	{
		$this->db->select('*')
		
		->from('ml_user_groups')
		
		->join('user_account','user_account.user_group=ml_user_groups.user_group_id','inner')
		
		->join('employee','user_account.employee_id=employee.employee_id','inner')
		
		//->join('user_groups_privileges','user_groups_privileges.user_group_id=ml_user_groups.user_group_id','inner')
		
		//->join('privileges','user_privilege_id=user_groups_privileges.privilege_id','inner')
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
///////////////////////////END OF FUNCTION //////////////////////////////////////////////////////////////
/////////////////////////////FUNCTION FOR VIEW JOIN RECORDS ////////////////////////////////////////////
	public function view_record()
	{
		$this->db->select('employee_code, full_name, user_name, user_group_title, user_group_id')

		->from('employee')

		->join('user_account','user_account.employee_id=employee.employee_id','inner')

		->join('ml_user_groups','user_group_id=user_account.user_group','inner');

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
///////////////////////END OF FUNCTION ////////////////////////////////////////////////////////////////////////
///////////////////// FUNCTION FOR VIEW USER INFORMATION JOIN BY WHERE ///////////////////////////////////////
	public function user_infromation($where)
	{
		$this->db->select('employee_code,full_name,user_name,user_group_title,user_group_id,user_account.enabled,user_account.password,employee.employee_id')
		
		->from('employee')
		
		->join('user_account','user_account.employee_id=employee.employee_id','inner')
		
		->join('ml_user_groups','user_group_id=user_account.user_group','inner')
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	
	}
/////////////////////////////END OF FUNCTION ///////////////////////////////////////////////////////////////
/////////////////FUNCTION FOR VIEW USER INFORMATION JOIN FOR DATATABLES/////////////////////////////////////
	public function view_user_infromation()
	{
		$this->datatables->select('employee_code,full_name,user_name,user_group_title,user_account.enabled,user_account.user_account_id')
		
		->from('employee')
		//->join('employment','employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0','left')
		->join('user_account','user_account.employee_id=employee.employee_id','left')
		->join('ml_user_groups','user_group_id=user_account.user_group','left')
        ->where('employee.enrolled = 1 AND employee.trashed = 0');
		
		if ($user= $this->input->post('user_name'))	
		{
			$this->datatables->where('user_account.user_account_id',$user);
		}
		if ($group= $this->input->post('user_group_title'))
		{
			$this->datatables->where('user_account.user_group',$group);
		}
		$this->datatables->edit_column('user_account.enabled', '$1','check_status(user_account.enabled)', 'user_account.enabled');
		
		$this->datatables->edit_column('user_account.user_account_id',
		'<a href="user_site/edit_user_management/$1"><span class="fa fa-pencil"></span></a>','user_account.user_account_id');
		
		return $this->datatables->generate();
	}	

///////////////////////////END OF FUNCTION //////////////////////////////////////////////////////////
//********************************* ***** ********* *****************************************//
//================================= END USER MANAGEMENT =========================================//
///******************************** ***** ********* ****************************************//	
	public function drop_down() 
	{
  		$rows = $this->db->select('*')
		
		->from('ml_user_groups')
		
		->where('trashed',0)
		
		->get()
		
		->result();
		
		$drop_down = array('' => '-- Select User Group --');
		
		foreach($rows as $row)
		{
			$drop_down[$row->user_group_id] = $row->user_group_title;
		}
  		return $drop_down;
	}
	public function drop_down_module()
	{
		$rows = $this->db->select('*')

			->from('ml_module_list')

			->where('trashed',0)

			->get()

			->result();

		$drop_down = array('' => '-- Select Module --');

		foreach($rows as $row)
		{
			$drop_down[$row->ml_module_id] = $row->module_name;
		}
		return $drop_down;
	}
	public function drop_down2() 
	{
  		$rows = $this->db->select('*')
		
		->from('employee')
		
		->where('trashed',0)
		
		->get()
		
		->result();
		
		$drop_down = array('' => '-- Select Employee Code --');
		foreach($rows as $row)
		{
			$drop_down[$row->employee_id] = $row->employee_code;
		}
  		return $drop_down;
	}


	public function drop_down4() 
	{
		$rows = $this->db->select('*')
		
		->from('user_account')
		
		->where('trashed',0)
		
		->get()
		
		->result();
		
		$drop_down = array('' => '-- Select User Name --');
		
		foreach($rows as $row)
		{
			$drop_down[$row->user_account_id] = $row->user_name;
		}
  		return $drop_down;
	}
	
	public function drop_down3() 
	{
		$rows = $this->db->select('*')
		
		->from('ml_leave_type')
		
		->where('trashed',0)
		
		->get()
		
		->result();
		
		$drop_down = array('' => '-- Select Leave Type --');
		
		foreach($rows as $row)
		{
			$drop_down[$row->ml_leave_type_id] = $row->leave_type;
		}
  		return $drop_down;
	}

	public  function html_select_box($table,$key,$value) 
	{
  		$this->db->select('*')
		
		->where('trashed',0);
  		
		$query = $this->db->get($table);
  		
		if($query->num_rows() > 0) 
		{
			//$data[''] = '--Select Option--';
			foreach($query->result() as $row) 
			{
				$data[$row->$key] = $row->$value;
   	 		}
			return $data;
 		}
	}   
	
		
	public function year_same()
	{
		$this->db->select('*');
		$this->db->from('ml_year');
		$query=$this->db->get();
		if($query->num_rows()> 0)
		{return $query->result();}
	}
	public function month_same()
	{
		$this->db->select('*');
		$this->db->from('ml_month');
		$query=$this->db->get();
		if($query->num_rows()> 0)
		{return $query->result();}
	}
	
	public function assign_projects()
	{
		$this->db->select('*');
		$this->db->from('ml_projects');
		$query=$this->db->get();
		if($query->num_rows()> 0)
		{return $query->result();}
	}
	
	public function attendance_status_type()
	{
		$this->db->select('*');
		$this->db->from('ml_attendance_status_type');
		$query=$this->db->get();
		if($query->num_rows()> 0)
		{return $query->result();}
	}
	
		public function dropdown_ml_department()
	{
		$this->db->select('*');
		$this->db->from('ml_department');
		$query=$this->db->get();
		if($query->num_rows()> 0)
		{return $query->result();}
	}
     
        
	public  function html_selectbox($table,$option,$key,$value) 
	{
		$this->db->select('*')
		
		->where('trashed',0);
  		
		$query = $this->db->get($table);
  		
		if($query->num_rows() > 0) 
		{
			$data[''] = $option;
			foreach($query->result() as $row) 
			{
				$data[$row->$key] = $row->$value;
   	 		}
			return $data;
 		}
	}        

	public function get_dept_dropdown()
	{
		$query = $this->db->get('department');
		
		if($query->num_rows() > 0) 
		{
			return  $query;
 		}
   	}
	public function search_username($where)
	{
		return $query = $this->db->get_where('user_management', array('employee_name'=> $where))->result();
	}
    public function get_by_id($ID){
        $this->db->where($ID);
        $query=$this->db->get('ml_expense_type');
        return $query->result();
    }

       
	public function fetch_user($id)
	{
		$this->db->select('*')->from('user');
		
		$this->db->where('dept_id',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				$data[$row->user_id] = $row->user_name;
            }
			return $data;
		}else{
			return array();
        }
	}
        
	/// For Making Thumbnails
	function resize_image($sourcePath, $desPath, $width = '300', $height = '300')
    {
		$this->load->library('image_lib');
		
        $this->image_lib->clear();
		
        $config['image_library'] 	= 'gd2';
        $config['source_image'] 	= $sourcePath;
        $config['new_image'] 		= $desPath;
        $config['quality'] 			= '100%';
        $config['create_thumb'] 	= TRUE;
        $config['maintain_ratio'] 	= true;
        $config['thumb_marker'] 	= '';
        $config['width'] 			= $width;
        $config['height'] 			= $height;
		
        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize())
            return true;
        return false;
    } 
	public function do_upload($file_name) 
	{
		$config['upload_path'] = 'upload/';
		
		$config['allowed_types'] = 'jpg|png|gif';
		
		$config['max_size'] = '10000000000';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload($file_name))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
            
			return $data;
        }
	}         
         
	public function upload_pic($image) 
	{
		
		$config['upload_path'] = 'images/';
		
		$config['allowed_types'] = 'jpg|png|gif';
		
		$config['max_size'] = '10000000000';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload($image))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
			
			return $data;
        }
	}        

	public function get_row_by_id($table,$where)
	{
		$this->db->select('*');
		
		$this->db->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	}

	public function get_2nd_last_id_where($where,$table,$id)
	{
		$this->db->select($id);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($id,'desc');
		$this->db->limit(2,1);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

	}
        
	public function get_row_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	} 
	
	public function get_num_rows_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
        }
	}

	public function create_new_record($table,$data)
	{
		$query = $this->db->insert($table, $data);
		
		if($query)
		{
			return true;
        } else{
			return false;
        }
	}

	public function insert_data_get_last_id($table,$data)
	{
		$query = $this->db->insert($table, $data);
		
		return $this->db->insert_id();
	}

	public function get_last_id() 
	{
		return $this->db->insert_id();
	}
       
	public function delete_row_by_where($table,$where)
	{
		$this->db->where($where);
		
		$query = $this->db->delete($table);
		
		if($query)
		{
			return true;
        }
	}		
             
	public function update_record($table,$where,$data)
	{
		$this->db->where($where);
		
		$query = $this->db->update($table,$data);
		
		if($query)
		{
	        return true;
		}else{
			return false;
		}
	}
	public function update_trash($table,$where,$id)
	{
		$this->db->where($where);
        
		$query = $this->db->update($table,array('trashed' => 0));
	    
		if($query)
		{
	        return true;
		}else {
			return false;
		}
	}
	
	public function delete_trash($where)
	{
		$this->db->where('trash_store_id',$where);
        
		$query = $this->db->delete('trash_store');
	    
		if($query)
		{
	        return true;
		}else {
			return false;
		}
	}
	

	public  function get_all($table)
	{
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	public  function get_one($table)
	{
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

	public function get_all_by_where($table,$where)
	{
		$this->db->select('*');
	    
		$this->db->where($where);
	    
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	public  function count_by_where($table,$where)
	{
		$this->db->where($where);
		
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}

	public function employee_table_count($from_table,$join_table,$where_join,$where)
	{
		$this->db->select('*');
		$this->db->from($from_table)
		->join($join_table,$where_join);
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
        
	public function redirect_to($location = NULL)
	{
		if($location != '')
		{
			redirect($location); 
		}    
	}
        public function chk_employee_code($where = NULL)
		{
			$this->db->where($where);
           
		    $query = $this->db->get('employee');
			
			if($query->num_rows() > 0)
			{
				return true;
			}else{
				return false;
			}  
        }
	/// Function to Check CNIC Duplicate Insertion
	function check_nic_already_exists($where)
	{
		$this->db->where($where);
		
		$query = $this->db->get('employee');
		
		if($query->num_rows() > 0)
		{
			return true;
		}else{
			return false;
		}
	}
	/// ************************* ********* ******* ** ****** ***********************************///
	//// ======================== Functions Created By Nadeem =================================////
	/// ************************* ********* ******* ** ****** ***********************************///
	/// Dynamic Function For Drop Down With Select Option
	public  function dropdown_wd_option($table, $option, $key, $value) 
	{
		$this->db->select('*');
  		
		$query = $this->db->get($table);
  		
		if($query->num_rows() > 0) 
		{
			$data['0'] = $option;
			foreach($query->result() as $row) 
			{
					$data[$row->$key] = $row->$value;
			}
			return $data;
		}
	}
	/// Function For Selecting Employee Emergency Contact Details
	public function emergency_contact_detail($employee_id)
	{
		$this->db->select('cotact_person_name, relation_name, home_phone, mobile_num, work_phone, emergency_contact_id')
		
		->from('emergency_contacts')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Qualification Details
	public function qualification_detail($employee_id)
	{
		$this->db->select('qualification_id, employee_id, qualification_title, qualification, institute, year')
		
		->from('qualification')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Experience Details
	public function emp_experience_detail($employee_id)
	{
		$this->db->select('organization, designation_name, from_date, to_date, experience_id')
		
		->from('emp_experience')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_designations', 'emp_experience.job_title = ml_designations.ml_designation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Skills Details
	public function emp_skill_detail($employee_id)
	{
		$this->db->select('skill_name, experience_in_years, skill_record_id, comments')
		
		->from('emp_skills')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Training Details
	public function emp_training_detail($employee_id)
	{
		$this->db->select('training_name, date, description')
		
		->from('training')
		
		//->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Dependents Details
	public function emp_dependent_detail($employee_id)
	{
		$this->db->select('dependent_name, relation_name, date_of_birth, dependent_id')
		
		->from('dependents')
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Icrements Details
	public function entitle_increment_detail($employee_id)
	{
		$this->db->select('increment_amount, increment_type, (select full_name from employee where employee_id = increments.approved_by) as emp_name, approval_date, status_title, increment_id')
		
		->from('increments')
		
		->join('employee', 'increments.employee_id = employee.employee_id', 'inner')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'inner')
		
		->join('ml_status', 'increments.status = ml_status.status_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Allowance Details
	public function entitle_allowance_detail($employee_id)
	{
		$this->db->select('allowance_amount, allowance_type, (select full_name from employee where employee_id = allowance.approved_by) as emp_name, allowance.date_approved, status_title, allowance_id')
		
		->from('allowance')
		
		->join('employee', 'allowance.employee_id = employee.employee_id', 'inner')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'inner')
		
		->join('ml_status', 'allowance.status = ml_status.status_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Benefit Details
	public function entitle_benefit_detail($employee_id)
	{
		$this->db->select('date_effective, benefit_type_title, benefit_name,
		(select full_name from employee where employee_id = benefits.approved_by) as emp_name,
		benefits.date_approved,
		status_title, emp_benefit_id')
		
		->from('benefits')
		
		->join('employee', 'benefits.employee_id = employee.employee_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Leave Details
	public function entitle_leave_detail($employee_id)
	{
		$this->db->select('no_of_leaves, leave_type, full_name, (select full_name from employee where employee_id = leave_entitlement.approved_by) as emp_name, date_approved, status_title, leave_entitlement_id')
		
		->from('leave_entitlement')
		
		->join('employee', 'leave_entitlement.employee_id = employee.employee_id', 'inner')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sup_auto_search()
	{
		$this->db->select('employee_id, full_name');
		
		$this->db->from('employee');
		
		$this->db->like('full_name', $this->input->post('queryString'), 'after');
		
		$this->db->limit(5);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sub_auto_search()
	{
		$this->db->select('employee_id, full_name');
		
		$this->db->from('employee');
		
		$this->db->like('full_name', $this->input->post('queryString'), 'after');
		
		$this->db->limit(5);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For All Employee List
	public function view_all_employee()
	{
		$this->datatables->select('employee.thumbnail, employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		//->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner');
		//->group_by('employee.employee_id');
		
		if($job_title = $this->input->post('job_title'))
		{
			$this->datatables->where('contract.employment_category', $job_title);
		}
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		$this->datatables->edit_column('employee.thumbnail', '<img src="upload/Thumb_Nails/$1" style="width:40px; height:40px" />', 'employee.thumbnail');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/edit_employee_acct/$1"><span class="fa fa-pencil"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp; <a href="human_resource/view_employee_profile/$1"><span class="fa fa-eye"></span></a> &nbsp;&nbsp;| &nbsp; &nbsp; <span class="fa fa-print" onclick="print_report($1)" style="cursor:pointer"></span>',
		'employee.employee_id');
		
		return $this->datatables->generate();		
	}
	/// Function to View Complete Profile of Employee
	public function view_complete_profile($where)
	{
		$this->db->select('employee.thumbnail, employee.full_name, employee.employee_code, employee.father_name, employee.CNIC, ml_marital_status.marital_status_title,employee.date_of_birth, ml_nationality.nationality, ml_religion.religion_title, current_contacts.address, current_contacts.home_phone, current_contacts.mob_num,current_contacts.email_address, current_contacts.office_phone, current_contacts.official_email,	permanant_contacts.address, permanant_contacts.phone, ml_city.city_name,ml_district.district_name, ml_province.province_name, emergency_contacts.cotact_person_name, emergency_contacts.home_phone, emergency_contacts.mobile_num, ml_relation.relation_name,qualification, institute, major_specialization, gpa_score, year, ml_designations.designation_name, organization, ml_employment_type.employment_type, emp_experience.from_date,emp_experience.to_date, ml_skill_type.skill_name, emp_skills.experience_in_years, position_management.job_specifications, ml_job_category.job_category_name, ml_department.department_name,ml_posting_location_list.posting_location, ml_shift.shift_name, contract_expiry_date, dependents.dependent_name, ml_relation.relation_name, dependents.date_of_birth,ml_pay_grade.pay_grade, ml_pay_frequency.pay_frequency, currency_name, ml_payrate.payrate, increment_amount, increment_type, ml_increment_type.increment_type, ml_benefit_type.benefit_type_title,ml_benefits_list.benefit_name, allowance_amount, ml_allowance_type.allowance_type, no_of_leaves, ml_leave_type.leave_type, (select employee.full_name from employee where employee_id = reporting_heirarchy.employeed_id) as emp_name, ml_reporting_options.reporting_option')
		
		->from('employee')
			
		->join('allowance', 'employee.employee_id = allowance.employee_id', 'inner')
			
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('current_contacts', 'employee.employee_id = current_contacts.employee_id', 'inner')
		
		->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'inner')
		
		->join('employment', 'employee.employee_id = employment.employment_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')
		
		->join('increments', 'employee.employee_id = increments.employee_id', 'inner')
		
		->join('qualification', 'employee.employee_id = qualification.employee_id', 'inner')
		
		->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'inner')	
		
		//->join('training', 'employee.employee_id = training.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('reporting_heirarchy', 'employee.employee_id = reporting_heirarchy.employeed_id', 'inner')		
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('salary', 'employment.employment_id = salary.employement_id', 'inner')
		
		->join('dependents', 'employee.employee_id = dependents.employee_id', 'inner')
		
		//->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_marital_status', 'employee.marital_status = ml_marital_status.marital_status_id', 'inner')
		
		->join('ml_nationality', 'employee.nationality = ml_nationality.nationality_id', 'inner')
		
		->join('ml_religion', 'employee.religion = ml_religion.religion_id', 'inner')		
		
		->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'inner')
		
		->join('ml_district', 'permanant_contacts.district = ml_district.district_id', 'inner')
		
		->join('ml_province', 'permanant_contacts.province = ml_province.province_id', 'inner')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->join('ml_reporting_options', 'reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id', 'inner')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id', 'inner')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'inner')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'inner')		
		
		->join('ml_currency', 'salary.ml_currency = ml_currency.ml_currency_id', 'inner')
		
		->join('ml_payrate', 'salary.ml_pay_rate_id = ml_payrate.ml_payrate_id', 'inner')
		
		->join('ml_pay_frequency', 'salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'inner')
		
		->join('ml_pay_grade', 'salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id', 'inner')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		
	}
	/// Function For Human Resource Task Management
	public function hr_task_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employment.employment_id, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		//->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($title = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $title);
		}
		if($task = $this->input->post('task'))
		{
			$this->datatables->where('assign_job.ml_assign_task_id', $task);
		}
		
		$this->datatables->edit_column('employment.employment_id',
		'<a href="human_resource/view_task_history/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_job/$1"><button class="btn green">Assign New Task</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	/// Function For Pagination Counting Rows
	public function task_history_count($where)
	{
		$this->db->select('*');
		$this->db->from('assign_job');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/// Function For Viewing Task Previous History
	public function view_task_history($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, task_description, task_name, project_title, assign_job.start_date, assign_job.completion_date, employee.employee_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')

		->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//->group_by('assign_job.assign_job_id');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}		
	}
	/// Function For Human Resource Task Management
	/*public function hr_task_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_assign_task.task_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($title = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $title);
		}
		if($task = $this->input->post('task'))
		{
			$this->datatables->where('assign_job.ml_assign_task_id', $task);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_job/$1"><button class="btn green">Assign New Task</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}*/
	/// Function For Assign Job And Performance Evaluation Details
	public function assign_job_perf_evalue($where)
	{
		$this->db->select('start_date, completion_date, completion_date, task_name, project_title, GROUP_CONCAT(kpi) AS kpi')
		
		->from('assign_job')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		
		->where($where);
		
		$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Assigned Job History
	public function assign_job_history($where)
	{
		$this->db->select('employee.full_name, employee.employee_code, ml_designations.designation_name, ml_assign_task.task_name, ml_projects.project_title')
		
		->from('employee')

		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

        ->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	public function assign_job_history_wd($where)
	{
		$this->db->select('task_name, project_title')
		
		->from('assign_job')
		
		//->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Performance Evaluation
	public function view_performance_evalution()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, kpi, start_date, completion_date, (select avg(percent_achieved) as percent from performance_evaluation where job_assignment_id = assign_job.assign_job_id) as percent_achieved, employee.employee_id, performance_evaluation.job_assignment_id')
		
		->from('employee')
		
		->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'left outer')
		
		//->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner');
		
		$this->db->group_by('performance_evaluation.job_assignment_id');
		
		if($kpi = $this->input->post('kpi'))
		{
			$this->datatables->where('performance_evaluation.ml_kpi_type_id', $kpi);
		}
		
		$this->datatables->edit_column('percent_achieved',
		'<div class="meter orange">
		<span style="width: $1%"></span>
		</div>',
		'percent_achieved');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/add_performance_evalution/$1/$2"><button class="btn green">Evaluate</button></a>',
		'employee.employee_id, performance_evaluation.job_assignment_id');
		
		return $this->datatables->generate();
	}
	/// Function To View Key Performance Indicators History Wdout Foreach
	public function view_kpi_history_pe($employee_id = NULL)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->row();
	}
    // Function for kpi view master list
    function get_kpi_project()
    {
        $this->db->select('*')
        ->from('ml_kpi')
        ->join('ml_projects','ml_projects.project_id=ml_kpi.project_id','inner')
        ->where('ml_kpi.trashed',0)
        ->where('ml_projects.trashed',0);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
           return  $query->result();
        }


    }
    //End
	/// Function To View Key Performance Indicators History Wd Foreach
	public function view_kpi_history_pe_wd($employee_id = NULL)
	{
		$this->db->select('*')
		
		->from('assign_job')
		
		->join('employee', 'assign_job.employee_id = employee.employee_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	/// Function For Benefits Management
	public function view_benefit_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, benefit_type_title, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
		
		->group_by('benefits.emp_benefit_id');
		
		if($designation = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $designation);
		}
		if($benefit = $this->input->post('benefit'))
		{
			$this->datatables->where('benefits.benefit_type_id', $benefit);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/add_benefits/$1"><button class="btn green">Add Benefits</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	/// Function For Add Benefit to View Added Benefit 
	public function view_benefit($employee_id = NULL)
	{
		$this->db->select('(select full_name from employee where employee_id = benefits.approved_by) as emp_name, date_approved, benefit_type_title, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		//->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
		
		->where($employee_id)
		
		->group_by('benefits.emp_benefit_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	public function view_assignd_benefit($employee_id = NULL)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, benefit_type_title, (select full_name from employee where employee_id = "approved_by") as name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->row();
	}
	public function view_assignd_benefit_wd($employee_id = NULL)
	{
		$this->db->select('benefit_type_title, (select full_name from employee where employee.employee_id = benefits.approved_by) as name, employee.employee_id')
		
		->from('employee')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	/// Function For Add Benefit to View Dependents
	public function view_dependent($employee_id = NULL)
	{
		$this->db->select('relation_name, dependents.dependent_name, dependents.date_of_birth, benefit_type_title, insurance_cover, benefits.date_rec_created, employee.employee_id, dependents.dependent_id')
		
		->from('employee')
				
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
				
		->join('dependents', 'employee.employee_id = dependents.employee_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')		
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id)
		
		->group_by('dependents.dependent_id');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{					
			return $query->result();
		}
	}
	/// Function For Employee Transfer View
	public function view_emp_transfer()
	{		
		$this->datatables->select('employee_code, full_name, posting_location, designation_name, department_name, (select full_name from employee where employee.employee_id = posting.approved_by) as emp_name, employee.employee_id')
		
		->from('employee')
				
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
				
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($dept = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $dept);
		}
		 
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		if($loc = $this->input->post('location'))
		{
			$this->datatables->where('posting.location', $loc);
		}
		
		$this->datatables->edit_column('employee.employee_id', 
		'<a href="human_resource/transfer/$1"><button class="btn green">Transfer</button></a>
		 | &nbsp;&nbsp; <a href="human_resource/transfer_history/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		//$this->datatables->edit_column('position_management.approved_by','$1','approved(position_management.approved_by)');
                
		return $this->datatables->generate();
	}
	/// Function For Employee Transfer
	public function view_transfer_detail($where)
	{		
		$this->db->select('full_name, posting_location, city_name, department_name, poting_start_date, posting_end_date, shift_name, branch_name, employment_id')
		
		->from('employment')
				
		->join('employee', 'employment.employee_id = employee.employee_id', 'inner')
				
		//->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
				
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		//->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_city', 'posting.ml_city_id = ml_city.city_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_branch', 'posting.ml_branch_id = ml_branch.branch_id', 'inner')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		
		->where($where);
		
		$query = $this->db->get();
		   
		return $query->result();
	}
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Nadeem =============================///
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	
//============================= work by hamid ==================///
	public function view_postion_management()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, position_management.job_specifications, status_title, employee.employee_id')
		
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($designation = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $designation);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" value="Position" class="btn green"></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/view_position/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		
		return $this->datatables->generate();
	}	
	
	public function view_position_record($where)
	{
		$this->db->select('*')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		->where($where)
		->group_by('employee.employee_id');	
		$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->result();
	}
	}
	// Function For Manage Position History
	public function emp_mp_history($where)
	{
		$this->db->select('full_name, designation_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where)
		;
		//->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->row();
	}
	public function emp_mp_history_wd($where)
	{
		$this->db->select('ml_pay_grade.pay_grade, job_specifications, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id =ml_pay_grade.ml_pay_grade_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where)
		;
		//->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	public function view_postion()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		//->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" class="btn green"></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	public function manage_postion($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_pay_grade.pay_grade, job_specifications, position_management.date_effective_from, ml_status.status_title,')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_pay_grade', 'ml_pay_grade.ml_pay_grade_id = position_management.ml_pay_grade_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->where($where)
		
		->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->result();
				 
	}
public function retirement()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_posting_reason_list.posting_reason, posting.enabled, employee.employee_id, posting.posting_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		->where('posting.enabled = 1');
		
		//->group_by('employee.employee_id')
		
		$this->datatables->edit_column('posting.enabled',
		'$1',
		'hr_retire(posting.enabled)');
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/terminate/$1/$2"><input type="submit" name="Discharge" value="Discharge" class="btn green" /></a>&nbsp; | &nbsp;<a href="human_resource/view_retire/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id,posting.posting_id');
		
		return $this->datatables->generate();
	}
	
	public function view_retire_record($where)
	{
		$this->db->select('*')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('seperation_management', 'employee.employee_id = seperation_management.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		->join('ml_separation_type', 'seperation_management.ml_seperation_type = ml_separation_type.separation_type_id', 'inner')
		
		->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		->where($where)
		->group_by('employee.employee_id');	
		$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->result();
	}
	}
	public function terminate_employees()
	{
		$this->db->select('employee.full_name,contract.comments,contract.contract_expiry_date')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner');
		
		$query = $this->db->get();
		
		return $query->result();
	}	
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Hamid Ali ===============================////
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///============================== Function for Leave Attendance Izhar ali khan ==========================/// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public function leave_application()
   {
	 $this->db->select('*')
	 ->from('employee')
      ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
	 ->join('leave_approval','employment.employment_id=leave_approval.employment_id','inner')
	 ->join('attendence','employee.employee_id=attendence.employee_id','inner')
	 ->join('ml_attendance_status_type','attendence.attendance_status_type=ml_attendance_status_type.attendance_status_type_id','inner')
     ->where('leave_approval.status',2); 
	 $query = $this->db->get();
			if($query->num_rows() > 0)
			{
			return $query->result();
			}
   }
   
   public function holiday()
   {
	 $this->db->select('*') 
	 ->from('ml_holiday')
     ->join('attendence','attendence.holiday = ml_holiday.ml_holiday_id','inner');
	 $query = $this->db->get();
			if($query->num_rows() > 0)
			{
			return $query->result();
			}
   }
     
	public function work_week($where)
   {
	 $this->db->select('*') 
	 ->from('work_week')
     ->where($where); 
	 $query = $this->db->get();
			if($query->num_rows() > 0)
			{
			return $query->result();
			}
   }
   	
	/*public function leave_type($where)
   {
	 $this->db->select('*') 
	 ->from('ml_leave_type')
     ->where($where); 
	 $query = $this->db->get();
			if($query->num_rows() > 0)
			{
			return $query->result();
			}
   }*/


public function leave_attendance_count()
{
	$this->db->select('employee_code, full_name, ml_leave_type.leave_type,total_days,from_date,to_date,ml_status.status_title,application_id')
	->from('employee')
   ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
	->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','inner')
    ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')
	->join('ml_leave_type','leave_application.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
	->join('ml_status','leave_approval.status=ml_status.status_id','inner')
	//->group_by('leave_approval.leave_approval_id')
	//->where('leave_approval.status = 2')
	->order_by('employee.employee_id');
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->result();
			}
	
}



public function leave_attendance($limit = NULL,$start = NULL,$full_name = NULL, $leave_type = NULL)
{
	$this->db->select('employee_code, full_name, ml_leave_type.leave_type,total_days,from_date,to_date,ml_status.status_title,application_date,application_id,employee.employee_id,leave_approval.leave_approval_id,leave_application.from_time,leave_application.to_time')
	->from('employee')
    ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
    ->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','inner')
    ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')
	->join('ml_leave_type','leave_application.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
	->join('ml_status','leave_approval.status=ml_status.status_id','inner')
	->like('employee.full_name',$full_name)
	->like('ml_leave_type.ml_leave_type_id',$leave_type)
	->group_by('leave_approval.leave_approval_id')
	//->where('leave_approval.status = 2')
	->order_by('employee.employee_id','asc');

	//->where('from_date',date('Y-m-d'),'leave_application.status = 2');
	//$this->db->limit($limit,$start);
	if($limit != '' && $start != '' && $start != 0){
		$this->db->limit($limit, $start);
	}else if($limit != '' && $start == 0){
		$this->db->limit($limit);
	}
	//->group_by('leave_application.employee_id');
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->result();
			}
	
}
//	public function view_leave_count($where)
//	{
//		$this->db->select('leave_approval.status')
//			->from('employee')
//
//			->join('leave_approval', 'leave_approval.employee_id = employee.employee_id','LEFT')
//			->where($where)
//			->where('leave_approval.status = 2')
//		    ;
//			$query = $this->db->get();
//			if($query->num_rows() > 0)
//			{
//				return $query->row();
//		    }
//
//	}

public function view_leave_detail($where = NULL)
{
	$this->db->select('employee_code, full_name, ml_leave_type.leave_type,total_days,from_date,to_date,note,leave_approval.approval_date,leave_application.application_id,(select full_name from employee where employee_id = leave_approval.approved_by) as name,leave_approval.status,leave_application.from_time,leave_application.to_time')
	->from('employee')
    ->join('employment', 'employee.employee_id = employee.employee_id AND employment.trashed = 0 AND employment.current = 1','inner')
	->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','inner')
	->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')
	->join('ml_leave_type','leave_application.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
	->join('ml_status','leave_approval.status=ml_status.status_id','inner')
	->where($where)
	//->where('leave_approval.status = 2')
	->group_by('leave_application.application_id');
	//$this->db->limit($limit,$start);
	
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->row();
			}
	
}

//////////////////////////////////////// Apply For Leave ////////////////////////////////////////////////////

	public function fetch_information()
	{
		$query1 = $this->db->select('*')
		->from('employee')
		->join('employment',  'employee.employee_id=employment.employee_id AND current = 1 AND employment.trashed = 0','LEFT')
		->join('leave_approval','employment.employment_id=leave_approval.employment_id','LEFT')
	    ->join('leave_application',  'employment.employment_id = leave_application.employment_id','LEFT')
	    ->join('leave_entitlement',  'employment.employment_id= leave_entitlement.employment_id','LEFT')
	    ->join('position_management',  'employment.employment_id=position_management.employement_id','LEFT')
	    ->join('ml_leave_type',  'leave_application.ml_leave_type_id = ml_leave_type.ml_leave_type_id','LEFT')
	    ->join('ml_designations',  'position_management.ml_designation_id=ml_designations.ml_designation_id','LEFT')
		->join('ml_status','leave_approval.status=ml_status.status_id','inner')
	    ->group_by('employee.employee_id')
		->where('leave_approval.status = 1');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	
	}
	
	
	
  public function fetch_leave_act($where)
	   {
		$query = $this->db->select('employee.employee_id, application_id ,employee.employee_code, employee.full_name, leave_application.note, ml_designations.designation_name,leave_entitlement.no_of_leaves,leave_application.from_date,leave_application.to_date,leave_application.total_days, ml_leave_type.leave_type')
		->from('employee')
	 ->join('employment',         'employee.employee_id=employment.employee_id','inner')
	 ->join('leave_application',  'employee.employee_id = leave_application.employee_id','inner') 
	 ->join('leave_entitlement',  'employee.employee_id = leave_entitlement.employee_id','inner') 
	 ->join('position_management','employment.employment_id=position_management.employement_id','inner') 
	 ->join('ml_leave_type',      'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id','inner')
	->join('ml_designations',     'position_management.ml_designation_id=ml_designations.ml_designation_id','inner')  
	->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	
	}
	
	////////////////////////// Function in Site Model For Staff On Leave and For pagination ///////////////////////////////


	public function leave_staff_count()
		{
         $this->db->select('*')
			->from('employee')
             ->join('employment','employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0')
			 ->join('leave_approval','employment.employment_id=leave_approval.employment_id','inner')
			 ->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','left')
			 ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')

             ->join('leave_entitlement','leave_entitlement.leave_entitlement_id=leave_application.entitlement_id AND leave_entitlement.status = 2','inner')
			 ->join('ml_leave_type','ml_leave_type.ml_leave_type_id=leave_application.ml_leave_type_id','inner');
		$this->db->where('CURDATE() BETWEEN leave_approval.approved_from AND leave_approval.approved_to');

			// ->join('ml_status','leave_approval.status = ml_status.status_id','left')
		     //->where('leave_approval.status = 2')
		//->group_by('employee.employee_id');
		$query = $this->db->get();
			if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	
  ////////////////////////// Function in Site Model For Staff On Leave ///////////////////////////////////////
	
	public function leave_staff($limit = NULL, $start = NULL, $full_name = NULL)
		{
         $this->db->select('*')
				 ->from('employee')
				 ->join('employment','employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0')
				 ->join('leave_approval','employment.employment_id=leave_approval.employment_id','inner')
				 ->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','left')
				 ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')

				 ->join('leave_entitlement','leave_entitlement.leave_entitlement_id=leave_application.entitlement_id AND leave_entitlement.status = 2','inner')
				 ->join('ml_leave_type','ml_leave_type.ml_leave_type_id=leave_application.ml_leave_type_id','inner');
			$this->db->where('CURDATE() BETWEEN leave_approval.approved_from AND leave_approval.approved_to');		//->join('ml_status','leave_approval.status=ml_status.status_id','left')

			if(!empty($full_name)){
			$this->db->like('employee.full_name',$full_name);
			}
			if(!empty($limit) || !empty($start)){
				$this->db->limit($limit,$start);
			}

		//->where('leave_approval.status = 2')
		$this->db->order_by('employee.employee_id','DESC');
		$this->db->group_by('employee.employee_id');
			if($date1 = $this->input->post('from_date'))
			{
				$date2 = $this->input->post('to_date');
				$this->db->where('leave_application.from_date BETWEEN "'. date('Y-m-d', strtotime($date1)). '" and "'. date('Y-m-d', strtotime($date2)).'"');

			}


			$query = $this->db->get();
			if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	
////////////////////////// Function in Site Model For update Staff On Leave ///////////////////////////////////////
	
public function update_leave_staff($where, $employee_code, $full_name, $leave_type, $from_date, $to_date, $mobile_num,$status)
{
	$query = $this->db->query("update employee
join user_account on user_account.employee_id=employee.employee_id 
join leave_application on employee.employee_id=leave_application.employee_id 
join leave_entitlement on employee.employee_id=leave_entitlement.employee_id 
join ml_leave_type on leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id 
join emergency_contacts on employee.employee_id=emergency_contacts.employee_id
	set employee.employee_code = '".$employee_code."',
	employee.full_name = '".$full_name."',
	leave_entitlement.ml_leave_type_id = '".$leave_type."',
	leave_application.from_date = '".$from_date."',
	leave_application.to_date = '".$to_date."',
	emergency_contacts.mobile_num = '".$mobile_num."',
	leave_application.status = '".$status."'
	where $where");
	
		if($query){
	        return true;
		}else {
		return false;
		}
		
		}
		
//////////////////////////////////////////////////////// Staff On Leave //////////////////////////////////////////////////		
		
		public function staff_infromation($where)
	{
		$this->db->select(   'employee_code,full_name,leave_type,leave_application.from_date,leave_application.to_date,emergency_contacts.mobile_num,leave_application.status,employee.employee_id')
		    ->from('employee')
		    ->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
			->join('leave_entitlement','leave_application.entitlement_id=leave_entitlement.leave_entitlement_id','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')

		    ->where($where);
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->row();
			}
	
      }


//////////////////////////////////////////////// Function for Attendence////////////////////////////////////////////////////

 public function leave_attend_count()
				{
				$this->db->select('employee_code,full_name,attendence.in_time,attendence.in_date,attendence.out_time,attendence.out_date,CONCAT(attendence.latitude,"   //  ",attendence.longitude) AS la_lo,employee.employee_id',false)
			->from('employee')
			->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
            ->group_by('employee.employee_id')
            ->order_by('attendence.attendence_id','DESC');
		$query=$this->db->get();
			 if($query->num_rows > 0)
		{
			return $query->result();
		}
}



      public function leave_attend($limit = NULL,$start = NULL,$full_name = NULL)
				{
				$this->db->select('
				employee_code,
				full_name,
				GROUP_CONCAT(DISTINCT ml_projects.Abbreviation) AS Abbreviation,
				ml_department.department_name,
				attendence.in_time,
				attendence.in_date,
				attendence.out_time,
				attendence.out_date,
				CONCAT(attendence.latitude,"   //  ",attendence.longitude) AS la_lo,
				ml_attendance_status_type.status_type,
				work_week.working_day,
				ml_holiday.holiday_type,
				employee.employee_id',false)
			->from('employee')
			->join('employment', 'employment.employee_id = employee.employee_id', 'inner')
			->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
			//->join('leave_application', 'employee.employee_id = leave_application.employee_id', 'inner')
			->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
			->join('work_week', 'work_week.work_week_id = attendence.work_week_id', 'inner')
			->join('employee_project', 'employment.employment_id = employee_project.employment_id', 'inner')
			->join('ml_attendance_status_type','attendence.attendance_status_type = ml_attendance_status_type.attendance_status_type_id','inner')
			->join('ml_projects', 'employee_project.project_id = ml_projects.project_id', 'inner')
			->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'LEFT')
            ->join('ml_holiday','ml_holiday.ml_holiday_id = attendence.holiday','LEFT')
			->limit($limit,$start)
			->like('employee.full_name',$full_name)
            ->group_by('attendence.attendence_id')
            ->order_by('attendence.attendence_id','DESC');
			//$this->db->select("CONCAT(latitude, '.', longitude) AS la_lo", FALSE);
		if($assign = $this->input->post('project_title'))
		{
			$this->db->where('ml_projects.project_id', $assign);
		}
				
		if($status = $this->input->post('status_type'))
		{
			$this->db->where('attendence.attendance_status_type', $status);
		}
		
		if($department = $this->input->post('department_name'))
		{
			$this->db->where('posting.ml_department_id', $department);
		}
		if($date1 = $this->input->post('in_date'))
		    {
				$date2 = $this->input->post('out_date');
				$this->db->where('attendence.in_date BETWEEN "'. date('Y-m-d', strtotime($date1)). '" and "'. date('Y-m-d', strtotime($date2)).'"');
			}

		$query=$this->db->get();
		 if($query->num_rows > 0)
	{
		return $query->result();
	}
				
				//return $this->datatables->generate();
				}
		
/////////////////////////////////////// Function for Time Sheet pagination ///////////////////////////////////////////////	
	

	public function time_sheet_count()
		{
			$this->db->select('employee.full_name,project_title, task_name,in_date, in_time, Out_date ,out_time ,employee.employee_id')
		 ->from('employee')
		 ->join('employment','employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trahsed = 0','INNER')
		 ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		 
		 ->join('assign_job', 'assign_job.employment_id = employment.employment_id', 'inner')
		 
		 ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		 
		 ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner');
		 
		 $query=$this->db->get();
		 if($query->num_rows > 0)
	{
		return $query->result();
	}
}
		
/////////////////////////////////////////////////// Function for Time Sheet ////////////////////////////////////////////////		
		
		public function time_sheet($full_name)
		{
			if($full_name=$this->input->post('full_name')){
			$this->db->select('employee.employee_id,employee.full_name, project_title, task_name, month,in_date,year,ml_attendance_status_type.status_type, in_time, Out_date ,out_time ,employee.employee_id,MONTH(in_date)as in_month,YEAR(in_date)as in_year,attendence.ml_month_id,attendence.ml_year_id')
		 
		 ->from('employee')

          ->join('employment','employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0','INNER')

          ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')

          ->join('assign_job', 'assign_job.employment_id = employment.employment_id', 'inner')
		 
		  ->join('ml_attendance_status_type', 'ml_attendance_status_type.attendance_status_type_id = attendence.attendance_status_type', 'inner')
		 
		 ->join('ml_month', ' ml_month.ml_month_id = attendence.ml_month_id', 'inner')
		 
		 ->join('ml_year', ' ml_year.ml_year_id = attendence.ml_year_id', 'inner')

	     ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		 
		 ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		 
		 ->like('employee.full_name',$full_name)
		 
	     ->group_by('attendence.attendence_id');
		
		if($month=$this->input->post('month'))
		{
			$this->db->where('MONTH(attendence.in_date)',$month);
		}
		
		if($year=$this->input->post('year'))
		{$this->db->where('attendence.ml_year_id',$year);}
		 
		 $query=$this->db->get();
		 if($query->num_rows > 0)
		{
		return $query->result();
		}}else{ return false;}
}


	public function get_emp_name($where, $month, $year)
	{
		$this->db->like('employee.full_name', $where);
		$this->db->like('ml_month.ml_month_id', $month);
		$this->db->like('ml_year.ml_year_id', $year);
		 $query=$this->db->get('employee, ml_month, ml_year');
		 if($query->num_rows > 0)
		{
		return $query->row();
		}		
	}

	
////////////////////////////////////////////// Function for Standard Time Sheet ////////////////////////////////////////   
	
	public function standard_timesheet()
	    {
	  $this->datatables->select('employee_code, full_name,ml_month.month,ml_year.year, work_hours, leave_hours, monthly_std_hrs, employee.employee_id, hour_time_sheet_id')
        ->from ('employee')
		
        ->join('hourly_time_sheet','hourly_time_sheet.employee_id=employee.employee_id','inner')
		
        ->join('ml_total_month_hour','hourly_time_sheet.monthly_std_hrs_id=ml_total_month_hour.monthly_std_hrs_id','inner')
		
		->join('ml_month','hourly_time_sheet.ml_month_id=ml_month.ml_month_id','inner')
		
        ->join('ml_year','hourly_time_sheet.ml_year_id=ml_year.ml_year_id','inner');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="leave_attendance/edit_accumulative/$1/$2"><span class="fa fa-pencil" onclick="edit_accumulative($1)" style="cursor:pointer"></span></a>', 
		'employee.employee_id, hour_time_sheet_id');

		if($year = $this->input->post('year'))
		{
			$this->datatables->where('ml_year.ml_year_id', $year);
		}
		
		if($month = $this->input->post('month'))
		{
			$this->datatables->where('ml_month.ml_month_id', $month);
		}
		
		return $this->datatables->generate();
		}
		
		
		
	public function standard_timesheet_edt($where)
	    {
	  $this->db->select('employee_code, full_name,ml_month.month, ml_month.ml_month_id, ml_year.ml_year_id,ml_year.year, work_hours, leave_hours, monthly_std_hrs, hourly_time_sheet.hour_time_sheet_id')
        ->from ('employee')
		
        ->join('hourly_time_sheet','hourly_time_sheet.employee_id=employee.employee_id','left')
		
        ->join('ml_total_month_hour','hourly_time_sheet.monthly_std_hrs_id=ml_total_month_hour.monthly_std_hrs_id','left')
		
		->join('ml_month','hourly_time_sheet.ml_month_id=ml_month.ml_month_id','left')
		
        ->join('ml_year','hourly_time_sheet.ml_year_id=ml_year.ml_year_id','left')

		->where($where);
		
		if($year = $this->input->post('year'))
		{
			$this->db->where('ml_year.ml_year_id', $year);
		}
		
		if($month = $this->input->post('month'))
		{
			$this->db->where('ml_month.ml_month_id', $month);
		}
		
		$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->row();
			}
		}
   //*///////////////////////////////////// Function For Update Accumulative Time Sheet ///////////////////////////////*//		
		public function update_accumulative($where, $month, $year, $work_hours, $leave_hours)
{
	$query = $this->db->query("update employee
inner join hourly_time_sheet on hourly_time_sheet.employee_id=employee.employee_id
	set 
	hourly_time_sheet.ml_month_id = '".$month."',
	hourly_time_sheet.ml_year_id = '".$year."',
	hourly_time_sheet.work_hours = '".$work_hours."',
	hourly_time_sheet.leave_hours = '".$leave_hours."'
	where $where");
	if($query){
	        return true;
		}else {
		return false;
		}
      }
	
		
		
		
	/*///////////////////////////////// Function for Houlry accomulative time sheet///////////////////////////////////////*/
	
	
	
	public function accomulative_hourly($emp)
	{
		  $this->db->select('employee.employee_code,employee.full_name, work_hours, leave_hours,ml_designations.designation_name, leave_entitlement.no_of_leaves, leave_application.from_date,leave_application.to_date, ml_leave_type.leave_type,ml_total_month_hour.monthly_std_hrs')
        ->from ('hourly_time_sheet')
        ->join('employee','hourly_time_sheet.employee_id=employee.employee_id','inner')
        ->join('ml_total_month_hour','hourly_time_sheet.monthly_std_hrs_id=ml_total_month_hour.monthly_std_hrs_id','inner')
        ->join('leave_application', 'employee.employee_id =leave_application.employee_id','inner')
        ->join('leave_entitlement','leave_application.entitlement_id = leave_entitlement.leave_entitlement_id','inner')
        ->join('ml_leave_type','leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id','inner')
	   ->join('employment', 'employee.employee_id=employment.employment_id','inner')		
		->join('position_management','employment.employment_id=position_management.employement_id','inner')
		->join('ml_designations', 'position_management.position_id=ml_designations.ml_designation_id','inner')

		    ->where($emp);
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->row();
			}
	
      }
	  
	  
	   /////////////////////////////////////////// Function for Auto Complete Leave Entitlement ////////////////////////////////
	 /// Auto complete for hourly Leave Entitlement /////
	public function get_auto_leave_entit()
	{	
		
		$name=$this->input->post('queryString');
		
	$query = $this->db->query("SELECT employee.full_name,employee.employee_id
		FROM employee
INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '%$name%'");
	
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	/// Auto complete for hourly Leave attendence/////
	public function get_auto_leave_attendence()
	{

		$name=$this->input->post('queryString');

		$query = $this->db->query("SELECT employee.full_name,employee.employee_id
FROM employee
INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '$name%'");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	/// Auto complete for  Over Time/////
	public function get_auto_over_time()
	{

		$name=$this->input->post('queryString');

		$query = $this->db->query("SELECT employee.full_name,employee.employee_id
		FROM employee
INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '%$name%'");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	 
	 /////////////////////////////////////////// Function for Auto Complete Time Sheet ////////////////////////////////
	 
	 
	 
	 /// Auto complete for hourly time sheet /////
	public function get_autocomplete_time_sheet()
	{
		$name=$this->input->post('queryString');
		
	$query = $this->db->query("SELECT full_name,employee.employee_id
FROM employee
INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '%$name%'");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END

/////////////////////////////////////////////// Function for Print Leave Status /////////////////////////////////////////////


     public function print_leave_status($id)
	        {
		    $this->db->select('*')		
			->from('employee')
			->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
			->join('leave_entitlement','leave_application.application_id=leave_entitlement.leave_entitlement_id','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')
			->join('ml_status','leave_application.status=ml_status.status_id','inner')
	        ->where($id);
		    $query = $this->db->get();
		    return $query->row();
			}
	
	
	     public function print_all_leave_status()
	        {
		    $this->db->select('*')		
			->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id AND current = 1 AND employment.trashed = 0','inner')
			->join('leave_application','employment.employment_id=leave_application.employment_id','inner')
			->join('leave_entitlement','employment.employment_id=leave_entitlement.employment_id AND leave_entitlement.trashed = 0 AND leave_entitlement.status = 2','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
            ->group_by('employee.employee_id')
            ->order_by('employee.employee_id','DESC');
	        //->where($where);
		    $query = $this->db->get();
		    return $query->result();
    }
	
	



/////////////////////////////////////////////////////// Print Attendence ///////////////////////////////////////////////////////


public function print_attendence($id)
{
	$this->db->select('*')		
	->from('employee')
    ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
	->join('ml_attendance_status_type','attendence.attendance_status_type = ml_attendance_status_type.attendance_status_type_id',     'inner')
	->join('ml_assign_task', 'employee.employee_id = ml_assign_task.ml_assign_task_id', 'inner')
	->where($id);
	$query = $this->db->get();
	return $query->row();
}


///////////////////////////////////////////////// Print Funtion for All Attendence //////////////////////////////////////


public function print_attendence_present($id)
	{
		$this->db->select('*')		
		->from('employee')
		->join('employment', 'employment.employee_id = employee.employee_id', 'inner')
		->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
		->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		->join('ml_attendance_status_type','attendence.attendance_status_type = ml_attendance_status_type.attendance_status_type_id','inner')
		->join('employee_project','employment.employment_id = employee_project.employment_id','inner')
		->join('ml_projects', 'employee_project.project_id = ml_projects.project_id', 'inner')
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		->group_by('attendence.attendence_id, employee.employee_id')
		->where($id);
		$query = $this->db->get();
		return $query->result();
}


////////////////////////////////////// Print Function for Time sheet ////////////////////////////////////////////////////////

public function print_timesheet($id)
    {
    $this->db->select('*')		
	     ->from('employee')
	     ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		 ->join('assign_job', 'assign_job.assign_job_id = employee.employee_id', 'inner')
		 ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		 ->join('hourly_time_sheet','hourly_time_sheet.employee_id=employee.employee_id','inner')
		 ->join('ml_total_month_hour', 'hourly_time_sheet.monthly_std_hrs_id = ml_total_month_hour.monthly_std_hrs_id', 'inner')
		 ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
         ->where($id);
	     $query = $this->db->get();
	     return $query->row();

   }
  
  /*/////////////////////////////////////////////////////// print for Standard Time sheet ///////////////////////////////////////////////////*/
   
   public function print_standard_timesheet($where)
   {
   	  $this->db->select('*')
        ->from ('hourly_time_sheet')
        ->join('employee','hourly_time_sheet.employee_id=employee.employee_id','inner')
        ->join('ml_total_month_hour','hourly_time_sheet.monthly_std_hrs_id=ml_total_month_hour.monthly_std_hrs_id','inner')
		->join('ml_month','hourly_time_sheet.ml_month_id=ml_month.ml_month_id','inner')
        ->join('ml_year','hourly_time_sheet.ml_year_id=ml_year.ml_year_id','inner')
		->where($where);
		$query = $this->db->get();
		return $query->result();

   }
   
  //*////////////////////////////// Function for pagination /////////////////////////////////////////*//  
   
      	public function over_time_model_count()
			{
			 $this->db->select('employee.employee_code, employee.full_name, ml_month.month, ml_year.year,calculated_hours,sanction_hours')
			->from('over_time')
			->join('employee','employee.employee_id=over_time.employee_id','inner')
			->join('ml_month','ml_month.ml_month_id=over_time.ml_month_id','inner')
			->join('ml_year','ml_year.ml_year_id=over_time.ml_year_id','inner');
			//->group_by('employee.employee_id');
			$query = $this->db->get();
				if($query->num_rows() > 0)
			{
				return $query->result();
			}
	   
	  } 
   
   
      //////////////////////// Function for Over time ///////////////////////////////////////////////////////////
   
   	public function over_time_model($limit = NULL, $start = NULL,$full_name = NULL, $month = NULL, $year = NULL)
		{
         $this->db->select('employee.employee_code, employee.full_name, ml_month.month, ml_year.year,calculated_hours,sanction_hours')
		->from('employee')
		->join('over_time','employee.employee_id=over_time.employee_id','inner')
		->join('ml_month','ml_month.ml_month_id=over_time.ml_month_id','inner')
		->join('ml_year','ml_year.ml_year_id=over_time.ml_year_id','inner')
		->like('employee.full_name',$full_name)
		->limit($limit,$start);
	    //->group_by('employee.employee_id');
		if($month)
		{$this->db->where('ml_month.ml_month_id',$month);}
		if($year)
		{$this->db->where('ml_year.ml_year_id',$year);}
		$query = $this->db->get();
			if($query->num_rows() > 0)
		{
			return $query->result();
		}
   
  }   
  
  
/////////////////////////////////////////////// Leave Entitlement Funtions //////////////////////////////////////  


//Deprecated Function..
    public function leave_entitlement_count()
	{
     $this->db->select('employee.employee_code,employee.full_name,leave_entitlement.no_of_leaves')
	 ->from('leave_entitlement')
	  ->join('employee', 'employee.employee_id=leave_entitlement.employee_id','inner')
	 // ->join('leave_entitlement',  'employee.employee_id = leave_entitlement.employee_id','inner') 
	  //->join('ml_year','ml_year.ml_year_id=attendence.ml_year_id','inner')
	  
     ->group_by('employee.employee_id')
	 ->where('leave_entitlement.status = 2');
		$query = $this->db->get();
			if($query->num_rows() > 0)
		{
			return $query->result();
		}
  
	}
  
  public function leave_enetitlement($limit = NULL, $start = NULL,$full_name = NULL, $year = NULL)
	{
     $this->db->select('*,(SUM(DISTINCT no_of_leaves_allocated)) as no_of_leaves')
	 ->from('employee')
         ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0')
         ->join('leave_entitlement', 'employment.employment_id= leave_entitlement.employment_id','inner')
         ->join('leave_application','leave_entitlement.leave_entitlement_id = leave_application.entitlement_id','left')
         ->join('leave_approval','leave_approval.employment_id = employment.employment_id','left')
	     ->like('employee.full_name',$full_name);
		if(!empty($limit) && !empty($limit))
		{
			$this->db->limit($limit,$start);
		}
		$this->db->group_by('employee.employee_id');
		$this->db->order_by('employee.employee_id','DESC');
		$this->db->where('leave_approval.status', 2);
		$query = $this->db->get();
			if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
	
   
 public function leave_enetitlement_detail($year = NULL, $employeeID = NULL)
 {$query = $this->db->query('SELECT
MLLT.leave_type AS AvailableLeaveTypes,

IFNULL(JN.AllocatedLeaves,0) AS TotalAllocatedLeaves,
IFNULL(JN.YearTotal,0) AS YearTotal,

IFNULL(JN.Balance,0) AS Balance,
JN.LeaveFrom,
JN.LeaveTo
FROM ml_leave_type MLLT
LEFT JOIN (SELECT
LA.leave_application_id,
LE.no_of_leaves_allocated AS AllocatedLeaves,
LA.LeaveFrom,
LA.LeaveTo,
LE.ml_leave_type_id,
IFNULL(LA.YearTotal,0) AS YearTotal,
(LE.no_of_leaves_allocated - IFNULL(LA.YearTotal,0)) AS Balance
FROM employee E
INNER JOIN employment ET ON E.employee_id = ET.employee_id AND ET.current = 1
INNER JOIN leave_entitlement LE ON ET.employment_id = LE.employment_id AND LE.status = 2

LEFT JOIN (SELECT
  LA.employment_id,
  SUM(DISTINCT CASE WHEN (LAPP.ml_leave_type_id) THEN DATEDIFF(LA.approved_to, LA.approved_from) + 1 ELSE 0 END) AS YearTotal,

  LAPP.entitlement_id,
  LAPP.ml_leave_type_id,
  LA.leave_application_id,
  LA.approved_from AS LeaveFrom,
  LA.approved_to AS LeaveTo
FROM
  leave_approval LA
  LEFT JOIN leave_application LAPP
    ON LAPP.application_id = LA.leave_application_id
    WHERE YEAR(LA.approval_date) = '.$year.'

GROUP BY LA.employment_id,
LAPP.ml_leave_type_id
         ) AS LA ON LA.entitlement_id = LE.leave_entitlement_id
WHERE E.employee_id = '.$employeeID.'
GROUP BY E.employee_id,LE.ml_leave_type_id
) AS JN ON MLLT.ml_leave_type_id = JN.ml_leave_type_id');
        if($query){
            return $query->result();
        }else{
	return FALSE;
}
}

	public function GetEmployeePersonalDetails($table,$where=null)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->get()->row();
	}
    ////////////////////// Function for attendance retreive from database //////////////////////////////////////////
    public function attendance_in($id,$datein)
    {
        $this->db->select('employee_id,in_date');
        $this->db->from('attendence');
        //$this->db->join('employee');
        $this->db->where('employee_id',$id);
        $this->db->where('in_date',$datein);
        // $this->db->where('out_date',$dateout);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function attendance_out($id,$dateout)
    {
        $this->db->select('employee_id,out_date');
        $this->db->from('attendence');
        $this->db->where('employee_id',$id);
        $this->db->where('out_date',$dateout);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    //// End of Function
	
	/// Auto complete for hourly time sheet /////
	public function get_autocomplete_attendence()
	{
		$name=$this->input->post('queryString');

		$query = $this->db->query("SELECT full_name,employee.employee_id
FROM employee
         INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '$name%'");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END

/// Auto complete for hourly time sheet /////
	public function get_autocomplete_name()
	{
		$name=$this->input->post('queryString');

		$query = $this->db->query("SELECT full_name,employee.employee_id
FROM employee
 INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '$name%'");
$this->db->select('DISTINCT employee.employee_id');
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END
	/// Auto complete for Apply for Leave /////
	public function get_autocomplete_apply_for_leave()
	{
		$name=$this->input->post('queryString');

		$query = $this->db->query("SELECT full_name,employee.employee_id
       FROM employee
       INNER JOIN employment ON employment.employee_id=employee.employee_id
		WHERE employment.current=1 AND full_name like '$name%'");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END


//***************************************** END OF Leave Attendance ***************************************************// 	

    public function getTotalEmployeeLeaves($month,$year,$employee,$employmentID){
        $query = $this->db->query('SELECT
MLLT.hex_color AS HexColor,
MLLT.leave_type AS AvailableLeaveTypes,
IFNULL(JN.AllocatedLeaves,0) AS TotalAllocatedLeaves,
IFNULL(JN.TotalLeavesTaken,0) AS TotalLeavesTaken,
IFNULL(JN.Balance,0) AS Balance,
JN.LeaveFrom,
JN.LeaveTo
FROM ml_leave_type MLLT
LEFT JOIN (SELECT
LA.leave_application_id,
LE.no_of_leaves_allocated AS AllocatedLeaves,
MLLT.leave_type AS LeaveTypeName,
MLLT.ml_leave_type_id AS LeaveTypeID,
LA.LeaveFrom,
LA.LeaveTo,
IFNULL(LA.TotalLeavesTaken,0) AS TotalLeavesTaken,
(LE.no_of_leaves_allocated - IFNULL(LA.TotalLeavesTaken,0)) AS Balance
FROM employee E
INNER JOIN employment EMP ON E.employee_id = EMP.employee_id
INNER JOIN leave_entitlement LE ON EMP.employment_id = LE.employment_id AND LE.status = 2
LEFT JOIN ml_leave_type MLLT ON MLLT.ml_leave_type_id = LE.ml_leave_type_id
LEFT JOIN (SELECT LA.employment_id, DATEDIFF(LA.approved_to, LA.approved_from) + 1 AS TotalLeavesTaken ,LAPP.entitlement_id, LAPP.ml_leave_type_id, LA.leave_application_id, LA.approved_from AS LeaveFrom, LA.approved_to AS LeaveTo
           FROM leave_approval LA
           INNER JOIN leave_application LAPP ON LAPP.application_id = LA.leave_application_id
           WHERE LA.employment_id = '.$employmentID.' AND YEAR(LA.approval_date) = '.$year.' AND MONTH(LA.approval_date) = '.$month.'
         ) AS LA ON LA.entitlement_id = LE.leave_entitlement_id
WHERE E.employee_id = '.$employee.'
) AS JN ON MLLT.ml_leave_type_id = JN.LeaveTypeID
WHERE MLLT.trashed = 0');
        if($query){
            return $query->result();
        }else{
            return FALSE;
        }
    }

    public function getTotalEmployeeLeavesNew($month,$year,$employee,$employmentID)
    {
        $query = $this->db->query('SELECT
MLLT.leave_type AS AvailableLeaveTypes,
MLLT.hex_color AS HexColor,
IFNULL(JN.AllocatedLeaves,0) AS TotalAllocatedLeaves,
IFNULL(JN.YearTotal,0) AS YearTotal,
IFNULL(JN.MonthTotal,0) AS MonthTotal,
IFNULL(JN.Balance,0) AS Balance,
JN.LeaveFrom,
JN.LeaveTo
FROM ml_leave_type MLLT
LEFT JOIN (SELECT
LA.leave_application_id,
LE.no_of_leaves_allocated AS AllocatedLeaves,
LA.LeaveFrom,
LA.LeaveTo,
LE.ml_leave_type_id,
IFNULL(LA.YearTotal,0) AS YearTotal,
IFNULL(LA.MonthTotal,0) AS MonthTotal,
(LE.no_of_leaves_allocated - IFNULL(LA.YearTotal,0)) AS Balance
FROM employee E
INNER JOIN employment EMP ON EMP.employee_id = E.employee_id
INNER JOIN leave_entitlement LE ON EMP.employment_id = LE.employment_id AND LE.status = 2
LEFT JOIN (SELECT
  LA.employment_id,
  SUM(DISTINCT CASE WHEN (LAPP.ml_leave_type_id) THEN DATEDIFF(LA.approved_to, LA.approved_from) + 1 ELSE 0 END) AS YearTotal,
  SUM(DISTINCT CASE WHEN (LAPP.ml_leave_type_id AND MONTH(LA.approval_date) = '.$month.') THEN DATEDIFF(LA.approved_to, LA.approved_from) + 1 ELSE 0 END) AS MonthTotal,
  LAPP.entitlement_id,
  LAPP.ml_leave_type_id,
  LA.leave_application_id,
  LA.approved_from AS LeaveFrom,
  LA.approved_to AS LeaveTo
FROM
  leave_approval LA
  INNER JOIN leave_application LAPP
    ON LAPP.application_id = LA.leave_application_id
    WHERE YEAR(LA.approval_date) = '.$year.'
  AND LA.employment_id = '.$employmentID.'
GROUP BY LA.employment_id,
LAPP.ml_leave_type_id,leave_approval_id
         ) AS LA ON LA.entitlement_id = LE.leave_entitlement_id
WHERE E.employee_id = '.$employee.'
GROUP BY E.employee_id,LE.ml_leave_type_id
) AS JN ON MLLT.ml_leave_type_id = JN.ml_leave_type_id
WHERE MLLT.trashed = 0');
        if ($query) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
    public function get_today_and_yesterday_checked_out_employees(){
        $result = $this->db->query('SELECT days.day AS `date`, ATT.EmployeeID, ATT.EmployeeName, ATT.EmployeeInDate, ATT.EmployeeOutDate
FROM
  (SELECT CURDATE() AS `day`
   UNION SELECT CURDATE() - INTERVAL 1 DAY) days
  LEFT JOIN (SELECT
  E.employee_id AS EmployeeID,
  `E`.`full_name` AS EmployeeName,
  `ATT`.`in_date` AS EmployeeInDate,
  `ATT`.`out_date` AS EmployeeOutDate,
  `ATT`.`in_time` AS EmployeeInTime,
  `ATT`.`out_time` AS EmployeeOutTime
FROM
  (`employee` E)
  INNER JOIN `employment` ET
    ON `E`.`employee_id` = `ET`.`employee_id`
    AND ET.current = 1
  INNER JOIN `employee_project` EP
    ON `ET`.`employment_id` = `EP`.`employment_id`
  INNER JOIN `attendence` ATT
    ON `ATT`.`employee_id` = `E`.`employee_id`
  INNER JOIN `posting` P
    ON `P`.`employement_id` = `ET`.`employment_id`
    AND P.current = 1
    AND P.status = 2
WHERE `E`.`enrolled` = 1
GROUP BY `E`.`employee_id` , ATT.in_date) AS ATT
   ON days.day = ATT.EmployeeInDate
   WHERE ATT.EmployeeOutDate != "0000-00-00"');
        return $result->result_array();
    }

	public function join_for_project($where)
	{
		$this->db->select('*')
			->from('ml_projects')
			->join('ml_project_status_type','ml_projects.project_status_type_id = ml_project_status_type.pst_id','inner')
            ->join('ml_program_list','ml_projects.ProgramID = ml_program_list.ProgramID','INNER')
			->where($where);
		$query = $this->db->get();
		if($query)
		{
			return $query->result();
		}


	}

	public function get_all_months_in_between($fromDate,$toDate){
		$query = "SELECT
  DATE_FORMAT(m1, '%Y') AS `Year`,
  DATE_FORMAT(m1, '%b') AS `Month`

FROM
(
SELECT
( '$fromDate' - INTERVAL DAYOFMONTH('$fromDate')-1 DAY)
+INTERVAL m MONTH AS m1
FROM
(
SELECT @rownum:=@rownum+1 AS m FROM
(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t1,
(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t2,
(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t3,
(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t4,
(SELECT @rownum:=-1) t0
) d1
) d2

WHERE m1<= '$toDate'
ORDER BY m1";
		$result = $this->db->query($query);
		return $result->result_array();
	}

}
?>