<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hr_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		
	}
	/// Function To Fetch User  
	public function fetch_user($id)
	{
		$this->db->select('*')->from('user');
		
		$this->db->where('dept_id',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				$data[$row->user_id] = $row->user_name;
            }
			return $data;
		}else{
			return array();
        }
	}
        
	/// For Making Thumbnails
	function resize_image($sourcePath, $desPath, $width = '300', $height = '300')
    {
		$this->load->library('image_lib');
		
        $this->image_lib->clear();
		
        $config['image_library'] 	= 'gd2';
        $config['source_image'] 	= $sourcePath;
        $config['new_image'] 		= $desPath;
        $config['quality'] 			= '100%';
        $config['create_thumb'] 	= TRUE;
        $config['maintain_ratio'] 	= true;
        $config['thumb_marker'] 	= '';
        $config['width'] 			= $width;
        $config['height'] 			= $height;
		
        $this->image_lib->initialize($config);
 
        if ($this->image_lib->resize())
            return true;
        return false;
    } 
	/// Function For Uploading Images
	public function do_upload($file_name) 
	{
		$config['upload_path'] = 'upload/';
		
		$config['allowed_types'] = 'jpg|png|gif|pdf|docx|xls|xlsx|JPEG|psd|txt|text';
		
		$config['max_size'] = '10000000000';
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload($file_name))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
            
			return $data;
        }
	}         
         
	public function upload_pic($image) 
	{
		
		$config['upload_path'] = 'upload/Download_Files/';
		
		$config['allowed_types'] = 'jpg|png|gif|pdf|docx|xls|xlsx|JPEG';
		
		$config['max_size'] = '10000000000';
		$config['file_name'] = 'File'.time();

		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload($image))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
			
			return $data;
        }
	}        

	public function get_row_by_id($table,$where)
	{
		$this->db->select('*');
		
		$this->db->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	}
        
	public function get_row_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	}


    public function get_row_by_where_repeat($table,$where)
    {
        $this->db->select('*')->from($table);

        $this->db->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function get_num_rows_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		
		$this->db->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
        }
	}

	public function create_new_record($table,$data)
	{
		$query = $this->db->insert($table, $data);
		
		if($query)
		{
			return true;
        } else{
			return false;
        }
	}

	public function insert_data_get_last_id($table,$data)
	{
		$query = $this->db->insert($table, $data);
		
		return $this->db->insert_id();
	}

	public function get_last_id() 
	{
		return $this->db->insert_id();
	}
       
	public function delete_row_by_where($table,$where)
	{
		$this->db->where($where);
		
		$query = $this->db->delete($table);
		
		if($query)
		{
			return true;
        }
	}	
	/// Delete By Join 
	public function delete_by_join($where)
	{
		$query = $this->db->query("DELETE assign_job.*, performance_evaluation.*
		FROM assign_job
		INNER JOIN performance_evaluation ON assign_job.assign_job_id = performance_evaluation.job_assignment_id
		WHERE $where");		
		if($query)
		{
			return true;
        }
	}	
	/// Update By Join 
	public function update_by_join($where_job, $project_id, $ml_assign_task_id, $start_date, $completion_date,$task_description,$status, $ml_kpi_type_id)
	{
		$query = $this->db->query("Update assign_job
		INNER JOIN performance_evaluation ON assign_job.assign_job_id = performance_evaluation.job_assignment_id
		SET 
		assign_job.project_id = '$project_id',
		assign_job.ml_assign_task_id = '$ml_assign_task_id',
		assign_job.start_date = '$start_date',
		assign_job.completion_date = '$completion_date',
		assign_job.task_description = '$task_description',
		assign_job.status = '$status',
		performance_evaluation.ml_kpi_type_id = '$ml_kpi_type_id'
		WHERE $where_job");		
		if($query)
		{
			return true;
        }
	}	
             
	public function update_record($table,$where,$data)
	{
		$this->db->where($where);
		
		$query = $this->db->update($table,$data);
		
		if($query)
		{
	        return true;
		}else{
			return false;
		}
	}
	public function update_trash($table,$where,$id)
	{
		$this->db->where($where);
        
		$query = $this->db->update($table,array('trashed' => 0));
	    
		if($query)
		{
	        return true;
		}else {
			return false;
		}
	}
	
	public function delete_trash($where)
	{
		$this->db->where('trash_store_id',$where);
        
		$query = $this->db->delete('trash_store');
	    
		if($query)
		{
	        return true;
		}else {
			return false;
		}
	}
	

	public  function get_all($table)
	{
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	public  function get_one($table)
	{
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

	public function get_all_by_where($table,$where)
	{
		$this->db->select('*');
	    
		$this->db->where($where);
	    
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

    public function get_tasks_where_project($table,$where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
        $tasks=array();
        if($query->result())
        {
            foreach($query->result() as $task)
            {
                $tasks[$task->ml_assign_task_id]=$task->task_name;
            }
            return $tasks;
        }else
        {return false;}
	}

    public function get_kpi_where_project($table,$where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($table);
        $kpis=array();
        if($query->result())
        {
            foreach($query->result() as $kpi)
            {
                $kpis[$kpi->ml_kpi_id]=$kpi->kpi;
            }
            return $kpis;
        }else
        {return false;}
    }

	
	public  function count_by_where($table,$where)
	{
		$this->db->where($where);
		
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
        
	public function redirect_to($location = NULL)
	{
		if($location != '')
		{
			redirect($location); 
		}    
	}
        public function chk_employee_code($where = NULL)
		{
			$this->db->where($where);
           
		    $query = $this->db->get('employee');
			
			if($query->num_rows() > 0)
			{
				return true;
			}else{
				return false;
			}  
        }
	/// Function to Check CNIC Duplicate Insertion
	function check_nic_already_exists($where)
	{
		$this->db->where($where);
		
		$query = $this->db->get('employee');
		
		if($query->num_rows() > 0)
		{
			return true;
		}else{
			return false;
		}
	}

    /// Function to Check CNIC Duplicate Insertion
    function check_nic_already_existsApplicant($where)
    {
        $this->db->where($where);

        $query = $this->db->get('applicants');

        if($query->num_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
    }
	/// ************************* ********* ******* ** ****** ***********************************///
	//// ======================== Functions Created By Nadeem =================================////
	/// ************************* ********* ******* ** ****** ***********************************///////
	/// Dynamic Function For Drop Down With Select Option
	public  function dropdown_wd_option($table, $option, $key, $value, $where = '')
	{
		$this->db->select('*');
		if(!empty($where)){
			$this->db->where($where);
		}
		$query = $this->db->get($table);
  		
		if($query->num_rows() > 0) 
		{
			$data[''] = $option;
			foreach($query->result() as $row) 
			{
					$data[$row->$key] = $row->$value;
			}
			return $data;
		}
	}
	/// Function For Selecting Employee Emergency Contact Details
	public function emergency_contact_detail($employee_id)
	{
		$this->db->select('cotact_person_name, relation_name, home_phone, mobile_num, work_phone, emergency_contact_id')
		
		->from('emergency_contacts')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Qualification Details
	public function qualification_detail($employee_id)
	{
		$this->db->select('qualification_id, employee_id, qualification_title, qualification, institute, year')
		
		->from('qualification')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id','inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Experience Details
	public function emp_experience_detail($employee_id)
	{
		$this->db->select('organization, designation_name, from_date, to_date, total_dur, experience_id')
		
		->from('emp_experience')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_designations', 'emp_experience.job_title = ml_designations.ml_designation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Skills Details
	public function emp_skill_detail($employee_id)
	{
		$this->db->select('skill_name, skill_level, skill_record_id, comments')
		
		->from('emp_skills')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		->join('ml_skill_level', 'emp_skills.ml_skill_level_id = ml_skill_level.level_id', 'inner')
		//->where('emp_skills.enabled',1)
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Training Details
	public function emp_training_detail($employee_id)
	{
		$this->db->select('training_name, date, description')
		
		->from('training')
		
		//->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Dependents Details
	public function emp_dependent_detail($employee_id)
	{
		$this->db->select('dependent_name, relation_name, date_of_birth, illness, insurance_start_date, dependent_id')
		
		->from('dependents')
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Icrements Details
	public function entitle_increment_detail($employee_id)
	{
		$this->db->select('increment_amount, increment_type, (select full_name from employee where employee_id = increments.approved_by) as emp_name, approval_date,date_effective, status_title, increment_id')
		
		->from('increments')
		
		->join('employment', 'increments.employment_id = employment.employment_id', 'left')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'left')

		->join('ml_status', 'increments.status = ml_status.status_id', 'left')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Allowance Details
	public function entitle_allowance_detail($employee_id)
	{
		$this->db->select('allowance_amount, allowance_type, (select full_name from employee where employee_id = allowance.approved_by) as emp_name, allowance.date_approved,ml_pay_frequency.pay_frequency,effective_date,status_title, allowance_id')
		
		->from('allowance')
		
		->join('employment', 'allowance.employment_id = employment.employment_id', 'inner')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'inner')
		
		->join('ml_status', 'allowance.status = ml_status.status_id', 'inner')

		->join('ml_pay_frequency', 'allowance.pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'inner')

		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Benefit Details
	public function entitle_benefit_detail($employee_id)
	{
		$this->db->select('date_effective, benefit_type_title,(select full_name from employee where employee_id = benefits.approved_by) as emp_name, benefits.date_approved, status_title, emp_benefit_id')
		
		->from('benefits')
		
		->join('employment', 'benefits.employment_id = employment.employment_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		//->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Leave Details
	public function entitle_leave_detail($employee_id)
	{
		$this->db->select('no_of_leaves_allocated,no_of_leaves_entitled, leave_type, full_name, (select full_name from employee where employee_id = leave_entitlement.approved_by) as emp_name, date_approved, status_title, leave_entitlement_id')
		->from('leave_entitlement')
		->join('employment', 'leave_entitlement.employment_id = employment.employment_id', 'inner')
        ->join('employee' , 'employment.employee_id = employee.employee_id', 'inner')
        //->join('contract' , 'employment.employment_id = contract.employment_id', 'inner')
        //->join('employee_contract_extensions' , 'contract.contract_id = employee_contract_extensions.contract_id', 'inner')
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'inner')
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sup_auto_search($where)
	{
		$this->db->select('employee.employee_id, employee.full_name');
        $this->db->distinct();
		$this->db->from('employee');
		$this->db->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed=0', 'inner');
        $this->db->where($where);
		$this->db->like('full_name', $this->input->post('queryString'), 'after');
		
		$this->db->limit(5);

		$query = $this->db->get();


        return $query->result();
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sub_auto_search($where = NULL)
	{
		$this->db->select('employee.employee_id,employee.full_name');
		
		$this->db->from('employee');
		$this->db->join('employment','employee.employee_id = employment.employee_id','inner');
		
		$this->db->like('full_name', $this->input->post('queryString'), 'after');


            $this->db->where('employment.current = 1');
        

        $this->db->limit(5);
		
		$query = $this->db->get();

		return $query->result();
	}
	/// Function For All Employee List
	public function view_all_employee()
	{
        $department = $this->input->post('department');
        $design = $this->input->post('designation');
        $project = $this->input->post('project');
        if(isset($department) && !is_numeric($department) && !empty($department)){
            echo "FAIL";
            return;
        }
        if(isset($design) && !is_numeric($design) && !empty($design)){
            echo "FAIL";
            return;
        }
   if(isset($project) && !is_numeric($project) && !empty($project)){
            echo "FAIL";
            return;
        }
		$this->datatables->select('
		employee.thumbnail,
		GROUP_CONCAT(DISTINCT employee_code) AS employee_code,
		GROUP_CONCAT(DISTINCT full_name) AS full_name,
		GROUP_CONCAT(DISTINCT ml_designations.designation_name) AS designation_name,
		ml_department.department_name,

		GROUP_CONCAT(DISTINCT ml_projects.Abbreviation separator ",") AS project_title,
		(CASE WHEN(IFNULL(posting.ml_branch_id,0) = 0) THEN ml_projects.Abbreviation ELSE ml_branch.branch_name END) as branch_name,
		GROUP_CONCAT(DISTINCT ml_posting_location_list.posting_location) AS posting_location,
		mob_num,
		email_address AS EmployeeEmailAddress,
		employee.employee_id AS EmployeeID',
            false)

		->from('employee')

		->join('employment', 'employee.employee_id = employment.employee_id', 'left')

		->join('current_contacts','employee.employee_id = current_contacts.employee_id','left')

		->join('position_management', 'employment.employment_id = position_management.employement_id', 'left')

		->join('posting', 'employment.employment_id = posting.employement_id', 'left')

		->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'left')

		->join('ml_posting_location_list','posting.location = ml_posting_location_list.posting_locations_list_id','left')

        ->join('ml_branch','ml_branch_id = posting.ml_branch_id AND posting.trashed = 0','left')

		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'left')

		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'left')
		->join('employee_project EP','EP.employment_id = employment.employment_id AND EP.trashed=0 AND EP.current=1 AND EP.enabled=1','left')
		->join('ml_projects','EP.project_id = ml_projects.project_id','left')
		->where('employee.enrolled = 1 AND employment.current = 1');



		if($department)
		{
			$this->datatables->where('posting.ml_department_id', $department);
		}
		if($design)
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		if($project)
		{
			$this->datatables->where('EP.project_id', $project);
		}
        $this->datatables->group_by('employee.employee_id');
		$this->datatables->edit_column('employee_id',
		'<a href="human_resource/edit_employee_acct/$1">
<span class="fa fa-pencil" title="Edit Record"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp;
<a href="human_resource/view_employee_profile/$1"><span class="fa fa-eye" title="View Record"></span></a> &nbsp;&nbsp;| &nbsp; &nbsp; <span class="fa fa-print" title="Print" onclick="print_report($1)" style="cursor:pointer"></span>',
		'EmployeeID');

		return $this->datatables->generate();
	}
	public function view_all_appraisal()
	{
		$design = $this->input->post('designation');
		if(isset($design) && !is_numeric($design) && !empty($design)){
			echo "FAIL";
			return;
		}
		$this->datatables->select('
		GROUP_CONCAT(DISTINCT employee_code) AS employee_code,
		GROUP_CONCAT(DISTINCT full_name) AS full_name,
		GROUP_CONCAT(DISTINCT ml_designations.designation_name) AS designation_name,
		GROUP_CONCAT(DISTINCT ml_posting_location_list.posting_location) AS posting_location,
		employee.employee_id AS EmployeeID',
			false)

			->from('employee')

			->join('employment', 'employee.employee_id = employment.employee_id', 'left')

			->join('current_contacts','employee.employee_id = current_contacts.employee_id','left')

			->join('position_management', 'employment.employment_id = position_management.employement_id', 'left')

			->join('posting', 'employment.employment_id = posting.employement_id', 'left')

			->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'left')

			->join('ml_posting_location_list','posting.location = ml_posting_location_list.posting_locations_list_id','left')

			->join('ml_branch','ml_branch_id = posting.ml_branch_id AND posting.trashed = 0','left')

			->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'left')

			->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'left')
			->join('employee_project EP','EP.employment_id = employment.employment_id AND EP.trashed=0 AND EP.current=1 AND EP.enabled=1','left')
			->join('ml_projects','EP.project_id = ml_projects.project_id','left')
			->where('employee.enrolled = 1 AND employment.current = 1');
		if($design)
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		$this->datatables->group_by('employee.employee_id');
		$this->datatables->edit_column('setup',
			'<a href="human_resource/appraisal_setup/$1">
             <span class="" title="Appraisal Setup">Setup</span></a>
          ',
			'EmployeeID');

		return $this->datatables->generate();
	}
	/// Function to View Complete Profile of Employee
	public function join_for_gender_etc($where)
	{
		$this->db->select('ml_marital_status.marital_status_title, ml_nationality.nationality, ml_religion.religion_title')
		
		->from('employee')

		->join('ml_marital_status', 'employee.marital_status = ml_marital_status.marital_status_id', 'inner')
		
		->join('ml_nationality', 'employee.nationality = ml_nationality.nationality_id', 'inner')
		
		->join('ml_religion', 'employee.religion = ml_religion.religion_id', 'inner')		
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_contact($where_con)
	{
		$this->db->select('permanant_contacts.address, permanant_contacts.phone, ml_city.city_name,ml_district.district_name, ml_province.province_name')
		
		->from('employee')
		
		->join('current_contacts', 'employee.employee_id = current_contacts.employee_id', 'inner')
		
		->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'inner')
		
		->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'inner')
		
		->join('ml_district', 'permanant_contacts.district = ml_district.district_id', 'inner')
		
		->join('ml_province', 'permanant_contacts.province = ml_province.province_id', 'inner')
			
		
		->where($where_con);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_rel($where_con)
	{
		$this->db->select(' ml_relation.relation_name')
		
		->from('employee')
		
		->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'inner')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->where($where_con);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_exp($where_exp)
	{
		$this->db->select(' ml_employment_type.employment_type, ml_designations.designation_name')
		
		->from('employee')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'left')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'left')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'left')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'left')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'left')
		
		->where($where_exp);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_skl($where_emp)
	{
		$this->db->select(' ml_skill_type.skill_name')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'left')

		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->where($where_emp);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function detl_employeeee($where_p)
	{
		$this->db->select('*')
		
		->from('position_management')
		
		->join('employment', 'position_management.employement_id = employment.employment_id', 'left')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'left')

		->join('contract', 'employment.employment_id = contract.employment_id', 'left')

		->join('employee_contract_extensions', 'contract.contract_id = employee_contract_extensions.contract_id AND employee_contract_extensions.current = 1', 'left')

		->join('employee', 'employment.employee_id = employee.employee_id', 'left')

		->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'left')

		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'left')

		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'left')

		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'left')

		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'left')

		->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'left')

		->where($where_p);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_depend($where_d)
	{
		$this->db->select('dependents.dependent_name, ml_relation.relation_name, dependents.date_of_birth')
		
		->from('employee')

        ->join('employment', 'employment.employee_id = employee.employee_id AND employment.current = 1 ', 'left')
		
		->join('dependents', 'dependents.employee_id = employee.employee_id', 'inner')
		
		->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'inner')
		
		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->where($where_d);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	public function join_for_pay($where_d)
	{
		$this->db->select('ml_pay_grade.pay_grade, ml_pay_frequency.pay_frequency, currency_name, ml_payrate.payrate')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('salary', 'employment.employment_id = salary.employement_id', 'inner')
		
		->join('ml_currency', 'salary.ml_currency = ml_currency.ml_currency_id', 'inner')
		
		->join('ml_payrate', 'salary.ml_pay_rate_id = ml_payrate.ml_payrate_id', 'inner')
		
		->join('ml_pay_frequency', 'salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'inner')
		
		->join('ml_pay_grade', 'salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		
		->where($where_d);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	public function join_for_last($where_d)
	{
		$this->db->select('increment_amount, ml_increment_type.increment_type, ml_benefit_type.benefit_type_title, allowance_amount, ml_allowance_type.allowance_type, no_of_leaves_allocated, ml_leave_type.leave_type, (select employee.full_name from employee where employee_id = reporting_heirarchy.employeed_id) as emp_name, ml_reporting_options.reporting_option')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'LEFT')
		
		->join('increments', 'employment.employment_id = increments.employment_id', 'LEFT')
		
		->join('allowance', 'employment.employment_id = allowance.employment_id', 'LEFT')
		
		->join('reporting_heirarchy', 'employee.employee_id = reporting_heirarchy.employeed_id', 'LEFT')
		
		->join('leave_entitlement', 'employment.employment_id = leave_entitlement.employment_id', 'LEFT')
		
		->join('benefits', 'employment.employment_id = benefits.employment_id', 'LEFT')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'LEFT')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'LEFT')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'LEFT')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'LEFT')
		
		->join('ml_reporting_options', 'reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id', 'LEFT')
		
		
		->where($where_d);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	
	
	public function view_complete_profile($where)
	{
		$this->db->select('employee.thumbnail, employee.full_name,
		employee.employee_code, employee.father_name, employee.CNIC,
		ml_marital_status.marital_status_title,employee.date_of_birth,
		ml_nationality.nationality, ml_religion.religion_title,
		current_contacts.address, current_contacts.home_phone,
		current_contacts.mob_num,current_contacts.email_address,
		current_contacts.office_phone, current_contacts.official_email,
		permanant_contacts.address, permanant_contacts.phone, ml_city.city_name,
		ml_district.district_name, ml_province.province_name,
		emergency_contacts.cotact_person_name, emergency_contacts.home_phone,
		emergency_contacts.mobile_num, ml_relation.relation_name,qualification,
		institute, major_specialization, gpa_score,emp_skills.experience_in_years, ml_designations.designation_name, organization, ml_employment_type.employment_type, emp_experience.from_date,emp_experience.to_date, ml_skill_type.skill_name, emp_skills.experience_in_years, position_management.job_specifications, ml_job_category.job_category_name, ml_department.department_name,ml_posting_location_list.posting_location, ml_shift.shift_name, contract_expiry_date, dependents.dependent_name, ml_relation.relation_name, dependents.date_of_birth,ml_pay_grade.pay_grade, ml_pay_frequency.pay_frequency, currency_name, ml_payrate.payrate, increment_amount, increment_type, ml_increment_type.increment_type, ml_benefit_type.benefit_type_title,allowance_amount, ml_allowance_type.allowance_type, no_of_leaves, ml_leave_type.leave_type, (select employee.full_name from employee where employee_id = reporting_heirarchy.employeed_id) as emp_name, ml_reporting_options.reporting_option')
		
		->from('employee')
			
		->join('allowance', 'employee.employee_id = allowance.employee_id', 'left')
			
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'left')
		
		->join('current_contacts', 'employee.employee_id = current_contacts.employee_id', 'left')
		
		->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'left')
		
		->join('employment', 'employee.employee_id = employment.employment_id', 'left')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'left')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'left')
		
		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'left')
		
		->join('increments', 'employee.employee_id = increments.employee_id', 'left')
		
		->join('qualification', 'employee.employee_id = qualification.employee_id', 'left')
		
		->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'left')
		
		//->join('training', 'employee.employee_id = training.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'left')
		
		->join('reporting_heirarchy', 'employee.employee_id = reporting_heirarchy.employeed_id', 'left')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'left')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'left')
		
		->join('salary', 'employment.employment_id = salary.employement_id', 'left')
		
		->join('dependents', 'employee.employee_id = dependents.employee_id', 'left')
		
		//->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'left')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'left')
		
		->join('ml_marital_status', 'employee.marital_status = ml_marital_status.marital_status_id', 'left')
		
		->join('ml_nationality', 'employee.nationality = ml_nationality.nationality_id', 'left')
		
		->join('ml_religion', 'employee.religion = ml_religion.religion_id', 'left')
		
		->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'left')
		
		->join('ml_district', 'permanant_contacts.district = ml_district.district_id', 'left')
		
		->join('ml_province', 'permanant_contacts.province = ml_province.province_id', 'left')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'left')
		
		->join('ml_reporting_options', 'reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id', 'left')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'left')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'left')
		
		//->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id', 'inner')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'left')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'left')
		
		->join('ml_currency', 'salary.ml_currency = ml_currency.ml_currency_id', 'left')
		
		->join('ml_payrate', 'salary.ml_pay_rate_id = ml_payrate.ml_payrate_id', 'left')
		
		->join('ml_pay_frequency', 'salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'left')
		
		->join('ml_pay_grade', 'salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id', 'left')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id', 'left')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'left')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'left')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'left')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'left')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'left')
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		
	}
	/// Function For Human Resource Task Management
	public function hr_task_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employment.employment_id, employee.employee_id')
		
		->from('employee')

		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		//->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		->where('employment.current = 1 AND position_management.current = 1')
		->group_by('employee.employee_id');
		
		if($title = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $title);
		}
		if($task = $this->input->post('status'))
		{
			$this->datatables->where('position_management.status',$task);
		}
		
		$this->datatables->edit_column('employment.employment_id',
		'<a href="human_resource/view_task_history/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_job/$1"><button class="btn green" style="font-size: 12px;">Assign Task</button></a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/task_progress/$1"><button class="btn green" style="font-size: 12px;">Task Progress</button></a>',
		'employee.employee_id');

        /*$this->datatables->edit_column('employee.employee_id',
            '<a href="human_resource/assign_job/$1"><button class="btn green" style="font-size: 12px;">Task Progress</button></a>',
            'employee.employee_id');*/
		
		return $this->datatables->generate();
	}
	/// Function For Pagination Counting Rows
	public function task_history_count($where)
	{
		$this->db->select('*');
		$this->db->from('assign_job');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
	}
	/// Function For Viewing Task Previous History
	public function view_task_history($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, task_description, task_name, project_title, assign_job.start_date, assign_job.completion_date, status, employee.employee_id')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

        ->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//->group_by('assign_job.assign_job_id');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}		
	}
	/// Function For Human Resource Task Management
	/*public function hr_task_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_assign_task.task_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($title = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $title);
		}
		if($task = $this->input->post('task'))
		{
			$this->datatables->where('assign_job.ml_assign_task_id', $task);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_job/$1"><button class="btn green">Assign New Task</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}*/
	/// Function For Assign Job And Performance Evaluation Details
	public function assign_job_perf_evalue($where)
	{
		$this->db->select('start_date, completion_date, completion_date, task_name, project_title, GROUP_CONCAT(kpi) AS kpi, status, assign_job.assign_job_id, GROUP_CONCAT(ml_kpi_id) as kpi_id, performance_evaluation_id')
		
		->from('assign_job')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		
		->where($where);
		
		$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Retrieving Assign Job And Performance Evaluation Details
	public function assign_job_edit($where)
	{
		$this->db->select('start_date, completion_date, completion_date, assign_job.task_description, assign_job.project_id, assign_job.ml_assign_task_id, task_name, performance_evaluation.ml_kpi_type_id, project_title, kpi, status, assign_job.assign_job_id,assign_job.project_id')
		
		->from('assign_job')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		
		->where($where);
		
		$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	/// Assigned Job History
	public function assign_job_history($where)
	{
		$this->db->select('full_name, employee_code, ml_designations.designation_name, task_name, project_title')
		
		->from('employee')

        ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')

		->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->row();
	}
	public function assign_job_history_wd($where)
	{
		$this->db->select('task_name, project_title')
		
		->from('assign_job')
		
		//->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->where($where);
		
		//$this->db->group_by('job_assignment_id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Performance Evaluation
	public function view_performance()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_department.department_name, employee.employee_id, performance_evaluation.job_assignment_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'left outer')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		
		
		if($department = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $department);
		}
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		//$this->datatables->edit_column('employee.thumbnail', '<img src="upload/Thumb_Nails/$1" style="width:40px; height:40px" />', 'employee.thumbnail');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/performance_evaluation/$1">| <span class="fa fa-eye"></span>  &nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/add_performance_evalution/$1/$2"><button class="btn green">Evaluate</button></a>',
		'employee.employee_id,performance_evaluation.job_assignment_id');
		
		return $this->datatables->generate();		
	}
	
	/// Function For Performance Evaluation
	public function view_performance_evalution()
	{
		
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_department.department_name, employment.employment_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

		->where('employee.enrolled = 1')
		->where('employment.current = 1')

		->group_by('employee.employee_id');
		
		if($department = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $department);
		}
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		/*$this->datatables->edit_column('employee.thumbnail', '<img src="upload/Thumb_Nails/$1" style="width:40px; height:40px" />', 'employee.thumbnail');*/
	$this->datatables->edit_column('employment.employment_id',
		'<a href="human_resource/view_kpi_perf_eval/$1"><span class="fa fa-eye"></span></a>',
		'employment.employment_id');
		
		return $this->datatables->generate();	
		/*$this->datatables->select('employee.employee_code, employee.full_name, kpi, start_date, completion_date, (select avg(percent_achieved) as percent from performance_evaluation where job_assignment_id = assign_job.assign_job_id) as percent_achieved, employee.employee_id, performance_evaluation.job_assignment_id')
		
		->from('employee')
		
		->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'left outer')
		
		//->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner');
		
		$this->db->group_by('performance_evaluation.job_assignment_id');
		
		if($kpi = $this->input->post('kpi'))
		{
			$this->datatables->where('performance_evaluation.ml_kpi_type_id', $kpi);
		}
		
		$this->datatables->edit_column('percent_achieved',
		'<div class="meter orange">
		<span style="width: $1%"></span>
		</div>',
		'percent_achieved');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/add_performance_evalution/$1/$2"><button class="btn green">Evaluate</button></a>',
		'employee.employee_id, performance_evaluation.job_assignment_id');
		
		return $this->datatables->generate();*/
	}
	
	public function annual_review()
	{
		
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_department.department_name, employee.employee_id as employee, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($department = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $department);
		}
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		/*$this->datatables->edit_column('employee.thumbnail', '<img src="upload/Thumb_Nails/$1" style="width:40px; height:40px" />', 'employee.thumbnail');*/
		
		$this->datatables->edit_column('employee',
		'<a href="human_resource/history_annual/$1"><span class="fa fa-eye"></span></a>',
		'employee, performance_evaluation.job_assignment_id');

        $this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/progress_report/$1"><button class="btn green"> Annual Review</button></a>',
		'employee.employee_id, performance_evaluation.job_assignment_id');
		
		return $this->datatables->generate();	
	}

public function view_annual_review($where,$year= NULL)
	{
		$this->db->select('reviewer,comments,submit_date,full_name,employer')
		->from('feedback')
		->join('employee','feedback.employee_id = employee.employee_id','inner')
		->join('ml_year','feedback.submit_date = ml_year.ml_year_id','left');
		//->like('feedback.submit_date');
		$this->db->where($where);
		if($year = $this->input->post('year'))
		{
			$this->db->where('YEAR(feedback.submit_date)',$year);
		}
		$query = $this->db->get();
					
		return $query->result();
	}

	/// Function For Viewing Single Employee KPI's Performance Evaluation
	public function view_kpi_performance($where = NULL)
	{
		$this->db->select('GROUP_CONCAT(kpi) as kpi, start_date, completion_date, (select avg(percent_achieved) as percent from performance_evaluation where job_assignment_id = assign_job.assign_job_id) as percent_achieved, task_name, employee.employee_id, employment.employment_id, performance_evaluation.job_assignment_id,performance_evaluation_id')
		
		->from('employee')

        ->join('employment', 'employment.employee_id = employee.employee_id AND current = 1 AND employment.trashed = 0', 'inner')

		->join('assign_job', 'assign_job.employment_id = employment.employment_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'left outer')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner');
		
		$this->db->where($where);
		
		$this->db->group_by('performance_evaluation.job_assignment_id');
		
		if($task = $this->input->post('tasks'))
		{
			$this->db->where('assign_job.ml_assign_task_id', $task);
		}
		
		if($project = $this->input->post('project'))
		{
			$this->db->where('assign_job.project_id', $project);
		}
		
		$query = $this->db->get();
					
		return $query->result();
	}

	public function view_kpi_performance_print($where)
	{
		$this->db->select('GROUP_CONCAT(kpi) as kpi, start_date, completion_date, (select avg(percent_achieved) as percent from performance_evaluation where job_assignment_id = assign_job.assign_job_id) as percent_achieved, task_name, employee.employee_id, performance_evaluation.job_assignment_id,performance_evaluation_id')

			->from('employee')

            ->join('employment', 'employment.employee_id = employee.employee_id', 'inner')

			->join('assign_job', 'assign_job.employment_id = employment.employment_id', 'inner')

			->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'left outer')

			->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')

			->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
            ->where('employment.current',1);

		$this->db->where($where);
		$this->db->group_by('performance_evaluation.job_assignment_id');
		if($kpi = $this->input->post('tasks'))
		{
			$this->db->where('assign_job.ml_assign_task_id', $kpi);
		}

		if($year = $this->input->post('year'))
		{
			$this->db->like('start_date', $year);
		}
		$query = $this->db->get();
		return $query->result();
	}
	public  function data_for_span_to_print($where)
	{
		$this->db->select('percent_achieved, date, remarks, ml_kpi.kpi, job_assignment_id')
		->from('performance_evaluation')
		->join('ml_kpi','performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id','inner')
		->where($where);
		$query = $this->db->get();

		return $query->result();
	}

	/// Function To View Key Performance Indicators History Wdout Foreach
	public function view_kpi_history_pe($employment_id = NULL)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
		
		->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'left')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'left')
		
		->where($employment_id)
		->where('employment.current = 1');
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->row();
	}
	/// Function To View Key Performance Indicators History Wd Foreach
	public function view_kpi_history_pe_wd($employee_id = NULL)
	{
		$this->db->select('*')
		
		->from('employee')
        ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		->join('assign_job', 'assign_job.employment_id = employment.employment_id', 'inner')
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		->where($employee_id)
        ->where('employment.current = 1');
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	/// Function For Benefits Management
	public function view_benefit_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, benefit_type_title, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
			->where('employment.current',1)

			->group_by('benefits.emp_benefit_id');
		
		if($designation = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $designation);
		}
		if($benefit = $this->input->post('benefit'))
		{
			$this->datatables->where('benefits.benefit_type_id', $benefit);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/add_benefits/$1"><button class="btn green">Allocate Benefits</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	/// Function For Add Benefit to View Added Benefit 
	public function view_benefit($employee_id = NULL)
	{
		$this->db->select('(select full_name from employee where employee_id = benefits.approved_by) as emp_name, date_approved, benefit_type_title, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0', 'inner')
		
		->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')
		
		//->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->join('ml_status', 'benefits.status = ml_status.status_id', 'inner')
		
		->where($employee_id)
		
		->group_by('benefits.emp_benefit_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	public function view_assignd_benefit($employee_id = NULL)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, benefit_type_title, (select full_name from employee where employee_id = "approved_by") as name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0', 'inner')
		
		->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->row();
	}
	public function view_assignd_benefit_wd($employee_id = NULL)
	{
		$this->db->select('benefit_type_title, (select full_name from employee where employee.employee_id = benefits.approved_by) as name, employee.employee_id')
		
		->from('employee')
        ->join('employment', 'employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0', 'inner')

        ->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')

		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id);
		
		//->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	/// Function For Add Benefit to View Dependents
	public function view_dependent($employee_id = NULL)
	{
		$this->db->select('relation_name, dependents.dependent_name, dependents.date_of_birth, benefit_type_title, insurance_cover, benefits.date_rec_created, employee.employee_id, dependents.dependent_id')
		
		->from('employee')

       ->join('employment', 'employee.employee_id = employment.employee_id AND current = 1 AND employment.trashed = 0', 'inner')

       ->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')

      ->join('dependents', 'employee.employee_id = dependents.employee_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')		
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id)
		
		->group_by('dependents.dependent_id');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{					
			return $query->result();
		}
	}
	/// Function For Employee Transfer View
	public function view_emp_transfer()
	{		
		$this->datatables->select("employee_code, full_name, posting_location, designation_name, department_name,date_format(posting.poting_start_date,'%d-%m-%Y'), employee.employee_id",false)
		
		->from('employee')
				
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
				
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		->where('employment.current',1)
		->group_by('employee.employee_id');
		
		if($dept = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $dept);
		}
		 
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		if($loc = $this->input->post('location'))
		{
			$this->datatables->where('posting.location', $loc);
		}
		
		$this->datatables->edit_column('employee.employee_id', 
		'<a href="human_resource/transfer/$1"><button class="btn green">Transfer</button></a>
		 | &nbsp;&nbsp; <a href="human_resource/transfer_history/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		//$this->datatables->edit_column('position_management.approved_by','$1','approved(position_management.approved_by)');
                
		return $this->datatables->generate();
	}
	/// Function For Employee Transfer
	public function view_transfer_detail($where)
	{		
		$this->db->select('full_name, posting_location, city_name, department_name, poting_start_date, posting_end_date, shift_name, branch_name, employment_id ,(select full_name from employee where employee_id = posting.approved_by) as emp_name ,posting.date_approved,posting.duration,approved_by, ml_posting_reason_list.posting_reason')
		
		->from('employment')
				
		->join('employee', 'employment.employee_id = employee.employee_id', 'inner')

		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')

		->join('ml_city', 'posting.ml_city_id = ml_city.city_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_branch', 'posting.ml_branch_id = ml_branch.branch_id', 'inner')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'LEFT')
		
		->where($where);
		
		$query = $this->db->get();
		   
		return $query->result();
	}
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Nadeem =============================///
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	
//============================= work by hamid ==================///
//Deprecated
	public function view_postion_management()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, position_management.job_specifications, status_title, employee.employee_id')
		
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'left')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'left')
		
		//->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'left')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'left')
		->where('employment.current = 1 AND employee.enrolled = 1')
		->group_by('employee.employee_id');
		
		if($designation = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $designation);
		}
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" value="Manage Position" class="btn green"></a>&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href="human_resource/view_position/$1"><span class="fa fa-eye"></span></a>',
		'employee.employee_id');
		
		
		return $this->datatables->generate();
	}	
	
	public function view_position_record($where)
	{
		$this->db->select('*,(select full_name from employee where position_management.approved_by = employee.employee_id) as approved')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		->where($where)
		->group_by('position_management.position_id');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// Function For Manage Position History
	public function emp_mp_history($where)
	{
		$this->db->select('full_name, designation_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->where($where)
		->where('employment.current = 1')
		;
		//->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->row();
	}
	public function emp_mp_history_wd($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name,
		ml_pay_grade.pay_grade, job_specifications, position_management.from_date, position_management.to_date,
		ml_status.status_title,(select full_name from employee where employee_id = position_management.approved_by)
		as emp_name, position_management.approval_date,position_management.position_id,position_management.status')

			->from('employee')

			->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

			->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

			->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

			->join('ml_pay_grade', 'ml_pay_grade.ml_pay_grade_id = position_management.ml_pay_grade_id', 'inner')

			->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')

			->where($where)

			->group_by('position_management.position_id');

		$query = $this->db->get();

		return $query->result();

	}
	public function view_postion()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, status_title, employee.employee_id')

		->from('employee')

		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')

		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

		//->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')

		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')

		//->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')

		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')

		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')

		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" class="btn green"></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	public function manage_postion($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name,
		ml_pay_grade.pay_grade, job_specifications, position_management.from_date,
		ml_status.status_title,(select full_name from employee where employee_id = position_management.approved_by)
		as emp_name, position_management.approval_date,position_management.ml_pay_grade_id,position_management.ml_designation_id')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_pay_grade', 'ml_pay_grade.ml_pay_grade_id = position_management.ml_pay_grade_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->where($where)
		
		->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->row();
				 
	}
public function retirement()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id,posting.posting_id')

				->from('employee')

				->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

				->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

				->join('posting', 'employment.employment_id = posting.employement_id', 'inner')

				->join('ml_branch','ml_branch_id = posting.ml_branch_id AND posting.trashed = 0','left')

				->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')

				->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

				->where('position_management.current AND posting.current = 1 AND employee.trashed = 0')

				->group_by('employee.employee_id');

		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		//$this->datatables->edit_column('posting.enabled',
		//'$1',
		//'hr_retire(posting.enabled)');
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/terminate/$1/$2"><input type="submit" name="Discharge" value="Discharge" class="btn green" /></a>',
		'employee.employee_id,posting.posting_id');

		return $this->datatables->generate();
	}
	
	public function view_retire_record($where)
	{
		$this->db->select('*')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('seperation_management', 'employee.employee_id = seperation_management.employee_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('ml_pay_grade', 'position_management.ml_pay_grade_id = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		->join('ml_separation_type', 'seperation_management.ml_seperation_type = ml_separation_type.separation_type_id', 'inner')
		
		//->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		->where($where)
		->group_by('seperation_management.seperation_id');
		$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->result();
	}
	}
	public function terminate_employees()
	{
		$this->db->select('employee.full_name,contract.comments,contract.contract_expiry_date')
		
		->from('employee')
		
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner');
		
		$query = $this->db->get();
		
		return $query->result();
	}	
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Hamid Ali ===============================////
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	public function view_progress($id)
	{
		$this->db->select('ml_kpi.kpi, performance_evaluation.percent_achieved, performance_evaluation.date, full_name, employee.employee_id')
		->from('employee')
        ->join('employment','employment.employee_id = employee.employee_id')
        ->join('assign_job','assign_job.employment_id = employment.employment_id')
        ->join('performance_evaluation','performance_evaluation.job_assignment_id = assign_job.assign_job_id')
		->join('ml_kpi','performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id')
		->where('employee.employee_id',$id);
		//->join('ml_district','current_contacts.District = ml_district.district_id');
		//$this->db->group_by('ml_kpi.ml_kpi_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	public function detail($id)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name')
		->from('employee')
		->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		//->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		->where('employee.employee_id',$id);
		//->join('ml_district','current_contacts.District = ml_district.district_id');
		//$this->db->group_by('ml_kpi.ml_kpi_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	///////////////////////////////////////////////////////////////////////// AGAIN BY ME HAMID ALI FROM QASMI MARDAN 03449470064////////////


	public function record_report_view_table2($where)
	{
		$this->db->select('employee.full_name,
		ml_reporting_options.reporting_option,
		employee.employee_id,
		reporting_heirarchy.report_heirarchy_id,
		reporting_heirarchy.reporting_authority_id AS SubOrdinateID')
			->from('reporting_heirarchy')
			->join('employee','reporting_heirarchy.reporting_authority_id = employee.employee_id')
			->join('ml_reporting_options','reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id')
			->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}


	public function record_report_view_table($where)
	{
		$this->db->select('employee.full_name,ml_reporting_options.reporting_option,employee.employee_id,reporting_heirarchy.report_heirarchy_id')
		->from('reporting_heirarchy')
		->join('employee','reporting_heirarchy.employeed_id = employee.employee_id')
		->join('ml_reporting_options','reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id')
		->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	public function base_salary($where)
	{
	$this->db->select('ml_pay_frequency.pay_frequency,SUM(increments.increment_amount) AS Increments, ml_currency.currency_name,ml_payrate.payrate,base_salary,date_approved,(select full_name from employee where employee.employee_id = salary.created_by) as creater,salary.date_created,(select full_name from employee where employee.employee_id = salary.approved_by) as apprved,ml_status.status_title,employee.employee_id')
	->from('salary')
	->join('employment','salary.employement_id = employment.employment_id')
	->join('employee','employment.employee_id = employee.employee_id')
	->join('ml_pay_grade','salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id')
	->join('ml_pay_frequency','salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id')
	->join('ml_currency','salary.ml_currency = ml_currency.ml_currency_id')
	->join('ml_payrate','salary.ml_pay_rate_id = ml_payrate.ml_payrate_id')
	->join('ml_status','salary.status = ml_status.status_id')
	->join('increments','increments.employment_id = employment.employment_id','left')
	->where($where)
	->where('increments.trashed = 0 AND increments.status = 2 AND salary.status = 2 AND salary.trashed = 0');
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}

	public function task_view_for($where)
	{
		$this->db->select("ml_projects.project_title,ml_assign_task.task_name,start_date,completion_date,status")
		->from('assign_job')
		->join('ml_projects','assign_job.project_id = ml_projects.project_id')
		->join('ml_assign_task','assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id')
		->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
    public function retired_employees()
    {
        $this->datatables->select('
        employee.employee_code,
         employee.full_name, ml_designations.designation_name,
        ml_separation_type.separation_type,date_format(date
,"%d-%m-%Y"),ml_posting_reason_list.posting_reason,
        seperation_management.employee_id',false)

            ->from('seperation_management')

            ->join('employee', 'seperation_management.employee_id = employee.employee_id', 'inner')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('posting', 'posting.employement_id = employment.employment_id', 'inner')

            ->join('ml_posting_reason_list','seperation_management.reason = ml_posting_reason_list.posting_reason_list_id','inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            ->join('ml_separation_type', 'seperation_management.ml_seperation_type = ml_separation_type.separation_type_id', 'inner')

            ->where('posting.current = 0 AND position_management.current = 0 AND employment.current = 0 AND seperation_management.rejoin = 0 AND employee.enrolled = 0')

            ->group_by('seperation_management.seperation_id');

      /*  if($design = $this->input->post('designation'))
        {
            $this->datatables->where('position_management.ml_designation_id', $design);
        }*/

        //$this->datatables->edit_column('posting.enabled',
        //'$1',
        //'hr_retire(posting.enabled)');
       /* $this->datatables->add_column('view_profile',
            '<a href="human_resource/view_retired_profile/$1"><span class="fa fa-eye"></span></a>');

		$this->datatables->add_column('separation_details',
            '<a href="human_resource/view_retire/$1"><span class="fa fa-eye"></span></a>');*/
		$this->datatables->edit_column('seperation_management.employee_id',
			'&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/view_retired_profile/$1"><span class="fa fa-eye"></span></a> &nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/view_retire/$1"><span class="fa fa-eye"></span></a>',
			'seperation_management.employee_id');


        return $this->datatables->generate();
    }

    public function extend()
    {
        $this->datatables->select('employee.thumbnail, employee.employee_code,
        employee.full_name, ml_designations.designation_name,
        ml_department.department_name,date_format(contract.contract_expiry_date
,"%d-%m-%Y"), employee.employee_id',false)

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1', 'inner')

			->join('contract','employment.employment_id = contract.employment_id','inner')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('posting', 'employment.employment_id = posting.employement_id', 'inner')

            ->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            ->where('position_management.current = 1 AND posting.current = 1 AND employee.enrolled = 1')

            ->group_by('employee.employee_id');
		$this->db->order_by("contract_expiry_date", "asc");

        if($department = $this->input->post('department'))
        {
            $this->datatables->where('posting.ml_department_id', $department);
        }
        if($design = $this->input->post('designation'))
        {
            $this->datatables->where('position_management.ml_designation_id', $design);
        }

//        $this->datatables->edit_column('employee.thumbnail', '<img src="upload/Thumb_Nails/$1" style="width:40px; height:40px" />', 'employee.thumbnail');

        $this->datatables->edit_column('employee.employee_id',
            '<a href="human_resource/extend_employement/$1"><button class="btn green">Extend</button></a>',
            'employee.employee_id');

        return $this->datatables->generate();
    }
    public function rejoin()
    {
        $this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name,
        date_format(date,"%d-%m-%Y") as date,seperation_management.note, employee.employee_id',false)

            ->from('seperation_management')

            ->join('employee', 'seperation_management.employee_id = employee.employee_id', 'inner')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('posting', 'posting.employement_id = employment.employment_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            ->where('posting.current = 0 AND position_management.current = 0  AND seperation_management.rejoin = 0 AND employee.enrolled = 0')

            ->group_by('seperation_management.employee_id');

        if($design = $this->input->post('designation'))
        {
            $this->datatables->where('position_management.ml_designation_id', $design);
        }

        //$this->datatables->edit_column('posting.enabled',
        //'$1',
        //'hr_retire(posting.enabled)');
        $this->datatables->edit_column('employee.employee_id',
            '<a href="human_resource/view_retired_profile/$1"><span class="fa fa-eye"></span></a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;|&nbsp;&nbsp;&nbsp;<a href="human_resource/add_employment_rejoin/$1">Rejoin</a>',
            'employee.employee_id');

        return $this->datatables->generate();
    }

    public function project_history()
    {
        $this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id')


            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            //->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            //->join('ml_status', 'position_management.status = ml_status.status_id', 'inner')

            ->group_by('employee.employee_id');



        $this->datatables->edit_column('employee.employee_id',
           '<a href="human_resource/history_project/$1"><span class="fa fa-eye"></span></a>',
           'employee.employee_id');


        return $this->datatables->generate();
    }
	public function join_for_history_skha($where)
	{
		$this->db->select("ml_department.department_name, ml_projects.project_title, employee_project.project_start_date, employee_project.project_end_date, employee_project.project_duration, employee_project.project_status")
			->from('employee_project')
			->join('employee','employee_project.employee_id = employee.employee_id','inner')
			->join('employment','employee.employee_id = employment.employee_id','inner')
			->join('position_management' ,'employment.employment_id = position_management.employement_id', 'inner')
			->join('posting','employment.employment_id = posting.employement_id','inner')
			->join('ml_department','posting.ml_department_id = ml_department.department_id','inner')
			->join('ml_projects','employee_project.project_name = ml_projects.project_id','inner')
			->group_by('employee_project.employee_project_id')
			->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
	public function contract_history_join_rpeat($where)
	{
			$this->db->select("*")
			->from('contract')
			->join('ml_employment_type','contract.employment_type = ml_employment_type.employment_type_id')
			->join('ml_job_category','contract.employment_category = ml_job_category.job_category_id')
			->where($where);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->row();
			}

	}
	public function getting_all_joing_to_project($where)
	{
		$this->db->select("*")
			->from('employee_project')
			->join('ml_projects','employee_project.project_name = ml_projects.project_id','inner')
			->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	public function join_for_project($where)
	{
		$this->db->select("ml_projects.project_id, project_title")
			->from('employee_project')
			->join('ml_projects','employee_project.project_id = ml_projects.project_id','INNER')
		->where($where)
        ->where('employee_project.current',1);

		$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
	public function insurance_employee($where)
	{
		$this->db->select("certificate_number,policy_number, effective_from, ml_insurance_type.insurance_type_name, employee_insurance_id")
			->from('employee_insurance')
			->join('ml_insurance_type','employee_insurance.ml_insurance_id = ml_insurance_type.insurance_type_id','inner')
			->join('employment','employee_insurance.employment_id = employment.employment_id','inner')
			->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
	public function get_last_id_desc_where($table,$where,$id)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($id,'desc');
		$this->db->limit(1);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

//////////// Function for employee info

    public function employee_info($where)
    {
        $this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, employee.employee_id')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('emp_skills', 'employee.employee_id = emp_skills.employment_id', 'left')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
            ->where($where)
            ->where('employment.current = 1');
//->group_by('employee.employee_id');

        $query = $this->db->get();

        return $query->row();


    }

	public function select_task_progress_where($table,$where){
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}

	}
    ////////// Function for task progress info of milestones and tasks
    public function tasks_info($where,$task = NULL)
    {
       $this->db->select('employee.employee_id,milestone.milestones,milestone.estimated_date,
       task_progress.reported_date,(select full_name from employee where employee_id=task_progress.reported_by) as name,task_progress.percent_achieved,
       task_progress.remarks,task_progress.tp_id,milestone.id,ml_assign_task.task_name')
       ->from('employee')
       ->join('employment', 'employee.employee_id = employment.employee_id AND employment.trashed = 0','INNER')
       ->join('assign_job','employment.employment_id= assign_job.employment_id','INNER')
       ->join('ml_assign_task','assign_job.ml_assign_task_id=ml_assign_task.ml_assign_task_id','INNER')
       ->join('milestone','ml_assign_task.ml_assign_task_id=milestone.task_id','INNER')
       ->join('task_progress','milestone.id=task_progress.milestone_id','LEFT')
       ->where($where)
       ->where('employment.current',1);
        if($task)
        {
            $this->db->where('milestone.task_id',$task);
        }
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    ///END

    ////////// Function for task progress info of milestones and tasks
    public function tasks_info_where($where,$task = NULL)
    {
        $this->db->select('*')
            ->from('employee')
            ->join('employment', 'employee.employee_id = employment.employee_id AND employment.trashed = 0','INNER')
            ->join('assign_job','employment.employment_id= assign_job.employment_id','INNER')
            ->join('ml_assign_task','assign_job.ml_assign_task_id=ml_assign_task.ml_assign_task_id','INNER')
            ->join('milestone','ml_assign_task.ml_assign_task_id=milestone.task_id','INNER')
            ->join('task_progress','milestone.id=task_progress.milestone_id','LEFT')
            ->where($where)
            ->where('employment.current',1);
        if($task)
        {
            $this->db->where('milestone.task_id',$task);
        }
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

	public function find_projects_for_employee($where)
	{
		$this->db->select('ml_projects.project_title,employee_project.project_id,employee_project.employee_project_id')
			->from('employee_project')
			->join('ml_projects','employee_project.project_id = ml_projects.project_id','inner')
			->where($where);

			$query=$this->db->get();
        if($query->num_rows() > 0){
			return $query->result();
		}
	}
    ///END

    public function qualificationApplicant($where)
    {
        $this->db->select('title,ml_qualification_type.qualification_title,institute,specialization,comp_year,gpa_division')
            ->from('applicant_qualification')
            ->join('applicants','applicants.applicantID = applicant_qualification.applicant_id','INNER')
            ->join('ml_qualification_type','applicant_qualification.qualification_type_id = ml_qualification_type.qualification_type_id','INNER')
            ->where($where)
            ->where('applicant_qualification.trashed = 0');

        $query=$this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
    }



    public function skiilApplicant($where = NULL)
    {
        $this->db->select('ml_skill_type.skill_name,applicant_skills.dateAdded,ml_skill_level.skill_level')
            ->from('applicants')
            ->join('applicant_skills','applicants.applicantID = applicant_skills.applicantID','inner')
            ->join('ml_skill_type','ml_skill_type.skill_type_id = applicant_skills.SkillID','inner')
            ->join('ml_skill_level','ml_skill_level.level_id = applicant_skills.skill_level','inner')
            ->where($where)
        ->where('applicant_skills.trashed = 0');

        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }

    }

    // Function for AppliedForJob //
    public function appliedforJob($where = NULL)
    {
        $this->db->select('
        job_advertisement.title,
        applicant_applied_jobs.dateAppliedOn,
        ml_applicant_status_types.statusTitle,
        job_advertisement.expiryDate,
        ml_applicant_status_types.applicantStatusTypeID,
        applicant_applied_jobs.appAppliedJobID,
        ml_jobad_type.Title,
        job_advertisement.JobAD_ID,
        applicant_applied_jobs.responseStatus')
            ->from('applicants')
            ->join('applicant_applied_jobs','applicant_applied_jobs.applicantID = applicants.applicantID','INNER')
            ->join('job_advertisement','applicant_applied_jobs.jobAdvertisementID = job_advertisement.advertisementID AND job_advertisement.trashed = 0','INNER')
            ->join('ml_applicant_status_types','ml_applicant_status_types.applicantStatusTypeID = applicant_applied_jobs.responseStatus AND applicant_applied_jobs.trashed = 0','INNER')
            ->join('ml_jobad_type','ml_jobad_type.JobAD_ID = job_advertisement.JobAD_ID AND ml_jobad_type.Trashed = 0','INNER')
            ->where($where)
            ->where('applicant_applied_jobs.trashed = 0');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function ApplicantExperience($where = NULL)
    {
        $this->db->select('applicant_experience.Exp_position,applicant_experience.Company,applicant_experience.FromDate,applicant_experience.ToDate,applicant_experience.DateCreated')
            ->from('applicants')
            ->join('applicant_experience','applicants.applicantID = applicant_experience.ApplicantID AND applicant_experience.Trashed = 0','inner')
            ->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    // Function for fetch shortlist Applicants Emails
    public function fetch_ShortListApplicants_email($ids){
   $query = $this->db->query("SELECT group_concat(DISTINCT Appl.officialEmailAddress) as Email,group_concat(DISTINCT Appl.applicantID) as ApplicantID FROM (`applicants` AS Appl) INNER JOIN `applicant_applied_jobs` APDJOB ON `Appl`.`applicantID` = `APDJOB`.`applicantID` WHERE `APDJOB`.`trashed` = 0 AND `Appl`.`applicantID` IN ($ids)");
if($query->num_rows() > 0)
{
    return $query->row();
}
    }


/// End of Hr_Model Class   
}
?>