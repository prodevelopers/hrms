<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_Model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
//********************************* ************ *****************************************//
//================================= CONFIGRATION =========================================//
///******************************** ************ ****************************************//

////////////////////////FUNCTION FOR NOTIFICATION /////////////////////////////////////////
public function configration()
{
	$this->datatables->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_approval_types.approval_types,ml_approval_authorities.enabled,ml_approval_authorities.approval_auth_id,employee.employee_id')
	->from('employee')
	->join('ml_approval_authorities', 'ml_approval_authorities.employee_id = employee.employee_id')
	->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
	->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
	->join('ml_approval_types', 'ml_approval_types.ml_approval_types_id = ml_approval_authorities.ml_approval_type_id')
	->where('ml_approval_authorities.trashed = 0');
	//->group_by('employee.employee_id')
	$this->datatables->edit_column('ml_approval_authorities.enabled','$1','check_status(ml_approval_authorities.enabled)');
	$this->datatables->edit_column('ml_approval_authorities.approval_auth_id',
			'<a href="config_site/edit_authrity/$1"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="config_site/delete_authrity/$1"><span class="fa fa-trash-o"></span></a>',
			'ml_approval_authorities.approval_auth_id');
			if($approval = $this->input->post('approval_types'))
			{
				$this->datatables->where('ml_approval_types.ml_approval_types_id', $approval);
			}
			if($Designations = $this->input->post('designation_name'))
			{
				$this->datatables->where('ml_designations.ml_designation_id', $Designations);
			}
	
                 return $this->datatables->generate();
}
	
	public function edit_configration($where)
{
	$this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,ml_designations.ml_designation_id,ml_approval_types.approval_types,ml_approval_types.ml_approval_types_id,ml_approval_authorities.enabled,ml_approval_authorities.approval_auth_id,employee.employee_id')
	->from('employee')
	->join('ml_approval_authorities', 'ml_approval_authorities.employee_id = employee.employee_id')
	->join('employment', 'employee.employee_id = employment.employee_id', 'inner')
	->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
	->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
	->join('ml_approval_types', 'ml_approval_types.ml_approval_types_id = ml_approval_authorities.ml_approval_type_id')
	->where($where);
	//->group_by('employee.employee_id')	
	 $query = $this->db->get();
    	if($query->num_rows() > 0)
		{
         	return $query->row();
         }
	
}


 /////////////////////////////////////////// Function for Auto Complete Time Sheet ////////////////////////////////
	 
	 
	 
	 /// Auto complete for hourly time sheet /////
	public function get_autocomplete_approval_authority()
	{	

		$name = $this->input->post('queryString');
	$query = $this->db->query("SELECT full_name, employee_code, ml_designations.designation_name,employee.employee_id
FROM employee
        INNER JOIN employment ON employee.employee_id=employment.employment_id	
 
INNER JOIN position_management ON employment.employment_id=position_management.employement_id 

INNER JOIN ml_designations ON position_management.position_id=ml_designations.ml_designation_id
WHERE full_name like '$name%'");
	//$this->db->like('full_name', $n, 'after');
	
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	// END
	
	public function config_new_record($tbl,$data,$enbl)
	{
		$this->db->insert();
	}
	
	


//********************************* ***** ********* *****************************************//
//================================= USER MANAGEMENT =========================================//
///******************************** ***** ********* ****************************************//	

/////////////////////////////////////////FUNCTION FOR VIEW GROUP PRIVILIGIES //////////////////////
	public function view_group_priviligies()
	{
		$this->db->select('user_groups_privileges.user_group_id,user_groups_privileges.privilege_id,user_groups_privileges.ml_module_list_id,user_groups_privileges.trashed,ml_user_groups.user_group_title,privileges.view_only,privileges.modify,privileges.create_new_record,privileges.print,ml_module_list.module_name,user_groups_privileges.user_group_privilege_id')
		->from('user_groups_privileges')
		->join('ml_user_groups','ml_user_groups.user_group_id=user_groups_privileges.user_group_id','inner')
		->join('ml_module_list','ml_module_list.ml_module_id=user_groups_privileges.ml_module_list_id','inner')
		->join('privileges','privileges.user_privilege_id=user_groups_privileges.privilege_id','inner');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
/////////////////////////END OF FUNCTION /////////////////////////////////////////////////////
////////////////////FUNCTION FOR GROUP JOIN BY WHERE /////////////////////////////////////////
	public function group_information($where)
	{
		$this->db->select('*')
		->from('ml_user_groups')
		->join('user_account','user_account.user_group=ml_user_groups.user_group_id','inner')
		->join('employee','user_account.employee_id=employee.employee_id','inner')
		->join('user_groups_privileges','user_groups_privileges.user_group_id=ml_user_groups.user_group_id','inner')
		->join('privileges','user_privilege_id=user_groups_privileges.privilege_id','inner')
		->where($where);
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
///////////////////////////END OF FUNCTION //////////////////////////////////////////////////////////////
/////////////////////////////FUNCTION FOR VIEW JOIN RECORDS ////////////////////////////////////////////
	public function view_record()
	{
		$this->db->select('employee_code,full_name,user_name,user_group_title,privileges.user_privilege_id')
		->from('employee')
		->join('user_account','user_account.employee_id=employee.employee_id','inner')
		->join('ml_user_groups','user_group_id=user_account.user_group','inner')
		->join('user_groups_privileges','user_groups_privileges.user_group_id=ml_user_groups.user_group_id','inner')
		->join('privileges','user_privilege_id=user_groups_privileges.privilege_id','inner');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		};
	
	}
///////////////////////END OF FUNCTION ////////////////////////////////////////////////////////////////////////
///////////////////// FUNCTION FOR VIEW USER INFORMATION JOIN BY WHERE ///////////////////////////////////////
	public function user_infromation($where)
	{
		$this->db->select('employee_code,full_name,user_name,user_group_title,user_account.enabled,user_account.password,employee.employee_id')
		->from('employee')
		->join('user_account','user_account.employee_id=employee.employee_id','inner')
		->join('ml_user_groups','user_group_id=user_account.user_group','inner')
		->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		};
	
	}
/////////////////////////////END OF FUNCTION ///////////////////////////////////////////////////////////////
/////////////////FUNCTION FOR VIEW USER INFORMATION JOIN FOR DATATABLES/////////////////////////////////////
public function view_user_infromation()
{
	$this->datatables->select('employee_code,full_name,user_name,user_group_title,user_account.enabled,user_account.user_account_id')
	->from('employee')
	->join('user_account','user_account.employee_id=employee.employee_id','inner')
	->join('ml_user_groups','user_group_id=user_account.user_group','inner');
	if ($user= $this->input->post('user_name'))

	{
		$this->datatables->where('user_account.user_account_id',$user);
	}
	if ($group= $this->input->post('user_group_title'))
	{
		$this->datatables->where('user_account.user_group',$group);
	}
	$this->datatables->edit_column('user_account.enabled', '$1','check_status(user_account.enabled)', 'user_account.enabled');
	$this->datatables->edit_column('user_account.user_account_id',
			'<a href="user_site/edit_user_management/$1"><span class="fa fa-pencil"></span></a>','user_account.user_account_id');
		
		return $this->datatables->generate();
}	

///////////////////////////END OF FUNCTION //////////////////////////////////////////////////////////
//********************************* ***** ********* *****************************************//
//================================= END USER MANAGEMENT =========================================//
///******************************** ***** ********* ****************************************//	
	public function drop_down() 
	{
  		$rows = $this->db->select('*')
		->from('ml_user_groups')
		->get()
		->result();
		$drop_down = array('' => '-- Select User Group --');
		foreach($rows as $row)
		{
			$drop_down[$row->user_group_id] = $row->user_group_title;
		}
  		return $drop_down;
	}
	
	public function drop_down2() 
	{
  		$rows = $this->db->select('*')
		->from('employee')
		->get()
		->result();
		$drop_down = array('' => '-- Select Employee Code --');
		foreach($rows as $row)
		{
			$drop_down[$row->employee_id] = $row->employee_code;
		}
  		return $drop_down;
	}

	public function drop_down4() 
	{
		$rows = $this->db->select('*')
		->from('user_account')
		->get()
		->result();
		$drop_down = array('' => '-- Select User Name --');
		foreach($rows as $row)
		{
			$drop_down[$row->user_account_id] = $row->user_name;
		}
  		return $drop_down;
	}
	
	public function drop_down3() 
	{
		$rows = $this->db->select('*')
		->from('ml_leave_type')
		->get()
		->result();
		$drop_down = array('' => '-- Select Leave Type --');
		foreach($rows as $row)
		{
			$drop_down[$row->ml_leave_type_id] = $row->leave_type;
		}
  		return $drop_down;
	}

	public  function html_select_box($table,$key,$value) 
	{
  		$this->db->select('*');
  		$query = $this->db->get($table);
  		if($query->num_rows() > 0) 
		{
			$data[''] = '--Select Option--';
			foreach($query->result() as $row) 
			{
				$data[$row->$key] = $row->$value;
   	 		}
			return $data;
 		}
	}        
        
	public  function html_selectbox($table,$option,$key,$value) 
	{
		$this->db->select('*');
  		$query = $this->db->get($table);
  		if($query->num_rows() > 0) 
		{
			$data[''] = $option;
			foreach($query->result() as $row) 
			{
				$data[$row->$key] = $row->$value;
   	 		}
			return $data;
 		}
	}        

	public function get_dept_dropdown()
	{
		$query = $this->db->get('department');
		if($query->num_rows() > 0) 
		{
			return  $query;
 		}
   	}
	public function search_username($where)
	{
		return $query = $this->db->get_where('user_management', array('employee_name'=> $where))->result();
	}

       
	public function fetch_user($id)
	{
		$this->db->select('*')->from('user');
		$this->db->where('dept_id',$id);
		$query = $this->db->get();
		if($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				$data[$row->user_id] = $row->user_name;
            }
			return $data;
		}else{
			return array();
        }
	}
        
	public function do_upload($file_name) 
	{
		$config['upload_path'] = 'upload/';
		$config['allowed_types'] = 'jpg|png|gif';
		$config['max_size'] = '10000000000';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($file_name))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
            
			return $data;
        }
	}         
         
	public function upload_pic($image) 
	{
		$config['upload_path'] = 'images/';
		$config['allowed_types'] = 'jpg|png|gif';
		$config['max_size'] = '10000000000';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($image))
		{
			return false;
        }else{
			$data = array('upload_data' => $this->upload->data());
			
			return $data;
        }
	}        

	public function get_row_by_id($table,$where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	}
        
	public function get_row_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
        }
	} 
	
	public function get_num_rows_by_where($table,$where)
	{
		$this->db->select('*')->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
        }
	}

	public function create_new_record($table,$data)
	{
		$query = $this->db->insert($table, $data);
		if($query)
		{
			return true;
        } else{
			return false;
        }
	}

	public function insert_data_get_last_id($table,$data)
	{
		$query = $this->db->insert($table, $data);
		
		return $this->db->insert_id();
	}

	public function get_last_id() 
	{
		return $this->db->insert_id();
	}
       
	public function delete_row_by_where($table,$where)
	{
		$this->db->where($where);
		$query = $this->db->delete($table);
		if($query)
		{
			return true;
        }
	}		
             
	public function update_record($table,$where,$data)
	{
		$this->db->where($where);
		$query = $this->db->update($table,$data);
		if($query)
		{
	        return true;
		}else{
			return false;
		}
	}

	public  function get_all($table)
	{
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	public function get_all_by_where($table,$where)
	{
		$this->db->select('*');
	    $this->db->where($where);
	    $query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	public  function count_by_where($table,$where)
	{
		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
        
	public function redirect_to($location = NULL)
	{
		if($location != '')
		{
			redirect($location); 
		}    
	}
        public function chk_employee_code($where = NULL)
		{
			$this->db->where($where);
            $query = $this->db->get('employee');
			if($query->num_rows() > 0)
			{
				return true;
			}else{
				return false;
			}  
        }
	/// Function to Check CNIC Duplicate Insertion
	function check_nic_already_exists($where)
	{
		$this->db->where($where);
		$query = $this->db->get('employee');
		if($query->num_rows() > 0)
		{
			return true;
		}else{
			return false;
		}
	}
	/// ************************* ********* ******* ** ****** ***********************************///
	//// ======================== Functions Created By Nadeem =================================////
	/// ************************* ********* ******* ** ****** ***********************************///
	/// Dynamic Function For Drop Down With Select Option
	public  function dropdown_wd_option($table, $option, $key, $value) 
	{
		$this->db->select('*');
  		
		$query = $this->db->get($table);
  		
		if($query->num_rows() > 0) 
		{
			$data['0'] = $option;
			foreach($query->result() as $row) 
			{
					$data[$row->$key] = $row->$value;
			}
			return $data;
		}
	}
	/// Function For Selecting Employee Emergency Contact Details
	public function emergency_contact_detail($employee_id)
	{
		$this->db->select('cotact_person_name, relation_name, home_phone, mobile_num, work_phone, emergency_contact_id')
		
		->from('emergency_contacts')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Qualification Details
	public function qualification_detail($employee_id)
	{
		$this->db->select('qualification_id, employee_id, qualification_title, qualification, institute, year')
		
		->from('qualification')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Experience Details
	public function emp_experience_detail($employee_id)
	{
		$this->db->select('organization, designation_name, from_date, to_date, experience_id')
		
		->from('emp_experience')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_designations', 'emp_experience.job_title = ml_designations.ml_designation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Skills Details
	public function emp_skill_detail($employee_id)
	{
		$this->db->select('skill_name, experience_in_years, skill_record_id')
		
		->from('emp_skills')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Training Details
	public function emp_training_detail($employee_id)
	{
		$this->db->select('training_name, date, description')
		
		->from('training')
		
		//->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Dependents Details
	public function emp_dependent_detail($employee_id)
	{
		$this->db->select('dependent_name, relation_name, date_of_birth, dependent_id')
		
		->from('dependents')
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Icrements Details
	public function entitle_increment_detail($employee_id)
	{
		$this->db->select('increment_amount, increment_type, increment_id')
		
		->from('increments')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Allowance Details
	public function entitle_allowance_detail($employee_id)
	{
		$this->db->select('allowance_amount, allowance_type, allowance_id')
		
		->from('allowance')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Benefit Details
	public function entitle_benefit_detail($employee_id)
	{
		$this->db->select('date_effective, benefit_type_title, emp_benefit_id')
		
		->from('benefits')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Selecting Employee Entitlement Leave Details
	public function entitle_leave_detail($employee_id)
	{
		$this->db->select('no_of_leaves, leave_type, leave_entitlement_id')
		
		->from('leave_entitlement')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'inner')
		
		->where($employee_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sup_auto_search()
	{
		$this->db->select('employee_id, full_name');
		
		$this->db->from('employee');
		
		$this->db->like('full_name', $this->input->post('queryString'), 'after');
		
		$this->db->limit(5);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Employee Report AutoSearch
	public function emp_report_sub_auto_search()
	{
		$this->db->select('employee_id, full_name');
		
		$this->db->from('employee');
		
		$this->db->like('full_name', $this->input->post('queryString'), 'after');
		
		$this->db->limit(5);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For All Employee List
	public function view_all_employee()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, user_account.enabled, employee.employee_id')
		
		->from('employee')
		
		->join('user_account', 'employee.employee_id = user_account.employee_id', 'inner')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($job_title = $this->input->post('job_title'))
		{
			$this->datatables->where('emp_experience.ml_employment_type_id', $job_title);
		}
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		$this->datatables->edit_column('user_account.enabled', '$1', 'hr_status(user_account.enabled)');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/edit_employee_acct/$1"><span class="fa fa-pencil"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp; <a href="human_resource/view_employee_profile/$1"><span class="fa fa-eye"></span></a> &nbsp;&nbsp;| &nbsp; &nbsp; <span class="fa fa-print" onclick="print_report($1)" style="cursor:pointer"></span></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();		
	}
	/// Function to View Complete Profile of Employee
	public function view_complete_profile($where)
	{
		$this->db->select('*')
		
		->from('employee')
				
		->join('current_contacts', 'employee.employee_id = current_contacts.employee_id', 'inner')
		
		->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'inner')
		
		->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'inner')	
		
		->join('qualification', 'employee.employee_id = qualification.employee_id', 'inner')
		
		->join('emp_skills', 'employee.employee_id = emp_skills.employee_id', 'inner')
		
		->join('training', 'employee.employee_id = training.employee_id', 'inner')
		
		->join('increments', 'employee.employee_id = increments.employee_id', 'inner')
		
		->join('allowance', 'employee.employee_id = allowance.employee_id', 'inner')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('reporting_heirarchy', 'employee.employee_id = reporting_heirarchy.employeed_id', 'inner')		
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('salary', 'employment.employment_id = salary.employement_id', 'inner')
		
		->join('dependents', 'employee.employee_id = dependents.employee_id', 'inner')
		
		->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_marital_status', 'employee.marital_status = ml_marital_status.marital_status_id', 'inner')
		
		->join('ml_nationality', 'employee.nationality = ml_nationality.nationality_id', 'inner')
		
		->join('ml_religion', 'employee.religion = ml_religion.religion_id', 'inner')		
		
		->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'inner')
		
		->join('ml_district', 'permanant_contacts.district = ml_district.district_id', 'inner')
		
		->join('ml_province', 'permanant_contacts.province = ml_province.province_id', 'inner')
		
		->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')
		
		->join('ml_reporting_options', 'reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id', 'inner')
		
		->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'inner')
		
		->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'inner')
		
		->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'inner')		
		
		->join('ml_currency', 'salary.ml_currency = ml_currency.ml_currency_id', 'inner')
		
		->join('ml_payrate', 'salary.ml_pay_rate_id = ml_payrate.ml_payrate_id', 'inner')
		
		->join('ml_pay_frequency', 'salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'inner')
		
		->join('ml_pay_grade', 'salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id', 'inner')
		
		->join('ml_qualification_type', 'qualification.qualification_type_id = ml_qualification_type.qualification_type_id', 'inner')
		
		->join('ml_employment_type', 'emp_experience.ml_employment_type_id = ml_employment_type.employment_type_id', 'inner')
		
		->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
		
		->where($where);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		
	}
	/// Function For Human Resource Task Management
	public function hr_task_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_job/$1"><button class="btn green">Assign Task</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	/// Function For Assign Job And Performance Evaluation Details
	public function assign_job_perf_evalue($where)
	{
		$this->db->select('start_date, completion_date, completion_date, task_name, project_title, kpi')
		
		->from('assign_job')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner')
		
		->where($where);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	/// Function For Performance Evaluation
	public function view_performance_evalution()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, task_name, start_date, completion_date, performance_evaluation.percent_achieved, performance_evaluation.job_assignment_id')
		
		->from('employee')
		
		->join('assign_job', 'employee.employee_id = assign_job.employee_id', 'inner')
		
		->join('performance_evaluation', 'assign_job.assign_job_id = performance_evaluation.job_assignment_id', 'inner')
		
		->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
		
		->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		
		->join('ml_kpi', 'performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id', 'inner');
		
		$this->db->group_by('performance_evaluation.job_assignment_id');
		
		$this->datatables->edit_column('performance_evaluation.percent_achieved',
		'<div class="meter orange">
		<span style="width: $1%"></span>
		</div>',
		'performance_evaluation.percent_achieved');
		
		$this->datatables->edit_column('performance_evaluation.job_assignment_id',
		'<a href="human_resource/add_performance_evalution/$1"><button class="btn green">Add Performance</button></a>',
		'performance_evaluation.job_assignment_id');
		
		return $this->datatables->generate();
	}
	/// Function For Benefits Management
	public function view_benefit_mgt()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		//->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		//->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		//->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/add_benefits/$1"><button class="btn green">Add Benefits</button></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	/// Function For Add Benefit to View Added Benefit
	public function view_benefit($employee_id = NULL)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, benefit_type_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')
		
		->where($employee_id)
		
		->group_by('employee.employee_id');
		
		$query = $this->db->get();
					
		return $query->result();
	}
	/// Function For Add Benefit to View Dependents
	public function view_dependent($employee_id = NULL)
	{
		$this->db->select('relation_name, dependents.dependent_name, dependents.date_of_birth, benefit_type_title, insurance_cover, benefits.date_rec_created, employee.employee_id, dependents.dependent_id')
		
		->from('employee')
				
		->join('benefits', 'employee.employee_id = benefits.employee_id', 'inner')
				
		->join('dependents', 'employee.employee_id = dependents.employee_id', 'inner')
		
		->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')		
		
		->join('ml_relation', 'dependents.ml_relationship_id = ml_relation.relation_id', 'inner')
		
		->where($employee_id)
		
		->group_by('dependents.dependent_id');
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{					
			return $query->result();
		}
	}
	/// Function For Employee Transfer View
	public function view_emp_transfer()
	{		
		$this->datatables->select('employee_code, full_name, posting_location, designation_name, department_name, employee.employee_id')
		
		->from('employee')
				
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
				
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->group_by('employee.employee_id');
		
		if($dept = $this->input->post('department'))
		{
			$this->datatables->where('posting.ml_department_id', $dept);
		}
		 
		if($design = $this->input->post('designation'))
		{
			$this->datatables->where('position_management.ml_designation_id', $design);
		}
		
		$this->datatables->edit_column('employee.employee_id', 
		'<a href="human_resource/transfer/$1"><button class="btn green">Transfer</button></a>',
		'employee.employee_id');
                
		return $this->datatables->generate();
	}
	/// Function For Employee Transfer
	public function view_transfer_detail($where)
	{		
		$this->db->select('employee_code, full_name, posting_location, city_name, department_name, posting_location, posting_reason, shift_name, branch_name, employee.employee_id')
		
		->from('employee')
				
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
				
		->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_city', 'posting.ml_city_id = ml_city.city_id', 'inner')
		
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		
		->join('ml_branch', 'posting.ml_branch_id = ml_branch.branch_id', 'inner')
		
		->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
		
		->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
		
		->join('ml_posting_reason_list', 'posting.reason_transfer = ml_posting_reason_list.posting_reason_list_id', 'inner')
		
		->where($where);
		
		$query = $this->db->get();
		   
		return $query->result();
	}
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Nadeem =============================///
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	
//============================= work by hamid ==================///
	public function view_postion_management()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" class="btn green"></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}	
	public function view_postion()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_job_category.job_category_name, status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/assign_position/$1"><input type="submit" name="continue" class="btn green"></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	public function manage_postion($where)
	{
		$this->db->select('employee.employee_code, employee.full_name, ml_designations.designation_name, ml_pay_grade.pay_grade,position_management.date_effective_from, ml_status.status_title,')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_pay_grade', 'ml_pay_grade.ml_pay_grade_id = position_management.ml_pay_grade_id', 'inner')
		
		->join('emp_experience', 'employee.employee_id = emp_experience.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('ml_job_title', 'emp_experience.ml_employment_type_id = ml_job_title.ml_job_title_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->where($where)
		
		->group_by('position_management.position_id');
		
		$query = $this->db->get();
		
		return $query->result();
				 
	}
	public function retirement()
	{
		$this->datatables->select('employee.employee_code, employee.full_name, ml_designations.designation_name, seperation_management.date,ml_posting_reason_list.posting_reason ,ml_status.status_title, employee.employee_id')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('leave_entitlement', 'employee.employee_id = leave_entitlement.employee_id', 'inner')
		
		->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')
		
		->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')
		
		->join('seperation_management', 'seperation_management.employee_id = employee.employee_id', 'inner')
		
		->join('ml_posting_reason_list', 'seperation_management.reason = ml_posting_reason_list.posting_reason_list_id', 'inner')
		
		->join('ml_status', 'leave_entitlement.status = ml_status.status_id', 'inner')
		
		->group_by('employee.employee_id');
		
		$this->datatables->edit_column('employee.employee_id',
		'<a href="human_resource/terminate/$1"><input type="submit" name="Discharge" class="btn green"></a>',
		'employee.employee_id');
		
		return $this->datatables->generate();
	}
	public function terminate_employees()
	{
		$this->db->select('employee.full_name,contract.comments,contract.contract_expiry_date')
		
		->from('employee')
		
		->join('employment', 'employment.employee_id = employment.employee_id', 'inner')
		
		->join('contract', 'employment.employment_id = contract.employment_id', 'inner');
		
		$query = $this->db->get();
		
		return $query->result();
	}	
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	/// ========================= End Of  Functions Created By Hamid Ali ===============================////
	/// ************************* *** ** ********** ******* ** ****** *****************************///
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// Function for Leave Attendance ////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public function leave_attendance()
{
	$this->datatables->select('employee_code, full_name, ml_leave_type.leave_type, (to_date-from_date), from_date,to_date, ml_approval_types.approval_types, employee.employee_id')
	->from('employee')
	->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
	->join('leave_entitlement','leave_application.entitlement_id=leave_entitlement.leave_entitlement_id','inner')
	->join('ml_leave_type','leave_entitlement.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
	->join('ml_approval_types','leave_entitlement.status=ml_approval_types.ml_approval_types_id','inner');
	
	if($leave_type = $this->input->post('leave_type'))
	{
		$this->datatables->where('ml_leave_type.ml_leave_type_id',$leave_type);
	}
	 $this->datatables->edit_column('employee.employee_id','<a href="leave_attendance/leave_action/$1"><button class="btn green">Action</button></a>','employee.employee_id');
	 	
		
	 
	 return $this->datatables->generate();
}


//////////////////////////////////////// Apply For Leave ////////////////////////////////////////////////////

	public function fetch_information($where)
	{
		$query = $this->db->query("SELECT employee.employee_code, employee.full_name, leave_application.note, ml_designations.designation_name, leave_entitlement.no_of_leaves, leave_application.from_date,leave_application.to_date, ml_leave_type.leave_type 
		FROM employee
	INNER JOIN employment ON employee.employee_id=employment.employee_id
	INNER JOIN leave_application ON employee.employee_id = leave_application.employee_id 
	INNER JOIN leave_entitlement ON leave_application.entitlement_id = leave_entitlement.leave_entitlement_id 
	INNER JOIN position_management ON employment.employment_id=position_management.employement_id 
	INNER JOIN ml_leave_type ON leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id
	INNER JOIN ml_designations ON position_management.ml_designation_id=ml_designations.ml_designation_id  
		WHERE $where");
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	
	}
	
	///////////////////////////////////// Function in Site Model For Staff On Leave /////////////////////////////////////
	
	public function leave_staff()
		{
         $this->datatables->select('employee.employee_code, employee.full_name, ml_leave_type.leave_type, (from_date-to_date), leave_application.from_date, leave_application.to_date, emergency_contacts.mobile_num, ml_status.status_title, employee.employee_id')
		->from('employee')
		->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
		->join('leave_entitlement','leave_application.application_id=leave_entitlement.leave_entitlement_id','inner')
		->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
		->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')
		->join('ml_status','leave_application.status=ml_status.status_id','inner');
		
		$this->datatables->edit_column('employee.employee_id', '<a href="leave_attendance/edit_leave_staff/$1"> <span class="fa fa-pencil"></span></a>&nbsp;&nbsp; &nbsp;&nbsp; | &nbsp;&nbsp;<span class="fa fa-print" onclick="print_leave_status($1)" style="cursor:pointer"></span>', 'employee.employee_id');
		
		if($leave_type = $this->input->post('leave_type'))
	    {
		$this->datatables->where('ml_leave_type.ml_leave_type_id',$leave_type);
	    }
		
	    if($status = $this->input->post('status_title'))
		{
		$this->datatables->where('leave_application.status', $status);
		}
		
		return $this->datatables->generate();
	}


public function update_leave_staff($where, $employee_code, $full_name, $leave_type, $from_date, $to_date, $mobile_num,$status)
{
	$query = $this->db->query("update employee
join user_account on user_account.employee_id=employee.employee_id 
join leave_application on employee.employee_id=leave_application.employee_id 
join leave_entitlement on employee.employee_id=leave_entitlement.employee_id 
join ml_leave_type on leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id 
join emergency_contacts on employee.employee_id=emergency_contacts.employee_id
	set employee.employee_code = '".$employee_code."',
	employee.full_name = '".$full_name."',
	leave_entitlement.ml_leave_type_id = '".$leave_type."',
	leave_application.from_date = '".$from_date."',
	leave_application.to_date = '".$to_date."',
	emergency_contacts.mobile_num = '".$mobile_num."',
	leave_application.status = '".$status."'
	where $where");
	
		if($query){
	        return true;
		}else {
		return false;
		}
		
		}
		
//////////////////////////////////////////////////////// Staff On Leave //////////////////////////////////////////////////		
		
		public function staff_infromation($where)
	{
		$this->db->select(   'employee_code,full_name,leave_type,leave_application.from_date,leave_application.to_date,emergency_contacts.mobile_num,leave_application.status,employee.employee_id')
		    ->from('employee')
		    ->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
			->join('leave_entitlement','leave_application.entitlement_id=leave_entitlement.leave_entitlement_id','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')

		    ->where($where);
			$query = $this->db->get();
			if($query->num_rows() > 0){
			return $query->row();
			}
	
	}

//////////////////////////////////////////////// Function for Attendence////////////////////////////////////////////////////

      public function leave_attend()
				{
				$this->datatables->select('employee_code,full_name,attendence.in_time,attendence.in_date,attendence.out_time,attendence.out_date, ml_assign_task.task_name,ml_department.department_name,ml_attendance_status_type.status_type,  employee.employee_id')
			->from('employee')
			->join('employment', 'employment.employee_id = employee.employee_id', 'inner')
			->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
			->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
			->join('ml_attendance_status_type','attendence.attendance_status_type = ml_attendance_status_type.attendance_status_type_id','inner')
			->join('ml_assign_task', 'employee.employee_id = ml_assign_task.ml_assign_task_id', 'inner')
			->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner');
				
			$this->datatables->edit_column('employee.employee_id','<span class="fa fa-print" onclick="print_attendence($1)" style="cursor:pointer"></span>', 'employee.employee_id');
		
		if($assign = $this->input->post('task_name'))
		{
			$this->datatables->where('ml_assign_task.ml_assign_task_id', $assign);
		}
				
		if($status = $this->input->post('status_type'))
		{
			$this->datatables->where('attendence.attendance_status_type', $status);
		}
		
		if($department = $this->input->post('department_name'))
		{
			$this->datatables->where('posting.ml_department_id', $department);
		}
			/*  if($month = $this->input->post('month'))
				{
				$this->datatables->where('ml_month.ml_month_id', $month);
				}
				*/
				
				return $this->datatables->generate();
				}
		
		
/////////////////////////////////////////////////// Function for Time Sheet ////////////////////////////////////////////////		
		
		public function time_sheet()
		{
			$this->datatables->select('project_title, ml_assign_task.task_name, in_time, in_date, out_time, out_date, ml_total_month_hour.monthly_std_hrs, employee.employee_id')
		 ->from('employee')
		 ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		 ->join('assign_job', 'assign_job.assign_job_id = employee.employee_id', 'inner')
		 ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		 ->join('hourly_time_sheet','hourly_time_sheet.employee_id=employee.employee_id','inner')
		 ->join('ml_total_month_hour', 'hourly_time_sheet.monthly_std_hrs_id = ml_total_month_hour.monthly_std_hrs_id', 'inner')
		 ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner');
		 
		 $this->datatables->edit_column('employee.employee_id','<span class="fa fa-print" onclick="print_timesheet($1)" style="cursor:pointer"></span>', 'employee.employee_id');
			
		return $this->datatables->generate();
	   }
	
	////////////////////////////////////////////// Function for Standard Time Sheet ////////////////////////////////////////   
	
	public function standard_timesheet()
	    {
		$this->datatables->select('employee_code,full_name,month,year,work_hours,leave_hours,monthly_std_hrs')
		->from('hourly_time_sheet')
		->join('employee','hourly_time_sheet.employee_id=employee.employee_id','inner')
		->join('ml_total_month_hour','hourly_time_sheet.monthly_std_hrs_id=ml_total_month_hour.monthly_std_hrs_id','inner')
		->join('ml_month','hourly_time_sheet.ml_month_id=ml_month.ml_month_id','inner')
        ->join('ml_year','hourly_time_sheet.ml_year_id=ml_year.ml_year_id','inner');
		
		if($year = $this->input->post('year'))
		{
			$this->datatables->where('ml_year.ml_year_id', $year);
		}
		
		if($month = $this->input->post('month'))
		{
			$this->datatables->where('ml_month.ml_month_id', $month);
		}
		
		return $this->datatables->generate();
		}
     /////////////////////////////////////////// Function for Auto Complete Time Sheet ////////////////////////////////
	  
	 /// Auto complete for loan advance pay back /////
	public function get_autocomplete_time_sheet()
	{	
	$query = $this->db->query("SELECT full_name, employee_code, ml_leave_type.leave_type, ml_designations .designation_name, leave_entitlement.no_of_leaves,employee.employee_id
FROM employee
INNER JOIN leave_application ON employee.employee_id =leave_application.employee_id

INNER JOIN leave_entitlement ON leave_application.entitlement_id = leave_entitlement.leave_entitlement_id
INNER JOIN ml_leave_type ON leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id
		INNER JOIN employment ON employee.employee_id=employment.employment_id		
		INNER JOIN position_management ON employment.employment_id=position_management.employement_id
		INNER JOIN ml_designations ON position_management.position_id=ml_designations.ml_designation_id");
	$this->db->like('full_name',$this->input->post('queryString'),'both');
	return $query;
	}
	// END

//////////////////////////////////////// Function for Print Leave Status /////////////////////////////////////////////
    public function print_leave_status($id)
	        {
		    $this->db->select('*')		
			->from('employee')
			->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
			->join('leave_entitlement','leave_application.application_id=leave_entitlement.leave_entitlement_id','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')
			->join('ml_status','leave_application.status=ml_status.status_id','inner')
	        ->where($id);
		    $query = $this->db->get();
		    return $query->row();
    }
	public function print_all_leave_status($where)
	        {
		    $this->db->select('*')		
			->from('employee')
			->join('leave_application','employee.employee_id=leave_application.employee_id','inner')
			->join('leave_entitlement','leave_application.application_id=leave_entitlement.leave_entitlement_id','inner')
			->join('emergency_contacts','emergency_contacts.employee_id=employee.employee_id','inner')
			->join('ml_leave_type','leave_entitlement.leave_entitlement_id=ml_leave_type.ml_leave_type_id','inner')
			->join('ml_status','leave_application.status=ml_status.status_id','inner')
	        ->where($where);
		    $query = $this->db->get();
		    return $query->result();
    }
/////////////////////////////////////////////////////// Print Attendence ///////////////////////////////////////////


public function print_attendence($id)
{
	$this->db->select('*')		
	->from('employee')
    ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
	->join('ml_attendance_status_type','attendence.attendance_status_type = ml_attendance_status_type.attendance_status_type_id',     'inner')
	->join('ml_assign_task', 'employee.employee_id = ml_assign_task.ml_assign_task_id', 'inner')
	->where($id);
	$query = $this->db->get();
	return $query->row();
}


///////////////////////////////////////////////// Print Funtion for All Attendence //////////////////////////////////////


public function print_attendence_present($where)
	{
		/*$present = array('attendence.attendance_status_type' => 1
		);*/
		$this->db->select('*')		
		->from('employee')
		->join('employment', 'employment.employee_id = employee.employee_id', 'inner')
		->join('posting', 'posting.employement_id = employment.employment_id', 'inner')
		->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		->join('ml_attendance_status_type','attendence.attendance_status_type =        ml_attendance_status_type.attendance_status_type_id','inner')
		->join('ml_assign_task', 'employee.employee_id = ml_assign_task.ml_assign_task_id', 'inner')
		->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
		->group_by('attendence.attendence_id, employee.employee_id')
		->where($where);
		$query = $this->db->get();
		return $query->result();
}
////////////////////////////////////// Print Function for Time sheet ///////////////////////////////////////////////////
public function print_timesheet($id)
    {
    $this->db->select('*')		
	     ->from('employee')
	     ->join('attendence', 'employee.employee_id = attendence.employee_id', 'inner')
		 ->join('assign_job', 'assign_job.assign_job_id = employee.employee_id', 'inner')
		 ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')
		 ->join('hourly_time_sheet','hourly_time_sheet.employee_id=employee.employee_id','inner')
		 ->join('ml_total_month_hour', 'hourly_time_sheet.monthly_std_hrs_id = ml_total_month_hour.monthly_std_hrs_id', 'inner')
		 ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')
         ->where($id);
	     $query = $this->db->get();
	     return $query->row();

   }   
	//*************************************** END OF Leave Attendance **********************************************// 	
	
       
}
?>