<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Essp_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Karachi");
		
	}
	
	public function employee_info($where)
	{
		$this->db->select('employee.employee_id,full_name, ml_projects.project_title, ml_department.department_name, ml_assign_task.task_name,
		assign_job.start_date, assign_job.completion_date');
		$this->db->from('employee')
        ->join('employment','employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0','LEFT')
        ->join('assign_job','employment.employment_id = assign_job.employment_id','LEFT')
        ->join('ml_assign_task','assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id','LEFT')
        ->join('ml_projects','assign_job.project_id = ml_projects.project_id','LEFT')

        ->join('posting','employment.employment_id = posting.employement_id','LEFT')
        ->join('ml_department','posting.ml_department_id = ml_department.department_id','LEFT');
		$this->db->where('employee.employee_id',$where);
		$query=$this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		
	}

	// End

	
	public function salary_info($where = NULL,$selc_month = NULL,$selc_year = NULL)
	{
		$this->db->select(
'employee.employee_id,employee.full_name,ml_month.month,ml_year.year,
salary.base_salary,
salary_payment.transaction_amount,
(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,
salary_payment.salary_payment_id,ml_status.status_title,
salary_payment.transaction_id,transaction.transaction_date')
	->from('employee')
	->where($where)
    ->where('transaction.status',2)
	->join('employment','employee.employee_id=employment.employee_id','LEFT')
	->join('salary','employment.employment_id=salary.employement_id','LEFT')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
    ->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
    ->join('ml_status','transaction.status=ml_status.status_id','LEFT')
	->join('ml_month','salary_payment.month=ml_month.ml_month_id')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id')
	//->like('employee.full_name',$emp_name)
	//->like('salary_payment.month',$selc_month)
	//->like('salary_payment.year',$selc_year)
	->group_by('salary_payment.salary_payment_id');
	if($selc_month)
	{$this->db->where('salary_payment.month',$selc_month);}
	if($selc_year)
	{$this->db->where('salary_payment.year',$selc_year);}
	//$this->db->limit($limit,$start);
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->result();
		}
		
	}
	
		/*public function salarypayment_slip_emp($where)
		{
		$this->db->select(
'employee.employee_id,employee.employee_code,employee.full_name,
employee.cnic,ml_month.month,ml_year.year,salary.base_salary,
SUM(DISTINCT CASE WHEN deduction.status=2 THEN deduction.deduction_amount ELSE 0 END) as ded_amount,
SUM(DISTINCT CASE WHEN allowance.status=2 THEN allowance.allowance_amount ELSE 0 END) as allowance_amnt,
SUM(DISTINCT CASE WHEN (expense_claims.status=2 AND expense_payment.transaction_id > 0) THEN expense_claims.amount ELSE 0 END) as  exp_amount,
salary_payment.transaction_amount,SUM(DISTINCT CASE WHEN (loan_advances.status=2 AND loan_advance_payment.transaction_id > 0)THEN loan_advances.amount ELSE 0 END) as loan_amount,
employment.employee_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as loan_payback_id,expense_claims.expense_claim_id,expense_disbursment.expense_id,
(deduction_processing.month) as ded_month,(deduction_processing.year) as ded_year,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,(expense_payment.month) as exp_month,
(expense_payment.year) as exp_year,(allowance_payment.month) as alwnc_month,(allowance_payment.year) as alwnc_year,(loan_advance_payment.month) as alonad_month,(loan_advance_payment.year) as alonad_year,
salary_payment.salary_payment_id,ml_status.status_title,transaction.transaction_date,payment_mode.payment_mode_type_id,cash_payment.received_by,cash_payment.remarks,emp_bank_account.account_no,emp_bank_account.bank_name,
emp_bank_account.branch,cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,deduction.deduction_frequency,allowance.pay_frequency,ml_month.month,ml_year.year')
	->from('employee')
	->where($where)
	->join('employment','employee.employee_id=employment.employee_id')
	->join('salary','employment.employment_id=salary.employement_id','LEFT')
	->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
	->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')
	->join('payment_mode','transaction.payment_mode=payment_mode.payment_mode_id','LEFT')
	->join('cash_payment','payment_mode.cash_id=cash_payment.cash_payment_id','LEFT')
	->join('emp_bank_account','payment_mode.account_id=emp_bank_account.bank_account_id','LEFT')
	->join('cheque_payments','payment_mode.cheque_id=cheque_payments.cheque_payment_id','LEFT')
	->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
	->join('position_management','employment.employment_id=position_management.employement_id')
	
	->join('deduction','deduction.employee_id=employee.employee_id','LEFT')
	->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
	->join('allowance','allowance.employee_id=employee.employee_id','LEFT')
	->join('allowance_payment','allowance_payment.allowance_id=allowance.allowance_id','LEFT')
	->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT')
	->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('expense_disbursment','expense_disbursment.expense_id=expense_claims.expense_claim_id','LEFT')
	->join('loan_advances','loan_advances.employee_id=employee.employee_id','LEFT')
	->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')
	//->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
	->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
	->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT');
	//$this->db->group_by('ml_month.month AND ml_year.year');
	//$this->db->where($where2);
	//$this->db->like($month_year);
	//$this->db->like('salary_payment.year',$year);
	
	$query=$this->db->get();
		if($query->num_rows()>0)
		{	
			return $query->row();
		}
		
		} */

    public function salarypayment_slip_emp($where2)
    {
        $this->db->select(
            'employee.employee_id,employee.employee_code,employee.full_name,employee.cnic,ml_month.month,ml_year.year,
salary.base_salary,ml_status.status_title,transaction.transaction_date,
payment_mode.payment_mode_type_id,cash_payment.received_by,cash_payment.remarks,
emp_bank_account.account_no,emp_bank_account.bank_name,emp_bank_account.branch,
cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,ml_month.month,
ml_year.year,transaction.transaction_amount,salary_payment.arears')
            ->from('employee')
            ->where($where2)
            ->where('transaction.status',2)
            ->join('employment','employee.employee_id=employment.employee_id')
            ->join('salary','employment.employment_id=salary.employement_id','LEFT')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction','salary_payment.transaction_id=transaction.transaction_id','LEFT')
            ->join('payment_mode','transaction.payment_mode=payment_mode.payment_mode_id','LEFT')
            ->join('cash_payment','payment_mode.cash_id=cash_payment.cash_payment_id','LEFT')
            ->join('emp_bank_account','payment_mode.account_id=emp_bank_account.bank_account_id','LEFT')
            ->join('cheque_payments','payment_mode.cheque_id=cheque_payments.cheque_payment_id','LEFT')
            ->join('ml_status','salary_payment.status=ml_status.status_id','LEFT')
            ->join('position_management','employment.employment_id=position_management.employement_id')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT');
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }

    }

    public function payroll_regded_trans($id,$month= NULL,$year= NULL,$salary_trans_date)
    {
        $this->db->select('deduction_processing.transaction_id,transaction.transaction_date,transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as total_ded_amount,
		(transaction.transaction_id) as ded_trans_id')
            ->from('employee')
            ->where('transaction.employee_id',$id)
            ->where('transaction.transaction_type',1)
            ->where('transaction.transaction_date',$salary_trans_date)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('deduction','employee.employee_id=deduction.employee_id','LEFT')
            ->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
            ->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
            ->join('transaction','transaction.transaction_id=deduction_processing.transaction_id','LEFT');

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regallw_trans($id,$month= NULL,$year= NULL,$salary_trans_date)
    {
        $this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
		SUM(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as total_alw_recm,
		(transaction.transaction_id) as alw_trans_id')
            ->from('employee')
            ->where('transaction.employee_id',$id)
            ->where('transaction.transaction_type',2)
            ->where('transaction.transaction_date',$salary_trans_date)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('allowance','employee.employee_id=allowance.employee_id','LEFT')

            ->join
            ('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id','LEFT')
            ->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
            ->join('transaction','transaction.transaction_id=allowance_payment.transaction_id','LEFT');

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regexp_trans($id,$month= NULL,$year= NULL,$salary_trans_date)
    {
        $this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as exp_recm,
		SUM(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as total_exp_recm,
		(transaction.transaction_id) as exp_trans_id')
            ->from('employee')
            ->where('transaction.employee_id',$id)
            ->where('transaction.transaction_type',4)
            ->where('transaction.transaction_date',$salary_trans_date)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('expense_claims','employee.employee_id=expense_claims.employee_id','LEFT')
            ->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
            ->join
            ('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
            ->join('transaction','transaction.transaction_id=expense_disbursment.transaction_id','LEFT');

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regadvance_trans($id,$month,$year,$salary_trans_date)
    {
        $this->db->select('(transaction.transaction_amount) as advance_trans_amount,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as loan_recm,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
		SUM(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as total_advamount,
		(transaction.transaction_id) as adv_trans_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as payback_advance_id')
            ->from('employee')
            ->where('transaction.employee_id',$id)
            ->where('transaction.transaction_type',3)
            ->where('transaction.transaction_date',$salary_trans_date)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('loan_advances','employee.employee_id=loan_advances.employee_id','LEFT')
            ->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
            ->join
            ('loan_advance_payback','loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id','LEFT')
            ->join('transaction','transaction.transaction_id=loan_advance_payback.transaction_id','LEFT')
            ->where('loan_advance_payback.month',$month)
            ->where('loan_advance_payback.year',$year);

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

///////////END

///////////////////add new records expense/////////////////
	
	public function expense_new_record($tbl,$data)
	{	
	
		$this->db->trans_start();
		$query=$this->db->insert($tbl,$data);
		$expense_id=$this->db->insert_id();
		/*$query=$this->db->insert('transaction',array(
		'calender_year'=>date('Y'),
		'calender_month'=>date('M'),
		'transaction_date'=>date('Y-m-d'),
		'transaction_time'=>date('h:i:s'),
		'transaction_amount'=>$this->input->post('expamount'),
		'transaction_type'=>4,
		'employee_id'=>$this->input->post('emp_id')
		));
		$transaction_id=$this->db->insert_id();
		$query=$this->db->insert('expense_payment',array(
		'expense_id'=>$expense_id,
		'month'=>$month,
		'year'=>$year,
		'transaction_id'=>$transaction_id
		));*/
		
		if ($this->db->trans_status() === FALSE)
		{
   			 $this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $query;
		$this->db->trans_complete();
		}
	
	
	//END


    /////////////////////////////////////// Function For Apply for leave in Model /////////////////////////////////////////////
    public function apply_for_leave($empid)
    {
        $this->db->select('employee.employee_id ,employee.employee_code, employee.full_name, leave_application.note,leave_application.from_date,leave_application.to_date, ml_leave_type.leave_type')
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id AND employment.trashed = 0 AND employment.current = 1','LEFT')
            ->join('leave_application',  'employment.employment_id = leave_application.employment_id','inner')
            ->join('leave_approval','employment.employment_id=leave_approval.employment_id','LEFT')
            //->join('leave_entitlement',  'employee.employee_id = leave_entitlement.employee_id','inner')
            ->join('ml_leave_type',  'leave_application.ml_leave_type_id = ml_leave_type.ml_leave_type_id','inner')
            ->join('ml_status','leave_approval.status=ml_status.status_id','inner')
            ->where('employee.employee_id',$empid);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }

    }

    ////// End//////


    /////////////////////////////////////////////////// Function for Time Sheet ////////////////////////////////////////////////

    public function time_sheet($empid)
    {
        if($full_name=$this->input->post('full_name')){
            $this->db->select('employee.employee_id,employee.full_name, project_title, task_name, month,in_date,year,ml_attendance_status_type.status_type, in_time, Out_date ,out_time ,employee.employee_id,MONTH(in_date)as in_month,YEAR(in_date)as in_year,attendence.ml_month_id,attendence.ml_year_id')

                ->from('employee')

                ->join('employment','employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0','inner')

                ->join('attendence', 'employee.employee_id = attendence.employee_id', 'left')

                ->join('ml_attendance_status_type', 'ml_attendance_status_type.attendance_status_type_id = attendence.attendance_status_type', 'left')

                ->join('ml_month', ' ml_month.ml_month_id = attendence.ml_month_id', 'left')

                ->join('ml_year', ' ml_year.ml_year_id = attendence.ml_year_id', 'left')

                ->join('assign_job', 'employment.employment_id =  assign_job.employment_id', 'left')

                ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'left')

                ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'left')

                //->like('employee.full_name',$full_name)
                ->where($empid)
                ->group_by('attendence.attendence_id');

            if($month=$this->input->post('month'))
            {
                $this->db->where('MONTH(attendence.in_date)',$month);
            }

            if($year=$this->input->post('year'))
            {$this->db->where('attendence.ml_year_id',$year);}

            $query=$this->db->get();
            if($query->num_rows > 0)
            {
                return $query->result();
            }}else{ return false;}
    }

/// End

//////////////////////////////// Common Function ///////////////////////////////////////

    public function create_new_record($table,$data)
    {
        $query = $this->db->insert($table, $data);

        if($query)
        {
            return true;
        } else{
            return false;
        }
    }


    ///////////////////////////////// Function For Leave Master List ////////////////////////////


    public function leave_type_ml()
    {
        $rows = $this->db->select('*')

            ->from('ml_leave_type')

            ->where('trashed',0)

            ->get()

            ->result();

        $drop_down = array('' => '-- Select Leave Type --');

        foreach($rows as $row)
        {
            $drop_down[$row->ml_leave_type_id] = $row->leave_type;
        }
        return $drop_down;
    }


    /////////////////////////// Common Function for Update ////////////////////////////////////
    public function update_record($table,$where,$data)
    {
        $this->db->where($where);

        $query = $this->db->update($table,$data);

        if($query)
        {
            return true;
        }else{
            return false;
        }
    }

////////////////////////////// Common Dynamic functions //////////////////////
 public  function html_selectbox($table,$option,$key,$value,$where) {
  		$this->db->select('*');
		$this->db->where($where);
  		$query = $this->db->get($table);
  		if($query->num_rows() > 0) {
			
   		$data[''] = $option;
   		foreach($query->result() as $row) {
    	        $data[$row->$key] = $row->$value;
   	 	}
   		return $data;
 		}
		
	}

    /////////////////////////  Function For Year ////////////////////////////
    public function year_same()
    {
        $rows = $this->db->select('*')

            ->from('ml_year')

            ->where('trashed',0)

            ->get()

            ->result();

        $drop_down = array('' => '-- Select Year --');

        foreach($rows as $row)
        {
            $drop_down[$row->ml_year_id] = $row->year;
        }
        return $drop_down;
    }
    //// End

    ////////////////////////// Function For Month ////////////////////////////


    public function month_same()
    {
        $rows = $this->db->select('*')

            ->from('ml_month')

            ->where('trashed',0)

            ->get()

            ->result();

        $drop_down = array('' => '-- Select Month --');

        foreach($rows as $row)
        {
            $drop_down[$row->ml_month_id] = $row->month;
        }
        return $drop_down;
    }

    public function get_row_by_where($table,$where)
    {
        $this->db->select('*')->from($table);

        $this->db->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }
    public function join_for_contact($where)
    {
        $this->db->select('permanant_contacts.address, permanant_contacts.phone, ml_city.city_name,ml_district.district_name, ml_province.province_name')

            ->from('employee')

            ->join('current_contacts', 'employee.employee_id = current_contacts.employee_id', 'inner')

            ->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'inner')

            ->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'inner')

            ->join('ml_district', 'permanant_contacts.district = ml_district.district_id', 'inner')

            ->join('ml_province', 'permanant_contacts.province = ml_province.province_id', 'inner')


            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }
    public function join_for_exp($where)
    {
        $this->db->select(' ml_employment_type.employment_type, ml_designations.designation_name')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('contract', 'employment.employment_id = contract.employment_id', 'left')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('ml_employment_type', 'contract.employment_type = ml_employment_type.employment_type_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function join_for_exp_result($where)
    {
        $this->db->select(' ml_employment_type.employment_type, ml_designations.designation_name,emp_experience.from_date,emp_experience.to_date,emp_experience.organization')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('contract', 'employment.employment_id = contract.employment_id', 'left')

            ->join('emp_experience', 'emp_experience.employee_id = employee.employee_id', 'left')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('ml_employment_type', 'contract.employment_type = ml_employment_type.employment_type_id', 'inner')

            ->join('ml_designations', 'emp_experience.job_title = ml_designations.ml_designation_id', 'inner')

            ->where($where)
            ->where('emp_experience.trashed',0)
            ->group_by('emp_experience.experience_id');

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function join_for_skl($where_emp)
    {
        $this->db->select(' ml_skill_type.skill_name,ml_skill_level.skill_level')
            ->from('employee')
            ->join('employment', 'employee.employee_id = employment.employee_id')
            ->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')
            ->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')
            ->join('ml_skill_level', 'emp_skills.ml_skill_level_id = ml_skill_level.level_id', 'inner')
            ->where('employment.employment_id',$where_emp)
            ->where('emp_skills.trashed',0)
           ->group_by('ml_skill_level.level_id');

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    public function detl_employeeee($where)
    {
        $this->db->select('job_specifications, ml_job_category.job_category_name, ml_city.city_name,ml_department.department_name,ml_posting_location_list.posting_location, ml_shift.shift_name, contract_expiry_date')
            ->from('position_management')
            ->join('employment', 'position_management.employement_id = employment.employment_id', 'inner')
            ->join('posting', 'employment.employment_id = posting.employement_id', 'inner')
            ->join('contract', 'employment.employment_id = contract.employment_id', 'inner')
            ->join('employee', 'employment.employment_id = employee.employee_id', 'inner')
            ->join('permanant_contacts', 'employee.employee_id = permanant_contacts.employee_id', 'inner')
            ->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')
            ->join('ml_job_category', 'contract.employment_category = ml_job_category.job_category_id', 'inner')
            ->join('ml_posting_location_list', 'posting.location = ml_posting_location_list.posting_locations_list_id', 'inner')
            ->join('ml_shift', 'posting.shift = ml_shift.shift_id', 'inner')
            ->join('ml_city', 'permanant_contacts.city_village = ml_city.city_id', 'inner')
            ->where($where);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function join_for_depend($where)
    {
        $this->db->select('dependents.dependent_name, ml_relation.relation_name, dependents.date_of_birth')

            ->from('employee')
            ->join('employment','employee.employee_id = employment.employee_id','inner')

            ->join('dependents', 'dependents.employee_id = employee.employee_id', 'inner')

            ->join('emergency_contacts', 'employee.employee_id = emergency_contacts.employee_id', 'inner')

            ->join('emp_skills', 'employment.employment_id = emp_skills.employment_id', 'inner')

            ->join('ml_skill_type', 'emp_skills.ml_skill_type_id = ml_skill_type.skill_type_id', 'inner')

            ->join('ml_relation', 'emergency_contacts.relationship = ml_relation.relation_id', 'inner')

            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function join_for_pay($where)
    {
        $this->db->select('ml_pay_grade.pay_grade, ml_pay_frequency.pay_frequency, currency_name, ml_payrate.payrate')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('salary', 'employment.employment_id = salary.employement_id', 'inner')

            ->join('ml_currency', 'salary.ml_currency = ml_currency.ml_currency_id', 'inner')

            ->join('ml_payrate', 'salary.ml_pay_rate_id = ml_payrate.ml_payrate_id', 'inner')

            ->join('ml_pay_frequency', 'salary.ml_pay_frequency = ml_pay_frequency.ml_pay_frequency_id', 'inner')

            ->join('ml_pay_grade', 'salary.ml_pay_grade = ml_pay_grade.ml_pay_grade_id', 'inner')


            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function join_for_last($where)
    {
        $this->db->select('increment_amount, ml_increment_type.increment_type, ml_benefit_type.benefit_type_title, allowance_amount, ml_allowance_type.allowance_type, no_of_leaves_allocated, ml_leave_type.leave_type, (select employee.full_name from employee where employee_id = reporting_heirarchy.employeed_id) as emp_name, ml_reporting_options.reporting_option')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'INNER')

            ->join('increments', 'employment.employment_id = increments.employment_id', 'INNER')

            ->join('allowance', 'employment.employment_id = allowance.employment_id', 'INNER')

            ->join('reporting_heirarchy', 'employee.employee_id = reporting_heirarchy.employeed_id', 'INNER')

            ->join('leave_entitlement', 'employment.employment_id = leave_entitlement.employment_id', 'INNER')

            ->join('benefits', 'employment.employment_id = benefits.employment_id', 'inner')

            ->join('ml_increment_type', 'increments.increment_type_id = ml_increment_type.increment_type_id', 'INNER')

            ->join('ml_benefit_type', 'benefits.benefit_type_id = ml_benefit_type.benefit_type_id', 'inner')

            //->join('ml_benefits_list', 'benefits.benefits_list_id = ml_benefits_list.benefit_item_id', 'inner')

            ->join('ml_allowance_type', 'allowance.ml_allowance_type_id = ml_allowance_type.ml_allowance_type_id', 'INNER')

            ->join('ml_leave_type', 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id', 'INNER')

            ->join('ml_reporting_options', 'reporting_heirarchy.ml_reporting_heirarchy_id = ml_reporting_options.reporting_option_id', 'INNER')


            ->where($where);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function view_progress($id)
    {
        $this->db->select('ml_kpi.kpi, performance_evaluation.percent_achieved, performance_evaluation.date, full_name, employee.employee_id')
            ->from('employee')
            ->join('employment','employee.employee_id = employment.employee_id')
            ->join('assign_job','assign_job.employment_id = employment.employment_id')
            ->join('performance_evaluation','performance_evaluation.job_assignment_id = assign_job.assign_job_id')
            ->join('ml_kpi','performance_evaluation.ml_kpi_type_id = ml_kpi.ml_kpi_id')
            ->where('employee.employee_id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    public function view_progress_table($id)
    {
        $this->db->select('ml_projects.project_title, performance_evaluation.percent_achieved,assign_job.start_date,assign_job.completion_date')
            ->from('employee')
            ->join('employment','employment.employee_id = employee.employee_id AND employment.current AND employment.trashed = 0')
            ->join('assign_job','assign_job.employment_id = employment.employment_id')
            ->join('performance_evaluation','performance_evaluation.job_assignment_id = assign_job.assign_job_id')
            ->join('ml_projects','assign_job.project_id = ml_projects.project_id')

            ->where('employee.employee_id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    public function view_task_history($where)
    {
        $this->db->select('employee.employee_code, employee.full_name, task_description, task_name, project_title, assign_job.start_date, assign_job.completion_date, status, employee.employee_id')

            ->from('employee')

            ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')

            ->join('assign_job', 'employment.employment_id = assign_job.employment_id', 'inner')

            ->join('ml_assign_task', 'assign_job.ml_assign_task_id = ml_assign_task.ml_assign_task_id', 'inner')

            ->join('ml_projects', 'assign_job.project_id = ml_projects.project_id', 'inner')

            ->where($where);

        //->group_by('assign_job.assign_job_id');

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    public function goto_list()
    {
        $this->datatables->select('employee.full_name, ml_designations.designation_name, ml_department.department_name, resposible, current_contacts.official_email, current_contacts.office_phone, goto_list.goto_list_id')

            ->from('goto_list')

            ->join('employee', 'goto_list.employee_id = employee.employee_id')

            ->join('ml_department', 'ml_department.department_id = goto_list.ml_department_id')

            ->join('current_contacts', 'current_contacts.employee_id = goto_list.employee_id')

            ->join('ml_designations', 'goto_list.ml_desigination_id = ml_designations.ml_designation_id');

        $this->datatables->edit_column('goto_list.goto_list_id',
            '<a href="dashboard_site/edit_goto_list/$1"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;
	<a href="dashboard_site/delete_goto_list/$1"><span class="fa fa-trash-o"></span></a>',
            'goto_list.goto_list_id');

        if($approval = $this->input->post('full_name'))
        {
            $this->datatables->where('employee.employee_id', $approval);
        }
        if($exp = $this->input->post('department_name'))
        {
            $this->datatables->where('ml_department.department_id', $exp);
        }

        return $this->datatables->generate();


    }

    public function phone_book()
    {
        $this->datatables->select('full_name, designation_name, project_title, department_name, mob_num, official_email')

            ->from('employee')

            ->join('current_contacts','employee.employee_id=current_contacts.employee_id','inner')

            ->join('employment', 'employee.employee_id = employment.employee_id', 'inner')

            ->join('position_management', 'employment.employment_id = position_management.employement_id', 'inner')

            ->join('ml_designations', 'position_management.ml_designation_id = ml_designations.ml_designation_id', 'inner')

            ->join('posting', 'posting.employement_id = employment.employment_id', 'inner')

            ->join('ml_department', 'posting.ml_department_id = ml_department.department_id', 'inner')

            ->join('assign_job','employee.employee_id=assign_job.employee_id','inner')

            ->join('ml_projects','ml_projects.project_id=assign_job.project_id','inner')

            ->group_by('employee.employee_id');

        return $this->datatables->generate();
    }
    //// End
    ////////////////////// Function for attendance retreive from database //////////////////////////////////////////
    public function attendance_in($id,$datein)
    {
        $this->db->select('employee_id,in_date');
        $this->db->from('attendence');
        //$this->db->join('employee');
        $this->db->where('employee_id',$id);
        $this->db->where('in_date',$datein);
        //$this->db->where('out_date',$dateout);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }
    public function attendance_out($id,$dateout)
    {
        $this->db->select('employee_id,out_date');
        $this->db->from('attendence');
        //$this->db->join('employee');
        $this->db->where('employee_id',$id);
        //$this->db->where('in_date',$datein);
        $this->db->where('out_date',$dateout);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function get_row_by_id($table,$where){
        $this->db->select('*')->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }
    }


//////////////////functions for employee salary slip

    public function month_salaries($where_emp)
    {
        $this->db->select('transaction.transaction_date,transaction.transaction_amount,
		salary_payment.arears,transaction.payment_mode,salary_payment.month,
		salary_payment.year,salary_payment.salary_payment_id,transaction.transaction_id')
            ->from('employee')
            ->where($where_emp)
            ->where('transaction.status =2')
            //->where('salary_payment.year',$year)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('salary','employment.employment_id=salary.employement_id','LEFT')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT');
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payment_mode_details($payment_mode_id)
    {
        $this->db->select("emp_bank_account.account_no,emp_bank_account.account_title,
		emp_bank_account.bank_name,emp_bank_account.branch,payment_mode.account_id,payment_mode.cash_id,
		payment_mode.cheque_id,cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,
		cash_payment.received_by,cash_payment.remarks")
            ->from('payment_mode')
            ->join('emp_bank_account','emp_bank_account.bank_account_id=payment_mode.account_id','LEFT')
            ->join('cheque_payments','cheque_payments.cheque_payment_id=payment_mode.cheque_id','LEFT')
            ->join('cash_payment','cash_payment.cash_payment_id=payment_mode.cash_id','LEFT')
            ->where('payment_mode.payment_mode_id',$payment_mode_id);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payment_mode_details_slip($payment_mode_id)
    {
        $this->db->select("emp_bank_account.account_no,emp_bank_account.account_title,
		emp_bank_account.bank_name,emp_bank_account.branch,payment_mode.account_id,payment_mode.cash_id,
		payment_mode.cheque_id,cheque_payments.cheque_no,(cheque_payments.bank_name) as chk_bank,
		cash_payment.received_by,cash_payment.remarks")
            ->from('payment_mode')
            ->join('emp_bank_account','emp_bank_account.bank_account_id=payment_mode.account_id','LEFT')
            ->join('cheque_payments','cheque_payments.cheque_payment_id=payment_mode.cheque_id','LEFT')
            ->join('cash_payment','cash_payment.cash_payment_id=payment_mode.cash_id','LEFT')
            ->where('payment_mode.payment_mode_id',$payment_mode_id);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regded_trans_slip($id,$month= NULL,$year= NULL,$salary_trans_date)
    {
        $this->db->select('deduction_processing.transaction_id,transaction.transaction_date,SUM(DISTINCT transaction.transaction_amount) as transaction_amount,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as ded_recmnt,
		group_concat(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN ml_deduction_type.deduction_type  ELSE NULL END) as ded_rectp,
		SUM(DISTINCT CASE WHEN (deduction.deduction_id=deduction_processing.deduction_id) THEN deduction.deduction_amount  ELSE NULL END) as total_ded_amount,
		(transaction.transaction_id) as ded_trans_id')
            ->from('employee')

            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('deduction','employment.employment_id=deduction.employment_id','LEFT')
            ->join('ml_deduction_type','ml_deduction_type.ml_deduction_type_id=deduction.ml_deduction_type_id','LEFT')
            ->join('deduction_processing','deduction.deduction_id=deduction_processing.deduction_id','LEFT')
            ->join('transaction','transaction.transaction_id=deduction_processing.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',1)
            ->or_where('transaction.transaction_type',8)
            ->where('transaction.transaction_date',$salary_trans_date);

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regallw_trans_slip($id,$month= NULL,$year= NULL,$salary_trans_date)
    {
        $this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN ml_allowance_type.allowance_type  ELSE NULL END) as alw_rectp,
		group_concat(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as alw_recm,
		SUM(DISTINCT CASE WHEN (allowance.allowance_id=allowance_payment.allowance_id) THEN allowance.allowance_amount  ELSE NULL END) as total_alw_recm,
		(transaction.transaction_id) as alw_trans_id')
            ->from('employee')

            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('allowance','employment.employment_id=allowance.employment_id','LEFT')
            ->join('allowance_payment','allowance.allowance_id=allowance_payment.allowance_id','LEFT')
            ->join('ml_allowance_type','ml_allowance_type.ml_allowance_type_id=allowance.ml_allowance_type_id','LEFT')
            ->join('transaction','transaction.transaction_id=allowance_payment.transaction_id','LEFT')
        ->where('transaction.employment_id',$id)
        ->where('transaction.transaction_type',2)
        ->where('transaction.transaction_date',$salary_trans_date);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regexp_trans_slip($id,$month,$year,$salary_trans_date)
    {
        $this->db->select('transaction.transaction_amount,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN ml_expense_type.expense_type_title  ELSE NULL END) as exp_rectp,
		group_concat(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as exp_recm,
		SUM(DISTINCT CASE WHEN (expense_claims.expense_claim_id=expense_disbursment.expense_id) THEN expense_claims.amount  ELSE NULL END) as total_exp_recm,
		(transaction.transaction_id) as exp_trans_id')
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
            ->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')
            ->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
            ->join('transaction','transaction.transaction_id=expense_disbursment.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',4)
            ->where('transaction.transaction_date',$salary_trans_date);

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_regadvance_trans_slip($id,$month,$year,$salary_trans_date)
    {
        $this->db->select('(transaction.transaction_amount) as advance_trans_amount,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as loan_recm,
		group_concat(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN ml_payment_type.payment_type_title  ELSE NULL END) as loan_rectp,
		SUM(DISTINCT CASE WHEN (loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id AND loan_advances.month='.$month.' AND loan_advances.year='.$year.') THEN loan_advances.amount ELSE NULL END) as total_advamount,
		(transaction.transaction_id) as adv_trans_id,loan_advances.loan_advance_id,(loan_advance_payback.loan_advance_id) as payback_advance_id')
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')
            ->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
            ->join('loan_advance_payback','loan_advances.loan_advance_id=loan_advance_payback.loan_advance_id','LEFT')
            ->join('transaction','transaction.transaction_id=loan_advance_payback.transaction_id','LEFT')
            ->where('transaction.employment_id',$id)
            ->where('transaction.transaction_type',3)
            ->where('transaction.transaction_date',$salary_trans_date)
            ->where('loan_advance_payback.month',$month)
            ->where('loan_advance_payback.year',$year);

        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->row();}
    }

    public function payroll_reg_details_slip($where,$month =NULL,$year =NULL,$salary_trans_date =NULL)
    {
        $this->db->select(
            "employee.employee_id,employee.employee_code,employee.full_name,
ml_designations.designation_name,ml_pay_grade.pay_grade,
ml_month.month,ml_year.year,salary.base_salary,
(CASE WHEN (transaction.transaction_type=5 AND transaction.transaction_date=$salary_trans_date) THEN transaction.transaction_amount ELSE 0 END) as salary_trans,
salary_payment.transaction_amount,(salary_payment.month) as salary_month,(salary_payment.year) as salary_year,
salary_payment.salary_payment_id,ST1.transaction_date,ST1.status,
(ST1.approval_date) as ap_date,ST1.transaction_date,ST1.transaction_id,
ST1.approval_date,(Select full_name from employee where employee_id=ST1.approved_by) as name,
ml_status.status_title,salary_payment.salary_id")
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id')
            ->join('transaction','transaction.employment_id=employment.employment_id')
            ->join('salary','employment.employment_id=salary.employement_id')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction ST1','salary_payment.transaction_id=ST1.transaction_id','LEFT')
            ->join('position_management','employment.employment_id=position_management.employement_id','LEFT')
            ->join('ml_status','ST1.status=ml_status.status_id','LEFT')
            ->join('ml_pay_grade','ml_pay_grade.ml_pay_grade_id=position_management.ml_pay_grade_id')
            ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
            ->where('transaction.transaction_date',$salary_trans_date)
            ->where('transaction.employment_id',$where)
            ->where('salary_payment.month',$month)
            ->where('salary_payment.year',$year);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row();
        }
    }
    public function year_salaries_slip($where_emp,$year)
    {
        $this->db->select('(CASE WHEN transaction.status=2 THEN salary_payment.transaction_amount ELSE 0 END) as salary_amount,
		ml_year.year,ml_month.month,(salary_payment.month) as sal_month,salary_payment.transaction_amount,
		salary_payment.transaction_id')
            ->from('employee')
            ->where($where_emp)
            ->where('transaction.status',2)
            ->where('salary_payment.year',$year)
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('salary','employment.employment_id=salary.employement_id','LEFT')
            ->join('salary_payment','salary.salary_id=salary_payment.salary_id','LEFT')
            ->join('transaction','transaction.transaction_id=salary_payment.transaction_id','LEFT')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','LEFT')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','LEFT')
            ->group_by('salary_payment.salary_payment_id');
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {return $query->result();}


    }
    public function last_salary($id)
    {
        $this->db->select('salary_payment.salary_payment_id,salary_payment.transaction_amount,ml_month.month,ml_year.year,salary_payment.arears')
            ->from('salary_payment')
            ->join('ml_month','salary_payment.month=ml_month.ml_month_id','left')
            ->join('ml_year','salary_payment.year=ml_year.ml_year_id','left')
            ->join('transaction','salary_payment.transaction_id=transaction.transaction_id','left')
            ->join('employee','transaction.employee_id=employee.employee_id','left')
            ->where('employee.employee_id',$id)
            ->where('transaction.status',2)
            ->limit(1,1)
            ->order_by('salary_payment_id','desc');
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }
    /// End

// Function for expense data view //
    public function expense_count($where)
    {
        $this->db->select('expense_claims.expense_claim_id')
            ->from('employee')
            ->join('employment','employment.employee_id=employee.employee_id','LEFT')
            ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
        ->where('employment.employment_id',$where);

        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();

        }
        //return $this->datatables->generate();
    }
    // Function for expense Claim records

    public function expense_view($limit,$page = NULL,$employmentID =NULL)
    {
        $this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
	ml_expense_type.expense_type_title,expense_claims.amount,expense_claims.expense_date,
	expense_claims.status,(expense_claims.expense_claim_id) as exp_id,
	expense_claims.expense_claim_id,employee.employee_id,expense_disbursment.expense_id,
	(expense_disbursment.month) as exp_dis_month,(expense_disbursment.year) as exp_dis_year,
	(expense_claims.month) as exp_month,(expense_claims.year) as exp_year,
	expense_disbursment.transaction_id,(expense_disbursment.expense_id) as exp_dis_id,expense_claims.paid,
	expense_claims.file_name,expense_claims.date_created')
            ->from('employee')
            ->join('employment','employment.employee_id=employee.employee_id','LEFT')
            ->join('expense_claims','employment.employment_id=expense_claims.employment_id','LEFT')
            ->join('ml_expense_type','ml_expense_type.ml_expense_type_id=expense_claims.expense_type','LEFT')

            ->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
            ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
            //->join('expense_payment','expense_payment.expense_id=expense_claims.expense_claim_id')

            ->join('ml_month','ml_month.ml_month_id=expense_claims.month','LEFT')
            ->join('ml_year','ml_year.ml_year_id=expense_claims.year','LEFT')
            ->join('ml_status','ml_status.status_id=expense_claims.status','LEFT')
            ->join('expense_disbursment','expense_claims.expense_claim_id=expense_disbursment.expense_id','LEFT')
            ->limit($limit,$page)
            ->order_by('expense_claims.expense_claim_id','desc')
            ->where('position_management.current',1)
            ->where('employment.employment_id',$employmentID)
            ->group_by('expense_claims.expense_claim_id');


        if($month=$this->input->post('month'))
        {
            $this->db->where('expense_claims.month',$month);
        }
        if($year=$this->input->post('year'))
        {
            $this->db->where('expense_claims.year',$year);
        }
        if($exp_type=$this->input->post('exp_type'))
        {
            $this->db->where('expense_claims.expense_type',$exp_type);
        }
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();

        }
        //return $this->datatables->generate();
    }

    // End
////////////// Function for master Lists
    public function master_list_data($table,$option,$key,$value)
    {
        $this->db->select('*');
        $query=$this->db->get($table);
        if($query->num_rows > 0)
        {
            foreach($query->result() as $row)
            {
                $data['']=$option;
                $data[$row->$key]= $row->$value;
            }
            return $data;
        }

    }
    //END
    //////////////// Function for advances view & count
    public function loan_advacne_count($id)
    {

        $where=array(
            'employment.employment_id'=>$id,
            'loan_advances.trashed'=>0);
        $this->db->select('loan_advances.loan_advance_id,employee.employee_id')
            ->from('employee')
            ->join('employment','employee.employee_id=employment.employee_id','LEFT')
            ->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')
            ->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        { return $query->result();
        }
    }

    public function loan_advacne_view($id,$limit,$page = NULL)
    {
        //$month=$this->input->post('month');
        //$year=$this->input->post('year');
        //$adnc_type=$this->input->post('adnc_type');
        $name=$this->input->post('name');
        $where=array(
            'employment.employment_id'=>$id,
            'loan_advances.trashed'=>0);
        $this->db->select('employee.employee_code,employee.full_name,ml_designations.designation_name,
	ml_payment_type.payment_type_title,ml_month.month,ml_year.year,loan_advances.amount,
	ml_status.status_title,(loan_advances.loan_advance_id) as loanadvances_id,
	loan_advances.loan_advance_id,employee.employee_id,
	loan_advances.date_created,loan_advances.status,
	(loan_advances.month) as loan_month,(loan_advances.year) as loan_year,
	loan_advances.paid,loan_advances.paid_back,(loan_advance_payment.transaction_id) as advance_trans_id,
	(transaction.status) as trans_status,loan_advances.balance,loan_advances.date_created')
            ->from('employee')
            ->join('employment','employment.employee_id=employee.employee_id','LEFT')
            ->join('loan_advances','employment.employment_id=loan_advances.employment_id','LEFT')

            ->join('position_management','position_management.employement_id=employment.employment_id','LEFT')
            ->join('ml_designations','ml_designations.ml_designation_id=position_management.ml_designation_id','LEFT')
            ->join('loan_advance_payment','loan_advance_payment.loan_advance_id=loan_advances.loan_advance_id','LEFT')
            ->join('transaction','transaction.transaction_id=loan_advance_payment.transaction_id AND transaction.employment_id=employment.employment_id','LEFT')
            ->join('ml_payment_type','ml_payment_type.payment_type_id=loan_advances.payment_type','LEFT')
            //->join('loan_advance_payback','loan_advance_payback.loan_advance_id=loan_advances.loan_advance_id','LEFT')
            ->join('ml_month','ml_month.ml_month_id=loan_advances.month','LEFT')
            ->join('ml_year','ml_year.ml_year_id=loan_advances.year','LEFT')
            ->join('ml_status','ml_status.status_id=loan_advances.status','LEFT')
            //->like('loan_advance_payment.month',$month)
            //->like('loan_advance_payment.year',$year)
            //->like('loan_advances.payment_type',$adnc_type)
            ->where('position_management.current',1)
            ->where($where)
            ->like('employee.full_name',$name)
            ->order_by('loan_advances.loan_advance_id','desc')
            ->limit($limit,$page);
        $this->db->group_by('loan_advances.loan_advance_id');

        if($month=$this->input->post('month'))
        {
            $this->db->where('loan_advances.month',$month);
        }

        if($year=$this->input->post('year'))
        {
            $this->db->where('loan_advances.year',$year);
        }

        if($adnc_type=$this->input->post('adnc_type'))
        {
            $this->db->where('loan_advances.payment_type',$adnc_type);
        }

        $query=$this->db->get();
        if($query->num_rows() > 0)
        { return $query->result();
        }

    }
    // END
    // Function get records where
    public function get_records_where($table,$where)
    {
        $this->db->select('*')
            ->from($table)
            ->where($where);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    // Function for leave notification
    public function get_records_where_desc($table,$where)
    {
        $this->db->select('*')
            ->from($table)
            ->where($where)
            ->order_by('notificationID','desc');
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }
    // Function for ESSP leave application view record

    public function leave_attendance_count($where)
    {
        $this->db->select('employee_code, full_name, ml_leave_type.leave_type,total_days,from_date,to_date,ml_status.status_title,application_id,leave_application.from_time,leave_application.to_time')
            ->from('employee')
            ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
            ->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','inner')
            ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')
            ->join('ml_leave_type','leave_application.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
            ->join('ml_status','leave_approval.status=ml_status.status_id','inner')
            //->group_by('leave_approval.leave_approval_id')
            ->where($where)
            ->order_by('employee.employee_id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }

    }

    public function leave_attendance($limit = NULL,$start = NULL,$full_name = NULL, $leave_type = NULL,$where= NULL)
    {
        $this->db->select('employee_code, full_name, ml_leave_type.leave_type,total_days,from_date,to_date,ml_status.status_title,application_date,application_id,employee.employee_id,leave_approval.leave_approval_id,leave_application.from_time,leave_application.to_time')
            ->from('employee')
            ->join('employment', 'employee.employee_id = employment.employee_id AND employment.current = 1 AND employment.trashed = 0', 'inner')
            ->join('leave_approval', 'leave_approval.employment_id = employment.employment_id','inner')
            ->join('leave_application','leave_approval.leave_application_id=leave_application.application_id','inner')
            ->join('ml_leave_type','leave_application.ml_leave_type_id=ml_leave_type.ml_leave_type_id','inner')
            ->join('ml_status','leave_approval.status=ml_status.status_id','inner')
            ->like('employee.full_name',$full_name)
            ->like('ml_leave_type.ml_leave_type_id',$leave_type)
            ->group_by('leave_approval.leave_approval_id')
            ->where($where)
            ->order_by('employee.employee_id','asc');

        //->where('from_date',date('Y-m-d'),'leave_application.status = 2');
        //$this->db->limit($limit,$start);
        if($limit != '' && $start != '' && $start != 0){
            $this->db->limit($limit, $start);
        }else if($limit != '' && $start == 0){
            $this->db->limit($limit);
        }
        //->group_by('leave_application.employee_id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }

    }
    // Function for Essp bar chart Project Task progress
    public function get_projectTask_progress($where)
    {
        $this->db->select('AJ.employment_id,AJ.project_id,
    AJ.assign_job_id,MLP.project_title,count(AJ.assign_job_id) as concat,AVG(DISTINCT PE.percent_achieved) as AvgProgress,group_concat(PE.percent_achieved) as percent')
            ->from('assign_job AJ')
            ->join('performance_evaluation PE','AJ.assign_job_id = PE.job_assignment_id','INNER')
            ->join('ml_projects MLP','MLP.project_id = AJ.project_id','INNER')
            ->where($where)
            ->group_by('AJ.project_id');
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();

        }
    }
// End

    /////////////////END
}
?>