<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan.
 * Date: 1/15/2015
 * Time: 6:04 PM
 */
class MY_Session extends CI_Session
{

    /**
     * Update an existing session
     *
     * @access    public
     * @return    void
     */
    function sess_update()
    { // skip the session update if this is an AJAX call!
        if (!IS_AJAX) {
            parent::sess_update();
        }
    }

    function sess_destroy()
    {
        $CI =& get_instance();
        //If the CI Session is Available Then We Will Pick Session Data From CI other wise, no data will be picked..
        if(isset($CI->session)){
            $employeeID = $CI->session->userdata('employee_id');
            $userID = $CI->session->userdata('UserID');
            $UTime = $CI->session->userdata('UTime');
        }
        $data = array(
            'logoutDateTime' => date("Y-m-d H:i:s"),
            'current' => 0
        );
        $where = array(
            'employee_id' => isset($employeeID)? $employeeID : $this->userdata('employee_id'),
            'user_id' => isset($userID)? $userID : $this->userdata('UserID'),
            'session_id' => isset($UTime)? $UTime :$this->userdata('UTime')//Not Using Session_ID as it is continuously Changing cant work with that, Need A work around..
        );

        //Update the Login-Logs Table If Session Is About To Destroy...
        $this->CI->db->update('users_login_logs', $data, $where);

        //call the parent
        parent::sess_destroy();
    }
}

/* End of file MY_Session.php */
/* Location: ./application/libraries/MY_Session.php */