<?php 
class Login extends CI_Controller{
		
		function __construct()
		{
			parent:: __construct();
		}
		
	public function index()
	{
		$this->load->library('form_validation');
		$this->load->library('session');
		//$this->load->view('login/login');
		$this->sign_in();
	}
		
	public function addAttendence_app()
	{
        $this->load->view('hrms_app/addAttendence_app'); 	
	}
	public function registerUser_app()
	{
        $this->load->view('hrms_app/registerUser_app'); 
	}
	public function apps_apk()
	{
        $this->load->view('Apps/Parexons'); 
	}
		 
		
	public function sign_in()
	{
		if($this->input->post('Login')) 
		{
			$this->form_validation->set_rules('user_name','user','required');
			$this->form_validation->set_rules('password','password','required');
			
			if($this->form_validation->run() == true)
			{
				$where 				=  array(
				'employee_email' 	=> $this->input->post('user_name'),
				'password'			=> md5($this->input->post('password'))
				);
				$query = $this->site_model->get_row_by_where('user_account',$where);
				 if($query)
				 {
					//echo print_r($query);die;	
					$employee_id 	= $query->employee_id;
					$sesion 		= array(
					'user_name'		=> $query->user_name,
					'employee_id'	=> $employee_id,
					'logged_in'		=> TRUE
					);
					$this->session->set_userdata($sesion);	
					redirect('dashboard_site/view_dashboard/');
              	}
			   	else{					
					$data['msg'] = "Invalid User Name or Password";
				}
			}
		}
		
		$data[''] = "";
		
		$this->load->view('login',$data);
	}
	public function log_out()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
/* End of Login Class */
}
?>