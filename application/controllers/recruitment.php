<?php
/**
 * Created by PhpStorm.
 * User: Syed Haider Hassan
 * Date: 3/17/2015
 * Time: 6:06 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @property common_model $common_model It resides all the methods which can be used in most of the controllers.
 **/
class recruitment extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('common_model');
    }

    function jobs_posted_list(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If Request Generated Through Proper Channel
                $table = 'job_advertisement JA';
                $selectData = array('advertisementID AS AdvertisementID, title AS AdvertisementTitle, datePosted AS AdPostedDate, expiryDate AS AdExpiryDate',false);
                $joins = array(
                    array(
                        'table' => 'employee E',
                        'condition' => 'E.employee_id = JA.postedBy AND E.trashed = 0',
                        'type' => 'LEFT'
                    )
                );
                $where = array(
                  'JA.trashed' => 0
                );
                $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where);
                echo $result;
            }
        }
    }

    function careers($list = NULL){
        if($list === 'list'){
            if($this->input->post()){
                if($this->input->is_ajax_request()){
                    //If User Came Through Proper Channel
                    $table = 'job_advertisement JA';
                    $selectData = array('
                        JA.advertisementID AS AdvertisementID,
                        JA.title AS Title,
                        DATE_FORMAT(JA.expiryDate,"%d-%m-%Y") AS ExpDate,
                        CONCAT(JA.minAgeRange," - ",JA.maxAgeRange) AS AgeLimit,
                        CONCAT(JA.salaryStartRange," - ",JA.salaryEndRange) AS SalaryRange,
                        JA.minQualification AS MinQualification,
                        MLD.department_name AS DepartmentName
                    ',false);
                    $joins = array(
                        array(
                            'table' => 'ml_department MLD',
                            'condition' => 'MLD.department_id = JA.department AND MLD.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'NOW() <=' => 'JA.expiryDate',
                        'JA.JobAD_ID'=>1
                    );
                    $add_column = array(
                        'detailsViewButton' => '<a style="cursor:pointer;cursor:hand;" class="detailView"><span class="fa fa-eye"></span></a>'
                    );
                    $filteredDisciplinaryAction = $this->input->post('filteredselectDepartments');

                    if(isset($filteredDisciplinaryAction) && !empty($filteredDisciplinaryAction)){
                        $where['JA.department'] = $filteredDisciplinaryAction;
                    }
                    $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$add_column);
                    echo $result;
                    return;
                }
            }
        }
        $viewData['pageTitle'] = 'Jobs Available';
        //Show The Careers Page..
        $this->load->view('recruitment/recruitment_header',$viewData);
        $this->load->view('recruitment/jobs_view');
        $this->load->view('recruitment/recruitment_footer');
    }

    function jobs_details_view($jobID){
        $headData['pageTitle'] = 'Job Details';

        if(!isset($jobID) || empty($jobID) || !is_numeric($jobID)){
            redirect('recruitment/careers');
        }

        //If Lets Suppose Reached To This Section Of Code, Then First Get the DropDowns
        $where = array(
            'trashed' => 0
        );
        $viewData['genderType'] = $this->common_model->classic_dropdown('ml_gender_type', '-- Select Gender --', 'gender_type_id', 'gender_type_title',$where);
        //Get Job Details From DB
        $PTable = 'job_advertisement JA';
        $selectData = array('
        title AS JobTitle,
        referenceNo AS ReferenceNo,
        GROUP_CONCAT(MLST.skill_name) AS RequiredSkills,
        description AS JobDescription,minQualification as MinQualification,
        CONCAT(minAgeRange," - ", maxAgeRange) AS AgeLimit,
        DATE_FORMAT(JA.datePosted,"%d-%b-%y") AS DatePosted,
        MLST.skill_name AS SkillName,JA.minExperience AS MinExperience,
        JA.transportFacilitie AS TransportFacilities,MLET.employment_type AS EmploymentType,
        MC.city_name AS CityName,JA.availablePositions as AvailablePositions,
        CONCAT(JA.salaryStartRange," - ",JA.salaryEndRange) AS SalaryLimit
  ',false);

        $joins = array(
            array(
                'table' => 'job_ad_required_skills JARS',
                'condition' => 'JARS.jobAdvertisementID = JA.advertisementID AND JARS.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table'=> 'ml_skill_type MLST',
                'condition' => 'MLST.skill_type_id = JARS.skillTypeID AND MLST.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_employment_type MLET',
                'condition' => 'MLET.employment_type_id=JA.employmentTypeID AND MLET.trashed=0',
                'type' => 'LEFT'
            ),
            array(
                'table'=>'ml_city MC',
                'condition'=>' MC.city_id=JA.cityID AND MC.trashed=0',
                'type'=>'LEFT'
            )
        );
        $where = array(
            'advertisementID' => $jobID
        );
        $viewData['result']= $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where,TRUE);
        $where_d = array('trashed' => 0);

        $viewData['qualification']= $this->common_model->dropdown_wd_option('ml_qualification_type', '-- Select Qualification --', 'qualification_type_id', 'qualification_title',$where_d);
        $where_S = array('trashed' => 0);

        $viewData['SKILLS']= $this->common_model->dropdown_wd_option('ml_skill_type', '-- Select Skills --', 'skill_type_id', 'skill_name',$where_S);
        $viewData['SKILLS_LEVELS']= $this->common_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$where_S);
        $viewData['SKILLS_LEVELS']= $this->common_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$where_S);
        $this->load->view('recruitment/recruitment_header',$headData);
        $this->load->view('recruitment/jobs_details_view',$viewData);
        $this->load->view('recruitment/recruitment_footer');
    }

    public function SendMail()
    {

        $this->load->library('email',$this->data['mailConfiguration']);

        $MailAddress = $this->input->post('MailAddress');
        $MailSubject = $this->input->post('MailSubject');
        $MailMessage = $this->input->post('MailMessage');

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['noReply'],$this->data['Company']); // Change these details
        $this->email->to($MailAddress);
        $this->email->subject($MailSubject);
        $this->email->message($MailMessage);
        if($this->email->send()){
            echo "OK::Mail Sent Successfully::success";
            return;
        }
    }

    public function load_all_available_skills(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "ml_skill_type";
                $data = array('skill_type_id AS SkillID','skill_name AS SkillName');
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'skill_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,'',FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields($tbl,$data);
                }
                /*Print the Json Result for All the Available Skills*/
                print_r(json_encode($result));
            }
        }
    }
    function get_selected_skills()
    {  ////// code for selected Skills
        //$selecteddata='group_concat(ApSkill.SkillID) as Skill,ApSkill.appSkillID,group_concat(MLSKL.skill_name) as SkillName';
        $selecteddata='ApSkill.SkillID,ApSkill.appSkillID,group_concat(MLSKL.skill_name) as SkillName';
        $where_skill=array('ApSkill.applicantID'=>$this->session->userdata('applicantID'),
            'ApSkill.trashed'=>0);
        $tbl='applicant_skills ApSkill';
        $joinsSkill=array(
            array(
                'table'=>'ml_skill_type MLSKL',
                'condition'=>'ApSkill.SkillID=MLSKL.skill_type_id',
                'type'=>'INNER'
            )

        );
        $viewData['skills']=$this->common_model->select_fields_where_like_join($tbl,$selecteddata,$joinsSkill,$where_skill,true);
        echo json_encode( $viewData['skills']);
        return;
    }
    public function load_all_available_skill_level(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "ml_skill_level";
                $data = array('level_id AS LevelID','skill_level AS SkillLevel');
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'skill_level';
                    $where=array('trashed'=>0);
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields($tbl,$data);
                }
                /*Print the Json Result for All the Available Skills*/
                print_r(json_encode($result));
            }
        }
    }
    function get_job_details(){
        if($this->input->post()){
            if($this->input->is_ajax_request()){
                //If User Has Access This Method Through a Proper Channel The Get the Data for User.
                //Check If We Are Getting The Required POST Data or Not..
                $advertisementID = $this->input->post('adID');
                if(!isset($advertisementID) || empty($advertisementID)){
                    echo "FAIL::There was Some Issue With The Ajax POST, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                $table = 'job_advertisement JA';
                $selectData = array('advertisementID AS AdvertisementID,
                title AS JobTitle,
                description AS JobDescription,
                 expiryDate AS ExpiryDate,
                 availablePositions AS AvailablePositions,
                 CONCAT(minAgeRange," - ",maxAgeRange) AS AgeLimit,
                 CONCAT(salaryStartRange," - ",salaryEndRange) AS SalaryRange,
                 minQualification AS MinQualification,
                 MLD.department_name AS DepartmentName,
                 PE.full_name AS PostedByEmployeeName
                ',false);
                $joins = array(
                    array(
                        'table' => 'ml_department MLD',
                        'condition' => 'MLD.department_id = JA.department AND MLD.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'employee PE',
                        'condition' => 'PE.employee_id = JA.postedBy AND PE.trashed = 0',
                        'type' => 'LEFT'
                    )
                );
                $where = array(
                    'advertisementID' => $advertisementID
                );

                //Finally The Query Time.
                $result = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,TRUE);
                if(isset($result) && !empty($result)){
                    echo json_encode($result);
                    return;
                }else{
                    echo "FAIL::No record Found, If You Feel You Got This Message In Error than Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }

    function get_applicant_details(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If User Came Through A Proper Channel, The Let Him Get The Applicant Details.
                $jobAdvertisementID = $this->input->post('jobAdvertisementID');
                if(!isset($jobAdvertisementID) || empty($jobAdvertisementID) || !is_numeric($jobAdvertisementID)){
                    echo "FAIL::Something Went Wrong With The POST, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                $applicantID = $this->session->userdata('applicantID');
                //So If Data is Posted Right We Can Move Forward.
                $applicantTable = 'applicants';
                $selectData = array('
                    firstName AS ApplicantFirstName,
                    lastName AS ApplicantLastName,
                     CNIC AS ApplicantCNIC,
                     fatherName AS FatherName,
                     officialEmailAddress,
                     avatar AS Avatar,
                     genderID AS ApplicantGender,
                     currentAddress AS ApplicantCurrentAddress,
                     permanentAddress AS ApplicantPermanentAddress,
                     residentPhone AS ResidentPhone,
                     officialEmailAddress AS Email,
                     contactNo AS ContactNo
                    ',false);
                $where = array(
                    'applicantID' => $applicantID
                );
                $result = $this->common_model->select_fields_where($applicantTable,$selectData,$where,TRUE);
                if(isset($result) && !empty($result)){
                    $empAvatar = empAvatarExist($result->Avatar);
                    if( $empAvatar === TRUE){
                        $result->Avatar = base_url().'/upload/Thumb_Nails/'.$result->Avatar;
                    }else{
                        $result->Avatar = $empAvatar;
                    }
                }
                echo json_encode($result);
            }
        }
    }
    //Apply For Job Function
    function apply_for_job(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //Check If User is LoggedIn or Not
                if(LoggedIn() === FALSE){
                    echo "FAIL::In Order To Apply For Job, Register/Login First::error";
                    return;
                }
                //Code In This Section Should Only Execute If Requested Through Proper Channel.
                //Getting The Required Data From POST
//                //But First Lets Do Little Validations On Some Fields.
                $this->form_validation->set_rules('firstName', 'First Name', 'trim|required|min_length[3]|xss_clean');
                $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|min_length[3]|xss_clean');
                $this->form_validation->set_rules('fatherName', 'Father Name', 'trim|xss_clean');
                $this->form_validation->set_rules('nic1', 'First CNIC Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('nic2', 'Mid CNIC Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('nic3', 'Last CNIC Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('gender', 'Gender', 'trim|numeric|xss_clean');
                //$this->form_validation->set_rules('skill', 'Skill', 'trim|required|xss_clean');


                //Contact Page Validations
                $this->form_validation->set_rules('currentAddress', 'Current Address', 'trim|xss_clean');
                $this->form_validation->set_rules('permanentAddress', 'Permanent Address', 'trim|xss_clean');
                $this->form_validation->set_rules('contactNumber', 'Contact Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('residentPhone', 'Resident Address', 'trim|xss_clean');
                $this->form_validation->set_rules('officialMail', 'Email', 'trim|valid_email|xss_clean');

                $this->form_validation->set_message('required', '<b>%s</b> field is required.');

                $jobAdvertisementID = $this->input->post('jobAdvertisementID');
                if(!isset($jobAdvertisementID) || empty($jobAdvertisementID) || !is_numeric($jobAdvertisementID)){
                    echo 'FAIL:: Something Went Wrong with the POST, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

                //Check For Posted Files.
                if(isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])){
                    $avatarFileName = $_FILES['avatar']['name'];
                }
                if(isset($_FILES['resume']['name']) && !empty($_FILES['resume']['name'])){
                    $resumeFileName = $_FILES['resume']['name'];
                    $allowedExtForResume = array('jpeg','jpg','png','gif','doc','docx','txt');
                }

                if (isset($avatarFileName) && !empty($avatarFileName)) {
                    //If Extension is Not In Defined Array
                    //First Lets Work On Avatar
                    $allowedExtForImage = array('jpeg','jpg','png','gif');
                    $avatarExtension = end(explode('.',$avatarFileName));
                    if (!in_array(strtolower(end(explode('.', $avatarFileName))), $allowedExtForImage)) {
                        echo "FAIL:: Only Image JPEG, PNG and GIF Images Allowed, No Other Extensions will be Accepted::error";
                        return;
                    }
                }

                //Now Lets Check Attachment Has Proper Extension Or Not.
                if(isset($resumeFileName) && !empty($resumeFileName)){
                    $resumeExtension = end(explode('.',$resumeFileName));
                    if(!in_array(strtolower(end(explode('.',$resumeFileName))),$allowedExtForResume))
                    {
                        echo "FAIL:: Resume Extensions Can Only Be \" JPEG, PNG, GIF, PDF, DOC, DOCX, TXT \", No Other Extensions will be Accepted. ::error";
                        return;
                    }
                }


                if ($this->form_validation->run() == FALSE) {
                    $errors = validation_errors();
                    echo "FAIL::" . $errors . "::error";
                    return;
                }

                //If Everything Went Fine. Then Do the Stuff This Function is Required To.


                ///Getting All Posted Data
                $firstName = $this->input->post('firstName');
                $lastName = $this->input->post('lastName');
                $fatherName = $this->input->post('fatherName');
                $cnic = $this->input->post('nic1') . "-" . $this->input->post('nic2') . "-" . $this->input->post('nic3');
                $genderID = $this->input->post('gender');
                $currentAddress = $this->input->post('currentAddress');
                $permanentAddress = $this->input->post('permanentAddress');
                $contactNumber = $this->input->post('contactNumber');
                $residentPhone = $this->input->post('residentPhone');
                $officialMail = $this->input->post('officialMail');
                //$skill = $this->input->post('skill');


                //echo "<pre>";
                //print_r($explodeSkill); die;

                //First We Need To Check If User is Applicant Or Not.
                $applicantID = $this->session->userdata('applicantID');
                if(isset($applicantID) && !empty($applicantID) && is_numeric($applicantID)){
                    //If There is ApplicantID in Session, Then We Can Further Proceed The Process of Application.
                    //If Reached Up To This Point, Means User is Applicable and All The Required Fields Are Present To Apply For Job.
                    //First Update Details In the Applicant Table.
                    $applicantTable = 'applicants';
                    $updateData = array(
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                        'fatherName' => $fatherName,
                        'CNIC' => $cnic,
                        'genderID' => $genderID,
                        'currentAddress' => $currentAddress,
                        'permanentAddress' => $permanentAddress,
                        'contactNo' => $contactNumber,
                        'residentPhone' => $residentPhone,
                        'officialEmailAddress' => $officialMail,
                        'profileDateUpdated' => date('Y-m-d')
                    );
                    $where = array(
                        'applicantID' => $applicantID
                    );

                    if (isset($avatarFileName) && !empty($avatarFileName)) {
                        //First Setting Up The Avatar Settings e-g upload path, filename etc
                        $uploadPath = './upload/Thumb_Nails/';
                        $uploadDirectory = './upload/Thumb_Nails';
                        $AvatarFileName = "HRMS_Applicant_" . $applicantID . "_avatar_" . time() . "." . $avatarExtension;

                        if (!is_dir($uploadDirectory)) {
                            mkdir($uploadDirectory, 0755, true);
                        }

                        move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadPath . $AvatarFileName);
                        $updateData['avatar'] = $AvatarFileName;
                    }

                    if (isset($resumeFileName) && !empty($resumeFileName)) {
                        //Need To Do Work For Resume..
                        $ResumeFileName = "HRMS_Applicant_" . $applicantID . "_resume_" . time() . "." . $resumeExtension;
                        $resumeUploadPath = './upload/ApplicantResumes/';
                        $resumeUploadDirectory = './upload/ApplicantResumes';
                        //Created The Directory If Don't Exist;
                        if(!is_dir($resumeUploadDirectory)){
                            mkdir($resumeUploadDirectory, 0755, true);
                        }
                        //Now Upload The File To Specified Directory
                        move_uploaded_file($_FILES['resume']['tmp_name'], $resumeUploadPath . $ResumeFileName);
                        $updateData['attachment'] = $ResumeFileName;
                    }

                    $result = $this->common_model->update($applicantTable, $where, $updateData);
/*                    if($result === true){
                        echo "OK::Record Successfully Updated::success";
                        return;
                    }else{
                        echo "FAIL::Something Went Wrong With The Database, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }*/
                    //Now Need To Add Skills of Applicant
                    // First we explode Skills Id's
                   /* $explodeSkill=explode(',',$skill);
                    foreach($explodeSkill as $skillId)
                    {
                        $SkillArray=array(
                            'SkillID'=>$skillId,
                            'applicantID'=>$applicantID,
                            'dateAdded'=>date("Y-m-d")
                        );

                         $this->common_model->insert_else_ignore('applicant_skills',$SkillArray);
                    }*/

                    //Now Need To Let User Apply For Job.
                    if(can_apply_for_job($applicantID,$jobAdvertisementID) === FALSE){
                        echo "FAIL::You Might Have Already Applied For This Job, Or You Are Not Allowed To Apply For This Post::error";
                        return;
                    }
                    $jobAdvertisementTable = 'applicant_applied_jobs';
                    $insertData = array(
                        'applicantID' => $applicantID,
                        'jobAdvertisementID' => $jobAdvertisementID,
                        'dateAppliedOn' => date('Y-m-d')
                    );
                    $result = $this->common_model->insert_record($jobAdvertisementTable,$insertData);
                    if($result > 0){
                        echo "OK::Successfully Applied For Job::success";
                        return;
                    }

                }else{
                    echo "FAIL::You Might Not Be Authorized To Apply For This Job, If You Feel that you are getting This Message In Error Then Please Logout and Log Back In::error";
                    return;
                }
                //Need To Insert Data in Three(3) Different Tables.

            }
        }
    }


    public function RegisterApplicantAccount(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
                if ($this->form_validation->run() == FALSE) {
                    $errors = validation_errors();
                    echo "FAIL::".$errors."::error";
                    return;
                }

                 $email     = $this->input->post('email');
                 $UserName  = $this->input->post('UserName');
                 $FirstName = $this->input->post('FirstName');
                 $LastName   = $this->input->post('LastName');

                /////Table Applicant
                $selectTable = 'applicants';
                //Before Insertion We First Need To Check If Record Already Exist Against Same Type.
                $selectData = array('COUNT(1) AS TotalEmail');
                $where = 'officialEmailAddress = "'.$email.'" OR username = "'.$UserName.'"';
                $selectResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
                //print_r($selectResult);die;
                $this->form_validation->set_rules('UserName', 'User Name', 'trim|required|min_length[5]|max_length[50]|strtolower|xss_clean');
                if ($this->form_validation->run() == FALSE) {
                    $errors = validation_errors();
                    echo "FAIL::".$errors."::error";
                    return;
                }
                if($selectResult-> TotalEmail > 0){
                    echo "FAIL::Email or Username Already Exist In The System::error";
                    return;
                }

                //////// Contact Address
                $selectTable = 'current_contacts';
                //Before Insertion We First Need To Check If Record Already Exist Against Same Type.
                $selectData = array('COUNT(1) AS TotalEmail');
                $where = array(
                    'email_address' => $email,
                    'trashed' => 0
                );
                $selectResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
                if($selectResult-> TotalEmail > 0){
                    echo "FAIL::Email Already Exist In The System::error";
                    return;
                }

                  //////// User Account //////
                $selectTable = 'user_account';
                //Before Insertion We First Need To Check If Record Already Exist Against Same Type.
                $selectData = array('COUNT(1) AS TotalEmail');
                $where = '(employee_email = "'.$email.'" OR user_name = "'.$UserName.'") AND trashed = 0';
                $selectResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
                if($selectResult->TotalEmail > 0){
                    echo "FAIL::Email or Username Already Exist In The System::error";
                    return;
                }

                //As Everything Worked Fine. Lets Do The Updates..
                //Insertion in to Applicants Table
                $t = explode(" ",microtime());
              $ActivationCode="Xavbr_HHGR-XXYaaahRGHI-JHSFGE-90JH".substr($t[0],1,6);
                $insertTable = 'applicants';
                $insertData = array (
                    'firstName' => $FirstName,
                    'lastName'  => $LastName,
                    'fullName'  =>$FirstName . " " . $LastName,
                    'officialEmailAddress' => $email,
                    'username'=> $UserName,
                    'password'=>$this->input->post('Password'),
                    'profileDateCreated' => date('Y-m-d'),
                    'email_verification_code' => $ActivationCode
                );

                //Lets Keep The Registration Activated By Default
                $insertData['active_status'] = 1;

                $insertResult = $this->common_model->insert_record($insertTable,$insertData);
                $id=$this->db->insert_id();
                if($insertResult > 0){
                    $messageSubject="Please Confirm Your Account Activation !";
                    $messageBody="Dear <br>".$FirstName . " " . $LastName."</b> to Confirm Your Account Activation please click link below if link does not work then copy link and past in browser .<br>".base_url()."recruitment/activation/".$id."/".$ActivationCode." <br> Your UserName:".$UserName." <br> password:".$this->input->post('Password')." <br>  Thanks .";
                    $config=$this->data['mailConfiguration'];
                    $this->load->library('email',$config);
                    $this->email->set_newline("\r\n");
                    //Now Lets Send The Email..

                    $this->email->to($email);

                   $this->email->from($this->data['noReply'],$this->data['Company']);
                    $this->email->subject($messageSubject);
                    $this->email->message($messageBody);
                    if($this->email->send()) {
                        echo 'OK::Verification Code has been sent to Your Email::success';
                    } else {
                        $this->email->print_debugger();
                        echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                        return;
                    }
                 /*    echo "OK::Record Successfully Added To The System::success";
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                    return;*/
                }
            }

            }

        }
    function activation()
    {
        $id=$this->uri->segment(3);
        $code=$this->uri->segment(4);
        $result=$this->common_model->get_record_where('applicants',array('applicantID'=>$id));
        if(!empty($result)){
            if($result->active_status == 1)
            {
                echo "<span><h2>Your Account has been Already Verified !</h2></span>"; return;
            }
           $where=array('applicantID'=>$id,'email_verification_code'=>$code);
            $query=$this->common_model->update('applicants',$where,array('active_status'=>1));
            if($query)
            {
                $msg="Thank You Your Account has been Verified";
                $this->session->set_flashdata('msg',$msg);
                redirect('recruitment/careers');

                return;}
            else{echo "Sorry Your Verification Code is Incorrect"; return;}
        }else{echo "<span><h2>Sorry No Record Found !</h2></span>"; return;}
    }
    function applicantLogin(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                if(LoggedIn() === TRUE){
                    echo "FAIL::You are Already Logged In, If you Feel You are receiving this message in Error, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                $this->form_validation->set_rules('UserName', 'User Name', 'trim|required|min_length[5]|max_length[50]|strtolower|xss_clean');
//                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');
                if ($this->form_validation->run() == FALSE) {
                    $errors = validation_errors();
                    echo "FAIL::".$errors."::error";
                    return;
                }
                //Login Applicant If Credentials Are Correct.
                $username = strtolower($this->input->post('UserName'));
                $password = $this->input->post('password');
                $rememberMe = $this->input->post('remember_me');

                $table = 'applicants APP';
                $selectData = ('COUNT(1) AS TotalRecordsFound');
                $whereData = array(
                    'LOWER(username)' => $username,
                    'password' => $password,
                    'active_status'=>1
                );
                $result = $this->common_model->select_fields_where($table,$selectData,$whereData,TRUE);

                // print_r($result);die;
                if(isset($result) && $result->TotalRecordsFound > 0){
                    $selectData = array(
                        'applicantID AS ApplicantID,
                        fullName AS FullName,
                        username AS Username,
                        avatar AS Avatar
                        ',false);
                    $applicantDetails = $this->common_model->select_fields_where($table,$selectData,$whereData,TRUE);
                   // print_r($applicantDetails);die;


                    //Need To Set Values In Session For Later Use.
                    $sessionData = array(
                        'applicantID' => $applicantDetails->ApplicantID,
                        'applicantFullName' => $applicantDetails->ApplicantID,
                        'applicantUsername' => $applicantDetails->Username,
                        'applicantAvatar' => $applicantDetails->Avatar,
                        'logged_in' => TRUE
                    );
                    $this->session->set_userdata($sessionData);

                    if(isset($rememberMe) && !empty($rememberMe) && $rememberMe === "on"){
                        $cookie = array(
                            'name'   => 'remember_me_token',
                            'value'  => 'fdasjkui23jbdsk7u3%%8dsk',
                            'expire' => '1209600',  // Two weeks
                            'domain' => '.parexonsHRMS',
                            'path'   => '/'
                        );
                        set_cookie($cookie);
                    }

                    //Need To Show The Message.
                    echo "OK::User Successfully LoggedIN::success";
                    return;
                }else{
                    echo "FAIL::No Record Found, Please Enter Correct User Credentials To Login OR Your Account is not Verified !::error";
                    return;
                }

            }
        }
    }
    //Function for forgot Password
    function forgotPassword()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                //Login Applicant If Credentials Are Correct.
                $EmailAddress = strtolower($this->input->post('EmailAddress'));
                $table = 'applicants APP';
                $selectData = ('COUNT(1) AS TotalRecordsFound');
                $whereData = array(
                    'LOWER(officialEmailAddress)' => $EmailAddress,
                    'active_status' => 1
                );
                $result = $this->common_model->select_fields_where($table, $selectData, $whereData, TRUE);

                // print_r($result);die;
                if (isset($result) && $result->TotalRecordsFound > 0) {
                    $selectData = array(
                        '
                         username AS Username,
                         password
                        ', false);
                    $applicantDetails = $this->common_model->select_fields_where($table, $selectData, $whereData, TRUE);

                    //Need To Set Values In Session For Later Use.
                    $sessionData = array(
                        'applicantUsername' => $applicantDetails->Username,
                        'applicantPassword' => $applicantDetails->password
                    );

                    $emailMessage = "
                    <table>
                       <tr style='border-bottom: 1px solid black'>
                          <td>User Name And Password</td>
                       </tr>
                       <tr>
                          <td>User Name</td>
                          <td>$applicantDetails->Username</td>
                       </tr>
                        <tr>
                          <td>Password</td>
                          <td>$applicantDetails->password</td>
                       </tr>
                    </table>";
                    $MailAddress = $EmailAddress;
                    $MailSubject = 'Your UserName and Password';
                    $this->load->library('email', $this->data['mailConfiguration']);
                    $this->email->set_newline("\r\n",$MailAddress);
                    $this->email->from($this->data['noReply'], $this->data['Company']); // Change these details
                    $this->email->to($MailAddress);
                    $this->email->subject($MailSubject);
                    $this->email->message($emailMessage);
                    if ($this->email->send()) {
                        echo "OK::Password  Sent Successfully::success";
                        return;
                    }
                }
                else {
                    echo "FAIL::Your Email is not Verified Please try again!::error";
                    return;
                }
            }
        }
    }
    // Function for Add qualification of applicant
    public function add_qualification()
    {
        $post=$this->input->post();
        if(isset($post))
        {
            $qualification_type_id=$this->input->post('qualification_type_id');
            $DegreeTitle=$this->input->post('DegreeTitle');
            $Institute=$this->input->post('Institute');
            $Specialization=$this->input->post('Specialization');
            $YearComp=$this->input->post('YearComp');
            $gpa=$this->input->post('gpa');
            if(isset($qualification_type_id) && empty($qualification_type_id))
            {
                echo "FAIL::Please select Qualification Title  !::error";
                return;
            }
            if(isset($YearComp) && !is_numeric($YearComp))
            {
                echo "FAIL::Please Enter Year in numbers !::error";
                return;
            }

            $data=array(
                'title'=>$DegreeTitle,
                'qualification_type_id'=>$qualification_type_id,
                'institute'=>$Institute,
                'specialization'=>$Specialization,
                'comp_year'=>$YearComp,
                'gpa_division'=>$gpa,
                'applicant_id'=>$this->session->userdata('applicantID')
            );

            $query=$this->common_model->insert_record('applicant_qualification',$data);
            if($query)
            {
                echo "OK:: Record add successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Insertion Failed !::error";
                return;
            }

        }
    }
    public function fetch_qualification()
    {
           $applicant=$this->input->post('applicantID');
        $table='applicant_qualification ApQlf';
        $data='ApQlf.title,MlQLTP.qualification_title,ApQlf.institute,ApQlf.comp_year,ApQlf.apql_id AS id';
        $join=array(
            array(
                'table'=>'applicants Appl',
                'condition'=>'Appl.applicantID = ApQlf.applicant_id',
                'type'=>'INNER'
            ),
            array(
                'table'=>'ml_qualification_type MlQLTP',
                'condition'=>'ApQlf.qualification_type_id = MlQLTP.qualification_type_id',
                'type'=>'INNER'
            )
        );
        $where=array('ApQlf.applicant_id'=>$applicant,'ApQlf.trashed'=>0);
        $data=$this->common_model->select_fields_where_like__orLikes_join($table,$data,$join,$where,false);
        echo json_encode($data);
        return;
    }
    // END

    public function trashed_qualification()
    {
        $id=$this->input->post('id');
        $query=$this->common_model->update('applicant_qualification',array('apql_id'=>$id),array('trashed'=>1));
        if($query)
        {
            echo "OK:: Record Trashed Successfully !::success";
            return;
        }else{
            echo "FAIL:: Record Trashing Failed !::error";
            return;
        }

    }
    public function update_qualification()
    {
        $Uid=$this->input->post('Uid');
        $applicant=$this->input->post('applicantID');
        $table='applicant_qualification ApQlf';
        $data='ApQlf.title,MlQLTP.qualification_title,ApQlf.qualification_type_id,ApQlf.institute,ApQlf.comp_year,ApQlf.specialization,ApQlf.gpa_division,ApQlf.apql_id AS id';
        $join=array(
            array(
                'table'=>'applicants Appl',
                'condition'=>'Appl.applicantID = ApQlf.applicant_id',
                'type'=>'INNER'
            ),
            array(
                'table'=>'ml_qualification_type MlQLTP',
                'condition'=>'ApQlf.qualification_type_id = MlQLTP.qualification_type_id',
                'type'=>'INNER'
            )
        );
        $where=array('ApQlf.apql_id'=>$Uid,'ApQlf.trashed'=>0);
        $data=$this->common_model->select_fields_where_like__orLikes_join($table,$data,$join,$where,true);
        echo json_encode($data);
        return;
    }
    // Function for Add qualification of applicant
    public function update_changes_qualification()
    {
        $post=$this->input->post();
        if(isset($post))
        {
            $qualification_type_id=$this->input->post('qualification_type_id');
            $DegreeTitle=$this->input->post('EDegree');
            $Institute=$this->input->post('EInstitute');
            $Specialization=$this->input->post('ESpecialization');
            $YearComp=$this->input->post('ECompYear');
            $gpa=$this->input->post('EGpa');
            $EQid=$this->input->post('EQid');
            if(isset($qualification_type_id) && empty($qualification_type_id))
            {
                echo "FAIL::Please select Qualification Title  !::error";
                return;
            }
            if(isset($YearComp) && !is_numeric($YearComp))
            {
                echo "FAIL::Please Enter Year in numbers !::error";
                return;
            }

            $data=array(
                'title'=>$DegreeTitle,
                'qualification_type_id'=>$qualification_type_id,
                'institute'=>$Institute,
                'specialization'=>$Specialization,
                'comp_year'=>$YearComp,
                'gpa_division'=>$gpa
            );

            $query=$this->common_model->update('applicant_qualification',array('apql_id'=>$EQid),$data);
            if($query)
            {
                echo "OK:: Record Update successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Updating Failed !::error";
                return;
            }

        }
    }
    // Function for add Experience
    // Function for Add Experience of applicant
    public function add_Experience()
    {
        $post=$this->input->post();
        if(isset($post))
        {
          $ExpPosition=$this->input->post('ExpPosition');
           $Company=$this->input->post('Company');
          $FromDate=$this->input->post('FromDate');
          $ToDate=$this->input->post('ToDate');
            $TDate=date("Y-m-d",strtotime($ToDate));
            $FDate=date("Y-m-d",strtotime($FromDate));

            if(isset($ExpPosition) && empty($ExpPosition))
            {
                echo "FAIL::Please Enter Position Title  !::error";
                return;
            }
            if(isset($Company) && empty($Company))
            {
                echo "FAIL::Please Enter Company Name !::error";
                return;
            }
            if(isset($FromDate) && empty($FromDate))
            {
                echo "FAIL::Please Enter Joining Date !::error";
                return;
            }
            if(isset($ToDate) && empty($ToDate))
            {
                echo "FAIL::Please Enter Leave Date !::error";
                return;
            }

            $data=array(
                'Exp_position'=>$ExpPosition,
                'Company'=>$Company,
                'FromDate'=>$FDate,
                'ToDate'=>$TDate,
                'ApplicantID'=>$this->session->userdata('applicantID')
            );

            $query=$this->common_model->insert_record('applicant_experience',$data);
            if($query)
            {
                echo "OK:: Record add successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Insertion Failed !::error";
                return;
            }

        }
    }
    // End
    public function fetch_Experience()
    {
        $applicant=$this->input->post('ApplID');
        $table='applicant_experience ApExp';
        $data='ApExp.Exp_position,ApExp.Company,ApExp.FromDate,ApExp.ToDate,ApExp.AppExpereinceID AS id';

        $where=array('ApExp.ApplicantID'=>$applicant,'ApExp.trashed'=>0);
        $result=$this->common_model->select_fields_where($table,$data,$where,FALSE,$field='',$value='',$group_by='',$order_by='AppExpereinceID desc');

        echo json_encode($result);
        return;
    }
    // END
    public function update_exp()
    {
        $id=$this->input->post('Uid');
        $table='applicant_experience ApExp';
        $data='ApExp.Exp_position,ApExp.Company,ApExp.FromDate,ApExp.ToDate,ApExp.AppExpereinceID AS id';

        $where=array('ApExp.AppExpereinceID'=>$id,'ApExp.trashed'=>0);
        $result=$this->common_model->select_fields_where($table,$data,$where,TRUE,$field='',$value='',$group_by='',$order_by='');

        echo json_encode($result);
        return;
    }

    // Function for Update Experience
    // Function for Update Experience of applicant
    public function UpdateRec_Experience()
    {
        $post=$this->input->post();
        if(isset($post))
        {
            $EPosition=$this->input->post('EPosition');
            $ECompany=$this->input->post('ECompany');
           $EFromDate=$this->input->post('EFromDate');
            $EToDate=$this->input->post('EToDate');
            $Expid=$this->input->post('Expid');
            $ETDate=date("Y-m-d",strtotime($EToDate));
            $EFDate=date("Y-m-d",strtotime($EFromDate));

            if(isset($EPosition) && empty($EPosition))
            {
                echo "FAIL::Please Enter Position Title  !::error";
                return;
            }
            if(isset($ECompany) && empty($ECompany))
            {
                echo "FAIL::Please Enter Company Name !::error";
                return;
            }
            if(isset($EFromDate) && empty($EFromDate))
            {
                echo "FAIL::Please Enter Joining Date !::error";
                return;
            }
            if(isset($EToDate) && empty($EToDate))
            {
                echo "FAIL::Please Enter Leave Date !::error";
                return;
            }

            $data=array(
                'Exp_position'=>$EPosition,
                'Company'=>$ECompany,
                'FromDate'=>$EFDate,
                'ToDate'=>$ETDate
            );
            $where=array('AppExpereinceID'=>$Expid);
            $query=$this->common_model->UPDATE('applicant_experience',$where,$data);
            if($query)
            {
                echo "OK:: Record Update successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Updating Failed !::error";
                return;
            }

        }
    }
    public function trashed_exp()
    {
        $id=$this->input->post('id');
        $query=$this->common_model->update('applicant_experience',array('AppExpereinceID'=>$id),array('Trashed'=>1));
        if($query)
        {
            echo "OK:: Record Trashed Successfully !::success";
            return;
        }else{
            echo "FAIL:: Record Trashing Failed !::error";
            return;
        }

    }

    // Function for Add Skills of applicant
    public function add_Skill()
    {
        $post=$this->input->post();
        if(isset($post))
        {
            $SkillTypeID=$this->input->post('skill_type_ids');
            $level=$this->input->post('level');


            if(!isset($SkillTypeID) || empty($SkillTypeID))
            {
                echo "FAIL::Please Enter Your Skills  !::error";
                return;
            }
            if(!isset($level) || empty($level))
            {
                echo "FAIL::Please Select Level of Skill!::error";
                return;
            }


            $data=array(
                'SkillID'=>$SkillTypeID,
                'skill_level'=>$level,
                'dateAdded'=>date('Y-m-d'),
                'applicantID'=>$this->session->userdata('applicantID'),
                'trashed'=>0
            );
            $where=array('applicantID'=>$this->session->userdata('applicantID'),'trashed'=>0);
            $Exist=$this->common_model->get_row_where('applicant_skills',$where);
            if($Exist){
                echo "FAIL:: Sorry Already Exist !::error";
                return;
            }else{
            //$query=$this->common_model->insert_record('applicant_skills',$data);
            $query=$this->common_model->insert_else_ignore('applicant_skills',$data);
            if($query)
            {
                echo "OK:: Record add successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Insertion Failed !::error";
                return;
            }}

        }
    }
    // End


    // Function For Applicant Skills //
    public function fetch_skill()
    {
        $applicant=$this->input->post('applicantID');
        $table='applicant_skills ApSk';
        $data='MLST.skill_name,MLSL.skill_level,ApSk.appSkillID AS id';
        $join=array(
            array(
                'table'=>'applicants Appl',
                'condition'=>'Appl.applicantID = ApSk.applicantID',
                'type'=>'INNER'
            ),
            array(
                'table'=>'ml_skill_type MLST',
                'condition'=>'MLST.skill_type_id = ApSk.SkillID',
                'type'=>'INNER'
            ),
            array(
                'table'=>'ml_skill_level MLSL',
                'condition'=>'MLSL.level_id = ApSk.skill_level',
                'type'=>'INNER'
            )
        );
        $where=array('ApSk.applicantID'=>$applicant,'ApSk.trashed'=>0);
        $data=$this->common_model->select_fields_where_like__orLikes_join($table,$data,$join,$where,false);
        echo json_encode($data);
        return;
    }
    // END

// Function for Update Skill ///
    public function update_Skills()
    {
        $Sid=$this->input->post('Sid');
        //$applicant=$this->input->post('applicantID');
        $table='applicant_skills ApSk';
        $data='MLST.skill_name,ApSk.skill_level AS SkillLevel,ApSk.appSkillID AS id,ApSk.SkillID AS SID';
        $join=array(
            array(
                'table'=>'applicants Appl',
                'condition'=>'Appl.applicantID = ApSk.applicantID',
                'type'=>'INNER'
            ),
            array(
                'table'=>'ml_skill_type MLST',
                'condition'=>'MLST.skill_type_id = ApSk.SkillID',
                'type'=>'INNER'
            )
        );
        $where=array('ApSk.appSkillID'=>$Sid,'ApSk.trashed'=>0);
        $data=$this->common_model->select_fields_where_like__orLikes_join($table,$data,$join,$where,true);
        echo json_encode($data);
        return;
    }
    // END
    // Function for Add qualification of applicant
    public function update_changes_Skills()
    {
        $post=$this->input->post();
        if(isset($post))
        {
            $Skill_type_id=$this->input->post('skill_type_ids');
            $Duration=$this->input->post('EDuration');
           $Sid=$this->input->post('SKID');
            if(!isset($Skill_type_id) || empty($Skill_type_id))
            {
                echo "FAIL::Please select Skill Title  !::error";
                return;
            }
            if(!isset($Duration) || empty($Duration))
            {
                echo "FAIL::Please Select Duration !::error";
                return;
            }

            $data=array(
                'SkillID'=>$Skill_type_id,
                'skill_level'=>$Duration
            );

            $query=$this->common_model->update('applicant_skills',array('appSkillID'=>$Sid),$data);
            if($query)
            {
                echo "OK:: Record Update successfully !::success";
                return;
            }
            else
            {
                echo "FAIL:: Updating Failed !::error";
                return;
            }

        }
    }
//END

    //// Function for Trashed Skills ////

    public function Skills_Trashed()
    {
        $Sid=$this->input->post('Skill_id');
        $query=$this->common_model->update('applicant_skills',array('appSkillID'=>$Sid),array('trashed'=>1));
        if($query)
        {
            echo "OK:: Record Trashed Successfully !::success";
            return;
        }else{
            echo "FAIL:: Record Trashing Failed !::error";
            return;
        }

    }
    // END




    public function logout()
    {
        $this->session->sess_destroy();
        delete_cookie("parexons_hrms_cookie");
        redirect('recruitment/careers');
    }


    function load_all_available_advertisedJobs(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If Requested Through A Proper Channel THen Should Execute The Below Code.
                $table = 'job_advertisement';
                $selectData = ('advertisementID AS AdvertisedJobID, title AS JobTitle');
                $where = 'CURDATE() <= expiryDate';

                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'title';
                    $result = $this->common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$selectData,$where,FALSE);
                }
                echo json_encode($result);
            }
        }
    }
    function load_all_available_applicant_status(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If Request Generated through A Proper Channel.

                $table = 'ml_applicant_status_types MLAST';
                $selectData = ('applicantStatusTypeID AS ID, statusTitle AS TEXT');
                $where = array(
                   'trashed' => 0
                );
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'statusTitle';
                    $result = $this->common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$selectData,$where,FALSE);
                }
                echo json_encode($result);
            }
        }
    }

    function load_all_applicant_interview_status(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If Request Generated through A Proper Channel.

                $table = 'ml_interviewstatus MLINTSTS';
                $selectData = ('InterviewStatusID AS ID, InterviewStatus AS TEXT');
                $where = array(
                    'trashed' => 0
                );
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'InterviewStatus';
                    $result = $this->common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$selectData,$where,FALSE);
                }
                echo json_encode($result);
            }
        }
    }

    /// Function For Department Filter Through Ajax //

    function loadDepartment()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $tbl = "ml_department MLDP";
                $data = array('MLDP.department_id AS DepartmentID,MLDP.department_name AS DepartmentName', false);
                $where = array(
                    'MLDP.trashed' => 0,
                    'MLDP.enabled' => 1
                );
                $group_by = 'MLDP.department_id';
                if ($this->input->post('term')) {
                    $searchValue = $this->input->post('term');
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl, $data, $where, FALSE, $field, $searchValue, $group_by);
                } else {
                    $result = $this->common_model->select_fields_where($tbl, $data, $where, FALSE, '', '', '', $group_by);
                }
                print_r(json_encode($result));
            }
        }
    }

    /// Function To Check CNIC Already Exist
    public function check_cnic_existApplicant()
    {
        $nic_no = $this->input->post('cnic');
        $where = array('CNIC' => $nic_no);
        $query = $this->hr_model->check_nic_already_existsApplicant($where);
        if ($query) {
            echo "<span style='color:red;float: right;margin-right: 37px;'>Applicant CNIC Already Exists! </span>";
        } else {
            echo "<span style='color:green;float: right;margin-right: 170px;'> Valid CNIC</span>";
        }
    }

 }