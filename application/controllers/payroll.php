<?php
class Payroll extends MY_Controller
{
    
    function __construct()
	{
		parent::__construct();
		
		ini_set('max_execution_time',30000);
		$this->load->library('session');
        $this->load->model('payroll_model');
		$this->load->library('datatables');
		$this->load->library('pagination');
		$this->load->helper('extra_helper');
		
		if($this->session->userdata('logged_in') != TRUE)
		{
			redirect('login');			
		}

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if(!is_module_Allowed($loggedInEmployeeIDAccessCheck,$moduleController,'view') && !is_admin($loggedInEmployeeIDAccessCheck)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }
	}
	//public function index()
//	{
//		$this->load->view('login');
//		$this->load->library('form_validation');
//		
//	}
	public function validate_credential($msg= NULL)
	{	
		if($this->input->post('log'))
		{
			$this->form_validation->set_rules('user_name','User Name','required');
			$this->form_validation->set_rules('password','Password','required');
			if($this->form_validation->run() == TRUE)
			{
				$where = array(
				'user_name'=>$this->input->post('user_name'),
				'password'=>$this->input->post('password'));
				$query = $this->payroll_model->get_row_by_id('user_account',$where);
				$query_data = $this->payroll_model->get_all('user_account');
				//echo"<pre>"; print_r($query_data); die;
				if($query > 0)
				{
					foreach($query_data as $row){
						$id = $row->employee_id;
						$role = $row->user_group;
						}
					$sesion = array(
					'user_name'=>$this->input->post('user_name'),
					'employee_id'=>$id,
					'user_group'=>$role,
					'is_logged_in'=>TRUE);
					
					$this->session->set_userdata($sesion);
					if($row->user_group == 'Admin')
					{
						redirect('payroll/payroll_view');
					} else if($row->user_group == 'HRM'){
						redirect('payroll/ ');
					}
				} else{
					$data['msg'] = "<b>ERROR!</b> &nbsp; Please check your Username and Password";
				}
			}
		}
		$data[''] = "";
		if($this->uri->segment(3))
		{
			$data['msgz'] = $this->uri->segment(3);
		}
		$this->load->view('login/login',$data);
		
	}
	
	public function log_out()
	{
		$this->session->sess_destroy();
		redirect('site/index');
	}
	
	// End of Login Function //
	
/// ********************************************* *** **** ****** ***** **** *********************************************///
	/// ============================================= Pay Roll Module Start Here ============================================///
	/// ********************************************* *** **** ****** ***** **** ********************************************///
	/*public function popup()
	{
		$data['pop_id']=$this->input->get('pop');
		//echo $vr1; die;
		$data['months_list']=$this->payroll_model->ml_month();
		$data['years_list']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$data['years_list']=$this->payroll_model->ml_year();		
		$this->load->view('payroll/popup_month',$data);
		$this->load->view('includes/footer');
	}*/
	public function payroll_reg()
	{
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['msg']="<span style='color:green'>&nbsp;&nbsp;&nbsp; Please Search records using  above filters !</span>";
		$this->load->view('includes/header');
		$this->load->view('payroll/payroll_register',$data);
		$this->load->view('includes/footer');
	}
	public function tax_adjustment()
	{
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_financial_year','Select Financial Year','FinancialYearID','FinancialYear');
		$data['msg']="<span style='color:green'>&nbsp;&nbsp;&nbsp; Please Search records using  above filters !</span>";
		$this->load->view('includes/header');
		$this->load->view('payroll/tax_adjustment',$data);
		$this->load->view('includes/footer');
	}
	public function payroll_register()
	{
		$rec=$this->payroll_model->payroll_view_count();
		$count=count($rec);
		$config['base_url']     =base_url().'payroll/payroll_register';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		//echo $month."<br>".$year; die;
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['name']=$this->input->post('name');
		//$data['months']=$this->payroll_model->ml_month_datatable('transaction','calender_month','calender_month');
//		$data['years']=$this->payroll_model->ml_year_datatable('transaction','calender_year','calender_year');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
        $data['employees_name']=$this->payroll_model->master_list_data('employee','Select Employee Name','full_name','full_name');
		$data['info']=$this->payroll_model->payroll_view($limit,$page);
		$this->load->view('includes/header');
		$this->load->view('payroll/payroll_register',$data);
		$this->load->view('includes/footer');
	}

	public function payroll_tax_adjustment()
	{
		$rec=$this->payroll_model->payroll_view_count();
		$count=count($rec);
		$config['base_url']     =base_url().'payroll/payroll_tax_adjustment';
		$config['total_rows']   = $count;
		$config['per_page']     = 12;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();

		$year=$this->input->post('year');
		$data['slct_y']=$this->input->post('year');
		$data['name']=$this->input->post('name');
		$data['year']=$this->payroll_model->master_list_data('ml_financial_year','Select Financial Year','FinancialYearID','FinancialYear');
		$data['employees_name']=$this->payroll_model->master_list_data('employee','Select Employee Name','full_name','full_name');
		$data['info']=$this->payroll_model->tax_adjustment_view($limit,$page);
		$this->load->view('includes/header');
		$this->load->view('payroll/tax_adjustment',$data);
		$this->load->view('includes/footer');
	}
	public function pay_transaction()
	{
		$rec=$this->payroll_model->paytransactions_count();
		$count=count($rec);
		$config['base_url']     =base_url().'payroll/pay_transaction';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$name=$this->input->post('name');
		$data['info']=$this->payroll_model->paytransactions_view($limit,$page);
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['name']=$this->input->post('name');
		$data['code']=$this->input->post('code');
		$data['months']=$this->payroll_model->ml_month_datatable('transaction','calender_month','calender_month');
		$data['years']=$this->payroll_model->ml_year_datatable('transaction','calender_year','calender_year');
        $data['employees_name']=$this->payroll_model->master_list_data('employee','Select Employee Name','full_name','full_name');
        $data['employees_code']=$this->payroll_model->master_list_data('employee','Select Employee Code','employee_code','employee_code');
        $this->load->view('includes/header');
		$this->load->view('payroll/pay_transactions',$data);
		$this->load->view('includes/footer');
	}

	public function review_failed()
	{
		$id=$this->uri->segment(3);
		$transaction_id=$this->uri->segment(4);
		$salary_trans_date=$this->uri->segment(5);

		$where=array(
			'employment.employment_id'=>$id,
			'salary_payment.transaction_id'=>$transaction_id,
			//'salary_payment.year'=>$year
		);

		$query['salary_info']=$this->payroll_model->month_salaries_review($where);
        //print_r($query['salary_info']); die;
		//$salary_trans_date=$query['salary_info']->trans_date;
		$month=$query['salary_info']->month;
	    $year=$query['salary_info']->year;

		$query['ded_trans']=$this->payroll_model->payroll_regded_trans($id,$month,$year,$salary_trans_date);

//$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$query['allw_trans']=$this->payroll_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
		$query['exp_trans']=$this->payroll_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
		$query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);

		$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
		$where_enable=array('enabled'=>1,
			'trashed'=>0);
		$query['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where_enable);
		/* $where_emp=array(
               'employee.employee_id'=>$id
            );*/
		// $query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);
		$salary_payment_id=$query['salary_info']->salary_payment_id;
		$where_fail=array(
			'fail_reviews.salary_payment_id'=>$salary_payment_id,
			'fail_reviews.status'=>1
		);
		$query['failed_info']=$this->payroll_model->fail_review_info($where_fail);

		$this->load->view('includes/header');
		$this->load->view('payroll/trans_failed_review',$query);
		$this->load->view('includes/footer');

	}

	public function re_transaction_request()
	{
		$alw_trans_id=$this->input->post('alw_trans_id');

		$ded_trans_id=$this->input->post('ded_trans_id');

		$adv_trans_id=$this->input->post('adv_trans_id');

		$exp_trans_id=$this->input->post('exp_trans_id');

		//$exp_id=$this->input->post('exp_ids');

		//$adv_id=$this->input->post('adv_ids');

		$review_id=$this->input->post('review_id');


	    ///$salary_payment_id=$this->input->post('salary_payment_id');

		// $remarks=$this->input->post('remarks');

		$salary_transaction_id=$this->uri->segment(3);
		//$where=array('transaction.transaction_id'=>$transaction_id);

		//$transaction_data=array('status' => $this->input->post('status'));
		//echo "<pre>"; print_r($add_type); die;
		$query = $this->payroll_model->review_transaction_request($salary_transaction_id,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id,$review_id);
		if($query)
		{
			redirect('payroll/pay_transaction');
			return;
		}
	}
	
	public function transaction_status_update()
	{
		$transaction_status=$this->uri->segment(3);
		$transaction_id=$this->uri->segment(4);
		$table='transaction';
		$where=array('transaction_id'=>$transaction_id);
		if($transaction_status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'approval_date'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//echo "<pre>";
		//print_r($data);
		//die;
		$query=$this->payroll_model->update_status($table,$where,$data);
		if($query)
		{
			redirect('payroll/pay_transaction');
		}
	}
	
	public function transaction_details()
	{
		$where=array('transaction.transaction_id'=>$this->uri->segment(3));
		$query['details']=$this->payroll_model->transaction_view_details($where);
		//echo "<pre>"; print_r($query['details']);
//		exit;
		$this->load->view('includes/header');
		$this->load->view('payroll/transaction_details',$query);
		$this->load->view('includes/footer');
	}


	public function payroll_reg_detail($id,$month,$year)
	{
        $employment_id=get_employment_from_employeeID($id);
		$where=array(
		'employment.employment_id'=>$employment_id,
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		 );
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        //$employmentid =  $data['empl']->employment_id;
		$query['last_salary']=$this->payroll_model->last_salary($employment_id);
       //echo "<pre>";print_r($query['last_salary']);die;
		$query['salary_info']=$this->payroll_model->month_salaries($where);
        //echo "<pre>";print_r($query['salary_info']);die;
		$salary_trans_date=$query['salary_info']->transaction_date;
		$salary_payment_mode_id=$query['salary_info']->payment_mode;
		$query['payment_mode']=$this->payroll_model->payment_mode_details($salary_payment_mode_id);
		$query['ded_trans']=$this->payroll_model->payroll_regded_trans($employment_id,$month,$year,$salary_trans_date);
        //print_r($query['ded_trans']); die;
        $ded_trans_id=$query['ded_trans']->transaction_id;
		//$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$query['allw_trans']=$this->payroll_model->payroll_regallw_trans($employment_id,$month,$year,$salary_trans_date);
		$query['exp_trans']=$this->payroll_model->payroll_regexp_trans($employment_id,$month,$year,$salary_trans_date);
		$query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($employment_id,$month,$year,$salary_trans_date);
		$query['email']=$this->hr_model->get_row_by_where('current_contacts',array('employee_id'=>$id));
		$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
		$where_emp=array(
		'employee.employee_id'=>$id
		 );
		$query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);
		/*echo "<pre>";
		print_r($query['salaries']); die;*/
		$query['pdfView'] = $this->load->view('payroll/pdf_reports/payroll_reg_view_print',$query,true);
		$this->load->view('includes/header');
		$this->load->view('payroll/payroll_reg_details_2',$query);
		$this->load->view('includes/footer');
	}

    public function payroll_reg_slip($id,$month,$year)
    {
        $id=get_employment_from_employeeID($id);
        $where=array(
            'employment.employment_id'=>$id,
            'salary_payment.month'=>$month,
            'salary_payment.year'=>$year
        );
        $query['last_salary']=$this->payroll_model->last_salary($id);
        $query['salary_info']=$this->payroll_model->month_salaries($where);
        $salary_trans_date=$query['salary_info']->transaction_date;
        $salary_payment_mode_id=$query['salary_info']->payment_mode;
        $query['payment_mode']=$this->payroll_model->payment_mode_details($salary_payment_mode_id);
        $query['ded_trans']=$this->payroll_model->payroll_regded_trans($id,$month,$year,$salary_trans_date);
        //print_r($query['ded_trans']); die;
        $ded_trans_id=$query['ded_trans']->transaction_id;
        //$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
        $query['allw_trans']=$this->payroll_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
        $query['exp_trans']=$this->payroll_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
        $query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);

        $query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
        $where_emp=array(
            'employee.employee_id'=>$id
        );
        $query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);
        /*echo "<pre>";
        print_r($query['salaries']); die;*/
		$query['pdfView'] = $this->load->view('payroll/pdf_reports/testPDF',$query,true);
        $this->load->view('includes/header');
        $this->load->view('payroll/payroll_reg_slip',$query);
        $this->load->view('includes/footer');
    }

		public function payroll_salary_detail($id,$month,$year)
	{
		$where=array(
		'employee.employee_id'=>$id,
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		 );
		$query['details']=$this->payroll_model->payroll_salary_details($where);
		
		$this->load->view('includes/header');
		$this->load->view('payroll/payroll_salary_details',$query);
		$this->load->view('includes/footer');
	}
	public function salary_payment()
	{
	
		$rec=$this->payroll_model->salarypayment_count();
		$count=count($rec);
		$config['base_url']     = base_url().'payroll/salary_payment/';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$emp_name=$this->input->post('emp_name');
		$selc_month=$this->input->post('month');
		$selc_year=$this->input->post('year');
		$limit=$config['per_page'];
		$data['info']=$this->payroll_model->salarypayment_test($limit,$page,$emp_name,$selc_month,$selc_year);
		//echo $data['info']->alwnc_pay_freq;
		//die;
		$data['status_info']=$this->payroll_model->ml_status_payment();
		//echo count($data['info']); die;
		//echo "<pre>"; print_r($data['info']); die;
		$data["links"] = $this->pagination->create_links();
		$data['months']=$this->payroll_model->ml_month_datatable('ml_month','ml_month_id','month');
		$data['years']=$this->payroll_model->ml_year_datatable('ml_year','ml_year_id','year');
		$data['select_month']=$this->input->post('month');
		//$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/salary_payment',$data);
		$this->load->view('includes/footer');
		
	}
	public function update_status_salarypayment()
	{
		$status=$this->uri->segment(3);
		$salary_payement_id=$this->uri->segment(4);
		
		$where=array('salary_payment_id'=>$salary_payement_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'date_approved'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('salary_payment',$where,$data);
		if($query){
		
		redirect('payroll/salary_payment');
		
		}
	}
	public function print_payroll_reg($id,$month,$year)
	{
        $employment_id=get_employment_from_employeeID($id);
		$where=array(
		'employment.employment_id'=>$employment_id,
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		 );
		 $query['salary_info']=$this->payroll_model->month_salaries($where);
		 $salary_trans_date=$query['salary_info']->transaction_date;
		$query['ded_trans']=$this->payroll_model->payroll_regded_trans($employment_id,$month,$year,$salary_trans_date);
		$ded_trans_id=$query['ded_trans']->transaction_id;
		//$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$query['allw_trans']=$this->payroll_model->payroll_regallw_trans($employment_id,$month,$year,$salary_trans_date);
		$query['exp_trans']=$this->payroll_model->payroll_regexp_trans($employment_id,$month,$year,$salary_trans_date);
		$query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($employment_id,$month,$year,$salary_trans_date);
		
		$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);

		//$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year);
		
		$this->load->view('payroll/payroll_reg_details_print',$query);
	}
	public function print_salarypayment($id,$month,$year)
	{
		$where=array(
		'employee.employee_id'=>$id,
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		 );
		$query['details']=$this->payroll_model->payroll_salary_details($where);
		
		$this->load->view('payroll/salarypayment_details_print',$query);
	}
	public function print_expensepayment()
	{
		$id=$this->uri->segment(3);
		$where=array('expense_claims.expense_claim_id'=>$id);
		$query['details']=$this->payroll_model->payroll_expense_print($where);
		
		$this->load->view('payroll/expensepayment_details_print',$query);
	}
	public function print_transactions($id = NULL)
	{
		$where=array('transaction.transaction_id'=>$id );
		$query['details']=$this->payroll_model->transaction_view_details($where);
		
		$this->load->view('payroll/transaction_details_print',$query);
	}
	public function print_payrollreg($id = NULL)
	{
		$where=array('employee.employee_id'=>$id );
		$query['details']=$this->payroll_model->payroll_reg_details($where);
		
		$this->load->view('payroll/payroll_reg_details_print',$query);
	}
	public function print_advancesalary()
	{

		$where=array(
			'employee.employee_id'=>$this->uri->segment(3),
			'loan_advances.month'=>$this->uri->segment(4),
		    'loan_advances.year'=>$this->uri->segment(5)
		);
		//print_r($where); die;
		$query['details']=$this->payroll_model->advanceloan_details($where);
		//echo "<pre>";print_r($query['details']); die;
		
		$this->load->view('payroll/advanceloan_details_print',$query);
	}
	
	public function paysalary_time()
	{	$month=$this->input->post('month');
		$year=$this->input->post('year');
		$data['info']=1;
		/*$where=array(
		'salary_payment.month !='=>$month
		);*/
		//$data['info']=$this->payroll_model->employee_view_salary($month);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['projects']=$this->payroll_model->get_all('ml_projects');
		//echo "<pre>";
		//print_r($data['projects']);
		$this->load->view('includes/header');
		$this->load->view('payroll/paysalary_time',$data);
		$this->load->view('includes/footer');
	}
	
	public function paysalary_time_month()
	{
		$data['sal_m']=$this->input->post('month'); 
		$data['sal_y']=$this->input->post('year');
        if(!empty($data['sal_m'])){
		$data['project']=$this->input->post('project');
		$month_where=array('ml_month_id'=>$data['sal_m']);
		$year_where=array('ml_year_id'=>$data['sal_y']);
		$month_query=$this->payroll_model->get_row_by_where('ml_month',$month_where);
		$year_query=$this->payroll_model->get_row_by_where('ml_year',$year_where);
		$data['month_sal']=$month_query->month;
		$data['year_sal']=$year_query->year;
	
		$employment_id=$this->payroll_model->salary_paid_employees($data['sal_m'],$data['sal_y']);
        //print_r($employment_id);die;
            if(!empty($employment_id)){
		 $salary_ids = array();
	    foreach($employment_id as $emp){
		 $pp=$emp->salary_id;
		 //$salry_id=intval($pp);
		 array_push($salary_ids, intval($pp));		 		 
		 }
		$salary_ids = implode(',', $salary_ids);
		if(!empty($salary_ids))
		{$ids=$salary_ids;}else{$ids=0;}
            }else{$ids=0;}

		$data['info']=$this->payroll_model->employee_view_salary($ids,$data['sal_m'],$data['sal_y'],$data['project']);
//		echo $this->db->last_query();
			//echo "<pre>";
		//print_r($data['info']); die;
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/paysalary_time_step2',$data);
		$this->load->view('includes/footer');
        }
        else{redirect('payroll/paysalary_time');}
	}
	
	public function pay_salary()
	{
        if($this->uri->segment(3) == "gess"){
            $selectedMonth=$this->uri->segment(4);
            $yearID=$this->uri->segment(5);
            $table = 'ml_year';
            $selectYear = '`year` AS selectedYear';
            $where = array(
                'ml_year_id' => $yearID
            );
            $YearInfo = $this->common_model->select_fields_where($table,$selectYear,$where,TRUE);
            $selectedYear = $YearInfo->selectedYear;
            $selectedMonthNumeric = date("m",strtotime("15-".$selectedMonth."-".$selectedYear));
		}
		else
		{
            $selectedMonth =  $this->input->post('salary_month');
//            var_dump($selectedMonth);
            $selectedYear =  $this->input->post('salary_year');
            $selectedMonthNumeric = date("m",strtotime("$selectedMonth 15 $selectedYear"));
        }

//        var_dump($this->input->post('check'));
//        echo $selectedMonth;

//        var_dump($selectedMonthNumeric);

        $totalDaysInSelectedMonth = cal_days_in_month ( CAL_GREGORIAN , intval($selectedMonthNumeric) , intval($selectedYear));

        if($this->uri->segment(3) == "gess"){
                $mth=$this->uri->segment(4);
                if(empty($mth)){
                    redirect('payroll/paysalary_time');
                    return;
                }
				$this->session->set_userdata(array('count'=>$this->session->userdata('count')-1));
				if($this->session->userdata('count') == 0){
					redirect('payroll/paysalary_time');
				}
				$where=array('enabled'=>1,
			 		 'trashed'=>0);
					 $arr = $this->session->userdata('emps');
					 $month=$this->uri->segment(4);
					 $year=$this->uri->segment(5);
		 
		$data['info']=$this->payroll_model->salary_employee_data($arr['emp_id'.$this->session->userdata('count')],$month,$year);
		$data['account_info']=$this->payroll_model->account_info($arr['emp_id'.$this->session->userdata('count')]);

			$data['count']=$this->session->userdata('count');
			$data['salary_month']=$this->input->post('salary_month'); 
			$data['salary_year']=$this->input->post('salary_year');
			
			$data['month_id']=$this->uri->segment(4);
			$data['year_id']=$this->uri->segment(5);
			$data['last_salary']=$this->payroll_model->last_salary($arr['emp_id'.$this->session->userdata('count')]);
			$month_where=array('ml_month_id'=>$data['month_id']);
			$year_where=array('ml_year_id'=>$data['year_id']);
			$month_query=$this->payroll_model->get_row_by_where('ml_month',$month_where);
			$year_query=$this->payroll_model->get_row_by_where('ml_year',$year_where);
			$data['salary_month']=$month_query->month;
			$data['salary_year']=$year_query->year;
			
			$data['payment_type']=$this->payroll_model->html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
			$data['payment_mode']=$this->payroll_model->html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
            } else {
                $mth=$this->input->post('sal_month');
                if(empty($mth)){
                    redirect('payroll/paysalary_time');
                    return;
                }
				$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$employee_list= array();
		$no = 1;
		foreach($this->input->post('check') as $chk){
			$employee_list['emp_id'.$no] = $chk;
			$no++;
		}
		$newdata = array();
		$newdata['emps'] = $employee_list;
		$this->session->set_userdata($newdata);
		 
		
		 $arr = $this->session->userdata('emps'); 
		
		 $count = count($this->session->userdata('emps'));
		
		 $this->session->set_userdata(array('count'=>$count));
		 $test=$arr['emp_id'.$this->session->userdata('count')];

		  $data['month_id']=$this->input->post('sal_month');
		  $data['year_id']=$this->input->post('sal_year');
          $data['last_salary']=$this->payroll_model->last_salary($test);
//		 echo $this->db->last_query();
		  $data['account_info']=$this->payroll_model->account_info($test); 
		 $data['info']=$this->payroll_model->salary_employee_data($test,$data['month_id'],$data['year_id'],$count);
//		 echo "<pre>";
//		 print_r($data['info']); die;
			
			$data['month_id']=$this->input->post('sal_month'); 
			$data['year_id']=$this->input->post('sal_year');
			
			$data['salary_month']=$this->input->post('salary_month'); 
			$data['salary_year']=$this->input->post('salary_year');
			$data['count']=$count;
			
			$data['payment_type'] = $this->payroll_model->html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
			$data['payment_mode'] = $this->payroll_model->html_selectbox_desc('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
			}

        //Taking Out The Per Day Salary For Current Month
        if (isset($data['info']) && !empty($data['info']->base_salary)) {
            $baseSalary = $data['info']->base_salary;
            $perDaySalary = $baseSalary/$totalDaysInSelectedMonth;
            //We Got Per Day Salary For the Employee..
            //But Need To Find Out How Many Absentees Has Been Done By This Employee.

            //First We Need To Find Out What Attendance Module This System is Following If Following.
            $table = 'organization';
            $where = 'organization_id = (SELECT MAX(organization_id) from organization where 1 = 1)';
            $result = $this->common_model->select_fields_where($table,array('ConfigID AS AttendanceType',false),$where,TRUE);
            if(isset($result) && !empty($result->AttendanceType)){
                //As We Get AttendanceType Now Find Absentees Accordingly.
                //We Have Two Types Of Tables. Attendance and Manual TimeSheet Table.
                //Attendance Table is Been Used By Bio Metric and Web Integrated Attendance.
                //ID for Bio Metric and Web Integrated are 4 and 1 respectively.
                $attendanceType = $result->AttendanceType;
                //Gathering the PreRequites.
                $month = $selectedMonthNumeric;
                $year = $selectedYear;
                $currentMonthLastDay = $year.'-'.$month.'-'.$totalDaysInSelectedMonth;
                $currentMonthFirstDay = $year.'-'.$month.'-'.'01';
                $datesInCurrentMonth = createDateRangeArray($currentMonthFirstDay,$currentMonthLastDay);
                if (!isset($test) || empty($test) || !is_numeric($test)) {
                    $employeeID = $arr['emp_id' . $this->session->userdata('count')];
                } else {
                    $employeeID = $test;
                }
                $employmentID = get_employment_from_employeeID($employeeID);



                //Now Lets Find The Leaves.
                $table = 'leave_approval LA';
                $selectData = array('LA.approved_from AS ApprovedFrom, approved_to AS ApprovedTo',false);
                $where = '(MONTH(LA.approved_from) = MONTH("'.$currentMonthFirstDay.'") OR MONTH(LA.approved_to) = MONTH("'.$currentMonthFirstDay.'")) AND employment_id='.$employmentID;
                $leavesTakenResult = $this->common_model->select_fields_where($table,$selectData,$where);


                //Find Out Weekends Which Should Not Be Counted As Absentees.
                $workWeekTable = 'work_week';
                $wwSelectedData = array('work_week AS DayName, working_day AS DayType');
                $where = array(
                    'trashed' => 0,
                    'enabled' => 1,
                    'working_day' => 'Weekend'
                );
                $weekendDays = $this->common_model->select_fields_where($workWeekTable,$wwSelectedData,$where);

                foreach($weekendDays as $key=>$val){
                    foreach($datesInCurrentMonth as $subKey=>$subVal){
                        //Now Compare Here, If DayName Matches Then Unset..
                        if(strtolower($val->DayName) === strtolower(date('l',strtotime($subVal)))){
                            unset($datesInCurrentMonth[$subKey]);
                        }
                    }
                }

                //We Need Employee ID Not Sure What Kind of Login Has Been Applied For Getting EmployeeID
                if(intval($attendanceType) === 1 || intval($attendanceType) === 4){
                    //Need To Use Attendance Table Here.
                    $table = 'attendence';
                    $selectData = 'in_date AS Date';
                    $where = 'in_date BETWEEN "'.$currentMonthFirstDay.'" AND "'.$currentMonthLastDay.'" AND employee_id='.$employeeID;
                    $presentDays = $this->common_model->select_fields_where($table,$selectData,$where);

                    if (isset($presentDays) && !empty($presentDays)) {
                        foreach ($datesInCurrentMonth as $keyMonthDateIndex => $dateVal) {
                            foreach ($presentDays as $dateKey => $dateInfo) {
                                if ($dateVal == $dateInfo->Date) {
                                    unset($datesInCurrentMonth[$keyMonthDateIndex]);
                                }
                            }

                        }
                    }

                    //Need To Take Out Approved Leaves From Deductions.
                    if (isset($leavesTakenResult) && !empty($leavesTakenResult)) {
                        $totalLeaveApplicationsDates = array();
                        foreach ($leavesTakenResult as $key => $valLeaveDates) {
                            $totalLeaveApplicationsDates[$key] =  createDateRangeArray($valLeaveDates->ApprovedFrom,$valLeaveDates->ApprovedTo);
                        }
                        foreach($datesInCurrentMonth as $keyMonthDateIndex => $dateValLeaves){

                            foreach ($totalLeaveApplicationsDates as $key => $valLeaveDates) {
                                foreach($valLeaveDates as $leaveDate){
                                    if($dateValLeaves === $leaveDate){
                                        unset($datesInCurrentMonth[$keyMonthDateIndex]);
                                    }
                                }
                            }
                        }
                    }

                    //One Final Step.. Find If Any Holidays are in between...
                    $table = 'ml_holiday H';
                    $selectData = array('from_date AS HolidayFrom, to_date AS HolidayTo',false);
                    $where = 'MONTH(H.from_date) = MONTH("'.$currentMonthFirstDay.'") OR MONTH(H.to_date) = MONTH("'.$currentMonthFirstDay.'")';
                    $Holidays = $this->common_model->select_fields_where($table,$selectData,$where);
                    $TotalHolidays = array();
                    if(isset($Holidays) && !empty($Holidays)){
                        foreach($Holidays as $HKey=>$HVal){
                            $TotalHolidays[$HKey] =  createDateRangeArray($HVal->HolidayFrom,$HVal->HolidayTo);
                        }

                        //Now Unset from $datesInCurrentMonth if Date Matches.
                        foreach($datesInCurrentMonth as $HDateIndex=>$HDateValue){
                            foreach($TotalHolidays as $HolidayKey=>$HolidayValue){
                                foreach($HolidayValue as $HolidayDate){
                                    if($HolidayDate === $HDateValue){
                                        unset($datesInCurrentMonth[$HDateIndex]);
                                    }
                                }
                            }
                        }
                    }

                }elseif(intval($attendanceType) === 2){  //If Using A Manual TimeSheet.
                    $table = 'timesheet T';
                    $selectData = array('*',false);
                    $joins = array(
                        array(
                            'table' => 'timesheet_details TD',
                            'condition' => 'T.id = TD.timesheet_id AND hours != 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = 'T.employment_id = '.$employmentID.' AND MONTH(T.date_created) = MONTH("'.$currentMonthFirstDay.'")';
                   $totalPresentDays =  $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where);
                   if(isset($totalPresentDays) && !empty($totalPresentDays)){
                        foreach($datesInCurrentMonth as $key=>$val){
                            foreach($totalPresentDays as $subKey=>$subVal){
                                if($subVal->date === $val){
                                    unset($datesInCurrentMonth[$key]);
                                }
                            }
                        }
                    }

                    if (isset($leavesTakenResult) && !empty($leavesTakenResult)) {
                        $totalLeaveApplicationsDates = array();
                        foreach ($leavesTakenResult as $key => $valLeaveDates) {
                            $totalLeaveApplicationsDates[$key] =  createDateRangeArray($valLeaveDates->ApprovedFrom,$valLeaveDates->ApprovedTo);
                        }
                        foreach ($datesInCurrentMonth as $keyMonthDateIndex => $dateValLeaves) {
                            foreach ($totalLeaveApplicationsDates as $key => $valLeaveDates) {
                                foreach ($valLeaveDates as $leaveDate) {
                                    if ($dateValLeaves === $leaveDate) {
                                        unset($datesInCurrentMonth[$keyMonthDateIndex]);
                                    }
                                }
                            }
                        }
                    }
                }
                //Now Finally We Have All the Data We Need, Only We Have to Process the Data to get the Required Results.
                $datesForWhichSalaryWillBeDeducted = $datesInCurrentMonth;
                $totalDaysForSalaryBeDeducted = count($datesForWhichSalaryWillBeDeducted);
                $amountToDeductFromSalary = floor($perDaySalary*$totalDaysForSalaryBeDeducted);

//                echo '<pre>';
//                echo 'Employee Salary is = '.$baseSalary.'<br />';
//                echo 'Employee Per Day Salary is = '.$perDaySalary.'<br />';
//                echo 'Amount To Deduct From Salary = '.$amountToDeductFromSalary;
//                echo "</pre>";
                //Lets Take Out only Days From Current Month on which Employee Was Absent.
                $absentDays = array();
                foreach($datesForWhichSalaryWillBeDeducted as $dateKey=>$date){
                    $dateInArray = explode('-',$date);
                    array_push($absentDays,$dateInArray[2]);
                }
               $data['AbsentDays'] = implode(',',$absentDays);
                $data['TotalAbsentDays'] = $totalDaysForSalaryBeDeducted;
                $data['AmountToDeduct'] = $amountToDeductFromSalary;
            }




            //Taking Out Per Month Income Tax from Annual Income Tax, Under Tax Slabs Table

                    //For this we need Incremented Salary, Means Employee's Current Salary he is getting.
           	$baseSalary = (float)$data['info']->base_salary;

			//Getting All Increments Allocated Up to Till This Month.
			$incrementsTable = "increments I";
			$selectDataIncrements = array(
				'increment_id AS ID, increment_amount AS Amount'
			);

            $whereIncrements = "I.status = 2 AND I.trashed = 0 AND date_effective < CURDATE() AND employment_id = ".$employmentID;

            $allIncrements = $this->common_model->select_fields_where($incrementsTable,$selectDataIncrements,$whereIncrements);

            $data['totalIncrementedAmount'] = 0;
            if(isset($allIncrements) && !empty($allIncrements)){
                foreach($allIncrements as $increment){
                    //Its Incremented Base Salary Now.
                    $baseSalary = floatval($baseSalary) + (float)$increment->Amount;
                    $data['totalIncrementedAmount']+=(float)$increment->Amount;
                }
            }


            $AnnualIncrementedBaseSalary = $baseSalary*12;

            $slabTable="ml_tax_slab TS";
            $selectDataSlab = array(
                '
                TaxSlabID AS SlabID,
                SlabNo,
                FixedAmount,
                Remarks,
                CASE WHEN FixedAmount IS NOT NULL THEN FLOOR(FixedAmount/12) ELSE 0 END AS MonthlyTax
                ',
                false
            );

            $joinsSlab = array(
                array(
                    'table' => 'ml_tax_year TY',
                    'condition' => 'TY.TaxYearID = TS.TaxYearID',
                    'type' => 'INNER'
                )
            );

            $whereSlab = "TS.AnnualIncomeTo >= ". $AnnualIncrementedBaseSalary." AND ". $AnnualIncrementedBaseSalary ." >= TS.AnnualIncomeFrom";
            $data['slabDetails'] = $this->common_model->select_fields_where_like_join($slabTable,$selectDataSlab,$joinsSlab,$whereSlab,TRUE);
        }

			$this->load->view('includes/header');
			$this->load->view('payroll/pay_salary',$data);
			$this->load->view('includes/footer');
	}

	///////////////// END //////////////////////
    //// Function For Attendance Type to check if data is selected or Not Using ajax ///
    public function CheckAttemdanceType()
    {
        if ($this->input->is_ajax_request()) {

                    $table = 'organization ORG';
                    $data = ('MLCT.ConfigID AS ConfigurationID');
                    $joins = array(
                        array(
                            'table' => 'ml_configrationtypes MLCT',
                            'condition' => 'MLCT.ConfigID = ORG.ConfigID AND MLCT.trashed = 0',
                            'type' => 'INNER'
                        )
                    );

                    $LeavesDedudction = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where='', TRUE);
                if (!isset($LeavesDedudction) || count($LeavesDedudction) > 0){
                    echo "";
                    return;
                }else{
                    echo "FAIL:: Please Select Attendance Type ::error";
                    return;
                }
        }
    }

    //END

	public function pay_remain_salary()
	{
		$emp_id=$this->uri->segment(3);
		$salary_month_id=$this->uri->segment(4);
		$salary_year_id=$this->uri->segment(5);
		$paid_amount=$this->uri->segment(6);
		
		//echo $emp_id."<br>".$salary_month_id."<br>".$salary_year_id."<br>".$paid_amount; die;
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
					 $employ_id=array(
					 'employee_id'=>$emp_id);
					$empoyment_id= $this->payroll_model->employement_id($employ_id);
					$em_id=$empoyment_id->employment_id;
					$salary= $this->payroll_model->fetch_salary_id($em_id);
					$salary_id=$salary->salary_id;
					 
					 
					 $salary_where=array(
					 'salary_payment.salary_id'=>$salary_id,
					 'salary_payment.month'=>$salary_month_id,
					 'salary_payment.year'=>$salary_year_id
					 );
					 
					 
		$data['remain_salary']=$this->payroll_model->pay_remain_salary_model
		($salary_where);
		//echo "<pre>";
//		print_r($data['remain_salary']);die;
		$data['payable']=$this->uri->segment(6);
		$data['month']=$this->payroll_model->ml_month_where($salary_month_id);
		$data['year']=$this->payroll_model->ml_year_where($salary_year_id);
		$data['payment_type']=$this->payroll_model->
		html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
		$data['payment_mode']=$this->payroll_model->
		html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
		$this->load->view('includes/header');
		$this->load->view('payroll/pay_remain_salary',$data);
		$this->load->view('includes/footer');
	}
	public function advance_salary()
	{
		$rec=$this->payroll_model->loan_advacne_count();
		$count=count($rec);
		$config['base_url']     =base_url().'payroll/advance_salary';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$data['info']=$this->payroll_model->loan_advacne_view($limit,$page);
        //echo "<pre>";print_r($data['info']); die;
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['name']=$this->input->post('name');
		$data['slct_adnc']=$this->input->post('adnc_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['advance_type']=$this->payroll_model->master_list_data('ml_payment_type','Select Type','payment_type_id','payment_type_title');
        $data['employees_name']=$this->payroll_model->master_list_data('employee','Select Employee Name','full_name','full_name');
        $this->load->view('includes/header');
		$this->load->view('payroll/advance_salary',$data);
		$this->load->view('includes/footer');
	}
	public function advance_payback($param = NULL)
	{
		if($param == "list")
			{
				echo $this->payroll_model->loan_advacnes_paid();
				die;
			}
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['slct_adnc']=$this->input->post('adnc_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['advance_type']=$this->payroll_model->master_list_data('ml_payment_type','Select Type','payment_type_id','payment_type_title');
		$this->load->view('includes/header');
		$this->load->view('payroll/advance_payback',$data);
		$this->load->view('includes/footer');
	}
	public function pay_advance()
	{
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['payment_type']=$this->payroll_model->
		html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
		$data['payment_mode']=$this->payroll_model->
		html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$this->load->view('includes/header');
		$this->load->view('payroll/pay_advance',$data);
		$this->load->view('includes/footer');
	}
	public function pay_advance_transaction()
	{
		$emp_id=$this->uri->segment(3);
		$loan_advance_id=$this->uri->segment(4);
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['payment_type']=$this->payroll_model->html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
		$data['payment_mode']=$this->payroll_model->html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
		$data['status']=$this->payroll_model->html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$data['account_info']=$this->payroll_model->account_info($emp_id);
		$data['auto_fill']=$this->payroll_model->get_autofill_loanad($emp_id,$loan_advance_id);
		$data['account_info']=$this->payroll_model->account_info($data['auto_fill']->employee_id);
		$this->load->view('includes/header');
		$this->load->view('payroll/pay_advance_transaction',$data);
		$this->load->view('includes/footer');
	}
	function autofill_loanadv()
	{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autofill_loanad();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->CNIC;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 
	public function advance_loan_payback()
	{
		$emp_id=$this->uri->segment(3);
		$loan_id=$this->uri->segment(4);
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['payment_mode']=$this->payroll_model->html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
		$data['info']=$this->payroll_model->loan_advance_rec($loan_id);
		$data['account_info']=$this->payroll_model->account_info($data['info']->employee_id);
		$this->load->view('includes/header');
		$this->load->view('payroll/loan_payback',$data);
		$this->load->view('includes/footer');
	}
	public function expense_disb()
	{
		//$employee_id=$this->uri->segment(3);
		$expense_id=$this->uri->segment(4);
		$amount=$this->uri->segment(5);
		$type=$this->uri->segment(6);
		
		$data['emp_id']=$this->uri->segment(3);
		$data['exp_id']=$this->uri->segment(4);
		//echo $employee_id."<br>".$expense_id."<br>".$amount."<br>".$type; die;
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['payment_mode']=$this->payroll_model->
		html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
		$data['expense']=$this->payroll_model->
		html_selectbox('ml_expense_type','-- Select Expense Type --','ml_expense_type_id','expense_type_title',$where);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$data['trans_type']=$this->payroll_model->
		html_selectbox('ml_transaction_types','-- Select Transaction Type --','ml_transaction_type_id','transaction_type',$where);
		
		$data['auto_fill']= $this->payroll_model->get_autocomplete_expenseback($expense_id);
		$data['account_info']=$this->payroll_model->account_info($data['auto_fill']->employee_id);
		$this->load->view('includes/header');
		$this->load->view('payroll/expense_payback',$data);
		$this->load->view('includes/footer');
	}
	
	function autocomplete_expenseback()
{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete_expenseback();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->CNIC;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->amount;?>','<?php echo $row->expense_claim_id;?>','<?php echo $row->expense_type_title;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 
	
	public function expense_mgt()
	{
		$rec=$this->payroll_model->expense_count();
		$count=count($rec);
		$config['base_url']     =base_url().'essp/expense_claim';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$data['info']=$this->payroll_model->expense_view($limit,$page);
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['slct_exp']=$this->input->post('exp_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['expense_type']=$this->payroll_model->master_list_data('ml_expense_type','Select Expense Type','ml_expense_type_id','expense_type_title');
		$this->load->view('includes/header');
		$this->load->view('payroll/expense_managemant',$data);
		$this->load->view('includes/footer');
	}	
	/// function for expense disbursment 
	public function expense_disbursment_view($param = NULL)
	{
		if($param == "list")
		{
			echo $this->payroll_model->expense_disbursment_view();
			
			die;
		}
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['slct_exp']=$this->input->post('exp_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['expense_type']=$this->payroll_model->master_list_data('ml_expense_type','Select Expense Type','ml_expense_type_id','expense_type_title');
		$this->load->view('includes/header');
		$this->load->view('payroll/expense_disbursment',$data);
		$this->load->view('includes/footer');
	}	
	
	public function add_expense()
	{
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['employee']=$this->payroll_model->html_selectbox('ml_employment_type','-- Select Employee Type --','employment_type_id','employment_type',$where);
		$data['expense']=$this->payroll_model->html_selectbox('ml_expense_type','-- Select Expense Type --','ml_expense_type_id','expense_type_title',$where);
		$data['status']=$this->payroll_model->html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$this->load->view('includes/header');
		$this->load->view('payroll/add_expense',$data);
		$this->load->view('includes/footer');
	}
	public function edit_expense($id = NULL)
	{
		$where1=array('enabled'=>1,
			 		 'trashed'=>0);
		$where=array('expense_claims.expense_claim_id'=>$id);
		$data['row']=$this->payroll_model->expense_data_fetch($where);
		//$data['employee']=$this->payroll_model->html_selectbox
		//('master_list_employment_type','Select Eemployment Type','employment_type_id','employment_type',$where1);
		$data['expense']=$this->payroll_model->payroll_model->html_selectbox
		('ml_expense_type','Select Expense Type','ml_expense_type_id','expense_type_title',$where1);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where1);
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_expense',$data);
		$this->load->view('includes/footer');
	}
	
	// Function for Expense Disburments //
	
	public function expense_disburments()
	{
		$exp_id=$this->input->post('expense_id');
		$paid_amount=$this->input->post('exp_amount');
        $emp_id = $this->input->post('emp_id');
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
        $employmentID = $data['empl']->employment_id;
// print_r($data['empl']);die;
		// acount
		$payment_account=array(
		'employee_id'			     =>$this->input->post('emp_id'),
		//'bank_name'				   =>$this->input->post('bank_account'),
		'account_no'				  =>$this->input->post('no_account'),
		'account_title'			   =>$this->input->post('titile_account'),
		'bank_name'				   =>$this->input->post('bank_account'),
		'branch'			 		  =>$this->input->post('branch_account'),
		'branch_code'			 		  =>$this->input->post('branch_code')
				);
				
		//echo "<pre>";
//		print_r($payment_account);
//		exit;
	
		// cheque
		$payment_cheque=array(
		'cheque_no'=>$this->input->post('no_cheque'),
		'bank_name'=>$this->input->post('bank_cheque')
		);
		
		//cash
		$payment_cash=array(
		'received_by'=>$this->input->post('received'),
		'remarks'=>$this->input->post('remarks')
		);
		
		$payment_mode=$this->input->post('payment_mode');
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		//echo $paid_amount. "<br>".$month."<br>".$year; exit;
		
				
		if($payment_mode ==3){
		$query=$this->payroll_model->expense_disburment_account($payment_account,$exp_id,$month,$year,$paid_amount,$employmentID);
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->expense_disburment_cheque($payment_cheque,$exp_id,$month,$year,$paid_amount,$employmentID);
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->expense_disburment_cash($payment_cash,$exp_id,$month,$year,$paid_amount,$employmentID);
		}
		if($query)
		{
			redirect('payroll/expense_mgt');
		}
	}
	// End
	
	
	
	public function deduction()
	{
		$rec=$this->payroll_model->deduction_count();
		$count=count($rec);
		$config['base_url']     =base_url().'payroll/deduction';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$limit=$config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$data['info']=$this->payroll_model->deduction_view($limit,$page);
		$data['slct_m']=$this->input->post('month');
		$data['name']=$this->input->post('name');
		$data['slct_y']=$this->input->post('year');
		$data['slct_ded']=$this->input->post('ded_type');
		$data['slct_freq']=$this->input->post('frequency');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['deduction_type']=$this->payroll_model->master_list_data('ml_deduction_type','Select Deduction Type','ml_deduction_type_id','deduction_type');
		$data['employees_name']=$this->payroll_model->master_list_data('employee','Select Employee Name','full_name','full_name');
		$data['frequency']=$this->payroll_model->master_list_data('ml_pay_frequency','Select Pay Frequnce','ml_pay_frequency_id','pay_frequency');
		$this->load->view('includes/header');
		$this->load->view('payroll/deductions',$data);
		$this->load->view('includes/footer');
	}
	public function add_deduction()
	{
		
		/*@$msg=$this->uri->sgement(3);
		if(isset($msg))
		{$data['msg']=$msg;
		echo $data;}else{*/
		//$employee_name=$this->input->post('employee_name');
//		$data['search_result']=$this->payroll_model->empSearchResult($employee_name);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
	    $data['employees']=$this->payroll_model->employee_names();
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$data['deduction']=$this->payroll_model->
		html_selectbox('ml_deduction_type','-- Select Deduction Type --','ml_deduction_type_id','deduction_type',$where);
		$data['deduction_freq']=$this->payroll_model->
		html_selectbox('ml_pay_frequency','-- Select Pay Frequency --','ml_pay_frequency_id','pay_frequency',$where);
		
		$data['deduction_fqrcy']=$this->payroll_model->deduction_frequency();
		$this->load->view('includes/header');
		$this->load->view('payroll/add_deductions',$data);
		$this->load->view('includes/footer');
		/*}*/
	}
	// trashed Payroll
    function deduction_trashed()
    {
        $id=$this->input->post('id');
        $table="deduction";
        $where=array('deduction_id'=>$id);
        $query=$this->payroll_model->delete_row_by_where($table,$where);
        if($query)
        {

            echo "record delete successfully !";
            return;
        }
    }
	/// update status ///////////////
	public function deduction_status_update()
	{
		$status=$this->uri->segment(3);
		$deduction_id=$this->uri->segment(4);
		
		$where=array('deduction_id'=>$deduction_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'date_approved'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('deduction',$where,$data);
		if($query){
		
		redirect('payroll/deduction');
		
		}
	}
	
		/// update status ///////////////
	public function expense_status_update()
	{
		$status=$this->uri->segment(3);
		$expense_claim_id=$this->uri->segment(4);
		
		$where=array('expense_claim_id'=>$expense_claim_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'approval_date'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('expense_claims',$where,$data);
		if($query){
		
		redirect('payroll/expense_mgt');
		
		}
	}
	
			/// update status ///////////////
	public function increment_status_update()
	{
		$status=$this->uri->segment(3);
		$eincrement_id=$this->uri->segment(4);
		
		$where=array('increment_id'=>$eincrement_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'approval_date'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('increments',$where,$data);
		if($query){
		
		redirect('payroll/increment');
		
		}
	}
	
	/// update status ///////////////
	public function allowance_status_update()
	{
		$status=$this->uri->segment(3);
		$eincrement_id=$this->uri->segment(4);
		
		$where=array('allowance_id'=>$eincrement_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'date_approved'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('allowance',$where,$data);
		if($query){
		
		redirect('payroll/allowance');
		
		}
	}
	
	/// update status ///////////////
	public function loan_status_update()
	{
		$status=$this->uri->segment(3);
		$loan_id=$this->uri->segment(4);
		
		$where=array('loan_advance_id'=>$loan_id);
		if($status=="Pending")
		{
			$data=array(
			'status'=>2,
			'approved_by'=>$this->session->userdata('employee_id'),
			'date_of_approval'=>date('Y-m-d'));
		}
		else
		{
			$data=array('status'=>1);
		}
		//print_r($data); die;
		
		$query=$this->payroll_model->update_status('loan_advances',$where,$data);
		if($query){
		
		redirect('payroll/advance_salary');
		
		}
	}
	

	// Function for increment view//
	public function increment( $param = NULL)
	{
		if($param == "list")
		{
			echo $this->payroll_model->increment_view();
			
			die;
		}
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['slct_incr']=$this->input->post('incr_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['increment_type']=$this->payroll_model->master_list_data('ml_increment_type','Select Increment Type','increment_type_id','increment_type');
		$this->load->view('includes/header');
		$this->load->view('payroll/increments',$data);
		$this->load->view('includes/footer');
	}
	
	public function add_increment()
	{
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$data['employee']=$this->payroll_model->
		html_selectbox('ml_employment_type','-- Select Employee Type --','employment_type_id','employment_type',$where);
		$data['increment']=$this->payroll_model->
		html_selectbox('ml_increment_type','-- Select Increment Type --','increment_type_id','increment_type',$where);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$this->load->view('includes/header');
		$this->load->view('payroll/add_increments',$data);
		$this->load->view('includes/footer');
	}
	public function allowance($param = NULL)
	{
		if($param == "list")
		{
			echo $this->payroll_model->allowance_view();
			die;
		}
		$data['slct_m']=$this->input->post('month');
		$data['slct_y']=$this->input->post('year');
		$data['slct_alw']=$this->input->post('alw_type');
		$data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		$data['allowance_type']=$this->payroll_model->master_list_data('ml_allowance_type','Select Allowance type','ml_allowance_type_id','allowance_type');
		$this->load->view('includes/header');
		$this->load->view('payroll/allowances',$data);
		$this->load->view('includes/footer');
	}
	public function add_allowance()
	{
		
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
	    $data['employees']=$this->payroll_model->employee_names();
		$data['allowance']=$this->payroll_model->
		html_selectbox('ml_allowance_type','-- Select Pay Frequency --','ml_allowance_type_id','allowance_type',$where);
		$data['frequency']=$this->payroll_model->
		html_selectbox('ml_pay_frequency','-- Select Pay Frequency --','ml_pay_frequency_id','pay_frequency',$where);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/add_allowance',$data);
		$this->load->view('includes/footer');
	}
	
	public function edit_allowance($id=NULL)
	{
		$where1=array('enabled'=>1,
			 		 'trashed'=>0);
		$where=array('allowance.allowance_id'=>$id);
		$data['info']=$this->payroll_model->allowance_data_fetch($where);
		$data['allowance']=$this->payroll_model->
		html_selectbox('ml_allowance_type','-- Select Allowance type --','ml_allowance_type_id','allowance_type',$where1);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where1);
		$data['frequency']=$this->payroll_model->
		html_selectbox('ml_pay_frequency','-- Select Pay Frequency --','ml_pay_frequency_id','pay_frequency',$where1);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_allowance',$data);
		$this->load->view('includes/footer');
		}
		
	
	public function end_service()
	{
	    $data['month']=$this->input->post('month');
        if(isset($data['month']) && !empty($data['month'])){
        $where=array('year'=>date('Y'));
        $year=$this->payroll_model->get_row_by_id('ml_year',$where);
        $data['current_year']=$year->ml_year_id;
	    $data['selected_month']=$this->input->post('month');
		$data['info']=$this->payroll_model->end_service_view($data['month'],$data['current_year']);
        $data['months']=$this->payroll_model->html_selectbox('ml_month','Select Month','ml_month_id','month',array('enabled'=>1,'trashed'=>0));
       // echo "<pre>";
        //print_r($data['info']); die;
		$this->load->view('includes/header');
		$this->load->view('payroll/end_service',$data);
		$this->load->view('includes/footer');}
        else
        {redirect('payroll/payroll_reg');}
	}

    public function end_of_service_payment_view()
	{
       $employment_id=$this->uri->segment(3);
        $month=$this->uri->segment(4);
        $year=$this->uri->segment(5);
        if(empty($month) && empty($year) && empty($employment_id))
        {
            redirect('payroll/end_service');
        }
		$data['info']=$this->payroll_model->end_of_service_pay_view($employment_id,$month,$year);
        $employee_id=get_employee_from_employmentID($employment_id);
        $data['account_info']=$this->payroll_model->account_info($employee_id);
        //echo "<pre>";
        //print_r($data['info']); die;
		$this->load->view('includes/header');
		$this->load->view('payroll/end_of_service_payment',$data);
		$this->load->view('includes/footer');
	}

    public function end_of_service_payment_details()
    {
        $employment_id=$this->uri->segment(3);
        $month=$this->uri->segment(4);
        $year=$this->uri->segment(5);
        if(empty($month) && empty($year) && empty($employment_id))
        {
            redirect('payroll/end_service');
        }
        $data['info']=$this->payroll_model->end_of_service_pay_view($employment_id,$month,$year);
        $data['account_info']=$this->payroll_model->account_info($employment_id);
        //echo "<pre>";
        //print_r($data['info']); die;
        $this->load->view('includes/header');
        $this->load->view('payroll/end_of_service_payment_details',$data);
        $this->load->view('includes/footer');
    }

    public function payment_end_of_service()
    {
       if($this->input->is_ajax_request()){
           $employment_id= $this->input->post('employment_id');
           $payable =$this->input->post('payable');
           $ded_amount =$this->input->post('ded_amount');
           $account_id= $this->input->post('account_id');
           $ded_id= $this->input->post('ded_id');
           $exp_id= $this->input->post('exp_id');
           $alw_id= $this->input->post('alw_id');
           $adv_id= $this->input->post('adv_id');
           $month= $this->input->post('month');
           $year= $this->input->post('year');
           $exp_amount=$this->input->post('exp_amount');
           $alw_amount=$this->input->post('alw_amount');
           $adv_amount=$this->input->post('adv_amount');
           $salary_id=$this->input->post('salary_id');


           $query=$this->payroll_model->salary_trans_eos($employment_id,$payable,$ded_amount,$account_id,$exp_id,$alw_id,$adv_id,$month,$year,$exp_amount,$alw_amount,$adv_amount,$salary_id);
            if($query)
            {
               echo "OK:: Request Submit for Approval Successfully !:: success";
                return;
            }
            else{
                echo "FAIL:: Request Submit for Approval Successfully !:: error";
                return;
            }

        }

    }
	public function eos_settlement($id = NULL)
	{
		$emp_id=$this->uri->segment(3);
		$query['data']= $this->payroll_model->get_eossettelment($emp_id);
		echo "<pre>";
		print_r($query['data']);
		exit;
		
		//echo $emp_id; exit;
		$this->load->view('includes/header');
		$this->load->view('payroll/eos_settlement');
		$this->load->view('includes/footer');
	}
	public function payroll_report()
	{
	
		$this->load->view('includes/header');
		$this->load->view('payroll/payroll_reports');
		$this->load->view('includes/footer');
	}
	
	public function expense_reports()
	{
		$data['info']=$this->payroll_model->expense_report_model();
		$this->load->view('includes/header');
		$this->load->view('payroll/reports_expense',$data);
		$this->load->view('includes/footer');
	}
	
	public function salary_reports()
	{
		$data['info']=$this->payroll_model->salary_report();
		//echo "<pre>";
		//print_r($data['info']); die;
		$this->load->view('includes/header');
		$this->load->view('payroll/reports_salary',$data);
		$this->load->view('includes/footer');
	}
	
	public function transactions_reports()
	{
		$data['info']=$this->payroll_model->transactions_reports();
		$this->load->view('includes/header');
		$this->load->view('payroll/reports_transaction',$data);
		$this->load->view('includes/footer');
	}
	
	function autocomplete()
	{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->CNIC;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 

function autocomplete_expense()
{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete_expense();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->amount;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->expense_type_title;?>')"><?php echo $row->full_name;?></li> <?php
      $i++;  //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 

function autocomplete_deduction()
{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete_deduction();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->deduction_amount;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->deduction_type;?>')"><?php echo $row->full_name;?></li> <?php
        $i++;//echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 

function autocomplete_loan_advacne()
{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete_loan_advance();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->amount;?>','<?php echo $row->loan_advance_id;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 




function autocomplete_paysalary()
{
    //$this->load->model('model','get_data');
    $query= $this->payroll_model->get_autocomplete_paysalary();
	
	$i=1;
    foreach($query->result() as $row):
	?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->CNIC;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->base_salary;?>','<?php echo $row->salary_id;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
    endforeach;    
} 



	public function add_deduction_record()
	{
		
		$emp_id=$this->input->post('emp_id');
		$ded_type=$this->input->post('deduction_type');
		$ded_freq=$this->input->post('deduction_freq');


        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
		$check=$this->payroll_model->check_deduction_record($data['empl']->employment_id,$ded_type,$ded_freq);
		if(!empty($check)){
			
			  $this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#F00 !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Sorry You Already Set this Deduction!</span>');
			redirect('payroll/add_deduction');
						}
			else{
		if($this->input->post('deduction_freq')==1){
		$deduction=array(
		'employment_id'         =>$data['empl']->employment_id,
		'ml_deduction_type_id'=>$this->input->post('deduction_type'),
		'deduction_frequency' =>$this->input->post('deduction_freq'),
		'deduction_amount'    =>$this->input->post('amount'),
		'eobi_no'             =>$this->input->post('eobi_no'),
		'created_by'          =>$this->session->userdata('employee_id'), // need to change later when session start
		'date_created'        =>date('Y-m-d')
		);
		
	}
     elseif($this->input->post('deduction_freq')==2){
     $deduction=array(
     'employment_id'         =>$data['empl']->employment_id,
     'ml_deduction_type_id'=>$this->input->post('deduction_type'),
     'deduction_frequency' =>$this->input->post('deduction_freq'),
     'deduction_amount'    =>$this->input->post('amount'),
     'eobi_no'             =>$this->input->post('eobi_no'),
     'created_by'          =>$this->session->userdata('employee_id'), // need to change later when session start
     'month'			   =>$this->input->post('month'),
     'date_created'        =>date('Y-m-d')
      );

     }
     elseif($this->input->post('deduction_freq')==3){
		$deduction=array(
		'employment_id'     		=>$data['empl']->employment_id,
		'ml_deduction_type_id'	=>$this->input->post('deduction_type'),
		'deduction_frequency'	=>$this->input->post('deduction_freq'),
		'deduction_amount'		=>$this->input->post('amount'),
		'eobi_no'               =>$this->input->post('eobi_no'),
		'created_by'			=>$this->session->userdata('employee_id'), // need to change later when session start
		'date_created'			=>date('Y-m-d'),
		'month'					=>$this->input->post('month'),
		'year'					=>$this->input->post('year')
		);
	}
	/*echo "<pre>";
	print_r($deduction); die;*/
		
		 $month=$this->input->post('month');
		 $year=$this->input->post('year');
		
		$frequency=$this->input->post('deduction_freq');
		/*if($frequency = 1)
		{
		$query=$this->payroll_model->add_deduction_records_monthly($deduction);
		}
		else{*/
		$query=$this->payroll_model->add_deduction_records($deduction);
		/*}*/
		if($query)
		{
			$this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Succesfully Deduction Set!</span>');
			redirect('payroll/add_deduction');
		}
		}
	}
	/*public function deduction_transactions()
	{
		$employee_id=$this->uri->segment(3);
		$deduction_id=$this->uri->segment(4);
		$amount=$this->uri->segment(5);
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_deductions',$data);
		$this->load->view('includes/footer');
		//echo $employee_id."<br>".$deduction_id."<br>".$amount; die;
		$query=$this->payroll_model->deduction_transaction_model($deduction_id,$amount,$employee_id);
		if($query)
		{
			redirect('payroll/deduction');
		}
	}*/
	// End of function for add record of deduction against employee//
	
	public function deduction_edit()
	{
		$id=$this->uri->segment(3);
		$where1=array('enabled'=>1,
			 		 'trashed'=>0);
		$where=array('deduction.deduction_id'=>$id);
		$data['info']=$this->payroll_model->deduction_data_fetch($where);
		$data['dedfrqcy']=$this->payroll_model->
		html_selectbox('ml_pay_frequency','-- Select Deduction Type --','ml_pay_frequency_id','pay_frequency',$where1);
		$data['deduction']=$this->payroll_model->
		html_selectbox('ml_deduction_type','-- Select Deduction Type --','ml_deduction_type_id','deduction_type',$where1);
		$data['months']=$this->payroll_model->master_list_data('ml_month','Select Month','ml_month_id','month');
		$data['years']=$this->payroll_model->master_list_data('ml_year','Select Year','ml_year_id','year');
		//$data['status']=$this->payroll_model->
		//html_selectbox('ml_status','-- Select status Type --','status_id','status_title',$where1);
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_deductions',$data);
		$this->load->view('includes/footer');
		}
		

// Function for Update deduction records //

	public function updte_deduction_record()
	{
		$where=array('deduction_id'=>$this->input->post('ded_id'));
         $month=$this->input->post('month');
         $year=$this->input->post('year');
        if(!is_numeric($month) && !is_numeric($year))
        {
            $data=array(
                'ml_deduction_type_id'=>$this->input->post('deduction_type'),
                'deduction_frequency'=>$this->input->post('deduction_freq'),
                'deduction_amount'=>$this->input->post('amount'),
                'month'=>NULL,
                'year'=>NULL,
                'modified_by'=>$this->session->userdata('employee_id'),
                'date_modified'=>date('Y-m-d')
            );
        }
        if(isset($month) && is_numeric($month)  && !is_numeric($year))
        {
            $data=array(
                'ml_deduction_type_id'=>$this->input->post('deduction_type'),
                'deduction_frequency'=>$this->input->post('deduction_freq'),
                'deduction_amount'=>$this->input->post('amount'),
                'month'=>$month,
                'modified_by'=>$this->session->userdata('employee_id'),// need to change when session start
                'date_modified'=>date('Y-m-d')
            );
        } if(isset($month) && isset($year) && is_numeric($year) && is_numeric($month))
        {
            $data=array(
                'ml_deduction_type_id'=>$this->input->post('deduction_type'),
                'deduction_frequency'=>$this->input->post('deduction_freq'),
                'deduction_amount'=>$this->input->post('amount'),
                'month'=>$month,
                'year'=>$year,
                'modified_by'=>$this->session->userdata('employee_id'),// need to change when session start
                'date_modified'=>date('Y-m-d')
            );
        }


		$query=$this->payroll_model->update_record('deduction',$where,$data);
		if($query)
		{
			redirect('payroll/deduction');
		}
	}
	
	// Function for add expenses records //
	
	public function add_expense_records()
	{ //error_reporting(0);
	
		$ecxp_date=$this->input->post('expdate');
		$exp_month=date("m",strtotime($ecxp_date));
		$exp_year=date("Y",strtotime($ecxp_date));
        $new_date=date("Y-m-d",strtotime($ecxp_date));
		$month=intval($exp_month);
		$where=array('ml_year.year'=>$exp_year);
		$eyear=$this->payroll_model->get_row_by_id('ml_year',$where);
		$year=$eyear->ml_year_id;
      //$file=$this->input->post('receipt');
        //$config['upload_path'] = 'upload/expense_receipts';

		$emp_id=$this->input->post('emp_id');
		$exp_type=$this->input->post('expense_type');
		$check_month=$month;
		$check_year=$year;
		//$check_expense=$this->payroll_model->check_expense_records($emp_id,$exp_type,$check_month,$check_year);
		//if(!empty($check_expense)){
			
			 // $this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#F00 !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Sorry You Already Set this Expense!</span>');
			//redirect('payroll/add_expense');
			//			}
		//	else{

        $data['empl'] = $this->payroll_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
        $employment_id=$data['empl']->employment_id;

        //echo $data['empl']->employment_id; die;
        //print_r($data['empl']);die;

		
		//$month=$this->input->post('month');
		//$year=$this->input->post('year');
        $config['upload_path'] = 'upload/expense_receipts';
        $config['allowed_types'] = 'doc|docx|gif|jpeg|pdf|jpg|png|rtf|txt|text';
        $config['max_size'] = '6000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $this->load->library('upload', $config);
        $file=$this->upload->do_upload('receipt');
        if($file)
        {

            $data = array('upload_data' => $this->upload->data());
            $file_name=$data['upload_data']['file_name'];
            $EXpdate = $this->input->post('expdate');
            if (isset($EXpdate) && !empty($EXpdate)) {
                $EXpdate = date('Y-m-d', strtotime($EXpdate));
            }
            $data=array(
                'employment_id'			 => $employment_id,
                //'employment_type_id' =>	$this->input->post('employee_type'),
                'expense_type'			 =>	$this->input->post('expense_type'),
                'expense_date'	   	 	 =>	$EXpdate,
                'amount'			     =>	$this->input->post('expamount'),
                'month'					 => $month,
                'year'					 => $year,
                'file_name'              => $file_name,
                'date_created'	      	 =>	date('Y-m-d'),
                'created_by'		     =>	$this->session->userdata('employee_id'), // need to change for session user id
            );
           //print_r($data);die;
            $query=$this->payroll_model->expense_new_record('expense_claims',$data);
            if($query)
            {
                $this->session->set_flashdata('message','<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Expense claim Set Successfully with attachment!</span>');
                redirect('payroll/add_expense');
            }
        }
        else
        {
            $data=array(
                'employment_id'			 =>	 $employment_id,
                //'employment_type_id' =>	$this->input->post('employee_type'),
                'expense_type'			 =>	$this->input->post('expense_type'),
                'expense_date'	   	 	 =>	$new_date,
                'amount'			     =>	$this->input->post('expamount'),
                'month'					 =>$month,
                'year'					 =>$year,
                'date_created'	      	 =>	date('Y-m-d'),
                'created_by'		     =>	$this->session->userdata('employee_id'), // need to change for session user id
            );
            $query=$this->payroll_model->expense_new_record('expense_claims',$data);
            if($query)
            {
                $this->session->set_flashdata('message','<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Expense claim Set Successfully without attachment!</span>');
                redirect('payroll/add_expense');
            }
        }


		


	//}
	}
	// End of Function for add expenses records //
	
	// Function for expense transaction //
	public function expense_transaction()
	{
		$emp_id=$this->uri->segment(3);
		$amount=$this->uri->segment(4);
		$exp_id=$this->uri->segment(5);
		//echo $emp_id."<br>".$amount."<br>".$exp_id; die;
		$query=$this->payroll_model->expense_transaction_model($emp_id,$amount,$exp_id);
		if($query)
		{
			redirect('payroll/expense_mgt');
		}
	}
	// END
	
	// Function for Update expense records //

	public function updte_expense_records()
	{
        $expdate = $this->input->post('expdate');
        if (isset($expdate) && !empty($expdate)) {
            $expdate = date('Y-m-d', strtotime($expdate));
        }
		$where=array(
		'expense_claim_id'=>$this->input->post('exp_id'));
		$data=array(
		//'employment_type_id'	 =>	$this->input->post('employee_type'),
		'expense_type'		   =>	$this->input->post('expense_type'),
		'expense_date'		   =>	$expdate,
		'amount'				 =>	$this->input->post('expamount'),
		//'status'				 =>	$this->input->post('status'),
		'modified_by'			=>	$this->session->userdata('employee_id'), // need to change for session user id
		'last_modify_date'	   =>	date('Y-m-d'),
		'approval_date'		  =>	date('Y-m-d')
		);
		
		$query=$this->payroll_model->update_record('expense_claims',$where,$data);
		if($query)
		{
			redirect('payroll/expense_mgt');
		}
	}
	
	// Function for Update expenses records //
	
	// Function for Add Allowance Records //
	public function add_allowance_record()
	{
		$allowances=array(
		'employee_id'			=>$this->input->post('emp_id'),
		'ml_allowance_type_id'   =>$this->input->post('allowance_type'),
		'pay_frequency'		  =>$this->input->post('pay_frequency'),
		'allowance_amount'	   =>$this->input->post('allowance_amount'),
		'effective_date'		 =>$this->input->post('allowance_date'),
		'created_by'			 =>$this->session->userdata('employee_id'), // need change later for session user id
		'date_rec_created'	   =>date('Y-m-d'),
		'status'				 =>1
		);
		$month=$this->input->post('month');
		$year=$this->input->post('year');

		$query=$this->payroll_model->add_allowance_records($allowances,$month,$year);
	if($query)
		{
			redirect('payroll/allowance');
		}
	} 
	
	// Function for Update allowance records //

	public function updte_allowance_record()
	{
		
		
			$data=array(
		'ml_allowance_type_id'=>$this->input->post('allowance_type'),
		'pay_frequency'=>$this->input->post('pay_frequency'),
		'allowance_amount'=>$this->input->post('amount'),
		'effective_date'=>$this->input->post('aldate'),
		'modified_by'=>$this->session->userdata('employee_id'),// need change for session user id
		'date_last_modified'=>date('Y-m-d'),
		'status'=>$this->input->post('status'),
		'approved_by'=>$this->session->userdata('employee_id'), // need change for session user id
		'date_approved'=>date('Y-m-d')
		);
		$where=array('allowance_id'=>$this->input->post('alwnc_id'));
		$query=$this->payroll_model->update_record('allowance',$where,$data);
		if($query)
		{
			redirect('payroll/allowance');
		}
	}
	
	// Function for add increment Record //
	
	public function add_increments_record()
	{
		$increments=array(
		'employee_id'=>$this->input->post('emp_id'),
		'increment_type_id'=>$this->input->post('increment_type'),
		'increment_amount'=>$this->input->post('increment_amount'),
		'date_effective'=>$this->input->post('increment_date'),
		'created_by'=>$this->session->userdata('employee_id'), // need to change later when session start
		'date_created'=>date('Y-m-d'),
		'status'=>1,
		);
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$query=$this->payroll_model->add_increments_records($increments,$month,$year);
		if($query)
		{
			redirect('payroll/increment');
		}
	
	}
	// End of function for add record of increment against employee//
	
	// Function for increment editing/updation //
	public function edit_increment($id = NULL)
	{
		$where1=array('enabled'=>1,
			 		 'trashed'=>0);
		$where=array('increments.increment_id'=>$id);
		$data['row']=$this->payroll_model->increment_data_fetch($where);
		$data['employee']=$this->payroll_model->html_selectbox
		('ml_employment_type','Select Eemployment Type','employment_type_id','employment_type',$where1);
		$data['increment_type']=$this->payroll_model->
		html_selectbox('ml_increment_type','-- Select Increment Type --','increment_type_id','increment_type',$where1);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select status Type --','status_id','status_title',$where1);
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_increment',$data);
		$this->load->view('includes/footer');
	}
	// END
	
	// Function for Update increment records //

	public function updte_increment_records()
	{
		$where=array(
		'increment_id'=>$this->input->post('incr_id'));
		$data=array(
		'increment_type_id'	   =>	$this->input->post('increment_type'),
		'date_effective'		  =>	$this->input->post('incEftDate'),
		'increment_amount'	    =>	$this->input->post('incamount'),
		'last_modified_by'	    =>	$this->session->userdata('employee_id'), // need change later for login user when session start
		'modify_date'	    	 =>	date('Y-m-d'),
		'approval_date'		   =>	date('Y-m-d'),
		'status'				  =>	$this->input->post('status'),
		'approved_by'			 =>	$this->session->userdata('employee_id'),  // need change later for login user when session start
		'approval_date'		   =>	date('Y-m-d'),
		'status'				  =>	$this->input->post('status')
			);

		$query=$this->payroll_model->update_record('increments',$where,$data);
		if($query)
		{
			redirect('payroll/increment');
		}
	}
	
	// Function for loan and advance pay //
	
	public function loan_advance_pay()
	{
		$emp_id=$this->input->post('emp_id');
		$advance_month=$this->input->post('month');
		$advance_year=$this->input->post('year');
		$advance_type=$this->input->post('payment_type');
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;
		$check=$this->payroll_model->check_advances($employmentID,$advance_type,$advance_month,$advance_year);
			
			if(!empty($check)){
			  $this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#F00 !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Sorry You Already Submit this Request!</span>');
			redirect('payroll/pay_advance');
						}else{
		$loan_advance=array(
		'employment_id'			 =>$employmentID,
		'payment_type'			 =>$this->input->post('payment_type'),
		'amount'				 =>$this->input->post('payment_amount'),
		'month'					 =>$this->input->post('month'),
		'year'					 =>$this->input->post('year'),
		'date_created'			 =>date('Y-m-d'),
		'created_by'			 =>$this->session->userdata('employee_id'), // need to change later for session user id
		);

		$query=$this->payroll_model->loan_advance_claim($loan_advance);
		

		if($query)
		{
			$this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Advance Request Submit succesfully!</span>');
			redirect('payroll/pay_advance');
		}}
	}
	// End
	
	
	// Function for loan/advance transaction //
	public function loanadvance_transaction()
	{
		$emp_id=$this->input->post('emp_id');
		$loan_advance_id=$this->input->post('loan_id');
	
	    $account_id=$this->input->post('account_id');
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;
		if(!empty($account_id))
		{$payment_account=$account_id;}
		else{
		$payment_account=array(
		'employee_id'			     =>$this->input->post('emp_id'),
		'account_no'				  =>$this->input->post('no_account'),
		'account_title'			   =>$this->input->post('title_account'),
		'bank_name'				   =>$this->input->post('bank_account'),
		'branch'			 		  =>$this->input->post('branch_account'),
		'branch_code'			 		  =>$this->input->post('branch_code')
		);
		}
//print_r($payment_account);die;
		$payment_cheque=array(
		'cheque_no'=>$this->input->post('cheque_no'),
		'bank_name'=>$this->input->post('bank_chiq')
		);
		
		$payment_cash=array(
		'received_by'=>$this->input->post('recieved'),
		'remarks'=>$this->input->post('remarks')
		);
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$payment_mode=$this->input->post('payment_mode');
		//echo $emp_id."<br>".$loan_advance_id."<br>".$payment_mode; die;
		if($payment_mode ==3){
		$query=$this->payroll_model->loan_advance_account_trans($loan_advance_id,$payment_account,$month,$year,$employmentID,$account_id);
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->loan_advance_cheque_trans($loan_advance_id,$payment_cheque,$month,$year,$employmentID);
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->loan_advance_cash_trans($loan_advance_id,$payment_cash,$month,$year,$employmentID);
		}
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		/////
	
	}
	// END
	
	
	
	// Function for Edit loan and advance page //
	public function edit_loan_advance()
	{
		$id=$this->uri->segment(3);
		//$where=array('loan_advances.loan_advance_id'=>$id);
		$where1=array('enabled'=>1,
		'trashed'=>0);
		//$data['check_pmode']=$this->payroll_model->check_payment_mode($id);
		//echo $data['check_pmode']->payment_mode_type_id;
		//die;
		$data['months']=$this->payroll_model->html_selectbox('ml_month','-- Select Month --','ml_month_id','month',$where1);
		$data['years']=$this->payroll_model->html_selectbox('ml_year','-- Select Year --','ml_year_id','year',$where1);
		//$data['payment_type']=$this->payroll_model->
		//html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where1);
		$data['status']=$this->payroll_model->
		html_selectbox('ml_status','-- Select status Type --','status_id','status_title',$where1);
		//$data['payment_mode']=$this->payroll_model->
		//html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where1);
		$data['info']=$this->payroll_model->loan_advance_fetch($id);
		//print_r($data['info']);
		//die;
		$this->load->view('includes/header');
		$this->load->view('payroll/edit_pay_advance',$data);
		$this->load->view('includes/footer');
		//echo "<pre>";
//		echo $data['check_pmode']->payment_mode_type;
//		die;
	}
	// END
	// Function for loan advance Updation //
	public function update_loan_advance()
		{
		$loan_id=$this->input->post('loan_id');
		$amount=$this->input->post('payment_amount');
		$data=array(
		'amount'=>$amount,
		'modified_by'=>$this->session->userdata('employee_id'),
		'last_modify_date'=>date('Y-m-d'),
         'status'=>1
		);
		$where=array(
		'loan_advance_id'=>$loan_id);
		$query=$this->payroll_model->update_record('loan_advances',$where,$data);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		
		}
	
	// Function for loan advance Updation //
	public function update_loan_advance_pay()
		{
			$loan_advance_id=array('loan_advance_id'=>$this->input->post('loan_advance_id'));
			$cash_payment_id=array('cash_payment_id'=>$this->input->post('cash_payment_id'));
			$account_payment_id=array('bank_account_id'=>$this->input->post('account_payment_id'));
			$cheque_payment_id=array('cheque_payment_id'=>$this->input->post('cheque_payment_id'));
			$payment_mode_id=array('payment_mode_id'=>$this->input->post('payment_mode_id'));
			$transaction_id=array('transaction_id'=>$this->input->post('transaction_id'));
			$loan_advance_payment_id=array('loan_advance_payment_id'=>$this->input->post('loan_advance_payment_id'));
			
			
			
			
			
		
		$loan_advance_up=array(
		//'employee_id'			 =>$this->input->post('emp_id'),
		'payment_type'			=>$this->input->post('payment_type'),
		'amount'				  =>$this->input->post('payment_amount'),
		'last_modify_date'		=>date('Y-m-d'),
		'modified_by'			 =>$this->session->userdata('employee_id'), // need to change later for session user id
		'status'				  =>	$this->input->post('status')
		);
		
		$payment_account_up=array(
		//'employee_id'			     =>$this->input->post('emp_id'),
		//'bank_name'				   =>$this->input->post('bank_account'),
		'account_no'				  =>$this->input->post('account_no'),
		'account_title'			   =>$this->input->post('account_title'),
		'bank_name'				   =>$this->input->post('bank_account'),
		'branch'			 		  =>$this->input->post('branch')
		
				);
		

		$payment_cheque_up=array(
		'cheque_no'=>$this->input->post('cheque_no'),
		'bank_name'=>$this->input->post('bank_chiq')
		);
		
		$payment_cash_up=array(
		'received_by'=>$this->input->post('recieved'),
		'remarks'=>$this->input->post('remarks')
		);
		
		$payment_mode=$this->input->post('payment_mode');

		if($payment_mode ==3){
		$query=$this->payroll_model->loan_advance_account_update($loan_advance_up,$loan_advance_id,$payment_account_up,$account_payment_id,$payment_mode_id,$transaction_id,$loan_advance_payment_id);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->loan_advance_cheque_update($loan_advance_up,$loan_advance_id,$payment_cheque_up,$cheque_payment_id,$payment_mode_id,$transaction_id,$loan_advance_payment_id);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->loan_advance_cash_update
		($loan_advance_up,$loan_advance_id,$payment_cash_up,$cash_payment_id,$payment_mode_id,$transaction_id,$loan_advance_payment_id);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		
	}
	// End
	////////////////////// Function for LOAN ADVANCE PAY BACK ////////////////////////////////
	
	public function loan_advance_payback()
	{
		$loan_advance_id	=		 $this->input->post('loan_advance_id');
		$pay_amount		    =		 $this->input->post('payback_amount');
        $total_amount       =        $this->input->post('total_amount');
		$account_id=$this->input->post('account_id');
        $employee_id=$this->input->post('emp_id');
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;
		$payment_account=array(
		'employee_id'			     =>$this->input->post('emp_id'),
		'account_no'				  =>$this->input->post('no_account'),
		'account_title'			   =>$this->input->post('title_account'),
		'bank_name'				   =>$this->input->post('bank_account'),
		'branch'			 		  =>$this->input->post('branch_account'),
		'branch_code'			 		  =>$this->input->post('branch_code')
				);

		$payment_cheque=array(
		'cheque_no'=>$this->input->post('no_cheque'),
		'bank_name'=>$this->input->post('bank_cheque')
		);
		
		$payment_cash=array(
		'received_by'=>$this->input->post('recieved'),
		'remarks'=>$this->input->post('remarks')
		);
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$payment_mode=$this->input->post('payment_mode');
		if($payment_mode ==3){
		$query=$this->payroll_model->loan_advance_payback_account($payment_account,$pay_amount,$loan_advance_id,$month,$year,$account_id,$total_amount,$employmentID);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->loan_advance_payback_cheque($payment_cheque,$pay_amount,$loan_advance_id,$month,$year,$total_amount,$employmentID);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->loan_advance_payback_cash($payment_cash,$pay_amount,$loan_advance_id,$month,$year,$total_amount,$employmentID);
		if($query)
		{
			redirect('payroll/advance_salary');
		}
		}
		
	}
	// End
	
	///////END
	
	
	////////////// Function for Pay salary to Employee //////////////////
	
	public function pay_salary_amount()
	{
        $postedAbsenteeDeduction = $this->input->post('absenteesDeduction');
         if(!isset($postedAbsenteeDeduction) || empty($postedAbsenteeDeduction)){
             $deductible_amount=0;
         }else{
             $deductible_amount=intval($this->input->post('deductible_amount'));
         };


	     $month=$this->input->post('month');
		 $year=$this->input->post('year');
		 $salary_id=$this->input->post('salary_id');
		$pay_amount=$this->input->post('pay_amount');
		
		$partial_pay=$this->input->post('to_pay');
		$arears=$this->input->post('arears');

	
		//echo $pay_amount; die;
		$account_id=$this->input->post('account_id');
		if(!empty($account_id))
		{$payment_account=$account_id;}
		else{
		$payment_account=array(
		'employee_id'			     =>$this->input->post('emp_id'),
		'account_no'				  =>$this->input->post('no_account'),
		'account_title'			   =>$this->input->post('title_account'),
		'bank_name'				   =>$this->input->post('bank_account'),
        'active'				   =>1,
		'branch'			 		  =>$this->input->post('branch_account')
		);
		}
		//echo "<pre>"; print_r($payment_account); die;
		$payment_cheque=array(
		'cheque_no'=>$this->input->post('no_cheque'),
		'bank_name'=>$this->input->post('bank_cheque')
		);
		
		$payment_cash=array(
		'received_by'=>$this->input->post('recieved'),
		'remarks'=>$this->input->post('remarks')
		);
		
		$payment_mode=$this->input->post('payment_mode');
		$deduction_amount=$this->input->post('deduction_amount');
		$expense_amount=$this->input->post('expense_amount');

		$advance_amount=$this->input->post('loan_amount');
        $total_adv_amount=$this->input->post('total_loan_amount');
        $advance_balance=$this->input->post('advance_balance');
        $remain_balance=$this->input->post('remain_balance');

		$advance_id=$this->input->post('loan_ids');
		$exp_ids=$this->input->post('exp_ids');
		$ded_ids=$this->input->post('ded_ids');
		$allowance_amount=$this->input->post('allowance_amount');
		$alw_ids=$this->input->post('alw_ids');
		
			if($payment_mode ==3){
		$query=$this->payroll_model->salary_trans_account($account_id,$salary_id,$payment_account,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$total_adv_amount,$advance_balance,$remain_balance,$deductible_amount);
		//var_dump($query); die;
		if($query)
		{
			redirect('payroll/pay_salary/gess/'.$month.'/'.$year);
		}
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->salary_trans_cheque($salary_id,$payment_cheque,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$total_adv_amount,$advance_balance,$remain_balance,$deductible_amount);
		if($query)
		{
			redirect('payroll/pay_salary/gess/'.$month.'/'.$year);
		}
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->salary_trans_cash($salary_id,$payment_cash,$pay_amount,$month,$year,$ded_ids,$deduction_amount,$exp_ids,$expense_amount,$advance_amount,$advance_id,$allowance_amount,$alw_ids,$partial_pay,$arears,$total_adv_amount,$advance_balance,$remain_balance,$deductible_amount);
		if($query)
		{
			redirect('payroll/pay_salary/gess/'.$month.'/'.$year);
		}
		}	
		
	}
	
	///////////////////////////////////////////////////////////////////////
	/*public function pay_salary_amount()
	{
		 $month=$this->input->post('month');
		 $year=$this->input->post('year');
		
		$salary_id=$this->input->post('salary_id');
	
		$pay_amount=$this->input->post('pay_amount');
		$query=$this->payroll_model->salary_pay_model($salary_id,$pay_amount,$month,$year);
		if($query)
		{
			redirect('payroll/pay_salary/gess/'.$month.'/'.$year);
		}
		
	}*/
	
	
/////////////////////////////////////////////////////////////////////////////////////	
	///////////////////salary transaction form///
	public function salary_payment_trans()
	{
		$id=$this->uri->segment(3);
		$data['info']=$this->payroll_model->salarypayment_trans($id);
		//echo "<pre>";
		//print_r($data['info']); die;
		$where1=array('enabled'=>1,
		'trashed'=>0);
		$data['payment_mode']=$this->payroll_model->
		html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where1);
		$this->load->view('includes/header');
		$this->load->view('payroll/pay_salary_transaction',$data);
		$this->load->view('includes/footer');
	}
	////// Pay salary transaction////
	public function pay_salary_transaction()
	{
		 $month=$this->input->post('month');
		 $year=$this->input->post('year');
		//echo $month."<br>".$year; die;
		$salary_payment_id=$this->input->post('salary_payment_id');
	   // echo $salary_payment_id; die;
		$pay_amount=$this->input->post('pay_amount');
		//echo $pay_amount; die;
		$payment_account=array(
		'employee_id'			     =>$this->input->post('emp_id'),
		'account_no'				  =>$this->input->post('no_account'),
		'account_title'			   =>$this->input->post('title_account'),
		'bank_name'				   =>$this->input->post('bank_account'),
		'branch'			 		  =>$this->input->post('branch_account')
		);
		//echo "<pre>"; print_r($payment_account); die;
		$payment_cheque=array(
		'cheque_no'=>$this->input->post('no_cheque'),
		'bank_name'=>$this->input->post('bank_cheque')
		);
		
		$payment_cash=array(
		'received_by'=>$this->input->post('recieved'),
		'remarks'=>$this->input->post('remarks')
		);
		
		$payment_mode=$this->input->post('payment_mode');
		///echo $payment_mode; die;
		
			if($payment_mode ==3){
		$query=$this->payroll_model->salary_trans_account($salary_payment_id,$payment_account,$pay_amount,$month,$year);
		if($query)
		{
			redirect('payroll/salary_payment');
		}
		}
		if($payment_mode ==2){
		$query=$this->payroll_model->salary_trans_cheque($salary_payment_id,$payment_cheque,$pay_amount,$month,$year);
		if($query)
		{
			redirect('payroll/salary_payment');
		}
		}
		if($payment_mode ==1){
		$query=$this->payroll_model->salary_trans_cash($salary_payment_id,$payment_cash,$pay_amount,$month,$year);
		if($query)
		{
			redirect('payroll/salary_payment');
		}
		}
		//$this->payroll_model->salary_transactions();
	}
	// End
	
	public function test()
	{
		$rec=$this->payroll_model->salarypayment_count();
		$count=count($rec);
		$config['base_url']     = base_url().'/payroll/test';
		$config['total_rows']   = $count;
		$config['per_page']     = 3;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$emp_name=$this->input->post('emp_name');
		$selc_month=$this->input->post('month');
		$selc_year=$this->input->post('year');
		$data['info']=$this->payroll_model->salarypayment_test($page,$emp_name,$selc_month,$selc_year);
		//echo "<pre>"; print_r($data['info']); die;
		$data["links"] = $this->pagination->create_links();
		$data['months']=$this->payroll_model->ml_month_datatable('ml_month','ml_month_id','month');
		$data['years']=$this->payroll_model->ml_year_datatable('ml_year','ml_year_id','year');
		$data['select_month']=$this->input->post('month');
		//$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/test_v',$data);
		$this->load->view('includes/footer');
	}
	
///////////////////////Function for generating salary slip//////////////////////
	public function salary_gen_slip()
	{
		$rec=$this->payroll_model->salarypayment_slip_count();
		$count=count($rec);
		$config['base_url']     = base_url().'/payroll/salary_gen_slip';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$limit=$config['per_page'];
		$data["links"] = $this->pagination->create_links();
		$emp_name=$this->input->post('emp_name');
		$selc_month=$this->input->post('month');
		$selc_year=$this->input->post('year');
		$data['months']=$this->payroll_model->ml_month_datatable('ml_month','ml_month_id','month');
		$data['years']=$this->payroll_model->ml_year_datatable('ml_year','ml_year_id','year');
		$data['info']=$this->payroll_model->salarypayment_slips($limit,$page,$emp_name,$selc_month,$selc_year);
		$this->load->view('includes/header');
		$this->load->view('payroll/salary_genslips',$data);
		$this->load->view('includes/footer');
	}
	
	public function slips()
	{
//        error_reporting(0);
	
	if($this->input->post('next') == "gess"){
				//echo  $this->input->post('next'); die;
////////////////////// 3rd Step ////////////////////////////////////////////////////
	$this->session->set_userdata(array('count'=>$this->session->userdata('count')-1));
				$data['no']=$this->session->userdata('count')-1;
				if($this->session->userdata('count') == 0){
					redirect('payroll/salary_gen_slip');
				}
				
					 $arr = $this->session->userdata('salemps');
		$emp_id=$arr['salemp_id'.$this->session->userdata('count')];
////////////////////////////////4th Step //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$where2=array(
		'salary_payment.salary_payment_id'=>$emp_id
		);
		$month_year=array(
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		);
		////
		$data['info']=$this->payroll_model->salarypayment_slip_emp($where2);
	    $em_id=$data['info']->employee_id;
	    $date_id=$data['info']->transaction_date;
	    $data['ded_trans']=$this->payroll_model->payroll_regded_trans($em_id,$month,$year,$date_id);
		$ded_trans_id=$data['ded_trans']->transaction_id;
		//$data['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$data['allw_trans']=$this->payroll_model->payroll_regallw_trans($em_id,$month,$year,$date_id);
		$data['exp_trans']=$this->payroll_model->payroll_regexp_trans($em_id,$month,$year,$date_id);
		$data['advance_trans']=$this->payroll_model->payroll_regadvance_trans($em_id,$month,$year,$date_id);
	
		}
		else
		{
			/////////////////// First step ////////////////////
			///////// employee id
		$employee_list=array();
		$no=1;
		foreach($this->input->post('salemp') as $check){
			$employee_list['salemp_id'.$no] = $check;
			$no++;}
			$newdata = array();
		$newdata['salemps'] = $employee_list;
	$this->session->set_userdata($newdata);
		 
		 $arr = $this->session->userdata('salemps'); 
		
		 $count = count($this->session->userdata('salemps')); 
		
		 $this->session->set_userdata(array('count'=>$count));
		$emp_id=$arr['salemp_id'.$this->session->userdata('count')];
		  ///////////end salary_payment id
	
	///////////////// 2nd step //////////////////////////////////////////
	
			  
	/////////////////////////////////////////////////////////////////////
		//echo "<pre>"; print_r($newdata['emps']); die;
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$where2=array(
		'salary_payment.salary_payment_id'=>$emp_id,
		'transaction.transaction_type'=>5,
		/*'transaction.transaction_date'=>$date_id*/
		);
		$month_year=array(
		'salary_payment.month'=>$month,
		'salary_payment.year'=>$year
		);
	///////////////////// testing 
	$data['info']=$this->payroll_model->salarypayment_slip_emp($where2);
			/*echo "<pre>";
			print_r($data['info']); die;*/
	$em_id=$data['info']->employee_id;
	$date_id=$data['info']->transaction_date;
	$data['ded_trans']=$this->payroll_model->payroll_regded_trans($em_id,$month,$year,$date_id);
	$ded_trans_id=$data['ded_trans']->transaction_id;
	//$data['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
    $data['allw_trans']=$this->payroll_model->payroll_regallw_trans($em_id,$month,$year,$date_id);
    $data['exp_trans']=$this->payroll_model->payroll_regexp_trans($em_id,$month,$year,$date_id);
    $data['advance_trans']=$this->payroll_model->payroll_regadvance_trans($em_id,$month,$year,$date_id);

	////////////////////
	//$data['info']=$this->payroll_model->salarypayment_slip_emp($where2);
		//echo "<pre>"; print_r($data['info']); die;
			}
			
		$this->load->view('payroll/salary_cheque',$data);
	}
////////////END

///////////////////////////// Trnsafer salary transactions Bank //////////////////////
	 public function trans_banktransfer()
	 {
		$rec=$this->payroll_model->transaction_info_count();
		$count=count($rec);
		$config['base_url']     = base_url().'/payroll/trans_banktransfer';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$limit=$config['per_page'];
		$data["links"] = $this->pagination->create_links();
	   $data['slct_y']=$this->input->post('year');
	   $data['slct_m']=$this->input->post('month');
	   $data['month']=$this->payroll_model->master_list_data('ml_month','Select Month','month','month');
	   $data['year']=$this->payroll_model->master_list_data('ml_year','Select Year','year','year');
	   $data['info']=$this->payroll_model->transaction_info($limit,$page);
	   $this->load->view('includes/header');
	   $this->load->view('payroll/bank_transfer',$data);
	   $this->load->view('includes/footer');
	 }
	 
	 public function bank_transfer_invoice()
	 {
//         error_reporting(0);
		 $data['info']=$this->payroll_model->transaction_info();
		 $this->load->view('payroll/page_invoice',$data);
	 }


	public function reviewsalary_time()
	{	$month=$this->input->post('month');
		$year=$this->input->post('year');
		$data['info']=1;
		/*$where=array(
		'salary_payment.month !='=>$month
		);*/
		//$data['info']=$this->payroll_model->employee_view_salary($month);
		$data['months']=$this->payroll_model->ml_month();
		$data['years']=$this->payroll_model->ml_year();
		$this->load->view('includes/header');
		$this->load->view('payroll/reviewsalary_time',$data);
		$this->load->view('includes/footer');
	}

	/////// Function for payroll sheet 
	public function salary_sheet()
	{
		$data['month']=$this->input->post('month');
		$data['year']=$this->input->post('year');
		$data['project_selected']=$this->input->post('project');
        if(empty($data['month']))
        {
            redirect('payroll/reviewsalary_time');
        }
		$project=$this->input->post('project');
		$name=$this->input->post('name');
		$code=$this->input->post('code');


		if(!empty($data['month']))
		{
			$month=$data['month'];
			$year=$data['year'];
			//$SProject=$project;
			$this->session->set_userdata('month',$month);
			$this->session->set_userdata('year',$year);
			// $this->session->set_userdata('project_select',$SProject);

		}
		else{
			$month=$this->session->userdata('month');
			$year=$this->session->userdata('year');
			//$data['project_selected']=$this->session->userdata('project_select');
		}

		if($this->input->post("formData"))
		{
            $newdata=$this->input->post("formData");
		    $explode=explode("&",$newdata);
			//print_r($explode);
			/////// for code
			$expCode=explode("filter_code=",$explode[5]);
			$code=$expCode[1];
			/////// for name
			$expName=explode("filter_name=",$explode[4]);
			$name=$expName[1];

			/////// for month
			$expMonth=explode("month=",$explode[2]);
			$month=$expMonth[1];
            if(isset($month) && empty($month)){
                redirect('payroll/reviewsalary_time');
            }
			/////// for year
			$expYear=explode("year=",$explode[3]);
			$year=$expYear[1];
			/////// for project
			$expProject=explode("filter_project=",$explode[6]);
			$project=$expProject[1];

			$where_project=array('ml_projects.project_id'=>$project);
			$where_month=array('ml_month.ml_month_id'=>$month);
			$where_year=array('ml_year.ml_year_id'=>$year);
			$data['project_info']=$this->payroll_model->get_row_by_where('ml_projects',$where_project);
			$data['month_info']=$this->payroll_model->get_row_by_where('ml_month',$where_month);
			$data['year_info']=$this->payroll_model->get_row_by_where('ml_year',$where_year);
			$data['info']=$this->payroll_model->payroll_sheet_print($month,$year,$name,$code,$project);
			$this->load->view('payroll/payroll_sheet',$data);
			return;
		}
		$rec=$this->payroll_model->count_payrollsheet($month,$year);
		$count=count($rec);
		$config['base_url']     = base_url().'/payroll/salary_sheet';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$limit=$config['per_page'];
		$data["links"] = $this->pagination->create_links(); 
		
		$data['month']=$this->input->post('month');
		$data['year']=$this->input->post('year');
		$data['project_selected']=$this->input->post('project');

		$project=$this->input->post('project');
		$name=$this->input->post('name');
		$code=$this->input->post('code');



		if(!empty($data['month']))
		{
			$month=$data['month'];
			$year=$data['year'];
			//$SProject=$project;
		   $this->session->set_userdata('month',$month);
		   $this->session->set_userdata('year',$year);
		  // $this->session->set_userdata('project_select',$SProject);
		  
		}
		else{
			$month=$this->session->userdata('month');
			$year=$this->session->userdata('year');
			//$data['project_selected']=$this->session->userdata('project_select');
			}
		

		$data['projects']=$this->payroll_model->get_all('ml_projects');
        /*if($this->input->post('name')){
            $info=$this->payroll_model->payroll_sheet($limit,$page,$month,$year,$name,$code,$project);
            //print_r($info);
            //echo "<br>";
           print_r(json_encode($info));
            return;
        }*/
        $data['name']=$name;
        $data['code']=$code;
        $data['proj']=$project;
        $data['info']=$this->payroll_model->payroll_sheet($limit,$page,$month,$year,$name,$code,$project);

        $query=$this->db->last_query();

        // Code for CSV File
        if(!empty($data['info'])){
            $this->load->dbutil();
            $this->load->helper('file');
            $newline="\r\n";
            $datas=$this->common_model->get_query_record($query);
            //$query=json_encode($query);
            $csv=$this->dbutil->csv_from_result($datas,',',$newline);
            $csv=str_replace("employee_id","",$csv);
            $csv=str_replace("expense_claim_id","",$csv);
            $csv=str_replace("exp_dis_id","",$csv);
//        $csv = ltrim(strstr($this->dbutil->csv_from_result($data, ',', "\r\n"), "\r\n"));

            $this->load->helper('download');

            $uploadPath = 'systemGeneratedFiles/PayRollSalarySheetCsvReports/'.$this->data['EmployeeID'].'/SalarySheet'.time().'.csv';
            $uploadDirectory = 'systemGeneratedFiles/PayRollSalarySheetCsvReports/'.$this->data['EmployeeID'];
            //If Directories are Not Avaialable, Create The Directories On The Go..
            if(!is_dir($uploadDirectory)){
                mkdir($uploadDirectory, 0755, true);
            }

            $path= 'systemGeneratedFiles/PayRollSalarySheetCsvReports/'.$this->data['EmployeeID'].'/';
            $delete_files= glob($path.'*');
            foreach($delete_files as $dFile){ // iterate files
                if(is_file($dFile))
                    unlink($dFile); // delete file
                //echo $file.'file deleted';
            }

            //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
            //$fileName = ("Parexons_GeneratedReport_HR_CSV_'.time().'.csv",$csv);
            //$filePath = $uploadPath.$fileName;
            write_file($uploadPath,$csv, 'w+');

            $data['files']=$uploadPath;
        }
        //END
	   //echo "<pre>";
	   //print_r($data['info']);
        // Page format for PDF download
        $data['view']=$this->load->view('payroll/salary_sheet_pdf',$data,true);
        //End
       $date['name_filter']=$this->input->post('name');
	   $this->load->view('includes/header');
	   $this->load->view('payroll/salary_sheet',$data);
	   $this->load->view('includes/footer');
	}
	// End

    // Function for Staff Salary Sheet PDF Download
    public function downloadPdfSalarySheetReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/PayRollSalarySheetReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/PayRollSalarySheetReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/PayRollSalarySheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_SalarySheet_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }
    // END

    //Function for Send pdf file of Time Sheet in email
    function SendPDFPayrollSheetReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/PayRollSalarySheetReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/PayRollSalarySheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'PayRollSalarySheetReportEmail_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    // END
	////////// Function for staff salary
	public function staff_salary()
	{
		$data['proj']=$this->input->post('project');
		if($this->input->post("formData"))
		{$newdata=$this->input->post("formData");
			$explode=explode("&",$newdata);
			//print_r($explode);
			$expProject=explode("projectSelected=",$explode[2]);
			$projects=$expProject[1];
			/////// for name
			$expName=explode("name=",$explode[0]);
			$names=$expName[1];
            print_r($names);
			$data['info']=$this->payroll_model->staff_sallaries_print($projects,$names);
			//print_r($data['info']);
			$this->load->view('payroll/staff_salaray_list',$data);
			return;
		}
		$project=$this->input->post('project');
        //$name=$this->input->post('name');
		$rec=$this->payroll_model->employee_count($project);
		$count=count($rec);
       // print_r($count);die;
		$config['base_url']     = base_url().'/payroll/staff_salary';
		$config['total_rows']   = $count;
		$config['per_page']     = 10;
		$data['rows']=$config['per_page'];
		$config['uri_segment']  = 3;
		$config['prev_link']='previous';
		$config['next_link']='next';
		$config['full_tag_open'] = "<div id='pagination'>";
		$config['full_tag_close'] = "</div>";
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links']    =round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['page']=$page;
		$limit=$config['per_page'];
		$data["links"] =$this->pagination->create_links();
	    $project=$this->input->post('project');
	    $name=$this->input->post('name');

		//$count=count($data['info']);
	    $data['projects']=$this->payroll_model->get_all('ml_projects');
        $data['info']=$this->payroll_model->staff_sallaries($limit,$page,$project,$name);
        $query=$this->db->last_query();
        $data['view']= $this->load->view('payroll/staff_salaray_pdf',$data,true);

        // Code for CSV File
        if(!empty($data['info'])){

            $this->load->dbutil();
            $this->load->helper('file');
            $newline="\r\n";
            $datas=$this->common_model->get_query_record($query);
           //$query=$data['info'];
           //print_r($datas); die;

            $csv=$this->dbutil->csv_from_result($datas,',',$newline);
//        $csv = ltrim(strstr($this->dbutil->csv_from_result($data, ',', "\r\n"), "\r\n"));

            $this->load->helper('download');

            $uploadPath = 'systemGeneratedFiles/PayRollStaffSalaryReportCsv/'.$this->data['EmployeeID'].'/StaffSalary'.time().'.csv';
            $uploadDirectory = 'systemGeneratedFiles/PayRollStaffSalaryReportCsv/'.$this->data['EmployeeID'];
            //If Directories are Not Avaialable, Create The Directories On The Go..
            if(!is_dir($uploadDirectory)){
                mkdir($uploadDirectory, 0755, true);
            }

            $path= 'systemGeneratedFiles/PayRollStaffSalaryReportCsv/'.$this->data['EmployeeID'].'/';
            $delete_files= glob($path.'*');
            foreach($delete_files as $dFile){ // iterate files
                if(is_file($dFile))
                    unlink($dFile); // delete file
                //echo $file.'file deleted';
            }

            //$filePath = $uploadPath.$fileName;
            write_file($uploadPath,$csv, 'w+');
            $data['files']=$uploadPath;
        }
        // End

	    $this->load->view('includes/header');
	    $this->load->view('payroll/staff_salaray',$data);
	    $this->load->view('includes/footer');
	}

    // Function for Staff Salary PDF Download
    public function downloadPdfSalaryStaffReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/PayRollStaffSalaryReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/PayRollStaffSalaryReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/PayRollStaffSalaryReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_StaffSalary_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }
    // END
/*	public function staff_salary_print()
	{}*/

	public function account_mng()
	{
		$name=$this->input->post('name');
		$bank=$this->input->post('bank');
		$branch=$this->input->post('branch');
		$active=$this->input->post('active');
		$data['account_info']=$this->payroll_model->employees_account($name,$bank,$branch,$active);
		$this->load->view('includes/header');
		$this->load->view('payroll/account_mng',$data);
		$this->load->view('includes/footer');
	}

	public function active_account()
	{
		$account_id=$this->uri->segment(3);
		$employee_id=$this->uri->segment(4);
		$where=array(
			'bank_account_id'=>$account_id,
			'employee_id'=>$employee_id
			);
		$data=array('active'=>1);
		$query=$this->payroll_model->update_account_activation($data,$account_id,$employee_id);
		if($query)
		{redirect("payroll/account_mng");}
	}
	public function add_account()
	{
		$this->load->view('includes/header');
		$this->load->view('payroll/add_account');
		$this->load->view('includes/footer');
	}

	// Function for employee auto complete
	function autocomplete_employees()
	{
		//$this->load->model('model','get_data');
		$query= $this->payroll_model->get_autocomplete_employees();
		$i=1;
		foreach($query->result() as $row):
			?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_code;?>','<?php echo $row->CNIC;?>','<?php echo $row->employee_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_name;?>','<?php echo $row->base_salary;?>')"><?php echo $row->full_name;?></li> <?php
			//echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
		endforeach;
	}

	public function add_account_info()
	{
			$data=array(
				'employee_id'=>$this->input->post('emp_id'),
				'account_no'=>$this->input->post('no_account'),
				'account_title'=>$this->input->post('title_account'),
				'bank_name'=>$this->input->post('bank_account'),
				'branch'=>$this->input->post('branch_account'),
				'branch_code'=>$this->input->post('branch_code')
			);
		$query=$this->payroll_model->create_new_record('emp_bank_account',$data);
		if($query)
		{
			redirect('payroll/account_mng');
		}
	}

	public function update_transaction_review()
	{
		if($this->input->post('status')==3)
		{
			$alw_trans_id=$this->input->post('alw_trans_id');
			$ded_trans_id=$this->input->post('ded_trans_id');
			$adv_trans_id=$this->input->post('adv_trans_id');
			$exp_trans_id=$this->input->post('exp_trans_id');

			$exp_id=$this->input->post('exp_ids');


			$salary_payment_id=$this->input->post('salary_payment_id');
			$remarks=$this->input->post('remarks');
			$transaction_id=$this->uri->segment(3);
			//$where=array('transaction.transaction_id'=>$transaction_id);
			$review_data= array(
				'reviewed_by' 			=> $this->session->userdata('employee_id'),
				'review_date'			=> date('Y-m-d'),
				'salary_payment_id' 	=> $salary_payment_id,
				'remarks'               => $remarks);
			$transaction_data=array('status' => $this->input->post('status'));
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->fail_review($review_data,$transaction_data,$transaction_id,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id,$exp_id);
			if($query)
			{
				redirect('dashboard_site/approvals');
				return;
			}
		}
		else
		{
			$transaction_id=$this->uri->segment(3);
			$where=array('transaction_id'=>$transaction_id);
			$data= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'approval_date'			=> date('Y-m-d'),
				'status' 			    => $this->input->post('status'));
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->update_record('transaction',$where, $data);
			if($query)
			{
				redirect('dashboard_site/approvals');
				return;
			}
		}

	}
    public function domPDF_Test($id,$month,$year){
        // Load all views as normal

		$where=array(
			'employee.employee_id'=>$id,
			'salary_payment.month'=>$month,
			'salary_payment.year'=>$year
		);
		$query['last_salary']=$this->payroll_model->last_salary($id);
		$query['salary_info']=$this->payroll_model->month_salaries($where);
		$salary_trans_date=$query['salary_info']->transaction_date;
		$salary_payment_mode_id=$query['salary_info']->payment_mode;
		$query['payment_mode']=$this->payroll_model->payment_mode_details($salary_payment_mode_id);
		$query['ded_trans']=$this->payroll_model->payroll_regded_trans($id,$month,$year,$salary_trans_date);
		$ded_trans_id=$query['ded_trans']->transaction_id;
		//$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$query['allw_trans']=$this->payroll_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
		$query['exp_trans']=$this->payroll_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
		$query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);

		$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
		$where_emp=array(
			'employee.employee_id'=>$id
		);
		$query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);
		/*echo "<pre>";
		print_r($query['salaries']); die;*/

		//$this->load->view('payroll/pdf_reports/testPDF',$query);
        // Get output html

        $query['pdfView'] = $this->load->view('payroll/pdf_reports/testPDF',$query,true);
        $this->load->view('payroll/pdf_reports/payroll_reg_slip',$query);
	}
	public function print_payroll_detail($id,$month,$year){
		// Load all views as normal
		$where=array(
			'employee.employee_id'=>$id,
			'salary_payment.month'=>$month,
			'salary_payment.year'=>$year
		);
		$query['last_salary']=$this->payroll_model->last_salary($id);
		$query['salary_info']=$this->payroll_model->month_salaries($where);
		$salary_trans_date=$query['salary_info']->transaction_date;
		$salary_payment_mode_id=$query['salary_info']->payment_mode;
		$query['payment_mode']=$this->payroll_model->payment_mode_details($salary_payment_mode_id);
		$query['ded_trans']=$this->payroll_model->payroll_regded_trans($id,$month,$year,$salary_trans_date);
		$ded_trans_id=$query['ded_trans']->transaction_id;
		//$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
		$query['allw_trans']=$this->payroll_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
		$query['exp_trans']=$this->payroll_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
		$query['advance_trans']=$this->payroll_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);

		$query['details']=$this->payroll_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
		$where_emp=array(
			'employee.employee_id'=>$id
		);
		$query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);
		$query['email']=$this->hr_model->get_row_by_where('current_contacts',array('employee_id'=>$id));
		/*echo "<pre>";
		print_r($query['salaries']); die;*/

		//$this->load->view('payroll/pdf_reports/testPDF',$query);
		// Get output html
		$html = $this->output->get_output($this->load->view('payroll/pdf_reports/payroll_reg_view_print',$query));

		// Load library
		$this->load->library('dompdf_gen');

		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$output = $this->dompdf->output();

		$uploadPath = 'systemGeneratedFiles/generatedPDFReports/'.$this->data['EmployeeID'].'/';
		$uploadDirectory = 'systemGeneratedFiles/generatedPDFReports/'.$this->data['EmployeeID'];
		//If Directories are Not Avaialable, Create The Directories On The Go..
		if(!is_dir($uploadDirectory)){
			mkdir($uploadDirectory, 0755, true);
		}
		//Need To Create the PDF File Which We Need TO Send later Throught Attachment..
		/*header("Content-type:application/pdf");

// It will be called downloaded.pdf
		header("Content-Disposition:attachment;filename='downloaded.pdf'");

// The PDF source is in original.pdf
		readfile("original.pdf");*/
		$fileName = 'Parexons_GeneratedReport_PDF_'.time().'.pdf';
		$this->session->set_flashdata('fileName',$fileName);
		//$html = $this->output->get_output($this->load->view('payroll/pdf_reports/payroll_reg_view_print',$fileName));
		file_put_contents($uploadPath.$fileName, $output);

		//Now As We Have The File Which We Can Send Through Attachment, Lets Work on Email Stuff..
		//According To Hamid, Already Getting Employee's Email..
		$email = $this->hr_model->get_row_by_where('current_contacts',array('employee_id'=>$id));
		$email_id =  $email->email_address;

		//Now Lets Get The File We Have Just Recently Created.

		$fileatt = $uploadPath.$fileName; // Path to the file

		$to = $email_id;
		$subject = 'PayRoll Slip';
		$message = 'This Email is Send to you by Admin.Find the Attachment!';


		/*
                $this->load->library('email');

                $this->email->from('Admin@gmail.com', 'Admin');
                $email = $this->hr_model->get_row_by_where('current_contacts',array('employee_id'=>$id));
                $email_id =  $email->email_address;
                //echo $email_id;
                $this->email->to($email_id);

                $this->email->subject('PayRoll Slip');
                $this->email->message('This Email is Send to you by Admin.Find the Attachment!');

                $send = $this->email->send();
                if($send)
                {
                    echo "OK::Email Successfully Send To User::success::";

                    redirect('payroll/payroll_reg_slip/'.$id.'/'.$month.'/'.$year);
                    return;
                } else {
                    echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
        //			redirect('payroll/payroll_reg_slip/'.$id.'/'.$month.'/'.$year);
                    return;
                }*/
	}

///////////////////////////////    END      //////////////////////////////////////////

/**
 * Generate Reports For Salaries, Expenses, Deductions ETC..
 * It was Hard Or Was not looking possible to do have all data comeout from single query, so i am splitting work to different queries based on number of checks User Do on CheckBoxes.
 * @return view
 */
    public function generateSalaryReport($view = NULL){
        if($view != NULL && $view = 'generate'){
            if($this->input->post()){
                $PTable = 'employee E';
                //Lets Get All The POsted Data From The View.
                $selectedCheckBoxes = $this->input->post('selectData');
                $employeeID = $this->input->post('empID');
                $ProgramID = $this->input->post('ProgID');
                $projectID = $this->input->post('proID');
                $departmentID = $this->input->post('department');
                $reportHeading = $this->input->post('reportHead');
                $postedYear = $this->input->post('selectedYear');
                if(isset($postedYear) and !empty($postedYear)){
                    /*echo $postedYear;*/
                    $yearFilter = intval($postedYear);
                }else{
                    $yearFilter = date("Y");
                }
                $viewData['filteredYear'] = $yearFilter;
                if(isset($reportHeading) && !empty($reportHeading)){
                    $viewData['reportHeading'] = $reportHeading;
                }

                $selectData = array('GROUP_CONCAT(E.employee_id) AS EmployeeID, COUNT(E.employee_id) AS TotalEmployeesPaid',false);
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'salary S',
                        'condition' => 'S.employement_id = ET.employment_id AND S.trashed = 0 AND S.status = 2',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.trashed' => 0,
                    'E.enrolled' => 1
                );
                //If Main Filters Are Selected Except The Year Filter Then This Below If Statement Will Execute..
                if ((isset($employeeID) && $employeeID > 0) || (isset($ProgramID) && $ProgramID > 0) || (isset($projectID) && $projectID > 0) || (isset($departmentID) && $departmentID > 0)) {
                    if (isset($employeeID) && $employeeID > 0) {
                        $where['E.employee_id'] = $employeeID;
                        //If Any of the Above Three Main Conditions Are True Then.. Hu Haa Get The Employee Data..
                        $employeeTable = 'employee E';
                        $data = array('
                    E.employee_id AS EmployeeID,
                    E.full_name AS EmployeeName,
                    E.employee_code AS EmployeeCode,
                    MLDg.designation_name AS Designation,
                    MLD.department_name AS Department
                    ', false);
                        $empInfoJoins = array(
                            array(
                                'table' => 'employment ET',
                                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'position_management PM',
                                'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.status = 2 AND PM.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'posting P',
                                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.status = 2 AND P.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_designations MLDg',
                                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                                'type' => 'LEFT'
                            ),
                            array(
                                'table' => 'ml_department MLD',
                                'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                                'type' => 'LEFT'
                            )
                        );
                        $viewData['employeeInfo'] = $this->common_model->select_fields_where_like_join($employeeTable, $data, $empInfoJoins, $where, TRUE);

                    } elseif (isset($projectID) && $projectID > 0) {
                        //Do Some Stuff Here in Where if Project Is Selected
                        $where['MLP.project_id'] = $projectID;
                        $projectsData = ('MLP.project_title AS projectTitle, MLP.Abbreviation AS ProjectAbbreviation');
                        $projectJoins = array(
                            array(
                                'table' => 'employee_project EP',
                                'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0 AND EP.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_projects MLP',
                                'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_project_status_type MLST',
                                'condition' => 'MLST.pst_id = MLP.project_status_type_id AND MLST.trashed = 0',
                                'type' => 'LEFT'
                            )
                        );
                        $joins = array_merge($joins,$projectJoins);
                        $viewData['projectInfo'] = $this->common_model->select_fields_where_like_join($PTable,$projectsData,$joins,$where,TRUE);

                    }elseif (isset($ProgramID) && $ProgramID > 0) {
                        //Do Some Stuff Here in Where if Program Is Selected
                        $where['MLPL.ProgramID'] = $ProgramID;
                        $projectsData = ('MLPL.Program AS programName');
                        $projectJoins = array(
                            array(
                                'table' => 'employee_project EP',
                                'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0 AND EP.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_projects MLP',
                                'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_program_list MLPL',
                                'condition' => 'MLPL.ProgramID = MLP.ProgramID AND MLPL.trashed = 0',
                                'type' => 'INNER'
                            )

                        );
                        $joins = array_merge($joins, $projectJoins);
                        $viewData['programInfo'] = $this->common_model->select_fields_where_like_join($PTable, $projectsData, $joins, $where, TRUE);

                    }elseif (isset($departmentID) && $departmentID > 0) {
                        //Do Some Stuff Here if District Is Selected
                        //Update the Where Condition and Update The Joins For the Result of the Report...
                        $where['MLD.department_id'] = $departmentID;
                        $departmentSelectData = 'department_id AS DepartmentID, department_name AS DepartmentName';
                        $departmentJoins = array(
                            array(
                                'table' => 'posting P',
                                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.status = 2',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_department MLD',
                                'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                                'type' => 'INNER'
                            )
                        );
                        $joins = array_merge($joins,$departmentJoins);
                        $viewData['departmentInfo'] = $this->common_model->select_fields_where_like_join($PTable,$departmentSelectData,$joins,$where,TRUE);
                    }
                }
                if(strpos($selectedCheckBoxes,'Salaries') !== false){
                    $array = array(
                        array(
                            'table' => 'salary_payment SP',
                            'condition' => 'SP.salary_id = S.salary_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction ST',
                            'condition' => 'ST.transaction_id = SP.transaction_id AND ST.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLSM',
                            'condition' => 'MLSM.ml_month_id = SP.month AND MLSM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLSY',
                            'condition' => 'MLSY.ml_year_id = SP.year AND MLSY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $salariesJoins = array_merge($joins,$array);
                    $salariesSelectData = $selectData;
                    $salariesSelectData[0] .= ', SUM(SP.transaction_amount) AS Salaries,MLSM.month AS Month, MLSY.year AS `Year`';
                    $group_by = 'MLSM.Month';
                    $salariesWhere = $where;
                    $salariesWhere['MLSY.year'] = $yearFilter;
                    $salaries = $this->common_model->select_fields_where_like_join($PTable,$salariesSelectData,$salariesJoins,$salariesWhere,FALSE,'','',$group_by);
                    $salaries = json_decode(json_encode($salaries),true);
                }
                //Now If Allowances Are Checked Then Do the Joining For Allowances
                if(strpos($selectedCheckBoxes,'Allowances') !== false){
                    $array = array(
                       array(
                           'table' => 'allowance A',
                           'condition' => 'A.employment_id = ET.employment_id AND A.trashed = 0',
                           'type' => 'INNER'
                       ),
                        array(
                            'table' => 'allowance_payment AP',
                            'condition' => 'AP.allowance_id = A.allowance_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction AT',
                            'condition' => 'AT.transaction_id = AP.transaction_id AND AT.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLAM',
                            'condition' => 'MLAM.ml_month_id = AP.month AND MLAM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLAY',
                            'condition' => 'MLAY.ml_year_id = AP.year AND MLAY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $allowancesJoins = array_merge($joins,$array);
                    $allowancesSelectData = $selectData;
                    $allowancesSelectData[0] .= ', SUM(A.allowance_amount) AS Allowances, MLAM.month AS `Month`, MLAY.year AS `Year`';
                    $group_by = 'MLAM.Month';
                    $allowancesWhere = $where;
                    $allowancesWhere['MLAY.year'] = $yearFilter;
                    $allowances = $this->common_model->select_fields_where_like_join($PTable,$allowancesSelectData,$allowancesJoins,$allowancesWhere,FALSE,'','',$group_by);
                    $allowances = json_decode(json_encode($allowances),true);
                }
                //Now If Expenses Are Checked Then Do the Joining For Allowances
                if(strpos($selectedCheckBoxes,'Expenses') !== false){
                    $array = array(
                        array(
                            'table' => 'expense_claims EXC',
                            'condition' => 'EXC.employment_id = ET.employment_id AND EXC.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'expense_disbursment EXD',
                            'condition' => 'EXD.expense_id = EXC.expense_claim_id AND EXD.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction EXT',
                            'condition' => 'EXT.transaction_id = EXD.transaction_id AND EXT.trashed = 0 AND EXT.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLEM',
                            'condition' => 'MLEM.ml_month_id = EXD.month AND MLEM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLEY',
                            'condition' => 'MLEY.ml_year_id = EXD.year AND MLEY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $expensesJoins = array_merge($joins,$array);
                    $expensesSelectData = $selectData;
                    $expensesSelectData[0] .= ', SUM(EXT.transaction_amount) AS Expenses, MLEM.month AS `Month`, MLEY.year AS `Year`';
                    $group_by = 'MLEM.Month';
                    $expensesWhere = $where;
                    $expensesWhere['MLEY.year'] = $yearFilter;
                    $expenses = $this->common_model->select_fields_where_like_join($PTable,$expensesSelectData,$expensesJoins,$expensesWhere,FALSE,'','',$group_by);
                    $expenses = json_decode(json_encode($expenses),true);
                }
                //Now If Deductions Are Checked Then Do the Joining For Allowances
                if(strpos($selectedCheckBoxes,'Deductions') !== false){
                    $array = array(
                        array(
                            'table' => 'deduction D',
                            'condition' => 'D.employment_id = ET.employment_id AND D.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'deduction_processing DP',
                            'condition' => 'DP.deduction_id = D.deduction_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction DT',
                            'condition' => 'DT.transaction_id = DP.transaction_id AND DT.trashed = 0 AND DT.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLDM',
                            'condition' => 'MLDM.ml_month_id = DP.month AND MLDM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLDY',
                            'condition' => 'MLDY.ml_year_id = DP.year AND MLDY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $deductionsJoins = array_merge($joins,$array);
                    $deductionsSelectData = $selectData;
                    $deductionsSelectData[0] .= ', SUM(DT.transaction_amount) AS Deductions, MLDM.month AS `Month`, MLDY.year AS `Year`';
                    $group_by = 'MLDM.Month';
                    $expensesWhere = $where;
                    $expensesWhere['MLDY.year'] = $yearFilter;
                    $deductions = $this->common_model->select_fields_where_like_join($PTable,$deductionsSelectData,$deductionsJoins,$expensesWhere,FALSE,'','',$group_by);
                    $deductions = json_decode(json_encode($deductions),true);
                }
                //Now if Advances are checked, Than join for Advances
                if(strpos($selectedCheckBoxes,'Advances') !== false){
                    $array = array(
                        array(
                            'table' => 'loan_advances LA',
                            'condition' => 'LA.employment_id = ET.employment_id AND LA.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'loan_advance_payment LAP',
                            'condition' => 'LAP.loan_advance_id = LA.loan_advance_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction LAT',
                            'condition' => 'LAT.transaction_id = LAP.transaction_id AND LAT.trashed = 0 AND LAT.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLDM',
                            'condition' => 'MLDM.ml_month_id = LA.month AND MLDM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLDY',
                            'condition' => 'MLDY.ml_year_id = LA.year AND MLDY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $advancesJoins = array_merge($joins,$array);
                    $advancesSelectData = $selectData;
                    $advancesSelectData[0] .= ', SUM(LA.amount) AS Advances, MLDM.month AS `Month`, MLDY.year AS `Year`';
                    $group_by = 'MLDM.Month';
                    $advancesWhere = $where;
                    $advancesWhere['MLDY.year'] = $yearFilter;
                    $deductions = $this->common_model->select_fields_where_like_join($PTable,$advancesSelectData,$advancesJoins,$advancesWhere,FALSE,'','',$group_by);
                    $deductions = json_decode(json_encode($deductions),true);
                }

                //First Lets Get All The Months In The System
                $monthTable = 'ml_month';
                $data = 'ml_month_id AS MonthID, ml_month.month AS `Month`';
                $where = array(
                    'trashed' => 0
                );
                $allMonths = $this->common_model->select_fields_where($monthTable,$data,$where,FALSE);
                if(!isset($allMonths) || empty($allMonths)){
                    //redirect to error page or do someThing As all Months Do Not Exist..
                }

                $mergedArray = array();
                foreach($allMonths as $month){
                    //Salaries Are Checked Then This Below Foreach Will Execute..
                    if(isset($salaries) and !empty($salaries) and is_array($salaries)){
                        foreach($salaries as $monthSalaries){
                            if($month->Month === $monthSalaries['Month']){
                                if(array_key_exists($month->Month,$mergedArray)){
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month],$monthSalaries);
                                }else{
                                    $mergedArray[$month->Month] = $monthSalaries;
                                }

                            }
                        }
                    }//End of Salaries Foreach

                    if(isset($allowances) and !empty($allowances) and is_array($allowances)){
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach($allowances as $monthAllowances){
                            if($month->Month === $monthAllowances['Month']){
                                if(array_key_exists($month->Month,$mergedArray)){
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month],$monthAllowances);
                                }else{
                                    $mergedArray[$month->Month] = $monthAllowances;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if(isset($expenses) and !empty($expenses) and is_array($expenses)){
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach($expenses as $monthExpenses){
                            if($month->Month === $monthExpenses['Month']){
                                if(array_key_exists($month->Month,$mergedArray)){
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month],$monthExpenses);
                                }else{
                                    $mergedArray[$month->Month] = $monthExpenses;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if(isset($deductions) and !empty($deductions) and is_array($deductions)){
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach($deductions as $deduction){
                            if($month->Month === $deduction['Month']){
                                if(array_key_exists($month->Month,$mergedArray)){
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month],$deduction);
                                }else{
                                    $mergedArray[$month->Month] = $deduction;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if(isset($advances) and !empty($advances) and is_array($advances)){
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach($advances as $advance){
                            if($month->Month === $advance['Month']){
                                if(array_key_exists($month->Month,$mergedArray)){
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month],$advance);
                                }else{
                                    $mergedArray[$month->Month] = $advance;
                                }
                            }
                        }
                    }//End of Advances Foreach

                    if(!array_key_exists($month->Month,$mergedArray)){
                        $array = array();
                        if(isset($salaries) && !empty($salaries)){
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Salaries'] = '-';
                        }
                        if(isset($allowances) && !empty($allowances)){
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Allowances'] = '-';
                        }
                        if(isset($expenses) && !empty($expenses)){
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Expenses'] = '-';
                        }
                        if(isset($deductions) && !empty($deductions)){
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Deductions'] = '-';
                        }
                        $mergedArray[$month->Month] = $array;
                    }
                }

                //Time To Send Data To View Now..
                $viewData['ReportTableData'] = $mergedArray;
                $viewData['SelectedCheckBoxes'] = $selectedCheckBoxes;
//                return;
            }else{
                //redirect to the Un Authorized Page.
                return;
            }
            if(!isset($viewData) || empty($viewData)){
                $viewData = '';
            }
            $viewData['view'] = $this->load->view('payroll/pdf_reports/GeneratePrintReport',$viewData,true);
            $viewData['viewCsv'] = $this->load->view('payroll/generateReportForCSV',$viewData,true);
            //Load The Generate View..
            $this->load->view('payroll/generateReportForPrint',$viewData);

            return;
        }

        //Load The View For The Generation of Report..
        $this->load->view('includes/header');
        $this->load->view('payroll/generateReport');
        $this->load->view('includes/footer');
    }

    // Function for send email staff salary list report as a pdf
    function sendGeneratedSlaryStaffPDFReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/PayRollStaffSalaryReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/PayRollStaffSalaryReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'PayRollStaffSalaryReportEmail_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    // END
    function sendGeneratedPDFReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
               // echo "hi"; die;
               $mailTo = $this->input->post('ToEmail');
               $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/generatedPDFReports/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/generatedPDFReports/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
/*                redirect($filePath);
                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = $this->data['mailConfiguration'];
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    function downloadGeneratedPDFReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                  /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                 // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/reportTempFiles/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/reportTempFiles/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/reportTempFiles/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
               // echo "Download::".$filePath;
                return;

            }
        }
    }
    function downloadGeneratedCSVReport()
    {
       $csv_data=$this->input->post('viewCsv');
        // Code for CSV File
        if(!empty($csv_data)){
           $new= preg_replace('/[^a-zA-Z0-9_ -%][().][\/]/s', '', $csv_data);
            $new=addQuotes($new);
            $array=explode(",",$new);
            $file='SalaryCustomReport'.time().'.csv';
            $uploadPath ='systemGeneratedFiles/PayRollSalaryCustomCsvReports/'.$this->data['EmployeeID'].'/'.$file;
            $uploadDirectory = 'systemGeneratedFiles/PayRollSalaryCustomCsvReports/'.$this->data['EmployeeID'];
            //If Directories are Not Avaialable, Create The Directories On The Go..
            if(!is_dir($uploadDirectory)){
                mkdir($uploadDirectory, 0755, true);
            }

            $path= 'systemGeneratedFiles/PayRollSalaryCustomCsvReports/'.$this->data['EmployeeID'].'/';
            $delete_files= glob($path.'*');
            foreach($delete_files as $dFile){ // iterate files
                if(is_file($dFile))
                    unlink($dFile); // delete file
                //echo $file.'file deleted';
            }
            //$csv=addQuotes($new);
            //$explode=explode("<br>",$new);
            //print_r($explode); die;
            //$csv=preg_replace( "/\r|\n/", "", $csv);
            //echo $csv; die;
            write_file($uploadPath,$new, 'w+');
/*
            $fp = fopen('systemGeneratedFiles/PayRollSalaryCustomCsvReports/'.$this->data['EmployeeID'].'/SalaryCustomReport'.time().'.csv', 'w');
                 fputcsv($fp, $array);
                 fclose($fp);*/
          // $this->load->helper('download');
            /*flush();
            readfile($uploadPath);*/
          /*  exit;*/
            $this->load->helper('download');
            $data = file_get_contents($uploadPath);
             force_download($file, $data);
         //redirect($uploadPath);
            return;
        }
        //END
    }
    function loadAllAvailableEmployees()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                //Getting Employees Data From emloyee Table.
                $table = 'employee E';
                $data = ('E.full_name AS EmployeeName, E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.thumbnail AS EmployeeAvatar');
                $where = array(
                    'E.trashed' => 0,
                    'E.enrolled' => 1
                );
                $joins = array(
                  array(
                      'table' => 'employment ET',
                      'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                      'type' => 'INNER'
                  )
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'E.full_name';
                    $orLikes = array(
                        array(
                            'field' => 'E.employee_code',
                            'value' => $sValue
                        )
                    );
                    $group_by = '';
                    $employees = $this->common_model->select_fields_where_like__orLikes_join($table, $data, $joins,$where, FALSE, $field, $sValue, $orLikes, $group_by);
                } else {
                    $employees = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where, FALSE);
                }
                print_r(json_encode($employees));
                return;
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect to unauthorized Page.
        }
    }

    function loadAllAvailableProjects()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_projects MLP';
                $data = array('MLP.project_id AS ProjectID, CONCAT(MLP.project_title,"(",MLP.Abbreviation,")") AS ProjectTitle', false);
                $group_by = '';
                $where = array(
                    'MLP.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLP.project_title';
                    $orLikes = array(
                        array(
                            'field' => 'MLP.Abbreviation',
                            'value' => $sValue
                        )
                    );
                    $projects = $this->common_model->select_fields_where_like_orLikes($table, $data, $where, FALSE, $field, $sValue, $orLikes, $group_by);
                } else {
                    $projects = $this->common_model->select_fields_where($table, $data, $where, FALSE);
                }
                print_r(json_encode($projects));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    function loadAllAvailableDepartments()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_department MLD';
                $data = array('MLD.department_id AS DepartmentID, MLD.department_name AS Department', false);
                $group_by = '';
                $where = array(
                    'MLD.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLD.department_name';
                    $departments = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by);
                } else {
                    $departments = $this->common_model->select_fields_where($table, $data, $where, FALSE);
                }
                print_r(json_encode($departments));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    function loadAllAvailableYears()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_year MLY';
                $data = array('MLY.ml_year_id AS YearID, MLY.year AS YearTitle', false);
                $group_by = '';
                $order_by = array('MLY.year', 'DESC');
                $where = array(
                    'MLY.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLY.year';
                    $years = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by,$order_by);
                } else {
                    $years = $this->common_model->select_fields_where($table, $data, $where, FALSE, '', '', '', $group_by, $order_by);
                }
                print_r(json_encode($years));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    function loadAllAvailableMonths()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_month MLM';
                $data = array('MLM.ml_month_id AS MonthID, MLM.month AS MonthTitle', false);
                $group_by = '';
                $where = array(
                    'MLM.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLM.month';
                    $months = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by);
                } else {
                    $months = $this->common_model->select_fields_where($table, $data, $where, FALSE);
                }
                print_r(json_encode($months));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }
 /// Function for Program Filter Using Ajax  ///
    function load_all_available_Program()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_program_list';
                $data = array(
                    'Program AS ProgramName, ProgramID AS PROGRAMID',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'Program';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }
    //END

    // Function For Search Employee Name Using Ajax //
    public  function loadAvailableEmployees(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');

                $tbl = 'employee E';
                $data = ('E.full_name AS EmployeeName, E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.thumbnail AS EmployeeAvatar');
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where);
                }

                //Need To Replace The EmployeeAvatar Key Value (Image Name To Image Path)
                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }
    //END

 }

/// ********************************************* *** **** ****** *** **** *********************************************///
/// ============================================= Pay Roll Module End Here ============================================///
/// ********************************************* *** **** ****** *** **** ********************************************///

?>