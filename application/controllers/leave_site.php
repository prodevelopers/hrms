<?php
/**
 * Comments By Syed Haider Hassan.
 * Class leave_attendance
 * @property common_model $common_model
 * This Controller Class is For Master List
 */
class leave_site extends CI_Controller
   {
    
    function __construct()
	{
	parent::__construct();
    $this->load->model('site_model');
    $this->load->model('common_model');
   if($this->session->userdata('logged_in') != true)
		{
			redirect('login');
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////// Master list for Leave Attendance //////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	 // Function for Viewing leave Type // 
	
	
	public function view_leave_type($ml_leave_type_id = NULL)
	    {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('ml_leave_type',$where);
        $this->load->view('includes/header');
		$this->load->view('leave_type_ml/leave_master_lists',$data);
		$this->load->view('includes/footer');
		}
	
	// Function for Leave Type Addition // 
	
	public function add_leave_type($leave_id = NULL)
		{
			if($this->input->post('add')){

                $leavetype = $this->input->post('leave_type');
                if(!isset($leavetype) || empty($leavetype))
                {
                    $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_leave_type');
                    return;
                }


                //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
                $table = 'ml_leave_type';
                $data = array('COUNT(1) As TotalRecordsFound');
                $where= array(
                    'LOWER(leave_type)' => strtolower($leavetype),
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                    $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_leave_type');
                    return;
                }


                $add_type = array (
					'leave_type' =>  $leavetype
                );
				$query = $this->site_model->create_new_record('ml_leave_type',$add_type);
				if ($query)
				{
					redirect('leave_site/view_leave_type');
				}
			}
 }
	
		// Function for Edit ML leave type //
	
	public function edit_ml_leave_type($ml_leave_type_id = NULL)
	    {

            if(!isset($ml_leave_type_id) || empty($ml_leave_type_id) || !is_numeric($ml_leave_type_id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_site/view_leave_type');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_leave_type_id' => $ml_leave_type_id);
		$data['user'] = $this->site_model->get_row_by_id('ml_leave_type', $where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'leave_type' => $this->input->post('leave_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_leave_type', $where, $insert);
		if($query)
		{
		redirect('leave_site/view_leave_type/');
		}
	}
}

		$this->load->view('includes/header');
		$this->load->view('leave_type_ml/edit_ml_leave_type', $data);
		$this->load->view('includes/footer');	
}
      
	  // Function for Trash leave Type //

     public function trashed_leave_type($id = NULL,$tableName = NULL)
           {

               if($tableName === NULL || !is_string($tableName)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('leave_site/view_leave_type');
                   return;
               }

               if($id === NULL || !is_numeric($id)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('leave_site/view_leave_type');
                   return;
               }



               //Leave entitlement.
               $selectTable = 'leave_entitlement LE';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !=' => 1,
                   'ml_leave_type_id' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('leave_site/view_leave_type');
                   return;
               }


               //Leave Application.
               $selectTable = 'leave_application LA';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   //'trashed !=' => 1,
                   'ml_leave_type_id' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('leave_site/view_leave_type');
                   return;
               }


		  $this->db->trans_start();
		  $table_id= $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_leave_type', array('ml_leave_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_leave_type', array('ml_leave_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_id_name' => $table_id,
		  'table_name' 	=> 'ml_leave_type',
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('leave_site/view_leave_type');
		}
	
 }
	
	// Function for Viewing Approval Status //
	
	public function view_approval_status($id = NULL)
	    {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('ml_approval_types',$where);
        $this->load->view('includes/header');
		$this->load->view('leave_type_ml/approval_status',$data);
		$this->load->view('includes/footer');
	    }
	
	// Function for Approval Status Addition //
	
	public function add_aprroval($ml_approval_types_id = NULL)
	        {
		    if($this->input->post('add'))
			{

                $approvaltypes = $this->input->post('approval_types');
                if(!isset($approvaltypes) || empty($approvaltypes))
                {
                    $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_approval_status');
                    return;
                }


                //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
                $table = 'ml_approval_types';
                $data = array('COUNT(1) As TotalRecordsFound');
                $where= array(
                    'LOWER(approval_types)' => strtolower($approvaltypes),
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                    $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_approval_status');
                    return;
                }


			$add_type = array ( 
			'approval_types' =>  $approvaltypes
            );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('ml_approval_types',$add_type);
			if ($query) 
			{
               $msg ="Record inserted Successfully::success";
             $this->session->set_flashdata('msg',$msg);
			redirect('leave_site/view_approval_status');
			}
		}
			
  }
	
			// Function for Edit ML Approval Status //
	
	public function edit_ml_approve_status($id = NULL)
		    {
                if(!isset($id) || empty($id) || !is_numeric($id)){
                    $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_approval_status');
                    return;
                }
			if($this->uri->segment(3))
			{
			$data['data'] = $this->uri->segment(3);
			$where = array('ml_approval_types_id' => $id);
			$data['user'] = $this->site_model->get_row_by_id('ml_approval_types',$where);
			//echo "<pre>";print_r($data['user']); die;
			if($this->input->post('edit'))
			{
			$insert 		= array(
			'approval_types' => $this->input->post('approval_types'),
			'enabled' => $this->input->post('enabled')
			); 
			//echo "<pre>";print_r($insert); die;
			$query = $this->site_model->update_record('ml_approval_types',$where,$insert);
			if($query)
			{
			redirect('leave_site/view_approval_status/');
			}
			}
			}
			
			$this->load->view('includes/header');
			$this->load->view('leave_type_ml/edit_ml_approve_status', $data);
			$this->load->view('includes/footer');	
		}

     // Function for Trash Approval Status //

	public function trashed_approval_status($id = NULL, $tableName =NULL)
    {


        if($tableName === NULL || !is_string($tableName))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/view_approval_status');
            return;
        }

        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/view_approval_status');
            return;
        }


        //Approval Authorities
        $selectTable = 'ml_approval_authorities';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_approval_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/view_approval_status');
            return;
        }




        $this->db->trans_start();
		  $table_id= $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_approval_types', array('ml_approval_types_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_approval_types', array('ml_approval_types_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_id_name' => $table_id,
		  'table_name' 	=> 'ml_approval_types',
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{
                $msg = "Record Have Been Successfully Trashed From The System::success";
                $this->session->set_flashdata('msg',$msg);
				redirect('leave_site/view_approval_status');
			}
		
	}
	
	// Function for View Total Hour //
	
	public function view_total_hour()
	{
		
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('ml_total_month_hour',$where);
        $this->load->view('includes/header');
		$this->load->view('leave_type_ml/total_month_hour',$data);
		$this->load->view('includes/footer');
	    
		
	}
	
	
	// Function for Total Month Hour Addition //
	
	public function add_total_hour($monthly_std_hrs_id = NULL)
	        {
		    if($this->input->post('add'))
			{
                $monthstdhrs = $this->input->post('monthly_std_hrs');
                if(!isset($monthstdhrs) || empty($monthstdhrs) || !is_numeric($monthstdhrs))
                {
                    $msg ="Please Enter Value In Number.::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_total_hour');
                    return;
                }


                //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
                $table = 'ml_total_month_hour';
                $data = array('COUNT(1) As TotalRecordsFound');
                $where= array(
                    'LOWER(monthly_std_hrs)' => strtolower($monthstdhrs),
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                    $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_total_hour');
                    return;
                }


                $add_type = array (
			'monthly_std_hrs' =>  $monthstdhrs
                );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('ml_total_month_hour', $add_type);
			if ($query) 
			{
                $msg ="Record Successfully Inserted::success";
                $this->session->set_flashdata('msg',$msg);
			redirect('leave_site/view_total_hour');
			}
		}
			
  }
  			// Function for Edit ML Total Month Hour //
	
	public function edit_total_hour($id = NULL)
		    {
                if(!isset($id) || empty($id) || !is_numeric($id)){
                    $msg = "Something Went Wrong With The Link, If This Message Showed In Error than Please Contact System Administrator For Further Assistance::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_total_hour');
                    return;
                }
			if($this->uri->segment(3))
			{
			$data['data'] = $this->uri->segment(3);
			$where = array('monthly_std_hrs_id' => $id);
			$data['user'] = $this->site_model->get_row_by_id('ml_total_month_hour',$where);
			//echo "<pre>";print_r($data['user']); die;
			if($this->input->post('edit'))
			{
			$insert 		= array(
			'monthly_std_hrs' => $this->input->post('monthly_std_hrs'),
			'enabled' => $this->input->post('enabled')
			); 
			//echo "<pre>";print_r($insert); die;
			$query = $this->site_model->update_record('ml_total_month_hour',$where,$insert);
			if($query)
			{
			redirect('leave_site/view_total_hour/');
			}
			}
			}
			
			$this->load->view('includes/header');
			$this->load->view('leave_type_ml/edit_total_hour', $data);
			$this->load->view('includes/footer');	
		}
		    // Function for Trash Total Month Hour //

	  public function trashed_total_month($id = NULL, $tableName = NULL)
       {


           if($tableName === NULL || !is_string($tableName))
           {
               $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('leave_site/view_total_hour');
               return;
           }

           if($id === NULL || !is_numeric($id))
           {
               $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('leave_site/view_total_hour');
               return;
           }


           //Hourly Time Sheet
           $selectTable = 'hourly_time_sheet';
           $selectData = array('COUNT(1) AS TotalRecordsFound',false);
           $where = array(
               //'trashed !=' => 1,
               'monthly_std_hrs_id' => $id
           );
           $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
           if(isset($countResult) && $countResult->TotalRecordsFound > 0){
               $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('leave_site/view_total_hour');
               return;
           }


              $this->db->trans_start();
              $table_id= $tableName;

              $data['emer_contact'] = $this->site_model->get_row_by_id('ml_total_month_hour', array('monthly_std_hrs_id' => $id));
              if($data['emer_contact']->trashed == 0)
              {
                  $update_trash = array('trashed' => 1);
                  $query = $this->site_model->update_record('ml_total_month_hour', array('monthly_std_hrs_id' => $id), $update_trash);
              }

              $insert_trash = array(
              'record_id' 	=> $id,
              'table_name' 	=> 'ml_total_month_hour',
              'table_id_name' => $table_id,
              'trash_date' 	=> date('Y-m-d'),
              'trashed_by' 	=> $this->session->userdata('employee_id')
              );
              $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

              $this->db->trans_complete();

              if($query || $query_trash)
                {
                    $msg = "Record Have Been Successfully Trashed From The System::success";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_total_hour');
                }

	}
		// Function for View Holidays //
	
	public function view_ml_holiday()
	{
		
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('ml_holiday',$where);
        $this->load->view('includes/header');
		$this->load->view('leave_type_ml/holiday',$data);
		$this->load->view('includes/footer');
	    
		
	}
	
		// Function for Holiday Addition //
	
	public function add_holiday($ml_holiday_id = NULL)
	        {
		    if($this->input->post('add'))
			{
              $FromDate = $this->input->post('from_date');
              if(isset($FromDate) && !empty($FromDate))
              {
                  $FromDate = date('y-m-d', strtotime($FromDate));
              }
                $ToDate = $this->input->post('to_date');
                if(isset($ToDate) && !empty($ToDate))
                {
                    $ToDate = date('y-m-d', strtotime($ToDate));
                }

            $add_type = array (
			'holiday_type' =>  $this->input->post('holiday_type'),
			'from_date' => $FromDate,
			'to_date' => $ToDate
            );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('ml_holiday', $add_type);
			if ($query) 
			{
			redirect('leave_site/view_ml_holiday');
			}
		}
			
  }
  
  		// Function for Edit Holidays //
	
	public function edit_holiday($id = NULL)
		    {
                if(!isset($id) || empty($id) || !is_numeric($id)){
                    $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_ml_holiday');
                    return;
                }
			if($this->uri->segment(3))
			{
			$data['data'] = $this->uri->segment(3);
			$where = array('ml_holiday_id' => $id);
			$data['user'] = $this->site_model->get_row_by_id('ml_holiday',$where);
			//echo "<pre>";print_r($data['user']); die;
			if($this->input->post('edit'))
			{
              $FromeDate = $this->input->post('from_date');
                if(isset($FromeDate) && !empty($FromeDate))
                {
                   $FromeDate = date('Y-m-d',strtotime($FromeDate));
                }
                $Todate = $this->input->post('to_date');
                if(isset($Todate) && !empty($Todate))
                {
                   $Todate =  date('Y-m-d',strtotime($Todate));
                }

			$insert 		= array(
			'holiday_type' => $this->input->post('holiday_type'),
			'from_date' => $FromeDate,
			'to_date' => $Todate,
			'enabled' => $this->input->post('enabled')); 
			//echo "<pre>";print_r($insert); die;
			$query = $this->site_model->update_record('ml_holiday',$where,$insert);
			if($query)
			{
			redirect('leave_site/view_ml_holiday/');
			}
			}
			}
			
			$this->load->view('includes/header');
			$this->load->view('leave_type_ml/edit_holiday', $data);
			$this->load->view('includes/footer');	
		}
		
		   // Function for Trash Holidays //

	public function trashed_holiday($id = NULL)
    {
		  error_reporting(0);
		  $this->db->trans_start();
		  $table_id=$this->uri->segment(4);
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_holiday', array('ml_holiday_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_holiday', array('ml_holiday_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_id_name' => $table_id,
		  'table_name' 	=> 'ml_holiday',
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{  
	
				redirect('leave_site/view_approval_status');
			}
		
	}
	// Function For Work Week //
	
	public function view_work_week()
	{
		
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('work_week',$where);
        $this->load->view('includes/header');
		$this->load->view('leave_type_ml/work_week',$data);
		$this->load->view('includes/footer');
	    
		
	}
	
		// Function for Holiday Addition //
	
	public function add_work_week($work_week_id = NULL)
	        {
		    if($this->input->post('add'))
			{
                $workweek = $this->input->post('work_week');
                if(!isset($workweek) || empty($workweek))
                {
                    $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_work_week');
                    return;
                }


                //Little Fix For Add........
                $table = 'work_week';
                $data = array('COUNT(1) As TotalRecordsFound');
                $where= array(
                    'LOWER(work_week)' => strtolower($workweek),
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
                {
                    $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_work_week');
                    return;
                }

                $add_type = array (
			'work_week' =>  $workweek,
			'working_day' =>  $this->input->post('working_day')
                );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('work_week', $add_type);
			if ($query) 
			{
                $msg ="Record Successfully Added::success";
                $this->session->set_flashdata('msg',$msg);
			redirect('leave_site/view_work_week');
			}
		}
			
  }
  
      // Function for Edit Work Week //
	
	public function edit_work_week($id = NULL)
		    {
                if(!isset($id) || empty($id) || !is_numeric($id)){
                    $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/view_work_week');
                    return;
                }
			if($this->uri->segment(3))
			{
			$data['data'] = $this->uri->segment(3);
			$where = array('work_week_id' => $id);
			$data['user'] = $this->site_model->get_row_by_id('work_week',$where);
			//echo "<pre>";print_r($data['user']); die;
			if($this->input->post('edit'))
			{
			$insert 		= array(
			
			'working_day' => $this->input->post('working_day'));
			//echo "<pre>";print_r($insert); die;
			$query = $this->site_model->update_record('work_week',$where,$insert);
			if($query)
			{
			redirect('leave_site/view_work_week/');
			}
			}
			}

			$this->load->view('includes/header');
			$this->load->view('leave_type_ml/edit_work_week',$data);
			$this->load->view('includes/footer');	
		}
		
		
			   // Function for Trash Holidays //

	public function trashed_work_week($id = NULL)
    {

		  $this->db->trans_start();
		  $table_id=$this->uri->segment(4);
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id(' work_week', array('work_week_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record(' work_week', array('work_week_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_id_name' => $table_id,
		  'table_name' 	=> ' work_week',
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{  
	
				redirect('leave_site/view_approval_status');
			}
		
	}
	
//	Function for Configuration either it is Manual TimeSheet or Attendance Based TimeSheet //
/*	public function ConfigutrationTypes()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed
        );
        $data['Config'] = $this->site_model->get_all_by_where('ml_configrationtypes',$where);
        //echo "<pre>"; print_r($data['Config']); die;
        $this->load->view('includes/header');
        $this->load->view('leave_type_ml/ConfigTypes_master_lists',$data);
        $this->load->view('includes/footer');

    }*/
    //END

    // Function for Add Configuration either it is Manual TimeSheet or Attendance Based TimeSheet //
    public function AddConfigurationTypes()
    {
        if($this->input->post('add'))
        {
            $Config = $this->input->post('config');
            if(!isset($Config) || empty($Config)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_site/ConfigutrationTypes');
                return;
            }
            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_configrationtypes';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(ConfigTypes)' => strtolower($Config),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_site/ConfigutrationTypes');
                return;
            }

            $add_type = array(
                'ConfigTypes' => $Config
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_configrationtypes', $add_type);
            if ($query) {
                $msg ="Record Successfully Added::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_site/ConfigutrationTypes');
            }

        }

    }
    //END

    // Function for Update Configuration Types //
    public function EditConfigurationTypes($ConfigID = NULL)
    {
       if(!isset($ConfigID) || empty($ConfigID) || !is_numeric($ConfigID)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
            return;
        }

        if($this->uri->segment(3))
        {
            $data['data'] = $this->uri->segment(3);
            $where = array('ConfigID' => $ConfigID);
            $data['user'] = $this->site_model->get_row_by_id('ml_configrationtypes', $where);
            //echo "<pre>";print_r($data['user']); die;
            if($this->input->post('edit'))
            {
                $insert 		  = array(
                    'ConfigTypes' => $this->input->post('config'),
                    'enabled'     => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert);die;
                $query = $this->site_model->update_record('ml_configrationtypes', $where, $insert);
                if($query)
                {
                    $msg = "Record Successfully Updated::success";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('leave_site/ConfigutrationTypes/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('leave_type_ml/edit_ml_Configuration', $data);
        $this->load->view('includes/footer');
    }
    //END

    public function trashedConfiguration($id = NULL, $tableName = NULL)
    {
        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
            return;
        }



  /*      //Leave entitlement.
        $selectTable = 'leave_entitlement LE';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_leave_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
            return;
        }*/


     /*   //Leave Application.
        $selectTable = 'leave_application LA';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'ml_leave_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
            return;
        }*/


        $this->db->trans_start();
        $table_id= $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_configrationtypes', array('ConfigID' => $id));
        if($data['emer_contact']->trashed == 0)
        {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_configrationtypes', array('ConfigID' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' 	=> $id,
            'table_id_name' => $table_id,
            'table_name' 	=> 'ml_configrationtypes',
            'trash_date' 	=> date('Y-m-d'),
            'trashed_by' 	=> $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_site/ConfigutrationTypes');
        }

    }
	
 }
?>