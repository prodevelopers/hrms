<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cron_jobs extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
        $this->load->library('email',$this->data['mailConfiguration']);
    }

    function sendProbationAlertEmails(){
        //First Need To Get Only Active Employees.
        $employeesTable = 'employee E';
        $employeeSelectData = array('E.employee_id AS EmployeeID, E.full_name AS EmployeeName,C.contract_id As ContractID, GROUP_CONCAT(DISTINCT RH.employeed_id) AS LineManagerIDs',false);
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'contract C',
                'condition' => 'C.employment_id = ET.employment_id AND C.trashed = 0 AND C.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_alerts EA',
                'condition' => 'C.contract_id = EA.alert_id AND alert_text = "ProbationExpiry"',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'alerts_mailed AM',
                'condition' => 'C.contract_id = AM.alert_id AND alert_table_name = "ProbationExpiry"',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'reporting_heirarchy RH',
                'condition' => 'RH.for_employee = E.employee_id AND RH.trashed = 0',
                'type' => 'INNER'
            )
        );
        $where = 'E.enrolled = 1 AND E.trashed = 0 AND C.prob_alert_dat <= "'.date("Y-m-d").'" AND EA.alert_text IS NULL AND AM.alert_table_name IS NULL';
        $employees = $this->common_model->select_fields_where_like_join($employeesTable,$employeeSelectData,$joins,$where);

        if(!is_array($employees) || empty($employees) || empty($employees[0]->EmployeeID)){
            echo "No Probation Alerts To Mail";
            return;
        }


        if(isset($employees) && !empty($employees)){
            //As We Have Employees Whose line Managers/Supervisors Are Set, So We will Gonna Send Them Email..
foreach($employees as $key=>$val){
    //We Need To Get LineManagers Email ID's That We Need To Send Email..
    $PTable = 'employee E';
    $selectData = array('E.employee_id AS EmployeeID, CC.official_email AS EmployeeOfficialEmailAddress',false);
    $joins = array(
        array(
            'table' => 'current_contacts CC',
            'condition' => 'CC.employee_id = E.employee_id AND CC.trashed = 0',
            'type' => 'INNER'
        )
    );

    $where = 'E.trashed = 0 AND E.enrolled = 1 AND CC.official_email != "" AND E.employee_id IN ('.$val->LineManagerIDs.')';
    $lineManagerEmails = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where);
    if(isset($lineManagerEmails) && !empty($lineManagerEmails)){
        $lineManagerEmailsImploded = json_decode(json_encode($lineManagerEmails),true);
        $lineManagerEmailsImploded = '"'.implode('","',array_column($lineManagerEmailsImploded,'EmployeeOfficialEmailAddress')).'"';

        //Now Going To Send Emails To The Responsible Line Managers.
        //Need To do Little Configurations for SMTP..

        $this->email->set_newline("\r\n");
        $this->email->from('phrismapp@gmail.com', 'HRMS Admin'); // Change these details
        $this->email->to($lineManagerEmailsImploded);
        $this->email->subject('Probation Period Expiry');
        $this->email->message('Probation Period Is About To Expirt Or Has Been Expired For Employee :'.$val->EmployeeName);
        if($this->email->send()){
            //If Email Successfully Sent, Then Update it In Table To Not To Send It Back Again..
            $table = 'alerts_mailed';
            $insertData = array(
                'date_mailed' => date("Y-m-d H:i:s"),
                'alert_id' => $val->ContractID,
                'mailed' => 1,
                'alert_table_name' => 'ProbationExpiry'
            );
            $this->common_model->insert_record($table,$insertData);
        };
        echo "Sent Mail Alert to Responsible Line Managers For Employee: ".$val->EmployeeName;
    }
}//End Of Foreach Loop
        }
    }
}