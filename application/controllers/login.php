<?php

/**
 * @property common_model $common_model It resides all the methods which can be used in most of the controllers.
 */
class Login extends CI_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->helper('security');
        $this->load->model('common_model');
    }

    public function index()
    {

        $this->sign_in();
    }

    public function addAttendence_app()
    {
        $this->load->view('hrms_app/addAttendence_app');
    }

    public function registerUser_app()
    {
        $this->load->view('hrms_app/registerUser_app');
    }

    public function apps_apk()
    {
        $this->load->view('Apps/Parexons');
    }

    public function gettingTime()
    {
        $timestamp = time();
        date_default_timezone_set('Asia/Karachi');
        $response['time'] = date('Y-m-d H:i:s', $timestamp);
        $response['status'] = 1;
        echo json_encode($response);
        return;
    }


    public function sign_in($auth_mode = 'NO_COOKIES')
    {
        global $set_CookieUsername;
        global $set_CookiePassword;
        global $Username;
        global $Password;

        $sessionEmployeeID = $this->session->userdata('employee_id');
        if (strlen($sessionEmployeeID) > 0) {
            //Redirect User to its Desired Page, If He is Already Logged In.
            $table = 'user_account U';
            $selectData = ('MLML.module_controllers ModuleControllers');
            $joins = array(
                array(
                    'table' => 'employee E',
                    'condition' => 'U.employee_id = E.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_user_groups MLUG',
                    'condition' => 'MLUG.user_group_id = U.user_group AND MLUG.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'user_groups_privileges UGP',
                    'condition' => 'UGP.user_group_id = MLUG.user_group_id AND UGP.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_module_list MLML',
                    'condition' => 'MLML.ml_module_id = UGP.ml_module_list_id AND MLML.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'U.trashed' => 0,
                'E.employee_id' => $sessionEmployeeID,
                'UGP.view_only' => 1
            );
            $limit = 1;
            $order_by = array('MLML.priority', 'ASC');
            $result = $this->common_model->select_fields_where_like_join($table, $selectData, $joins, $where, TRUE, '', '', '', $order_by, $limit);
            if (isset($result) && !empty($result)) {
                switch ($result->ModuleControllers) {
                    case 'human_resource':
                        $redirectPath = 'human_resource/all_employee_list';
                        break;
                    case 'payroll':
                        $redirectPath = 'payroll/payroll_reg';
                        break;
                    case 'essp':
                        $redirectPath = 'essp/index';
                        break;
                    case 'leave_attendance':
                        $redirectPath = 'leave_attendance/view_leave_attandance';
                        break;
                    case 'dashboard_site':
                        $redirectPath = 'dashboard_site/view_dashboard';
                        break;
                    default:
                        $redirectPath = 'essp/index';
                }
                //Redirect to the Module Selected..
                redirect($redirectPath);
            } else {
                redirect('essp/index');
            }
            return;
        }
        if ($auth_mode === 'NO_COOKIES') {
            $this->form_validation->set_rules('user_name', 'user_name', 'trim|required|min_length[5]|max_length[50]|strtolower|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[50]');
            $this->form_validation->run();
            $set_CookieUsername = $this->input->post('user_name');
            $set_CookiePassword = $this->input->post('password');
            /*            $this->load->helper('security');
                        $this->load->helper('cookie');*/
            $Username = $this->input->post('user_name'); // already forced to lower case
            $Password = $this->input->post('password'); // this password is already encrypted and
        } else {
            // Use Cookies for re-authentication
            $Username = $this->encrypt->decode($this->input->cookie('Username', TRUE));
            $Password = $this->encrypt->decode($this->input->cookie('Password', TRUE));
            if (!isset($Username) || !isset($Password)) {
                return FALSE;
            }
        }
        if(!isset($Username) && !isset($Password) || (empty($Username) || empty($Password))) {
            $data ['msg'] = 'Invalid Username or Password -- ';
            $this->load->view('login', $data);
            return FALSE;
        }

        //Now We Need To Query To Check If User Details Match Up..
        $table = 'user_account U';
        $selectData = ('E.employee_id AS EmployeeID, U.user_name AS Username, U.user_account_id AS UserID, E.full_name AS EmployeeName, E.thumbnail AS EmployeeThumbnail, MLUG.user_group_id AS UserGroupID');
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = U.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_user_groups MLUG',
                'condition' => 'MLUG.user_group_id = U.user_group',
                'type' => 'INNER'
            )
        );
        $where = array(
            'LOWER(`U`.`employee_email`)' => $Username,
            'U.password' => $Password,
            'U.trashed' => 0
        );
        $result = $this->common_model->select_fields_where_like_join($table, $selectData, $joins, $where, TRUE);

        if (isset($result) && !empty($result)) {
            $employeeID = $result->EmployeeID;
            $userID = $result->UserID;
            $username = $result->Username;
            $employeeName = $result->EmployeeName;
            $thumbnail = $result->EmployeeThumbnail;
            $UserGroupID = $result->UserGroupID;
            if (empty($employeeID)) {
                $msg = "Some Problem Occurred, Please Try Back Logging Again::error";
                $this->session->set_flashdata('msg', $msg);
                return;
            }
            $uniqueSomeThing = $userID."-".strtotime(date("Y-m-d H:i:s"));
            $session = array(
                'UserID' => $userID,
                'user_name' => $username,
                'employee_id' => $employeeID,
                'full_name' => $employeeName,
                'thumbnail' => $thumbnail,
                'logged_in' => TRUE,
                'UTime' => $uniqueSomeThing
            );
            //Now Need To Setup The Session and Redirect The User To its own Belonging Module..
            if ($UserGroupID == 1) {
                $this->session->set_userdata($session);
//                $sessionID = $this->session->userdata('session_id'); Can Not Use this, As It is Continuously Changing..
                //Now Lets Log the Login Info For This Employee..

                //If There is AnyOther Log Active Then we will remove it from Active for This Specific Employee
                $logTable = 'users_login_logs';
                //Now Lets Do The Insertion, No Need To Destroy Old Active Records From Here. Its Other Controllers Job.
                $insertLogData = array(
                    'user_id' => $userID,
                    'employee_id' => $employeeID,
                    'loginDateTime' => date("Y-m-d H:i:s"),
                    'current' => 1,
                    'loginSuccessType' => 1,
                    'session_id' => $uniqueSomeThing
//                    'session_id' => $sessionID Cant Use This Session, As It Is Continuously Changing
                );
                $this->common_model->insert_record($logTable, $insertLogData);
                redirect('dashboard_site/view_dashboard');
                return;
            } else {
                //First Lets Get the Modules Assigned To The Group..
                $table = 'user_groups_privileges UGP';
                $selectData = ('MLML.module_controllers ModuleControllers');
                $joins = array(
                    array(
                        'table' => 'ml_module_list MLML',
                        'condition' => 'MLML.ml_module_id = UGP.ml_module_list_id AND MLML.trashed = 0',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'UGP.user_group_id' => $UserGroupID,
                    'UGP.view_only' => 1
                );
                $limit = 1;
                $order_by = array('MLML.priority', 'ASC');
                $result = $this->common_model->select_fields_where_like_join($table, $selectData, $joins, $where, TRUE, '', '', '', $order_by, $limit);
                $this->session->set_userdata($session);
                switch ($result->ModuleControllers) {
                    case 'human_resource':
                        $redirectPath = 'human_resource/all_employee_list';
                        break;
                    case 'payroll':
                        $redirectPath = 'payroll/payroll_reg';
                        break;
                    case 'essp':
                        $redirectPath = 'essp/index';
                        break;
                    case 'leave_attendance':
                        $redirectPath = 'leave_attendance/view_leave_attandance';
                        break;
                    case 'dashboard_site':
                        $redirectPath = 'dashboard_site/view_dashboard';
                        break;
                    default:
                        $redirectPath = 'essp/index';
                }
                //Redirect to the Module Selected..
                redirect($redirectPath);
                return;
            }
        } else {
            $data['msg'] = "Invalid User Name or Password ++";
        }
        $this->load->view('login', $data);
    }

    public function log_out()
    {
        $userID = $this->session->userdata('userID');
        $employeeID = $this->session->userdata('employee_id');
        $sessionID = $this->session->userdata('session_id');

        //Now we Need To Update the Logs for Logging Out of This Employee..
        $logTable = 'users_login_logs';
        $updateData = array(
            'current' => 0,
            'logoutDateTime' => date("Y-m-d H:i:s")
        );
        $whereData = array(
            'session_id' => $sessionID,
            'user_id' => $userID,
            'employee_id' => $employeeID
        );
//        $this->common_model->update($logTable, $whereData, $updateData);

        //Now Will Unset and will Destory The Session As We No Longer Need Session of The User.
        //unset($this->session->userdata);
        $this->session->sess_destroy();
        delete_cookie("parexons_hrms_cookie");
        redirect('login');
    }

    public function forget_password()
    {

        $this->load->helper('form');

        if ($this->input->post('Login')) {
            $email = $this->input->post('user_email');

            $where = array(
                'employee_email' => $email);
            $user = $this->site_model->get_row_by_id('user_account', $where);
            if(@$user->password > 0)
            {
            $password=$user->password;
            }
            else
            {$data['msg']="<span style='color: indianred; font-size: large'>No Record found please try correct Email  ! </span>";
                $this->load->view('login/login',$data);
                return;
            }
            //echo"<pre>"; print_r($user);die;
            //Need To do Little Configurations for SMTP..
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'phrismapp@gmail.com',
                'smtp_pass' => 'securePass786',
                'mailtype'  => 'html',
                'smtp_timeout' => '4',
                'charset'   => 'iso-8859-1'/*,
                'newline'=>"\r\n",
                'wordwrap' => TRUE*/
            );
            $this->load->library('email',$config);
            //$this->email->set_newline("\r\n");
            $this->email->from('phrismapp@gmail.com', 'HRMS Admin'); // Change these details
            $this->email->to($email);
            $this->email->subject('To Recover Password');
            $this->email->message('Your Password Is : ' . $password);
            if($this->email->send())
            {
                $data['msg'] = "Your Password Is Send To Your Email";

            }
            else{  //$data['msg'] = "Sorry try again ";
                show_error($this->email->print_debugger());
            }
            //echo $this->email->print_debugger();

            $this->load->view('login/login',$data);
            return;
        }


        $this->load->view('login/login');
    }


    /* End of Login Class */
}

?>