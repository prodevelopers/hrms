<?php
class Config_site extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('site_model');
			$this->load->library('datatables');
	
			if($this->session->userdata('logged_in') != TRUE)
			{
				redirect('login');
			}
		}
		
 // Function for Auto Complete approval type // 
 
      function autocomplete_approval_authority()
      {
      	$query = $this->site_model->get_autocomplete_approval_authority();
	 	// echo "<pre>"; print_r($query); die;
	  	$i=1;
		if(!empty($query)):
      	foreach($query as $row):
	  	?><li tabindex="<?php echo $i;?>" onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->designation_name;?>','<?php 							echo $row->employee_id;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
   		endforeach; 
		endif;   
}


	
public function notification($param = NULL)
	{
		if($param == "list")
		{
			echo $this->site_model->configration();
			die;
		}
		$data['approval_types'] 		= $this->input->post('approval_types');
		$data['approval'] 				= $this->site_model->dropdown_wd_option('ml_approval_types', '-- Select Authorise for --', 
		'ml_approval_types_id','approval_types');
		$data['designation_name'] 		= $this->input->post('designation_name');
		$data['Designations'] 			= $this->site_model->dropdown_wd_option('ml_designations', '-- Select Designations --', 
		'ml_designation_id','designation_name');
		//echo "<pre>"; print_r($data['approval'] );die;
		$this->load->view('includes/header');
		$this->load->view('configration/approval',$data);
		$this->load->view('includes/footer');
	}
	
public function delete_authrity($id = NULL)
{
	
		  error_reporting(0);
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_approval_authorities', array('approval_auth_id' => $id));
		 // print_r($data['emer_contact']);die;
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_approval_authorities', array('approval_auth_id' => $id), $update_trash);
		  }else{
			  $update_trash = array('trashed' => 0);
			  $query = $this->site_model->update_record('ml_approval_authorities', array('approval_auth_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_approval_authorities',
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  
		  if($query || $query_trash)
		  {
			  redirect('config_site/notification');
		  }	  
		
}
public function edit_authrity($employee_id = NULL)
	{
	 	if($this->uri->segment(3))
		{
			$data['employee_id'] = $this->uri->segment(3);
			$where = array('ml_approval_authorities.approval_auth_id' => $employee_id);
			$data['data'] = $this->site_model->edit_configration($where);
			//echo "<pre>";print_r($data['data']); die;
			if($this->input->post('edit'))
			{
				$insert 		= array(
				'ml_approval_type_id' => $this->input->post('approval_types'),
				'enabled' => $this->input->post('enabled')); 
				//echo "<pre>";print_r($insert); die;
				$query = $this->site_model->update_record('ml_approval_authorities', $where, $insert);
				if($query)
				{
				redirect('config_site/notification');
				}
				}
		}
		
				$data['ml_approval_type_id'] 		= $this->input->post('ml_approval_type_id');
				$data['approval'] 				= $this->site_model->dropdown_wd_option('ml_approval_types', '-- Select Authorise for --', 
		'ml_approval_types_id','approval_types');
		//$data['designation_name'] 			= $this->input->post('designation_name');
				$data['Designations'] 			= $this->site_model->dropdown_wd_option('ml_designations', '-- Select Designations --', 
		'ml_designation_id','designation_name');
				$this->load->view('includes/header');
				$this->load->view('configration/edit_authrity',$data);
				$this->load->view('includes/footer');	
	}	
	
public function add_authority()
{
		
     	if($this->input->post('add'))
		{
			$counter_type = 0;
			$approval_type = $this->input->post('approval_types');
			//echo "<pre>";print_r($approval_type); die;
			foreach($approval_type as $type)
			{
				$employee_id = $this->input->post('employee_id');
				$add_type= array(
				'employee_id' => $employee_id,
				'ml_approval_type_id' => $type,
				'enabled'=> $this->input->post('enabled'));
				$query = $this->site_model->create_new_record('ml_approval_authorities', $add_type);
				$counter_type++;
			}
			if($query)
			{
				redirect('config_site/notification');
			}
		}
				$data['ml_approval_type_id'] 	= $this->input->post('ml_approval_type_id');
				$data['approval'] = $this->site_model->dropdown_wd_option('ml_approval_types', '-- Select Authorise for --', 
				'ml_approval_types_id','approval_types');
				$this->load->view('includes/header');
				$this->load->view('configration/add_authority',$data);
				$this->load->view('includes/footer');	
}		

}
?>