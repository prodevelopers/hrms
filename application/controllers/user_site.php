<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class dashboard_site
 * @property common_model $common_model It resides all the methods which can be used in most of the controllers.
 */
class User_site extends MY_Controller{

		function __construct()
		{
		parent::__construct();
		$this->load->model('site_model');
		$this->load->library('datatables');

            //Restrict UnAuthorized Users..
            $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
            $moduleController = $this->router->fetch_class();
            if(!is_module_Allowed($loggedInEmployeeIDAccessCheck,$moduleController,'view') && !is_admin($loggedInEmployeeIDAccessCheck)){
                $msg = "You Do Not Have Access To This Certain Section::error";
                $this->session->set_flashdata('msg',$msg);
                redirect(previousURL());
                exit;
            }
	
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////		


//////////////////////////////// UPDATE FUNCTION FOR GROUP PRIVILIGIES ///////////////////////////////////

public function update_priviligies($privilege_id = NULL)
{
	
	if($this->uri->segment(3))
		{
			
			$data['privilege_id'] = $this->uri->segment(3);
			$where = "user_privilege_id =" .$this->uri->segment(3);
			$data['join'] = $this->site_model->get_row_by_id('privileges',$where);
			//echo "<pre>";print_r($data); die;
			//echo $view=$this->input->post('modify'); die;
			if($this->input->post('edit'))
			{
				$insert = array(
				'view_only' 			=> 		$this->input->post('view_only'),
				'modify' 				=> 		$this->input->post('modify'),
				'create_new_record' 	=> 		$this->input->post('create_new_record'),
				'print' 				=> 		$this->input->post('print'));
				//echo "<pre>";print_r($insert); die;
				$query = $this->site_model->update_record('privileges',$where,$insert);
				if($query)
				{
					redirect('user_site/group_edit_priviligies/');
				}
			}
		}
		
		$this->load->view('includes/header');
		$this->load->view('user_management/edit_pri', $data);
		$this->load->view('includes/footer');	
}
////////////////////////////////END OF FUNCTION ////////////////////////////////////////////////
    function user_list($list = NULL)
    {
        if($list === 'list'){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'employee E';
            $selectData = array('
             E.employee_id,
             E.employee_code,
             E.full_name,
             E.father_name,
             C.mob_num,
             PM.job_specifications AS Position
            ',false);
            $joins = array(
                array(
                    'table' => 'employment EMPL',
                    'condition' => 'EMPL.employee_id = E.employee_id AND EMPL.current = 1',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'user_account U',
                    'condition' => 'U.employee_id = E.employee_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'current_contacts C',
                    'condition' => 'C.employee_id = E.employee_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id = EMPL.employment_id',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.trashed' => 0,
                'U.user_account_id'=> NULL
            );

            $add_column = array(
                'ViewEditActionButtons' => '<a style="cursor:pointer;cursor:hand;" class="AddUserAccount" id="UserAccounttModal"><span class="fa fa-plus-square"></span></a>'
            );
            //Need To Check For Filters..
//            print_r($this->input->post());
            $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$add_column);

            if(isset($listResult) && !empty($listResult)){
                $jsonDecoded = json_decode($listResult,true);
//                var_dump($jsonDecoded)
                echo json_encode($jsonDecoded);
                return;
            }
            echo $listResult;
            return;
        }
    }
////////////////// Fucntion to Add Employee Full Name And User Account Information ///////////////////////////

	public function add_employee_user_account($param = NULL)
	{
        $where2 = array('trashed' => 0);
		$data['user_group'] = $this->hr_model->dropdown_wd_option('ml_user_groups', '-- Select Group --', 'user_group_id', 'user_group_title',$where2);



		if ($this->input->post('continue')) {
            $checked = $this->input->post('login');

            $id = $this->input->post('emp_id');
            $where = array('employee_id' => $id);
            //$last_id = $this->hr_model->get_last_id();
            $data['rec'] = $this->site_model->get_row_by_where('user_account', $where);
            if (isset($data['rec']) && $data['rec']->employee_id != NULL && $data['rec']->trashed == 0) {
                $msg= "User login Detail Already Exist..!::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('user_site/add_employee_user_account');
                return;

            } else {
                $now = date('Y-m-d');
                $user_account_data = array(
                    'employee_id' => $id,
                    //'user_name'     		=> $this->input->post('user_name'),
                    'employee_email' => $this->input->post('employee_email'),
                    'password' => $this->input->post('password'),
                    'acct_creation_date' => $now,
                    'enabled' => $this->input->post('status'),
                    'user_group' => $this->input->post('user_group')
                );
                //echo"<pre>";print_r($user_account_data);die;
                $user_account_query = $this->hr_model->create_new_record('user_account', $user_account_data);

                $last_id = $this->input->post('emp_id');



                if ($user_account_query) {
                    redirect('user_site/add_employee_user_account');
                }
            }
        }

		$this->load->view('includes/header');
		$this->load->view('user_management/add_employees', $data);
		$this->load->view('includes/footer');
	}

//////////////////////////// END OF Function ////////////////////////////////////////////////

/////////////////////////// FUNCTION FOR EDIT GROUP PRIVILLIGIES ///////////////////////////////

public function group_edit_priviligies()
{
	
	$data{'join'} = $this->site_model->view_group_priviligies();
	$this->load->view('includes/header');
	$this->load->view('user_management/group_edit', $data);
	$this->load->view('includes/footer');
}
/////////////////////////////END OF FUNCTION ///////////////////////////////////////////////////


/////////////////////////////FUNCTION OF VIEW GROUP PRIVILIGIES //////////////////////////////////
public function group_priviligies()
{
	$data['user_group_title']		= 		$this->input->post('user_group_title');
	$data['user_group_title'] 		=	 	$this->site_model->drop_down();
	$data['employee_code']			= 		$this->input->post('employee_code');
	//echo $data['group'];
	$data['employee_code']			= 		$this->site_model->drop_down2();
	$data['join']					= 		$this->site_model->view_group_priviligies();
	//echo "<pre>"; print_r($data{'join'}); die;
	$this->load->view('includes/header');
	$this->load->view('user_management/group_priviligies',$data);
	$this->load->view('includes/footer');
}
//////////////////////////////END OF FUNCTION ////////////////////////////////////////////////////

		
///////////////////////////// ADD FUNCTION FOR USER GROUPS ///////////////////////////////////////
public function user_group_add_pri($selectors = NULL)
{
    if($this->input->is_ajax_request() && $selectors !== NULL){
        if($selectors == 'selectingGroup'){
            $table = 'ml_user_groups MLUG';
            $data = 'MLUG.user_group_id AS UserGroupID, MLUG.user_group_title AS UserGroupName';
            $where = array(
                'MLUG.trashed' => 0
            );
            $result = $this->common_model->select_fields_where($table,$data,$where);
            print_r(json_encode($result));
            return;
        }elseif($selectors == 'selectingModule'){
            $moduleTable = 'ml_module_list MLML';
            $moduleData = array('MLML.module_name AS ModuleName, MLML.ml_module_id AS ModuleID', false);
            $moduleWhere = array(
                'MLML.trashed' => 0
            );
            $result = $this->common_model->select_fields_where($moduleTable,$moduleData,$moduleWhere);
            print_r(json_encode($result));
            return;
        }elseif($selectors == 'loadCheckBoxes'){
            //Get All the CheckBoxes Values.
            $groupID = $this->input->post('Group');
            $moduleID = $this->input->post('Module');
            if(!isset($groupID) && !isset($moduleID)){
                return;
            }
            $table = 'user_groups_privileges UGP';
            $data = ('UGP.user_group_privilege_id AS UserGroupPrivilegeID, UGP.view_only AS PView, UGP.modify AS PModify, UGP.create_new_record AS PCreate, UGP.print AS PPrint');
            $where = array(
                'UGP.user_group_id' => intval($groupID),
                'UGP.ml_module_list_id' => intval($moduleID)
            );
//            $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);
            $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($result) && !empty($result)){
                print_r(json_encode($result));
                return;
            }else{
                $msg = "FAIL::No Record Found::error";
                $array =array(
                    'msg' => $msg
                );
                print_r(json_encode($array));
                return;
            }

        }elseif($selectors == "updatePrivilege"){
            //This Section Should Insert Or Update the User Group Privilege.
            $groupID = $this->input->post('Group');
            $moduleID = $this->input->post('Module');

            ///Getting The Data of CheckBoxes in Post
            $view = $this->input->post('view');
            $modify = $this->input->post('modify');
            $print = $this->input->post('print');
            $create = $this->input->post('create');
            //End Of CheckBoxes Data.

            //First We Need To Check If the User Group Already Exist in Database or Not.
            $table = 'user_groups_privileges';
            $selectData = array('user_group_privilege_id AS GroupPrivilegeID',false);
            $where = array(
                'user_group_id' => $groupID,
                'ml_module_list_id' => $moduleID
            );
            $result = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($result) && !empty($result)){
                //If Data is Present in $result then we will update the The Record
                $GroupPrivilegeID = $result->GroupPrivilegeID;
//                $PrivilegesID = $result->PrivilegesID;
                if(isset($GroupPrivilegeID) && $GroupPrivilegeID > 0){
                    //We Will Update the Privilege

                    $data = array(
                    );
                    if(isset($view) && is_numeric($view)){
                        $data['view_only'] = $view;
                    }
                    if(isset($modify) && is_numeric($modify)){
                        $data['modify'] = $modify;
                    }
                    if(isset($create) && is_numeric($create)){
                        $data['create_new_record'] = $create;
                    }
                    if(isset($print) && is_numeric($print)){
                        $data['print'] = $print;
                    }
                    $updateTable = 'user_groups_privileges';
                    $where = array(
                        'user_group_id' => $groupID,
                        'ml_module_list_id' => $moduleID
                    );
                    $result = $this->common_model->update($updateTable,$where,$data);
                    if($result == true){
                        echo "OK::Record Successfully Update::success";
                        return;
                    }
                }
            }else{
                //As No Record Is Present For the groupID and moduleID Then We Will Create New Record.
                $insertTable = 'user_groups_privileges';
                $insertData = array(
                    'user_group_id' => $groupID,
                    'ml_module_list_id' => $moduleID
                );
                if(isset($view) && is_numeric($view)){
                    $insertData['view_only'] = $view;
                }
                if(isset($modify) && is_numeric($modify)){
                    $insertData['modify'] = $modify;
                }
                if(isset($create) && is_numeric($create)){
                    $insertData['create_new_record'] = $create;
                }
                if(isset($print) && is_numeric($print)){
                    $insertData['print'] = $print;
                }
                $result = $this->common_model->insert_record($insertTable,$insertData);
                if($result > 0){
                    echo "OK::Record Successfully Added::success";
                    return;
                }
            }
            return;
        }
    }
		if($this->input->post('add'))
		{
			$add_type= array(
			'group_id' 			=> 		$this->input->post('group_id'),
			'view_only' 			=> 		$this->input->post('view_only'),
			'modify' 				=> 		$this->input->post('modify'),
			'create_new_record' 	=> 		$this->input->post('create_new_record'),
			'print' 				=> 		$this->input->post('print'));

			$query = $this->site_model->create_new_record('privileges', $add_type);
			$last_id = $this->site_model->get_last_id();
			$add_user_pri= array(
				'user_group_id' 			=> 		$this->input->post('group_id'),
				'ml_module_list_id' 			=> 		$this->input->post('module_id'),
				'privilege_id' 			=> 		$last_id,

				);
			//var_dump($add_user_pri);die;
			$query2 = $this->site_model->create_new_record('user_groups_privileges', $add_user_pri);

			if($query && $query2)
			{
				redirect('user_site/manage_group/');
			}
		}
	$data['user_group_title'] 		=	 	$this->site_model->drop_down();
	$data['module'] 		=	 	$this->site_model->drop_down_module();
	$this->load->view('includes/header');
	$this->load->view('user_management/user_group_edit', $data);
	$this->load->view('includes/footer');
	//}

}
///////////////////////////////END OF FUNCTION /////////////////////////////////////////////

/////////////////////////VIEW FUNCTION FOR USER GROUPS /////////////////////////////////////
public function manage_group()
{
	$data{'data'} 				= 		$this->site_model->get_all('ml_user_groups');
	$data['group']				= 		$this->input->post('user_group_title');
	//echo $data['group'];
	$data['user_group_title'] 	= 		$this->site_model->drop_down();
	$data['employee_code']		= 		$this->input->post('employee_code');
	//echo $data['group'];
	$data['employee_code'] 		= 		$this->site_model->drop_down2();
	//$where 					= 		"ml_user_groups.user_group_id = ". $user_group_id;
	$data{'join'} 				= 		$this->site_model->view_record();
	//echo "<pre>";print_r($data{'join'});
	$this->load->view('includes/header');
	$this->load->view('user_management/manage_group', $data);
	$this->load->view('includes/footer');	
}
////////////////////////////////END OF FUNCTION /////////////////////////////////////////////

///////////////////// VIEW FUNCTION FOR JOIN SHOW USER MANAGEMENT ////////////////////////////
public function view_user_management($param = NULL)
{
$this->load->library('datatables');
			if($param == 'list')
			{
  //This Section is For Getting the Data For DataTables.
                $empTable = 'employee E';
                $data = array('
                E.employee_code,
                E.full_name,
                U.user_name,
                UG.user_group_title,
                user_account.enabled,user_account.user_account_id',false);
				echo $this->site_model->view_user_infromation();
				return;
			}
	$data['uname']			  = 	$this->input->post('user_name');
	//echo $data['uname'];
	$data['user_name']		  = 	$this->site_model->drop_down4();
	$data['group']			  = 	$this->input->post('user_group_title');
	//echo $data['group'];
	$data['user_group_title'] = 	$this->site_model->drop_down();
	$this->load->view('includes/header');
	$this->load->view('user_management/users_management',$data);
	$this->load->view('includes/footer');
}

/////////////////////////////END //////////////////////////////////////////////////////////////
//////////////////////////////EDIT FUNCTION FOR USER ACCOUNT ON JOIN FUNCTION//////////////////

public function edit_user_management($employee_id = NULL)
{
	if($employee_id !== NULL && is_numeric($employee_id) && $employee_id > 0)
		{
		$data['employee_id'] 		= 		$this->uri->segment(3);
		$where 						= 		"user_account.user_account_id = ". $employee_id;
		$data['data']		 		= 		$this->site_model->user_infromation($where);
		//echo"<pre>"; print_r($data['data']);die;
		if($this->input->post('edit'))
			{
                $password = $this->input->post('password');
				$insert				= 		array(
				'user_name' 		=> 		$this->input->post('user_name'),
				'user_group' 		=> 		$this->input->post('user_group_title'),
				'enabled' 			=> 		$this->input->post('enabled'));	
				//echo "<pre>";print_r($insert); die;

                if(isset($password) && !empty($password)){
                    $insert['password'] = $password;
                }

				$query 				= 		$this->site_model->update_record('user_account',$where,$insert);
				if($query)
				{
					redirect('user_site/view_user_management/');
				}
			}
				//$data['group']		= 		$this->input->post('user_group_title');
				//echo $data['group'];
				$data['user_group_title'] = $this->site_model->drop_down();
				$this->load->view('includes/header');
				$this->load->view('user_management/edit_user_management',$data);
				$this->load->view('includes/footer');
		}

}
//////////////////////////////////////END OF FUNCTION ///////////////////////////////////////////////

///Manage Groups Drop Downs..//////
//Load Available Modules
function loadAvailableModules(){
    if($this->input->is_ajax_request()){
        $table = 'ml_module_list MLML';
        $data = ('MLML.ml_module_id AS ModuleID, MLML.module_name AS ModuleName');
        $where = array(
          'MLML.trashed' => 0
        );
        if($this->input->post('term')){
            $field = 'MLML.module_name';
            $value = $this->input->post('term');
            $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$value);
        }else{
            $result = $this->common_model->select_fields_where($table,$data,$where);
        }
        print_r(json_encode($result));
    }
}

///Load Available Groups
function loadAllAvailableGroups(){
    if($this->input->is_ajax_request()){
        $table = 'ml_user_groups MLUG';
        $data = ('MLUG.user_group_id AS UserGroupID, MLUG.user_group_title AS UserGroupName');
        if($this->input->post('dependent')){
            $dependent = $this->input->post('dependent');
            $dependentText = $dependent['text'];
            $dependentID = $dependent['id'];
            $joins = array(
                array(
                    'table' => 'user_groups_privileges UGP',
                    'condition' => 'UGP.user_group_id = MLUG.user_group_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_module_list MLML',
                    'condition' => 'MLML.ml_module_id = UGP.ml_module_list_id',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'MLUG.trashed' => 0,
                'MLML.ml_module_id' => $dependentID
            );
            $group_by = '';
            if($this->input->post('term')){
                $field = 'MLUG.user_group_title';
                $value = $this->input->post('term');
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,$field,$value,$group_by);
            }else{
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }
            print_r(json_encode($result));
            return;
        }
        $where = array(
          'MLUG.trashed' => 0
        );
        if($this->input->post('term')){
            $field = 'MLUG.user_group_title';
            $value = $this->input->post('term');
            $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$value);
        }else{
            $result = $this->common_model->select_fields_where($table,$data,$where);
        }
        print_r(json_encode($result));
    }
}
    public function listUsers_DT(){
        if($this->input->is_ajax_request()){
            $userGroupID = $this->input->post('userGroup');
            $appModuleID = $this->input->post('appModule');
            $table = "employee E";
            $data = array('E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.full_name AS EmployeeName, U.user_name AS EmployeeUserName, MLUG.user_group_title AS EmployeeUserGroup',false);
            $joins = array(
                array(
                    'table' => 'user_account U',
                    'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_user_groups MLUG',
                    'condition' => 'MLUG.user_group_id = U.user_group AND MLUG.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'user_groups_privileges UGP',
                    'condition' => 'UGP.user_group_id = MLUG.user_group_id AND UGP.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_module_list MLML',
                    'condition' => 'MLML.ml_module_id = UGP.ml_module_list_id AND MLML.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.trashed' => 0,
                'E.enrolled' => 1
            );
            if(isset($userGroupID) && !empty($userGroupID)){
                $where['MLUG.user_group_id'] = $userGroupID;
            }
            if(isset($appModuleID) && !empty($appModuleID)){
                $where['MLML.ml_module_id'] = $appModuleID;
            }
            $group_by = 'E.employee_id';
            $result = $this->common_model->select_fields_joined_DT($data,$table,$joins,$where,'','',$group_by);
            print_r($result);
        }
    }
    public function user_logs($viewValue = NULL)
    {
        //We Need To Get the Logs For Admin of User Logins..
        if($viewValue === 'login_logs_DT'){
            if($this->input->is_ajax_request()){
                if(is_admin($this->data['EmployeeID'])){
                    $selectedDate = $this->input->post('selectedDate');
                    $selectedMonths = $this->input->post('selectedMonth');
                    $selectedYear = $this->input->post('selectedYear');

                    if(isset($selectedMonths) && !empty($selectedMonths) && $selectedMonths !== false && $selectedMonths !== '[]'){
                        $selectedMonths = json_decode($selectedMonths,true);
                        $selectedMonths = '"'.implode('","',array_column($selectedMonths,'id')).'"';
                    }

                    //As the LoggedIn User is Admin So We Will Load DataTables For This Employee/User..
                    $table = 'users_login_logs ULL';
                    $data = array('DATE_FORMAT(ULL.loginDateTime,"%d/%b/%Y (%h:%i %p)") AS LoginDate, DATE_FORMAT(ULL.logoutDateTime,"%d/%b/%Y (%h:%i %p)") AS LogoutDate,E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.full_name AS EmployeeName',false);
                    $joins = array(
                        array(
                        'table' => 'employee E',
                        'condition' => 'E.employee_id = ULL.employee_id',
                        'type' => 'INNER'
                        )
                    );
                    $where = 'E.trashed = 0';
                    if(isset($selectedDate) && !empty($selectedDate)){
                        $where .= ' AND DATE_FORMAT(ULL.loginDateTime, "%d") = '.$selectedDate;
                    }
                    if(isset($selectedYear) && !empty($selectedYear)){
                        $where .= ' AND DATE_FORMAT(ULL.loginDateTime, "%Y") = '.$selectedYear;
                    }
                    if(isset($selectedMonths) && !empty($selectedMonths) && $selectedMonths !== false && $selectedMonths !== '[]'){
                            $where .= ' AND DATE_FORMAT(ULL.loginDateTime, "%M") IN ('.$selectedMonths.')';
                    }
                    $result = $this->common_model->select_fields_joined_DT($data,$table,$joins,$where);
                    print_r($result);
                }else{
                    echo "FAIL::Only Admins are Allowed To View LoginLogs::error";
                }
            }
            return;
        }
//Load The View..
        $this->load->view('includes/header');
        $this->load->view('user_management/login_logs');
        $this->load->view('includes/footer');
    }

    //Selecting Available Years in UsersLoginLogs
    function selectAvailableLoggedInYear(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If User Has Come Through the Proper Way it is Built From, Then We Will Execute Query to Get Data From Database.
                $table = 'users_login_logs ULL';
                $data = array('DATE_FORMAT(ULL.loginDateTime,"%Y") AS Year',false);
                $group_by = 'Year';
                $result = $this->common_model->select_fields($table,$data,FALSE,$group_by);
                print_r(json_encode($result));
            }else{
                echo "FAIL::You are Not Authorized To Access This Page, If you Think you are Receiving this Error wrongly please contact System Administrator::error";
            }
        }else{
            //Perform Some Function if User has Not Accessed The function In Proper Way.
        }
    }

    //Selecting Available Months in UsersLoginLogs
    function selectAvailableLoggedInMonths(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If User Has Come Through the Proper Way it is Built From, Then We Will Execute Query to Get Data From Database.
                $table = 'users_login_logs ULL';
                $data = array('DATE_FORMAT(ULL.loginDateTime,"%M") AS Months',false);
                $group_by = 'Months';
                $result = $this->common_model->select_fields($table,$data,FALSE,$group_by);
                print_r(json_encode($result));
            }else{
                echo "FAIL::You are Not Authorized To Access This Page, If you Think you are Receiving this Error wrongly please contact System Administrator::error";
            }
        }else{
            //Perform Some Function if User has Not Accessed The function In Proper Way.
        }
    }

    //Selecting Available Months in UsersLoginLogs
    function selectAvailableLoggedInDates(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If User Has Come Through the Proper Way it is Built From, Then We Will Execute Query to Get Data From Database.
                $table = 'users_login_logs ULL';
                $data = array('DATE_FORMAT(ULL.loginDateTime,"%d") AS Dates',false);
                $group_by = 'Dates';
                $result = $this->common_model->select_fields($table,$data,FALSE,$group_by);
                print_r(json_encode($result));
            }else{
                echo "FAIL::You are Not Authorized To Access This Page, If you Think you are Receiving this Error wrongly please contact System Administrator::error";
            }
        }else{
            //Perform Some Function if User has Not Accessed The function In Proper Way.
        }
    }
    public  function loadAvailableEmployees(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');

                $tbl = 'employee E';
                $data = ('E.full_name AS EmployeeName, E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.thumbnail AS EmployeeAvatar');
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where);
                }

                //Need To Replace The EmployeeAvatar Key Value (Image Name To Image Path)
                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }
    public  function employeeDesignation(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');

                $tbl = 'employee E';
                $data = ('E.full_name AS EmployeeName, E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.thumbnail AS EmployeeAvatar');
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where);
                }

                //Need To Replace The EmployeeAvatar Key Value (Image Name To Image Path)
                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }


}

?>