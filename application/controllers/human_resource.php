<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Human_Resource extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hr_model');
        $this->load->library('datatables');
        $this->load->helper('hr_helper');

        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('login');
        }

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if (!is_module_Allowed($loggedInEmployeeIDAccessCheck, $moduleController, 'view') && !is_admin($loggedInEmployeeIDAccessCheck)) {
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg', $msg);
            redirect(previousURL());
            exit;
        }
    }

    public function index()
    {
        $this->load->helper('url');
        $this->load->library('image_lib');
    }

    public function db_config_app()
    {
        $this->load->view('hrms_app/db_config_app');
    }

    public function db_connect_app()
    {
        $this->load->view('hrms_app/db_connect_app');
    }

    public function addAttendence_app()
    {
        $this->load->view('hrms_app/addAttendence_app');
    }

    public function registerUser_app()
    {
        $this->load->view('hrms_app/registerUser_app');
    }

    /// Function to Add Employee And User Account Information
    public function add_employee_acct()
    {
        if ($this->input->post('continue')) {
            $code = $this->input->post('employee_code');
            $where_code = array('employee.employee_code' => $code);
            $data['code'] = $this->hr_model->get_row_by_id('employee', $where_code);

            //If Function will Executes only if records already exist for the employee Code Given.
            if (isset($data['code']) && $data['code']->employee_code == $code) {
                $msg = "Employee Code already Exists Try Another One..!";
                $this->session->set_flashdata('msg', $msg);
                redirect('human_resource/add_employee_acct');
                return;
            } else {
                //If Employee Code do not already Exist in DB then This Else Portion Executes.
                $checked = $this->input->post('login');
                //If the Check is Selected to create User Login Details then This If Section Executes.
                if ($checked == "login-details") {
                    $cnic = $this->input->post('nic1') . "-" . $this->input->post('nic2') . "-" . $this->input->post('nic3');

                    $file = $this->hr_model->do_upload('employee_photo');
                    $thumb = $this->hr_model->resize_image($file['upload_data']['full_path'], "upload/Thumb_Nails");
                    $thumbNail = $thumb['upload_data']['file_name'];
                    $file_name = $file['upload_data']['file_name'];

                    if ($file_name == '') {
                        $file_name = 'default.png';
                    }
                    $firstName = $this->input->post('first_name');
                    $lastName = $this->input->post('last_name');
                    $insert_data = array(
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'full_name' => $firstName . " " . $lastName,
                        'employee_code' => $code,
                        'photograph' => $file_name,
                        'CNIC' => $cnic,
                        'thumbnail' => $file_name
                    );
                    //echo"<pre>";print_r($insert_data);

                    $emp_query = $this->hr_model->create_new_record('employee', $insert_data);
                    // $emp_query = $this->common_model->insert_record('employee',$insert_data);

                    $last_id = $this->hr_model->get_last_id();
                    if(!isset($last_id) || empty($last_id)){
                        $msg = 'Could Not Create New User Account::error';
                        $this->session->set_flashdata('msg',$msg);
                        redirect('human_resource/all_employee_list');
                    }

                    $now = date('Y-m-d');
                    $user_account_data = array(
                        'employee_id' => $last_id,
                        //'user_name'     		=> $this->input->post('user_name'),
                        'employee_email' => $this->input->post('employee_email'),
                        'password' => $this->input->post('password'),
                        'acct_creation_date' => $now,
                        'enabled' => $this->input->post('status'),
                        'user_group' => $this->input->post('user_group')
                    );
                    $user_account_query = $this->hr_model->create_new_record('user_account', $user_account_data);
                    if ($emp_query && $user_account_query) {
                        redirect('human_resource/add_employee_info/' . $last_id);
                    }

                } else {
//                    If Not Checked to Create User Login, Then only Create Employee
                    $cnic = $this->input->post('nic1') . "-" . $this->input->post('nic2') . "-" . $this->input->post('nic3');
                    $file = $this->hr_model->do_upload('employee_photo');
                    $thumb = $this->hr_model->resize_image($file['upload_data']['full_path'], "upload/Thumb_Nails");
                    $thumbNail = $thumb['upload_data']['file_name'];
                    $file_name = $file['upload_data']['file_name'];

                    if ($file_name == '') {
                        $file_name = 'default.png';
                    }

                    $firstName = $this->input->post('first_name');
                    $lastName = $this->input->post('last_name');
                    $insert_data = array(
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'full_name' => $firstName . " " . $lastName,
                        'employee_code' => $this->input->post('employee_code'),
                        'photograph' => $file_name,
                        'CNIC' => $cnic,
                        'thumbnail' => $file_name
                        // 'full_name' => $firstName.' '.$lastName
                    );

                    //Query to Create Employee Record In Employee Table..
                    $emp_query = $this->hr_model->create_new_record('employee', $insert_data);
                    $last_id = $this->hr_model->get_last_id();
                    if ($emp_query == true) {
                        redirect('human_resource/add_employee_info/' . $last_id);
                    }
                }
            }

        }


        $table = 'employee E';
        $data = array('E.employee_code', false);
        $where = 'E.employee_id = (SELECT MAX(E.employee_id) from employee E where E.trashed = 0)';
        $last_empID = $this->common_model->select_fields_where($table, $data, $where, TRUE);

        if(isset($last_empID) && !empty($last_empID)){
            $data['last_emp_code'] = $last_empID->employee_code;
            //Get the Numeric Value From Employee Code Which Will Be AutoIncremented.
            if(is_numeric($data['last_emp_code'])){
                $EmpNumericCode = intval($data['last_emp_code']);
                echo "is Numeric";
            }else{
                $matches = null;
                $returnValue = preg_match('/-(?<number>\d+)/', $data['last_emp_code'], $matches);
                $EmpNumericCode = intval($matches['number']);
            }
            $NewEmpNumericCode = $EmpNumericCode + 1;
            $totalPad = substr_count($this->data['empCodeFormat'], 'x');
            $empCodeFormatStart = substr($data['last_emp_code'], 0, -$totalPad);
            $newEmployeeCode = str_pad($NewEmpNumericCode, $totalPad, 0, STR_PAD_LEFT);
            $data['newFullEmployeeCode'] = $empCodeFormatStart.$newEmployeeCode;
        }

        $where2 = array('trashed' => 0);
        $data['user_group'] = $this->hr_model->dropdown_wd_option('ml_user_groups', '-- Select Group --', 'user_group_id', 'user_group_title', $where2);

        //$data['msg'] = $this->hr_model->get_row_by_where('employee' , array('employee.employee_id'=> $emp_query));
        // print_r($data['msg']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/add_employees', $data);
        $this->load->view('includes/footer');
    }

    public function add_employee_info($employee_id = NULL)
    {
        $where = array('employee_id' => $employee_id);
        $where2 = array('trashed' => 0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['gender'] = $this->hr_model->dropdown_wd_option('ml_gender_type', '-- Select Gender --', 'gender_type_id', 'gender_type_title', $where2);
        $data['marital_status'] = $this->hr_model->dropdown_wd_option('ml_marital_status', '-- Select Marital Status --', 'marital_status_id', 'marital_status_title', $where2);
        $data['nationality'] = $this->hr_model->dropdown_wd_option('ml_nationality', '-- Select Nationality --', 'nationality_id', 'nationality', $where2);
        $data['religion'] = $this->hr_model->dropdown_wd_option('ml_religion', '-- Select Religion --', 'religion_id', 'religion_title', $where2);
        $data['employee_id'] = $this->uri->segment(3);

        /*        var_dump($this->input->post());
                return;*/
        if ($this->input->post()) {
            $this->db->trans_start();

            $emp_id = $this->input->post('emp_id');
            $cnic = $this->input->post('nic1') . "-" . $this->input->post('nic2') . "-" . $this->input->post('nic3');

            $DateofBirth = $this->input->post('date_of_birth');
            if(isset($DateofBirth) && ($DateofBirth))
            {
                $DateofBirth = date('Y-m-d', strtotime($DateofBirth));
            }

            $visaExpiry = $this->input->post('visa_expiry');
            if(isset($visaExpiry) && ($visaExpiry))
            {
                $visaExpiry = date('Y-m-d', strtotime($visaExpiry));
            }

            $insert_data = array(
                'full_name' => $this->input->post('full_name'),
                'father_name' => $this->input->post('father_name'),
                'CNIC' => $cnic,
                'gender' => $this->input->post('gender'),
                'marital_status' => $this->input->post('marital_status'),
                'nationality' => $this->input->post('nationality'),
                'religion' => $this->input->post('religion'),
                'tax_number' => $this->input->post('tax_number'),
                'date_of_birth' => $DateofBirth
            );
            //echo "<pre>";print_r($insert_data); die;
            if (!empty($emp_id)) {
                $emp_query = $this->hr_model->update_record('employee', $where, $insert_data);
            } else {
                $insert_data = array(
                    'full_name' => $this->input->post('full_name'),
                    'father_name' => $this->input->post('father_name'),
                    'CNIC' => $cnic,
                    'gender' => $this->input->post('gender'),
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'religion' => $this->input->post('religion'),
                    'tax_number' => $this->input->post('tax_number'),
                    'date_of_birth' => $DateofBirth
                );

                $emp_query1 = $this->hr_model->create_new_record('employee', $insert_data);
            }
            if ($this->input->post('immigration')) {
                $insert_immigration = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'passport_num' => $this->input->post('passport_num'),
                    'visa_num' => $this->input->post('visa_num'),
                    'visa_expiry' => $visaExpiry,
                );
                $immigration_query = $this->hr_model->create_new_record('immigration', $insert_immigration);
            }
            $this->db->trans_complete();

            if ($emp_query || $emp_query1 || $immigration_query) {
                redirect('human_resource/add_emp_employment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_employee_personal_info', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Checking Employee Code for Duplicate Insertion
    public function check_employee_code()
    {
        $employee_code = $this->input->post('employee_id');
        $where = array('employee_code' => $employee_code);
        $query = $this->hr_model->chk_employee_code($where);
        if ($query) {
            echo "<span style='color:red'>Employee Code  Already Exists! </span>";
        } else {
            echo "<span style='color:green'>OK</span>";
        }
    }

    /// Function To Check CNIC Already Exist
    public function check_cnic_exist()
    {
        $nic_no = $this->input->post('cnic');
        $where = array('CNIC' => $nic_no);
        $query = $this->hr_model->check_nic_already_exists($where);
        if ($query) {
            echo "<span style='color:red;float: right;margin-right: 37px;'>Employee CNIC Already Exists! </span>";
        } else {
            echo "<span style='color:green;float: right;margin-right: 170px;'> Valid CNIC</span>";
        }
    }

    /// Function To Add Employee Current Contact Details
    public function add_emp_curr_contact($employee_id = NULL)
    {
        $where = array('employee_id' => $employee_id);
        $where2 = array('trashed' => 0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name', $where2);
        $data['district'] = $this->hr_model->dropdown_wd_option('ml_district', '-- Select District --', 'district_id', 'district_name', $where2);
        $data['province'] = $this->hr_model->dropdown_wd_option('ml_province', '-- Select Province --', 'province_id', 'province_name', $where2);
        $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --', 'relation_id', 'relation_name', $where2);
        $data['employee_id'] = $this->uri->segment(3);

        $emp_id = $this->input->post('emp_id');
        if ($this->input->post('continue')) {
            $data['rec'] = $this->hr_model->get_row_by_id('current_contacts', array('employee_id' => $emp_id));
            //print_r($data['rec']);die;

            if (empty($data['rec']->employee_id)) {
                $insert_current = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'address' => $this->input->post('address'),
                    'city_village' => $this->input->post('city_village'),
                    'province' => $this->input->post('province'),
                    'home_phone' => $this->input->post('home_phone'),
                    'mob_num' => $this->input->post('mob_num'),
                    'email_address' => $this->input->post('email_address'),
                    'office_phone' => $this->input->post('office_phone'),
                    'official_email' => $this->input->post('official_email'),
                );
                $current_query = $this->hr_model->create_new_record('current_contacts', $insert_current);

                if ($current_query) {
                    $msg = "<span style='background-color: green; text-align: center; color:#ffffff;'>Insertion Successfully</span>";
                    $this->session->set_flashdata('alert', $msg);
                    redirect('human_resource/add_emp_curr_contact/' . $emp_id);
                }
            } else {
                $msg = "<span style='background-color: red; text-align: center; color:#ffffff;'>Already Inserted Information Before</span>";
                $this->session->set_flashdata('alert', $msg);
                redirect('human_resource/add_emp_curr_contact/' . $emp_id);
                return;
            }
        }

        /**
         * Need To Do Little Changes, That if Record Already Exist Don't Show the Entry Form to that user for that relavent Entry.
         */

        $empData = array('COUNT(1) AS TotalRecords',false);

        //Check If Current Contacts Exist.
        $table = 'current_contacts CC';
        $where = array(
            'CC.employee_id' => $employee_id
        );
        $result = $this->common_model->select_fields_where($table,$empData,$where,TRUE);
        $data['TCurrentContacts'] = $result->TotalRecords;

        //Check if Permanent Contacts Exist.
        $table = 'permanant_contacts PC';
        $where = array(
            'PC.employee_id' => $employee_id
        );
        $result = $this->common_model->select_fields_where($table,$empData,$where,TRUE);
        $data['TPermanentContacts'] = $result->TotalRecords;

        //Check if Emergency Contacts Exist.
        $table = 'emergency_contacts EC';
        $where = array(
            'EC.employee_id' => $employee_id
        );
        $result = $this->common_model->select_fields_where($table,$empData,$where,TRUE);
        $data['TEmergencyContacts'] = $result->TotalRecords;

        $this->load->view('includes/header');
        $this->load->view('HR/add_contact_details', $data);
        $this->load->view('includes/footer');
    }

    public function add_city2()
    {
        if ($this->input->post('city_name')) {
            $rec = $this->site_model->get_all('ml_city');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach ($rec as $rec) {
                    if (strcasecmp($rec->city_name, $this->input->post('city_name')) == 0) {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }

            $cityName = $this->input->post('city_name');
            if (!isset($cityName) || empty($cityName)) {
                echo "FAIL::Data Is Not Posted Accurately::error";
                return;
            }


            //Data To Enter In to The Database..
            $table = 'ml_city';
            $data = array(
                'city_name' => $cityName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);

            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;
            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }
        }
    }

    public function add_province2()
    {
        if ($this->input->post('province_name')) {

            $rec = $this->site_model->get_all('ml_province');

            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach ($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->province_name, $this->input->post('province_name')) == 0) {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }
            $provinceName = $this->input->post('province_name');
            if (!isset($provinceName) || empty($provinceName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_province';
            $data = array(
                'province_name' => $provinceName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    public function add_distt()
    {
        if ($this->input->post('district_name')) {

            $rec = $this->site_model->get_all('ml_district');

            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->district_name,$this->input->post('district_name')) == 0)

                    {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('district_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_district';
            $data = array(
                'district_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    public function add_relation()
    {
        if ($this->input->post('relation_name')) {

            $rec = $this->site_model->get_all('ml_relation');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->relation_name,$this->input->post('relation_name')) == 0)

                    {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }

            $add_type = array(
                'relation_name' => $this->input->post('relation_name'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_relation', $add_type);
            if ($query) {
                echo "OK::Record Successfully Inserted::success";
                return true;
            }
        }
    }

    /// Function To Add Employee Permanent Contact Details
    public function add_emp_perm_contact($employee_id = NULL)
    {

        $where = array('employee_id' => $employee_id);
        $where2 = array('trashed' => 0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where2);
        $data['district'] = $this->hr_model->dropdown_wd_option('ml_district', '-- Select District --', 'district_id', 'district_name',$where2);
        $data['province'] = $this->hr_model->dropdown_wd_option('ml_province', '-- Select Province --', 'province_id', 'province_name',$where2);
        $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --', 'relation_id', 'relation_name',$where2);
        $data['employee_id'] = $this->uri->segment(3);
        $emp_id = $this->input->post('emp_id');
        if ($this->input->post('continue')) {
            $data['rec'] = $this->hr_model->get_row_by_id('permanant_contacts',array('employee_id' => $emp_id ));
            //print_r($data['rec']);die;
            if(@$data['rec']->employee_id == $emp_id )
            {
                $msg="<span style='background-color: red; text-align: center; color:#ffffff;'>Already Inserted Information Before</span>";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_perm_contact/' . $emp_id);
                return;
            }
            $insert_permanent = array(
                'employee_id' => $this->input->post('emp_id'),
                'address' => $this->input->post('address'),
                'city_village' => $this->input->post('city_village'),
                'district' => $this->input->post('district'),
                'province' => $this->input->post('province'),
                'phone' => $this->input->post('phone')
            );
            $permanent_query = $this->hr_model->create_new_record('permanant_contacts', $insert_permanent);

            if ($permanent_query) {
                $msg ="<span style='background-color: green; text-align: center; color:#ffffff;'>Insertion Successfully</span>";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_curr_contact/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_contact_details', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Emergency Contact Details
    public function add_emp_emerg_contact($employee_id = NULL)
    {

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $emp_id = $this->input->post('emp_id');

        $data['emrg_detail'] = $this->hr_model->emergency_contact_detail($where);
        $where2 = array('trashed' => 0);
        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where2);
        $data['district'] = $this->hr_model->dropdown_wd_option('ml_district', '-- Select District --', 'district_id', 'district_name',$where2);
        $data['province'] = $this->hr_model->dropdown_wd_option('ml_province', '-- Select Province --', 'province_id', 'province_name',$where2);
        $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --', 'relation_id', 'relation_name',$where2);
        $data['employee_id'] = $this->uri->segment(3);

        if ($this->input->post('continue')) {
            $where2 = array('emergency_contacts.employee_id' => $emp_id);
            $data['re'] = $this->hr_model->get_row_by_where('emergency_contacts',$where2);
            if(!empty($data['re'])){
                $msg ="<span style='background-color: red; text-align: center; color:#ffffff;'>OOps Already Inserted !</span>";
                $this->session->set_flashdata('sms',$msg);
                redirect('human_resource/add_emp_emerg_contact/'. $emp_id);
                return;

            }
            else {
                $insert_emergency = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'cotact_person_name' => $this->input->post('cotact_person_name'),
                    'relationship' => $this->input->post('relationship'),
                    'home_phone' => $this->input->post('home_phone'),
                    'mobile_num' => $this->input->post('mobile_num'),
                    'work_phone' => $this->input->post('work_phone')
                );
                $emergency_query = $this->hr_model->create_new_record('emergency_contacts', $insert_emergency);
            }
            if ($emergency_query) {
                $msg ="<span style='background-color: green; text-align: center; color:#ffffff;'>Insertion Successfully</span>";
                $this->session->set_flashdata('sms',$msg);
                redirect('human_resource/add_emp_qualification/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_contact_details', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Emergency Contact
    public function send_2_trash($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $trash = array('emergency_contacts.trashed' => 1);
        $data['emergency'] = $this->hr_model->get_row_by_where('emergency_contacts', array('employee_id' => $employee_id));
        if ($data['emergency']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emergency_contacts', array('emergency_contact_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('emergency_contacts', array('emergency_contact_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'emergency_contacts',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_curr_contact/' . $employee_id);
        }
    }

    /// Function to Add Employee Qualification
    public function add_emp_qualification($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $whereqq = array('employee_id' => $employee_id,'qualification.trashed' =>0);
        $where2 = array('trashed' => 0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $data['qualification'] = $this->hr_model->dropdown_wd_option('ml_qualification_type', '-- Select Qualification --',
            'qualification_type_id', 'qualification_title',$where2);
        $data['qualification_detail'] = $this->hr_model->qualification_detail($whereqq);

        if ($this->input->post('continue')) {
            /* $qualification = $this->input->post('qualification1') . "" . $this->input->post('qualification2') . "" . $this->input->post('qualification3');
             $institute = $this->input->post('institute1') . "" . $this->input->post('institute2') . "" . $this->input->post('institute3');
             $year = $this->input->post('year1') . "" . $this->input->post('year2') . "" . $this->input->post('year3');*/
            $type = $this->input->post('qualification_type_id');
            switch ($type) {
                case '1' :
                    $qualification = $this->input->post('qualification1');
                    $institute = $this->input->post('institute1');
                    $year = $this->input->post('year1');
                    break;
                case '2' :
                    $qualification = $this->input->post('qualification2');
                    $institute = $this->input->post('institute2');
                    $year = $this->input->post('year2');
                    break;
                case '3' :
                    $qualification = $this->input->post('qualification3');
                    $institute = $this->input->post('institute3');
                    $year = $this->input->post('year3');
                    break;
            }
            if(isset($year) && !empty($year))
            {
                $year = date('Y-m-d',strtotime($year));
            }

            $insert_qualification = array(
                'employee_id' => $this->input->post('emp_id'),
                'qualification_type_id' => $this->input->post('qualification_type_id'),
                'qualification' => $qualification,
                'institute' => $institute,
                'major_specialization' => $this->input->post('major_specialization'),
                'year' => $year,
                'gpa_score' => $this->input->post('gpa_score'),
                'duration' => $this->input->post('duration'),
            );
            //echo "<pre>";print_r($insert_qualification);die;
            $qualification_query = $this->hr_model->create_new_record('qualification', $insert_qualification);

            if ($qualification_query) {
                $msg ="Record Successfully Inserted !::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_qualification/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_qualification', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Qualification
    public function send_qualify_2_trash($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $trash = array('qualification.trashed' => 1);
        $data['qualification'] = $this->hr_model->get_row_by_where('qualification', array('employee_id' => $employee_id));
        if ($data['qualification']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'qualification',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_qualification/' . $employee_id);
        }
    }

    /// Function to Add Employee Experience
    public function add_emp_experience($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $where_exp = array('employee_id' => $employee_id,'emp_experience.trashed' =>0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Title --',
            'ml_designation_id', 'designation_name',$where2);
        $data['employment'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$where2);
        $data['experience_detail'] = $this->hr_model->emp_experience_detail($where_exp);

        if ($this->input->post('continue')) {

            $fromDate = $this->input->post('from_date');

            $toDate = $this->input->post('to_date');

            if(isset($fromDate) && !empty($fromDate))
            {
                $fromDate = date('Y-m-d', strtotime($fromDate));
            }

            if(isset($toDate) && !empty($toDate))
            {
                $toDate = date('Y-m-d',strtotime($toDate));
            }


            $insert_experience = array(
                'employee_id' => $this->input->post('emp_id'),
                'job_title' => $this->input->post('job_title'),
                'ml_employment_type_id' => $this->input->post('ml_employment_type_id'),
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'organization' => $this->input->post('organization'),
                'comment' => $this->input->post('comment'),
            );
            //echo "<pre>";print_r($insert_qualification);die;
            $experience_query = $this->hr_model->create_new_record('emp_experience', $insert_experience);

            if ($experience_query) {
                $msg ="Record Successfully Inserted !::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_experience/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_experience', $data);
        $this->load->view('includes/footer');
    }
    public function add_designation()
    {
        if ($this->input->post('designation_name')) {

            $rec = $this->site_model->get_all('ml_designations');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    if (strcasecmp($rec->designation_name,$this->input->post('designation_name')) == 0)
                    {
                        echo "FAIL::Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('designation_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_designations';
            $data = array(
                'designation_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }



    /// Function to Add Employee Skills
    public function add_emp_skill($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $where2 = array('trashed' => 0);
        $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$where2);
        $data['skill_level'] = $this->hr_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$where2);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $EmploymentIDs = $data['empl']->employment_id;
//echo $employee_id; die;
        if(!isset($EmploymentIDs) && empty($EmploymentIDs))
        {
            //echo "test"; die;
            $msg ="You Need to provide Employment Information!::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('human_resource/add_emp_employment/' . $employee_id);
            return;
        }





        if ($this->input->post('continue')) {

            $emp_id = $this->input->post('emp_id');
            $employment_id = $this->input->post('employment_id');
            $SkillType = $this->input->post('ml_skill_type_id');


            //Need To Check if Record Already Exist..
            $table = 'emp_skills';
            $selectData = 'COUNT(1) AS TotalRecords';
            $where = array(
                'employment_id' =>$employment_id,
                'trashed' => 0,
                'ml_skill_type_id' => $SkillType
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ="Record Already Exist !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_skill/' . $emp_id);
                return;
            }

            $insert_skills = array(
                'employment_id' => $employment_id,
                'ml_skill_type_id' => $SkillType,
                'ml_skill_level_id' => $this->input->post('Skill_level'),
                'comments' => $this->input->post('comments')
            );
            $skills_query = $this->hr_model->create_new_record('emp_skills', $insert_skills);

            if ($skills_query) {
                $msg ="Record Successfully Inserted !::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_skill/' . $emp_id);
            }
        }

        $employmentID = $data['empl']->employment_id;

        $where_sk = array('emp_skills.employment_id' => $employmentID, 'emp_skills.trashed' => 0);
        $data['skill_details'] = $this->hr_model->emp_skill_detail($where_sk);

        $this->load->view('includes/header');
        $this->load->view('HR/add_skills_trainings', $data);
        $this->load->view('includes/footer');
    }

    public function add_skill()
    {
        if ($this->input->post('skill_name')) {

            $rec = $this->site_model->get_all('ml_skill_type');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    if (strcasecmp($rec->skill_name,$this->input->post('skill_name')) == 0)
                    {
                        echo "FAIL::Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('skill_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_skill_type';
            $data = array(
                'skill_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }



    /// Function to Add Employee Trainings
    public function add_emp_training($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed'=> 0);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = get_employment_from_employeeID($employee_id);
        $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$where2);
        $where_sk = array('emp_skills.employment_id' => $employmentID, 'emp_skills.trashed' => 0);
        $data['skill_details'] = $this->hr_model->emp_skill_detail($where_sk);


        $where_tr = array('employee_id' => $employee_id, 'training.trashed' => 0);
        $data['training_detail'] = $this->hr_model->get_all_by_where('training', $where_tr);

        if ($this->input->post('continue')) {
            $fromDate = $this->input->post('frm_date');
            $toDate = $this->input->post('to_date');
            if(isset($fromDate) && !empty($fromDate)){
                $fromDate = date('Y-m-d',strtotime($fromDate));
            }
            if(isset($toDate) && !empty($toDate)){
                $toDate = date('Y-m-d',strtotime($toDate));
            }
            $insert_training = array(
                'employee_id' => $emp_id,
                'training_name' => $this->input->post('training_name'),
                'Institute' => $this->input->post('Institute'),
                'frm_date' => $fromDate,
                'to_date' => $toDate,
                'tdays' => $this->input->post('tdays'),
                'description' => $this->input->post('description')
            );
            //echo "<pre>";print_r($insert_qualification);die;
            $training_query = $this->hr_model->create_new_record('training', $insert_training);

            if ($training_query) {
                redirect('human_resource/add_emp_training/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_skills_trainings', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Add Employee Employment
    public function add_emp_employment($employee_id = NULL)
    {
//        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where_oo = array('employee_id' => $employee_id,'employment.current'=>1);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['experience'] = $this->hr_model->get_row_by_where('emp_experience', $where);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where_oo);
//echo"<pre>";print_r($data['employment']);die;

        if($data['employment'] !== NULL and isset($data['employment'])){
            $where_employment = array('employment_id' => $data['employment']->employment_id, 'current' => 1,'trashed'=> 0);
            $where_posting = array('employement_id' => $data['employment']->employment_id, 'current' => 1);
            $where_contract = array('employment_id' => $data['employment']->employment_id, 'current' => 1);
            $data['posting'] = $this->hr_model->get_row_by_where('posting', $where_posting);
            $data['position_mgt'] = $this->hr_model->get_row_by_where('position_management', $where_posting);
            $data['contract'] = $this->hr_model->get_row_by_where('contract',array('employment_id' => $data['employment']->employment_id));
            //Project Details
            $EmploymentID = get_employment_from_employeeID($employee_id);
            $whereEmployment = array('employment_id' => $EmploymentID, 'current' => 1,'trashed'=> 0);
            $data['project'] = $this->hr_model->get_all_by_where('employee_project', $whereEmployment);
        }




        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Designation --', 'ml_designation_id',
            'designation_name',$where2);

        $data['employment_type'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$where2);

        $data['job_category'] = $this->hr_model->dropdown_wd_option('ml_job_category', '-- Select Job Category --', 'job_category_id',
            'job_category_name',$where2);

        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id',
            'department_name',$where2);

        $data['branch'] = $this->hr_model->dropdown_wd_option('ml_branch', '-- Select Branch --', 'branch_id', 'branch_name',$where2);

        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where2);

        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --',
            'posting_locations_list_id', 'posting_location',$where2);

        $data['work_shift'] = $this->hr_model->dropdown_wd_option('ml_shift', '-- Select Work Shift --', 'shift_id', 'shift_name',$where2);

        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --',
            'ml_pay_grade_id', 'pay_grade',$where2);

        if ($this->input->post('continue')) {
            $this->db->trans_start();
//                var_dump($data['employment']);
            if ((isset($data['posting']) && $data['posting']->current == 1) || (isset($data['position_mgt']) && $data['position_mgt']->current == 1) || (isset($data['employment']) && $data['employment']->current == 1)) {
                $msg = "Inserted Already !";
                $this->session->set_flashdata('alert', $msg);
            } else {
                $joindate = $this->input->post('joining_date');
                if(isset($joindate) && !empty($joindate))
                {
                    $joindate = date('Y-m-d',strtotime($joindate));
                }
                $insert_employment = array(
                    'employee_id' => $emp_id,
                    'joining_date' => $joindate,
                    'current' => 1,
                    'trashed' => 0
                );
                $query_employment = $this->hr_model->create_new_record('employment', $insert_employment);
                $employment_id = $this->db->insert_id();

                //Getting All The Posted Values
                $postedLocation = $this->input->post('location');
                if(!isset($postedLocation) || empty($postedLocation)){
                    $msg = 'Please Select Location From Dropdown::FAIL';
                    $this->session->set_flashdata('msg',$msg);
                    redirect('human_resource/add_emp_employment/'.$employee_id);
                    return;
                }
                $insert_posting = array(
                    'employement_id' => $employment_id,
                    'poting_start_date' => $joindate,
                    'ml_department_id' => $this->input->post('department'),
                    'ml_branch_id' => $this->input->post('branch'),
                    'ml_city_id' => $this->input->post('city'),
                    'location' => $postedLocation,
                    'shift' => $this->input->post('work_shift')
                );
                $query_posting = $this->hr_model->create_new_record('posting', $insert_posting);

                $contractstartdate =  $this->input->post('contract_start_date');
                $contractexpirydate =  $this->input->post('contract_expiry_date');
                $datecontractexpirydate = $this->input->post('date_contract_expiry_alert');
                $probAlertDate =  $this->input->post('prob_alert_dat');
                if(isset($contractstartdate) && !empty($contractstartdate))
                {
                    $contractstartdate = date('Y-m-d',strtotime($contractstartdate));
                }

                if(isset($contractexpirydate) && !empty($contractexpirydate))
                {
                    $contractexpirydate = date('Y-m-d',strtotime($contractexpirydate));
                }
                if(isset($probAlertDate) && !empty($probAlertDate))
                {
                    $probAlertDate = date('Y-m-d',strtotime($probAlertDate));
                }

                if(isset($datecontractexpirydate) && !empty($datecontractexpirydate))
                {
                    $datecontractexpirydate = date('Y-m-d',strtotime($datecontractexpirydate));
                }




                $insert_contract = array(
                    'employment_id' => $employment_id,
                    'contract_start_date' => $contractstartdate,
                    'employment_type' => $this->input->post('employment_type'),
                    'employment_category' => $this->input->post('job_category'),
                    'prob_alert_dat' => $probAlertDate,
                    'probation' => $this->input->post('probation'),
                    'contract_expiry_date' => $contractexpirydate,
                    'comments' => $this->input->post('comments'),
                    'date_contract_expiry_alert' => $datecontractexpirydate
                );
                $query_contract = $this->hr_model->create_new_record('contract', $insert_contract);

                $insert_position_mgt = array(
                    'employement_id' => $employment_id,
                    'ml_pay_grade_id' => $this->input->post('pay_grade'),
                    'ml_designation_id' => $this->input->post('curr_job_title'),
                    'job_specifications' => $this->input->post('job_specification'),
                    'from_date' => $joindate,
                    'status' => 1
                );
                $query_position_mgt = $this->hr_model->create_new_record('position_management', $insert_position_mgt);

                /* $counter_type = 0;
                 $project_id = $this->input->post('project_id');
                 //echo "<pre>";print_r($approval_type); die;
                 foreach ($project_id as $type) {
                     $insert_project = array(
                         'employment_id' => $employment_id,
                         'project_id' => $type
                     );
                     $insert_project = $this->hr_model->create_new_record('employee_project', $insert_project);
                     $counter_type++;
                 }*/

                $this->db->trans_complete();

                if ($query_employment || $query_posting || $query_contract || $query_position_mgt || $insert_project) {
                    redirect('human_resource/add_emp_curr_contact/' . $emp_id);
                }
            }
        }
        $data['project_id'] 	= $this->input->post('project_id');
        $data['approval'] = $this->site_model->dropdown_wd_option('ml_projects', '-- Select Project --',
            'project_id','project_title',$where2);
        $this->load->view('includes/header');
        $this->load->view('HR/add_employment_info', $data);
        $this->load->view('includes/footer');
    }
    public function add_designation5()
    {
        if ($this->input->post('desi')) {

            $rec = $this->site_model->get_all('ml_designations');

            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->designation_name,$this->input->post('desi')) == 0)
                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('desi');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_designations';
            $data = array(
                'designation_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    public function add_department2()
    {
        if ($this->input->post('dept_name')) {

            $rec = $this->site_model->get_all('ml_department');

            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->department_name,$this->input->post('dept_name')) == 0)

                    {
                        echo "FAIL::Already Inserted !::error";
                        return;
                    }
                }
            }

            $add_type = array(
                $disttName = $this->input->post('dept_name'));
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_department';
            $data = array(
                'department_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    //////// Function For Add Department Master List For Employee Transfer Using Ajax

    public function AddDepartmentTransfer()
    {
        if ($this->input->post('DeptName')) {

            $rec = $this->site_model->get_all('ml_department');

            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->department_name,$this->input->post('DeptName')) == 0)

                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }

            $add_type = array(
                $disttName = $this->input->post('DeptName'));
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_department';
            $data = array(
                'department_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }



    //////// Function For Add Reason Of Transfer Master List Pencil For Employee Transfer Using Ajax

    public function AddReasonTransfer()
    {
        if ($this->input->post('TransferName')) {

            $rec = $this->site_model->get_all('ml_posting_reason_list');

            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->posting_reason,$this->input->post('TransferName')) == 0)

                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }

            $add_type = array(
                $TransferReason = $this->input->post('TransferName'));
            if (!isset($TransferReason) || empty($TransferReason)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_posting_reason_list';
            $data = array(
                'posting_reason' => $TransferReason,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_city3()
    {
        if ($this->input->post('city_name')) {

            $rec = $this->site_model->get_all('ml_city');
            if(isset($rec) && !empty($rec) && is_array($rec)) {
                foreach ($rec as $rec) {
                    if (strcasecmp($rec->city_name, $this->input->post('city_name')) == 0)
                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('city_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_city';
            $data = array(
                'city_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    public function addCityEdit()
    {
        if ($this->input->post('CityName')) {

            $rec = $this->site_model->get_all('ml_city');
            if(isset($rec) && !empty($rec) && is_array($rec)) {
                foreach ($rec as $rec) {
                    if (strcasecmp($rec->city_name, $this->input->post('CityName')) == 0)
                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('CityName');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_city';
            $data = array(
                'city_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_posting_location()
    {
        if ($this->input->post('posting_location')) {

            $rec = $this->site_model->get_all('ml_posting_location_list');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    if (strcasecmp($rec->posting_location,$this->input->post('posting_location')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $disttName = $this->input->post('posting_location');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_posting_location_list';
            $data = array(
                'posting_location' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    ////// Function For Add Posting Location Master List For Employee Transfer Using Ajax /////

    public function AddPostingLocationTransfer()
    {
        if ($this->input->post('posting_location')) {

            $rec = $this->site_model->get_all('ml_posting_location_list');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    if (strcasecmp($rec->posting_location,$this->input->post('posting_location')) == 0)

                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $disttName = $this->input->post('posting_location');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_posting_location_list';
            $data = array(
                'posting_location' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }



    public function add_emp_dependent($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where2 = array('trashed' => 0);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --',
            'relation_id', 'relation_name',$where2);
        $data['dependent_details'] = $this->hr_model->emp_dependent_detail($where);

        if ($this->input->post('continue')) {

            $dateofBirth = $this->input->post('date_of_birth');

            if(isset($dateofBirth) && !empty($dateofBirth))
            {
                $dateofBirth = date('Y-m-d',strtotime($dateofBirth));
            }

            $insert_dependent = array(
                'employee_id' => $this->input->post('emp_id'),
                'dependent_name' => $this->input->post('dependent_name'),
                'ml_relationship_id' => $this->input->post('ml_relationship_id'),
                'illness'           =>$this->input->post('illness'),
                'date_of_birth' => $dateofBirth
            );
            $query_dependent = $this->hr_model->create_new_record('dependents', $insert_dependent);

            if ($query_dependent) {
                redirect('human_resource/add_emp_dependent/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_dependents', $data);
        $this->load->view('includes/footer');
    }

    public function add_relation23()
    {
        if ($this->input->post('relation_name')) {

            $rec = $this->site_model->get_all('ml_relation');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->relation_name,$this->input->post('relation_name')) == 0)

                    {
                        echo "FAIL::Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('relation_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_relation';
            $data = array(
                'relation_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
///////////////////////////////////new for edit//////////////////////
    public function edit_add_emp_pay_package($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where2 = array('employee_id' => $employee_id,'employment.current'=>1);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where2);
        $data['rec'] =  $this->hr_model->get_row_by_where('salary',array('salary.employement_id'=>$data['employment']->employment_id,'salary.trashed'=>0));

        $where_emp_id = $data['employment']->employment_id;

        if(!isset($where_emp_id) && empty($where_emp_id))
        {
            $msg ="You Need to provide Employment Information!::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('human_resource/edit_emp_pay_package/' . $employee_id);
            return;
        }
        /*var_dump($where_emp_id);die;*/
        $emp_id = $this->input->post('emp_id');


        $employment_id = $this->input->post('employment_id');
        $where2 = array('trashed' => 0);
        $data['pay_grade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --', 'ml_pay_grade_id', 'pay_grade',$where2);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);
        $data['currency'] = $this->hr_model->dropdown_wd_option('ml_currency', '-- Select Currency --', 'ml_currency_id', 'currency_name',$where2);
        $data['pay_rate'] = $this->hr_model->dropdown_wd_option('ml_payrate', '-- Select Pay Rate --', 'ml_payrate_id', 'payrate',$where2);
        $msg = "One Record Status Already Pending";
        if ($this->input->post('continue')) {
            $now = date('Y-m-d');

            switch ($base_salary = $this->input->post('ml_pay_rate_id')) {
                case '1' :
                    $base_salary = $this->input->post('Monthly');
                    break;

                case '2' :
                    $base_salary = $this->input->post('Hourly-Pay');
                    break;

                case '3' :
                    $base_salary = $this->input->post('Daily_pay');
                    break;
                case '4' :
                    $base_salary = $this->input->post('Weekly_pay');
                    break;
                case '5' :
                    $base_salary = $this->input->post('Yearly-Pay');
                    break;
            }

            // Function for checking status is pending..
            $data['chk_stat'] = $this->hr_model->get_row_by_where('salary',array('status =' => 1,'employement_id'=>$where_emp_id));
            //var_dump($data['chk_stat']);die;

            if(isset($data['chk_stat']) && $data['chk_stat']->status == 1)
            {
                $msg = "One Record Status Is Pending..!";
                $this->session->set_flashdata('alert',$msg);
                $id = $this->uri->segment(3);
                redirect('human_resource/edit_emp_pay_package/'.$id);
                return;
            }
            else{



                $insert_pay_package = array(
                    'employement_id' => $this->input->post('employment_id'),
                    'ml_pay_grade' => $this->input->post('ml_pay_grade'),
                    'ml_pay_frequency' => $this->input->post('ml_pay_frequency'),
                    'ml_currency' => $this->input->post('ml_currency'),
                    'ml_pay_rate_id' => $this->input->post('ml_pay_rate_id'),
                    'created_by' => $this->session->userdata('employee_id'),
                    'date_created' => $now,
                    'base_salary' => $base_salary,
                    'status' => 1
                );
                $query_pay_package = $this->hr_model->create_new_record('salary', $insert_pay_package);
                if ($query_pay_package) {
                    redirect('human_resource/edit_emp_pay_package/' . $emp_id);
                }
            }
        }
    }




//////////////////////////////////////////////////////////////////////////end//


    public function add_emp_pay_package($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where2 = array('employee_id' => $employee_id,'employment.current'=>1);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where2);
        $data['rec'] =  $this->hr_model->get_row_by_where('salary',array('salary.employement_id'=>$data['employment']->employment_id,'salary.trashed'=>0));

        $where_emp_id = $data['employment']->employment_id;

        if(!isset($where_emp_id) && empty($where_emp_id))
        {
            $msg ="You Need to provide Employment Information!::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('human_resource/add_emp_employment/' . $employee_id);
            return;
        }
        /*var_dump($where_emp_id);die;*/
        $emp_id = $this->input->post('emp_id');


        $employment_id = $this->input->post('employment_id');
        $where2 = array('trashed' => 0);
        $data['pay_grade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --', 'ml_pay_grade_id', 'pay_grade',$where2);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);
        $data['currency'] = $this->hr_model->dropdown_wd_option('ml_currency', '-- Select Currency --', 'ml_currency_id', 'currency_name',$where2);
        $data['pay_rate'] = $this->hr_model->dropdown_wd_option('ml_payrate', '-- Select Pay Rate --', 'ml_payrate_id', 'payrate',$where2);
        $msg = "One Record Status Already Pending";
        if ($this->input->post('continue')) {
            $now = date('Y-m-d');

            switch ($base_salary = $this->input->post('ml_pay_rate_id')) {
                case '1' :
                    $base_salary = $this->input->post('Monthly');
                    break;

                case '2' :
                    $base_salary = $this->input->post('Hourly-Pay');
                    break;

                case '3' :
                    $base_salary = $this->input->post('Daily_pay');
                    break;
                case '4' :
                    $base_salary = $this->input->post('Weekly_pay');
                    break;
                case '5' :
                    $base_salary = $this->input->post('Yearly-Pay');
                    break;
            }

            // Function for checking status is pending..
            $data['chk_stat'] = $this->hr_model->get_row_by_where('salary',array('status =' => 1,'employement_id'=>$where_emp_id));
            //var_dump($data['chk_stat']);die;

            if(isset($data['chk_stat']) && $data['chk_stat']->status == 1)
            {
                $msg = "One Record Status Is Pending..!";
                $this->session->set_flashdata('alert',$msg);
                $id = $this->uri->segment(3);
                redirect('human_resource/add_emp_pay_package/'.$id);
                return;
            }
            else{



                $insert_pay_package = array(
                    'employement_id' => $this->input->post('employment_id'),
                    'ml_pay_grade' => $this->input->post('ml_pay_grade'),
                    'ml_pay_frequency' => $this->input->post('ml_pay_frequency'),
                    'ml_currency' => $this->input->post('ml_currency'),
                    'ml_pay_rate_id' => $this->input->post('ml_pay_rate_id'),
                    'created_by' => $this->session->userdata('employee_id'),
                    'date_created' => $now,
                    'base_salary' => $base_salary,
                    'status' => 1
                );
                $query_pay_package = $this->hr_model->create_new_record('salary', $insert_pay_package);
                if ($query_pay_package) {
                    redirect('human_resource/add_emp_pay_package/' . $emp_id);
                }
            }
        }
        $id = array('employee.employee_id' => $this->uri->segment(3));
        $data['record'] = $this->hr_model->base_salary($id);
        $this->load->view('includes/header');
        $this->load->view('HR/add_pay_package', $data);
        $this->load->view('includes/footer');
    }

    public function add_curr22()
    {
        if ($this->input->post('currency_name')) {

            $rec = $this->site_model->get_all('ml_currency');
            foreach($rec as $rec) {
                //strcasecmp($rec->module_name,$this->input->post('module_name'));
                if (strcasecmp($rec->currency_name,$this->input->post('currency_name')) == 0)

                {
                    $msg ="OOps Already Inserted !";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('human_resource/add_emp_pay_package/' . $this->uri->segment(3));
                    return;}}

            $disttName = $this->input->post('currency_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_currency';
            $data = array(
                'currency_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_employment_type()
    {
        if ($this->input->post('employmentType')) {
            // echo "test"; die;
            $rec = $this->site_model->get_all('ml_employment_type');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->employment_type,$this->input->post('employment_type')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $dEmpType = $this->input->post('employmentType');
            if (!isset($dEmpType) || empty($dEmpType)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_employment_type';
            $data = array(
                'employment_type' => $dEmpType,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    // Function For Edit Employment Info Masterlist //
    public function add_employment_typeEdit()
    {
        if ($this->input->post('Employment')) {
            // echo "test"; die;
            $rec = $this->site_model->get_all('ml_employment_type');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->employment_type,$this->input->post('Employment')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $dEmpType = $this->input->post('Employment');
            if (!isset($dEmpType) || empty($dEmpType)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_employment_type';
            $data = array(
                'employment_type' => $dEmpType,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    public function add_job_category()
    {
        if ($this->input->post('Job_category')) {
            // echo "test"; die;
            $rec = $this->site_model->get_all('ml_job_category');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    if (strcasecmp($rec->job_category_name,$this->input->post('Job_category')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $JobCat = $this->input->post('Job_category');
            if (!isset($JobCat) || empty($JobCat)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_job_category';
            $data = array(
                'job_category_name' => $JobCat,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    /////// Function For Add Job Category Edit Masterlist

    public function add_job_categoryEdit()
    {
        if ($this->input->post('JobCat')) {
            // echo "test"; die;
            $rec = $this->site_model->get_all('ml_job_category');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    if (strcasecmp($rec->job_category_name,$this->input->post('JobCat')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $JobCat = $this->input->post('JobCat');
            if (!isset($JobCat) || empty($JobCat)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_job_category';
            $data = array(
                'job_category_name' => $JobCat,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    public function add_pay_grade()
    {
        if ($this->input->post('pay_grade')) {

            $rec = $this->site_model->get_all('ml_pay_grade');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {

                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if ($rec->pay_grade == $this->input->post('pay_grade'))
                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $PayGrade = $this->input->post('pay_grade');
            if (!isset($PayGrade) || empty($PayGrade)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_pay_grade';
            $data = array(
                'pay_grade' => $PayGrade,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    //// Function For pay Grade Edit Master list /////

    public function add_pay_gradeEdit()
    {
        if ($this->input->post('paygrade')) {

            $rec = $this->site_model->get_all('ml_pay_grade');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {

                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if ($rec->pay_grade == $this->input->post('paygrade'))
                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $PayGrade = $this->input->post('paygrade');
            if (!isset($PayGrade) || empty($PayGrade)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_pay_grade';
            $data = array(
                'pay_grade' => $PayGrade,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_branch()
    {
        if ($this->input->post('add_branch')) {

            $rec = $this->site_model->get_all('ml_branch');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {

                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->branch_name,$this->input->post('add_branch')) == 0)
                    {

                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $add_branch = $this->input->post('add_branch');
            if (!isset($add_branch) || empty($add_branch)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_branch';
            $data = array(
                'branch_name' => $add_branch,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    /////  Function For Employee Transfer Branch Master list Using Ajax

    public function AddBranchTransferEmployee()
    {
        if ($this->input->post('AddBranch')) {

            $rec = $this->site_model->get_all('ml_branch');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {

                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->branch_name,$this->input->post('AddBranch')) == 0)
                    {

                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $add_branch = $this->input->post('AddBranch');
            if (!isset($add_branch) || empty($add_branch)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_branch';
            $data = array(
                'branch_name' => $add_branch,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    //// Function For Branch Master List To Edit /////
    public function add_branchEdit()
    {
        if ($this->input->post('AddBranch')) {

            $rec = $this->site_model->get_all('ml_branch');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {

                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->branch_name,$this->input->post('AddBranch')) == 0)
                    {

                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $add_branch = $this->input->post('AddBranch');
            if (!isset($add_branch) || empty($add_branch)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_branch';
            $data = array(
                'branch_name' => $add_branch,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_workShift()
    {
        if ($this->input->post('ws')) {

            $rec = $this->site_model->get_all('ml_shift');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {

                    if (strcasecmp($rec->shift_name,$this->input->post('ws')) == 0)
                    {
                        echo "FAIL::Record Already Inserted !::error";
                        return;
                    }
                }

            }


            $workShift = $this->input->post('ws');
            if (!isset($workShift) || empty($workShift)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_shift';
            $data = array(
                'shift_name' => $workShift,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    ////// Function For Work Shift Master List For Employee Transfer Using Ajax ////

    public function AddWorkShiftTransfer()
    {
        if ($this->input->post('WorkShift')) {

            $rec = $this->site_model->get_all('ml_shift');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {

                    if (strcasecmp($rec->shift_name,$this->input->post('WorkShift')) == 0)
                    {
                        echo "FAIL::Record Already Inserted !::error";
                        return;
                    }
                }

            }


            $workShift = $this->input->post('WorkShift');
            if (!isset($workShift) || empty($workShift)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_shift';
            $data = array(
                'shift_name' => $workShift,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }



    //////// Function For Master List Of Work Shift Edit /////////

    public function AddWorkShiftEdit()
    {
        if ($this->input->post('WorkS')) {

            $rec = $this->site_model->get_all('ml_shift');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {

                    if (strcasecmp($rec->shift_name,$this->input->post('WorkS')) == 0)
                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }


            $workShift = $this->input->post('WorkS');
            if (!isset($workShift) || empty($workShift)) {
                echo "FAIL:: Data Was Is Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_shift';
            $data = array(
                'shift_name' => $workShift,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    /// ************************* ********* *** ******* ************ ***** **** ******************************///
    /// ========================= Functions For Emplyee Entitlements Start Here ==============================///
    /// Function to Add Employee Entitlement (Increments)
    public function add_emp_entitlement_increment($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array(
            'employee.employee_id' => $employee_id
        );
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
//        print_r($data['empl']);die;

        if(!isset($data['empl']->employment_id) && empty($data['empl']->employment_id))
        { $msg ="You Need to provide Employment Information!::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('human_resource/add_emp_employment/' . $employee_id);
            return;
        }

        $benefitwhere = array(
            'benefits.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'benefits.trashed' => 0
        );
        $incrementwhere = array(
            'increments.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'increments.trashed' => 0
        );
        $allowancewhere = array(
            'allowance.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'allowance.trashed' => 0
        );
        $whereIncrements = $incrementwhere;
        //$whereIncrements['increments.trashed'] = 0;
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($whereIncrements);

        $whereAllowance = $allowancewhere;
        //$whereAllowance['allowance.trashed'] = 0;
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($whereAllowance);

        $whereBenefits = $benefitwhere;
        //$whereBenefits['benefits.trashed'] = 0;
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($whereBenefits);

        $allowancewhere = array(
            'leave_entitlement.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'leave_entitlement.trashed' => 0
        );
        $whereLeaveEntitlementDetails = $allowancewhere;
        //$whereLeaveEntitlementDetails['leave_entitlement.trashed'] = 0;
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($whereLeaveEntitlementDetails);

        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$where2);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$where2);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);

        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$where2);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$where2);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$where2);


        if ($this->input->post('add_increment')) {
            $data['src'] = $this->hr_model->get_all_by_where('increments',array('employee_id'=>$emp_id));
            //var_dump($data['src']);die;
            $inc_id = $this->input->post('increment_type_id');
            foreach($data['src'] as $data['src']){
                if(isset($data['src']) && $data['src']->increment_type_id = $inc_id);
                {  $msg ="<span style='background-color: red;'>Already Inserted !";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
                    return;}}
            $insert_increments = array(
                'employment_id' => $data['empl']->employment_id,
                'increment_type_id' => $inc_id,
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $this->input->post('date_effective'),
                'created_by' => $this->session->userdata('employee_id'),
                'date_created' => date('Y-m-d'),
                'status' => 1
            );

            $query_increments = $this->hr_model->create_new_record('increments', $insert_increments);
            if ($query_increments) {
                $msg = "<span style='background-color: green; text-align: center; color:#ffffff;'>Insertion Successful!</span>";
                $this->session->set_flashdata('msg',$msg);

                redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
            }

        }

        $where = array(
            'employee_insurance.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'employee_insurance.trashed' => 0
        );
        $data['insurance'] = $this->hr_model->insurance_employee($where);
        $whered =array('trashed' => 0);
        //print_r($data['insurance']);die;

        $data['insurance_type'] = $this->hr_model->dropdown_wd_option('ml_insurance_type', '-- Select Insurance Type --', 'insurance_type_id',
            'insurance_type_name',$whered);
        //echo"<pre>";print_r( $data['insurance']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/add_entitlements', $data);
        $this->load->view('includes/footer');
    }

    function add_increment(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInUser = $this->data['EmployeeID'];
                $employmentid = $this->input->post('emp_id');
                $increment_amount = $this->input->post('increment_amount');
                $date_effective = $this->input->post('date_effective');
                $increment_type_id = $this->input->post('increment_type_id');
                if(isset($employmentid) && $employmentid >0){
                    if(!isset($increment_amount) || empty($increment_amount) || !is_numeric($increment_amount)){
                        echo "FAIL::Please Provide Numeric Value For Increment Amount::error";
                        return;
                    }

                    if(!isset($date_effective) || empty($date_effective)){
                        echo 'FAIL::Please Provide The Date For This Increment To Effect::error';
                        return;
                    }else{
                        $date_effective = date('Y-m-d',strtotime($date_effective));
                    }

                    if(!isset($increment_type_id) || empty($increment_type_id)){
                        echo 'FAIL::Please Provide Increment Type::error';
                        return;
                    }

                    $insertTable = 'increments';
                    //Before Insertion We First Need To Check If Record Already Exist Against Same Type.
                    $selectData = array('COUNT(1) AS TotalIncrements');
                    $where = array(
                        'employment_id' => $employmentid,
                        'increment_type_id' => $increment_type_id,
                        'trashed' => 0
                    );
                    $selectResult = $this->common_model->select_fields_where($insertTable,$selectData,$where,TRUE);
                    if($selectResult->TotalIncrements > 0){
                        echo "FAIL::Record Already Exist For Same Increment Type::error";
                        return;
                    }
                    //As Everything Worked Fine. Lets Do The Updates..
                    $insertData = array(
                        'employment_id' => $employmentid,
                        'increment_amount' => $increment_amount,
                        'increment_type_id' => $increment_type_id,
                        'date_effective' => $date_effective,
                        'status' => 1,
                        'created_by' => $loggedInUser,
                        'date_created' => $this->data['dbCurrentDate']
                    );
                    $insertResult = $this->common_model->insert_record($insertTable,$insertData);
                    if($insertResult > 0){
                        echo "OK::Record Successfully Added To The System::success::".$insertResult;
                        return;
                    }else{
                        echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }else{
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }
    function trash_increment($incrementID = NULL, $employeeID = NULL){
        if($this->input->is_ajax_request()){
            if(isset($employeeID) && $employeeID > 0 && $incrementID > 0){
                if($this->input->post('delType') === 'delete'){
                    $loggedInUser = $this->data['EmployeeID'];
                    //Now We Only Checked If A User is Coming Through A Proper Channel With Proper Data.
                    //As User Passed The Checks So We Will Do The Trashing..
                    $employmentTable = 'employment';
                    $selectData = 'employment_id AS EmploymentID';
                    $where = array(
                        'current' => 1,
                        'trashed' => 0,
                        'employee_id' => $employeeID
                    );
                    $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
                    // print_r($employmentInfo);die;

                    $table = 'increments';
                    $data = array(
                        'trashed' => 1,
                        'last_modified_by' => $loggedInUser,
                        'modify_date' => $this->data['dbCurrentDate']
                    );
                    $where = array(
                        'employment_id' => $employmentInfo->EmploymentID,
                        'increment_id' => $incrementID
                    );
                    $trashResult = $this->common_model->update($table,$where,$data);
                    if($trashResult === true){
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    }else{
                        echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }
            }
        }
    }

    function trash_benefit_emp_entitlement($benefitID = NULL, $employeeID = NULL)
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $loggedInEmployee = $this->data['EmployeeID'];

                $employmentTable = 'employment';
                $selectData = 'employment_id AS EmploymentID';
                $where = array(
                    'current' => 1,
                    'trashed' => 0,
                    'employee_id' => $employeeID
                );
                $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
//                print_r($employmentInfo);
                if (isset($employeeID) && $employeeID > 0 && $benefitID > 0 && isset($employmentInfo) && $employmentInfo->EmploymentID > 0) {
                    $table = 'benefits';
                    $updateData = array(
                        'trashed' => 1,
                        'modified_by' => $loggedInEmployee,
                        'date_rec_modified' => $this->data['dbCurrentDate']
                    );
                    $where = array(
                        'employment_id' => $employmentInfo->EmploymentID,
                        'emp_benefit_id' => $benefitID
                    );
                    $result = $this->common_model->update($table, $where, $updateData);
                    if ($result === true) {
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    } else {
                        echo "FAIL:: Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }
            }
        }
    }

    function add_benefit_emp_entitlement()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $loggedInEmployee = $this->data['EmployeeID'];
                $benefit_type_id = $this->input->post('benefit_type_id');
                $date_effective = $this->input->post('date_effective');
                $employmentID = $this->input->post('emp_id');

                if(!isset($employmentID) || empty($employmentID) || !is_numeric($employmentID)){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }

                if(!isset($benefit_type_id) || empty($benefit_type_id) || !is_numeric($benefit_type_id)){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                if(!isset($date_effective) || empty($date_effective)){
                    echo "FAIL::Please Specify The Date Effective Field::error";
                    return;
                }else{
                    $date_effective = date('Y-m-d',strtotime($date_effective));
                }

                //Lets Check If Record Already Exist.
                $selectTable = 'benefits';
                $selectData = 'COUNT(1) AS TotalRecordsFound';
                $where = array(
                    'employment_id' => $employmentID,
                    'benefit_type_id' => $benefit_type_id,
                    'trashed' => 0
                );
                $selectResult = $this->common_model->select_fields_where($selectTable, $selectData, $where, TRUE);
                if(isset($selectResult) && $selectResult->TotalRecordsFound > 0){
                    echo "FAIL::Record Already Exist For This Type::error";
                    return;
                }
                //If We Reached Up to This Point, It Means Everything Above Went Fine, Now Lets Start Working On Insertion Of New Data.
                $table = 'benefits';
                $insertData = array(
                    'employment_id' => $employmentID,
                    'benefit_type_id' => $benefit_type_id,
                    'trashed' => 0,
                    'date_effective' => $date_effective,
                    'date_rec_created' => $this->data['dbCurrentDate'],
                    'created_by' => $loggedInEmployee,
                    'status' => 1
                );
                $resultInsert = $this->common_model->insert_record($table, $insertData);
                if ($resultInsert > 0) {
                    echo "OK::Record Successfully Added To The System::success::".$resultInsert;
                    return;
                } else {
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }
    //Add insurance By Ajax Call
    function add_insurance_emp_entitlement(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $postedCertificateNumber = $this->input->post('certificate_number');
                $postedEffectiveDate = $this->input->post('effective_from');
                $postedInsuranceNo = $this->input->post('insurance_no');
                $postedEmployeeID = $this->input->post('emp_id');
                $postedInsuranceTypeID = $this->input->post('insurance_type_id');

                if(!isset($postedEmployeeID) || empty($postedEmployeeID) || !is_numeric($postedEmployeeID)){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                if(!isset($postedInsuranceTypeID) || empty($postedInsuranceTypeID) || !is_numeric($postedInsuranceTypeID)){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }

                if(!isset($postedInsuranceNo) || empty($postedInsuranceNo) ){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }

                if(!isset($postedEffectiveDate) || empty($postedEffectiveDate)){
                    echo "FAIL::Please Specify The Date Effective Field::error";
                    return;
                }else{
                    $EffectiveDate = date('Y-m-d',strtotime($postedEffectiveDate));
                }
                //Insurance Is Based On Employment, So We Need To Find Current Employement ID for posted Employee

                $table = 'employment ET';
                $data = ('ET.employment_id AS EmploymentID');
                $where = array(
                    'ET.employee_id' => $postedEmployeeID,
                    'current' => 1,
                    'trashed' => 0
                );
                $result = $this->common_model->select_fields_where($table, $data, $where, TRUE);
                if(!isset($result) || empty($result)){
                    echo "FAIL::This Employee Is Not In Roll, Can Not Add Details For This Employee::error";
                    return;
                }

                //Saving EmploymentID in a Variable for Later Use.
                $employmentID = $result->EmploymentID;

                //If We Reached Up To This Point, Means Everything Went Fine, We Have All the Required Information For Adding Insurance.
                //But In Order to Insert New Data, We first Need To Know There Already Is Any Data present In Table.

                $table = 'employee_insurance EI';
                $data = array('COUNT(1) AS TotalInsurancesFound',false);
                $where = array(
                    'EI.employment_id' => $employmentID,
                    'EI.trashed' => 0,
                    'ml_insurance_id' => $postedInsuranceTypeID
                );
                $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);

                if($result->TotalInsurancesFound > 0){
                    echo "FAIL:: Record Already Exist For Current Insurance Type::error";
                    return;
                }

                //Now As We Reached Here, Means Record Don't Exist Or is Not Active, Then Now We Can Insert New Data.
                $insertTable = 'employee_insurance';
                $insertData = array(
                    'employment_id' => $employmentID,
                    'trashed' => 0,
                    'effective_from' => $EffectiveDate,
                    'ml_insurance_id' => $postedInsuranceTypeID,
                    'certificate_number' => $postedCertificateNumber,
                    'policy_number' => $postedInsuranceNo
                );
                $insertResult = $this->common_model->insert_record($insertTable,$insertData);
                if($insertResult > 0){
                    echo  "OK::Record Successfully Added To The System::success::$insertResult";
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }
    function trash_insurance_emp_entitlement($insuranceID = NULL, $employeeID = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                if(isset($employeeID) && is_numeric($employeeID) && $employeeID > 0 && $insuranceID > 0){
                    //As The Post Is Posted Through Proper Channel, We Will Do The Deletion..

                    //Need Current EmploymntID for employee.
                    $PTable = 'employee E';
                    $data = array('ET.employment_id AS EmploymentID',false);
                    $joins = array(
                        array(
                            'table' => 'employment ET',
                            'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $employeeID
                    );
                    $employmentResult = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, TRUE);
                    if(!isset($employmentResult) || empty($employmentResult)){
                        echo "FAIL, Some Problem Occurred, Employment Record Do Not Exist, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }

                    $table = 'employee_insurance';
                    $updateData = array(
                        'trashed' => 1
                    );
                    $updateWhere = array(
                        'trashed' => 0,
                        'employee_insurance_id' => $insuranceID,
                        'employment_id' => $employmentResult->EmploymentID
                    );
                    $updateResult = $this->common_model->update($table,$updateWhere,$updateData);
                    if($updateResult === true){
                        echo "OK::Record Successfully Trashed From The System.::success";
                        return;
                    }else{
                        echo "FAIL::Some Database Error Occrred, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }
            }
        }
    }

    //Add Entitled Leaves By Ajax Call
    function  add_leaves_emp_entitlement(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $postedEmployeeID = $this->input->post('emp_id');
                $leaveTypeID = $this->input->post('ml_leave_type_id');
                $totalAllocatedLeaves = $this->input->post('no_of_leaves');

                if(!isset($postedEmployeeID) || empty($postedEmployeeID) || !is_numeric($postedEmployeeID)){
                    echo "FAIL::Some Error In Post, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                if(!isset($leaveTypeID) || empty($leaveTypeID) || !is_numeric($leaveTypeID)){
                    echo "FAIL::Please Select Leave Type::error";
                    return;
                }
                if(!isset($totalAllocatedLeaves) || empty($totalAllocatedLeaves) || !is_numeric($totalAllocatedLeaves)){
                    echo "FAIL::Please Provide Numeric Value For No Of Days::error";
                    return;
                }
                //Leave Entitlement Is Based On Employment, So We Need To Find Current Employement ID for posted Employee


                $table = 'employment ET';
                $data = ('ET.employment_id AS EmploymentID');
                $where = array(
                    'ET.employee_id' => $postedEmployeeID,
                    'current' => 1,
                    'trashed' => 0
                );
                $result = $this->common_model->select_fields_where($table, $data, $where,TRUE);
                if(!isset($result) || empty($result)){
                    echo "FAIL::This Employee Is Not In Roll, Can Not Add Details For This
Employee::error";
                    return;
                }
                $employmentID = $result->EmploymentID;


                //We Now Have The Required data, But To Insert We First Now Need To Check If Record Already Exist.

                $table = 'leave_entitlement LE';
                $data = array('COUNT(1) AS TotalRecordsFound',false);
                $where = array(
                    'employment_id' => $employmentID,
                    'trashed' => 0,
                    'ml_leave_type_id' => $leaveTypeID
                );
                $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                    echo "FAIL::Record Already Exist For Selected Leave Type::error";
                    return;
                }

                //GET contract start date and In date
                $employmentTable = 'contract C';
                $selectData = array('IFNULL(ECE.e_end_date,C.contract_expiry_date) AS ContractExpiryDate,IFNULL(ECE.e_start_date,C.contract_start_date) AS ContractStartDate',false);
                $joins = array(
                    array(
                        'table' => 'employee_contract_extensions ECE',
                        'condition' =>'C.contract_id = ECE.contract_id AND ECE.current = 1 AND ECE.trashed = 0',
                        'type' => 'LEFT'
                    )
                );
                $where = array(
                    'C.status'=> 2,
                    'C.trashed' => 0,
                    'employment_id' => $employmentID
                );
                $employmentInfo = $this->common_model->select_fields_where_like_join($employmentTable,$selectData,$joins,$where,TRUE);

                /*   var_dump($employmentInfo);
                         return;*/
                // $employmentID = $employmentInfo->EmploymentID;

                //$contract start Date = $employmentInfo->ContractStartDate;
                if(isset($employmentInfo) && !empty($employmentInfo))
                {
                    // Need to find leave entitlements before insertion
                    $currentYear = date('Y');
                    $totalDaysInCurrentYear = date("z", mktime(0,0,0,12,31,$currentYear)) + 1;
                    $contractStartDate = strtotime($employmentInfo->ContractStartDate);

                    if (date('Y', $contractStartDate) === date('Y')) {

                        $lastDayOfYear = strtotime($currentYear . '-12-31');
                        $unContractDays = floor(($lastDayOfYear - $contractStartDate) / (60 * 60 * 24));
                        /*                    $contractDays = $totalDaysInCurrentYear - $unContractDays;*/
                        $totalEntitledLeaves = $totalAllocatedLeaves/$totalDaysInCurrentYear;
                        $availableEntitledLeaves = $totalEntitledLeaves * $unContractDays;
                    }
                }else{
                    $availableEntitledLeaves = 0;
                }


                $insertTable = 'leave_entitlement';
                $insertData = array(
                    'employment_id' => $employmentID,
                    'trashed' => 0,
                    'ml_leave_type_id' => $leaveTypeID,
                    'status' => 1,
                    'no_of_leaves_allocated' => $totalAllocatedLeaves,
                    'no_of_leaves_entitled' => $availableEntitledLeaves
                );
                //If We Reached Here, Means Record Don't Exist or Record is not Active. So Its Cool To Create New Record.
                $insertResult = $this->common_model->insert_record($insertTable,$insertData);
                if($insertResult > 0){
                    echo "OK::Record Successfully Created for Leave Entitlement::success::".$insertResult;
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }
    function trash_leave_emp_entitlement($leaveEntitlementID = NULL, $employeeID = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                if(isset($employeeID) && is_numeric($employeeID) && $employeeID > 0 && $leaveEntitlementID > 0){

                    //Need Current EmploymentID for employee.
                    $PTable = 'employee E';
                    $data = array('ET.employment_id AS EmploymentID',false);
                    $joins = array(
                        array(
                            'table' => 'employment ET',
                            'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $employeeID
                    );
                    $employmentResult = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, TRUE);
                    //print_r($employmentResult);die;
                    if(!isset($employmentResult) || empty($employmentResult)){
                        echo "FAIL, Some Problem Occurred, Employment Record Do Not Exist, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }


                    //As Request Has Been Generated Through A proper Channel We Will Allow Deletion
                    $table = 'leave_entitlement';
                    $data = array(
                        'trashed' => 1,
                        'date_rec_modfied' => $this->data['dbCurrentDate'],
                        'modified_by' => $this->data['EmployeeID']
                    );
                    $where = array(
                        'employment_id' => $employmentResult->EmploymentID,
                        'leave_entitlement_id' => $leaveEntitlementID
                    );
                    $updateResult = $this->common_model->update($table,$where,$data);
                    if($updateResult === true){
                        echo "OK::Record Successfully Trashed From Database::success";
                        return;
                    }else{
                        echo "FAIL::Some Database Error Occurred, Record Could Not Be Trashed, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }
            }
        }
    }
    public function add_allowance()
    {
        if ($this->input->post('allowance_type')) {
            $rec = $this->site_model->get_all('ml_allowance_type');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {

                    if (strcasecmp($rec->allowance_type,$this->input->post('allowance_type')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('allowance_type');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_allowance_type';
            $data = array(
                'allowance_type' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    public function add_benefit_type()
    {
        if ($this->input->post('benefit_type_title')) {

            $rec = $this->site_model->get_all('ml_benefit_type');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->benefit_type_title,$this->input->post('benefit_type_title')) == 0)

                    {
                        echo "FAIL:: Record Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('benefit_type_title');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_benefit_type';
            $data = array(
                'benefit_type_title' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    public function add_insure23()
    {
        if ($this->input->post('insurance_type_name')) {

            $rec = $this->site_model->get_all('ml_insurance_type');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec)
                {
                    if (strcasecmp($rec->insurance_type_name,$this->input->post('insurance_type_name')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }


            $disttName = $this->input->post('insurance_type_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_insurance_type';
            $data = array(
                'insurance_type_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    public function add_leave_type()
    {
        if ($this->input->post('leave_type')) {

            $rec = $this->site_model->get_all('ml_leave_type');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {


                foreach($rec as $rec)
                {

                    if (strcasecmp($rec->leave_type,$this->input->post('leave_type')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('leave_type');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Is Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_leave_type';
            $data = array(
                'leave_type' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }


    public function add_insure($employee_id = NULL)
    {
        $id = $this->uri->segment(3);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id'=>$employee_id,'current'=>1));
        $emp_id =  $data['empl']->employment_id;
        //print_r($data['empl']);die;
        if ($this->input->post('add_insurance')) {
            $data['src'] = $this->hr_model->get_all_by_where('employee_insurance',array('employment_id'=>$emp_id));
//var_dump($data['src']);die;
            $ins_id = $this->input->post('insurance_type_id');
            foreach($data['src'] as $data['src']){
                if(isset($data['src']) && $data['src']->insurance_type_id == $ins_id)
                {  $msg ="<span style='background-color: red;'>OOps Already Inserted !";
                    $this->session->set_flashdata('smser',$msg);
                    redirect('human_resource/add_emp_entitlement_increment/' . $employee_id);
                    return;}}

            $insert_insurance = array(
                'employment_id' => $emp_id,
                'ml_insurance_id' => $ins_id,
                'policy_number' =>  $this->input->post('insurance_no'),
                'certificate_number' =>  $this->input->post('certificate_number'),

                'effective_from' => $this->input->post('effective_from')
            );
            // print_r($insert_insurance);die;
            $query_insurance = $this->hr_model->create_new_record('employee_insurance', $insert_insurance);

            if ($query_insurance) {
                $msg = "<span style='background-color: green; text-align: center; color:#ffffff;'>Insertion Successful!</span>";
                $this->session->set_flashdata('smser', $msg);
                redirect('human_resource/add_emp_entitlement_increment/' . $id);
            }
        }
    }
    public function delete_rec($increment_id = NULL,$employee_id = NULL)
    {
        $data['rec'] = $this->hr_model->get_row_by_where('increments',array('increment_id' =>$increment_id));
        // var_dump($data['rec']);die;
        $query = $this->hr_model->delete_row_by_where('increments',array('increment_id' =>$increment_id));
        if($query)
        {
            redirect('human_resource/add_emp_entitlement_increment/'.$employee_id);
        }


    }

    //Deprecated Function.. Updated The Function For Ajax Call..
    public function dle_allowance($allowance_id = NULL, $employee_id =NULL)
    {
        $data['rec'] = $this->hr_model->get_row_by_where('allowance',array('allowance_id'=>$allowance_id));
        $query = $this->hr_model->delete_row_by_where('allowance',array('allowance_id'=>$allowance_id));
        if($query)
        {
            redirect('human_resource/add_emp_entitlement_increment/'.$employee_id);
        }
        //var_dump($data['rec']);die;
    }

    public function del_ben($emp_benefit_id = NULL,$employee_id = NULL)
    {
        $data['re'] = $this->hr_model->get_row_by_where('benefits',array('emp_benefit_id'=>$emp_benefit_id));
        $query = $this->hr_model->delete_row_by_where('benefits',array('emp_benefit_id'=>$emp_benefit_id));
        if($query)
        {
            redirect('human_resource/add_emp_entitlement_increment/'.$employee_id);
        }
        //var_dump($data['re']);die;
    }
    public function del_leav($leave_entitlement_id = NULL, $employee_id = NULL)
    {
        $data['re'] =$this->hr_model->get_row_by_where('leave_entitlement',array('leave_entitlement_id'=>$leave_entitlement_id));
//        var_dump($data['re']);die;
        $query = $this->hr_model->delete_row_by_where('leave_entitlement',array('leave_entitlement_id'=>$leave_entitlement_id));
        if($query)
        {
            redirect('human_resource/add_emp_entitlement_increment/'.$employee_id);
        }
    }
    /// Function to Add Employee Entitlement (Allowances)
    public function add_emp_entitlement_allowance($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $allowancewhere = array(
            'allowance.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'allowance.trashed' => 0
        );
        $incrementwhere= array(
            'increments.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'increments.trashed' => 0
        );
        $benefitwhere= array(
            'benefits.employment_id' => $data['empl']->employment_id,
            'employment.current' => 1,
            'employment.trashed' => 0,
            'benefits.trashed' => 0
        );
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($incrementwhere);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($allowancewhere);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($benefitwhere);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where);

        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id', 'increment_type',$where2);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id', 'allowance_type',$where2);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id', 'benefit_type_title',$where2);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id', 'leave_type',$where2);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$where2);

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $data['src'] = $this->hr_model->get_all_by_where('allowance',array('employment_id'=> $data['empl']->employment_id, 'trashed' => 0));
//var_dump($data['src']);die;
            $all_id = $this->input->post('ml_allowance_type_id');
            if(!empty($data['src']) and is_array($data['src'])){
                foreach($data['src'] as $data['src']){
                    if(isset($data['src']) && $data['src']->ml_allowance_type_id == $all_id)
                    {   echo "FAIL::Record Already Exist::error";
                        return;}
                }
            }


            $now = date('Y-m-d');
            $yer=date('Y');
            $where_year=array('year'=>$yer);
            $year_info=$this->hr_model->get_row_by_id('ml_year',$where_year);
            $year=$year_info->ml_year_id;

            $mo=date('m');
            $month=intval($mo);

            $login_user = $this->session->userdata('employee_id');

            $dateEffective = $this->input->post('effective_date');
            if(!isset($dateEffective) || empty($dateEffective)){
                echo "FAIL::Please Select The Effective Date::error";
                return;
            }else{
                $dateEffective = date('Y-m-d',strtotime($dateEffective));
            }
            $insert_allowance = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_allowance_type_id' => $all_id,
                'allowance_amount' => $this->input->post('allowance_amount'),
                'pay_frequency' => $this->input->post('pay_frequency'),
                'effective_date' => $dateEffective,
                'created_by' => $login_user,
                'month'      =>$month,
                'year'      =>$year,
                'date_rec_created' => $now
            );

            //echo "<pre>"; print_r($insert_allowance);die;
//            $query_allowance = $this->hr_model->create_new_record('allowance', $insert_allowance);

            $allowanceID = $this->common_model->insert_record('allowance',$insert_allowance);
            $allowanceTable = 'allowance A';
            $data = array('MLAT.allowance_type AS AllowanceType,
            MLPF.pay_frequency AS PayFrequency,
            A.effective_date AS EffectiveDate,
            A.allowance_amount AS AllowanceAmount,
            A.allowance_id AS AllowanceID',false);
            $joins = array(
                array(
                    'table' => 'ml_allowance_type MLAT',
                    'condition' => 'MLAT.ml_allowance_type_id = A.ml_allowance_type_id AND MLAT.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_pay_frequency MLPF',
                    'condition' => 'MLPF.ml_pay_frequency_id = A.pay_frequency AND MLPF.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'allowance_id' => $allowanceID
            );
            $allowanceResultData = $this->common_model->select_fields_where_like_join($allowanceTable,$data,$joins,$where,FALSE);
//            print_r($allowanceResultData);

            if($allowanceID > 0)
            {
                echo "OK::Insertion Successful::success>>>";
                if(!empty($allowanceResultData) && is_array($allowanceResultData)){
                    print_r(json_encode($allowanceResultData));
                }
                return;
            }
            else{
                echo "FAIL::Record Already Exist::error";
                return;
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_entitlements', $data);
        $this->load->view('includes/footer');
    }


    /// Function to Trash Employee Entitlement (Allowance)
    public function trash_emp_entitlement_allowance($allowance_id = NULL, $employee_id = NULL)
    {
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //Now As Channel Is OK.
                //We Should Have First Sent Data Through POST, But This Whole Page is Already Been Set to Send ID's In The URL Then Lets Work Trhough URL..
                if($employee_id !== NULL && $employee_id > 0 && $allowance_id > 0){
                    $loggedInEmployeeID = $this->data['EmployeeID'];


                    $employmentTable = 'employment';
                    $selectData = 'employment_id AS EmploymentID';
                    $where = array(
                        'current' => 1,
                        'trashed' => 0,
                        'employee_id' => $employee_id
                    );
                    $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
//                print_r($employmentInfo);

                    //If Everthing Worked Fine. Then Lets Trash The Record From The System.
                    $table = 'allowance';
                    $data = array(
                        'trashed' => 1,
                        'modified_by' => $loggedInEmployeeID,
                        'date_last_modified' => $this->data['dbCurrentDate']
                    );
                    $where = array(
                        'allowance_id' => $allowance_id,
                        'employment_id' => $employmentInfo->EmploymentID
                    );
                    $resultTrashUpdate = $this->common_model->update($table,$where,$data);
                    if($resultTrashUpdate === true){
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    }else{
                        echo "FAIL::Some Database Error, Please Contact System Administrator For Further Assistance::error";
//                        print($this->db->last_query());
                        return;
                    }
                }
            }else{
                echo "FAIL::Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                return;
            }
        }else{
            redirect(previousURL());
        }
    }

    /// Function to Add Employee Entitlement (Benefits)
    public function add_emp_entitlement_benefit($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $where1 = array(
            'employment.employee_id' => $employee_id,
            'current'=> 1,
            'trashed' => 0
        );
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where1);

        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where1);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where);

        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id', 'increment_type',$where2);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id', 'allowance_type',$where2);

        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id', 'benefit_type_title',$where2);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id', 'leave_type',$where2);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$where2);

        if ($this->input->post('add_benefit')) {
            $data['src'] = $this->hr_model->get_all_by_where('benefits',array('employee_id'=>$emp_id));
//var_dump($data['src']);die;
            $bn_id = $this->input->post('benefit_type_id');
            foreach($data['src'] as $data['src']){
                if(isset($data['src']) && $data['src']->benefit_type_id == $bn_id)
                {  $msg ="<span style='background-color: red;'>OOps Already Inserted !</span>";
                    $this->session->set_flashdata('smse',$msg);
                    redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
                    return;}}

            $insert_benefit = array(
                'employment_id' => $this->input->post('emp_id'),
                'benefit_type_id' => $bn_id,
                'date_effective' => $this->input->post('date_effective'),
                'created_by' => $this->session->userdata('employee_id'),
                'date_rec_created' => date('Y-m-d'),
                'status' => 1
            );
            $query_benefit = $this->hr_model->create_new_record('benefits', $insert_benefit);
            if ($query_benefit) {
                $msg = "Insertion Successful!";
                $this->session->set_flashdata('smse',$msg);
                redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_entitlements', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Add Employee Entitlement (Leaves)
    public function add_emp_entitlement_leave($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where);

        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id', 'increment_type',$where2);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id', 'allowance_type',$where2);

        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id', 'pay_frequency',$where2);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id', 'benefit_type_title',$where2);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id', 'leave_type',$where2);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$where2);

        if ($this->input->post('add_leave')) {

            // Function for find record in pending status...
            $data['status'] =$this->hr_model->get_row_by_where('leave_entitlement',array('leave_entitlement.employee_id'=>$this->input->post('emp_id')));
            // var_dump($data['status']);die;
            if(isset($data['status']) &&  $data['status']->status = 1)
            { $msg ="Already One Record Is In Pending Status!!!!";
                $this->session->set_flashdata('lsms',$msg);
                redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
                return;
            }
            else {
                $insert_leave = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'ml_leave_type_id' => $this->input->post('ml_leave_type_id'),
                    'no_of_leaves' => $this->input->post('no_of_leaves'),
                    'status' => 1
                );
                $query_leave = $this->hr_model->create_new_record('leave_entitlement', $insert_leave);
                if ($query_leave) {
                    redirect('human_resource/add_emp_entitlement_increment/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_entitlements', $data);
        $this->load->view('includes/footer');
    }
    /// ========================= Functions For Emplyee Entitlements End Here ==============================///
    /// ************************* ********* *** ******* ************ *** **** ******************************///
    /// Function to Add Employee Report ////
    public function add_emp_report_to($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['report_method'] = $this->hr_model->dropdown_wd_option('ml_reporting_options', '-- Select Report Method --', 'reporting_option_id', 'reporting_option',$where2);
        $supervisor_id = $this->input->post('supervisor_id');

        if ($this->input->post('add_supervisor')) {
            //Getting the Posted values
            var_dump($this->input->post());
            $postedSupervisorID = $this->input->post('supervisor_id');
            $postedReportingMethod = $this->input->post('supervisor_method');

            if(!isset($postedReportingMethod) || empty($postedReportingMethod) || !is_numeric($postedReportingMethod)){
                $msg ="Please Select Reporting Method::error";
                $this->session->set_flashdata('msg',$msg);
                redirect(previousURL());
            }

            //First Need To Check If Record Already Exist Or Not.
            $table = 'reporting_heirarchy';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'for_employee' => $employee_id,
                'employeed_id' => $postedSupervisorID,
                'ml_reporting_heirarchy_id' => $postedReportingMethod,
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_report_to/'. $emp_id);
                return;
            }
            $insert_report_to_sup = array(
                'for_employee'  => $this->input->post('emp_id'),
                'employeed_id' => $supervisor_id,
                'ml_reporting_heirarchy_id' => $this->input->post('supervisor_method')
            );
            $query_supervisor = $this->hr_model->create_new_record('reporting_heirarchy', $insert_report_to_sup);

            if ($query_supervisor) {
                $msg ="Record Successfully Inserted::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_report_to/' . $emp_id);
            }
        }
        $sub_ordinate_id = $this->input->post('sub_ordinate_id');
        if ($this->input->post('add_sub_ordinate')) {

            $subOrdinateID = $this->input->post('SubOrdinateEmployeeID');
            $subReportingMethod = $this->input->post('subordinate_method');
            var_dump($this->input->post());
            //return;
            //Little Fix For Add........
            $table = 'reporting_heirarchy';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'for_employee' => $employee_id,
                'reporting_authority_id' => $subOrdinateID,
                'ml_reporting_heirarchy_id' => $subReportingMethod,
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);

            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_report_to/'. $emp_id);
                return;
            }
            $insert_report_to_sub = array(
                'for_employee'  => $this->input->post('emp_id'),
                'reporting_authority_id' => $subOrdinateID,
                'ml_reporting_heirarchy_id' => $subReportingMethod,
            );
            //echo "<pre>";print_r($insert_report_to_sub);die;
            $query_suordinate = $this->hr_model->create_new_record('reporting_heirarchy', $insert_report_to_sub);

            if ($query_suordinate) {
                $msg ="Record Successfully Inserted::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_report_to/' . $emp_id);
            }
        }
        $where = array('reporting_heirarchy.trashed !=' => 1,'for_employee'=>$employee_id);
        $data['record'] = $this->hr_model->record_report_view_table($where);
        $where2 = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.reporting_authority_id !='=>0,'for_employee'=>$employee_id);
        $data['reports'] = $this->hr_model->record_report_view_table2($where2);
        //echo"<pre>";print_r($data['reports']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/add_report_to', $data);
        $this->load->view('includes/footer');
    }

    /// Auto Complete Function For Report To Supervisor
    public function report_2_supervisor_autocomplete()
    {

        $where = array(
            'employee.employee_id !='=> $this->uri->segment(3)

        );
//var_dump($where);
        $query = $this->hr_model->emp_report_sup_auto_search($where);
        $counter_sup = 1;
        foreach ($query as $row):
            ?>
            <li tabindex="<?php echo $counter_sup ?>"
                onClick="fill('<?php echo $row->full_name ?>','<?php echo $row->employee_id ?>')"><?php echo $row->full_name ?></li> <?php
            $counter_sup++;
        endforeach;
    }

    /// Auto Complete Function For Report To Sub Ordinate
    public function report_2_subordinate_autocomplete()
    {
        $query = $this->hr_model->emp_report_sub_auto_search();
        $counter_sub = 1;
        foreach ($query as $row):
            ?>
            <li tabindex="<?php echo $counter_sub ?>"
                onClick="fill2('<?php echo $row->full_name ?>','<?php echo $row->employee_id ?>')"><?php echo $row->full_name ?></li> <?php
            $counter_sub++;
        endforeach;
    }

    /// Function to Add Employee Attachments
    public function add_emp_attachment($employee_id = NULL)
    {

        $data['employee_id'] = $employee_id;
        $where = array('employee_id' => $employee_id,'trashed !=' => 1);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $data['attach_details'] = $this->hr_model->get_all_by_where('attachment', $where);

        //If Attachment Is Posted..
        if ($this->input->post('continue'))
        {
            $file = $this->hr_model->do_upload('attached_file');
            $file_name = $file['upload_data']['file_name'];
            //echo "<pre>"; print_r($file);die;

            if(!isset($file_name) || empty($file_name))
            {
                $msg = 'This File is Restricted !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_attachment/' . $emp_id);
                return;
            }

            $insert_attachments = array(
                'employee_id' => $emp_id,
                'attached_file' => $file_name,
                'remarks' => $this->input->post('remarks')
            );
            //print_r($insert_attachments);die;
            $query_attachments = $this->hr_model->create_new_record('attachment', $insert_attachments);

            if ($query_attachments) {
                $msg ='Successfully inserted !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_emp_attachment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_attachments',$data);
        $this->load->view('includes/footer');
    }
    /// ***************************** ********* *** ****** **** ** *** ***********************************///
    /// ============================= Functions For Report Side in HRM ===================================///
    /// ***************************** ********* *** ****** **** ** *** ***********************************///
    /// To View All Employees
    public function all_employee_list($parameter = NULL){
        if ($parameter == "list") {
            //Need To Do Little Changes For the Employee Default Avatar..
            $employeeListJSON = $this->hr_model->view_all_employee();
            $employeeListArray = json_decode($employeeListJSON,true);
            if(isset($employeeListArray) && !empty($employeeListArray['aaData'])){
                foreach($employeeListArray['aaData'] as $key=> $val){
                    $imageNameAndExtension = $val['thumbnail'];
                    if(empAvatarExist($imageNameAndExtension) === TRUE){
                        $employeeListArray['aaData'][$key]['thumbnail'] = '<img src="upload/Thumb_Nails/'.$imageNameAndExtension.'" style="width:40px; height:40px" />';
                    }else{
                        $employeeListArray['aaData'][$key]['thumbnail'] = '<img src="'.empAvatarExist($imageNameAndExtension).'" style="width:40px; height:40px" />';
                    }
                }
            }
            echo json_encode($employeeListArray);
            return;
        }
        $where2 = array('trashed' => 0);
        $data['employee_id'] = $this->session->userdata('employee_id');
        $data['slct_dept'] = $this->input->post('department');
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --','department_id', 'department_name',$where2);

        $data['design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --','ml_designation_id', 'designation_name',$where2);
        $data['selectedProject'] = $this->input->post('project');
        $data['project'] = $this->hr_model->dropdown_wd_option('ml_projects', '-- Select Project --','project_id', 'project_title',$where2);
        $data['employee_id'] = $this->uri->segment(3);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/all_employee_list', $data);
        $this->load->view('includes/footer');
    }
    public function all_appraisal_list($param = NULL){

        if($param === "list"){
            //Get Data for List
            $table = "employee E";
            $selectData = array("",false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0
            );

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where);
        }

        //Get Data For Designation Filter
        $where2 = array('trashed' => 0);
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --','ml_designation_id', 'designation_name',$where2);



        $this->load->view('includes/header');
        $this->load->view('HR/Reports/all_appraisal_list', $data);
        $this->load->view('includes/footer');
    }
    public function view_employee_profile($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $employment_id=get_employment_from_employeeID($employee_id);
        $where = array('employee_id' => $employee_id,'enrolled'=>1);
        $where_con = array('current_contacts.employee_id' => $employee_id);
        $where_emr = array('emergency_contacts.employee_id' => $employee_id);
        $where_q = array('qualification.employee_id' => $employee_id);
        //$where_exp = array('emp_experience.employee_id' => $employee_id,'emp_experience.trashed'=>0);

        $where_emply = array('employment.employee_id' => $employee_id,'employment.current'=>1);

        $where_d = array('dependents.employee_id' => $employee_id);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $where_ay = array('employment.employment_id' => $data['empl']->employment_id);

        //$data['complete_profile'] = $this->hr_model->view_complete_profile($where);

        $data['personal_info'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['gender_etc'] = $this->hr_model->join_for_gender_etc($where);
        $data['cont'] = $this->hr_model->get_row_by_where('current_contacts', $where_con);
        $data['contact'] = $this->hr_model->join_for_contact($where_con);

        $data['emr'] = $this->hr_model->get_row_by_where('emergency_contacts', $where_emr);
        $data['jn_emr'] = $this->hr_model->join_for_rel($where_emr);
        $data['qua'] = $this->hr_model->get_row_by_where('qualification', $where_q);
        // $data['exp'] = $this->hr_model->get_all_by_where('emp_experience', $where_exp);
        $where_exp = array('emp_experience.employee_id' => $employee_id, 'emp_experience.trashed' => 0);
        $data['exp'] = $this->hr_model->emp_experience_detail($where_exp);

        $where_exp2 = array('emp_experience.employee_id' => $employee_id,'employment.current'=>1);
        $data['jn_exp'] = $this->hr_model->join_for_exp($where_exp2);
        $where_emp = array('emp_skills.employment_id' =>  $employment_id,'emp_skills.trashed'=>0);
        $data['skill'] = $this->hr_model->emp_skill_detail($where_emp);

        $data['jn_skl'] = $this->hr_model->join_for_skl($where_emp);
        $data['emply'] = $this->hr_model->get_row_by_where('employment', $where_emply);

        $where_p = array('position_management.employement_id' =>  $data['emply']->employment_id);
        $data['emplyeee'] = $this->hr_model->detl_employeeee($where_p);
        //echo "<pre>";
        // print_r($data['emplyeee']); die;
        $data['depend'] = $this->hr_model->join_for_depend($where_d);
        $data['pay'] = $this->hr_model->join_for_pay($where_ay);
        $data['last'] = $this->hr_model->join_for_last($where_ay);
//        print_r($data['depend']);die;

        //echo "<pre>"; print_r( $data['last']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_employee_profile', $data);
        $this->load->view('includes/footer');
    }

    public function emp_profile_print_report($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $employment_id=get_employment_from_employeeID($employee_id);
        $where = array('employee_id' => $employee_id,'enrolled'=>1);
        $where_con = array('current_contacts.employee_id' => $employee_id);
        $where_emr = array('emergency_contacts.employee_id' => $employee_id);
        $where_q = array('qualification.employee_id' => $employee_id);
        //$where_exp = array('emp_experience.employee_id' => $employee_id,'emp_experience.trashed'=>0);

        $where_emply = array('employment.employee_id' => $employee_id,'employment.current'=>1);

        $where_d = array('dependents.employee_id' => $employee_id);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $where_ay = array('employment.employment_id' => $data['empl']->employment_id);

        //$data['complete_profile'] = $this->hr_model->view_complete_profile($where);

        $data['personal_info'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['gender_etc'] = $this->hr_model->join_for_gender_etc($where);
        $data['cont'] = $this->hr_model->get_row_by_where('current_contacts', $where_con);
        $data['contact'] = $this->hr_model->join_for_contact($where_con);

        $data['emr'] = $this->hr_model->get_row_by_where('emergency_contacts', $where_emr);
        $data['jn_emr'] = $this->hr_model->join_for_rel($where_emr);
        $data['qua'] = $this->hr_model->get_row_by_where('qualification', $where_q);
        // $data['exp'] = $this->hr_model->get_all_by_where('emp_experience', $where_exp);
        $where_exp = array('emp_experience.employee_id' => $employee_id, 'emp_experience.trashed' => 0);
        $data['exp'] = $this->hr_model->emp_experience_detail($where_exp);

        $where_exp2 = array('emp_experience.employee_id' => $employee_id,'employment.current'=>1);
        $data['jn_exp'] = $this->hr_model->join_for_exp($where_exp2);
        $where_emp = array('emp_skills.employment_id' =>  $data['empl']->employment_id);

        $where_emp = array('emp_skills.employment_id' =>  $employment_id,'emp_skills.trashed'=>0);
        $data['skill'] = $this->hr_model->emp_skill_detail($where_emp);
        $data['jn_skl'] = $this->hr_model->join_for_skl($where_emp);
        $data['emply'] = $this->hr_model->get_row_by_where('employment', $where_emply);

        $where_p = array('position_management.employement_id' =>  $data['emply']->employment_id);
        $data['emplyeee'] = $this->hr_model->detl_employeeee($where_p);
        //echo "<pre>";
        // print_r($data['emplyeee']); die;
        $data['depend'] = $this->hr_model->join_for_depend($where_d);
        $data['pay'] = $this->hr_model->join_for_pay($where_ay);
        $data['last'] = $this->hr_model->join_for_last($where_ay);
        /* print_r($data['personal_info'] &&  $data['gender_etc'] &&  $data['cont'] &&  $data['contact'] &&  $data['emr']
         && $data['jn_emr'] && $data['qua'] && $data['exp'] && $data['jn_exp'] && $data['skill'] && $data['jn_skl']
         && $data['emply'] && $data['emplyeee'] && $data['depend'] && $data['pay'] && $data['last']);*/
        $this->load->view('HR/Reports/emp_profile_print_report', $data);
    }

    /// Function For Job Assignment / Placement
    public function view_job_assignment($employee_id = NULL)
    {

//        if ($employee_id == "list") {
//            echo $this->hr_model->hr_task_mgt();
//            die;
//        }
        if ($employee_id == "list") {
            //Old Deprecated Method
            /*            echo $this->hr_model->view_postion_management();
                        die;*/
            $designation = $this->input->post('designation');
            $table = "employee E";
            $selectData = array(
                'E.employee_code AS EmployeeCode,
                 E.full_name AS EmployeeName,
                 MLDg.designation_name AS Designation,
                 E.employee_id AS EmployeeID,
                 ET.employment_id,
                 ',


                false
            );

            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'ET.current' => 1,
                'PM.current' => 1
            );
            $groupBy = "E.employee_id";
            if(!empty($designation) && $designation !== 'null')
            {
                $where['PM.ml_designation_id'] = $designation;
            }
            if($task = $this->input->post('status'))
            {
                $where['PM.status']=$task;
            }


            $edit_column = array(
                array(
                    'Action','<a href="human_resource/view_task_history/$1"><span class="fa fa-eye"></span></a>','EmployeeID'
                ),
                array(
                    'editAction','<a href="human_resource/assign_job/$1"><button class="btn green" style="font-size: 12px;">Assign Task</button></a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="human_resource/task_progress/$1"><button class="btn green" style="font-size: 12px;">Task Progress</button></a>','EmployeeID'
                )
            );

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$groupBy,'',$edit_column);
            echo $result;
            return;
        }
        $where2 = array('trashed' => 0);
        $data['employee_id'] = $this->uri->segment(3);
        $data['slct_design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --', 'ml_designation_id', 'designation_name',$where2);
        $data['status'] = $this->input->post('status');
        $data['status'] = $this->hr_model->dropdown_wd_option('ml_status', '-- Select Status --', 'status_id', 'status_title',$where2);
        //$data['slct_task'] = $this->input->post('task');
        //$data['task_name'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name');

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/job_assignment', $data);
        $this->load->view('includes/footer');
    }

    /// Function To View Job Assignment / Placement History
    public function view_task_history($employee_id = NULL)
    {

        $where_emp = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_emp);

        $data['task_history'] = $this->hr_model->view_task_history(array('employee.employee_id' => $employee_id));

        $data['employee_id'] = $this->uri->segment(3);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_task_history', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Assign Job to Employee
    public function assign_job($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $data['employment']= $this->hr_model->get_row_by_id('employment',array('employee_id'=>$data['employee_id'],'current'=> 1,'trashed'=> 0));
        $employment_id=$data['employment']->employment_id;
        $whereEmployment = array(
            'employment_id' => $employment_id,
            'employee_project.trashed' => 0
        );
        $data['projects'] = $this->hr_model->join_for_project($whereEmployment);

//       var_dump($data['projects']);die;



        if(!isset($data['projects']) || empty($data['projects'])){
            $msg = 'To Assign Job, This Employee Must Have Projects Assigned To him/her::error';
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
            return;
        }

        /*        $where_task = array(
                    'project_id'=> $data['projects']->project_id,
                    'trashed' => 0);*/
        $projectIDs = implode(',',array_column(json_decode(json_encode($data['projects']),true),'project_id'));
//        var_dump($projectIDs);
        $where_task = 'project_id IN ('.$projectIDs.') AND trashed = 0';
        //Above Where Condition is Not OK, It would Have Been Ok if there would be only 1 project ID
        //$data['projects'] = $this->hr_model->dropdown_wd_option('ml_projects', '-- Select Project --', 'project_id', 'project_title');
        $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where_task);
        $data['kpi'] = $this->hr_model->dropdown_wd_option('ml_kpi', '-- Select Option --', 'ml_kpi_id', 'kpi',$where_task);

        $where = array('assign_job.employment_id' => $employment_id);
        $data['project_details'] = $this->hr_model->assign_job_perf_evalue($where);

        $where_h = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_h);

        $where_ajh = array('assign_job.employment_id' => $employment_id);
        $data['job_history_wd'] = $this->hr_model->assign_job_history_wd($where_ajh);

        if ($this->input->post('continue')) {

            $where_check_task=array(
                'employment_id'=>$employment_id,
                'project_id'=>$this->input->post('project_id'),
                'ml_assign_task_id'=>$this->input->post('ml_assign_task_id'));
            $check_task_rec=$this->hr_model->get_row_by_where('assign_job',$where_check_task);
            if(count($check_task_rec) > 0)
            {  $this->session->set_flashdata('warning','<span style="color: #ff0000">Sorry this Task is already assigned !</span>');
                redirect('human_resource/assign_job/' . $employee_id);
                return;
            }
            else{
                $milestone1=$this->input->post('milestones');
                if(!empty($milestone1)){
                    $new_array=array();
                    $estimate_date = $this->input->post('estimate_date');
                    $actual_date = $this->input->post('actual_date');
                    $milestones = $this->input->post('milestones');
                    $remarks = $this->input->post('remarks');
                    $employment_id=$this->input->post('employment');
                    $task_id=$this->input->post('ml_assign_task_id');


                    //Need Temporary Variables..
                    $postedEstimate_date = $estimate_date;
                    $postedActual_date = $actual_date;

                    foreach($milestones as $mKey=>$milestone)
                    {

                        $new_array[$mKey] = array();
                        if(isset($postedEstimate_date) && !empty($postedEstimate_date))
                        {
                            $estimate_date = date('Y-m-d',strtotime($postedEstimate_date[$mKey]));
                        }

                        if(isset($actual_date) && ($actual_date))
                        {
                            $actual_date = date('Y-m-d',strtotime($postedActual_date[$mKey]));
                        }
                        $subArrayValues = array(
                            'milestones' => $milestone,
                            'estimated_date' => $estimate_date,
                            'actual_date' => $actual_date,
                            'remarks' => $remarks[$mKey],
                            'employment_id' => $employment_id,
                            'task_id' => $task_id

                        );
                        $new_array[$mKey] = array_merge($new_array[$mKey],$subArrayValues);
                    }




                    $this->db->trans_start();
                    foreach($new_array as $data)
                    {
                        $this->hr_model->create_new_record('milestone',$data);
                    }
                }

                $StartDate = $this->input->post('start_date');
                if(isset($StartDate) && !empty($StartDate))
                {
                    $StartDate = date('Y-m-d', strtotime($StartDate));
                }

                $CompletionDate = $this->input->post('completion_date');
                if(isset($CompletionDate) && !empty($CompletionDate))
                {
                    $CompletionDate = date('Y-m-d',strtotime($CompletionDate));
                }


                $insert_job = array(
                    'employment_id' => $employment_id,
                    'project_id' => $this->input->post('project_id'),
                    'ml_assign_task_id' => $this->input->post('ml_assign_task_id'),
                    'start_date' => $StartDate,
                    'completion_date' => $CompletionDate,
                    'task_description' => $this->input->post('task_description'),
                    'status' => 'Assigned'//$this->input->post('status'),
                );
                $query_job = $this->hr_model->create_new_record('assign_job', $insert_job);
                $last_id = $this->db->insert_id();

                $kpi_counter = 0;
                $key_performance_ind = $this->input->post('ml_kpi_type_id');
                foreach ($key_performance_ind as $kpi) {
                    $insert_perf_evalue = array(
                        'job_assignment_id' => $last_id,
                        'ml_kpi_type_id' => $kpi,
                    );
                    $query_perf_evalue = $this->hr_model->create_new_record('performance_evaluation', $insert_perf_evalue);
                    $kpi_counter++;
                }

                $this->db->trans_complete();
                if($this->db->trans_status()=== FALSE)
                {
                    $this->db->trans_rollback();
                }
                else
                {
                    $this->db->trans_commit();
                }

                if ($query_job || $query_perf_evalue) {
                    redirect('human_resource/assign_job/' . $employee_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/assign_job', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Assign Job to Employee
    public function edit_assign_job($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $employeeID = $this->uri->segment(3);
        $assignJobID = $this->uri->segment(4);
        $data['employment']= $this->hr_model->get_row_by_id('employment',array('employee_id'=>$data['employee_id'],'current'=> 1,'trashed'=> 0));
        //$employment_id=$data['employment']->employment_id;
        $employment_id=get_employment_from_employeeID($employeeID);
        $where = array('employee_id'=>$employeeID);
        $whereEmployment = array(
            'employment_id' => $employment_id,
            'employee_project.trashed' => 0
        );
        $data['projects'] = $this->hr_model->join_for_project($whereEmployment);
        // print_r($data['projects']);die;
        if(!isset($data['projects']) || empty($data['projects'])){
            $msg = 'To Assign Job, This Employee Must Have Projects Assigned To him/her::error';
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
            return;
        }

        $where2 = array(
            'trashed' => 0
        );
        /*        $where_task = array(
                    'project_id'=> $data['projects']->project_id,
                    'trashed' => 0);*/
        $projectIDs = implode(',',array_column(json_decode(json_encode($data['projects']),true),'project_id'));
//        var_dump($projectIDs);
        $where_task = 'project_id IN ('.$projectIDs.') AND trashed = 0';
        $where_project = 'trashed = 0';
        //Above Where Condition is Not OK, It would Have Been Ok if there would be only 1 project ID
        //$data['projects'] = $this->hr_model->dropdown_wd_option('ml_projects', '-- Select Project --', 'project_id', 'project_title');
        $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where_task);
        $data['kpi'] = $this->hr_model->dropdown_wd_option('ml_kpi', '-- Select Option --', 'ml_kpi_id', 'kpi',$where_task);
        $data['project'] = $this->hr_model->dropdown_wd_option('ml_projects', '-- Select Project --', 'project_id', 'project_title',$where_project);
        $where = array('assign_job.employment_id' => $employment_id,'assign_job.assign_job_id' =>$assignJobID);
        $data['edit_aj'] = $this->hr_model->assign_job_edit($where);
        // echo "<pre>";
        // print_r($data['edit_aj']);die;
        $where_h = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_h);

        $where_ajh = array('assign_job.employment_id' => $employment_id);
        $data['job_history_wd'] = $this->hr_model->assign_job_history_wd($where_ajh);

        if ($this->input->post('update')) {
            $this->db->trans_start();
            $where_check_task=array(
                'employment_id'=>$employment_id,
                'project_id'=>$this->input->post('project_id'),
                'ml_assign_task_id'=>$this->input->post('ml_assign_task_id'));
            $check_task_rec=$this->hr_model->get_row_by_where('assign_job',$where_check_task);
            if(count($check_task_rec) > 0)
            {  $this->session->set_flashdata('warning','<span style="color: #ff0000">Sorry this Task is already assigned !</span>');
                redirect('human_resource/assign_job/' . $employee_id);
                return;
            }
            else{
                /*$milestone1=$this->input->post('milestones');
                if(!empty($milestone1)){
                    $new_array=array();
                    $estimate_date = $this->input->post('estimate_date');
                    $actual_date = $this->input->post('actual_date');
                    $milestones = $this->input->post('milestones');
                    $remarks = $this->input->post('remarks');
                    $employment_id=$this->input->post('employment');
                    $task_id=$this->input->post('ml_assign_task_id');


                    foreach($milestones as $mKey=>$milestone)
                    {

                        $new_array[$mKey] = array();
                        if(isset($estimate_date) && !empty($estimate_date))
                        {
                            $estimate_date = date('Y-m-d',strtotime($estimate_date[$mKey]));

                        }

                        if(isset($actual_date) && ($actual_date))
                        {
                            $actual_date = date('Y-m-d',strtotime($actual_date[$mKey]));
                        }
                        $subArrayValues = array(
                            'milestones' => $milestone,
                            'estimated_date' => $estimate_date,
                            'actual_date' => $actual_date,
                            'remarks' => $remarks[$mKey],
                            'employment_id' => $employment_id,
                            'task_id' => $task_id

                        );
                        $new_array[$mKey] = array_merge($new_array[$mKey],$subArrayValues);


                    }

                    foreach($new_array as $data)
                    {
                        $this->hr_model->create_new_record('milestone',$data);
                    }
                }*/

                $StartDate = $this->input->post('start_date');
                if(isset($StartDate) && !empty($StartDate))
                {
                    $StartDate = date('Y-m-d', strtotime($StartDate));
                }

                $CompletionDate = $this->input->post('completion_date');
                if(isset($CompletionDate) && !empty($CompletionDate))
                {
                    $CompletionDate = date('Y-m-d',strtotime($CompletionDate));
                }


                $insert_job = array(
                    'project_id' => $this->input->post('project_id'),
                    'ml_assign_task_id' => $this->input->post('ml_assign_task_id'),
                    'start_date' => $StartDate,
                    'completion_date' => $CompletionDate,
                    'task_description' => $this->input->post('task_description')
                );
                $where_assignJob=array('assign_job_id'=>$assignJobID);
                $query_job = $this->hr_model->update_record('assign_job',$where_assignJob,$insert_job);
                //$last_id = $assignJobID;
                /* $where_assignJobKpi=array('assign_job_id'=>$assignJobID);*/
                /*  $kpi_counter = 0;
                  $key_performance_ind = $this->input->post('ml_kpi_type_id');
                  foreach ($key_performance_ind as $kpi) {
                      $insert_perf_evalue = array(
                          'ml_kpi_type_id' => $kpi
                      );
                      $where_assignJobKpi=array('job_assignment_id'=>$assignJobID);
                      $query_perf_evalue = $this->hr_model->update_record('performance_evaluation',$where_assignJobKpi,$insert_perf_evalue);
                      $kpi_counter++;
                  }*/

                $this->db->trans_complete();
                if($this->db->trans_status()=== FALSE)
                {
                    $this->db->trans_rollback();
                }
                else
                {
                    $this->db->trans_commit();
                }

                /*if ($query_job || $query_perf_evalue) {*/
                if ($query_job) {
                    $this->session->set_flashdata('update',"OK:: Record update successfully !::success");
                    redirect('human_resource/assign_job/' . $employee_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/edit_assign_job', $data);
        $this->load->view('includes/footer');
    }

    //// Function for Task Progress
    public function task_progress()
    {
        $employee_id=$this->uri->segment(3);
        $where=array('employee.employee_id'=>$employee_id);
        $data['employee_info']=$this->hr_model->employee_info($where);
        $data['tasks']=$this->hr_model->get_all('ml_assign_task');
        $task=$this->input->post('task');
        $data['task_info']=$this->hr_model->tasks_info($where,$task);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/task_progress',$data);
        $this->load->view('includes/footer');
    }
    ////////// Function for task Populate

    /////////// Function for task progress
    public function task_eval()
    {
        $employee_id=$this->uri->segment(3);
        $milestone_id=$this->uri->segment(4);
        $segmentFive = $this->uri->segment(5);
        /*Uri Segment Five Containing task Progress Id*/
        $data['taskProgressId']= $this->uri->segment(5);


        /*
         * Let Take Out Taks Progress In Percent And Remarks
         * */
        $whreProgress=array('tp_id'=>$segmentFive);
        //print_r($whreProgress);die;
        $data['TaskProgressInfo']=$this->hr_model->select_task_progress_where('task_progress',$whreProgress);
        //print_r($data['TaskProgressInfo']);die;
        $where=array(
            'employee.employee_id'=>$employee_id);
        $where_milestone=array(
            'milestone.id'=>$milestone_id,
            'employee.employee_id'=>$employee_id);
        $data['employee_info']=$this->hr_model->employee_info($where);
        $data['task_info']=$this->hr_model->tasks_info_where($where_milestone);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/task_eval',$data);
        $this->load->view('includes/footer');
    }
    ///////////End
    ////////////////Function Task Progress Report
    public function task_progress_report()
    {
        $percentage = $this->input->post('percent');

        //Little Percentage Validation To Check If its A Numeric Value or Not
        if (!is_numeric($percentage) || empty($percentage) || $percentage > 100 || $percentage < 0) {
            $msg = 'Please Enter Valid Percentage::error';
            $this->session->set_flashdata('msg', $msg);
            redirect(previousURL());
        }

        $remarks = $this->input->post('remarks');
        $milestone_id = $this->input->post('milestone_id');
        $employee_id = $this->input->post('employee_id');
        $TaksProgress_id = $this->input->post('taskID');


        $where = array('tp_id' => $TaksProgress_id);

        $data = array(
            'milestone_id' => $milestone_id,
            'reported_date' => date("Y-m-d"),
            'reported_by' => $this->session->userdata('employee_id'),
            'percent_achieved' => $percentage,
            'remarks' => $remarks
        );
        if (!empty($TaksProgress_id)) {
            //$query=$this->hr_model->create_new_record('task_progress',$data);
            $query = $this->hr_model->update_record('task_progress', $where, $data);
        }
        else {
            $query=$this->hr_model->create_new_record('task_progress',$data);
        }
        if($query)
        {redirect('human_resource/task_progress/'.$employee_id);}
    }
    ////////////////
    public function task_populate()
    {
        $project_id=$this->input->post('project_id');
        $data=$this->hr_model->get_tasks_where_project('ml_assign_task',array('project_id'=>$project_id,'trashed'=>0));
        echo json_encode($data);

    }
    ///////////////////////
    ////////////////
    public function kpi_populate()
    {
        $project_id=$this->input->post('project_id');
        $data=$this->hr_model->get_kpi_where_project('ml_kpi',array('project_id'=>$project_id,'trashed'=>0));
        echo json_encode($data);
        return;

    }
    ///////////////////////


    /////////////// Add task
    public function add_task()
    {
        if ($this->input->post('tasks')) {
            $task=$this->input->post('tasks');
            $project=$this->input->post('project_id');
            $where=array(
                'task_name'=>$task,
                'project_id'=>$project);
            $rec = $this->site_model->get_row_by_id('ml_assign_task',$where);
            //echo"<pre>";print_r($rec);die;
            if(count($rec) > 0)
            {
                echo "FAIL::Record Already Exist::error";
                return;
            }
            else{

                //Data to enter in to the database. .
                $table = 'ml_assign_task';
                $data = array(
                    'task_name' => $task,
                    'project_id' => $project
                );
                $insertedID = $this->common_model->insert_record($table, $data);
                if ($insertedID > 0) {
                    echo "OK::Record Successfully Inserted::success::" . $insertedID;
                    return;

                } else {
                    echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                    return;
                }
            }
        }
    }
    /////end

    public function project_tasks()
    {
        $where2 = array('trashed' => 0);
        $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where2);
    }

    public function delete_assign_job($employee_id = NULL, $assign_id = NULL)
    {
        $where = "assign_job_id =" . $assign_id;
        $query_delete = $this->hr_model->delete_by_join($where);

        if ($query_delete) {
            redirect('human_resource/assign_job/' . $employee_id);
        }
    }

    /// Function For Performance Evaluation

    public function view_performance($parameter = NULL)
    {

        if ($parameter == "list") {
            echo $this->hr_model->view_performance();
            die;
        }
        $where2 = array('trashed' => 0);
        $data['employee_id'] = $this->session->userdata('employee_id');
        $data['slct_dept'] = $this->input->post('department');
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --',
            'department_id', 'department_name',$where2);

        $data['design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $data['employee_id'] = $this->uri->segment(3);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_performance', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Performance Evaluation
    public function performance_evaluation($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
//        if ($employee_id == "list") {
//            echo $this->hr_model->view_performance_evalution();
//            die;
//        }
        if ($employee_id == "list") {
            //Old Deprecated Method
            /*            echo $this->hr_model->view_postion_management();
                        die;*/
            $designation = $this->input->post('designation');
            $table = "employee E";

            $selectData = array(
                'E.employee_code AS EmployeeCode,
                 E.full_name AS EmployeeName,
                 MLDg.designation_name AS Designation,
                 D.department_name,
                 E.employee_id AS EmployeeID,
                 ET.employment_id AS EmploymentID,
                 ',


                false
            );

            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'posting P',
                    'condition' => 'P.employement_id = ET.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_department D',
                    'condition' => 'P.ml_department_id = D.department_id',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'ET.current' => 1,
                'E.enrolled' => 1
            );
            $groupBy = "E.employee_id";
            if($department = $this->input->post('department'))
            {
                $where['P.ml_department_id']=$department;
            }
            if($design = $this->input->post('designation'))
            {
                $where['PM.ml_designation_id']=$design;
            }

            $edit_column = array(
                array(
                    'Action','<a href="human_resource/view_kpi_perf_eval/$1"><span class="fa fa-eye"></span></a>','EmployeeID'
                )
            );

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$groupBy,'',$edit_column);
            echo $result;
            return;
        }
        $where2 = array('trashed' => 0);
        $data['slct_dept'] = $this->input->post('department');
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id', 'department_name',$where2);

        $data['slct_design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --', 'ml_designation_id', 'designation_name',$where2);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/performance_evaluation', $data);
        $this->load->view('includes/footer');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function annual_review($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        if ($employee_id == "list")
        {
            echo $this->hr_model->annual_review();
            die;
        }
        $where2 = array('trashed' => 0);
        $data['slct_dept'] = $this->input->post('department');
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id', 'department_name',$where2);

        $data['slct_design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --', 'ml_designation_id', 'designation_name',$where2);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/annual_review', $data);
        $this->load->view('includes/footer');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    public function annual_history()
    {
        $id = $this->uri->segment(3);
        $data['full'] = $this->hr_model->get_row_by_where('feedback', array('feedback.employer' => $id));
        $data['exp'] = $this->hr_model->get_row_by_where('employee', array('employee.employee_id' => $data['full']->employee_id));
        $data['view'] = $this->hr_model->get_row_by_where('employee', array('employee.employee_id' => $data['full']->employer));
        $data['name'] = $this->hr_model->detail($id);
        //echo"<pre>";print_r($data['exp']);die;
        //$data['name'] = $this->hr_model->view_progress($id);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/histry_annual', $data);
        $this->load->view('includes/footer');
    }

    public function history_annual($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where2 = array('trashed' => 0);
        $data['slctd_task'] = $this->input->post('tasks');
        $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where2);

        $data['kpi_emp_h'] = $this->hr_model->view_kpi_history_pe(array('employee.employee_id' => $this->uri->segment(3)));
        $year = $this->input->post('year');
        $data['kpi_eval'] = $this->hr_model->view_annual_review(array('feedback.employer' => $this->uri->segment(3)),$year);
        //echo"<pre>";print_r($data['kpi_eval']);die;

        $data['year'] = $this->hr_model->dropdown_wd_option('ml_year', '-- Select Year --', 'year', 'year',$where2);
        $year = $this->input->post('year');
        // $s_y= data("Y",strtotime($year));
        //$where=  array('ml_year.year'=>$s_y);
        //$se = $this->hr_model->get_row_by_id('ml_year',$where);
        //$rr = $se->ml_year_id;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/history_annual', $data);
        $this->load->view('includes/footer');
    }

    /// Function To View Single Employee KPI's Performance Evaluation
    public function view_kpi_perf_eval($employee_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $data['employeee_id']=$this->uri->segment(3);
        //echo "employeeID ".$employee_id;

        $employment_id = get_employment_from_employeeID($employee_id);

        // echo "<br />Employment ".$employment_id;
        // return;

        $where2 = array('trashed' => 0);
        $whereEmployment = array(
            'employment_id' => $employment_id,
            'employee_project.trashed' => 0
        );
        $data['projects'] = $this->hr_model->join_for_project($whereEmployment);
        $data['slctd_task'] = $this->input->post('tasks');
        $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where2);
        //$data['projects'] = $this->hr_model->dropdown_wd_option('ml_projects', '-- Select Project --', 'project_id', 'project_title',$where_employment);
        //$data['empl'] = $this->hr_model->get_row_by_where('employment',array('employment_id' => $employee_id,'current'=> 1,'trashed'=>0));
        // print_r($data['empl']);die;
        //$employmentID = $data['empl']->employment_id;
        $data['kpi_emp_h'] = $this->hr_model->view_kpi_history_pe(array('employment.employment_id' => $employment_id));
        $data['kpi_eval'] = $this->hr_model->view_kpi_performance(array('assign_job.employment_id' => $employment_id));

        $data['slctd_project'] = $this->input->post('project');
        //$data['year'] = $this->hr_model->dropdown_wd_option('ml_year', '-- Select Year --', 'year', 'year',$where2);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_perform_evaluation', $data);
        $this->load->view('includes/footer');
    }

    public function print_perform($employment_id = NULL,$job_assignment_id = NULL)
    {
        if($employment_id !== NULL && $employment_id > 0){
            //$data['employee_id'] = $employee_id;
            $where2 = array('trashed' => 0);
            $data['slctd_task'] = $this->input->post('tasks');
            $data['tasks'] = $this->hr_model->dropdown_wd_option('ml_assign_task', '-- Select Task --', 'ml_assign_task_id', 'task_name',$where2);
            $data['kpi_emp_h'] = $this->hr_model->view_kpi_history_pe(array('employment.employment_id' => $employment_id));
            $data['kpi_eval'] = $this->hr_model->view_kpi_performance_print(array('employment.employment_id' => $employment_id,'assign_job.assign_job_id' => $job_assignment_id));
            //echo"<pre>";print_r($data['kpi_eval']);die;
            $data['slctd_year'] = $this->input->post('year');
            $data['year'] = $this->hr_model->dropdown_wd_option('ml_year', '-- Select Year --', 'year', 'year',$where2);
            $data['kpi_select'] = $this->hr_model->data_for_span_to_print( array('job_assignment_id' => $job_assignment_id));
            $this->load->view('HR/Reports/print_perform', $data);
        }
    }


    /////////////////////////////////////////////////////////////////
    public function progress_report($employee_id = NULL)
    {
        if ($this->input->post('continue')) {

            $SubmitDate = $this->input->post('submit_date');
            if(isset($SubmitDate) && !empty($SubmitDate))
            {
                $SubmitDate = date('Y-m-d', strtotime($SubmitDate)) ;
            }

            $insert = array(
                'employee_id' => $this->session->userdata('employee_id'),
                'employer' => $this->input->post('employer'),
                'reviewer' => $this->input->post('reviewer'),
                'comments' => $this->input->post('comments'),
                'submit_date' => $SubmitDate
            );
            $this->site_model->create_new_record('feedback', $insert);

            //echo"<pre>"; print_r($insert); die;
        }
        $id = $this->uri->segment(3);
        $data['full'] = $this->hr_model->get_row_by_where('employee', array('employee.employee_id' => $id));
        $data['name'] = $this->hr_model->view_progress($id);
        //echo"<pre>";print_r($data['name']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/progress_reports', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Add Performance
    public function add_performance_evalution($employment_id = NULL, $job_assignment_id = NULL, $performance_id = NULL)
    {

        $employmentID  = $this->uri->segment(3);
        $data['kpi_rec'] = $this->hr_model->get_row_by_id('performance_evaluation', array('performance_evaluation_id' => $performance_id));
        $data['kpi_select'] = $this->hr_model->get_all_by_where('performance_evaluation', array('job_assignment_id' => $job_assignment_id));
        //echo "<pre>"; print_r($data['kpi_select']);die;
        //$data['empl'] = $this->hr_model->get_row_by_where('employment',array('employment_id' => $employment_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        //$employmentID = $data['empl']->employment_id;
        $data['kpi_achive'] = $this->hr_model->view_kpi_history_pe(array('employment.employment_id' => $employmentID));
        $data['assignd_kpi'] = $this->hr_model->view_kpi_history_pe_wd(array('assign_job.employment_id' => $employmentID));
        $where2 = array('trashed' => 0);
        $data['kpi'] = $this->hr_model->dropdown_wd_option('ml_kpi', '-- Select Key Performance Indicator --', 'ml_kpi_id', 'kpi',$where2);
        if ($this->input->post('add_performance')) {
            $ml_kpi_type = $this->input->post('kpi');
            $countr = 0;

            $dateevaluated = $this->input->post('date_evaluated');



            foreach ($ml_kpi_type as $kpi_type) {
                $percent = $this->input->post('percent_achieved');
                $evaluated = $this->input->post('emp_id');
//                $eval_date = $dateevaluated;
                if(isset($dateevaluated) && !empty($dateevaluated)){
                    $eval_date = date('Y-m-d',strtotime($dateevaluated[$countr]));
                }
                $remarks = $this->input->post('remarks');

                $perf_id = $this->input->post('performance_id');

                $insert_kpi = array(
                    'ml_kpi_type_id' => $kpi_type,
                    'percent_achieved' => $percent[$countr],
                    'evaluated_by' => $evaluated,
                    'remarks' => $remarks[$countr],
                );
                isset($eval_date)?$insert_kpi['date'] = $eval_date:'';
                $query_kpi = $this->hr_model->update_record('performance_evaluation', array('performance_evaluation_id' => $perf_id[$countr]), $insert_kpi);
                $countr++;
            }
            if ($query_kpi) {
                //redirect('human_resource/view_kpi_perf_eval/'.$this->uri->segment(3));
                redirect('human_resource/view_kpi_perf_eval/'.$this->input->post('emp_id'));
            }
        }
        $where = array('assign_job.employment_id' => $employmentID);
        $data['task'] = $this->hr_model->task_view_for($where);
        //echo"<pre>";print_r($data['task']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/performance', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Benefit Management
    public function benifit_management($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        if ($employee_id == "list") {
            echo $this->hr_model->view_benefit_mgt();
            die;
        }
        $where2 = array('trashed' => 0);
        $data['slct_design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --', 'ml_designation_id', 'designation_name',$where2);

        $data['slct_benefit'] = $this->input->post('benefit');
        $data['benefit'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id', 'benefit_type_title',$where2);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/benifits_management', $data);
        $this->load->view('includes/footer');
    }

    public function add_benefits($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where2 = array('trashed' => 0);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id', 'benefit_type_title',$where2);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $emp_id = $this->input->post('emp_id');

        $data['benefit_detail'] =   $this->hr_model->view_benefit(array('benefits.employment_id' => $data['empl']->employment_id));
        $data['depend_detail'] =    $this->hr_model->view_dependent(array('benefits.employment_id' => $data['empl']->employment_id));
        $data['assignd_benft'] =    $this->hr_model->view_assignd_benefit(array('benefits.employment_id' => $data['empl']->employment_id));
        $data['assignd_benft_wd'] = $this->hr_model->view_assignd_benefit_wd(array('benefits.employment_id' => $data['empl']->employment_id));

        if ($this->input->post('continue')) {
            $insert_benefit = array(
                'employment_id' => $data['empl']->employment_id,
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                'date_effective' => $this->input->post('date_effective'),
                'created_by' => $this->session->userdata('employee_id'),
                'date_rec_created' => date('Y-m-d'),
                'status' => 1
            );
            $query_benefit = $this->hr_model->create_new_record('benefits', $insert_benefit);
            if ($query_benefit) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $where_increm = array('increments.employment_id' => $data['empl']->employment_id, 'increments.trashed' => 0);
        $where_allow = array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $where_benef = array('benefits.employment_id' => $data['empl']->employment_id, 'benefits.trashed' => 0);
        $where_leave = array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);

        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);
        //echo "<pre>";print_r($data['increment_details']);die;
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['in'] = $this->hr_model->get_row_by_where('increments', $where_increm);
        $data['al'] = $this->hr_model->get_row_by_where('allowance', $where_allow);
        $data['be'] = $this->hr_model->get_row_by_where('benefits', $where_benef);
        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);

        $data['increment'] = @$this->hr_model->get_row_by_where('increments', array('increment_id' => $data['in']->increment_id));
        $data['allowance'] = $this->hr_model->get_row_by_where('allowance', array('allowance_id' => $data['al']->allowance_id));
        $data['benefit'] = $this->hr_model->get_row_by_where('benefits', array('emp_benefit_id' => $data['be']->emp_benefit_id));
        @$data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $data['le']->leave_entitlement_id));

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/add_benifits', $data);
        $this->load->view('includes/footer');
    }

    public function add_insurance($employee_id = NULL, $dependent_id = NULL)
    {
//        error_reporting(0);
        $data['employee_id'] = $this->uri->segment(3);
        $data['dependents'] = $this->hr_model->get_row_by_where('dependents', array('dependent_id' => $dependent_id));
        //echo "<pre>";print_r($data['dependents']);die;
        if ($data['dependents']->insurance_cover == 0) {
            $insert_dependent = array(
                'insurance_cover' => 1
            );
            $query_dependent = $this->hr_model->update_record('dependents', array('dependent_id' => $dependent_id), $insert_dependent);
            if ($query_dependent) {
                redirect('human_resource/add_benefits/' . $employee_id);
            }
        } else {
            $insert_dependent = array(
                'insurance_cover' => 0
            );
            $query_dependent = $this->hr_model->update_record('dependents', array('dependent_id' => $dependent_id), $insert_dependent);
            if ($query_dependent) {
                redirect('human_resource/add_benefits/' . $employee_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/add_insurance', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Employee Transfer
    public function view_employee_transfer($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where2 = array('trashed' => 0);

        $data['slctd_dept'] = $this->input->post('department');
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id',
            'department_name',$where2);

        $data['slctd_deign'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $data['slctd_loc'] = $this->input->post('location');
        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --',
            'posting_locations_list_id', 'posting_location',$where2);

        if ($employee_id == "list") {
            echo $this->hr_model->view_emp_transfer();
            die;
        }

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/employee_transfer', $data);
        $this->load->view('includes/footer');
    }

    /// Function For Employee Transfer
    public function transfer($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where2 = array('trashed' => 0);
        $where_emp = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_emp);
        $data['employment_id'] = $this->hr_model->get_row_by_where('employment', array('employee_id' => $employee_id));

        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id',
            'department_name',$where2);

        $data['branch'] = $this->hr_model->dropdown_wd_option('ml_branch', '-- Select Branch --', 'branch_id', 'branch_name',$where2);

        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where2);

        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --',
            'posting_locations_list_id', 'posting_location',$where2);

        $data['work_shift'] = $this->hr_model->dropdown_wd_option('ml_shift', '-- Select Work Shift --', 'shift_id', 'shift_name',$where2);

        $data['reason_transfer'] = $this->hr_model->dropdown_wd_option('ml_posting_reason_list', '-- Select Reason --', 'posting_reason_list_id', 'posting_reason',$where2);
        //echo $employee_id; die;
        $data['trans_details'] = $this->hr_model->view_transfer_detail(array('employee.employee_id' => $employee_id));
        if($this->input->post("add_transfer")){
            $where =array(
                'employement_id'=>$data['employment_id']->employment_id,
                'status'=>1);
            $data['rec'] = $this->hr_model->get_last_id_desc_where('posting',$where,'posting_id');

            if(!empty($data['rec']->status))
            {
                $this->session->set_flashdata('msg','previous transfer is in pending !');
                redirect('human_resource/transfer/'.$employee_id);
                return;
            }
            else{
                $insert_transfer = array(
                    'employement_id' => $this->input->post('employment_id'),
                    'poting_start_date' => date('Y-m-d'),
                    'ml_city_id' => $this->input->post('ml_city_id'),
                    'ml_branch_id' => $this->input->post('ml_branch_id'),
                    'location' => $this->input->post('location'),
                    'shift' => $this->input->post('shift'),
                    'ml_department_id' => $this->input->post('ml_department_id'),
                    'reason_transfer' => $this->input->post('reason_transfer'),
                    'transfer_remarks' => $this->input->post('transfer_remarks'),
                    'duration' => $this->input->post('duration')
                );
                //print_r($insert_transfer);die;
                $query_transfer = $this->hr_model->create_new_record('posting', $insert_transfer);
                if($query_transfer)
                {
                    redirect('human_resource/transfer/'.$employee_id);
                }}}


        $this->load->view('includes/header');
        $this->load->view('HR/Reports/transfer', $data);
        $this->load->view('includes/footer');
    }

    /// Function To View Employee Transfer History
    public function transfer_history($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where_emp = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_emp);

        $data['emp_info'] = $this->hr_model->get_row_by_where('employee', array('employee_id' => $employee_id));
        $data['employment_id'] = $this->hr_model->get_row_by_where('employment', array('employee_id' => $employee_id));

        $data['trans_details'] = $this->hr_model->view_transfer_detail(array('employment.employment_id' => $data['employment_id']->employment_id));


        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_transfer_history', $data);
        $this->load->view('includes/footer');
    }

    //======================= Functions By Hamid Ali================================//
    public function view_position_management($parameter = NULL)
    {

        if ($parameter == "list") {
            //Old Deprecated Method
            /*            echo $this->hr_model->view_postion_management();
                        die;*/
            $designation = $this->input->post('designation');
            $table = "employee E";
            $selectData = array(
                'E.employee_code AS EmployeeCode,
                 E.full_name AS EmployeeName,
                 MLDg.designation_name AS Designation,
                 PM.job_specifications AS JobSpecification,
                 status_title AS Status,
                 E.employee_id AS EmployeeID',

                false
            );

            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'PM.status = MLS.status_id',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'ET.current' => 1,
                'E.enrolled' => 1
            );
            $groupBy = "E.employee_id";

            if(!empty($designation) && $designation !== 'null')
            {
                $where['PM.ml_designation_id'] = $designation;
            }

            $edit_column = array(
                array(
                    'Action','<a href="human_resource/assign_position/$1"><input type="submit" name="continue" value="Manage Position" class="btn green"></a>&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <a href="human_resource/view_position/$1"><span class="fa fa-eye"></span></a>','EmployeeID'
                )
            );

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$groupBy,'',$edit_column);
            echo $result;
            return;
        }
        $where2 = array('trashed' => 0);
        $data['employee_id'] = $this->session->userdata('employee_id');

        $data['title'] = $this->input->post('job_title');

        $data['design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $data['employee_id'] = $this->uri->segment(3);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/position_management', $data);
        $this->load->view('includes/footer');
    }

    public function assign_position($employee_id = NULL)
    {
        $where2 = array('trashed' => 0);
        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --',
            'ml_pay_grade_id', 'pay_grade',$where2);

        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $where = array('employee_id' => $employee_id,'employment.current'=>1);

        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where);

        $data['mp_history'] = $this->hr_model->emp_mp_history(array('employee.employee_id' => $employee_id));
        $data['mp_history_wd'] = $this->hr_model->emp_mp_history_wd(array('employee.employee_id' => $employee_id));
        $employement_id = $data['employment']->employment_id;
        $data['chk'] = $this->hr_model->get_all_by_where('position_management',array('employement_id'=> $data['employment']->employment_id));
        //var_dump($data['employment']);

        $data['join'] = $this->hr_model->manage_postion(array('employee.employee_id' => $employee_id, 'position_management.current =' => 1,'employment.current' =>1,'employee.enrolled' =>1,'position_management.position_id'=>$this->uri->segment(3)));

        if ($this->input->post('add')) {
            if(!empty($data['chk']))
                foreach($data['chk'] as $re) {
                    if ($re->status == 1) {
                        $msg = "One Status is Already in Pending::error";
                        $this->session->set_flashdata('msg', $msg);
                        redirect('human_resource/assign_position/' . $employee_id);
                        return;

                    } }



            $FromDate = $this->input->post('from_date');
            /*   if(isset($FromDate) && !empty($FromDate))
               {
                   $FromDate = date('Y-m-d',strtotime($FromDate));
               }*/


            $add_type = array(
                'employement_id' => $employement_id,
                'ml_pay_grade_id' => $this->input->post('pay_grade'),
                'ml_designation_id' => $this->input->post('designation_name'),
                'job_specifications' => $this->input->post('job_specifications'),
                'from_date' => date("Y-m-d"),
                'date_created' => date("Y-m-d"),
                'status' => 1
            );
//echo "<pre>";

            $query_suordinate = $this->hr_model->create_new_record('position_management', $add_type);


            if ($query_suordinate) {
                $mgs = "Apply For Postion Successfuly..::success";
                $this->session->set_flashdata('msg',$mgs);
                redirect('human_resource/assign_position/' . $employee_id);
            }
        }
        $data['join'] = $this->hr_model->manage_postion(array('employee.employee_id' => $employee_id, 'position_management.current =' => 1,'employment.current' =>1,'employee.enrolled' =>1,'position_management.status'=>2));

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/assign_position', $data);
        $this->load->view('includes/footer');
    }

    public function edit_pend_rec($position_id =NULL, $employee_id =NULL)
    {
        $where2 = array('trashed' => 0);
        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --',
            'ml_pay_grade_id', 'pay_grade',$where2);

        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $data['mp_history'] = $this->hr_model->emp_mp_history(array('employee.employee_id' => $employee_id));
        $where = array('employee_id' => $employee_id,'employment.current'=>1);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where);
        $employement_id = $data['employment']->employment_id;


        if ($this->input->post('add')) {
            $FromDate = $this->input->post('from_date');
            if(isset($FromDate) && !empty($FromDate))
            {
                $FromDate = date('Y-m-d',strtotime($FromDate));
            }

            $add_type = array(
                'employement_id' => $employement_id,
                'ml_pay_grade_id' => $this->input->post('pay_grade'),
                'ml_designation_id' => $this->input->post('designation_name'),
                'job_specifications' => $this->input->post('job_specifications'),
                'from_date' => $FromDate,
            );
//echo "<pre>";print_r($add_type);die;

            $query_suordinate = $this->hr_model->update_record('position_management',array('position_management.position_id'=>$position_id), $add_type);


            if ($query_suordinate) {
                $mgs = "Postion Updated Successfuly..::success";
                $this->session->set_flashdata('msg',$mgs);
                redirect('human_resource/assign_position/' . $employee_id);
            }
        }
        $data['join'] = $this->hr_model->manage_postion(array('employee.employee_id' => $employee_id, 'position_management.current =' => 1,'employment.current' =>1,'employee.enrolled' =>1,'position_management.position_id'=>$position_id));
        // echo "<pre>";print_r($data['join']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/edit_assign', $data);
        $this->load->view('includes/footer');
    }

    public function view_position($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array(
            'employee.employee_id' => $employee_id,
        );

        $data['complete_profile'] = $this->hr_model->view_position_record($where);
        //echo "<pre>"; print_r($data['complete_profile']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_table', $data);
        $this->load->view('includes/footer');
    }

    public function view_retire($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array(
            'employee.employee_id' => $employee_id,
            //'employee.enrolled' => 0,
            // 'employment.current'=>0,
            // 'posting.current'=>0,
            // 'position_management.current'=>0
        );

        $data['complete_profile'] = $this->hr_model->view_retire_record($where);
        //echo "<pre>"; print_r($data['complete_profile']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_retire', $data);
        $this->load->view('includes/footer');
    }

    public function view_retirement($param = NULL)
    {
        if ($param == "list") {
//            echo $this->hr_model->retirement();

            //Custom Code to get the Currently Enrolled Employees SHH.
            /**
             * Datatables will have only those employees
             *  if employemnet is set and is employee is currently employeed.
             *  if position is set for employee and position is currently active and approved
             *  if posting is set for employee
             */
            $PTable = 'employee E';
            $data = array('E.employee_code, E.full_name, MLDg.designation_name, MLS.status_title AS SeparationStatus, E.employee_id, P.posting_id',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id AND PM.current= 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'posting P',
                    'condition' => 'ET.employment_id = P.employement_id AND P.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_branch MLB',
                    'condition' => 'MLB.branch_id = P.ml_branch_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_department MLD',
                    'condition' => 'P.ml_department_id = MLD.department_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'seperation_management SM',
                    'condition' => 'SM.employee_id = ET.employee_id AND SM.status < 2',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'MLS.status_id = SM.status',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.trashed' => 0,
                'PM.status' => 2
            );
            $group_by = 'E.employee_id';
            $addColumn = '';
            $editColumn =
                array(
                    array(
                        'E.employee_id',
                        '<a data-status="$3" href="human_resource/terminate/$1/$2"><input type="button" name="Discharge" value="Discharge" class="btn green" /></a>',
                        'E.employee_id,P.posting_id,SeparationStatus'
                    )
                );
            $resultDT = $this->common_model->select_fields_joined_DT($data,$PTable,$joins,$where,'','',$group_by,$addColumn,$editColumn,'');
            $resultDT= json_decode($resultDT,true);
            foreach($resultDT['aaData'] as $keyArrayDT=>$valueArrayDT){
                if(isset($resultDT['aaData'][$keyArrayDT][3]) && !empty($resultDT['aaData'][$keyArrayDT][3])){
                    $originalBtn = $resultDT['aaData'][$keyArrayDT][4];
                    $dischargeBtn = stripAttributes($originalBtn ,array('data-status','class','type','value','name'));
                    $getPosition = strpos($dischargeBtn,"type=");
                    $disabled = ' disabled="true" ';
                    $dischargeBtn = substr_replace($dischargeBtn,$disabled,$getPosition,0);
                    $replacingSubArray = array(
                        4 => $dischargeBtn
                    );
                    $resultDT['aaData'][$keyArrayDT] = array_replace($resultDT['aaData'][$keyArrayDT],$replacingSubArray);
                }
            }
            print_r(json_encode($resultDT));
            return;
        }
        $where2 = array('trashed' => 0);
        $data['design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$where2);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/retirement', $data);
        $this->load->view('includes/footer');
    }

    public function retired_employees($param = NULL)
    {
        if ($param == "list") {
            echo $this->hr_model->retired_employees();
            die;
        }
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/retired_employees');
        $this->load->view('includes/footer');
    }

    public function terminate($employee_id = NULL,$posting_id = NULL)
    {
        //error_reporting(0);
//        $employee_id = $this->uri->segment(3);
//        $posting_id = $this->uri->segment(4);
        //If Data Is Posted then If Statement Will Execute SHH.
        if($this->input->post()){
            $separationType = $this->input->post('separationType');
            $reason = $this->input->post('reason');
            $date = $this->input->post('date');
            if(isset($date) && !empty($date))
            {
                $date = date('Y-m-d',strtotime($date));
            }
            $note = $this->input->post('note');
            $terminationData = array(
                'employee_id' => $employee_id,
                'date' => $date,
                'note' => $note,
                'ml_seperation_type' => $separationType,
                'reason' => $reason,
                'trashed' => 0,
                'enabled' => 1,
                'status' => 1
            );
            //echo "<pre>"; print_r($terminationData);die;
            $table = 'seperation_management';

            $result = $this->common_model->insert_record($table,$terminationData);
            if($result>0){
                redirect('human_resource/view_retirement/');
            }
            return;
        }
        //$data['data'] = $this->hr_model->terminate_employees();
        $where2 = array('trashed' => 0);
        $data['reason'] = $this->hr_model->dropdown_wd_option('ml_separation_type', '-- Select Separation --',
            'separation_type_id', 'separation_type',$where2);
        $data['reason2'] = $this->hr_model->dropdown_wd_option('ml_posting_reason_list', '-- Select Reason --',
            'posting_reason_list_id', 'posting_reason',$where2);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/terminate', $data);
        $this->load->view('includes/footer');
    }
    /// ======================== ====== ========= === ======== ================================ ///
    /// ************************ Update Functions For Employee *************************** ///
    /// ======================== ====== ========= === ======== ================================ ///
    public function edit_employee_acct($employee_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
            $data['immigration'] = $this->hr_model->get_row_by_where('immigration', $where);
            $where_d =array('trashed' =>0);
            $data['gender'] = $this->hr_model->dropdown_wd_option('ml_gender_type', '-- Select Gender --', 'gender_type_id', 'gender_type_title',$where_d);
            $data['marital_status'] = $this->hr_model->dropdown_wd_option('ml_marital_status', '-- Select Marital Status --', 'marital_status_id', 'marital_status_title',$where_d);
            $data['nationality'] = $this->hr_model->dropdown_wd_option('ml_nationality', '-- Select Nationality --', 'nationality_id', 'nationality',$where_d);
            $data['religion'] = $this->hr_model->dropdown_wd_option('ml_religion', '-- Select Religion --', 'religion_id', 'religion_title',$where_d);
            $data['employee_id'] = $this->uri->segment(3);
            if ($this->input->post('continue')) {
                $this->db->trans_start();

                $dateofbirth = $this->input->post('date_of_birth');
                $visaexpiray = $this->input->post('visa_expiry');
                if(isset($dateofbirth) && !empty($dateofbirth)){
                    $dateofbirth = date('Y-m-d',strtotime($dateofbirth));
                }
                if(isset($visaexpiray) && !empty($visaexpiray)){
                    $visaexpiray = date('Y-m-d',strtotime($visaexpiray));
                }


                $emp_id = $this->input->post('emp_id');
                $cnic = $this->input->post('nic1') . "-" . $this->input->post('nic2') . "-" . $this->input->post('nic3');

                $first_name = $this->input->post('first_name');
                $last_name = $this->input->post('last_name');
                $gender = $this->input->post('gender');
                $marital_status = $this->input->post('marital_status');
                $nationality = $this->input->post('nationality');
                $religion = $this->input->post('religion');

                $file = $this->hr_model->do_upload('employee_photo');

                $insert_data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $first_name . " " . $last_name,
//                    'employee_code' => $this->input->post('employee_code'),
                    'blood_group' => $this->input->post('blood_group'),
                    'disabled_people' => $this->input->post('disabled_people'),
                    'father_name' => $this->input->post('father_name'),
                    'CNIC' => $cnic,
                    'tax_number' => $this->input->post('tax_number'),
                    'date_of_birth' => $dateofbirth
                );
                if(isset($gender) && !empty($gender)){
                    $insert_data['gender'] = $gender;
                }
                if(isset($marital_status) && !empty($marital_status)){
                    $insert_data['marital_status'] = $marital_status;
                }
                if(isset($nationality) && !empty($nationality)){
                    $insert_data['nationality'] = $nationality;
                }
                if(isset($religion) && !empty($religion)){
                    $insert_data['religion'] = $religion;
                }

                //Little Validations
                if(!isset($first_name) || empty($first_name)){
                    return;
                }

                //If File Has Been Selected For Insert/Update Then Please Update the File, Unless And Until Not Selected Then Take No Action.
                if(isset($file) && !empty($file)){
                    $thumb = $this->hr_model->resize_image($file['upload_data']['full_path'], "upload/Thumb_Nails");
                    $thumbNail = $thumb['upload_data']['file_name'];
                    $file_name = $file['upload_data']['file_name'];
                    if ($file_name == '') {
                        $file_name = $this->input->post('uploaded_photo');
                    } else {
                        $file_name = $file['upload_data']['file_name'];
                    }

                    $delete_pic = $this->hr_model->get_row_by_id('employee', $where);
                    if(file_exists('upload/' . $delete_pic->photograph)){
                        unlink('upload/' . $delete_pic->photograph);
                        unlink('upload/Thumb_Nails/' . $delete_pic->thumbnail);
                    }
                    $insert_data['photograph'] = $file_name;
                    $insert_data['thumbnail'] = $file_name;
                }

                // print_r($insert_data);die;
                if (!empty($emp_id)) {
                    $emp_query = $this->hr_model->update_record('employee', $where, $insert_data);
                } else {
                    $insert_data = array(
                        'full_name' => $first_name . " " . $last_name,
                        'father_name' => $this->input->post('father_name'),
                        'CNIC' => $cnic,
                        'gender' => $this->input->post('gender'),
                        'blood_group' => $this->input->post('blood_group'),
                        'disabled_people' => $this->input->post('disabled_people'),
                        'marital_status' => $this->input->post('marital_status'),
                        'nationality' => $this->input->post('nationality'),
                        'religion' => $this->input->post('religion'),
                        'tax_number' => $this->input->post('tax_number'),
                        'date_of_birth' => $dateofbirth
                    );
                    $emp_query = $this->hr_model->create_new_record('employee', $insert_data);
                }


                //Immigration Details
                $passportNum =  $this->input->post('passport_num');
                $visaNum = $this->input->post('visa_num');
                $insert_immigration = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'passport_num' => $passportNum,
                    'visa_num' => $visaNum,
                    'visa_expiry' => $visaexpiray
                );
                if(isset($passportNum) && !empty($passportNum) && !empty($visaNum) && !empty($visaexpiray)){
                    if(isset($emp_id)) {
                        $table= 'immigration';
                        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
                        $selectWhere = array(
                            'employee_id' => $emp_id,
                            'trashed' => 0
                        );
                        $countResult = $this->common_model->select_fields_where($table,$selectData,$selectWhere,TRUE);
                        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                            $immigration_query = $this->hr_model->update_record('immigration', $where, $insert_immigration);
                        }else{
                            $insert_immigration = array(
                                'employee_id' => $this->input->post('emp_id'),
                                'passport_num' => $this->input->post('passport_num'),
                                'visa_num' => $this->input->post('visa_num'),
                                'visa_expiry' => $visaexpiray
                            );
                            $immigration_query = $this->hr_model->create_new_record('immigration', $insert_immigration);
                        }
                    }else{
                        $insert_immigration = array(
                            'employee_id' => $this->input->post('emp_id'),
                            'passport_num' => $this->input->post('passport_num'),
                            'visa_num' => $this->input->post('visa_num'),
                            'visa_expiry' => $visaexpiray
                        );
                        $immigration_query = $this->hr_model->create_new_record('immigration', $insert_immigration);
                    }
                }
                $this->db->trans_complete();

                if ($emp_query || (isset($immigration_query) && $immigration_query === true)) {
                    if(isset($file_name) && !empty($file_name)){
                        $this->session->set_userdata('thumbnail',$file_name);
                    }
                    $this->session->set_flashdata('update','Update Successfully !');
                    redirect('human_resource/edit_employee_acct/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_employee_personal_info',$data);
        $this->load->view('includes/footer');
    }
    public function appraisal_setup($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
//        $employment_id=get_employment_from_employeeID($employee_id);
        $where = array('employee_id' => $employee_id,'enrolled'=>1);

        $where_emply = array('employment.employee_id' => $employee_id,'employment.current'=>1);

        $data['personal_info'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['emply'] = $this->hr_model->get_row_by_where('employment', $where_emply);

        $where_p = array('position_management.employement_id' =>  $data['emply']->employment_id);
        $data['emplyeee'] = $this->hr_model->detl_employeeee($where_p);

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/appraisal_setup',$data);
        $this->load->view('includes/footer');
    }
    public function add_gender()
    {
        if ($this->input->post('gender_type_title')) {
            $genderTypeTitle = $this->input->post('gender_type_title');

            //This Above Check Do not Check If its Been Trashed Or Not
            $table = 'ml_gender_type MLGT';
            $selectData = array('COUNT(1) AS TotalRecordsFound');
            $where = array(
                'gender_type_title' => $genderTypeTitle,
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                echo "FAIL::Record Already Exist::error";
                return;
            }

            $disttName = $this->input->post('gender_type_title');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_gender_type';
            $data = array(
                'gender_type_title' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }
        }
    }


    public function add_nation()
    {
        if ($this->input->post('nationality')) {

            $rec = $this->site_model->get_all('ml_nationality');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->nationality,$this->input->post('nationality')) == 0)

                    {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }

            $disttName = $this->input->post('nationality');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_nationality';
            $data = array(
                'nationality' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }
        }
    }
    public function add_marital()
    {
        if ($this->input->post('marital_status_title')) {

            $rec = $this->site_model->get_all('ml_marital_status');
            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    if (strcasecmp($rec->marital_status_title,$this->input->post('marital_status_title')) == 0)

                    {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }


            $disttName = $this->input->post('marital_status_title');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_marital_status';
            $data = array(
                'marital_status_title' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    public function add_religion()
    {
        if ($this->input->post('religion_title')) {

            $rec = $this->site_model->get_all('ml_religion');
            if(isset($rec) && !empty($rec) && is_array($rec)) {
                foreach ($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->religion_title, $this->input->post('religion_title')) == 0) {
                        echo "FAIL::Record Already Exist::error";
                        return;
                    }
                }
            }


            $disttName = $this->input->post('religion_title');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_religion';
            $data = array(
                'religion_title' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }
    /// Function To Edit Employee Current Contact Details
    public function edit_emp_curr_contact($employee_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $where = array('employee_id' => $employee_id);
            $where_detail = array('employee_id' => $employee_id, 'emergency_contacts.trashed' => 0);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $data['curr_add'] = $this->hr_model->get_row_by_where('current_contacts', $where);

            $data['perm_add'] = $this->hr_model->get_row_by_where('permanant_contacts', $where);

            $data['emerg_add'] = $this->hr_model->get_row_by_where('emergency_contacts', $where_detail);
//            print_r($data['emerg_add']);die;
            $data['emrg_detail'] = $this->hr_model->emergency_contact_detail($where_detail);
            $where_d =array('trashed' => 0);
            $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where_d);

            $data['district'] = $this->hr_model->dropdown_wd_option('ml_district', '-- Select District --', 'district_id', 'district_name',$where_d);

            $data['province'] = $this->hr_model->dropdown_wd_option('ml_province', '-- Select Province --', 'province_id', 'province_name',$where_d);

            $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --', 'relation_id', 'relation_name',$where_d);

            $data['employee_id'] = $this->uri->segment(3);

            $emp_id = $this->input->post('emp_id');

            /// To Update Employee Current Contacts
            if ($this->input->post('add_current')) {
                $province = $this->input->post('province');
                $city_village = $this->input->post('city_village');
                $insert_current = array(
                    'address' => $this->input->post('address'),
                    'home_phone' => $this->input->post('home_phone'),
                    'mob_num' => $this->input->post('mob_num'),
                    'email_address' => $this->input->post('email_address'),
                    'office_phone' => $this->input->post('office_phone'),
                    'official_email' => $this->input->post('official_email'),
                );

                //Only Add if Available
                if(isset($province) && !empty($province) && is_numeric($province)){
                    $insert_current['province'] = $province;
                }

                //Only Add if Available
                if(isset($city_village) && !empty($city_village) && is_numeric($city_village)){
                    $insert_current['city_village'] = $city_village;
                }

                //First Lets Check if Record Already Exists for this employee.
                $result = $this->common_model->select_fields_where('current_contacts',array('COUNT(1) AS Total',false),$where,TRUE);

                if($result->Total > 0){
                    $current_query = $this->hr_model->update_record('current_contacts', $where, $insert_current);
                }else{ //If No Record Found Than we need to insert the record.
                    $insert_current['employee_id'] = $employee_id;
                    $insertID = $this->common_model->insert_record('current_contacts',$insert_current);
                }

                if ($current_query || $insertID > 0) {
                    $this->session->set_flashdata('cmsg',"<span style='color: #000000;background-color: #008000'>Record Update Successfully !</span>");
                    redirect('human_resource/edit_emp_curr_contact/' . $emp_id);
                }
            }
            /// To Update Employee Permanent Contacts
            if ($this->input->post('add_permanant')) {
                $insert_permanent = array(
                    'address' => $this->input->post('address'),
                    'city_village' => $this->input->post('city_village'),
                    'district' => $this->input->post('district'),
                    'province' => $this->input->post('province'),
                    'phone' => $this->input->post('phone')
                );
                $permanent_query = $this->hr_model->update_record('permanant_contacts', $where, $insert_permanent);

                if ($permanent_query) {
                    $this->session->set_flashdata('pmsg',"<span style='color: #000000;background-color: #008000'>Record Update Successfully !</span>");
                    redirect('human_resource/edit_emp_curr_contact/' . $emp_id);
                }
            }
            /// To Update Employee Emergency Contact
            if ($this->input->post('add_emergency')) {
                $insert_emergency = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'cotact_person_name' => $this->input->post('cotact_person_name'),
                    'relationship' => $this->input->post('relationship'),
                    'home_phone' => $this->input->post('home_phone'),
                    'mobile_num' => $this->input->post('mobile_num'),
                    'work_phone' => $this->input->post('work_phone')
                );
                $emergency_query = $this->hr_model->update_record('emergency_contacts', $where, $insert_emergency);

                if ($emergency_query) {
                    $this->session->set_flashdata('emsg',"<span style='color: #000000;background-color: #008000'>Record Update Successfully !</span>");
                    redirect('human_resource/edit_emp_curr_contact/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_contact_details', $data);
        $this->load->view('includes/footer');
    }
    public function add_city()
    {
        if ($this->input->post('add')) {

            $rec = $this->site_model->get_all('ml_city');
            if(isset($rec) && !empty($rec) && is_array($rec))
            {
                foreach($rec as $rec) {
                    if (strcasecmp($rec->city_name,$this->input->post('city_name')) == 0)

                    {
                        echo "FAIL:: Already Inserted !::error";
                        return;
                    }
                }
            }

            $add_type = array(
                'city_name' => $this->input->post('city_name'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_city', $add_type);
            if ($query) {
                redirect('human_resource/edit_emp_curr_contact/' . $this->uri->segment(3));
            }
        }
    }
    public function add_province()
    {
        if ($this->input->post('province_name')) {

            $rec = $this->site_model->get_all('ml_province');

            if(isset($rec) && !empty($rec) && is_array($rec)){
                foreach($rec as $rec) {
                    //strcasecmp($rec->module_name,$this->input->post('module_name'));
                    if (strcasecmp($rec->province_name,$this->input->post('province_name')) == 0)

                    {
                        $msg ="OOps Already Inserted !";
                        $this->session->set_flashdata('msg',$msg);
                        redirect('human_resource/edit_emp_curr_contact/' . $this->uri->segment(3));
                        return;
                    }
                }
            }

            $disttName = $this->input->post('province_name');
            if (!isset($disttName) || empty($disttName)) {
                echo "FAIL:: Data Was Not Send Accurately::error";
                return;
            }
            //Data to enter in to the database. .
            $table = 'ml_province';
            $data = array(
                'province_name' => $disttName,
                'trashed' => 0
            );
            $insertedID = $this->common_model->insert_record($table, $data);
            if ($insertedID > 0) {
                echo "OK::Record Successfully Inserted::success::" . $insertedID;
                return;

            } else {
                echo "FAIL::Some Database Error Occurred, Record Could Not Be Added, Please Contact System Administrator For Further Assistance.::error";
                return;
            }

        }
    }

    /// Function To Edit Employee Single Emergency Contact Details
    public function edit_emp_single_curr_contact($employee_id = NULL, $emergency_id = NULL)
    {
//        error_reporting(0);
        if ($this->uri->segment(3)) {
            $where = array('employee_id' => $employee_id);
            $where_detail = array('employee_id' => $employee_id, 'emergency_contacts.trashed' => 0);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $data['curr_add'] = $this->hr_model->get_row_by_where('current_contacts', $where);

            $data['perm_add'] = $this->hr_model->get_row_by_where('permanant_contacts', $where);

            $data['emerg_add'] = $this->hr_model->get_row_by_where('emergency_contacts', $where_detail);

            $data['emrg_detail'] = $this->hr_model->emergency_contact_detail($where_detail);

            $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name');

            $data['district'] = $this->hr_model->dropdown_wd_option('ml_district', '-- Select District --', 'district_id', 'district_name');

            $data['province'] = $this->hr_model->dropdown_wd_option('ml_province', '-- Select Province --', 'province_id', 'province_name');

            $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --', 'relation_id', 'relation_name');

            $data['employee_id'] = $this->uri->segment(3);

            $emp_id = $this->input->post('emp_id');

            /// To Update Employee Emergency Contact
            if ($this->input->post('add_emergency')) {
                $insert_emergency = array(
                    'cotact_person_name' => $this->input->post('cotact_person_name'),
                    'relationship' => $this->input->post('relationship'),
                    'home_phone' => $this->input->post('home_phone'),
                    'mobile_num' => $this->input->post('mobile_num'),
                    'work_phone' => $this->input->post('work_phone')
                );
                $emergency_query = $this->hr_model->update_record('emergency_contacts', array('emergency_contact_id' => $emergency_id), $insert_emergency);

                if ($emergency_query) {
                    redirect('human_resource/edit_emp_curr_contact/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_contact_details', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Emergency Contact
    public function send_2_trash_contct($employee_id = NULL, $record_id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();

        $data['emer_contact'] = $this->hr_model->get_row_by_id('emergency_contacts', array('employee_id' => $employee_id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emergency_contacts', array('emergency_contact_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('emergency_contacts', array('emergency_contact_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'emergency_contacts',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_curr_contact/' . $employee_id);
        }
    }

    /// Function to Edit Employee Qualification
    public function edit_emp_qualification($employee_id = NULL)
    {

        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);

            //$where1 = array('employee_id' => $employee_id, 'qualification.trashed' => 0);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $data['qualify'] = $this->hr_model->get_row_by_where('qualification', $where);

            if (isset($data['qualify']) && !empty($data['qualify'])) {
                $where_qualify = array('qualification_id' => $data['qualify']->qualification_id);
            }

            $emp_id = $this->input->post('emp_id');
            $where_d =array('trashed' =>0);
            $data['qualification'] = $this->hr_model->dropdown_wd_option('ml_qualification_type', '-- Select Qualification --',
                'qualification_type_id', 'qualification_title',$where_d);

            $where_qua = array('employee_id' => $employee_id, 'qualification.trashed' => 0);
            $data['qualification_detail'] = $this->hr_model->qualification_detail($where_qua);

            if ($this->input->post('continue')) {
                /*$qualification 	= $this->input->post('qualification1')."".$this->input->post('qualification2')."".$this->input->post('qualification3');
				$institute 		= $this->input->post('institute1')."".$this->input->post('institute2')."".$this->input->post('institute3');
				$year 			= $this->input->post('year1')."".$this->input->post('year2')."".$this->input->post('year3');
				*/
                $type = $this->input->post('qualification_type_id');
                switch ($type) {
                    case '1' :
                        $qualification = $this->input->post('qualification1');
                        $institute = $this->input->post('institute1');
                        $year = $this->input->post('year1');
                        break;
                    case '2' :
                        $qualification = $this->input->post('qualification2');
                        $institute = $this->input->post('institute2');
                        $year = $this->input->post('year2');
                        break;
                    case '3' :
                        $qualification = $this->input->post('qualification3');
                        $institute = $this->input->post('institute3');
                        $year = $this->input->post('year3');
                        break;
                }

                $insert_qualification = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'qualification_type_id' => $this->input->post('qualification_type_id'),
                    'qualification' => $qualification,
                    'institute' => $institute,
                    'major_specialization' => $this->input->post('major_specialization'),
                    'year' => $year,
                    'gpa_score' => $this->input->post('gpa_score'),
                    'duration' => $this->input->post('duration'),
                );
                $qualification_query = $this->hr_model->update_record('qualification', $where_qualify, $insert_qualification);

                if ($qualification_query) {
                    redirect('human_resource/edit_emp_qualification/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_qualification', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Qualification in Edit Mode
    public function add_qualification($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where_d = array('trashed' => 0);
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $emp_id = $this->input->post('emp_id');

        $data['qualification'] = $this->hr_model->dropdown_wd_option('ml_qualification_type', '-- Select Qualification --', 'qualification_type_id', 'qualification_title',$where_d);

        $where_qua = array('employee_id' => $employee_id, 'qualification.trashed' => 0);
        $data['qualification_detail'] = $this->hr_model->qualification_detail($where_qua);

        if ($this->input->post('continue')) {
            $type = $this->input->post('qualification_type_id');
            switch ($type) {
                case '1' :
                    $qualification = $this->input->post('qualification1');
                    $institute = $this->input->post('institute1');
                    $year = $this->input->post('year1');
                    break;
                case '2' :
                    $qualification = $this->input->post('qualification2');
                    $institute = $this->input->post('institute2');
                    $year = $this->input->post('year2');
                    break;
                case '3' :
                    $qualification = $this->input->post('qualification3');
                    $institute = $this->input->post('institute3');
                    $year = $this->input->post('year3');
                    break;
            }

            if(isset($year) && !empty($year))
            {
                $year = date('Y-m-d',strtotime($year));
            }
            $insert_qualification = array(
                'employee_id' => $this->input->post('emp_id'),
                'qualification_type_id' => $this->input->post('qualification_type_id'),
                'qualification' => $qualification,
                'institute' => $institute,
                'major_specialization' => $this->input->post('major_specialization'),
                'year' => $year,
                'gpa_score' => $this->input->post('gpa_score'),
                'duration' => $this->input->post('duration'),
            );
            $qualification_query = $this->hr_model->create_new_record('qualification', $insert_qualification);

            if ($qualification_query) {

                redirect('human_resource/edit_emp_qualification/' . $emp_id);
            }
        }
        $this->load->view('includes/header');
        $this->load->view('HR/add_qualification_edt', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_qualification_single($employee_id = NULL, $qualify_id = NULL)
    {
        //error_reporting(0);
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where1 = array('employee_id' => $employee_id, 'qualification_id' => $qualify_id);

            $where_qualify = array('qualification_id' => $qualify_id);
            $data['qualify'] = $this->hr_model->get_row_by_where('qualification', $where_qualify);
            //echo "<pre>";print_r($where_qualify ); die;

            $emp_id = $this->input->post('emp_id');
            $where_trashed=array('trashed'=>0);
            $data['qualification'] = $this->hr_model->dropdown_wd_option('ml_qualification_type', '-- Select Qualification --',
                'qualification_type_id', 'qualification_title',$where_trashed);

            $where_qua = array('qualification.employee_id' => $employee_id, 'qualification.trashed' => 0);
            $data['qualification_detail'] = $this->hr_model->qualification_detail($where_qua);

            if ($this->input->post('continue')) {
                /*$qualification 	= $this->input->post('qualification1')."".$this->input->post('qualification2')."".$this->input->post('qualification3');
				$institute 		= $this->input->post('institute1')."".$this->input->post('institute2')."".$this->input->post('institute3');
				$year 			= $this->input->post('year1')."".$this->input->post('year2')."".$this->input->post('year3');
				*/
                $type = $this->input->post('qualification_type_id');
                switch ($type) {
                    case '1' :
                        $qualification = $this->input->post('qualification1');
                        $institute = $this->input->post('institute1');
                        $year = $this->input->post('year1');
                        break;
                    case '2' :
                        $qualification = $this->input->post('qualification2');
                        $institute = $this->input->post('institute2');
                        $year = $this->input->post('year2');
                        break;
                    case '3' :
                        $qualification = $this->input->post('qualification3');
                        $institute = $this->input->post('institute3');
                        $year = $this->input->post('year3');
                        break;
                }

                if(isset($year) && !empty($year))
                {
                    $year = date('Y-m-d',strtotime($year));
                }

                $insert_qualification = array(
                    'employee_id' => $this->input->post('emp_id'),
                    'qualification_type_id' => $this->input->post('qualification_type_id'),
                    'qualification' => $qualification,
                    'institute' => $institute,
                    'major_specialization' => $this->input->post('major_specialization'),
                    'year' => $year,
                    'gpa_score' => $this->input->post('gpa_score'),
                    'duration' => $this->input->post('duration'),
                );
                $qualification_query = $this->hr_model->update_record('qualification', $where_qualify, $insert_qualification);

                if ($qualification_query) {
                    redirect('human_resource/edit_emp_qualification/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_qualification_single', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Qualification
    public function send_2_trash_qualification($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('qualification', array('qualification_id' => $record_id));
        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'qualification',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_qualification/' . $employee_id);
        }
    }
    public function send_2_trash_qualificationadd($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('qualification', array('qualification_id' => $record_id));
        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('qualification', array('qualification_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'qualification',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_qualification/' . $employee_id);
        }
    }
    /// Function to Edit Employee Experience
    public function edit_emp_experience($employee_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);
            $where = array('employee_id' => $employee_id);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $data['experience'] = $this->hr_model->get_row_by_where('emp_experience', $where);

            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Title --',
                'ml_designation_id', 'designation_name',$whered);

            $data['employment'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
                'employment_type_id', 'employment_type',$whered);

            $where_exp = array('employee_id' => $employee_id, 'emp_experience.trashed' => 0);
            $data['experience_detail'] = $this->hr_model->emp_experience_detail($where_exp);

            if ($this->input->post('continue')) {
                $insert_experience = array(
                    'job_title' => $this->input->post('job_title'),
                    'ml_employment_type_id' => $this->input->post('ml_employment_type_id'),
                    'from_date' => $this->input->post('from_date'),
                    'to_date' => $this->input->post('to_date'),
                    'organization' => $this->input->post('organization'),
                    'comment' => $this->input->post('comment'),
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $experience_query = $this->hr_model->update_record('emp_experience', $where, $insert_experience);

                if ($experience_query) {
                    redirect('human_resource/edit_emp_experience/' . $emp_id);
                }
            }
        }
        //echo "<pre>";
        //print_r($data['experience_detail']); die;
        $this->load->view('includes/header');
        $this->load->view('HR/edit_experience', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Experience in Edit View
    public function add_experience_edt($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Designation --',
            'ml_designation_id', 'designation_name',$whered);

        $data['employment'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$whered);

        $where_exp = array('employee_id' => $employee_id, 'emp_experience.trashed' => 0);
        $data['experience_detail'] = $this->hr_model->emp_experience_detail($where_exp);

        if ($this->input->post('continue')) {

            $fromdate = $this->input->post('from_date');

            $todate = $this->input->post('to_date');

            if(isset($fromdate) && !empty($fromdate))
            {
                $fromdate = date('Y-m-d',strtotime($fromdate));
            }

            if(isset($todate) && !empty($todate))
            {
                $todate = date('Y-m-d',strtotime($todate));
            }

            $insert_experience = array(
                'employee_id' => $emp_id,
                'job_title' => $this->input->post('job_title'),
                'ml_employment_type_id' => $this->input->post('ml_employment_type_id'),
                'from_date' => $fromdate,
                'to_date' =>$todate,
                'total_dur' => $this->input->post('total_dur'),
                'organization' => $this->input->post('organization'),
                'comment' => $this->input->post('comment'),
            );
            // echo "<pre>";print_r($insert_experience);die;
            $experience_query = $this->hr_model->create_new_record('emp_experience', $insert_experience);

            if ($experience_query) {
                $msg ="Record Successfully Inserted !::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_experience/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_experience_edt', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_single_experience($employee_id = NULL, $experince_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_experince = array('experience_id' => $experince_id);

            $data['experience'] = $this->hr_model->get_row_by_where('emp_experience', $where_experince);

            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Title --',
                'ml_designation_id', 'designation_name',$whered);

            $data['employment'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
                'employment_type_id', 'employment_type',$whered);

            $where_exp = array('employee_id' => $employee_id, 'emp_experience.trashed' => 0);
            $data['experience_detail'] = $this->hr_model->emp_experience_detail($where_exp);

            if ($this->input->post('continue')) {
                $Fdate=strtotime($this->input->post('from_date'));
                $Tdate=strtotime($this->input->post('to_date'));
                $dateFrom=date("Y-m-d",$Fdate);
                $dateTo=date("Y-m-d",$Tdate);
                $insert_experience = array(
                    'job_title' => $this->input->post('job_title'),
                    'ml_employment_type_id' => $this->input->post('ml_employment_type_id'),
                    'from_date' => $dateFrom,
                    'to_date' => $dateTo,
                    'organization' => $this->input->post('organization'),
                    'comment' => $this->input->post('comment'),
                );
                // echo "<pre>";print_r($insert_experience);die;
                $experience_query = $this->hr_model->update_record('emp_experience', $where_experince, $insert_experience);

                if ($experience_query) {
                    redirect('human_resource/edit_emp_experience/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_experience_single', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Experience
    public function send_2_trash_experience($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('emp_experience', array('experience_id' => $record_id));
        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emp_experience', array('experience_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('emp_experience', array('experience_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'emp_experience',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_experience/' . $employee_id);
        }
    }

    public function send_2_trash_experience_add($employee_id = NULL, $record_id = NULL)
    {
        $table_id=$this->uri->segment(5);
        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('emp_experience', array('experience_id' => $record_id));
        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emp_experience', array('experience_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('emp_experience', array('experience_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'emp_experience',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_experience/' . $employee_id);
        }
    }


    /// Function to Edit Employee Skills And Trainings
    public function edit_emp_skill($employee_id = NULL)
    {
        if (isset($employee_id) && !empty($employee_id) && is_numeric($employee_id)) {
            $data['employee_id'] = $this->uri->segment(3);
            $where = array('employee_id' => $employee_id);
            $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
            $employmentID = $data['empl']->employment_id;
            $where_sk = array('emp_skills.employment_id' => $employmentID, 'emp_skills.trashed' => 0);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
            $data['skill'] = $this->hr_model->get_row_by_where('emp_skills', $where_sk);
            $data['training'] = $this->hr_model->get_row_by_where('training', $where);
            $whered =array('trashed' => 0);
            $emp_id = $this->input->post('emp_id');
            $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$whered);
            //$where2=array('trashed'=>0);
            //$data['skill_level'] = $this->hr_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$where2);

            $data['skill_details'] = $this->hr_model->emp_skill_detail($where_sk);

            $where_tr = array('employee_id' => $employee_id, 'training.trashed' => 0);
            $data['training_detail'] = $this->hr_model->get_all_by_where('training', $where_tr);
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_skills_trainings', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Add Employee Skills In Edit View
    public function add_skill_edt($employee_id = NULL)
    {
        if($employee_id === NULL || !is_numeric($employee_id) || empty($employee_id)){
            redirect(base_url());
            return;
        }

        $data['employee_id'] = $employee_id;
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $whered =array('trashed' => 0);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;
        $where_sk = array('emp_skills.employment_id' => $employmentID, 'emp_skills.trashed' => 0);
        $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$whered);

        $data['skill_level'] = $this->hr_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$whered);
        //$where_sk = array('employee_id' => $employee_id, 'emp_skills.trashed' => 0);
        $data['skill_details'] = $this->hr_model->emp_skill_detail($where_sk);

        if ($this->input->post('add_skill')) {
            $skillTypeID = $this->input->post('ml_skill_type_id');
            $level = $this->input->post('level');
            $comments = $this->input->post('comments');
            $emp_id = $this->input->post('emp_id');
            if(!isset($emp_id) || !is_numeric($emp_id) || empty($emp_id)){
                $msg = "Some Problem With The Post, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_skill_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }
            if(!isset($skillTypeID) || !is_numeric($skillTypeID) || empty($skillTypeID)){
                $msg = "Please Select Skill Type From Drop Down::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_skill_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }
            if(!isset($level) || empty($level)){
                $msg = "Please Select Skill Level::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_skill_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }
            //Need To Check if Record Already Exist..
            $table = 'emp_skills';
            $selectData = 'COUNT(1) AS TotalRecords';
            $where = array(
                'employment_id' => $employmentID,
                'trashed' => 0,
                'ml_skill_type_id' => $skillTypeID
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ="Record Already Exist::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_skill/' . $emp_id);
                return;
            }

            $insert_skills = array(
                'employment_id' => $employmentID,
                'ml_skill_type_id' => $skillTypeID,
                'ml_skill_level_id' => $level,
                'comments' => $comments
            );

            $skills_query = $this->hr_model->create_new_record('emp_skills', $insert_skills);

            if ($skills_query) {
                $msg = "Employee Skill Successfully Added To The System::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_skill/' . $emp_id);
            }
        }
        $this->load->view('includes/header');
        $this->load->view('HR/add_skills', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Add Employee Training In Edit View
    public function add_training_edt($employee_id = NULL)
    {
        //Hamid Work Edited By Me.. To Fix the Issues He Left Unfixed.

        $data['employee_id'] = $employee_id;
        $emp_id = $this->input->post('emp_id');
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $where_tr = array('employee_id' => $employee_id, 'training.trashed' => 0);
        $data['training_detail'] = $this->hr_model->get_all_by_where('training', $where_tr);

        if ($this->input->post('add_training')) {
            //Get All The Form Fields.
            $trainingName = $this->input->post('training_name');
            $institute = $this->input->post('Institute');
            $fromDate = $this->input->post('from_date');
            $toDate = $this->input->post('to_date');
            $totalDays = $this->input->post('total_days');
            $description = $this->input->post('description');

            if(!isset($trainingName) || empty($trainingName)){
                $msg = "Please Enter Training Name::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_training_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }
            if(!isset($institute) || empty($institute)){
                $msg = "Please Enter Institute::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_training_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }
            if(!isset($toDate) || empty($toDate)){
                $msg = "Please Enter From Date For Training::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_training_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }else{
                $toDate_str = strtotime($toDate);
                $toDate = date("Y-m-d",$toDate_str);
            }
            if(!isset($fromDate) || empty($fromDate)){
                $msg = "Please Enter To Date For Training::error";
                $this->session->set_flashdata('msg',$msg);
                $redirectURL = 'human_resource/add_training_edt/'.$employee_id;
                redirect($redirectURL);
                return;
            }else{
                $fromDate_str = strtotime($fromDate);
                $fromDate = date("Y-m-d",$fromDate_str);
            }


            $insert_training = array(
                'employee_id' => $emp_id,
                'Institute' => $institute,
                'training_name' => $trainingName,
                'frm_date' => $fromDate,
                'to_date' => $toDate,
                'tdays' => $totalDays,
                'description' => $description
            );
            //echo "<pre>";print_r($insert_qualification);die;
            $training_query = $this->hr_model->create_new_record('training', $insert_training);

            if ($training_query) {
                redirect('human_resource/edit_emp_skill/' . $emp_id);
            }
        }
        $this->load->view('includes/header');
        $this->load->view('HR/add_trainings', $data);
        $this->load->view('includes/footer');
    }

    // Edit Employee Single Skill
    public function edit_single_skill($employee_id = NULL, $skill_id = NULL)
    {
//        error_reporting(0);
        if ($this->uri->segment(4)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_skill = array('skill_record_id' => $skill_id);
            $data['skill'] = $this->hr_model->get_row_by_where('emp_skills', $where_skill);

            $where2=array('trashed'=>0);
            $data['skill_level'] = $this->hr_model->dropdown_wd_option('ml_skill_level', '-- Select Skill Level --', 'level_id', 'skill_level',$where2);
            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$whered);

            /// Edit Employee Skills
            if ($this->input->post('add_skill')) {
                $insert_skills = array(
                    'ml_skill_type_id' => $this->input->post('ml_skill_type_id'),
                    'experience_in_years' => $this->input->post('experience_in_years'),
                    'comments' => $this->input->post('comments')
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $skills_query = $this->hr_model->update_record('emp_skills', $where_skill, $insert_skills);

                if ($skills_query) {
                    redirect('human_resource/edit_emp_skill/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_skills', $data);
        $this->load->view('includes/footer');
    }


    // Edit Employee Single Report To //

    public function edit_single_report_to($employee_id = NULL, $report_heirarchy_id = NULL)
    {

        if ($this->uri->segment(4)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_skill = array('report_heirarchy_id' => $report_heirarchy_id);
            $data['reports'] = $this->hr_model->get_row_by_where('reporting_heirarchy', $where_skill);
            $where_report= array('reporting_option_id'=>  $data['reports']->ml_reporting_heirarchy_id);
            $data['method'] = $this->hr_model->get_row_by_where('ml_reporting_options', $where_report);
//print_r($data['method']) ;die;
            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['report_method'] = $this->hr_model->dropdown_wd_option('ml_reporting_options', '-- Select Report Type --', 'reporting_option_id', 'reporting_option',$whered);

            /// Edit Employee Skills
            if ($this->input->post('add_skill')) {
                $insert_skills = array(
                    'ml_skill_type_id' => $this->input->post('ml_skill_type_id'),
                    'experience_in_years' => $this->input->post('experience_in_years'),
                    'comments' => $this->input->post('comments')
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $skills_query = $this->hr_model->update_record('emp_skills', $where_skill, $insert_skills);

                if ($skills_query) {
                    redirect('human_resource/edit_emp_skill/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_report_to', $data);
        $this->load->view('includes/footer');
    }








    /// Edit Employee Single Training
    public function edit_single_training($employee_id = NULL, $skill_id = NULL)
    {
        if ($skill_id !== NULL && $skill_id > 0 && $employee_id > 0) {
            $data['employee_id'] = $employee_id;

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_training = array('training_record_id' => $skill_id);
            $data['training'] = $this->hr_model->get_row_by_where('training', $where_training);

            $emp_id = $this->input->post('emp_id');

            /// Edit Employee Trainings
            if ($this->input->post('add_training')) {
                //Get the Data From Posted Data First.
                $trainingName = $this->input->post('training_name');
                $Institute = $this->input->post('institute');
                $fromDate = $this->input->post('from_date');
                $toDate = $this->input->post('to_date');
                $totalDays = $this->input->post('total_days');
                $description = $this->input->post('description');

                if(!isset($trainingName) || empty($trainingName)){
                    $msg = "Please Enter Training Name::error";
                    $this->session->set_flashdata('msg',$msg);
                    $redirectURL = 'human_resource/edit_single_training/'.$employee_id.'/'.$skill_id;
                    redirect($redirectURL);
                    return;
                }
                if(!isset($Institute) || empty($Institute)){
                    $msg = "Please Enter Training Name::error";
                    $this->session->set_flashdata('msg',$msg);
                    $redirectURL = 'human_resource/edit_single_training/'.$employee_id.'/'.$skill_id;
                    redirect($redirectURL);
                    return;
                }
                if(!isset($fromDate) || empty($fromDate)){
                    $msg = "Please Enter Training Name::error";
                    $this->session->set_flashdata('msg',$msg);
                    $redirectURL = 'human_resource/edit_single_training/'.$employee_id.'/'.$skill_id;
                    redirect($redirectURL);
                    return;
                }else{
                    $fromDate_STR = strtotime($fromDate);
                    $fromDate = date('Y-m-d',$fromDate_STR);
                }
                if(!isset($toDate) || empty($toDate)){
                    $msg = "Please Enter Training Name::error";
                    $this->session->set_flashdata('msg',$msg);
                    $redirectURL = 'human_resource/edit_single_training/'.$employee_id.'/'.$skill_id;
                    redirect($redirectURL);
                    return;
                }else{
                    $toDate_STR = strtotime($toDate);
                    $toDate = date('Y-m-d',$toDate_STR);
                }
                $insert_training = array(
                    'training_name' => $trainingName,
                    'Institute' => $Institute,
                    'frm_date' => $fromDate,
                    'to_date' => $toDate,
                    'tdays' => $totalDays,
                    'description' => $description
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $training_query = $this->hr_model->update_record('training', $where_training, $insert_training);

                if ($training_query) {
                    redirect('human_resource/edit_emp_skill/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_training', $data);
        $this->load->view('includes/footer');
    }

    ///sd asdfasdf
    public function edit_emp_single_skill($employee_id = NULL, $skill_id = NULL)
    {
//        error_reporting(0);
        if ($skill_id !== NULL && $skill_id > 0 && $employee_id > 0) {
            $data['employee_id'] = $employee_id;

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_skill = array('skill_record_id' => $skill_id);
            $data['skill'] = $this->hr_model->get_row_by_where('emp_skills', $where_skill);

            $where_training = array('training_record_id' => $skill_id);
            $data['training'] = $this->hr_model->get_row_by_where('training', $where_training);
            $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
            $employmentID = $data['empl']->employment_id;
            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['skill_type'] = $this->hr_model->dropdown_wd_option('ml_skill_type', '-- Select Skill Type --', 'skill_type_id', 'skill_name',$whered);

            $where_sk = array('emp_skills.employment_id' => $employmentID, 'emp_skills.trashed' => 0);
            $data['skill_details'] = $this->hr_model->emp_skill_detail($where_sk);

            $where_tr = array('employee_id' => $employee_id, 'training.trashed' => 0);
            $data['training_detail'] = $this->hr_model->get_all_by_where('training', $where);

            /// Edit Employee Skills
            if ($this->input->post('add_skill')) {
                $insert_skills = array(
                    'ml_skill_type_id' => $this->input->post('ml_skill_type_id'),
                    'ml_skill_level_id' => $this->input->post('level'),
                    'comments' => $this->input->post('comments')
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $skills_query = $this->hr_model->update_record('emp_skills', $where_skill, $insert_skills);
                if ($skills_query) {
                    redirect('human_resource/edit_emp_skill/' . $emp_id);
                }
            }
            /// Edit Employee Trainings
            if ($this->input->post('add_training')) {
                $insert_training = array(
                    'training_name' => $this->input->post('training_name'),
                    'date' => $this->input->post('date'),
                    'description' => $this->input->post('description')
                );
                //echo "<pre>";print_r($insert_qualification);die;
                $training_query = $this->hr_model->update_record('training', $where_training, $insert_training);

                if ($training_query) {
                    redirect('human_resource/edit_emp_skill/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_skills_trainings', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Skill
    public function send_2_trash_skill($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $data['skill_trash'] = $this->hr_model->get_row_by_id('emp_skills', array('skill_record_id' => $record_id));
        if ($data['skill_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emp_skills', array('skill_record_id' => $record_id), $update_trash);
        }



        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'emp_skills',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_skill/' . $employee_id);
        }
    }


    public function send_2_trash_skill_add($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $data['skill_trash'] = $this->hr_model->get_row_by_id('emp_skills', array('skill_record_id' => $record_id));
        if ($data['skill_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('emp_skills', array('skill_record_id' => $record_id), $update_trash);
        }



        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'emp_skills',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_training/' . $employee_id);
        }
    }


    public function send_2_trash_report_to($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);

        $this->db->trans_start();

        $data['skill_trash'] = $this->hr_model->get_row_by_id('reporting_heirarchy', array('report_heirarchy_id' => $record_id));
        if ($data['skill_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('reporting_heirarchy', array('report_heirarchy_id' => $record_id), $update_trash);
        }



        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'reporting_heirarchy',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_report_to/' . $employee_id);
        }
    }

    public function edit_emp_send_2_trash_report_to($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);
        $EmployeeIDToPageRedirect = $this->uri->segment(6);

        $this->db->trans_start();

        $data['skill_trash'] = $this->hr_model->get_row_by_id('reporting_heirarchy', array('report_heirarchy_id' => $record_id));
        if ($data['skill_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('reporting_heirarchy', array('report_heirarchy_id' => $record_id), $update_trash);
        }


        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'reporting_heirarchy',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_report_to/' . $EmployeeIDToPageRedirect);
        }
    }

    /// Trash Function For Training
    public function send_2_trash_training($employee_id = NULL, $record_id = NULL)
    {
        $table_id=$this->uri->segment(5);
        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('training', array('training_record_id' => $record_id));

        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('training', array('training_record_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'training',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_skill/' . $employee_id);
        }
    }


    public function send_2_trash_training_add($employee_id = NULL, $record_id = NULL)
    {
        $table_id=$this->uri->segment(5);
        $this->db->trans_start();

        $data['qualify'] = $this->hr_model->get_row_by_id('training', array('training_record_id' => $record_id));

        if ($data['qualify']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('training', array('training_record_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'training',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_training/' . $employee_id);
        }
    }

    /// Function to Edit Employee Employment
    public function edit_emp_employment($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where_oo = array('employee_id' => $employee_id,'employment.current'=>1);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['experience'] = $this->hr_model->get_row_by_where('emp_experience', $where);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where_oo);
        //echo"<pre>";print_r($data['employment']);die;

        $where_employment = array('employment_id' => $data['employment']->employment_id, 'current' => 1, 'trashed' => 0);
        $where_posting = array('employement_id' => $data['employment']->employment_id, 'current' => 1);
        $where_contract = array('employment_id' => $data['employment']->employment_id, 'current' => 1);

        $data['posting'] = $this->hr_model->get_row_by_where('posting', $where_posting);

        //update This Below Line.
        $data['contract'] = $this->hr_model->get_row_by_where('contract',array('employment_id' => $data['employment']->employment_id));

        $data['position_mgt'] = $this->hr_model->get_row_by_where('position_management', $where_posting);
        //echo"<pre>";print_r($data['position_mgt']);die;
        $data['project'] = $this->hr_model->get_all_by_where('employee_project', $where_employment);
        //echo"<pre>";print_r( $data['project']);die;
        $emp_id = $this->input->post('emp_id');
        $whered = array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Title --', 'ml_designation_id','designation_name',$whered);

        $data['employment_type'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --','employment_type_id', 'employment_type',$whered);

        $data['job_category'] = $this->hr_model->dropdown_wd_option('ml_job_category', '-- Select Job Category --', 'job_category_id','job_category_name',$whered);

        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id','department_name',$whered);

        $data['branch'] = $this->hr_model->dropdown_wd_option('ml_branch', '-- Select Branch --', 'branch_id', 'branch_name',$whered);

        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$whered);

        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --','posting_locations_list_id', 'posting_location',$whered);

        $data['work_shift'] = $this->hr_model->dropdown_wd_option('ml_shift', '-- Select Work Shift --', 'shift_id', 'shift_name',$whered);

        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --','ml_pay_grade_id', 'pay_grade',$whered);
        $where_pro = array('employment_id'=>$data['employment']->employment_id,'current'=> 1);
        $data['approval'] = $this->hr_model->find_projects_for_employee($where_pro);

        if ($this->input->post('continue')) {
            if(isset($data['approval']) && !empty($data['approval'])){
                foreach($data['approval'] as $rec){
                    $data = array('current'=>0);
                    $where_pp = array('project_id'=>$rec->project_id,'employment_id'=>$data['employment']->employment_id,'current'=> 1);
                    $qq = $this->hr_model->update_record('employee_project',$where_pp,$data);
                }
            }
            /*$project_id = $this->input->post('project_id');
            $id=$this->input->post('employee_project_id');
            if (isset($project_id) && !empty($project_id)) {
                foreach ($project_id as $type) {
                    $explode = explode(":", $type);
                    $insert_project = array(
                        'current' => 1
                    );
                    //echo"<pre>"; print_r($insert_project);
                    $where_up = array('employee_project_id' => $explode[1], 'employment_id' => $data['employment']->employment_id, 'current' => 0);
                    $insert_project = $this->hr_model->update_record('employee_project', $where_up, $insert_project);
                }
            }*/



            $postdate =  $this->input->post('joining_date');

            if(isset($postdate) && !empty($postdate))
            {
                $postdate = date('Y-m-d',strtotime($postdate));
            }

            $insert_employment = array(
                'joining_date' => $postdate
            );
            $query_employment = $this->hr_model->update_record('employment', $where, $insert_employment);


            $insert_posting = array(
                'poting_start_date' => $postdate,
                'ml_department_id'  => $this->input->post('department'),
                'ml_branch_id'      => $this->input->post('branch'),
                'ml_city_id'        => $this->input->post('city'),
                'location'          => $this->input->post('location'),
                'shift'             => $this->input->post('work_shift')
            );
            $query_posting = $this->hr_model->update_record('posting', $where_posting, $insert_posting);



            $insert_position_mgt = array(
                'ml_pay_grade_id' => $this->input->post('pay_grade'),
                'ml_designation_id' => $this->input->post('curr_job_title'),
                'job_specifications' => $this->input->post('job_specification'),
                'from_date' => $postdate
            );

            $query_position_mgt = $this->hr_model->update_record('position_management', $where_posting, $insert_position_mgt);
            $probation = $this->input->post('probation');
            $probAlertDate =  $this->input->post('prob_alert_dat');
            if(!isset($probAlertDate) || empty($probAlertDate)){
                //Redirect The User To Previous Page, As He Did Not Entered The Probation Alert Date.
                $msg = "You Need To set the Probation Alert Date::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_employment/' . $emp_id);
                return;
            }
            if(!isset($probation) || empty($probation)){
                //Redirect The User To Previous Page, As He Did Not Entered The Probation Alert Date.
                $msg = "You Need To set the Probation Days::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_employment/' . $emp_id);
                return;
            }


            $contractstartdate =  $this->input->post('contract_start_date');
            $contractexpirydate =  $this->input->post('contract_expiry_date');
            $datecontractexpirydate = $this->input->post('date_contract_expiry_alert');
            if(isset($contractstartdate) && !empty($contractstartdate))
            {
                $contractstartdate = date('Y-m-d',strtotime($contractstartdate));
            }

            if(isset($contractexpirydate) && !empty($contractexpirydate))
            {
                $contractexpirydate = date('Y-m-d',strtotime($contractexpirydate));
            }
            if(isset($probAlertDate) && !empty($probAlertDate))
            {
                $probAlertDate = date('Y-m-d',strtotime($probAlertDate));
            }

            if(isset($datecontractexpirydate) && !empty($datecontractexpirydate))
            {
                $datecontractexpirydate = date('Y-m-d',strtotime($datecontractexpirydate));
            }


            $insert_contract = array(
                'contract_start_date' => $contractstartdate,
                'contract_expiry_date' => $contractexpirydate,
                'contract_exp_alert' => $this->input->post('contract_exp_alert'),
                'prob_alert_dat' => $probAlertDate,
                'probation' => $probation,
                'date_contract_expiry_alert' => $datecontractexpirydate,
                'comments' => $this->input->post('comments')
            );
            //print_r($insert_contract);die;
            $query_contract = $this->hr_model->update_record('contract', $where_contract, $insert_contract);
            if ( (isset($insert_project) && $insert_project === true) || $query_employment || $query_posting || $query_position_mgt || $query_contract)
            {
                redirect('human_resource/edit_emp_dependent/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_employment_info', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Edit Employee Dependents
    public function edit_emp_dependent($employee_id = NULL)
    {
//        error_reporting(0);
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);
            $where = array('employee_id' => $employee_id);

            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
            $data['depend'] = $this->hr_model->get_row_by_where('dependents', $where);

            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --',
                'relation_id', 'relation_name',$whered);

            $where_dep = array('employee_id' => $employee_id, 'dependents.trashed' => 0);
            $data['dependent_details'] = $this->hr_model->emp_dependent_detail($where_dep);

            if ($this->input->post('continue')) {
                $insert_dependent = array(
                    'dependent_name' => $this->input->post('dependent_name'),
                    'ml_relationship_id' => $this->input->post('ml_relationship_id'),
                    'date_of_birth' => $this->input->post('date_of_birth')
                );
                $query_dependent = $this->hr_model->update_record('dependents', $where, $insert_dependent);

                if ($query_dependent) {
                    redirect('human_resource/edit_emp_dependent/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_dependents', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Dependent in Edit View
    public function add_dependent_edt($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --',
            'relation_id', 'relation_name',$whered);

        $where_dep = array('employee_id' => $employee_id, 'dependents.trashed' => 0);
        $data['dependent_details'] = $this->hr_model->emp_dependent_detail($where_dep);



        if ($this->input->post('continue')) {

            $dateofbirth = $this->input->post('date_of_birth');
            if(isset($dateofbirth) && !empty($dateofbirth))
            {
                $dateofbirth = date('Y-m-d',strtotime($dateofbirth));
            }

            $insert_dependent = array(
                'employee_id' => $emp_id,
                'dependent_name' => $this->input->post('dependent_name'),
                'ml_relationship_id' => $this->input->post('ml_relationship_id'),
                'date_of_birth' => $dateofbirth,
                'illness' => $this->input->post('illness'),
                'insurance_start_date' => $this->input->post('insurance_start_date')
            );
            $query_dependent = $this->hr_model->create_new_record('dependents', $insert_dependent);

            if ($query_dependent) {
                redirect('human_resource/edit_emp_dependent/' . $emp_id);
            }
        }
        $this->load->view('includes/header');
        $this->load->view('HR/add_dependents_edt', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_single_dependent($employee_id = NULL, $dependent_id = NULL)
    {
        //error_reporting(0);
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);

            $where = array('employee_id' => $employee_id);
            $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

            $where_dependent = array('dependent_id' => $dependent_id);
            $data['depend'] = $this->hr_model->get_row_by_where('dependents', $where_dependent);

            $emp_id = $this->input->post('emp_id');
            $whered =array('trashed' => 0);
            $data['relation'] = $this->hr_model->dropdown_wd_option('ml_relation', '-- Select Relation --',
                'relation_id', 'relation_name',$whered);

            $where_dep = array('employee_id' => $employee_id, 'dependents.trashed' => 0);
            $data['dependent_details'] = $this->hr_model->emp_dependent_detail($where_dep);

            if ($this->input->post('continue')) {

                $dateofbirth = $this->input->post('date_of_birth');
                if(isset($dateofbirth) && !empty($dateofbirth))
                {
                    $dateofbirth = date('Y-m-d',strtotime($dateofbirth));
                }

                $insert_dependent = array(
                    'dependent_name' => $this->input->post('dependent_name'),
                    'ml_relationship_id' => $this->input->post('ml_relationship_id'),
                    'date_of_birth' => $dateofbirth,
                    'illness' => $this->input->post('illness')
                );
                $query_dependent = $this->hr_model->update_record('dependents', $where_dependent, $insert_dependent);

                if ($query_dependent) {
                    redirect('human_resource/edit_emp_dependent/' . $emp_id);
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_dependents', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Dependent
    public function send_2_trash_dependent($employee_id = NULL, $record_id = NULL)
    {
        $employee_id = $this->uri->segment(3);
        $table_id=$this->uri->segment(5);


        $this->db->trans_start();

        $data['dep_trash'] = $this->hr_model->get_row_by_id('dependents', array('dependent_id' => $record_id));
        if ($data['dep_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('dependents', array('dependent_id' => $record_id), $update_trash);
        } else {
            $update_trash = array('trashed' => 0);
            $query = $this->hr_model->update_record('dependents', array('dependent_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'dependents',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_dependent/' . $employee_id);
        }
    }

    /// Function To Edit Employee Pay Package
    public function edit_emp_pay_package($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where2 = array('employee_id' => $employee_id,'current'=>1);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where2);
        //print_r( $data['employment']);
        //$emp_id = $data['employment']->employment_id;
        $where_salary = array('employement_id' => $data['employment']->employment_id,'status ='=>1);
        $data['salary'] = $this->hr_model->get_row_by_where('salary', $where_salary);
        $whered =array('trashed' => 0);
        $emp_id = $this->input->post('emp_id');
        $employment_id = $this->input->post('employment_id');
        $data['pay_grade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --', 'ml_pay_grade_id', 'pay_grade',$whered);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id',
            'pay_frequency',$whered);
        $data['currency'] = $this->hr_model->dropdown_wd_option('ml_currency', '-- Select Currency --', 'ml_currency_id',
            'currency_name',$whered);
        $data['pay_rate'] = $this->hr_model->dropdown_wd_option('ml_payrate', '-- Select Pay Rate --', 'ml_payrate_id', 'payrate',$whered);

        if ($this->input->post('continue')) {
            $now = date('Y-m-d');

            switch ($base_salary = $this->input->post('ml_pay_rate_id')) {
                case '1' :
                    $base_salary = $this->input->post('base_salary');
                    break;

                case '2' :
                    $base_salary = $this->input->post('weekly_pay');
                    break;

                case '3' :
                    $base_salary = $this->input->post('daily_pay');
                    break;
            }

            $insert_pay_package = array(
                'employement_id' => $data['employment']->employment_id,
                'ml_pay_grade' => $this->input->post('ml_pay_grade'),
                'ml_pay_frequency' => $this->input->post('ml_pay_frequency'),
                'ml_currency' => $this->input->post('ml_currency'),
                'ml_pay_rate_id' => $this->input->post('ml_pay_rate_id'),
                'last_modified_by' => $this->session->userdata('employee_id'),
                'date_modified' => $now,
                'base_salary' => $base_salary
            );
            // echo"<pre>";print_r($insert_pay_package);die;
            $query_pay_package = $this->hr_model->create_new_record('salary', $insert_pay_package);
            if ($query_pay_package) {
                redirect('human_resource/edit_emp_pay_package/' . $emp_id);
            }
        }
        $employee_id = $this->uri->segment(3);
        $employment_id = get_employment_from_employeeID($employee_id);
        $whereEmploymentId = array('employment.employment_id' => $employment_id);

        $data['record'] = $this->hr_model->base_salary($whereEmploymentId);
        $data['rec'] = $this->hr_model->get_row_by_where('salary',array('salary.employement_id'=>$data['employment']->employment_id));
        // echo"<pre>";print_r($data['rec']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/edit_pay_package', $data);
        $this->load->view('includes/footer');
    }
    public function edit_emp_pay_package2($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $where_09 = array('employee_id' => $employee_id,'current' => 1);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where_09);
        $where_salary = array('employement_id' => $data['employment']->employment_id,'status !='=> 2);
        $data['salary'] = $this->hr_model->get_row_by_where('salary', $where_salary);
        //echo"<pre>";print_r($data['salary']);
        $whered =array('trashed' => 0);
        $emp_id = $this->input->post('emp_id');
        $employment_id = $this->input->post('employment_id');
        $data['pay_grade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --', 'ml_pay_grade_id', 'pay_grade',$whered);
        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id',
            'pay_frequency',$whered);
        $data['currency'] = $this->hr_model->dropdown_wd_option('ml_currency', '-- Select Currency --', 'ml_currency_id',
            'currency_name',$whered);
        $data['pay_rate'] = $this->hr_model->dropdown_wd_option('ml_payrate', '-- Select Pay Rate --', 'ml_payrate_id', 'payrate',$whered);

        if ($this->input->post('continue')) {
            $now = date('Y-m-d');

            switch ($base_salary = $this->input->post('ml_pay_rate_id')) {
                case '1' :
                    $base_salary = $this->input->post('base_salary');
                    break;

                case '2' :
                    $base_salary = $this->input->post('weekly_pay');
                    break;

                case '3' :
                    $base_salary = $this->input->post('daily_pay');
                    break;
            }

            $insert_pay_package = array(
                'ml_pay_grade' => $this->input->post('ml_pay_grade'),
                'ml_pay_frequency' => $this->input->post('ml_pay_frequency'),
                'ml_currency' => $this->input->post('ml_currency'),
                'ml_pay_rate_id' => $this->input->post('ml_pay_rate_id'),
                'last_modified_by' => $this->session->userdata('employee_id'),
                'date_modified' => $now,
                'base_salary' => $base_salary,
                'status' => 1
            );

            $query_pay_package = $this->hr_model->update_record('salary', $where_salary, $insert_pay_package);
            if ($query_pay_package) {
                redirect('human_resource/edit_emp_pay_package/' . $emp_id);
            }
        }
        $id = array('employee.employee_id' => $this->uri->segment(3));
        $data['record'] = $this->hr_model->base_salary($id);
        $data['rec'] = $this->hr_model->get_row_by_where('salary',array('salary.employement_id'=>$data['employment']->employment_id,'salary.status !='=> 2));
        //var_dump($data['rec']);
        $this->load->view('includes/header');
        $this->load->view('HR/edit_pay_package2', $data);
        $this->load->view('includes/footer');
    }
    /// ========================= Functions For Emplyee Entitlements Start Here ==============================///
    /// Function to Edit Employee Entitlement (Increments)
    public function edit_emp_entitlement_increment($employee_id = NULL)
    {
        if(!isset($employee_id)){
            echo "Have You Missed Some Thing, Page Not Found";
            return;
        }
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment' , array('employee_id'=>$employee_id,'current' =>1,'trashed' =>0));

        $where_increm = array('increments.employment_id' => $data['empl']->employment_id, 'increments.trashed' => 0);
        $where_allow =  array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $where_benef =  array('benefits.employment_id' => $data['empl']->employment_id,'benefits.trashed' => 0);
        $where_leave =  array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);
        //print_r($where_leave);die;
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);
        //echo "<pre>";print_r($data['increment_details']);die;
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['in'] = $this->hr_model->get_row_by_where('increments', $where_increm);
        $data['al'] = $this->hr_model->get_row_by_where('allowance', $where_allow);
        $data['be'] = $this->hr_model->get_row_by_where('benefits', $where_benef);
        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);
        if(isset($data['in']) && !empty($data['in'])){
            $data['increment'] = $this->hr_model->get_row_by_where('increments', array('increment_id' => $data['in']->increment_id));
        }
        $data['allowance'] = @$this->hr_model->get_row_by_where('allowance', array('allowance_id' => $data['al']->allowance_id));
        $data['benefit'] = @$this->hr_model->get_row_by_where('benefits', array('emp_benefit_id' => $data['be']->emp_benefit_id));
        $data['leave'] = @$this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $data['le']->leave_entitlement_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);
        $data['insurance_type'] = $this->hr_model->dropdown_wd_option('ml_insurance_type', '-- Select Insurance Type --', 'insurance_type_id',
            'insurance_type_name',$whered);

        if ($this->input->post('add_increment')) {
            $insert_increments = array(
                'increment_type_id' => $this->input->post('increment_type_id'),
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $this->input->post('date_effective'),
                'last_modified_by' => $this->session->userdata('employee_id'),
                'modify_date' => date('Y-m-d')
            );
            $query_increments = $this->hr_model->update_record('increments', $where, $insert_increments);
            if ($query_increments) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {
            $now = date('Y-m-d');
            $login_user = $this->session->userdata('employee_id');

            $insert_allowance = array(
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $this->input->post('allowance_amount'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_last_modified' => date('Y-m-d')
            );
            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->update_record('allowance', $where, $insert_allowance);
            if ($query_allowance) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {
            $insert_benefit = array(
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                'date_effective' => $this->input->post('date_effective'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modified' => date('Y-m-d')
            );
            $query_benefit = $this->hr_model->update_record('benefits', $where, $insert_benefit);
            if ($query_benefit) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }



        if ($this->input->post('add_insurance')) {
            //Get Records For Insurance.
            $employmentID = $data['empl']->employment_id;
            $insuranceTypeID = $this->input->post('insurance_type_id');
            $dateEffective =  $this->input->post('effective_from');;
            $policyNo =   $this->input->post('insurance_no');
            $certificateNo =   $this->input->post('certificate_number');

            if(!isset($insuranceTypeID) || !is_numeric($insuranceTypeID) || empty($insuranceTypeID)){
                $msg ="Please Select Insurance Type !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            }

            if(!isset($dateEffective) || empty($dateEffective)){
                $msg ="Please Select Effective Date !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            }else{
                $date = date_parse($dateEffective);
                if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
                {
                    $dateEffective = date('Y-m-d',strtotime($dateEffective));
                }
                else
                {
                    $msg ="Invalid Date Entered !::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                    return;
                }

            }

            if(!isset($policyNo) || empty($policyNo)){
                $msg ="Please Fill Policy Number !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            }

            //First Need To Check If Record Already Exist Or Not.
            $table = 'employee_insurance';
            $selectData = array('COUNT(1) AS TotalRecords',false);
            $where = array(
                'employment_id' => $employmentID,
                'trashed' => 0,
                'ml_insurance_id' => $insuranceTypeID
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ="Record Already Exist For Selected Insurance Type !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            }


            $date_effective = $this->input->post('effective_from');

            if(isset($date_effective) && !empty($date_effective))
            {
                $date_effective = date('Y-m-d',strtotime($date_effective));
            }


            //Back To Below Hamid's Code. As Hopefully Below Code Will Be OK..
            $insert_insurance = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_insurance_id' => $this->input->post('insurance_type_id'),
                'policy_number' =>  $this->input->post('insurance_no'),
                'certificate_number' =>  $this->input->post('certificate_number'),
                'effective_from' => $date_effective
            );
            // print_r($insert_insurance);die;
            $query_insurance = $this->hr_model->create_new_record('employee_insurance', $insert_insurance);
            $id = $this->uri->segment(3);
            if ($query_insurance) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $id);
            }
        }

        /// Function to Update Leave
        if ($this->input->post('add_leave')) {
            $insert_leave = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_leave_type_id' => $this->input->post('ml_leave_type_id'),
                'no_of_leaves_allocated' => $this->input->post('no_of_leaves_allocated'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modfied' => date('Y-m-d')
            );
            $query_leave = $this->hr_model->update_record('leave_entitlement', $where, $insert_leave);
            if ($query_leave) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        //echo $this->uri->segment(3);die;
        $insuranceWhere = array('employee_insurance.employment_id' => $data['empl']->employment_id,'employment.current'=>1);
        $data['insurance'] = $this->hr_model->insurance_employee($insuranceWhere);
        //echo"<pre>";print_r( $data['insurance']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/edit_entitlements',$data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Increments in Edit View
    public function add_increment_edt($employee_id = NULL)
    {
        $data['employee_id'] = $employee_id;
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_increm = array('increments.employment_id' =>$data['empl']->employment_id, 'increments.trashed' => 0);
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);
        $emp_id = $this->input->post('emp_id');

        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);

        if ($this->input->post('add_increment')) {
            //Get All the Posted Data.
            $incrementTypeID = $this->input->post('increment_type_id');
            $incrementAmount = $this->input->post('increment_amount');
            $dateEffective = $this->input->post('date_effective');

            if(!isset($dateEffective) || empty($dateEffective)){
                $msg ='Please Select Date !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_increment_edt/' . $emp_id);
                return;
            }else{
                $dateEffective = date('Y-m-d',strtotime($dateEffective));
            }
            if(!is_numeric($incrementAmount)){
                $msg ='Please Provide Numeric Value For increment !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_increment_edt/' . $emp_id);
                return;
            }

//            $data['src'] = $this->hr_model->get_all_by_where('increments',array('employee_id'=>$emp_id,'trashed'=>0));
            //Need To Check if Record Already Exist..
            $table = 'increments';
            $selectData = 'COUNT(1) AS TotalRecords';
            $where = array(
                'employment_id' => $data['empl']->employment_id,
                'trashed' => 0,
                'increment_type_id' => $incrementTypeID
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ="<span style='background-color: red;'>Record Already Exist !";
                $this->session->set_flashdata('txt',$msg);
                redirect('human_resource/add_increment_edt/' . $emp_id);
                return;
            }

            $insert_increments = array(
                'employment_id' => $data['empl']->employment_id,
                'increment_type_id' => $incrementTypeID,
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $dateEffective,
                'created_by' => $this->data['EmployeeID'],
                'date_created' => date('Y-m-d'),
                'status' => 1
            );
            $query_increments = $this->hr_model->create_new_record('increments', $insert_increments);
            if ($query_increments) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_increments_edt', $data);
        $this->load->view('includes/footer');
    }

    ////////////////////////////////////function for benefit add increnent////

    public function add_benefit_increment($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        //echo "";print_r($data['employee']);die;

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_increm = array('increments.employment_id' => $data['empl']->employment_id, 'increments.trashed' => 0);
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);
        //echo "";print_r($data['increment_details']);die;
        $emp_id = $this->uri->segment(3);
        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);

        if ($this->input->post('add_increment')) {
            $dateeffective  = $this->input->post('date_effective');
            if(isset($dateeffective) && !empty($dateeffective)){
                $dateeffective = date('Y-m-d',strtotime($dateeffective));
            }
            $insert_increments = array(
                'employment_id' => $data['empl']->employment_id,
                'increment_type_id' => $this->input->post('increment_type_id'),
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $dateeffective,
                'created_by' => $this->session->userdata('employee_id'),
                'date_created' => date('Y-m-d'),
                'status' => 1
            );
            //echo "<pre>"; print_r($insert_increments); die;
            $query_increments = $this->hr_model->create_new_record('increments', $insert_increments);
            if ($query_increments) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_benefit_increment', $data);
        $this->load->view('includes/footer');

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function add_allowance_benefit($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
        // print_r($data['empl']);die;

        $where_allow = array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);

        $emp_id = $this->uri->segment(3);
        $whered =array('trashed' => 0);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);

        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id',
            'pay_frequency',$whered);

        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {

            $dateeffective = $this->input->post('effective_date');
            if(isset($dateeffective) && !empty($dateeffective))
            {
                $dateeffective = date('Y-m-d',strtotime($dateeffective));
            }


            $insert_allowance = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $this->input->post('allowance_amount'),
                'pay_frequency' => $this->input->post('pay_frequency'),
                'effective_date' => $dateeffective,
                'created_by' => $this->session->userdata('employee_id'),
                'date_rec_created' => date('Y-m-d')
            );

            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->create_new_record('allowance', $insert_allowance);
            if ($query_allowance) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_allowance_benefit', $data);
        $this->load->view('includes/footer');
    }

    ///////////////////////////////////////////////////////////////////////////

    public function add_benefit_ben($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;


        $where_benef = array('benefits.employment_id' =>  $data['empl']->employment_id, 'benefits.trashed' => 0);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$whered);

        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {

            $dateeffective  = $this->input->post('date_effective');
            if(isset($dateeffective) && !empty($dateeffective)){
                $dateeffective = date('Y-m-d',strtotime($dateeffective));
            }

            $insert_benefit = array(
                'employment_id' => $data['empl']->employment_id,
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                //'benefits_list_id' => $this->input->post('benefits_list_id'),
                'date_effective' => $dateeffective,
                'created_by' => $this->session->userdata('employee_id'),
                'date_rec_created' => date('Y-m-d'),
                'status' => 1
            );
            //echo "<pre>";print_r($insert_benefit);die;
            $query_benefit = $this->hr_model->create_new_record('benefits', $insert_benefit);
            if ($query_benefit) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_benefits_ben', $data);
        $this->load->view('includes/footer');
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function add_leave_benefit($employee_id = NULL, $single_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        //echo"";print_r($data['employee']);die;

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_leave = array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);

        $data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $single_id));

        $emp_id = $this->uri->segment(3);
        $whered =array('trashed' => 0);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);

        /// Function to Update Leave
        if ($this->input->post('add_leave')) {
            $insert_leave = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_leave_type_id' => $this->input->post('ml_leave_type_id'),
                'no_of_leaves_allocated' => $this->input->post('no_of_leaves'),
                'status' => 1
            );
            ///echo "<pre>";print_r($insert_leave);die;
            $query_leave = $this->hr_model->create_new_record('leave_entitlement', $insert_leave);
            if ($query_leave) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_leave_ben', $data);
        $this->load->view('includes/footer');
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// Function To Add Employee Allowance in Edit View
    public function add_allowance_edt($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
        //print_r($data['empl']);die;
        $where_allow = array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);

        $data['pay_frequency'] = $this->hr_model->dropdown_wd_option('ml_pay_frequency', '-- Select Pay Frequency --', 'ml_pay_frequency_id',
            'pay_frequency',$whered);

        ///////////////

        /*   $employmentTable = 'employment';
           $selectData = 'employment_id AS EmploymentID';
           $where = array(
               'current' => 1,
               'trashed' => 0,
               'employee_id' => $emp_id
           );
           $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
           //print_r($employmentInfo);die;
           $employmentID = $employmentInfo->EmploymentID;
           //*/



        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {

            //Assigning Posted Variables.
            $allowanceTypeID = $this->input->post('ml_allowance_type_id');
            $allowanceAmount = $this->input->post('allowance_amount');
            $payFrequencyID = $this->input->post('pay_frequency');
            $effectiveDate = $this->input->post('effective_date');


            if(!isset($allowanceTypeID) || empty($allowanceTypeID) || !is_numeric($allowanceTypeID)){
                $msg ='Please Select Allowance Type !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }

            if(!isset($effectiveDate) || empty($effectiveDate)){
                $msg ='Please Provide Date !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }else{
                $effectiveDate = date('Y-m-d',strtotime($effectiveDate));
            }

            if(!isset($allowanceAmount) || !is_numeric($allowanceAmount)){
                $msg ='Please Provide Numeric Value To Be Enter !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }

            if(!isset($payFrequencyID) || empty($payFrequencyID)){
                $msg ='Please Select Pay Frequency From Drop Down !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }

            //To Add New Allowance We First Need To Know If Allowance to Certain Type Already Exist or Not.
            //If Exist Then We Can Not Add Another Record..

            $table = 'allowance';
            $selectData = array('COUNT(1) AS TotalRecords',false);
            $where = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_allowance_type_id' => $allowanceTypeID,
                'trashed' => 0
            );

            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ='Record Already Exist !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }

            $insert_allowance = array(
                'employment_id' => $data['empl']->employment_id,
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $allowanceAmount,
                'pay_frequency' => $payFrequencyID,
                'effective_date' => $effectiveDate,
                'created_by' => $this->data['EmployeeID'],
                'date_rec_created' => $this->data['dbCurrentDate']
            );

            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->create_new_record('allowance', $insert_allowance);
            if ($query_allowance) {
                $msg ='Record Successfully Inserted !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_allowance_edt', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Benefit in Edit View
    public function add_benefit_edt($employee_id = NULL)
    {
        $data['employee_id'] = $employee_id;
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_benef = array('benefits.employment_id' => $data['empl']->employment_id, 'benefits.trashed' => 0);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);
        //print_r( $data['benefit_details']);die;
        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$whered);

        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {
            //Setting Up Variables.
            $benefitTypeID = $this->input->post('benefit_type_id');
            $dateEffective = $this->input->post('date_effective');





            if(!isset($benefitTypeID) || empty($benefitTypeID)){
                $msg ="Please Select Benefit Type !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_benefit_edt/' . $emp_id);
                return;
            }

            if(!isset($dateEffective) || empty($dateEffective)){
                $msg ='Please Select Date !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_increment_edt/' . $emp_id);
                return;
            }else{
                $dateEffective = date('Y-m-d',strtotime($dateEffective));
            }


            //Need To Check If Record Already Exist.
            $table = 'benefits';
            $selectData = array('COUNT(1) AS TotalRecords',false);
            $where = array(
                'employment_id' =>  $data['empl']->employment_id,
                'benefit_type_id' => $benefitTypeID,
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords > 0){
                $msg ='Record Already Exist !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_benefit_edt/' . $emp_id);
                return;
            }
            //To Insert We First Need To Find Employment ID For The Given EmpoyeeID
            $employmentTable = 'employment';
            $selectData = 'employment_id AS EmploymentID';
            $where = array(
                'current' => 1,
                'trashed' => 0,
                'employee_id' => $emp_id
            );
            $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
            $employmentID = $employmentInfo->EmploymentID;

            $insert_benefit = array(
                'employment_id' => $employmentID,
                'benefit_type_id' => $benefitTypeID,
                'date_effective' => $dateEffective,
                'created_by' => $this->data['EmployeeID'],
                'date_rec_created' => date('Y-m-d'),
                'status' => 1
            );
            $query_benefit = $this->hr_model->create_new_record('benefits', $insert_benefit);

            if ($query_benefit) {
                $msg ='Record Successfully Inserted !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/add_benefits_edt', $data);
        $this->load->view('includes/footer');
    }

    /// Function To Add Employee Leave in Edit View
    public function add_leave_edt($employee_id = NULL, $single_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment' , array('employee_id'=>$employee_id,'current' =>1,'trashed' =>0));

        $where_leave = array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);

        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);

        $data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);

        /// Function to Update Leave
        if ($this->input->post('add_leave')) {
            $totalAllocatedLeaves = $this->input->post('no_of_leaves');
            $LeaveTypeID = $this->input->post('ml_leave_type_id');

            if(!isset($LeaveTypeID) || empty($LeaveTypeID) || !is_numeric($LeaveTypeID)){
                $msg = "Please Select Leave Type::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_leave_edt/'.$employee_id);
                return;
            };

            if(!isset($totalAllocatedLeaves) || empty($totalAllocatedLeaves) || !is_numeric($totalAllocatedLeaves)){
                $msg = "Please Provide Total Entitled leaves Information in Numeric::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_leave_edt/'.$employee_id);
                return;
            };

            //To Insert We First Need To Check If Same Type Of Record Already Don't Exist For Current Employee.
            $table = 'leave_entitlement';
            $selectData = array('COUNT(1) AS TotalRecords',false);
            $where = array(
                'employment_id' => $data['empl']->employment_id,
                'trashed' => 0,
                'ml_leave_type_id' => $LeaveTypeID
            );
            $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
            if(isset($countResult) && $countResult->TotalRecords >0){
                $msg = "Record Already Exist For Same Type::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_leave_edt/'.$employee_id);
                return;
            }





            //To Insert We First Need To Find Employment ID For The Given EmpoyeeID
            $employmentTable = 'employment';
            $selectData = 'employment_id AS EmploymentID';
            $where = array(
                'current' => 1,
                'trashed' => 0,
                'employee_id' => $emp_id
            );
            $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
            $employmentID = $employmentInfo->EmploymentID;



            //GET contract start date and In date
            $employmentTable = 'contract C';
            $selectData = array('IFNULL(ECE.e_end_date,C.contract_expiry_date) AS ContractExpiryDate,IFNULL(ECE.e_start_date,C.contract_start_date) AS ContractStartDate',false);
            $joins = array(
                array(
                    'table' => 'employee_contract_extensions ECE',
                    'condition' =>'C.contract_id = ECE.contract_id AND ECE.current = 1 AND ECE.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'C.status'=> 2,
                'C.trashed' => 0,
                'employment_id' => $employmentID
            );
            $employmentInfo = $this->common_model->select_fields_where_like_join($employmentTable,$selectData,$joins,$where,TRUE);

            /*   var_dump($employmentInfo);
                     return;*/
            // $employmentID = $employmentInfo->EmploymentID;

            //$contract start Date = $employmentInfo->ContractStartDate;
            if(isset($employmentInfo) && !empty($employmentInfo))
            {
                // Need to find leave entitlements before insertion
                $currentYear = date('Y');
                $totalDaysInCurrentYear = date("z", mktime(0,0,0,12,31,$currentYear)) + 1;
                $contractStartDate = strtotime($employmentInfo->ContractStartDate);

                if (date('Y', $contractStartDate) === date('Y')) {

                    $lastDayOfYear = strtotime($currentYear . '-12-31');
                    $unContractDays = floor(($lastDayOfYear - $contractStartDate) / (60 * 60 * 24));
                    /*                    $contractDays = $totalDaysInCurrentYear - $unContractDays;*/
                    $totalEntitledLeaves = $totalAllocatedLeaves/$totalDaysInCurrentYear;
                    $availableEntitledLeaves = $totalEntitledLeaves * $unContractDays;
                }
            }else{
                $availableEntitledLeaves = 0;
            }

            $insert_leave = array(
                'employment_id' => $employmentID,
                'ml_leave_type_id' => $LeaveTypeID,
                'no_of_leaves_allocated' => $totalAllocatedLeaves,
                'no_of_leaves_entitled' => $availableEntitledLeaves,
                'status' => 1
            );
            $query_leave = $this->hr_model->create_new_record('leave_entitlement', $insert_leave);
            if ($query_leave) {
                $msg = "Record Successfully Added To The System::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        $this->load->view('includes/header');
        $this->load->view('HR/add_leave_edt', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_single_increment($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_increm = array('increments.employment_id' => $data['empl']->employment_id, 'increments.trashed' => 0);
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);


        $data['in'] = $this->hr_model->get_row_by_where('increments', $where_increm);
        $data['increment'] = $this->hr_model->get_row_by_where('increments', array('increment_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);

        if ($this->input->post('add_increment')) {

            $IncrementAmount = $this->input->post('increment_amount');

            if(!isset($IncrementAmount) || !is_numeric($IncrementAmount)){
                $msg ='Please Provide Numeric Value To Be Enter !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
                return;
            }

            $insert_increments = array(
                'increment_type_id' => $this->input->post('increment_type_id'),
                'increment_amount' => $IncrementAmount,
                'date_effective' => $this->input->post('date_effective'),
                'last_modified_by' => $this->session->userdata('employee_id'),
                'modify_date' => date('Y-m-d'),
            );
            //echo "<pre>"; print_r($insert_increments);die;
            $query_increments = $this->hr_model->update_record('increments', array('increment_id' => $single_id), $insert_increments);
            if ($query_increments) {
                $msg ='Record Successfully Updated !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_increment', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_single_allowance($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;


        $where_allow = array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);

        $data['al'] = $this->hr_model->get_row_by_where('allowance', $where_allow);

        $data['allowance'] = $this->hr_model->get_row_by_where('allowance', array('allowance_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);

        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {
            $now = date('Y-m-d');
            $login_user = $this->session->userdata('employee_id');
            $allowanceAmount = $this->input->post('allowance_amount');

            if(!isset($allowanceAmount) || !is_numeric($allowanceAmount)){
                $msg ='Please Provide Numeric Value To Be Enter !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/add_allowance_edt/' . $emp_id);
                return;
            }
            $insert_allowance = array(
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $allowanceAmount,
                'modified_by' => $this->session->userdata('employee_id'),
                'date_last_modified' => date('Y-m-d')
            );
            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->update_record('allowance', array('allowance_id' => $single_id), $insert_allowance);
            if ($query_allowance) {
                $msg ='Record Successfully Updated !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_allowance', $data);
        $this->load->view('includes/footer');
    }

    public function edit_single_benefit($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_benef = array('benefits.employment_id' => $data['empl']->employment_id, 'benefits.trashed' => 0);

        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);


        $data['be'] = $this->hr_model->get_row_by_where('benefits', $where_benef);

        $data['benefit'] = $this->hr_model->get_row_by_where('benefits', array('emp_benefit_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$whered);

        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {

            $dateEffective = $this->input->post('date_effective');
            if(!isset($dateEffective) || empty($dateEffective)){
                $msg ='Please Select Date For Benefit !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
                return;
            }else{
                $dateEffective = date('Y-m-d',strtotime($dateEffective));
            }
            $insert_benefit = array(
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                'date_effective' => $dateEffective,
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modified' => date('Y-m-d')
            );
            $query_benefit = $this->hr_model->update_record('benefits', array('emp_benefit_id' => $single_id), $insert_benefit);
            if ($query_benefit) {
                $msg ='Record Successfully Updated !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_benefits', $data);
        $this->load->view('includes/footer');
    }

    public function edit_single_leave_type($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment' , array('employee_id'=>$employee_id,'current' =>1,'trashed' =>0));


        $where_leave = array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);

        $data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);




        /// Function to Update Leave
        if ($this->input->post('add_leave')) {

            $totalAllocatedLeaves = $this->input->post('no_of_leaves');
            $LeaveTypeID = $this->input->post('ml_leave_type_id');

            if(!isset($LeaveTypeID) || empty($LeaveTypeID) || !is_numeric($LeaveTypeID)){
                $msg = "Please Select Leave Type::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            };

            if(!isset($totalAllocatedLeaves) || empty($totalAllocatedLeaves) || !is_numeric($totalAllocatedLeaves)){
                $msg = "Please Provide Total Entitled leaves Information in Numeric::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
                return;
            };

            $insert_leave = array(
                // 'employment_id' => $emp_id,
                'ml_leave_type_id' => $LeaveTypeID,
                'no_of_leaves_allocated' => $totalAllocatedLeaves,
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modfied' => date('Y-m-d')
            );
            $query_leave = $this->hr_model->update_record('leave_entitlement', array('leave_entitlement_id' => $single_id), $insert_leave);
            if ($query_leave) {
                $msg = "Record Successfully Updated::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_leave_type', $data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_single_entitlement_increment($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $where_increm = array('increments.employee_id' => $employee_id, 'increments.trashed' => 0);
        $where_allow = array('allowance.employee_id' => $employee_id, 'allowance.trashed' => 0);
        $where_benef = array('benefits.employee_id' => $employee_id, 'benefits.trashed' => 0);
        $where_leave = array('leave_entitlement.employee_id' => $employee_id, 'leave_entitlement.trashed' => 0);

        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);


        $data['in'] = $this->hr_model->get_row_by_where('increments', $where);
        $data['al'] = $this->hr_model->get_row_by_where('allowance', $where);
        $data['be'] = $this->hr_model->get_row_by_where('benefits', $where);
        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where);

        $data['increment'] = $this->hr_model->get_row_by_where('increments', array('increment_id' => $single_id));
        $data['allowance'] = $this->hr_model->get_row_by_where('allowance', array('allowance_id' => $single_id));
        $data['benefit'] = $this->hr_model->get_row_by_where('benefits', array('emp_benefit_id' => $single_id));
        $data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$whered);

        if ($this->input->post('add_increment')) {
            $insert_increments = array(
                'increment_type_id' => $this->input->post('increment_type_id'),
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $this->input->post('date_effective'),
            );
            $query_increments = $this->hr_model->update_record('increments', array('increment_id' => $single_id), $insert_increments);
            if ($query_increments) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {
            $now = date('Y-m-d');
            $login_user = $this->session->userdata('employee_id');

            $insert_allowance = array(
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $this->input->post('allowance_amount')
            );
            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->update_record('allowance', array('allowance_id' => $single_id), $insert_allowance);
            if ($query_allowance) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {
            $insert_benefit = array(
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                'benefits_list_id' => $this->input->post('benefits_list_id'),
                'date_effective' => $this->input->post('date_effective'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modified' => date('Y-m-d')
            );
            $query_benefit = $this->hr_model->update_record('benefits', array('emp_benefit_id' => $single_id), $insert_benefit);
            if ($query_benefit) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }
        /// Function to Update Leave
        if ($this->input->post('add_leave')) {
            $insert_leave = array(
                'employee_id' => $this->input->post('emp_id'),
                'ml_leave_type_id' => $this->input->post('ml_leave_type_id'),
                'no_of_leaves' => $this->input->post('no_of_leaves'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modfied' => date('Y-m-d')
            );
            $query_leave = $this->hr_model->update_record('leave_entitlement', array('leave_entitlement_id' => $single_id), $insert_leave);
            if ($query_leave) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_entitlements', $data);
        $this->load->view('includes/footer');
    }
    /// ======================== Entitlements Trash Function Start Here ==================================///
    /// Trash Function For Entitlements
    public function send_2_trash_increment($employee_id = NULL, $record_id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();

        $data['incr_trash'] = $this->hr_model->get_row_by_id('increments', array('increment_id' => $record_id));
        if ($data['incr_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('increments', array('increment_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'increments',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }
    }

    /// Trash Function For Entitlements
    public function send_2_trash_allowance($employee_id = NULL, $record_id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();

        $data['allow_trash'] = $this->hr_model->get_row_by_id('allowance', array('allowance_id' => $record_id));
        if ($data['allow_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('allowance', array('allowance_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'allowance',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }
    }

    /// Trash Function For Entitlements
    public function send_2_trash_benefit($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $data['benef_trash'] = $this->hr_model->get_row_by_id('benefits', array('emp_benefit_id' => $record_id));
        if ($data['benef_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('benefits', array('emp_benefit_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'benefits',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }
    }

    /// Trash Function For Entitlements
    public function send_2_trash_leave($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $data['leave_trash'] = $this->hr_model->get_row_by_id('leave_entitlement', array('leave_entitlement_id' => $record_id));
        if ($data['leave_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('leave_entitlement', array('leave_entitlement_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'leave_entitlement',
            'table_id_name'=>'leave_entitlement_id',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }
    }
    /// ========================= Entitlements Trash Function End Here ==================================///
    /// ========================= Functions For Emplyee Entitlements End Here ==============================///
    /// Function to Edit Employee Report
    public function edit_emp_report_to($employee_id = NULL, $ajaxType = NULL)
    {
        if($employee_id === NULL || empty($employee_id) || !is_numeric($employee_id)){
            $msg = 'Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
            $this->session->set_flashdata('msg',$msg);
            return;
        }
        $data['employee_id'] = $employee_id;
        $where = array('employee_id' => $employee_id);
        $where_report_2 = array('employeed_id' => $employee_id);
        $wheresb =array('reporting_authority_id' =>$employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['report_method'] = $this->hr_model->dropdown_wd_option('ml_reporting_options', '-- Select Report Method --',
            'reporting_option_id', 'reporting_option',$whered);

        if ($this->input->post('add_supervisor')) {
            $supervisor_id = $this->input->post('supervisor_id');

            //Little Fix For Add........
            $table = 'reporting_heirarchy';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'for_employee' => $employee_id,
                'employeed_id' => $supervisor_id,
                'ml_reporting_heirarchy_id' => $this->input->post('supervisor_method'),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_report_to/'. $emp_id);
                return;
            }


            $insert_report_to_sup = array(
                'for_employee' => $employee_id,
                'employeed_id' => $supervisor_id,
                'ml_reporting_heirarchy_id' => $this->input->post('supervisor_method')
            );
            $query_supervisor = $this->hr_model->create_new_record('reporting_heirarchy', $insert_report_to_sup);

            if ($query_supervisor) {
                $msg ="Record Successfully Inserted::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_report_to/' . $emp_id);
            }
        }
        if ($this->input->post('add_sub_ordinate')) {
            $sub_ordinate_id = $this->input->post('SubOrdinateEmployeeID');

            //Little Fix For Add........
            $table = 'reporting_heirarchy';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'for_employee' => $employee_id,
                'reporting_authority_id' => $sub_ordinate_id,
                'ml_reporting_heirarchy_id' => $this->input->post('subordinate_method'),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_report_to/'. $emp_id);
                return;
            }

            $insert_report_to_sub = array(
                'for_employee' => $employee_id,
                'reporting_authority_id' => $sub_ordinate_id,
                'ml_reporting_heirarchy_id' => $this->input->post('subordinate_method'),
            );
            // print_r($insert_report_to_sub);die;
            $query_suordinate = $this->hr_model->create_new_record('reporting_heirarchy', $insert_report_to_sub);

            if ($query_suordinate)
            {
                $msg ="Record Successfully Inserted::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_report_to/' . $emp_id);
            }
        }
        $wheredd = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.for_employee' =>$employee_id);
        $data['record'] = $this->hr_model->record_report_view_table($wheredd);
        /*        $where2 = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.for_employee '=>$employee_id);
                $data['reports'] = $this->hr_model->record_report_view_table2($where2);*/
        //New Code, Hamid Code is Buggy ATM(At The Moment).
        //My Joining Code Starts Here.. SHH
        $table = 'reporting_heirarchy RH';
        $selectData = array('RH.report_heirarchy_id AS ReportHierarchyID, RHFE.employee_id AS ForEmployeeID, RHFE.full_name AS ForEmployeeName, RHSO.employee_id AS SubOrdinateEmployeeID,RHSO.full_name AS SubOrdinateEmployeeName,MLRO.reporting_option AS ReportingType',false);
        $joins = array(
            array(
                'table' => 'employee RHFE',
                'condition' => 'RHFE.employee_id = RH.for_employee AND RHFE.enrolled = 1 AND RHFE.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee RHSO',
                'condition' => 'RHSO.employee_id = RH.reporting_authority_id AND RHSO.trashed = 0 AND RHSO.enrolled = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_reporting_options MLRO',
                'condition' =>'MLRO.reporting_option_id = RH.ml_reporting_heirarchy_id AND MLRO.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where =array(
            'RH.for_employee' => $employee_id,
            'RH.trashed' => 0
        );
        $data['subOrdinates'] = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,FALSE);
        /*        echo "<pre>";
                print_r($data['subOrdinates']);
        //        echo "</pre>";*/
        //End Of My Code.
        $this->load->view('includes/header');
        $this->load->view('HR/edit_report_to', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Edit Employee Attachments
    public function edit_emp_attachment($employee_id = NULL)
    {
//        error_reporting(0);
        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['attach'] = $this->hr_model->get_row_by_where('attachment', $where);

        $data['attach_single'] = $this->hr_model->get_row_by_where('attachment', array('attachment_id' => $this->input->post('attach_id')));

        $emp_id = $this->input->post('emp_id');

        $where_atta = array('employee_id' => $employee_id, 'attachment.trashed' => 0);
        $data['attach_details'] = $this->hr_model->get_all_by_where('attachment', $where_atta);

        if ($this->input->post('continue')) {
            $file = $this->hr_model->do_upload('attached_file');
            $file_name = $file['upload_data']['file_name'];
            //echo "<pre>"; print_r($file);die;

            $insert_attachments = array(
                'employee_id' => $emp_id,
                'attached_file' => $file_name,
                'remarks' => $this->input->post('remarks')
            );
            $query_attachments = $this->hr_model->update_record('attachment', $where, $insert_attachments);

            if ($query_attachments) {
                redirect('human_resource/edit_emp_attachment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_attachments', $data);
        $this->load->view('includes/footer');
    }

    /// Function to Edit Employee Attachments
    public function edit_single_attachment($employee_id = NULL, $attach_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['attach'] = $this->hr_model->get_row_by_where('attachment', $where);
        $data['attach_single'] = $this->hr_model->get_row_by_where('attachment', array('attachment_id' => $attach_id));

        $emp_id = $this->input->post('emp_id');

        $where_atta = array('employee_id' => $employee_id, 'attachment.trashed' => 0);
        $data['attach_details'] = $this->hr_model->get_all_by_where('attachment', $where_atta);

        if ($this->input->post('continue')) {
            $file = $this->hr_model->do_upload('attached_file');
            $file_name = $file['upload_data']['file_name'];

            if ($file_name == '') {
                $file_name = $this->input->post('uploaded_file');

            } else {
                $file_name = $file['upload_data']['file_name'];

            }

            if(!isset($file_name) || empty($file_name)){
                $msg ='This File is Restricted  !::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_attachment/' . $emp_id);
                return;
            }

            $insert_attachments = array(
                'employee_id' => $emp_id,
                'attached_file' => $file_name,
                'remarks' => $this->input->post('remarks')
            );
            $query_attachments = $this->hr_model->update_record('attachment', array('attachment_id' => $attach_id), $insert_attachments);

            if ($query_attachments) {
                $msg ='Successfully Inserted !::success';
                $this->session->set_flashdata('msg',$msg);
                redirect('human_resource/edit_emp_attachment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_attachments_single', $data);
        $this->load->view('includes/footer');
    }

    /// Trash Function For Attachments
    public function send_2_trash_attachment_add($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $data['attach_trash'] = $this->hr_model->get_row_by_id('attachment', array('attachment_id' => $record_id));
        if ($data['attach_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('attachment', array('attachment_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'attachment',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_emp_attachment/' . $employee_id);
        }
    }

    public function send_2_trash_attachment($employee_id = NULL, $record_id = NULL)
    {

        $this->db->trans_start();

        $data['attach_trash'] = $this->hr_model->get_row_by_id('attachment', array('attachment_id' => $record_id));
        if ($data['attach_trash']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('attachment', array('attachment_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_name' => 'attachment',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_attachment/' . $employee_id);
        }
    }

    public function view_retired_profile($employee_id = NULL)
    {
//        error_reporting(0);
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $where_con = array('current_contacts.employee_id' => $employee_id);
        $where_emr = array('emergency_contacts.employee_id' => $employee_id);
        $where_q = array('qualification.employee_id' => $employee_id);
        $where_exp = array('emp_experience.employee_id' => $employee_id);

        $where_emp = array('emp_skills.employment_id' => $employee_id);
        $where_emply = array('employment.employee_id' => $employee_id);
        $where_p = array('position_management.employement_id' => $employee_id);
        $where_d = array('dependents.employee_id' => $employee_id);
        $where_ay = array('employee.employee_id' => $employee_id);

        //$data['complete_profile'] = $this->hr_model->view_complete_profile($where);

        $data['personal_info'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['gender_etc'] = $this->hr_model->join_for_gender_etc($where);
        $data['cont'] = $this->hr_model->get_row_by_where('current_contacts', $where_con);
        $data['contact'] = $this->hr_model->join_for_contact($where_con);
        $data['emr'] = $this->hr_model->get_row_by_where('emergency_contacts', $where_emr);
        $data['jn_emr'] = $this->hr_model->join_for_rel($where_emr);
        $data['qua'] = $this->hr_model->get_row_by_where('qualification', $where_q);
        $data['exp'] = $this->hr_model->get_row_by_where('emp_experience', $where_exp);
        $data['jn_exp'] = $this->hr_model->join_for_exp($where_exp);

        $data['skill'] = $this->hr_model->get_row_by_where('emp_skills', $where_emp);
        $data['jn_skl'] = $this->hr_model->join_for_skl($where_emp);
        $data['emply'] = $this->hr_model->get_row_by_where('employment', $where_emply);
        $data['emplyeee'] = $this->hr_model->detl_employeeee($where_p);
        $data['depend'] = $this->hr_model->join_for_depend($where_d);
        $data['pay'] = $this->hr_model->join_for_pay($where_ay);
        $data['last'] = $this->hr_model->join_for_last($where_ay);

        //echo "<pre>"; print_r($data['last']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/view_retired_profile', $data);
        $this->load->view('includes/footer');
    }
    ////////////////////////////trashed for skills////////////////////////////////////////////
    ///////////////////////////new work done for benefit list
    public function edit_benefit_increment($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
        // print_r($data['empl']);die;

        $where_increm = array('increments.employment_id' => $data['empl']->employment_id, 'increments.trashed' => 0);
        $data['increment_details'] = $this->hr_model->entitle_increment_detail($where_increm);


        $data['in'] = $this->hr_model->get_row_by_where('increments', $where_increm);
        $data['increment'] = $this->hr_model->get_row_by_where('increments', array('increment_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['increment_type'] = $this->hr_model->dropdown_wd_option('ml_increment_type', '-- Select Increment Type --', 'increment_type_id',
            'increment_type',$whered);

        if ($this->input->post('add_increment')) {

            $dateeffective  = $this->input->post('date_effective');
            if(isset($dateeffective) && !empty($dateeffective)){
                $dateeffective = date('Y-m-d',strtotime($dateeffective));
            }

            $insert_increments = array(
                'increment_type_id' => $this->input->post('increment_type_id'),
                'increment_amount' => $this->input->post('increment_amount'),
                'date_effective' => $dateeffective,
                'last_modified_by' => $this->session->userdata('employee_id'),
                'modify_date' => date('Y-m-d'),
            );
            //echo "<pre>"; print_r($insert_increments);die;
            $query_increments = $this->hr_model->update_record('increments', array('increment_id' => $single_id), $insert_increments);
            if ($query_increments) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_benefit_increment', $data);
        $this->load->view('includes/footer');
    }

    public function send_ben_trash_increment($employee_id = NULL, $id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();
        $table_id = $this->uri->segment(5);

        $data['emer_contact'] = $this->site_model->get_row_by_id('increments', array('increment_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('increments', array('increment_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'increments',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_benefits/' . $employee_id);
        }
    }

    public function edit_ben_single_allowance($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;


        $where_allow = array('allowance.employment_id' => $data['empl']->employment_id, 'allowance.trashed' => 0);
        $data['allowance_details'] = $this->hr_model->entitle_allowance_detail($where_allow);

        $data['al'] = $this->hr_model->get_row_by_where('allowance', $where_allow);

        $data['allowance'] = $this->hr_model->get_row_by_where('allowance', array('allowance_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['allowance_type'] = $this->hr_model->dropdown_wd_option('ml_allowance_type', '-- Select Allowance Type --', 'ml_allowance_type_id',
            'allowance_type',$whered);

        /// Function to Update Allowance
        if ($this->input->post('add_allowance')) {
            $now = date('Y-m-d');
            $login_user = $this->session->userdata('employee_id');




            $insert_allowance = array(
                'ml_allowance_type_id' => $this->input->post('ml_allowance_type_id'),
                'allowance_amount' => $this->input->post('allowance_amount'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_last_modified' => date('Y-m-d')
            );
            //echo "<pre>"; print_r($insert_allowance);die;
            $query_allowance = $this->hr_model->update_record('allowance', array('allowance_id' => $single_id), $insert_allowance);
            if ($query_allowance) {
                redirect('human_resource/edit_emp_entitlement_increment/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_single_ben_allowance', $data);
        $this->load->view('includes/footer');
    }


    public function send_ben_trash_allowance($employee_id = NULL, $id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();
        $table_id = $this->uri->segment(5);

        $data['emer_contact'] = $this->site_model->get_row_by_id('allowance', array('allowance_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('allowance', array('allowance_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'allowance',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_benefits/' . $employee_id);
        }
    }

    public function edit_ben_benefit($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

        $where_benef = array('benefits.employment_id' => $data['empl']->employment_id, 'benefits.trashed' => 0);
        $data['benefit_details'] = $this->hr_model->entitle_benefit_detail($where_benef);


        $data['be'] = $this->hr_model->get_row_by_where('benefits', $where_benef);

        $data['benefit'] = $this->hr_model->get_row_by_where('benefits', array('emp_benefit_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['benefit_type'] = $this->hr_model->dropdown_wd_option('ml_benefit_type', '-- Select Benefit Type --', 'benefit_type_id',
            'benefit_type_title',$whered);
        $data['benefit_list'] = $this->hr_model->dropdown_wd_option('ml_benefits_list', '-- Select Benefit --', 'benefit_item_id', 'benefit_name',$whered);

        /// Function to Update Benefits
        if ($this->input->post('add_benefit')) {

            $dateeffective  = $this->input->post('date_effective');
            if(isset($dateeffective) && !empty($dateeffective)){
                $dateeffective = date('Y-m-d',strtotime($dateeffective));
            }

            $insert_benefit = array(
                'benefit_type_id' => $this->input->post('benefit_type_id'),
                //'benefits_list_id' => $this->input->post('benefits_list_id'),
                'date_effective' => $dateeffective,
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modified' => date('Y-m-d')
            );
            $query_benefit = $this->hr_model->update_record('benefits', array('emp_benefit_id' => $single_id), $insert_benefit);
            if ($query_benefit) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_ben_benefits', $data);
        $this->load->view('includes/footer');
    }

    public function send_ben_trash_beneift($employee_id = NULL, $id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();
        $table_id = $this->uri->segment(5);

        $data['emer_contact'] = $this->site_model->get_row_by_id('benefits', array('emp_benefit_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('benefits', array('emp_benefit_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'benefits',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_benefits/' . $employee_id);
        }
    }

    public function edit_ben_leave_type($employee_id = NULL, $single_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);

        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;


        $where_leave = array('leave_entitlement.employment_id' => $data['empl']->employment_id, 'leave_entitlement.trashed' => 0);
        $data['leave_details'] = $this->hr_model->entitle_leave_detail($where_leave);

        $data['le'] = $this->hr_model->get_row_by_where('leave_entitlement', $where_leave);

        $data['leave'] = $this->hr_model->get_row_by_where('leave_entitlement', array('leave_entitlement_id' => $single_id));

        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['leave_type'] = $this->hr_model->dropdown_wd_option('ml_leave_type', '-- Select Leave Type --', 'ml_leave_type_id',
            'leave_type',$whered);

        /// Function to Update Leave
        if ($this->input->post('add_leave')) {
            $insert_leave = array(
                //'employee_id' => $this->input->post('emp_id'),
                'ml_leave_type_id' => $this->input->post('ml_leave_type_id'),
                'no_of_leaves_allocated' => $this->input->post('no_of_leaves'),
                'modified_by' => $this->session->userdata('employee_id'),
                'date_rec_modfied' => date('Y-m-d')
            );
            $query_leave = $this->hr_model->update_record('leave_entitlement', array('leave_entitlement_id' => $single_id), $insert_leave);
            if ($query_leave) {
                redirect('human_resource/add_benefits/' . $emp_id);
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/edit_ben_leave_type', $data);
        $this->load->view('includes/footer');
    }


    public function send_ben_trash_leave($employee_id = NULL, $id = NULL)
    {
//        error_reporting(0);
        $this->db->trans_start();
        $table_id = $this->uri->segment(5);

        $data['emer_contact'] = $this->site_model->get_row_by_id('leave_entitlement', array('leave_entitlement_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('leave_entitlement', array('leave_entitlement_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'leave_entitlement',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            redirect('human_resource/add_benefits/' . $employee_id);
        }
    }

    public function view_employee_extensions($parameter = NULL)
    {
        if ($parameter == "list") {
            $employeeExtensionsList = $this->hr_model->extend();
            $employeeListArray = json_decode($employeeExtensionsList,true);
//            print_r($employeeListArray);
            if(isset($employeeListArray) && !empty($employeeListArray['aaData'])){
                foreach($employeeListArray['aaData'] as $key=> $val){
                    $imageNameAndExtension = $val[0];
//                    echo $val[0];
                    if(empAvatarExist($imageNameAndExtension) === TRUE){
                        $employeeListArray['aaData'][$key][0] = '<img src="upload/Thumb_Nails/'.$imageNameAndExtension.'" style="width:40px; height:40px" />';
                    }else{
                        $employeeListArray['aaData'][$key][0] = '<img src="'.empAvatarExist($imageNameAndExtension).'" style="width:40px; height:40px" />';
                    }
                }
            }
            echo json_encode($employeeListArray);
            return;
            die;
        }
        $data['employee_id'] = $this->session->userdata('employee_id');
        $data['slct_dept'] = $this->input->post('department');
        $whered =array('trashed' => 0);
        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --',
            'department_id', 'department_name',$whered);

        $data['design'] = $this->input->post('designation');
        $data['designation'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Designation --',
            'ml_designation_id', 'designation_name',$whered);

        $data['employee_id'] = $this->uri->segment(3);
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/extension', $data);
        $this->load->view('includes/footer');
    }


    public function extend_employement($employee_id = NULL)
    {
        $data['employee_id'] = $employee_id;
        ///Need To Find Current Employment ID
//        $employmentID = get_employment_from_employeeID($employee_id);
        $where = array('employee_id' => $employee_id, 'enrolled' => 1);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        $where = array('employee_id' => $employee_id);
        $data['experience'] = $this->hr_model->get_row_by_where('emp_experience', $where);

        $employmentWhere = array('employee_id' => $employee_id, 'current' => 1);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $employmentWhere);
        // echo"<pre>";print_r($data['employment']);die;


        $where_posting = array('employement_id' => $data['employment']->employment_id, 'current' => 1);
        $data['posting'] = $this->hr_model->get_row_by_where('posting', $where_posting);


        //Employment Query
        $where_employment = array('employment_id' => $data['employment']->employment_id,'current' => 1);
        $data['contract'] = $this->hr_model->get_row_by_where('contract', $where_employment);

        $whereEmployeeProject = array(
            'employment_id' => $data['employment']->employment_id,
            'current' => 1,
            'trashed' => 0
        );
        $data['position_mgt'] = $this->hr_model->get_row_by_where('position_management', $where_posting);
        $data['project'] = $this->hr_model->get_row_by_where('employee_project', $whereEmployeeProject);
        //echo"<pre>";print_r( $data['project']);die;
        $emp_id = $this->input->post('emp_id');
        $whered =array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Title --', 'ml_designation_id',
            'designation_name',$whered);

        $data['employment_type'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$whered);

        $data['job_category'] = $this->hr_model->dropdown_wd_option('ml_job_category', '-- Select Job Category --', 'job_category_id',
            'job_category_name',$whered);

        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id',
            'department_name',$whered);

        $data['branch'] = $this->hr_model->dropdown_wd_option('ml_branch', '-- Select Branch --', 'branch_id', 'branch_name',$whered);

        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$whered);

        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --',
            'posting_locations_list_id', 'posting_location',$whered);

        $data['work_shift'] = $this->hr_model->dropdown_wd_option('ml_shift', '-- Select Work Shift --', 'shift_id', 'shift_name',$whered);

        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --',
            'ml_pay_grade_id', 'pay_grade',$whered);

        if ($this->input->post('updateBtn')) {
            //Getting All The Inputs..
            $extensionStartDate = $this->input->post('extension_start_date');
            $extensionExpiryDate = $this->input->post('extension_expiry_date');
            $alertOnExpiry = $this->input->post('exp_alert');
            $DateOfAlertOnExpiry = $this->input->post('date_contract_expiry_alert');
            $comments = $this->input->post('comments');
            if(isset($extensionStartDate) && isset($extensionExpiryDate) && isset($alertOnExpiry) && !empty($extensionStartDate) && !empty($extensionExpiryDate)){
                //First We Need to Get The Contract of The Employee
                $table = 'employee E';
                $data = ('C.contract_id AS ContractID, ET.employment_id AS EmploymentID, C.extension AS Extension');
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'contract C',
                        'condition' => 'C.employment_id = ET.employment_id AND C.current = 1',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.trashed' => 0,
                    'E.enrolled' => 1,
                    'E.employee_id' => $employee_id
                );

                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);
                if(!isset($result)){
                    $msg = "Contract is Not Available Anymore::error";
                    $this->session->set_flashdata('extensionMsg',$msg);
                    redirect('human_resource/view_employee_extensions');
                    return;
                }
                //Next We Need To Check if This COntract Has Any Previous Extensions..
                if($result->Extension == 0){
                    //As If Extension is 0, It Means Its Being Extended For the First Time..

                    //Need to Add Data to Extension..
                    $table='employee_contract_extensions';
                    $ExtensionData = array(
                        'e_start_date' => $extensionStartDate,
                        'e_end_date' => $extensionExpiryDate,
                        'e_exp_alert' => $alertOnExpiry,
                        'contract_id' => $result->ContractID,
                        'comments' => $comments
                    );
                    //Set the Expiry Date if Date is Sent From Page
                    if(isset($DateOfAlertOnExpiry) && !empty($DateOfAlertOnExpiry)){
                        $ExtensionData['e_exp_date'] = $DateOfAlertOnExpiry;
                    }
                    $insertResult = $this->common_model->insert_record($table,$ExtensionData);
                    if($insertResult>0){ //Checking If Record Inserted in Extension Successfully..
                        //Now Update the Contract As The Extension Has Been Added.
                        $table = 'contract';
                        $where = array(
                            'contract_id' => $result->ContractID
                        );
                        $contractData = array(
                            'extension' => 1
                        );
                        $updateContractResult = $this->common_model->update($table,$where,$contractData);

                        if ($updateContractResult == true && $insertResult > 0) {
                            $msg = "Contract Successfully Extended::success";
                            $this->session->set_flashdata('extensionMsg',$msg);
                            redirect('human_resource/view_employee_extensions');
                            return;
                        }else{
                            $msg = "Some Error, Data Could Not Be Updated::error";
                            $this->session->set_flashdata('extensionMsg',$msg);
                            redirect('human_resource/view_employee_extensions');
                            return;
                        }
                    }
                }elseif($result->Extension == 1){
                    //if Extension is 1, It Means This Contract Has Previous Extensions..
                    //So we will follow a little different approach here, first get the old extensions and do there current 0
                    $table = 'employee_contract_extensions';
                    $updateData = array(
                        'current' => 0
                    );
                    $where = array(
                        'contract_id' =>  $result->ContractID
                    );
                    $updateResult = $this->common_model->update($table,$where,$updateData);
                    if($updateResult == true){

                        //Need to Add Data to Extension..
                        $table='employee_contract_extensions';
                        $ExtensionData = array(
                            'e_start_date' => $extensionStartDate,
                            'e_end_date' => $extensionExpiryDate,
                            'e_exp_alert' => $alertOnExpiry,
                            'contract_id' => $result->ContractID,
                            'comments' => $comments
                        );
                        //Set the Expiry Date if Date is Sent From Page
                        if(isset($DateOfAlertOnExpiry) && !empty($DateOfAlertOnExpiry)){
                            $ExtensionData['e_exp_date'] = $DateOfAlertOnExpiry;
                        }
                        $insertExtensionResult = $this->common_model->insert_record($table,$ExtensionData);
                    }
                    if($updateResult == true && $insertExtensionResult>0){
                        $msg = "Contract Successfully Extended::success";
                        $this->session->set_flashdata('extensionMsg',$msg);
                        redirect('human_resource/view_employee_extensions');
                        return;
                    }
                }
            }else{
                $msg = "Form Was Not Filled Correctly! ::error";
                $this->session->set_flashdata('extensionMsg',$msg);
                redirect('human_resource/view_employee_extensions');
            }
        }
        $data['rec'] = $this->hr_model->contract_history_join_rpeat(array('contract.employment_id' => $data['employment']->employment_id, 'current' =>1) );
        $this->load->view('includes/header');
        $this->load->view('HR/extend_employement', $data);
        $this->load->view('includes/footer');
    }

    public function view_employee_rejoin($param = NULL)
    {
        if ($param == "list") {
            echo $this->hr_model->rejoin();
            die;

        }

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/rejoin');
        $this->load->view('includes/footer');
    }

    public function back_to_work($employee_id = NULL)
    {

        $whered =array('trashed' => 0);
        $data['employment_type'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$whered);

        $data['job_category'] = $this->hr_model->dropdown_wd_option('ml_job_category', '-- Select Job Category --', 'job_category_id',
            'job_category_name',$whered);

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where);

        $where_employment = array('employment_id' => $data['employment']->employment_id);
        $data['contract'] = $this->hr_model->get_row_by_where('contract', $where_employment);
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $data['emp'] = $this->hr_model->get_row_by_id('employment', $where);
        echo"<pre>";print_r($data['emp']);
        $data['position'] = $this->hr_model->get_row_by_id('position_management', array('employement_id' => $data['emp']->employment_id));
        echo"<pre>";print_r($data['position']);die;
        $data['emer_contact'] = $this->hr_model->get_row_by_id('posting', array('posting.posting_id' =>$data['emp']->employment_id));
        //echo"<pre>";print_r($data['emer_contact']);die;


        if ($this->input->post('continue')) {




            $insert_contract = array(
                'employment_type' => $this->input->post('employment_type'),
                'employment_category' => $this->input->post('job_category'),
                'contract_expiry_date' => $this->input->post('contract_expiry_date'),
                'comments' => $this->input->post('comments'),
                'contract_exp_alert' => $this->input->post('contract_exp_alert'),
                'date_contract_expiry_alert' => $this->input->post('date_contract_expiry_alert')
            );
            // echo"<pre>";print_r($insert_contract);die;
            $query_contract = $this->hr_model->update_record('contract', $where_employment, $insert_contract);
            if ($data['position']->current == 0) {
                $insert = array('current' => 1);
                $query1 = $this->hr_model->update_record('position_management', array('employement_id' => $data['emp']->employment_id), $insert);
                /// echo"<pre>";print_r($query1);die;
            }

            if ($data['emer_contact']->current == 0) {
                $update = array('current' => 1);
                //echo"<pre>"; print_r($data['emer_contact']->enabled);die;
                $query = $this->hr_model->update_record('posting', array('posting.posting_id' =>  $data['emp']->employment_id), $update);
            }

            if ($query_contract || $query1 || $query) {
                redirect('human_resource/view_employee_rejoin');
            }
        }

        $this->load->view('includes/header');
        $this->load->view('HR/back_to_work', $data);
        $this->load->view('includes/footer');
    }

    /**
     * @param null $parameter
     * @deprecated view_employment_project_history is a deprectacted Function New Function Should Be "view_employment_history"
     */
    public function view_employment_project_history($parameter = NULL)
    {

        if ($parameter == "list") {
            echo $this->hr_model->project_history();
            die;
        }
        $this->load->view('includes/header');
        $this->load->view('HR/employment_history');
        $this->load->view('includes/footer');
    }

    public function view_employment_history(){
        $this->load->view('includes/header');
        $this->load->view('HR/employment_history');
        $this->load->view('includes/footer');
    }

    public function employment_History_DT(){
        if($this->input->is_ajax_request()){
            if($this->input->post('sEcho') && $this->input->post('EmpID')){
                $employeeID = $this->input->post('EmpID');
                $selectData = array('
                ET.employment_id AS EmploymentID,
                MLDg.designation_name AS Designation,
                MLPG.pay_grade AS Grade,
                MLD.department_name AS DepartmentName,
                IFNULL(MLB.branch_name,"Work Location NOT SET") AS Branch,
                date_format(ET.joining_date,"%d-%m-%y") AS FromDate,
                date_format(ET.end_date,"%d-%m-%y") AS ToDate,
                ET.current AS Status
                ',false);
                $PTable = 'employee E';
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'E.employee_id = ET.employee_id` AND ET.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'employee_project EP',
                        'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'position_management PM',
                        'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.status = 2',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_designations MLDg',
                        'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_pay_grade MLPG',
                        'condition' => 'MLPG.ml_pay_grade_id = PM.ml_pay_grade_id',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'posting P',
                        'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.current = 1',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_branch MLB',
                        'condition' => 'MLB.branch_id = P.ml_branch_id AND MLB.trashed = 0',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_department MLD',
                        'condition' => 'P.ml_department_id = MLD.department_id',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.trashed' => 0,
                    'E.employee_id' => $employeeID
                );
                $group_by = 'ET.employment_id';
                $add_column = array(
                    'ViewEmploymentDetails' => '<a style="cursor:pointer;cursor:hand;" class="employmentHistoryDetails"><span class="fa fa-eye"></span></a></span>'
                );
                $result = $this->common_model->select_fields_joined_DT($selectData,$PTable,$joins,$where,'','',$group_by,$add_column,'','');
                $resultArray = json_decode($result,true);
//                print_r($resultArray);
//                print_r($resultArray);
                foreach($resultArray['aaData'] as $key=>$value){
                    if($resultArray['aaData'][$key]['Status'] == 1){
                        $activeArray = array(
                            'Status' => 'Active'
                        );
                        $resultArray['aaData'][$key] = array_replace($resultArray['aaData'][$key],$activeArray);
                    }elseif($resultArray['aaData'][$key]['Status'] == 0){
                        $activeArray = array(
                            'Status' => 'InActive'
                        );
                        $resultArray['aaData'][$key] = array_replace($resultArray['aaData'][$key],$activeArray);
                    }
                }
                print_r(json_encode($resultArray));
            }
        }
    }
    public function employment_history_details(){
        if($this->input->post('employmentID')){
            $viewData['employmentID'] = $this->input->post('employmentID');
            $viewData['employeeID'] = $this->input->post('employeeID');
            $this->load->view('includes/header');
            $this->load->view('HR/employment_history_details',$viewData);
            $this->load->view('includes/footer');
        }else{
            echo "No Data Posted.";
        }
    }
    public function employment_history_details_DT(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $employeeID = $this->input->post('EmpID');
                $employmentID = $this->input->post('Employment');
                $selectData = array('
                ET.employment_id AS EmploymentID,
                MLDg.designation_name AS Designation,
                MLPG.pay_grade AS Grade,
                GROUP_CONCAT(DISTINCT MLD.department_name SEPARATOR ",<br />") AS DepartmentName,
                GROUP_CONCAT(DISTINCT MLP.project_title SEPARATOR ",<br />") AS Projects,
                IFNULL(MLB.branch_name,"Work Location NOT SET") AS Branch,
                date_format(PM.from_date,"%d-%m-%y") AS FromDate,
                date_format(PM.to_date,"%d-%m-%y") AS ToDate
                ',false);
                $PTable = 'employee E';
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'E.employee_id = ET.employee_id',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'position_management PM',
                        'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_designations MLDg',
                        'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_pay_grade MLPG',
                        'condition' => 'MLPG.ml_pay_grade_id = PM.ml_pay_grade_id',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'employee_project EP',
                        'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_projects MLP',
                        'condition' => 'MLP.project_id = EP.project_id',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'posting P',
                        'condition' => 'P.employement_id = PM.employement_id AND P.trashed = 0',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_branch MLB',
                        'condition' => 'MLB.branch_id = P.ml_branch_id AND MLB.trashed = 0',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_department MLD',
                        'condition' => 'P.ml_department_id = MLD.department_id',
                        'type' => 'LEFT'
                    )
                );
                $where = array(
                    'E.trashed' => 0,
                    'E.employee_id' => $employeeID,
                    'ET.employment_id' => $employmentID
                );
                $group_by = 'ET.employment_id,PM.position_id';
                $result = $this->common_model->select_fields_joined_DT($selectData,$PTable,$joins,$where,'','',$group_by);
                print_r($result);
            }
        }
    }


    public function history_project($employee_id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);
        $data['employment'] = $this->hr_model->get_row_by_where('employment', $where);
        // echo"<pre>";print_r($data['employment']);die;

        $where_employment = array('employment_id' => $data['employment']->employment_id,'current' => 1);
        $where_emp = array('employee.employee_id' => $employee_id);
        $data['job_history'] = $this->hr_model->view_kpi_history_pe($where_emp);
        $data['join'] = $this->hr_model->join_for_history_skha($where_emp);
        $data['rec'] = $this->hr_model->contract_history_join_rpeat(array('contract.employment_id' => $data['employment']->employment_id, 'current' =>0) );
        $data['exp'] = $this->hr_model->get_row_by_where('emp_skills', $where);
        $data['skill'] = $this->hr_model->get_row_by_where('ml_skill_type',array('ml_skill_type.skill_type_id' => $data['exp']->ml_skill_type_id));
        $data['contact']  = $this->hr_model->get_row_by_where('current_contacts',$where);
        $data['project'] = $this->hr_model->getting_all_joing_to_project($where);
        // echo"<pre>";print_r($data['skill']);
        //echo"<pre>";print_r($data['project']);die;
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/page', $data);
        $this->load->view('includes/footer');
    }
    /* End of Class Human_Resource */


    //So Finally Starting Work on Report Generation.
    public function generate_report(){
        $this->load->view('includes/header');
        $this->load->view('HR/generate_report');
        $this->load->view('includes/footer');
    }
    public function generate_report_print(){
        /**
         * Some Select Fields For Quick Info.
         * E employee
         * ET employment
         * EP employee_project
         * PC permanant_contacts
         * MLD ml_districts
         * PS posting
         * MLB ml_branch
         * MLC ml_city
         * MLDg ml_designations
         * PM position_management
         * MLPG ml_pay_grade
         * CC current_contacts
         * CN Contract
         * QF Qualification
         * ED Employee Dependents
         * MLDP ml_department
         * EI employee_insurance
         * MLIT ml_insurance_type
         */
        $selectsData = ('E.employee_code AS Employee_Code,E.full_name AS Name,E.father_name AS Father_Name');
        $PTable = 'employee E';
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_project EP',
                'condition' => 'EP.employment_id = ET.employment_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'permanant_contacts PC',
                'condition' => 'PC.employee_id = E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_district MLD',
                'condition' => 'MLD.district_id = PC.district',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'MLP.project_id = EP.project_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_program_list MLProg',
                'condition' => 'MLProg.ProgramID = MLP.ProgramID AND MLProg.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = 'E.trashed = 0 AND E.enrolled = 1';
        $group_by = 'E.employee_id';

        //Now When Some Filters Are Posted..
        if($this->input->post()){
            $selects = $this->input->post('selects');
            $employees = $this->input->post('employees');
            $Program = $this->input->post('Program');
            $projects = $this->input->post('projects');
            $domiciles = $this->input->post('domiciles');
            $reportHeading = $this->input->post('reportHeading');
            if(isset($reportHeading) && !empty($reportHeading)){
                $viewData['reportHeading'] = htmlentities($reportHeading);
            }

//            need to decode the Json Data to array
            if(isset($employees) && !empty($employees) && $employees !== '[]'){
                $employees = json_decode($employees,true);
                $employeeIDs = array();
                foreach($employees as $key=>$value){
                    array_push($employeeIDs,$value['id']);
                }
                $employeeIDs = implode(",",$employeeIDs);
                //Where Query for the Employees If Employees Are Selected
                $where .= ' AND E.employee_id IN ('.$employeeIDs.')';
            }
            if(isset($Program) && !empty($Program) && $Program !== '[]'){
                $Program = json_decode($Program,true);
                $programIDs = array();
                foreach($Program as $key=>$value){
                    array_push($programIDs,$value['id']);
                }
                $programIDs = implode(",",$programIDs);
                //Where Query for the Projects If Projects Are Selected
                $where .= ' AND MLP.ProgramID IN ('.$programIDs.')';
            }
            if(isset($projects) && !empty($projects) && $projects !== '[]'){
                $projects = json_decode($projects,true);
                $projectIDs = array();
                foreach($projects as $key=>$value){
                    array_push($projectIDs,$value['id']);
                }
                $projectIDs = implode(",",$projectIDs);
                //Where Query for the Projects If Projects Are Selected
                $where .= ' AND EP.project_id IN ('.$projectIDs.')';
            }
            if(isset($domiciles) && !empty($domiciles) && $projects !== '[]'){
                $domiciles = json_decode($domiciles,true);
                $districtIDs = array();
                foreach($domiciles as $key=>$value){
                    array_push($districtIDs,$value['id']);
                }
                $districtIDs = implode(",",$districtIDs);
                //Where Query for the Districts If Districts Are Selected
                $where .= ' AND MLD.district_id IN ('.$districtIDs.')';
            }

            //If There are Some Checkboxes Selected Then If Statement Should Populate..
            if(isset($selects) && !empty($selects)){
                //For The Selected Fields if the Joins Are Needed Will Be Added On Demand..
                if (strpos($selects,'MLC') !== false) {
                    $array = array(
                        'table' => 'ml_city MLC',
                        'condition' => 'PC.city_village = MLC.city_id',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }

                if (strpos($selects,'PM') !== false) {
                    $array = array(
                        'table' => 'position_management PM',
                        'condition' => 'PM.employement_id = ET.employment_id AND PM.current = 1 AND PM.status = 2 AND PM.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'MLDg') !== false) {
                    $array = array(
                        'table' => 'ml_designations MLDg',
                        'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND PM.current = 1',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'MLPG') !== false) {
                    $array = array(
                        'table' => 'ml_pay_grade MLPG',
                        'condition' => 'MLPG.ml_pay_grade_id = PM.ml_pay_grade_id AND PM.current = 1 AND MLPG.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'CC') !== false) {
                    $array = array(
                        'table' => 'current_contacts CC',
                        'condition' => 'CC.employee_id = E.employee_id AND CC.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'CN') !== false) {
                    $array = array(
                        'table' => 'contract CN',
                        'condition' => 'CN.employment_id = ET.employment_id AND CN.trashed = 0 AND CN.current = 1',
                        'type' => 'INNER'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'QF') !== false) {
                    $array = array(
                        'table' => '( SELECT *,MAX(Q.year) AS BigYear FROM qualification Q GROUP BY Q.employee_id ) AS QF',
                        'condition' => 'QF.employee_id = E.employee_id AND QF.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'PS') !== false) {
                    $array = array(
                        'table' => 'posting PS',
                        'condition' => 'PS.employement_id = ET.employment_id AND PS.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'MLB') !== false) {
                    $array = array(
                        'table' => 'ml_branch MLB',
                        'condition' => 'MLB.branch_id = PS.ml_branch_id AND MLB.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'ED') !== false) {
                    $array = array(
                        'table' => 'dependents ED',
                        'condition' => 'ED.employee_id = E.employee_id AND ED.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'MLDP') !== false) {
                    $array = array(
                        'table' => 'ml_department MLDP',
                        'condition' => 'MLDP.department_id = PS.ml_department_id AND MLDP.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'EI') !== false) {
                    $array = array(
                        'table' => 'employee_insurance EI',
                        'condition' => 'EI.employment_id = ET.employment_id AND EI.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'MLIT') !== false) {
                    $array = array(
                        'table' => 'ml_insurance_type MLIT',
                        'condition' => 'MLIT.insurance_type_id = EI.ml_insurance_id AND MLIT.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'DD') !== false) {
                    $array = array(
                        'table' => 'deduction DD',
                        'condition' => 'DD.employment_id = ET.employment_id AND DD.trashed = 0 AND DD.status = 2 AND DD.eobi_no != ""',
                        'type' => 'LEFT'
                    );
                    array_push($joins,$array);
                }
                if (strpos($selects,'GEN') !== false) {
                    $array = array(
                        'table' => 'ml_gender_type GEN',
                        'condition' => 'GEN.gender_type_id = E.gender',
                        'type' => 'INNER'
                    );
                    array_push($joins,$array);
                }
                //Selected Fields Filters, If Checked will Be Added to the Selects..
                $selectedFieldsFilter = explode(",",$selects);
                if(in_array('NationalIDCardNo',$selectedFieldsFilter)){
                    $selectsData .=',E.CNIC AS CNIC';
                }
                if(in_array('ET.PS.MLDP.Department',$selectedFieldsFilter)){
                    $selectsData .=',MLDP.department_name AS Department';
                }
                if(in_array('MLC.CityVillage',$selectedFieldsFilter)){
                    $selectsData .=',MLC.city_name AS City_Name';
                }
                if(in_array('MLD.Domicile',$selectedFieldsFilter)){
                    $selectsData .=',MLD.district_name AS Domicile';
                }
                if(in_array('DOB',$selectedFieldsFilter)){
                    $selectsData .=',DATE_FORMAT(E.date_of_birth,"%D %M %Y") AS `D.O.B`';
                }
                if(in_array('ET.PM.MLDg.Designation',$selectedFieldsFilter)){
                    $selectsData .=',MLDg.designation_name AS Designation';
                }
                if(in_array('ET.PM.MLPG.Grade',$selectedFieldsFilter)){
                    $selectsData .=',MLPG.pay_grade AS Grade';
                }
                if(in_array('ET.JoiningDate',$selectedFieldsFilter)){
                    $selectsData .=',DATE_FORMAT(ET.joining_date,"%d-%m-%Y") AS Joining_Date';
                }
                if(in_array('PC.PostalAddress',$selectedFieldsFilter)){
                    $selectsData .=',PC.address AS Postal_Address';
                }
                if(in_array('CC.OfficialEmail',$selectedFieldsFilter)){
                    $selectsData .=',CC.official_email AS OfficialEmail';
                }
                if(in_array('CC.Contact',$selectedFieldsFilter)){
                    $selectsData .=',CC.mob_num AS Mobile_No';
                }
                if(in_array('ET.CN.ContractExpiry',$selectedFieldsFilter)){
                    $selectsData .=',date_format(CN.contract_expiry_date,"%d-%m-%Y") AS Contract_Expiry';
                }
                if(in_array('ET.EI.MLIT.InsuranceNo',$selectedFieldsFilter)){
                    $selectsData .=',IFNULL(GROUP_CONCAT(DISTINCT CONCAT(MLIT.insurance_type_name, " (", EI.ml_insurance_id, ")") SEPARATOR ",<br />"), " No Insurance FOUND ") AS Insurance_No';
                }
                if(in_array('DD.EOBI',$selectedFieldsFilter)){
                    $selectsData .=',DD.eobi_no AS EOBI_No';
                }
                if(in_array('QF.Qualification',$selectedFieldsFilter)){
                    $selectsData .=',QF.qualification AS Qualification';
                }
                if(in_array('ET.PS.MLB.OfficeLocation',$selectedFieldsFilter)){
                    $selectsData .=',MLB.branch_name AS Office_Location';
                }
                if(in_array('ED.Dependents',$selectedFieldsFilter)){
                    $selectsData .=',IFNULL(GROUP_CONCAT(DISTINCT CONCAT(ED.dependent_name, "(", DATE_FORMAT(ED.date_of_birth,"%D %M %Y"), ")") SEPARATOR ",<br />")," No Dependents ") AS Dependents';
                }
                if(in_array('GEN.gender_type_title',$selectedFieldsFilter)){
                    $selectsData .=',GEN.gender_type_title AS Gender';
                }
            }
        }
        //Need to Remove Extra BackTicks Added By the CodeIgniter It Self.
        $data = array($selectsData,false);
        $viewData['EmployeeData'] = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
        if(!empty($viewData['EmployeeData'])){
            $query=$this->db->last_query();
            //Loading the View.
            // Code for CSV File
            $this->load->dbutil();
            $this->load->helper('file');
            $newline="\r\n";
            $data=$this->common_model->get_query_record($query);
            //$query=json_encode($query);
            //echo "<pre>";
            //print_r($data);die;
            //echo "</pre>";
            $csv=$this->dbutil->csv_from_result($data,',',$newline);
//        $csv = ltrim(strstr($this->dbutil->csv_from_result($data, ',', "\r\n"), "\r\n"));

            $this->load->helper('download');

            $uploadPath = 'systemGeneratedFiles/HrCsvReports/'.$this->data['EmployeeID'].'/test'.time().'.csv';
            $uploadDirectory = 'systemGeneratedFiles/HrCsvReports/'.$this->data['EmployeeID'];
            //If Directories are Not Avaialable, Create The Directories On The Go..
            if(!is_dir($uploadDirectory)){
                mkdir($uploadDirectory, 0755, true);
            }

            $path= 'systemGeneratedFiles/HrCsvReports/'.$this->data['EmployeeID'].'/';
            $delete_files= glob($path.'*');
            foreach($delete_files as $dFile){ // iterate files
                if(is_file($dFile))
                    unlink($dFile); // delete file
                //echo $file.'file deleted';
            }

            //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
            //$fileName = ("Parexons_GeneratedReport_HR_CSV_'.time().'.csv",$csv);
            //$filePath = $uploadPath.$fileName;
            write_file($uploadPath,$csv, 'w+');

            $viewData['files']=$uploadPath;
        }

        //END

//        print_r($this->db->last_query());
        $viewData['view']=$this->load->view('HR/generate_pdf_email',$viewData,true);
        $this->load->view('HR/generate_print',$viewData);
    }
    public function downloadPdfHrReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_HR_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }

    // Function for email HR report as pdf
    public function emailPdfHrReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                // Email validation
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                // End

                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/HrReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_HR_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);

                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
                return;

            }
        }
    }

    // End

    public function loadAvailableEmployees(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "employee E";
                $data = array('E.employee_id AS EmployeeID,E.full_name AS EmployeeName,E.thumbnail AS EmployeeAvatar,E.employee_code AS EmployeeCode',false);
                $where = array(
                    'E.trashed' => 0
                );
                $group_by = 'E.employee_id';
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue,$group_by);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where,FALSE,'','','',$group_by);
                }
//                print(json_encode($result));
//                $employeesJson = json_encode($result);
                //Need To Update The employees Images, If Employees Images Don't Exist, Show Default

                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }

    public function loadReportToEmployees($selectedEmployeeID){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "employee E";
                $data = array('E.employee_id AS EmployeeID,E.full_name AS EmployeeName,E.thumbnail AS EmployeeAvatar,E.employee_code AS EmployeeCode',false);
                $where = array(
                    'E.trashed' => 0,
                    'E.employee_id !=' => $selectedEmployeeID,
                    'E.enrolled' => 1
                );
                $group_by = 'E.employee_id';
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue,$group_by);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where,FALSE,'','','',$group_by);
                }

                //We Got All Employees Except the One For Whom We Want to Assign.
                //Which is OK, But we Need TO Unset also Those which this Selected Employee has already been assigned as superviser or subordinate.

                $rhTable = 'reporting_heirarchy';
                $rhData = '*';
                $rhWhere = array(
                    'trashed' => 0,
                    'for_employee' => $selectedEmployeeID
                );
                $selectedEmployeeReporters = $this->common_model->select_fields_where($rhTable,$rhData,$rhWhere);
                //Unset The Items That We Don't Need.
                //As If One is Supervisor of an employee, Then That One Can Not Be Subordinate For Same Employee At Same Time.
                if (isset($selectedEmployeeReporters) && !empty($selectedEmployeeReporters)) {
                    foreach ($result as $key => $val) {
                        foreach ($selectedEmployeeReporters as $RHKey=>$RHVal) {
                            if($RHVal->employeed_id === $val->EmployeeID || $RHVal->reporting_authority_id === $val->EmployeeID){
                                unset($result[$key]);
                            }
                        }
                    }
                }

                //Need To Update The employees Images, If Employees Images Don't Exist, Show Default
                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }
    public function loadAvailableProjects(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "ml_projects MLP";
                $data = array('MLP.project_id AS ProjectID','MLP.project_title AS ProjectName');
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'MLP.project_id';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,'',FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields($tbl,$data);
                }
                /*Print the Json Result for All the Available Skills*/
                print_r(json_encode($result));
            }
        }
    }
    public function add_employment_rejoin($employee_id = NULL)
    {

        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee_id' => $employee_id);

        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $emp_id = $this->input->post('emp_id');
        $where2 = array('trashed' => 0);
        $data['job_title'] = $this->hr_model->dropdown_wd_option('ml_designations', '-- Select Job Designation --', 'ml_designation_id',
            'designation_name',$where2);

        $data['employment_type'] = $this->hr_model->dropdown_wd_option('ml_employment_type', '-- Select Employment Type --',
            'employment_type_id', 'employment_type',$where2);

        $data['job_category'] = $this->hr_model->dropdown_wd_option('ml_job_category', '-- Select Job Category --', 'job_category_id',
            'job_category_name',$where2);

        $data['department'] = $this->hr_model->dropdown_wd_option('ml_department', '-- Select Department --', 'department_id',
            'department_name',$where2);

        $data['branch'] = $this->hr_model->dropdown_wd_option('ml_branch', '-- Select Branch --', 'branch_id', 'branch_name',$where2);

        $data['city'] = $this->hr_model->dropdown_wd_option('ml_city', '-- Select City --', 'city_id', 'city_name',$where2);

        $data['location'] = $this->hr_model->dropdown_wd_option('ml_posting_location_list', '-- Select Location --',
            'posting_locations_list_id', 'posting_location',$where2);

        $data['work_shift'] = $this->hr_model->dropdown_wd_option('ml_shift', '-- Select Work Shift --', 'shift_id', 'shift_name',$where2);

        $data['paygrade'] = $this->hr_model->dropdown_wd_option('ml_pay_grade', '-- Select Pay Grade --',
            'ml_pay_grade_id', 'pay_grade',$where2);
        $data['separate'] = $this->hr_model->get_row_by_where('seperation_management', $where);

        //$data['any_prj'] = $this->hr_model->get_all_by_where('employee_project',array('employee_id' => $this->uri->segment(3)));
        //echo"<pre>";print_r(any_prjseparate']);die;

        if ($this->input->post('continue')) {
            $this->db->trans_start();

            $data = array('current' => 0);
            $query = $this->hr_model->update_record('employee_project',array('employment_id' => $this->uri->segment(3)),$data);
            $data = array('enrolled' => 1);
            $query99 = $this->hr_model->update_record('employee',$where,$data);


            $data = array('rejoin' => 1);
            $query100 = $this->hr_model->update_record('seperation_management',$where,$data);



            $joindate  = $this->input->post('joining_date');
            if(isset($joindate) && !empty($joindate))
            {
                $joindate = date('Y-m-d',strtotime($joindate));
            }


            $insert_employment = array(
                'employee_id' => $emp_id,
                'joining_date' => $joindate,
                'end_date'=>0000-00-00
            );
            $query_employment = $this->hr_model->create_new_record('employment', $insert_employment);
            $employment_id = $this->db->insert_id();

            $insert_posting = array(
                'employement_id' => $employment_id,
                'poting_start_date' => $joindate,
                'ml_department_id' => $this->input->post('department'),
                'ml_branch_id' => $this->input->post('branch'),
                'ml_city_id' => $this->input->post('city'),
                'location' => $this->input->post('location'),
                'shift' => $this->input->post('work_shift'),
                'date_approved'=>0000-00-00
            );
            $query_posting = $this->hr_model->create_new_record('posting', $insert_posting);

            $contractsartdate  = $this->input->post('contract_start_date');
            if(isset($contractsartdate) && !empty($contractsartdate))
            {
                $contractsartdate = date('Y-m-d',strtotime($contractsartdate));
            }

            $contractexpirydate  = $this->input->post('contract_expiry_date');
            if(isset($contractexpirydate) && !empty($contractexpirydate))
            {
                $contractexpirydate = date('Y-m-d',strtotime($contractexpirydate));
            }

            $contractexpirydatealert  = $this->input->post('date_contract_expiry_alert');
            if(isset($contractexpirydatealert) && !empty($contractexpirydatealert))
            {
                $contractexpirydatealert = date('Y-m-d',strtotime($contractexpirydatealert));
            }


            $insert_contract = array(
                'employment_id' =>  $employment_id,
                'contract_start_date' => $contractsartdate,
                'employment_type' => $this->input->post('employment_type'),
                'employment_category' => $this->input->post('job_category'),
                'contract_expiry_date' => $contractexpirydate,
                'comments' => $this->input->post('comments'),
                'contract_exp_alert' => $this->input->post('contract_exp_alert'),
                'date_contract_expiry_alert' => $contractexpirydatealert
            );
            $query_contract = $this->hr_model->create_new_record('contract', $insert_contract);

            $insert_position_mgt = array(
                'employement_id' => $employment_id,
                'ml_pay_grade_id' => $this->input->post('pay_grade'),
                'ml_designation_id' => $this->input->post('curr_job_title'),
                'job_specifications' => $this->input->post('job_specification'),
                'from_date' => $joindate,
                'status' => 1
            );
            $query_position_mgt = $this->hr_model->create_new_record('position_management', $insert_position_mgt);

            $counter_type = 0;
            $project_id = $this->input->post('project_id');
            //echo "<pre>";print_r($approval_type); die;
            foreach($project_id as $type) {
                $insert_project = array(
                    'employment_id' => $this->uri->segment(3),
                    'project_id' => $type
                );
                $insert_project = $this->hr_model->create_new_record('employee_project', $insert_project);
                $counter_type++;
            }

            $this->db->trans_complete();

            if ($query || $query99 || $query100 || $query_employment || $query_posting || $query_contract || $query_position_mgt || $insert_project) {
                redirect('human_resource/add_emp_pay_package/'.$this->uri->segment(3));
            }
        }
        $data['project_id'] 	= $this->input->post('project_id');
        $data['approval'] = $this->site_model->dropdown_wd_option('ml_projects', '-- Select Project --',
            'project_id','project_title',$where2);
        $this->load->view('includes/header');
        $this->load->view('HR/rejoin_for_employement', $data);
        $this->load->view('includes/footer');
    }


    public function loadAvailableEmployeeDomiciles(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = "ml_district MLD";
                $data = array('MLD.district_id AS DistrictID','MLD.district_name AS DistrictName');
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'MLD.district_id';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,'',FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields($tbl,$data);
                }
                print_r(json_encode($result));
            }
        }
    }
    public function edit_emp_supervisor($employee_id = NULL)
    {
        // Function for get supervisor records to field.
        $data['get_rec'] = $this->hr_model->get_row_by_where('reporting_heirarchy', array('report_heirarchy_id'=>$this->uri->segment(4)));
        // var_dump($data['get_rec']);

        //Function for dropdown ....
        $whered =array('trashed' => 0);
        $data['report_method'] = $this->hr_model->dropdown_wd_option('ml_reporting_options', '-- Select Report Method --',
            'reporting_option_id', 'reporting_option',$whered);

        //Function for employee detail.
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        /// Function for update supervisor detail.
        if ($this->input->post('add_supervisor')) {
            $supervisor_id = $this->input->post('supervisor_id');

            $insert_report_to_sup = array(
                'employeed_id' => $supervisor_id,
                'ml_reporting_heirarchy_id' => $this->input->post('supervisor_method')
            );
            // echo"<pre>";print_r($insert_report_to_sup);die;
            $query_supervisor = $this->hr_model->update_record('reporting_heirarchy', array('reporting_heirarchy.report_heirarchy_id'=>$this->uri->segment(4)), $insert_report_to_sup);

            if ($query_supervisor) {
                redirect('human_resource/edit_emp_report_to/' . $employee_id);
            }
        }

        //Function for record shown in the table for supervisor and subordinate.
        $wheredd = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.employeed_id !=' =>0);
        $data['record'] = $this->hr_model->record_report_view_table($wheredd);
        $where2 = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.reporting_authority_id !='=>0);
        $data['reports'] = $this->hr_model->record_report_view_table2($where2);
        $this->load->view('includes/header');
        $this->load->view('HR/edit_report_to',$data);
        $this->load->view('includes/footer');
    }

    public function edit_emp_subordinate($employee_id = NULL)
    {
        // Function for get Subordinate records to field.
        $data['get_sub'] = $this->hr_model->get_row_by_where('reporting_heirarchy', array('report_heirarchy_id'=>$this->uri->segment(4)));
        // var_dump($data['get_rec']);

        //Function for dropdown ....
        $whered =array('trashed' => 0);
        $data['report_method'] = $this->hr_model->dropdown_wd_option('ml_reporting_options', '-- Select Report Method --',
            'reporting_option_id', 'reporting_option',$whered);

        //Function for employee detail.
        $where = array('employee_id' => $employee_id);
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        /// Function for update supervisor detail.
        if ($this->input->post('add_sub_ordinate')) {
            $sub_ordinate_id = $this->input->post('reporting_authority_id');

            $insert_report_to_sub = array(
                'reporting_authority_id' => $sub_ordinate_id,
                'ml_reporting_heirarchy_id' => $this->input->post('subordinate_method'),
            );
            // print_r($insert_report_to_sub);die;
            $query_suordinate = $this->hr_model->update_record('reporting_heirarchy',  array('reporting_heirarchy.report_heirarchy_id'=>$this->uri->segment(4)), $insert_report_to_sub);

            if ($query_suordinate) {
                redirect('human_resource/edit_emp_report_to/' . $employee_id);
            }
        }

        //Function for record shown in the table for supervisor and subordinate.
        $wheredd = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.employeed_id !=' =>0);
        $data['record'] = $this->hr_model->record_report_view_table($wheredd);
        $where2 = array('reporting_heirarchy.trashed !=' => 1,'reporting_heirarchy.reporting_authority_id !='=>0);
        $data['reports'] = $this->hr_model->record_report_view_table2($where2);
        $this->load->view('includes/header');
        $this->load->view('HR/edit_report_to',$data);
        $this->load->view('includes/footer');
    }
    public function delete_complete($employee_id = NULL, $report_heirarchy_id = NULL)
    {
        $where = array('reporting_heirarchy.report_heirarchy_id' =>$report_heirarchy_id);
        $data['rec'] = $this->hr_model->get_row_by_where('reporting_heirarchy',$where);
        //var_dump($data['rec']);
        $query = $this->hr_model->delete_row_by_where('reporting_heirarchy',$where);
        if($query)
        {
            redirect('human_resource/add_emp_report_to/'.$employee_id);
        }
    }
    public function delete_complete2($employee_id = NULL, $report_heirarchy_id = NULL)
    {
        $where = array('reporting_heirarchy.report_heirarchy_id' =>$report_heirarchy_id);
        $data['rec'] = $this->hr_model->get_row_by_where('reporting_heirarchy',$where);
        //var_dump($data['rec']);
        $query = $this->hr_model->delete_row_by_where('reporting_heirarchy',$where);
        if($query)
        {
            redirect('human_resource/edit_emp_report_to/'.$employee_id);
        }
    }


    // Function for delete insurance...
    public function delete_complete_insure($employee_id = NULL, $employee_insurance_id = NULL)
    {
        $where = array('employee_insurance.employee_insurance_id' =>$employee_insurance_id);
        $data['rec'] = $this->hr_model->get_row_by_where('employee_insurance',$where);
        //var_dump($data['rec']);die;
        $query = $this->hr_model->delete_row_by_where('employee_insurance',$where);
        if($query)
        {
            redirect('human_resource/edit_emp_entitlement_increment/'.$employee_id);
        }
    }

    //Deprecated Function not Using It Any More..
    public function delete_complete_insure2($employee_id = NULL, $employee_insurance_id = NULL)
    {
        $where = array('employee_insurance.employee_insurance_id' =>$employee_insurance_id);
        $data['rec'] = $this->hr_model->get_row_by_where('employee_insurance',$where);
        //var_dump($data['rec']);die;
        $query = $this->hr_model->delete_row_by_where('employee_insurance',$where);
        if($query)
        {
            redirect('human_resource/add_emp_entitlement_increment/'.$employee_id);
        }
    }

    public function send_2_trash_insure($employee_id = NULL, $record_id = NULL)
    {
        if($record_id === NULL || !is_numeric($record_id) || empty($record_id) || !is_numeric($employee_id) || empty($employee_id)){
            echo "Are You Missing Something, If you Think You Got This Page By Mistake, Please Contact System Administrator For Further Assistance";
            return;
        }

        //Need Employment ID
        $table = 'employment ET';
        $selectData = 'ET.employment_id AS EmploymentID';
        $where = array(
            'ET.trashed' => 0,
            'ET.current' => 1
        );
        $employment= $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
        if(!isset($employment) || empty($employment)){
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }

        $employmentID = $employment->EmploymentID;
        $table_id=$this->uri->segment(5);
        $data['emergency'] = $this->hr_model->get_row_by_where('employee_insurance', array('employment_id' => $employmentID));
        // var_dump($data['emergency']);die;
        if ($data['emergency']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->hr_model->update_record('employee_insurance', array('employee_insurance_id' => $record_id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $record_id,
            'table_id_name' => $table_id,
            'table_name' => 'employee_insurance',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        //print_r($insert_trash);die;
        //var_dump($insert_trash);die;
        $query_trash = $this->hr_model->create_new_record('trash_store', $insert_trash);



        if ($query || $query_trash) {
            redirect('human_resource/edit_emp_entitlement_increment/' . $employee_id);
        }
    }


    /**
     * Appointment Letter and Contract Letter Functions
     * @param null $employeeID
     */
    public function appointment($employeeID = NULL)
    {
        if ($employeeID != NULL) {
            $PTable = 'employee E';
            $employeeData = array('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        ET.joining_date AS StartDate,
        MLDg.designation_name AS Position,
        IFNULL(S.base_salary,"Salary Not SET") AS BaseSalary
        ',false);
            $joins = array(
                array(
                    'table' => 'current_contacts CC',
                    'condition' => 'E.employee_id=CC.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id=ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id AND PM.current=1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'salary S',
                    'condition' => 'ET.employment_id=S.employement_id AND S.status = 2',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.trashed' => '0',
                'E.employee_id' => $employeeID
            );
            $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);

            //Now Need to Get Company Information
            $tbl = 'organization O';
            $org_data = ('*');
            $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
            //Now need to get the General Format for User..
            $table = 'ml_letter_text';
            $letterData = ('LetterText');
            $condition = array(
                'LetterTextID' => '1'
            );
            $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        } else {
            $data = array();
        }
        //Need Some Data For Left Menus, So Using the Code That Already is been Present in the System.
        $data['employee_id'] = $this->uri->segment(3);
        $where = array(
            'employee_id' => $data['employee_id']
        );
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);
        $this->load->view('includes/header');
        $this->load->view('HR/appointmentLetter', $data);
        $this->load->view('includes/footer');
    }
//Cover Letter
    public function coverLetter($employeeID = NULL)
    {
        if ($employeeID != NULL) {
            $PTable = 'employee E';
            $employeeData = array('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        IFNULL(ECE.e_start_date,C.contract_start_date) AS StartDate,
        IFNULL(ECE.e_end_date,C.contract_expiry_date) AS EndDate,
        MLDg.designation_name AS Position,
        S.base_salary AS BaseSalary
        ',false);
            $joins = array(
                array(
                    'table' => 'current_contacts CC',
                    'condition' => 'E.employee_id=CC.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id=ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id AND PM.current=1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'salary S',
                    'condition' => 'ET.employment_id=S.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'contract C',
                    'condition' => 'C.employment_id = ET.employment_id AND C.trashed = 0 AND C.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee_contract_extensions ECE',
                    'condition' => 'ECE.contract_id = C.contract_id AND C.extension = 1 AND ECE.current = 1 AND ECE.trashed = 1',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.trashed' => '0',
                'E.employee_id' => $employeeID
            );
            $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);

            //Now Need to Get Company Information
            $tbl = 'organization O';
            $org_data = ('*');
            $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
            //Now need to get the General Format for User..
            $table = 'ml_letter_text';
            $letterData = ('LetterText');
            $condition = array(
                'LetterTextID' => '2'
            );
            $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        } else {
            $data = array();
        }

        //Need Some Data For Left Menus, So Using the Code That Already is been Present in the System.
        $data['employee_id'] = $this->uri->segment(3);
        $where = array(
            'employee_id' => $data['employee_id']
        );
        $data['employee'] = $this->hr_model->get_row_by_where('employee', $where);

        //Now Just Display the View and Send the Above Data to that View
        $this->load->view('includes/header');
        $this->load->view('HR/contractLetter', $data);
        $this->load->view('includes/footer');
    }


    /**
     * Projects Allocation
     */
    public function view_projects_management($viewType = NULL){
        if($viewType == 'DT'){
            //This Section is For Loading the DataTables.
            $table = "employee E";
            $data = array('E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.current = 1 AND PM.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'posting P',
                    'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.current = 1 AND P.status = 2',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0
            );
            $addColumn = array(
                'Manage' => '<input type="button" class="btn green ManageProjectsButton" value="Manage">'
            );
            $group_by = '';
            $result = $this->common_model->select_fields_joined_DT($data,$table,$joins,$where,'','',$group_by,$addColumn);
            print_r($result);
            return;
        }

        //Loading The View For The Loading Active Employees
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/projectsManagement');
        $this->load->view('includes/footer');
    }
    public function manage_employee_projects(){
        $employee = $this->input->post('employee');
        if(isset($employee) and !empty($employee)){
            $table = "employee E";
            $data = 'E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode, MLD.department_name AS Department, MLDg.designation_name AS Designation';
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.current = 1 AND PM.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'posting P',
                    'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.current = 1 AND P.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_department MLD',
                    'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.employee_id' => $employee,
                'E.enrolled' => 1,
                'E.trashed' => 0
            );
            $group_by = '';
            $viewData['employeeData'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE,'','',$group_by);

            //Now We Need To Get The Available Projects...
            $table = 'ml_projects MLP';
            $data = array('EP.ProjectID,MLP.project_id AS AllProjectIDs, MLP.project_title AS ProjectTitle, MLP.project_start_date AS ProjectStartDate, CONCAT(IFNULL(EP.EmployeePercentCharged,0),"%") AS EmployeePercentCharged, EP.ProjectAssignedDate',false);
            $joins = array(
                array(
                    'table' => '(SELECT MLP.project_id AS ProjectID, EPP.percentCharge AS EmployeePercentCharged, EP.project_assign_date AS ProjectAssignedDate FROM
  ml_projects MLP
  INNER JOIN employee_project EP
    ON EP.project_id = MLP.project_id
    AND EP.current = 1
    AND EP.trashed = 0
    LEFT JOIN employee_project_percentage EPP
    ON EPP.employeeProjectID = EP.employee_project_id AND EPP.trashed = 0 AND EPP.current = 1
    AND EPP.trashed = 0
    INNER JOIN employment ET
        ON ET.employment_id = EP.employment_id
        AND ET.trashed = 0
        AND ET.current = 1
    INNER JOIN employee E
        ON ET.employee_id = E.employee_id
    WHERE E.enrolled = 1
      AND E.trashed = 0
      AND E.employee_id = '.$employee.'
      AND MLP.trashed = 0
  ) EP',
                    'condition' => 'EP.ProjectID = MLP.project_id',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'MLP.trashed' => 0,
                'MLP.project_status_type_id !=' => 4
            );
            $resultEmployeeProjects = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE);
//                $viewData['employeeProjects'] = json_encode($resultEmployeeProjects);
            $viewData['employeeProjects'] = $resultEmployeeProjects;



            // Now Lets Work Employee Projects For Current Month.
            $joins = array(
                array(
                    'table' => '(SELECT MLP.project_id AS ProjectID, EPP.percentCharge AS EmployeePercentCharged, EP.project_assign_date AS ProjectAssignedDate FROM
  ml_projects MLP
  INNER JOIN employee_project EP
    ON EP.project_id = MLP.project_id
    AND EP.trashed = 0
    LEFT JOIN employee_project_percentage EPP
    ON EPP.employeeProjectID = EP.employee_project_id
    AND EPP.trashed = 0
    AND EPP.current = 1
    AND EPP.percentageToBeAppliedOn = (SELECT MAX(percentageToBeAppliedOn) FROM employee_project_percentage INNER JOIN employee_project ON employee_project_percentage.employeeProjectID = employee_project.employee_project_id WHERE MONTH(percentageToBeAppliedOn) = MONTH(NOW()))
    INNER JOIN employment ET
        ON ET.employment_id = EP.employment_id
        AND ET.trashed = 0
        AND ET.current = 1
    INNER JOIN employee E
        ON ET.employee_id = E.employee_id
    WHERE E.enrolled = 1
      AND E.trashed = 0
      AND E.employee_id = '.$employee.'
      AND MLP.trashed = 0
  ) EP',
                    'condition' => 'EP.ProjectID = MLP.project_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'MLP.trashed = 0 AND MLP.project_status_type_id != 4';
            $viewData['employeeProjectsInCurrentMonth'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE);

//                echo $this->db->last_query();
        }else{
            $msg = "Post Data is Not Good, Please Contact System Administrator::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('human_resource/view_projects_management');
            return;
        }

        //Getting Months for Current And Next.
        $viewData['currentMonth'] = date('F-Y');
        $currentDate = date("Y-m-1");// current date
        $nextMonthDate = strtotime(date("Y-m-d", strtotime($currentDate)) . " +1 month");
        $viewData['nextMonth'] = date('F-Y',$nextMonthDate);

        //Loading The View For The Employee Projects Management..
        $this->load->view('includes/header');
        $this->load->view('HR/Reports/employeeProjects',$viewData);
        $this->load->view('includes/footer');
    }

    function update_employee_projects(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployee = $this->data['EmployeeID'];
//                getting All Posted Values
                $employeeID = $this->input->post('empID');
                $jsonDataProjectsCurrentMonth = $this->input->post('currentMonthJsonData');
                $jsonDataProjectsNextMonth = $this->input->post('nextMonthJsonData');
                //Now Lets Work On Allocation Of Revoking of Projects From Posted Employee.
                if(!isset($employeeID) || empty($employeeID) || !is_numeric($employeeID) || !isset($jsonDataProjectsNextMonth) || empty($jsonDataProjectsCurrentMonth) || $jsonDataProjectsCurrentMonth === '[]' || empty($jsonDataProjectsNextMonth) || $jsonDataProjectsNextMonth === '[]'){
                    echo "FAIL::There was some Issue with The Posted Data, PLease Contact System Administrator For Further Assistance::error";
                    return;
                }
                //Need To Find Employment ID First..
                $employmentTable = 'employment';
                $selectData = 'employment_id AS EmploymentID';
                $where = array(
                    'employee_id' => $employeeID,
                    'trashed' => 0,
                    'current' => 1
                );
                $employmentInfo = $this->common_model->select_fields_where($employmentTable,$selectData,$where,TRUE);
                if(!isset($employmentInfo) || empty($employmentInfo)){
                    echo "FAIL::This User Don't Have Any Active Employment::error";
                    return;
                }else{
                    $employmentID = $employmentInfo->EmploymentID;
                }

                $CurrentMonthProjectsInfoArray= json_decode($jsonDataProjectsCurrentMonth);
                $NextMonthProjectsInfoArray= json_decode($jsonDataProjectsNextMonth);



                //First We Need To Find Out How Many Projects Have Been Assigned now to This Current Employee..
                $table = 'employee_project';
                $selectData = 'project_id';
                $where = array(
                    'trashed' => 0,
                    'employment_id' => $employmentID,
                    'current' => 1
                );
                $projectIDs = $this->common_model->select_fields_where($table,$selectData,$where);
//                print_r($projectIDs);

                //Now Need To Find Out What Projects Have Been Selected To Insert or Update.
                $ProjectsToInsert = array();
                $ProjectsToUpdate = array();
                $ProjectsToTrash = array();

                //Getting Projects That Only Needs Updating.
                if(isset($projectIDs) && !empty($projectIDs)){
                    foreach($CurrentMonthProjectsInfoArray as $postedProjectKey=>$postedProjectValue){
                        foreach($projectIDs as $assignedProjectKey => $assignedProjectValue){
                            if($postedProjectValue->proID === $assignedProjectValue->project_id){
                                $array = array(
                                    'project_id' => $postedProjectValue->proID,
                                    'percentCharged' => $postedProjectValue->percentCharge
                                );
                                array_push($ProjectsToUpdate,$array);
                            }
                        }
                    }
                }

                //Getting Projects That Only Needs Insertion.
                $ProjectsToInsert = $CurrentMonthProjectsInfoArray;
                if(!empty($ProjectsToInsert)){
                    foreach($ProjectsToUpdate as $updateProjectKey=>$updateProjectValue){
                        foreach($ProjectsToInsert as $insertProjectKey=>$insertProjectValue){
//                           echo $insertProjectValue->proID.'='.$updateProjectValue['project_id'];
                            if($insertProjectValue->proID === $updateProjectValue['project_id']){
                                unset($ProjectsToInsert[$insertProjectKey]);
                            }
                        }
                    }
                }

                //Now Finally Code, Trash Records Which Needs Trashing..
                $ProjectsToTrash = $projectIDs;
                if(isset($ProjectsToTrash) && !empty($ProjectsToTrash)){
                    foreach($ProjectsToTrash as $trashProjectKey=> $trashProjectValue){
                        foreach($ProjectsToUpdate as $updateProjectKeySub => $updateProjectValueSub){
                            if($trashProjectValue->project_id === $updateProjectValueSub['project_id']){
                                unset($ProjectsToTrash[$trashProjectKey]);
                            }
                        }
                        foreach($ProjectsToInsert as $insertProjectKey=> $insertProjectValue){
                            if($trashProjectValue->project_id === $insertProjectValue->proID){
                                unset($ProjectsToTrash[$trashProjectKey]);
                            }
                        }
                    }
                }

                //Need To Know If There Are AnyThing Left To Trash After UnSetting The UpdateProject And InsertProject
                if(is_array($ProjectsToTrash) && !empty($ProjectsToTrash)){
                    $ProjectsToTrashArray = json_decode(json_encode($ProjectsToTrash),true);
                    foreach($ProjectsToTrashArray as $trashProjectKey=>$trashProjectValue){
                        $ProjectsToTrashArray[$trashProjectKey]['project_revoke_date'] = $this->data['dbCurrentDate'];
                        $ProjectsToTrashArray[$trashProjectKey]['trashed'] = 1;
                    }
                }

//               echo "Projects To Insert";
                if(isset($ProjectsToInsert) && !empty($ProjectsToInsert)){
                    foreach($ProjectsToInsert as $key=>$val){
                        $ProjectsToInsert[$key]->dateCreated = $this->data['dbCurrentDate'];
                        $ProjectsToInsert[$key]->trashed = 0;
                    }
                }

                //To Start Queries We First Need To Apply a Little Check, To See if All Active Projects Percentages Sum Up to 100%
                $totalPercent = 0;
                //First Sum Up All The Percentages in InsertArray
                if(isset($ProjectsToInsert) && !empty($ProjectsToInsert)){
                    $insertProjectSum = 0;
                    foreach($ProjectsToInsert as $val){
                        $insertProjectSum +=  intval($val->percentCharge);
                    }
                    $totalPercent += $insertProjectSum;
                }
                //Now Lets Sum All the Updating Projects If Exist in UpdateArray.
                if(isset($ProjectsToUpdate) && !empty($ProjectsToUpdate)){
                    $updateProjectSum = 0;
                    foreach($ProjectsToUpdate as $val){
                        $updateProjectSum +=  intval($val['percentCharged']);
                    }
                    $totalPercent += $updateProjectSum;
                }

                if($totalPercent !== 100){
                    echo "FAIL::Total Active Project Charge Percentages Must Sum Up to <b>100%</b>::error";
                    return;
                }

                //As We Got All The Data So Now We Need To Run The Queries.
                $this->db->trans_begin();
                $table = 'employee_project';
                //First Run The Query For Trash.
                if(isset($ProjectsToTrashArray) && !empty($ProjectsToTrashArray)){
//                    print_r($ProjectsToTrashArray);
                    $ProjectIDsToTrash = implode(',',array_column($ProjectsToTrashArray,'project_id'));
                    //If User Revokes Project, It Will Always Be Revoked In the Next Month
                    $date = date('Y-m-1');
                    //First Do The Trashed of Data If Any.
                    $updateData = array(
                        'trashed' => 1,
//                        'project_revoke_date' => $this->data['dbCurrentDate'],
                        'project_revoke_date' => $date,
                        'current' => 0
                    );
                    $whereTrash = 'trashed = 0 AND employment_id ='.$employmentID.' AND project_id IN ('.$ProjectIDsToTrash.')';
                    $trashResult = $this->common_model->update($table,$whereTrash,$updateData);
                    //Roll Back If Some Issue Occurred.
                    if($trashResult !== true){
                        $this->db->trans_rollback();
                    }
                }
                //Now Lets Run The Query For Updates, These Queries Will Run in Loop
                if(isset($ProjectsToUpdate) && !empty($ProjectsToUpdate)){
                    foreach($ProjectsToUpdate as $key=>$val){
                        //First Find Out the employee_project ID to Update In the Respective Record on the employee_project_percent table.
                        $selectProjectsToUpdateData = array('employee_project_id AS EmployeeProjectID');
                        $whereSelect = array(
                            'trashed' => 0,
                            'employment_id' => $employmentID,
                            'project_id' => $val['project_id']
                        );
                        $empProjectInfo = $this->common_model->select_fields_where($table,$selectProjectsToUpdateData,$whereSelect,TRUE);

                        if(!isset($empProjectInfo) || empty($empProjectInfo)){
                            echo "FAIL:: SomeThing Went Wrong When Updating The Project, Please Contact System Administrator For Futher Assistance::error";
                            $this->db->trans_rollback();
                            return;
                        }

                        //As We Got The Employee Project ID Now, Lets Update The Percentage for Respective employeeProjectID
                        $eppTable = 'employee_project_percentage';
                        $updateData = array(
                            'current' => 0,
                            'UpdatedBy' => $loggedInEmployee,
                            'dateUpdated' => $this->data['dbCurrentDateTime']
                        );
                        $whereUpdate = 'employeeProjectID = '.$empProjectInfo->EmployeeProjectID.' AND trashed = 0 AND dateUpdated IS NULL AND percentCharge != '.$val['percentCharged'];
                        $updateResult = $this->common_model->update($eppTable,$whereUpdate,$updateData);

                        if ($updateResult === true) {
                            //After Update Need To Insert New Data To Keep History..
                            $date = date('Y-m-1');
                            $insertPercentData = array(
                                'percentCharge' => $val['percentCharged'],
                                'employeeProjectID' => $empProjectInfo->EmployeeProjectID,
                                'dateCreated' => $this->data['dbCurrentDateTime'],
                                'CreatedBy' => $loggedInEmployee,
                                'percentageToBeAppliedOn' => $date,
                                'trashed' => 0
                            );
                            $insertPercentResult = $this->common_model->insert_record($eppTable, $insertPercentData);
                        }
                    }
                }
                if(isset($ProjectsToInsert) && !empty($ProjectsToInsert)){
                    //Need To Check If There Is Any Date That is Not of Current Month But of Preceding Month..

                    foreach($ProjectsToInsert as $key=>$val){
                        //Need To Insert In Two Tables. As Employee Project And Project Percentages Are Linked Tightly
                        //First Insert In Parent Table employee_project

                        $projectAssignDate = date('Y-m-1');
                        $insertData = array(
                            'percentCharged' => $val->percentCharge,
                            'project_id' => $val->proID,
                            'project_assign_date' => $projectAssignDate,
                            'dateCreated' => $val->dateCreated,
                            'trashed' => $val->trashed,
                            'employment_id' => $employmentID
                        );
                        $insertResult = $this->common_model->insert_record($table,$insertData);

                        //Now if Insert Has Been Successful Then Insert In The Child Table employee_project_percentage
                        if($insertResult > 0 ){
                            $date = date("Y-m-1");// current Month Date
                            //Time To Do The Insertion for New Percentage..
                            $projectPercentTable = 'employee_project_percentage';
                            $eppInsertData = array(
                                'percentCharge' => $val->percentCharge,
                                'employeeProjectID' => $insertResult,
                                'dateCreated' => $this->data['dbCurrentDateTime'],
                                'percentageToBeAppliedOn' => $date,
                                'CreatedBy' => $loggedInEmployee,
                                'trashed' => 0
                            );
                            $eppInsertResult = $this->common_model->insert_record($projectPercentTable,$eppInsertData);
                        }
                    }
                }
                if($this->db->trans_status() !== FALSE){
                    $this->db->trans_commit();
                }elseif($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    echo "FAIL::Some DataBase Error Occurred, If you This You Received This Error By Mistake Then Please Contact System Administrator For Futher Assistance::error";
                    return;
                }


                ///Now Need To Work For Next Month Projects
                //First We Need To Find Out How Many Projects Have Been Assigned now to This Current Employee..
                $table = 'employee_project EP';
                $selectData = 'project_id,EPP.percentCharge';
                $joins = array(
                    array(
                        'table' => 'employee_project_percentage EPP',
                        'condition' => 'EPP.employeeProjectID = EP.employee_project_id AND EPP.trashed = 0 AND EPP.current = 1',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'EP.trashed' => 0,
                    'employment_id' => $employmentID,
                    'EP.current' => 1
                );
                $projectIDs = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where);
//                print_r($NextMonthProjectsInfoArray);
//                print_r($CurrentMonthProjectsInfoArray);
//                print_r($projectIDs);
                //First Lets Unset That Data, Which We Don't Need To Update.
                $NotToRevokeProjectsInWay = $NextMonthProjectsInfoArray;
                foreach($NextMonthProjectsInfoArray as $projectKey=>$projectInfo){
                    //Need To Check If Data is Identical with The Current Month Record.
                    foreach($CurrentMonthProjectsInfoArray as $cProjectKey=>$cProjectValue){
                        if($projectInfo->proID === $cProjectValue->proID && $projectInfo->percentCharge === $cProjectValue->percentCharge){
                            unset($NextMonthProjectsInfoArray[$projectKey]);
                        }
                    }
                }
//                print_r($NextMonthProjectsInfoArray);
                //Now Lets Check If There Are AnyThing To Be Assigned Or Be Updated In Next Month
                if(!empty($NextMonthProjectsInfoArray) && is_array($NextMonthProjectsInfoArray)){

                    //Seems Like Now We Need To Process This Mess Now.
                    // Need To Assign Update Revoke Accordingly...
                    //NM denotes "Next Month"

                    $NMProjectsToAssign = array();
                    $NMProjectsToUpdate = array();
                    $NMProjectsToRevoke = array();


                    //Getting Projects That Only Needs Updating.
                    if(isset($projectIDs) && !empty($projectIDs)){
                        foreach($NextMonthProjectsInfoArray as $postedProjectKey=>$postedProjectValue){
                            foreach($projectIDs as $assignedProjectKey => $assignedProjectValue){
                                if($postedProjectValue->proID === $assignedProjectValue->project_id){
                                    $array = array(
                                        'project_id' => $postedProjectValue->proID,
                                        'percentCharged' => $postedProjectValue->percentCharge
                                    );
                                    array_push($NMProjectsToUpdate,$array);
                                }
                            }
                        }
                    }


                    //Getting Projects That Only Needs Insertion.
                    $NMProjectsToAssign = $NextMonthProjectsInfoArray;
                    if(!empty($NMProjectsToAssign)){
                        foreach($NMProjectsToUpdate as $updateProjectKey=>$updateProjectValue){
                            foreach($NMProjectsToAssign as $insertProjectKey=>$insertProjectValue){
                                if($insertProjectValue->proID === $updateProjectValue['project_id']){
                                    unset($NMProjectsToAssign[$insertProjectKey]);
                                }
                            }
                        }
                    }

                    //Now Finally Code, Trash Records Which Needs Trashing..
                    $NMProjectsToRevoke = $projectIDs;
                    if(isset($NMProjectsToRevoke) && !empty($NMProjectsToRevoke) && is_array($NMProjectsToRevoke)){
                        foreach($NMProjectsToRevoke as $trashProjectKey=> $trashProjectValue){
                            foreach($NMProjectsToUpdate as $updateProjectKeySub => $updateProjectValueSub){
                                if($trashProjectValue->project_id === $updateProjectValueSub['project_id']){
                                    unset($NMProjectsToRevoke[$trashProjectKey]);
                                }
                            }
                            foreach($NMProjectsToAssign as $insertProjectKey=> $insertProjectValue){
                                if($trashProjectValue->project_id === $insertProjectValue->proID){
                                    unset($NMProjectsToRevoke[$trashProjectKey]);
                                }
                            }
                            if(!empty($NotToRevokeProjectsInWay) && is_array($NotToRevokeProjectsInWay)){
                                foreach($NotToRevokeProjectsInWay as $noRevokeProjectKey=>$noRevokeProjectInfo){
                                    if($noRevokeProjectInfo->proID === $trashProjectValue->project_id){
                                        unset($NMProjectsToRevoke[$trashProjectKey]);
                                    }
                                }
                            }
                        }
                    }


                    //Need To Know If There Are AnyThing Left To Trash After UnSetting The UpdateProject And InsertProject
                    if(is_array($NMProjectsToRevoke) && !empty($NMProjectsToRevoke)){
                        $NMProjectsToRevokeArray = json_decode(json_encode($NMProjectsToRevoke),true);
                        foreach($NMProjectsToRevokeArray as $trashProjectKey=>$trashProjectValue){
                            $NMProjectsToRevokeArray[$trashProjectKey]['project_revoke_date'] = $this->data['dbCurrentDate'];
                            $NMProjectsToRevokeArray[$trashProjectKey]['trashed'] = 1;
                        }
                    }

//                    print_r($NMProjectsToRevoke);
                    ///Finally Got All The Insert Update and Revoke Options..
//                    Just Only Need To Update Records Accordingly In Database..
                    $this->db->trans_begin();
                    $table = 'employee_project';
                    //First Run The Query For Trash.
                    if(isset($NMProjectsToRevokeArray) && !empty($NMProjectsToRevokeArray)){
//                    print_r($ProjectsToTrashArray);
                        $ProjectIDsToRevoke = implode(',',array_column($NMProjectsToRevokeArray,'project_id'));
                        //If User Revokes Project, It Will Always Be Revoked In the Next Month
                        $date = date('Y-m-1');
                        $projectToBeRevokeOnDate = strtotime(date("Y-m-d", strtotime($date)) . " +1 month"); //Getting Next Month Date
                        $date = date('Y-m-d',$projectToBeRevokeOnDate);
                        //First Do The Trashed of Data If Any.
                        $updateData = array(
                            'trashed' => 0,
//                        'project_revoke_date' => $this->data['dbCurrentDate'],
                            'project_revoke_date' => $date,
                            'current' => 0
                        );
                        $whereTrash = 'trashed = 0 AND employment_id ='.$employmentID.' AND project_id IN ('.$ProjectIDsToRevoke.')';
                        $trashResult = $this->common_model->update($table,$whereTrash,$updateData);
                        //Roll Back If Some Issue Occurred.
                        if($trashResult !== true){
                            $this->db->trans_rollback();
                        }
                    }


                    //Now Lets Run The Query For Updates, These Queries Will Run in Loop
                    if(isset($NMProjectsToUpdate) && !empty($NMProjectsToUpdate)){
                        foreach($NMProjectsToUpdate as $key=>$val){
                            //First Find Out the employee_project ID to Update In the Respective Record on the employee_project_percent table.
                            $selectProjectsToUpdateData = array('employee_project_id AS EmployeeProjectID');
                            $whereSelect = array(
                                'trashed' => 0,
                                'employment_id' => $employmentID,
                                'project_id' => $val['project_id']
                            );
                            $empProjectInfo = $this->common_model->select_fields_where($table,$selectProjectsToUpdateData,$whereSelect,TRUE);

                            if(!isset($empProjectInfo) || empty($empProjectInfo)){
                                echo "FAIL:: SomeThing Went Wrong When Updating The Project, Please Contact System Administrator For Further Assistance::error";
                                $this->db->trans_rollback();
                                return;
                            }

                            //As We Got The Employee Project ID Now, Lets Update The Percentage for Respective employeeProjectID
                            $eppTable = 'employee_project_percentage';
                            $updateData = array(
                                'current' => 0,
                                'UpdatedBy' => $loggedInEmployee,
                                'dateUpdated' => $this->data['dbCurrentDateTime']
                            );
                            $whereUpdate = 'employeeProjectID = '.$empProjectInfo->EmployeeProjectID.' AND trashed = 0 AND dateUpdated IS NULL AND percentCharge != '.$val['percentCharged'];
                            $updateResult = $this->common_model->update($eppTable,$whereUpdate,$updateData);

                            if ($updateResult === true) {
                                //After Update Need To Insert New Data To Keep History..
                                $date = date('Y-m-1');
                                $nextMonthStartDate = strtotime(date("Y-m-d", strtotime($date)) . " +1 month");
                                $insertPercentData = array(
                                    'percentCharge' => $val['percentCharged'],
                                    'employeeProjectID' => $empProjectInfo->EmployeeProjectID,
                                    'dateCreated' => $this->data['dbCurrentDateTime'],
                                    'CreatedBy' => $loggedInEmployee,
                                    'percentageToBeAppliedOn' => date('Y-m-d',$nextMonthStartDate),
                                    'trashed' => 0
                                );
                                $insertPercentResult = $this->common_model->insert_record($eppTable, $insertPercentData);
                            }
                        }
                    }

                    if(isset($NMProjectsToAssign) && !empty($NMProjectsToAssign)){
                        //Need To Check If There Is Any Date That is Not of Current Month But of Preceding Month..
                        $nextMonthStartDate = strtotime(date("Y-m-d", strtotime($date)) . " +1 month");
                        foreach($NMProjectsToAssign as $key=>$val){
                            //Need To Insert In Two Tables. As Employee Project And Project Percentages Are Linked Tightly
                            //First Insert In Parent Table employee_project

                            $projectAssignDate = date('Y-m-1',$nextMonthStartDate);
                            $insertData = array(
                                'percentCharged' => $val->percentCharge,
                                'project_id' => $val->proID,
                                'project_assign_date' => $projectAssignDate,
                                'dateCreated' => $this->data['dbCurrentDate'],
                                'trashed' => 0,
                                'employment_id' => $employmentID,
                                'current' => 1
                            );
                            $insertResult = $this->common_model->insert_record($table,$insertData);

                            //Now if Insert Has Been Successful Then Insert In The Child Table employee_project_percentage
                            if($insertResult > 0 ){
                                $date = date("Y-m-1",$nextMonthStartDate);// Next Month Date
                                //Time To Do The Insertion for New Percentage..
                                $projectPercentTable = 'employee_project_percentage';
                                $eppInsertData = array(
                                    'percentCharge' => $val->percentCharge,
                                    'employeeProjectID' => $insertResult,
                                    'dateCreated' => $this->data['dbCurrentDateTime'],
                                    'percentageToBeAppliedOn' => $date,
                                    'CreatedBy' => $loggedInEmployee,
                                    'trashed' => 0,
                                    'current' => 1
                                );
                                $eppInsertResult = $this->common_model->insert_record($projectPercentTable,$eppInsertData);
                            }
                        }
                    }

                    //Finally A Validation of Success. Commit or RollBack According to Success or Fail respectively.
                    if ($this->db->trans_status() !== FALSE) {
                        $this->db->trans_commit();
                        echo "OK::Records Successfully Updated For Allocation Of Projects::success";
                        return;
                    } elseif ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        echo "FAIL::Some DataBase Error Occurred, If you This You Received This Error By Mistake Then Please Contact System Administrator For Futher Assistance::error";
                        return;
                    }


                }


            }else{
                echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator::error";
                return;
            }
        }else{
            //Redirect With Message if Post was Not Done From Ajax..

        }
    }

    //This Should Be Simple Employee Projects.
    function employee_projects($employeeID)
    {
        //Demo Employee ID For Now.

        if(!isset($employeeID) || empty($employeeID) || !is_numeric($employeeID)){
            //Redirect To Previous URL if EmployeeID have not been Provided.
            redirect(previousURL());
        }
        $employee = $employeeID;



        //Get Employee Basic Info To Show On Page.
        $table = "employee E";
        $data = 'E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode, MLD.department_name AS Department, MLDg.designation_name AS Designation';
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.current = 1 AND PM.status = 2',
                'type' => 'INNER'
            ),
            array(
                'table' => 'posting P',
                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.current = 1 AND P.status = 2',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_department MLD',
                'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = array(
            'E.employee_id' => $employee,
            'E.enrolled' => 1,
            'E.trashed' => 0
        );
        $group_by = '';
        $viewData['employeeData'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE,'','',$group_by);

        //Now We Need To Get The Available Projects...
        $table = 'ml_projects MLP';
        $data = array('EP.ProjectID,MLP.project_id AS AllProjectIDs, MLP.project_title AS ProjectTitle, MLP.project_start_date AS ProjectStartDate, CONCAT(IFNULL(EP.EmployeePercentCharged,0),"%") AS EmployeePercentCharged, EP.ProjectAssignedDate',false);
        $joins = array(
            array(
                'table' => '(SELECT MLP.project_id AS ProjectID, EPP.percentCharge AS EmployeePercentCharged, EP.project_assign_date AS ProjectAssignedDate FROM
  ml_projects MLP
  INNER JOIN employee_project EP
    ON EP.project_id = MLP.project_id
    AND EP.current = 1
    AND EP.trashed = 0
    LEFT JOIN employee_project_percentage EPP
    ON EPP.employeeProjectID = EP.employee_project_id AND EPP.trashed = 0 AND EPP.current = 1
    AND EPP.trashed = 0
    INNER JOIN employment ET
        ON ET.employment_id = EP.employment_id
        AND ET.trashed = 0
        AND ET.current = 1
    INNER JOIN employee E
        ON ET.employee_id = E.employee_id
    WHERE E.enrolled = 1
      AND E.trashed = 0
      AND E.employee_id = '.$employee.'
      AND MLP.trashed = 0
  ) EP',
                'condition' => 'EP.ProjectID = MLP.project_id',
                'type' => 'LEFT'
            )
        );
        $where = array(
            'MLP.trashed' => 0,
            'MLP.project_status_type_id !=' => 4
        );
        $resultEmployeeProjects = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE);
//                $viewData['employeeProjects'] = json_encode($resultEmployeeProjects);
        $viewData['employeeProjects'] = $resultEmployeeProjects;


//        echo $this->db->last_query();

        $this->load->view('includes/header');
        $this->load->view('HR/Reports/employeeProjectsSimple',$viewData);
        $this->load->view('includes/footer');
    }


    //method to update employee projects, a simple one.
    function update_employee_projects_simple(){
        if($this->input->is_ajax_request()){
            $toAssignProjects = $this->input->post('ToAssignProjects');
            $toRevokeProjects = $this->input->post('ToRevokeProjects');
            $postedEmployeeID = $this->input->post('empID');

            $employmentID = get_employment_from_employeeID($postedEmployeeID);


            //We are getting Values in JSON, SO In Order to Use Them, Need to Convert To Array First.
            $toAssignArray = json_decode($toAssignProjects);
            $toRevokeArray = json_decode($toRevokeProjects);

            if(empty($toAssignArray) && empty($toRevokeArray)){
                echo "FAIL::Nothing To Update::warning";
                return;
            }


            $this->db->trans_start();
            foreach($toAssignArray as $key=>$val){
                $table = 'employee_project';
                if(!isset($val->projectAssignDate) || empty($val->projectAssignDate)){
                    $projectAssignDate = date("Y-m-d");
                }else{
                    $projectAssignDate = date("Y-m-d",strtotime($val->projectAssignDate));
                }
                $insertNewProjectsData =array(
                    'employment_id' => $employmentID,
                    'project_id' => $val->proID,
                    'project_assign_date' => $projectAssignDate,
                    'current' => 1,
                    'trashed' => 0,
                    'percentCharged' => 0,                    //Putting It On Zero, As Assigning Seems More Complex.
                    'dateCreated' => $this->data['dbCurrentDate']
                );

                if ($val->status === 'ToAssign') {
                    $insertedRecordID = $this->common_model->insert_record($table, $insertNewProjectsData);

                    ///Now Need To Do the Entry For Project Percentage
                    $table = 'employee_project_percentage';
                    $projectPercentageData = array(
                        'employeeProjectID' => $insertedRecordID,
                        'percentCharge' => 0,
                        'dateCreated' => $this->data['dbCurrentDateTime'],
                        'percentageToBeAppliedOn' => $projectAssignDate,
                        'CreatedBy' => $this->data['EmployeeID'],
                        'trashed' => 0,
                        'current' => 1
                    );
                    $this->common_model->insert_record($table, $projectPercentageData);
                }
            }


            //Now Work For Revoking Of Projects
            if(!empty($toRevokeArray)){
                foreach($toRevokeArray as $key=>$val){
                    $table = 'employee_project';
                    $updateData = array(
                        'current' => 0,
                        'trashed' => 1,
                        'project_revoke_date' => $this->data['dbCurrentDate']
                    );
                    $where = array(
                        'employment_id'=>$employmentID,
                        'project_id' => $val->proID
                    );

                    if ($val->status === 'ToRevoke') {
                        $this->common_model->update($table,$where,$updateData);
                    }
                }
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                echo "FAIL::Records Could Not Be Updated, Some Database Error Occurred:error";
            }else{
                echo "OK::Records Successfully Updated::success";
            }
            return;
        }
        return;
    }

    //Disciplinary Action Section Code..
    function disciplinary_action($list = NULL){
        if($list === 'list'){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'disciplinary_action_reports DAR';
            $selectData = array('
            DAR.DAreportID AS ReportID,
            DAR.disciplineBreached AS BreachedDiscipline,
            DATE_FORMAT(DAR.dateOfBreach,"%d-%m-%Y") AS DateOfBreach,
            RE.full_name AS ReportedBy,
            DAR.reportingDate AS DateReported,
            CASE WHEN DAR.status = 0 THEN "Pending Investigation" WHEN DAR.status = 1 THEN "Pending Review" WHEN DAR.status = 2 THEN "Reviewed Report" END AS ReportStatus,
            MLDA.action_type AS DisciplinaryAction,
            IE.full_name AS InvestigatedBy,
            AE.full_name AS AccusedEmployee,
            DAR.employeeFeedBack',false);
            $joins = array(
                array(
                    'table' => 'employee RE',
                    'condition' => 'DAR.reportedBy = RE.employee_id AND RE.trashed = 0',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'employee IE',
                    'condition' => 'DAR.investigatedBy = IE.employee_id AND IE.trashed = 0',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_disciplinary_action MLDA',
                    'condition' => 'MLDA.disciplinaryActionID = DAR.disciplinaryActionID',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'employment AET', //Join For Accused Employee
                    'condition' => 'AET.employment_id = DAR.employmentID AND AET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee AE',
                    'condition' => 'AE.employee_id = AET.employee_id AND AE.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'DAR.trashed' => 0
            );
            $add_column = array(
                'ViewEditActionButtons' => '<a style="cursor:pointer;cursor:hand;" class="disciplinaryActionReportEdit"><span class="fa fa-pencil"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp; <a style="cursor:pointer;cursor:hand;"  class="disciplinaryActionReportViewDetails"><span class="fa fa-eye"></span></a>'
            );
            //Need To Check For Filters..
            $filteredDisciplinaryAction = $this->input->post('filteredDisciplinaryAction');
            if(isset($filteredDisciplinaryAction) && !empty($filteredDisciplinaryAction)){
                $where['MLDA.disciplinaryActionID'] = $filteredDisciplinaryAction;
            }
//            print_r($this->input->post());
            $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$add_column);
            echo $listResult;
            return;
        }
        //Loading The View For The Disciplinary Action Management..
        $this->load->view('includes/header');
        $this->load->view('HR/disciplinary_action');
        $this->load->view('includes/footer');
    }
    function get_report_details($modalType = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //if Came Through Proper Channel Then Lets Get The Data..
                $reportID = $this->input->post('reportID');
                if(!isset($reportID) || empty($reportID)){
                    echo "FAIL::Something Went Wrong with the POST, Please Contact System Administrator For Futher Assistance::error";
                    return;
                }
                $table = 'disciplinary_action_reports DAR';
                if($modalType === 'edit'){
                    $selectData = array('AE.full_name AS AccusedEmployeeName,AE.thumbnail AS EmployeeAvatar, AE.employee_id AS AccusedEmployeeID,DAR.disciplineBreached AS BreachedDiscipline, DATE_FORMAT(DAR.dateOfBreach,"%d-%m-%Y") AS BreachedDate,DAR.disciplineBreachedDescription AS BreachedDisciplineDescription',false);
                    $joins = array(
                        //Joining For Accused Employee
                        array(
                            'table' => 'employment AET',
                            'condition' => 'AET.employment_id = DAR.employmentID AND AET.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employee AE',
                            'condition' => 'AE.employee_id = AET.employee_id AND AE.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'DAR.DAreportID' => $reportID
                    );

                }elseif($modalType === 'view'){
                    //If View Button Is Pressed in The DataTable.
                    $selectData = array('
                    AE.employee_code AS AccusedEmployeeCode,
                    AE.full_name AS AccusedEmployeeName,
                    AE.thumbnail AS EmployeeAvatar,
                    AE.employee_id AS AccusedEmployeeID,
                    DAR.disciplineBreached AS BreachedDiscipline,
                    DATE_FORMAT(DAR.dateOfBreach,"%d-%m-%Y") AS BreachedDate,
                    DAR.disciplineBreachedDescription AS BreachedDisciplineDescription,
                    MLDA.action_type AS DisciplinaryActionType,
                    IE.full_name AS InvestigatedByEmployeeName,
                    RE.full_name AS ReportedByEmployeeName,
                    DAR.employeeFeedBack AS EmployeeFeedBack,
                    DAR.reviewerRemarks AS ReviewerRemarks,
                    DAR.investigatorRemarks AS InvestigatorRemarks,
                    CASE WHEN DAR.status = 0 THEN "Pending Investigation" WHEN DAR.status = 1 THEN "Pending Review" WHEN DAR.status = 2 THEN "Reviewed Report" END AS ReportStatus
                    ',
                        false
                    );
                    $joins = array(
                        //Joining For Accused Employee
                        array(
                            'table' => 'employment AET',
                            'condition' => 'AET.employment_id = DAR.employmentID AND AET.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employee AE',
                            'condition' => 'AE.employee_id = AET.employee_id AND AE.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_disciplinary_action MLDA',
                            'condition' => 'MLDA.disciplinaryActionID = DAR.disciplinaryActionID AND MLDA.trashed = 0',
                            'type' => 'LEFT'
                        ),
                        array(
                            'table' => 'employee RE',
                            'condition' => 'RE.employee_id = DAR.reportedBy AND RE.trashed = 0',
                            'type' => 'LEFT'
                        ),
                        array(
                            'table' => 'employee IE',
                            'condition' => 'IE.employee_id = DAR.investigatedBy AND IE.trashed = 0',
                            'type' => 'LEFT'
                        )
                    );
                    $where = array(
                        'DAR.DAreportID' => $reportID
                    );
                }else{
                    echo "FAIL::Something Went Wrong with The Link, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                $reportDetails = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,TRUE);
                //Need To Send Avatar Link For Employee
                if(isset($reportDetails) && !empty($reportDetails)){
                    $employeeAvatar = empAvatarExist($reportDetails->EmployeeAvatar);
                    if($employeeAvatar === TRUE){
                        $reportDetails->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$reportDetails->EmployeeAvatar);
                    }else{
                        $reportDetails->EmployeeAvatar = $employeeAvatar;
                    }
                }
                //Finally Print Out the Json Encoded Report Details.
                print_r(json_encode($reportDetails));
                return;
            }
        }
    }
    function report_discipline_breach(){
        if($this->input->post()){
            if($this->input->is_ajax_request()){
                //Add Report If Request Generated Through Proper Channel.
                //Getting All The Posted Values.
                $employeeID = $this->input->post('addEmployee');
                $disciplineBreached = $this->input->post('addDisciplineBreached');
                $disciplineBreachedDesc = $this->input->post('addDisciplineBreachedDesc');
                $dateOfOccurrence = $this->input->post('addDateOfOccurrence');
//                $investigatedBy = $this->input->post('investigatedBy');
                //////////////////////////
                $loggedInEmployee = $this->data['EmployeeID'];

                ///Checking If Values POSTED OK.
                if(!isset($employeeID) || empty($employeeID) || !is_numeric($employeeID)){
                    echo "FAIL::Please Select Employee From The Dropdown::error";
                    return;
                }
                if(!isset($disciplineBreached) || empty($disciplineBreached)){
                    echo "FAIL::Please Enter The Discipline that Breached.::error";
                    return;
                }
                if(!isset($dateOfOccurrence) || empty($dateOfOccurrence)){
                    echo "FAIL::Please Select Date From DatePicker for Occurance::error";
                    return;
                }else{
                    $dateExploded = explode('-',$dateOfOccurrence);
                    $dateOfOccurrence = $dateExploded[2].'-'.$dateExploded[1].'-'.$dateExploded[0];
                }

                //Need The Employment ID..
                $employmentID = get_employment_from_employeeID($employeeID);

                //Gonna Do The Insertions Now.
                $table = 'disciplinary_action_reports';
                $insertData = array(
                    'employmentID' => $employmentID,
                    'disciplineBreached' => $disciplineBreached,
                    'dateOfBreach' => $dateOfOccurrence,
                    'reportedBy' => $loggedInEmployee,
                    'reportingDate' => $this->data['dbCurrentDate']
                );
                $result = $this->common_model->insert_record($table,$insertData);
                if($result > 0){
                    echo "OK::Record Successfully Added To The System::success";
                    return;
                }
            }
        }
    }

    function update_investigation_report()
    {
        //update the Investigation Report.
        if ($this->input->is_ajax_request()) {

            if ($this->input->post()) {
                //Getting All The Posted Values
                $employeeFeedBack = $this->input->post('employeeRemarks');
                $investigatorRemarks = $this->input->post('investigatorRemarks');
                $IReportID = $this->input->post('IReportID');
                $loggedInEmployeeID = $this->data['EmployeeID'];

                if (!isset($investigatorRemarks) || empty($investigatorRemarks)) {
                    echo "FAIL::Please Add Some Description Regarding Your Investigation::error";
                    return;
                }

                if (!isset($IReportID) || empty($IReportID) || !is_numeric($IReportID)) {
                    echo "FAIL::Something Went Wrong With The POST, Please Contact System Administrator For Further Assistance::error";
                    return;
                }

                $table = 'disciplinary_action_reports';
                $insertData = array(
                    'investigatorRemarks' => $investigatorRemarks,
                    'investigatedBy' => $loggedInEmployeeID,
                    'employeeFeedBack' => $employeeFeedBack,
                    'status' => 1
                );
                $where = array(
                    'DAreportID' => $IReportID
                );
                $result = $this->common_model->update($table, $where, $insertData);
                if ($result === true) {
                    echo "OK::Investigation Report Successfully Updated::success";
                    return;
                }
            }
        }
    }

    //Select2 DropDown Functions
    function loadAvailableDisciplinaryActions(){
        if($this->input->is_ajax_request()){
            $table = 'ml_disciplinary_action MLDA';
            $selectData = 'action_type AS DisciplinaryAction, disciplinaryActionID AS DisciplinaryActionID';
            $where = array(
                'trashed' => 0
            );
            if($this->input->post('term')){
                $searchValue = $this->input->post('term');
                $field = 'action_type';
                $result = $this->common_model->select_fields_where_like($table,$selectData,$where,FALSE,$field,$searchValue);
            }
            else{
                $result = $this->common_model->select_fields_where($table,$selectData,$where,FALSE);
            }
            $result = array_merge(array('all' => new stdClass), $result);
            $result['all']->DisciplinaryAction = 'All';
            $result['all']->DisciplinaryActionID = 0;

            print_r(json_encode($result));
        }
    }

    //End Of Disciplinary Action Section Code..


    /**
     * Recruitment Module
     */
    function post_job_advertisement(){
        //If Not A Proper Post Then Redirect User To Previous Page.
        if(!$this->input->post()){
            $msg = "Direct Access Is Not Allowed, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        //If Successful POST Then Needs To Check If Its A Ajax Request.. :)
        if(!$this->input->is_ajax_request()){
            echo "FAIL::There is Some Error with POST, Please Contact System Administrator For Further Assistance::error";
            return;
        }

        //If Passed From Both Top Conditions Then Lets Do The Insertion.
        echo "Successful Post";
    }

    function available_Advert_applicants(){
        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/available_advert_applicants');
        $this->load->view('includes/footer');
    }
    function load_available_Advert_applicants(){
        $list=$this->uri->segment(3);
        if(isset($list) && !empty($list)){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'applicants AS Appl';
            $selectData = array('
            APDJOB.jobAdvertisementID AS AdvertisementID,
            Appl.applicantID AS applicantID,
            CONCAT(Appl.firstName," ",Appl.lastName) AS Name,
             Appl.contactNo AS contactNo,
            MLAST.statusTitle AS ApplicantStatus,
            JBADT.title AS Job_Title,
             Appl.officialEmailAddress AS email,
            DATE_FORMAT(APDJOB.dateAppliedOn,"%d-%M-%Y") AS AppliedDate,
            Appl.attachment AS attachment,
            APDJOB.responseStatus AS ApplicantType,
            SHI.applicantID AS Called',false);
            $joins = array(
                array(
                    'table' => 'applicant_applied_jobs APDJOB',
                    'condition' => 'Appl.applicantID = APDJOB.applicantID',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'job_advertisement JBADT',
                    'condition' => 'APDJOB.jobAdvertisementID = JBADT.advertisementID',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_applicant_status_types MLAST',
                    'condition' => 'MLAST.applicantStatusTypeID = APDJOB.responseStatus AND MLAST.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'shortlisted_interview SHI',
                    'condition' => 'SHI.applicantID = APDJOB.applicantID',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'JBADT.trashed' => 0,
                'APDJOB.jobAdvertisementID'=>$list
            );
            $add_column = array(
                'ViewApplicantDetails' => '<a style="cursor:pointer;cursor:hand;" class="AdvertViewDetails"><span class="fa fa-eye"></span></a>',
                'CheckBoxShortList' => '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]"></span>'
            );
            //Need To Check For Filters..
            $filteredCandidate = $this->input->post('filterCandidates');
            $group_by = 'Appl.applicantID';

            //If There Are Custom Filters. We Should Know.
            $jobsFilter = $this->input->post('filterApplicantJob');
            $applicantStatusFilter = $this->input->post('filterApplicantStatus');


            // But We Also Need to Check For Another Filter Which Is the depending on This Current Filter.
            if(isset($applicantStatusFilter) && !empty($applicantStatusFilter) && is_numeric($applicantStatusFilter)){
                //Now Means Both Filters Have Been Selected So Need To Query For Bother Filters.
                $where['APDJOB.responseStatus'] = intval($applicantStatusFilter);
                $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
                /// my code
                if(isset($listResult) && !empty($listResult)){
                    $jsonDecoded = json_decode($listResult,true);
                    foreach($jsonDecoded['aaData'] as $key=>$val){
                        if(intval($val['ApplicantType']) === 2 && !empty($val['Called'])){
                            $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" id="checkbox_select" disabled="disabled" title="Aleardy Called !" class="checkbox_select" value="'.$val['applicantID'].'"></span>';

                        }
                        elseif(intval($val['ApplicantType']) === 2){
                            $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" id="checkbox_select" class="checkbox_select" value="'.$val['applicantID'].'"><input type="hidden" name="AdvertiseID[]" id="AdvertisementID" class="AdvertisementID" value="'.$val['AdvertisementID'].'"></span>';

                        }else{
                            $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" disabled="disabled"></span>';
                        }
                    }
                    echo json_encode($jsonDecoded);
                    return;
                }
                // End
                echo $listResult;
                return;
            }else{
                $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
            }

            if(isset($listResult) && !empty($listResult)){
                $decodedJSON = json_decode($listResult,true);
                $newResult = array_walk_recursive($decodedJSON, function (&$item, $key) {
                    if ($key == 'attachment') {
                        if (file_exists(FCPATH . "upload/ApplicantResumes/" . $item) && !empty($item)) {
                            $fileURL = base_url() . 'upload/ApplicantResumes/' . $item;
                            $item = '<a href="' . $fileURL . '" class="AdvertViewDetails"><span class="fa fa-download"></span></a>';
                        } else {
                            $item = '';
                        }
                    }
                });
                echo json_encode($decodedJSON);
                return;
            }
            echo $listResult;
            return;
        }
    }


    function available_applicants(){
        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/available_applicants');
        $this->load->view('includes/footer');
    }

    function load_available_applicant( $list = NULL){
        if($list === 'list'){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'applicants AS Appl';
            $selectData = array('
            APDJOB.jobAdvertisementID AS AdvertisementID,
            Appl.applicantID AS applicantID,
            CONCAT(Appl.firstName," ",Appl.lastName) AS Name,
            Appl.contactNo AS contactNo,
            MLAST.statusTitle AS ApplicantStatus,
            Appl.officialEmailAddress AS email,
            JBADT.title AS Job_Title,
            DATE_FORMAT(APDJOB.dateAppliedOn,"%d-%M-%Y") AS AppliedDate,
            Appl.attachment AS attachment,
            APDJOB.responseStatus AS ApplicantType,
            SHI.applicantID AS Called',false);
            $joins = array(
                array(
                    'table' => 'applicant_applied_jobs APDJOB',
                    'condition' => 'Appl.applicantID = APDJOB.applicantID',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'job_advertisement JBADT',
                    'condition' => 'APDJOB.jobAdvertisementID = JBADT.advertisementID',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_applicant_status_types MLAST',
                    'condition' => 'MLAST.applicantStatusTypeID = APDJOB.responseStatus AND MLAST.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'shortlisted_interview SHI',
                    'condition' => 'SHI.applicantID = APDJOB.applicantID',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'applicant_skills APSkill',
                    'condition' => 'APSkill.applicantID = APDJOB.applicantID AND APSkill.trashed=0',
                    'type' => 'LEFT'
                )

            );
            $where = 'JBADT.trashed = 0';
            $add_column = array(
                'ViewApplicantDetails' => '<a style="cursor:pointer;cursor:hand;" class="AdvertViewDetails"><span class="fa fa-eye"></span></a>',
                'CheckBoxShortList' => '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]"></span>'
            );
            //Need To Check For Filters..
            $filteredCandidate = $this->input->post('filterCandidates');
            $group_by ='APDJOB.appAppliedJobID';

            //If There Are Custom Filters. We Should Know.
            $jobsFilter = $this->input->post('filterApplicantJob');
            $applicantStatusFilter = $this->input->post('filterApplicantStatus');
            $applicantSkillFilter = $this->input->post('selectSkill');
            $applicantExpFilter = $this->input->post('selectExp');
            if(isset($applicantSkillFilter) && !empty($applicantSkillFilter)){
                $where .=' AND APSkill.SkillID in('.$applicantSkillFilter.')';

            }
            if(isset($applicantExpFilter) && !empty($applicantExpFilter)) {

                $where .=' AND APSkill.skill_level ='.$applicantExpFilter;

            }

            if(isset($jobsFilter) && !empty($jobsFilter) && is_numeric($jobsFilter)){
                //If We Reached Up To This Point Means We Need A Job Filter.
//                $where['APDJOB.jobAdvertisementID'] = intval($jobsFilter);
                $where .= ' AND APDJOB.jobAdvertisementID ='.intval($jobsFilter);
                // But We Also Need to Check For Another Filter Which Is the depending on This Current Filter.
                if(isset($applicantStatusFilter) && !empty($applicantStatusFilter) && is_numeric($applicantStatusFilter)){
                    //Now Means Both Filters Have Been Selected So Need To Query For Bother Filters.
                    //$where['APDJOB.responseStatus'] = intval($applicantStatusFilter);
                    $where .=' AND APDJOB.responseStatus ='.intval($applicantStatusFilter);
                    $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
                    /// my code
                    if(isset($listResult) && !empty($listResult)){
                        $jsonDecoded = json_decode($listResult,true);
                        foreach($jsonDecoded['aaData'] as $key=>$val){
                            if(intval($val['ApplicantType']) === 2 && !empty($val['Called'])){
                                $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" id="checkbox_select" disabled="disabled" title="Aleardy Called !" class="checkbox_select" value="'.$val['applicantID'].'"></span>';

                            }
                            elseif(intval($val['ApplicantType']) === 2){
                                $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" id="checkbox_select" class="checkbox_select" value="'.$val['applicantID'].'"><input type="hidden" name="AdvertiseID[]" id="AdvertisementID" class="AdvertisementID" value="'.$val['AdvertisementID'].'"></span>';

                            }else{
                                $jsonDecoded['aaData'][$key]['CheckBoxShortList'] = '<span style="cursor:pointer;cursor:hand;" class="CheckBoxSht"><input type="checkbox" name="AppID[]" disabled="disabled"></span>';
                            }
                        }
                        echo json_encode($jsonDecoded);
                        return;
                    }
                    // End
                    echo $listResult;
                    return;
                }else{
                    $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
                }
            }else
            {
                $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
            }



            if(isset($listResult) && !empty($listResult)){
                $decodedJSON = json_decode($listResult,true);
                $newResult = array_walk_recursive($decodedJSON, function (&$item, $key) {
                    if ($key == 'attachment') {
                        if (file_exists(FCPATH . "upload/ApplicantResumes/" . $item) && !empty($item)) {
                            $fileURL = base_url() . 'upload/ApplicantResumes/' . $item;
                            $item = '<a href="' . $fileURL . '" class="AdvertViewDetails"><span class="fa fa-download"></span></a>';
                        } else {
                            $item = '';
                        }
                    }
                });
                echo json_encode($decodedJSON);
                return;
            }
            echo $listResult;
            return;
        }
    }


    // Function for send email for interview call to shortlisted applicants
    //Function for Send pdf file of Time Sheet in email
    function SendInterviewCall(){
        if($this->input->is_ajax_request()){
            $phone=$this->input->post('phone');
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            $mailTo = $this->input->post('ToEmail');

            $mailFrom = $this->input->post('FromEmail');
            $messageSubject = $this->input->post('MessageSubject');
            $messageBody = $this->input->post('MessageBody');
            $ApplicantID = $this->input->post('ApplicantID');
            $InterviewDate = $this->input->post('InterviewDate');
            $AdvertID = $this->input->post('AdvertID');

            if(isset($InterviewDate) && !empty($InterviewDate))
            {
                $InterviewDate = date('Y-m-d',strtotime($InterviewDate));
            }

            if(!isset($InterviewDate) || empty($InterviewDate))
            {
                echo "FAIL::You Must Provide To Date::error";
                return;
            }
            if(isset($phone) && empty($phone)){
                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }

                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }}


            //As We Have All The Requirements From the Post We Will Email To the User..
            //Need To do Little Configurations for SMTP..

            $config = $this->data['mailConfiguration'];

            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            //Now Lets Send The Email..

            $this->email->to($mailTo);

            if(is_admin($this->data['EmployeeID'])){
                $this->email->from($mailFrom,'Phrism Administrator');
            }else{
                $this->email->from($mailFrom,'Employee At Phrism');
            }
            $this->email->subject($messageSubject);
            $this->email->message($messageBody);
            if(isset($phone) && !empty($phone)){
                $ApplicantsIDS=explode(',',$ApplicantID);
                foreach($ApplicantsIDS as $id){
                    $data_insert=array('applicantID'=>$id,
                        'call_date'=>date("Y-m-d"),
                        'InterviewDate'=>$InterviewDate,
                        'AdvertisementID'=>$AdvertID
                    );
                    $query=$this->common_model->insert_record('shortlisted_interview',$data_insert);
                }
                if($query){
                    echo 'OK::Interview Call Successfully ::success';
                    return;}
                else {
                    //$this->email->print_debugger();

                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
            }else{
                if($this->email->send()) {
                    $ApplicantsIDS=explode(',',$ApplicantID);
                    foreach($ApplicantsIDS as $id){
                        $data_insert=array('applicantID'=>$id,
                            'call_date'=>date("Y-m-d"),
                            'InterviewDate'=>$InterviewDate,
                            'AdvertisementID'=>$AdvertID
                        );
                        $this->common_model->insert_record('shortlisted_interview',$data_insert);
                    }
                    echo 'OK::Interview Call Successfully Emailed::success';
                    return;
                } else {
                    //$this->email->print_debugger();

                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }}


        }
    }
    // END
    //End

    /**
     * Recruitment Module
     */
    function job_advertisement()
    {
        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/job_advertisement_form');
        $this->load->view('includes/footer');
    }

    function loadDepartment()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $tbl = "ml_department MLDP";
                $data = array('MLDP.department_id AS DepartmentID,MLDP.department_name AS DepartmentName', false);
                $where = array(
                    'MLDP.trashed' => 0,
                    'MLDP.enabled' => 1
                );
                $group_by = 'MLDP.department_id';
                if ($this->input->post('term')) {
                    $searchValue = $this->input->post('term');
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl, $data, $where, FALSE, $field, $searchValue, $group_by);
                } else {
                    $result = $this->common_model->select_fields_where($tbl, $data, $where, FALSE, '', '', '', $group_by);
                }
                print_r(json_encode($result));
            }
        }
    }

    // Function for Job Ad Type Filter //

    function JobAdCloseOpen()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $tbl = "ml_jobad_type MLJAdT";
                $data = array('MLJAdT.JobAD_ID AS jobAdID,MLJAdT.Title AS TITLE', false);
                $where = array(
                    'MLJAdT.Trashed' => 0
                );
                $group_by = 'MLJAdT.JobAD_ID';
                if ($this->input->post('term')) {
                    $searchValue = $this->input->post('term');
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl, $data, $where, FALSE, $field, $searchValue, $group_by);
                } else {
                    $result = $this->common_model->select_fields_where($tbl, $data, $where, FALSE, '', '', '', $group_by);
                }
                print_r(json_encode($result));
            }
        }
    }

    // END

    // Function for Filter Month
    function loadMonth()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $tbl = "ml_month MLMNT";
                $data = array('MLMNT.ml_month_id AS MonthID,MLMNT.month AS MonthName', false);
                $where = array(
                    'MLMNT.trashed' => 0,
                    'MLMNT.enabled' => 1
                );
                $group_by = 'MLMNT.ml_month_id';
                if ($this->input->post('term')) {
                    $searchValue = $this->input->post('term');
                    $field = 'MLMNT.ml_month_id';
                    $result = $this->common_model->select_fields_where_like($tbl, $data, $where, FALSE, $field, $searchValue, $group_by);
                } else {
                    $result = $this->common_model->select_fields_where($tbl, $data, $where, FALSE, '', '', '', $group_by);
                }
                print_r(json_encode($result));
            }
        }
    }

    // Function for Filter Year
    function loadYear()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $tbl = "ml_year MLYR";
                $data = array('MLYR.ml_year_id AS YearID,MLYR.year AS YearName', false);
                $where = array(
                    'MLYR.trashed' => 0,
                    'MLYR.enabled' => 1
                );
                $group_by = 'MLYR.ml_year_id';
                $order_by = 'MLYR.ml_year_id DESC';
                if ($this->input->post('term')) {
                    $searchValue = $this->input->post('term');
                    $field = 'MLYR.ml_year_id';
                    $result = $this->common_model->select_fields_where_like($tbl, $data, $where, FALSE, $field, $searchValue, $group_by,$order_by);
                } else {
                    $result = $this->common_model->select_fields_where($tbl, $data, $where, FALSE, '', '', '', $group_by,$order_by);
                }
                print_r(json_encode($result));
            }
        }
    }
    function advertise_submit_form($list = NULL)
    {
        if($list === 'list'){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'job_advertisement JADT';
            $selectData = array('
            JADT.advertisementID AS advertisementID,
            JADT.title AS Title,
            MDPT.department_name AS DepTitle,
            JADT.availablePositions AS availPositions,
            JADT.description AS Description,
            JADT.JobAD_ID AS close,
            JADT.minQualification AS minQualification,
            CONCAT(JADT.minAgeRange,"-",JADT.maxAgeRange) AS AgeRange,
            CONCAT(JADT.salaryStartRange,"-",JADT.salaryEndRange) AS salaryRange,
            DATE_FORMAT(JADT.datePosted,"%d-%M-%Y") AS datePosted,
            DATE_FORMAT(JADT.expiryDate,"%d-%M-%Y") AS expiryDate,
            (select COUNT(applicantID) from applicant_applied_jobs where jobAdvertisementID = JADT.advertisementID) AS applieds,
            (select group_concat(applicantID) from applicant_applied_jobs where jobAdvertisementID = JADT.advertisementID) AS appliedIds,JADT.referenceNo AS ReferenceNo',false);
            $joins = array(
                array(
                    'table' => 'ml_department MDPT',
                    'condition' => 'JADT.department = MDPT.department_id AND JADT.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'JADT.trashed' => 0
            );

            $add_column = array(
                'ViewEditActionButtons' => '<a style="cursor:pointer;cursor:hand;" class="AdvertEdit" id="EditM"><span class="fa fa-pencil"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp; <a style="cursor:pointer;cursor:hand;" class="AdvertViewDetails" id="AdvertViewDetail"><span class="fa fa-eye"></span></a>&nbsp; &nbsp;|&nbsp; &nbsp; <a href="human_resource/available_Advert_applicants" style="cursor:pointer;cursor:hand;" class="AdvertApplicants" id="AdvertApplicants"><span class="fa fa-list-alt"></span></a>',
                'closeView' => '<a style="cursor:pointer;cursor:hand; color:green;" class="CloseJob" id="CloseJob"><span class="fa fa-check-circle-o"></span></a>'
            );
            //Need To Check For Filters..
            $filteredDisciplinaryAction = $this->input->post('filteredselectDepartments');
            $filteredStatusAction = $this->input->post('filteredselectjobstatus');
            $filteredMonthAction = $this->input->post('filteredselectMonth');
            $filteredYearAction = $this->input->post('filteredselectYear');
            if(isset($filteredDisciplinaryAction) && !empty($filteredDisciplinaryAction)){
                $where['JADT.department'] = $filteredDisciplinaryAction;
            }
            if(isset($filteredStatusAction) && !empty($filteredStatusAction)){
                $where['JADT.JobAD_ID'] = $filteredStatusAction;
            }
            if(isset($filteredMonthAction) && !empty($filteredMonthAction)){
                $where['Month(JADT.datePosted)'] = $filteredMonthAction;
            }
            if(isset($filteredYearAction) && !empty($filteredYearAction)){
                $where['Year(JADT.datePosted)'] = $filteredYearAction;
            }
//            print_r($this->input->post());
            $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$add_column);

            if(isset($listResult) && !empty($listResult)){
                $jsonDecoded = json_decode($listResult,true);
//                var_dump($jsonDecoded);

                foreach($jsonDecoded['aaData'] as $key=>$val){
                    if(intval($val['close']) === 1){
                        $jsonDecoded['aaData'][$key]['ViewEditActionButtons'] = '<a style="cursor:pointer;cursor:hand;" class="AdvertEdit" id="EditM"><span class="fa fa-pencil"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp; <a style="cursor:pointer;cursor:hand;" class="AdvertViewDetails" id="AdvertViewDetail"><span class="fa fa-eye"></span></a>&nbsp; &nbsp;|&nbsp; &nbsp; <a href="human_resource/available_Advert_applicants/'.$val['advertisementID'].'" style="cursor:pointer;cursor:hand;" class="AdvertApplicants" id="AdvertApplicants"><span class="fa fa-users" title="Click to view Applicants"></span></a>';
                        $jsonDecoded['aaData'][$key]['closeView'] = '<a style="cursor:pointer;cursor:hand; color:green;" class="CloseJob" id="CloseJob" title="Click to Close the Job !" ><span class="fa fa-check-circle-o"></span></a>';
                    }elseif(intval($val['close']) === 2){
                        $jsonDecoded['aaData'][$key]['ViewEditActionButtons'] = '<span class="fa fa-pencil" title="Cant Edit Advertise Closed !"></span> &nbsp; &nbsp;|&nbsp; &nbsp; <a style="cursor:pointer;cursor:hand;" class="AdvertViewDetails" id="AdvertViewDetail"><span class="fa fa-eye"></span></a>&nbsp; &nbsp;|&nbsp; &nbsp; <a href="human_resource/available_Advert_applicants/'.$val['advertisementID'].'" style="cursor:pointer;cursor:hand;" class="AdvertApplicants" id="AdvertApplicants"><span class="fa fa-users" title="Click to view Applicants"></span></a>';
                        $jsonDecoded['aaData'][$key]['closeView'] = '<span class="fa fa-remove" style="cursor:not-allowed;cursor:hand; color:red;"  id="CloseJob" title="Job Closed !" ></span>';
                    }
                }
                echo json_encode($jsonDecoded);
                return;
            }


            echo $listResult;
            return;
        }
        // Code for Form submit
        if ($this->input->post()) {
            if ($this->input->is_ajax_request()) {
                //Add Report If Request Generated Through Proper Channel.
                //Getting All The Posted Values.
                $JobTitle = $this->input->post('JobTitle');
                $Description = $this->input->post('Description');
                $SalaryStartRange = $this->input->post('SalaryStartRange');
                $SalaryEndRange = $this->input->post('SalaryEndRange');
                $AgeStartLimit = $this->input->post('AgeStartLimit');
                $AgeEndLimit = $this->input->post('AgeEndLimit');
                $AvailablePositions  = $this->input->post('AvailablePositions');
                $DateOfExpiry  = $this->input->post('DateOfExpiry');
                $Department = $this->input->post('Department');
                $MinimumQualification = $this->input->post('MinimumQualification');
                $reference_no = $this->input->post('refrence_no');

                //////////////////////////
                $loggedInEmployee = $this->data['EmployeeID'];

                ///Checking If Values POSTED OK.
                if (!isset($JobTitle) || empty($JobTitle)) {
                    echo "FAIL::Please Enter Job Title::error";
                    return;
                }
                if (!isset($Department) || empty($Department) || !is_numeric($Department)) {
                    echo "FAIL::Please Select Project From The Dropdown::error";
                    return;
                }
                if (!isset($DateOfExpiry) || empty($DateOfExpiry)) {
                    echo "FAIL::Please Enter The Expiry Date.::error";
                    return;
                }
                else {
                    $dateExploded = explode('-', $DateOfExpiry);
                    $dateOfExp = $dateExploded[2] . '-' . $dateExploded[1] . '-' . $dateExploded[0];
                }

                //Need The Employment ID..
                $employmentID = get_employment_from_employeeID($loggedInEmployee);

                //Gonna Do The Insertions Now.
                $table = 'job_advertisement';
                $insertData = array(
                    //'employmentID' => $employmentID,
                    'title' => $JobTitle,
                    'description' => $Description,
                    'datePosted' => $this->data['dbCurrentDate'],
                    'expiryDate' => $dateOfExp,
                    'postedBy' => $employmentID,
                    'availablePositions' => $AvailablePositions,
                    'department' => $Department,
                    'minAgeRange' => $AgeStartLimit,
                    'maxAgeRange' => $AgeEndLimit,
                    'salaryStartRange' => $SalaryStartRange,
                    'salaryEndRange' => $SalaryEndRange,
                    'minQualification' => $MinimumQualification,
                    'referenceNo' => $reference_no,
                    'JobAD_ID' => 1

                );
//                    var_dump($insertData);
                $result = $this->common_model->insert_record($table, $insertData);
                if ($result > 0) {
                    echo "OK::Record Successfully Added To The System::success";
                    return;
                }
            }
        }
        // End form code
    }
    // Function for Close Job Advertisement
    public function advertise_close()
    {
        $id=$this->input->post("ID");
        $table='job_advertisement';
        $where=array('advertisementID'=>$id);
        $data=array('JobAD_ID'=>2);
        $query=$this->common_model->update($table,$where,$data);
        if($query)
        {
            echo "OK:: Job Advertisement Closed Successfully !::success";
            return;
        }
        else
        {
            echo "FAIL::Failed Job Advertisement Closed !::error";
            return;
        }
    }
    // End
    // Function for Close Job advertise reference no
    public function advertise_reference_no()
    {
        $tbl='job_advertisement';
        $data='referenceNo';
        $order_by='advertisementID desc';
        $query=$this->common_model->get_row_order_desc($tbl, $data, $single = TRUE,$order_by);
        echo $query->referenceNo; return;
    }
    // End
// Function for edit job advertise
    function edit_advert_details($modalType = NULL){

        if($this->input->is_ajax_request()){

            if($this->input->post()){

                //if Came Through Proper Channel Then Lets Get The Data..
                $AdvertID = $this->input->post('AdvertID');
                if(!isset($AdvertID) || empty($AdvertID)){
                    echo "FAIL::Something Went Wrong with the POST, Please Contact System Administrator For Futher Assistance::error";
                    return;
                }

                if($modalType === 'edit'){
                    $table = 'job_advertisement JADT';
                    $AdvertID = $this->input->post('AdvertID');
                    $selectData = array('
            JADT.advertisementID AS advertisementID,
            JADT.title AS Title,
            MDPT.department_name AS DepTitle,
            MDPT.department_id AS department_id,
            JADT.availablePositions AS availPositions,
            JADT.description AS Description,
            JADT.minQualification AS minQualification,
            JADT.minAgeRange AS minAgeRange,
            JADT.maxAgeRange AS maxAgeRange,
            JADT.salaryStartRange AS salaryStartRange,
            JADT.salaryEndRange AS salaryEndRange,
            DATE_FORMAT(JADT.datePosted,"%d-%m-%Y") AS datePosted,
            DATE_FORMAT(JADT.expiryDate,"%d-%m-%Y") AS expiryDate',false);
                    $joins = array(
                        array(
                            'table' => 'ml_department MDPT',
                            'condition' => 'JADT.department = MDPT.department_id AND JADT.trashed = 0',
                            'type' => 'LEFT'
                        )
                    );
                    $where = array(
                        'JADT.advertisementID' => $AdvertID
                    );
                    $reportDetails = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,TRUE);

                    print_r(json_encode($reportDetails));
                    return;
                }
            }
        }
    }

    // FUNCTION FOR UPDATING RECORD
    function advertise_update_record()
    {
        // Code for Form submit
        if ($this->input->post()) {
            if ($this->input->is_ajax_request()) {
                //Add Report If Request Generated Through Proper Channel.
                //Getting All The Posted Values.
                $JobTitle = $this->input->post('EJobTitle');
                $Description = $this->input->post('EDescription');
                $SalaryStartRange = $this->input->post('ESalaryStartRange');
                $SalaryEndRange = $this->input->post('ESalaryEndRange');
                $AgeStartLimit = $this->input->post('EAgeStartLimit');
                $AgeEndLimit = $this->input->post('EAgeEndLimit');
                $AvailablePositions  = $this->input->post('EAvailablePositions');
                $DateOfExpiry  = $this->input->post('EDateOfExpiry');
                $Department = $this->input->post('EDepartment');
                $MinimumQualification = $this->input->post('EMinimumQualification');
                $AdvertiseID = $this->input->post('EAdvertiseID');
                //////////////////////////
                $loggedInEmployee = $this->data['EmployeeID'];

                //////// Set Date Format ////////
                if(isset($DateOfExpiry) && !empty($DateOfExpiry))
                {
                    $DateOfExpiry = date('Y-m-d',strtotime($DateOfExpiry));
                }

                /////
                ///Checking If Values POSTED OK.
                if (!isset($JobTitle) || empty($JobTitle)) {
                    echo "FAIL::Please Enter Job Title::error";
                    return;
                }
                if (!isset($Department) || empty($Department) || !is_numeric($Department)) {
                    echo "FAIL::Please Select Project From The Dropdown::error";
                    return;
                }
                if (!isset($DateOfExpiry) || empty($DateOfExpiry)) {
                    echo "FAIL::Please Enter The Expiry Date.::error";
                    return;
                }
                /* else {
                     $dateExploded = explode('-', $DateOfExpiry);
                     $dateOfExp = $dateExploded[2] . '-' . $dateExploded[1] . '-' . $dateExploded[0];
                 }
                 echo $dateOfExp; die;*/
                //Need The Employment ID..
                $employmentID = get_employment_from_employeeID($loggedInEmployee);

                //Gonna Do The Insertions Now.
                $table = 'job_advertisement';
                $where=array('advertisementID'=>$AdvertiseID);
                $insertData = array(
                    //'employmentID' => $employmentID,
                    'title' => $JobTitle,
                    'description' => $Description,
                    'dateupdated' => $this->data['dbCurrentDate'],
                    'expiryDate' => $DateOfExpiry,
                    'postedBy' => $employmentID,
                    'availablePositions' => $AvailablePositions,
                    'department' => $Department,
                    'minAgeRange' => $AgeStartLimit,
                    'maxAgeRange' => $AgeEndLimit,
                    'salaryStartRange' => $SalaryStartRange,
                    'salaryEndRange' => $SalaryEndRange,
                    'minQualification' => $MinimumQualification,

                );
//                    var_dump($insertData);
                $result = $this->common_model->update($table,$where,$insertData);
                if ($result > 0) {
                    echo "OK::Record Successfully Update To The System::success";
                    return;
                }
            }
        }
        // End form code
    }
    // END
    public function applicantDetails($applicantID = NULL)
    {
        $data['applicant_id'] = $this->uri->segment(3);
        $where =array('applicants.applicantID'=>$applicantID);
        $data['Applicant_info'] = $this->hr_model->get_row_by_where('applicants',$where);
        $data['qualificationInfo'] = $this->hr_model->qualificationApplicant($where);
        //echo "<pre>"; print_r($data['qualificationInfo']);die;
        $data['skills'] = $this->hr_model->skiilApplicant($where);
        $data['appliedJob'] = $this->hr_model->appliedforJob($where);
        $data['AppExperience'] = $this->hr_model->ApplicantExperience($where);
        //echo "<pre>"; print_r($data['AppExperience']);die;
        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/ApplicantDetail',$data);
        $this->load->view('includes/footer');

    }

    // Function for Shortlisted applicant....
    public function shortListApplicant()
    {
        if ($this->input->post())
        {
            if ($this->input->is_ajax_request()){
                $applicantAppliedJobID= $this->input->post('ID');
                //Need Job AdvertisementID
                $applicantAppliedJobTable = 'applicant_applied_jobs';
                $selectData = array('jobAdvertisementID AS JobAdID',false);
                $where = array(
                    'appAppliedJobID' => $applicantAppliedJobID
                );
                $resultFirst = $this->common_model->select_fields_where($applicantAppliedJobTable,$selectData,$where,TRUE);
                if(!isset($resultFirst) || empty($resultFirst->JobAdID)){
                    echo "FAIL::No Record Found, Please Contact System Administrator For Further Assistance::error";
                    return;
                }


                /////Table Job Advertisement
                $selectTable = 'job_advertisement';
                //Before Insertion We First Need To Check If Record Already Exist Against Same Type.
                $selectData = array('COUNT(1) AS Totaljobs');
                $where = array(
                    'JobAD_ID' => 2,
                    'trashed' => 0,
                    'advertisementID' => $resultFirst->JobAdID
                );
                $selectResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
                //print_r($selectResult);die;

                if(isset($selectResult) && $selectResult->Totaljobs > 0){
                    echo "FAIL::Sorry You Are not Allowed to Apply Job Has Been Closed::error";
                    return;
                }



                $table = 'applicant_applied_jobs';
                $where=array('appAppliedJobID'=> $applicantAppliedJobID);
                $insertData = array(
                    'responseStatus' => 2
                );

                $result = $this->common_model->update($table,$where,$insertData);
                if ($result > 0) {
                    echo "OK::Applicant Successfully Shortlisted::success";
                    return;
                }else{
                    echo "FAIL::Some Error Occur in Shortlisting Please Contact To System Administrator::Error";
                    return;
                }

            }
        }

    }


    // Fetch multiple applicant email //

    public function ShorlistedApplicantEmail()
    {
        if ($this->input->post()) {
            if ($this->input->is_ajax_request()) {
                $ids = $this->input->post('ids');
                $ApplicantIDs = implode(",", $ids);
                $result = $this->hr_model->fetch_ShortListApplicants_email($ApplicantIDs);
                //print_r($result); die;
                echo json_encode($result);
                return;
            }

        }
    }
    // Function For Interview List //
    Public function InterviewList()
    {
        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/Interview_Candidate');
        $this->load->view('includes/footer');
    }

//// Function for Applicant Interview ////
    function load_ApplicantForInterview( $list = NULL){
        if($list === 'list'){
            //If Requested For Data Tables List, Then Lets See if Its a Proper
            $table = 'applicants AS Appl';
            $selectData = array('
            Appl.applicantID AS applicantID,
            Appl.fullName AS Name,
            JBADT.title AS Job_Title,
            Appl.officialEmailAddress AS Email,
             (CASE WHEN(SHI.present = 1) THEN "YES" WHEN (SHI.present = 0) THEN "NO" ELSE 0 END)  AS Present,
            SHI.feedback AS FeedBack,
           MLIS.InterviewStatus AS Interview_Status,
            (Select full_name from employee where employee_id = SHI.feedback_by) AS FeedBack_By,
            DATE_FORMAT(SHI.feedback_date,"%d-%M-%Y") AS FeedBackDate,
            SHI.call_date AS CallDate,
            SHI.shtintr_id AS SHID
            ',false);
            $joins = array(
                array(
                    'table' => 'shortlisted_interview SHI',
                    'condition' => 'SHI.applicantID = Appl.applicantID',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_interviewstatus MLIS',
                    'condition' => 'MLIS.InterviewStatusID = SHI.status',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'job_advertisement JBADT',
                    'condition' => 'SHI.advertisementID = JBADT.advertisementID',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'SHI.trashed' => 0
            );
            $add_column = array(
                'ViewApplicantDetails' => '<a style="cursor:pointer;cursor:hand;" title="Edit" class="AdvertViewDetails"><span class="fa fa-pencil"></span></a>'
            );
            //Need To Check For Filters..
            $filteredCandidate = $this->input->post('filterCandidates');
            $group_by = 'Appl.applicantID';

            //If There Are Custom Filters. We Should Know.
            $jobsFilter = $this->input->post('selectStatus');
            $applicantStatusFilter = $this->input->post('filterApplicantStatus');
            $applicantScoreFilter = $this->input->post('score');
            if(isset($applicantScoreFilter) && !empty($applicantScoreFilter)){
                //If We Reached Up To This Point Means We Need A Job Filter.
                $where['SHI.Score'] = $applicantScoreFilter;
            }

            if(isset($jobsFilter) && !empty($jobsFilter) && is_numeric($jobsFilter)){
                //If We Reached Up To This Point Means We Need A Job Filter.
                $where['SHI.status'] = intval($jobsFilter);
                // But We Also Need to Check For Another Filter Which Is the depending on This Current Filter.

                $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
                echo $listResult;
                return;
            }else{
                $listResult = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column);
            }

//echo $this->db->last_query();
            echo $listResult;
            return;
        }
    }

    // Function for Applicant Interview Details //
    public function ApplicantInterviewDetails($InterviewID = NULL)
    {
        $data['InterviewID'] = $this->uri->segment(3);
        $where =array('shortlisted_interview.shtintr_id'=>$InterviewID);
        $dataApp= $this->hr_model->get_row_by_where('shortlisted_interview',$where);
        $ApplicantID = $dataApp->applicantID;
        $data['Present'] = $dataApp->present;
        $data['score'] = $dataApp->Score;
        $data['feedback'] = $dataApp->feedback;
        $data['stat'] = $dataApp->status;
        $where2 = array('applicants.applicantID'=>$ApplicantID);
        $data['Applicant_info'] = $this->hr_model->get_row_by_where('applicants',$where2);
        $data['status'] = $this->site_model->html_selectbox('ml_interviewstatus', '-- Selection --', 'InterviewStatusID', 'InterviewStatus');
        $data['qualificationInfo'] = $this->hr_model->qualificationApplicant($where2);
        $data['skills'] = $this->hr_model->skiilApplicant($where2);
        $data['appliedJob'] = $this->hr_model->appliedforJob($where2);
        $data['AppExperience'] = $this->hr_model->ApplicantExperience($where2);

        $this->load->view('includes/header');
        $this->load->view('recruitment/protected/ApplicantInterviewDetail',$data);
        $this->load->view('includes/footer');

    }

    public function ApplicantFeedbackInterview()
    {
        if($this->input->post('update'))
        {

            $present = $this->input->post('present');
            if(!isset($present) || empty($present))
            {
                echo "FAIL::Please Select Attendance Status Type::error";
                return;
            }
            $addInsert= array(
                'feedback' => $this->input->post('feedback'),
                'present'  => $present,
                'Score'  => $this->input->post('Score'),
                'feedback_by'  => $this->data['EmployeeID'],
                'feedback_date'=> date("Y-m-d"),
                'status'=> $this->input->post('InterviewStatus')

            );
            //echo "<pre>"; print_r($addInsert);die;
            $id=$this->input->post('id');
            $where=array('shtintr_id'=>$id);
            $query = $this->hr_model->update_record('shortlisted_interview',$where,$addInsert);
            if($query)
            {
                $selected=$this->input->post('InterviewStatus');
                if($selected == 1){
                    $updateRec=array('selected_approval'=>1);
                    $query = $this->hr_model->update_record('shortlisted_interview',$where,$updateRec);
                }
                redirect('human_resource/InterviewList');
                return;
            }

        }
        if($this->input->post('add'))
        {

            $present = $this->input->post('present');
            if(!isset($present) || empty($present))
            {
                echo "FAIL::Please Select Attendance Status Type::error";
                return;
            }

            $addInsert= array(
                'feedback' => $this->input->post('feedback'),
                'present'  => $present,
                'Score'  => $this->input->post('Score'),
                'feedback_by'  => $this->data['EmployeeID'],
                'feedback_date'=> date("Y-m-d"),
                'status'=> $this->input->post('InterviewStatus')
            );
            //echo "<pre>"; print_r($addInsert);die;

            $id=$this->input->post('id');
            $where=array('shtintr_id'=>$id);
            $query = $this->hr_model->update_record('shortlisted_interview',$where,$addInsert);
            if($query)
            {
                $selected=$this->input->post('InterviewStatus');
                if($selected == 1){
                    $updateRec=array('selected_approval'=>1);
                    $query = $this->hr_model->update_record('shortlisted_interview',$where,$updateRec);
                }
                redirect('human_resource/InterviewList');
            }

        }

    }

    /// Function for Program Filter Using Ajax  ///
    function load_all_available_Program()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_program_list';
                $data = array(
                    'Program AS ProgramName, ProgramID AS PROGRAMID',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'Program';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }

    function getCurrentAddressDetails(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $empID = $this->input->post('empID');

                if(empty($empID) || !isset($empID)){
                    echo "FAIL::Something Went Wrong with The Post, Please Try Again::error";
                    return;
                }

                if(!is_numeric($empID)){
                    echo "FAIL::Employee ID Not Posted In Correct Format";
                    return;
                }

                $table = 'current_contacts CC';
                $where = array(
                    'employee_id' => $empID
                );
                $joins = array(
                    array(
                        'table' => 'ml_city MLC',
                        'condition' => 'MLC.city_id = CC.city_village',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_province MLP',
                        'condition' => 'MLP.province_id = CC.province',
                        'type' => 'LEFT'
                    ),
                    array(
                        'table' => 'ml_district MLD',
                        'condition' => 'MLD.district_id = CC.District',
                        'type' => 'LEFT'
                    )
                );
                $data = 'CC.address AS Address, MLC.city_name AS City, MLP.province_name AS Province,CC.home_phone AS Phone,MLD.district_name AS District';
                $resultData = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);
                print_r(json_encode($resultData));
            }
        }
    }

    function tax_test($employmentID,$financialYearID){
        //First Check If Employee Has His Salary and Contract Set.

        if(empty($employmentID) || !is_numeric($employmentID)){
            return "No Employment Information Provided";
        }

        if(empty($financialYearID) || !is_numeric($financialYearID)){
            return "No Financial Year Information Provided";
        }

        $ci =& get_instance();
        $ci->load->model('common_model');


        $arrayToShow = array(
            'employeeInfo' => array()
        );

        //Lets Get selected Financial Year for which we need to create records
        $financialYearTable = "ml_financial_year MLFY";
        $selectData = 'FY.year AS FromYear, TY.year AS ToYear, FM.month AS FromMonth, TM.month AS ToMonth';
        $joins = array(
            array(
                'table' => 'ml_year FY',
                'condition' => 'FY.ml_year_id = MLFY.FromYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_year TY',
                'condition' => 'TY.ml_year_id = MLFY.ToYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month FM',
                'condition' => 'FM.ml_month_id = MLFY.FromMonthID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month TM',
                'condition' => 'TM.ml_month_id = MLFY.ToMonthID',
                'type' => 'INNER'
            )
        );
        $where = array(
            'FinancialYearID' => $financialYearID
        );

        $result = $ci->common_model->select_fields_where_like_join($financialYearTable,$selectData,$joins,$where,TRUE);
        if(empty($result)){
            return "Could not find provided financial year in the system";
        }else{
            $FromYear = $result->FromYear;
            $ToYear = $result->ToYear;
            $FromMonth = $result->FromMonth;
            $ToMonth = $result->ToMonth;

            echo $FromYear." - ".$ToYear;
        }

        //Lets get all the months of a selected Financial Year.
        $fromDate = date("Y-m-d",strtotime("01-".$FromMonth."-".$FromYear));
        $toDate = date("Y-m-d",strtotime("30-".$ToMonth."-".$ToYear));
        $monthsArray =  $this->site_model->get_all_months_in_between($fromDate,$toDate);

        if(empty($monthsArray)){
            return "Something Went Wrong while fetching Months.";
        }

        //Checking Up Contract
        $contractTable = "contract C";
        $contractSelectData = array(
            "
            C.contract_start_date AS ContractStartDate,
            C.contract_expiry_date AS ContractExpiryDate,
            ECE.e_start_date AS ExtensionStartDate,
            ECE.e_end_date AS ExtensionExpiryDate
            ",
            false
        );
        $contractJoins = array(
            array(
                'table' => 'employee_contract_extensions ECE',
                'condition' => 'ECE.contract_id = C.contract_id AND ECE.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $contractWhere = "`C`.`employment_id` = '".$employmentID."' AND C.status = 2 AND C.trashed = 0 AND C.current = 1
         AND (((C.contract_start_date <= '".$fromDate."' AND C.contract_expiry_date > '".$fromDate."')
         OR
         (C.contract_start_date < '".$toDate."' AND C.contract_expiry_date > '".$fromDate."'))
         OR((ECE.e_start_date <= '".$fromDate."' AND ECE.e_end_date > '". $fromDate."') OR
         (ECE.e_start_date < '".$toDate."' AND ECE.e_end_date > '". $fromDate."')))";

        $contractInfo = $ci->common_model->select_fields_where_like_join($contractTable,$contractSelectData,$contractJoins,$contractWhere);
        echo "<pre> contract Info:<br />";

        $contractObjectArray = $contractInfo;
        $contractInfo = array();

        //Will Move it later to Else Section
        foreach($contractObjectArray as $key=>$object){
            $contractStartDate = $object->ContractStartDate;
            $contractExpiryDate = $object->ContractExpiryDate;
            $extensionStartDate = $object->ExtensionStartDate;
            $extensionExpiryDate = $object->ExtensionExpiryDate;

            //Need Keys To Differentiate Between Contracts and their extensions.
            $objectContractKey = $contractStartDate."::".$contractExpiryDate;
            $objectExtensionKey = $extensionStartDate."::".$extensionExpiryDate;

            if(!array_key_exists($objectContractKey,$contractInfo)){
                $objectContract = new stdClass();
                $objectContract->ContractStartDate = $contractStartDate;
                $objectContract->ContractExpiryDate = $contractExpiryDate;
                $contractInfo[$objectContractKey] = $objectContract;
            }

            if(!array_key_exists($objectExtensionKey,$contractInfo)){
                $objectContract = new stdClass();
                $objectContract->ContractStartDate = $extensionStartDate;
                $objectContract->ContractExpiryDate = $extensionExpiryDate;
                $contractInfo[$objectExtensionKey] = $objectContract;
            }

        }

//        print_r($contractInfo);

        if(empty($contractInfo)){
            return "Missing Contract Information, Please Set Current Employee's Employment Contract";
        }else{
            /////Main Problem Starts Here.
            $totalContracts = count($contractInfo);

            //Getting all Months from First Date Range
            //DateRange of FinancialYear
            $f_start = (new DateTime($fromDate))->modify('first day of this month');

            $f_end = (new DateTime($toDate))->modify('first day of next month');

            $f_interval = DateInterval::createFromDateString('1 month');

            $firstPeriod = new DatePeriod($f_start, $f_interval, $f_end);


            $collidingMonths = array();
            foreach($contractInfo as $contract){
                //Getting all Months from Second Date Range.
                $start = (new DateTime($contract->ContractStartDate))->modify('first day of this month');

                $end = (new DateTime($contract->ContractExpiryDate))->modify('first day of next month');

                $interval = DateInterval::createFromDateString('1 month');

                $secondPeriod = new DatePeriod($start, $interval, $end);
                if(!array_key_exists($contract->ContractStartDate."::".$contract->ContractExpiryDate,$collidingMonths)){
                    //Initialize and empty array.
                    $collidingMonths[$contract->ContractStartDate."::".$contract->ContractExpiryDate] = array(
                            'TotalMonths' => 0
                    );
                }

                //Now Start Pushing Colliding MonthsYears.
                $first = array();
                $second = array();
                foreach ($firstPeriod as $fKey => $f_dt) {
                    $first[$fKey] = $f_dt->format("Y-m");
                    foreach($secondPeriod as $sKey => $s_dt){
                        if(!array_key_exists($sKey,$second)){
                            $second[$sKey] = $s_dt->format("Y-m");
                        }
/*                        echo $f_dt->format("Y-m")."<br />";
                        echo $s_dt->format("Y-m")."<br /><br /><br />";*/
                        if($f_dt->format("Y-m") ===  $s_dt->format("Y-m")){
                            array_push($collidingMonths[$contract->ContractStartDate."::".$contract->ContractExpiryDate],$s_dt->format("Y-m-d"));
                            $collidingMonths[$contract->ContractStartDate."::".$contract->ContractExpiryDate]['TotalMonths']++;
                        }
                    }
                }

/*                echo "Financial Dates";
                print_r($first);

                echo "Contract Dates";
                print_r($second);

                echo "<br />=-=--=-=-=-=<br />";*/
            }


            echo "<pre>";
            echo $fromDate." - ".$toDate."<br />";


            $outPutMonths = [];
            $TotalMonths = 0;
            foreach($collidingMonths as $innerArray) {
                $TotalMonths += $innerArray['TotalMonths'];
                unset($innerArray['TotalMonths']);
                $outPutMonths = array_merge($outPutMonths, $innerArray);
            }
            $outPutMonths['TotalMonths'] = $TotalMonths;
            unset($collidingMonths);
            $collidingMonths[0] = $outPutMonths;
        }




        //Checking Up Salary
        $salaryTable = "salary S";
        $salarySelectData = "*";
        $salaryWhere = array(
            'S.employement_id' => $employmentID,
            'S.status' => 2,
            'S.trashed' => 0
        );

        $salaryInfo = $ci->common_model->select_fields_where($salaryTable,$salarySelectData,$salaryWhere,true);

        if(empty($salaryInfo)){
            return "Missing Salary, Please Set Current Employee's Employment Salary";
        }else {
            $employeeSalary = $salaryInfo->base_salary;
            $employeeSalaryDateApproved = $salaryInfo->date_approved;
            $employeeSalaryDateModified = $salaryInfo->date_modified;
        }

        //Checking Up Increments
        $incrementsTable = "increments INC";
        $incrementsSelectData = array("increment_id AS ID, date_effective AS DateEffective,increment_amount AS Amount ",false);

        $whereIncrements = "INC.employment_id =".$employmentID." AND INC.trashed=0 AND INC.status=2 AND INC.date_effective < '".$toDate."'";

        $increments = $ci->common_model->select_fields_where($incrementsTable,$incrementsSelectData,$whereIncrements);


        ///LETS see what do we have so far for our logic to implement
        ///Salary Info - YES (Salary is Unique all the Way)
        ///Increments - YES
        //Contracts - Yes
        //List of MonthsYears in Financial Year - Yes,
        //Allowances - YES
        //Deductions - NO
        //TaxSlabs - No


        //Picking Up Allowances
        $allowancesTable = "allowance A";
        $allowancesSelectData = array("allowance_id AS ID, allowance_amount AS Amount, effective_date AS DateEffective",false);
        $whereAllowances = array(
          'A.status' => 2,
            'A.trashed' => 0,
            'A.employment_id' => $employmentID,
            'A.effective_date <' => $toDate
        );

        $allowances = $ci->common_model->select_fields_where($allowancesTable,$allowancesSelectData,$whereAllowances);

        echo "<pre>";
//        echo $this->db->last_query();


        //Getting Tax Slabs.
        $TaxSlabsTable = "ml_tax_year TY";
        $TaxSlabsSelectData = "*";
        $TaxSlabsJoins = array(
            array(
                'table' => 'ml_tax_slab TS',
                'condition' => 'TS.TaxYearID = TY.TaxYearID AND TS.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_financial_year MLFY',
                'condition' => 'MLFY.FinancialYearID = TY.FinancialYearID',
                'type' => 'INNER'
            )
        );
        $whereTaxSlab = array(
            'MLFY.FinancialYearID' => $financialYearID
        );

        $taxSlabsData = $ci->common_model->select_fields_where_like_join($TaxSlabsTable,$TaxSlabsSelectData,$TaxSlabsJoins,$whereTaxSlab);

        print_r($taxSlabsData);
        //Picking Up Deductions
        //Deduction also have types. That We Should Care about.
        //1. Monthly Deduction
        //2. Yearly/Annual Deduction
        //3. One-Time Deduction
        //Also Need to care that if to duduct amount after applying of tax or before applying tax.
        $deductionsTable = "deduction D";

        //AllowDeductionBeforeTax:
        //If 1(Means Do Deductions Before Applying Taxes) If 0(Do Apply Taxes Before Applying Deductions)
        $deductionsSelectData = array(
            "deduction_id AS ID,
            deduction_amount AS Amount,
            MLDT.AllowDeductionBeforeTax,
            date_approved AS DateApproved,
            deduction_frequency AS FrequencyID"
        ,false
        );
        $joins = array(
            array(
                'table' => 'ml_deduction_type MLDT',
                'condition' => 'MLDT.ml_deduction_type_id = D.ml_deduction_type_id',
                'type' => 'INNER'
            )
        );
        $where = array(
            'D.employment_id' => $employmentID,
            'D.status' => 2,
            'D.trashed' => 0,
            'D.date_approved <' => $toDate
        );

        $deductions = $ci->common_model->select_fields_where_like_join($deductionsTable,$deductionsSelectData,$joins,$where);

        //Now if we have all the above mentioned Info, I guess we are in good shape of setting up our logic.
        echo "<pre>";
/*        echo $this->db->last_query();
        print_r($deductions);*/
        $tempArray = array();
        $Salary = intval($employeeSalary);
        $taxableMonthlySum = 0;
        $taxPayableMonthlySum = 0;

        print_r($monthsArray);

        $change= array(
            'index' => 0,
            'totalAppliedIncrements' => 0,
            'incrementInCurrentFinancialYear' => 0,
            'totalAppliedAllowances' => 0,
            'allowanceInCurrentFinancialYear' => 0,
            'SalaryIncreasedUpTo' => 0,
            'tempSalary' => 0,
            'totalRemainingMonths' => 0
        );

        foreach($monthsArray as $monthYear){
            $Month = $monthYear['Month'];
            $Year = $monthYear['Year'];

            //Setup Base Salary For Every Month
            if(!array_key_exists($Year."-".$Month,$tempArray)){
                $tempArray[$Year."-".$Month]['MonthlySalary'] = $Salary; //It Should Include Allowances and Increments
                $tempArray[$Year."-".$Month]['Salary'] = $Salary; //This Will Also Might Include the Deductions Depending on deduction type
                $tempArray[$Year."-".$Month]['Totals']['TotalIncrementsApplied'] = 0;
                $tempArray[$Year."-".$Month]['Totals']['TotalAllowancesApplied'] = 0;
                $tempArray[$Year."-".$Month]['PayableTax'] = 0;
                $tempArray[$Year."-".$Month]['Arrears'] = 0;
            }

            //Checking Up Increments That Can Be Added To Monthly Salary.
            if(!empty($increments)){
                foreach($increments as $increment){
                    if(strtotime($increment->DateEffective) <= strtotime("01-".$Month."-".$Year)){
                        //Check If this Increments has not already been Applied
                        if(!array_key_exists('IncrementDates',$tempArray[$Year."-".$Month])){
                            $tempArray[$Year."-".$Month]['IncrementDates'] = array();
                        }
                        if(!array_key_exists($increment->DateEffective,$tempArray[$Year."-".$Month]['IncrementDates'])){
                            $tempArray[$Year."-".$Month]['IncrementDates'][$increment->DateEffective] = $increment->Amount;
                            $tempArray[$Year."-".$Month]['Totals']['TotalIncrementsApplied']++;
                            $tempArray[$Year."-".$Month]['MonthlySalary'] += intval($increment->Amount);
                            $tempArray[$Year."-".$Month]['Salary'] += intval($increment->Amount);
                        }
                    }
                }
            }

            //Checking Up Allowances That can be Added to Monthly Salary.
            if(!empty($allowances)){
                foreach($allowances as $allowance){
                    if(strtotime($allowance->DateEffective) < strtotime("01-".$Month."-".$Year)){
                        if(!array_key_exists('AllowanceDates',$tempArray[$Year."-".$Month])){
                            $tempArray[$Year."-".$Month]['AllowanceDates'] = array();
                        }

                        if(!array_key_exists($allowance->DateEffective."::".$allowance->ID,$tempArray[$Year."-".$Month]['AllowanceDates'])){
                            $tempArray[$Year."-".$Month]['AllowanceDates'][$allowance->DateEffective."::".$allowance->ID] = $allowance->Amount;
                            $tempArray[$Year."-".$Month]['Totals']['TotalAllowancesApplied']++;
                            $tempArray[$Year."-".$Month]['MonthlySalary'] += intval($allowance->Amount);
                            $tempArray[$Year."-".$Month]['Salary'] += intval($allowance->Amount);
                        }
                    }
                }
            }

            //Initialize an Empty Array, only and only if there it is not already been initialized.
            if(!array_key_exists('Deduction',$tempArray[$Year."-".$Month])){
                $tempArray[$Year."-".$Month]['Deduction'] = array();
            }

            if(!empty($deductions)){
                foreach($deductions as $deduction){
                    //Here We Also Need To Take Care of Frequency. As Frequency is a Critical Issue related to Time Occurrance.

                    $frequency = intval($deduction->FrequencyID);

                    if(date('Y-m',strtotime($deduction->DateApproved)) <= date('Y-m',strtotime("01-".$Month."-".$Year))){
                        if($frequency === 1){ //Monthly Deduction Should Happen Here.

                            if(!array_key_exists('Monthly',$tempArray[$Year."-".$Month]['Deduction'])){
                                $tempArray[$Year."-".$Month]['Deduction']['Monthly'] = array();
                            }
                            //Deduct Only If Allowed
                            if(intval($deduction->AllowDeductionBeforeTax) === 1){
                                if(!array_key_exists($deduction->ID, $tempArray[$Year."-".$Month]['Deduction']['Monthly'])){
                                    $tempArray[$Year."-".$Month]['Salary'] -= $deduction->Amount;

                                    //Now Add Info About Deduction To Array.
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['Amount'] = intval($deduction->Amount);
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['DateApproved'] = intval($deduction->DateApproved);
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['DeductBeforeTax'] = true;

                                }
                            }else {
                                if(!array_key_exists($deduction->ID, $tempArray[$Year."-".$Month]['Deduction']['Monthly'])){

                                    //Now Add Info About Deduction To Array.
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['Amount'] = intval($deduction->Amount);
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['DateApproved'] = intval($deduction->DateApproved);
                                    $tempArray[$Year."-".$Month]['Deduction']['Monthly'][$deduction->ID]['DeductBeforeTax'] = false;

                                }
                            }


                        }elseif($frequency === 2){ //Yearly/Annual Deduction.
                            if(date('m',strtotime($deduction->DateApproved) === date('m',strtotime("01-".$Month."-".$Year)))){
                                $tempArray[$Year."-".$Month]['Deduction']['Yearly'] = array();
                                //Deduct Only If Allowed
                                if(intval($deduction->AllowDeductionBeforeTax) === 1){
                                    if(!array_key_exists($deduction->ID,$tempArray[$Year."-".$Month]['Deduction']['Yearly'])){
                                        $tempArray[$Year."-".$Month]['Salary'] -= $deduction->Amount;
                                    }
                                }
                                //Add Deduction Info To Array
                                $tempArray[$Year."-".$Month]['Deduction']['Yearly'][$deduction->ID]['Amount'] = intval($deduction->Amount);
                                $tempArray[$Year."-".$Month]['Deduction']['Yearly'][$deduction->ID]['DateApproved'] = intval($deduction->DateApproved);
                            }
                        }elseif($frequency === 3){
                            if(date('Y-m',strtotime($deduction->DateApproved)) === date('Y-m',strtotime("01-".$Month."-".$Year))){
                                $tempArray[$Year."-".$Month]['Deduction']['Once'] = array();
                                //Deduct Only If Allowed
                                if(intval($deduction->AllowDeductionBeforeTax) === 1){
                                    if(!array_key_exists($deduction->ID,$tempArray[$Year."-".$Month]['Deduction']['Once'])){
                                        $tempArray[$Year."-".$Month]['Salary'] -= $deduction->Amount;
                                    }
                                }

                                //Its One Time Deduction, so if Month and Year Matches then and only than there should be deduction.
                                //Adding Deduction Info To Array.
                                $tempArray[$Year."-".$Month]['Deduction']['Once'][$deduction->ID]['Amount'] = intval($deduction->Amount);
                                $tempArray[$Year."-".$Month]['Deduction']['Once'][$deduction->ID]['DateApproved'] = intval($deduction->DateApproved);
                            }

                        }
                    }
                }
            }

            //CheckUp How Many Contracts That is in this Financial Year. Every Contract will have its own Annual Tax
                foreach($collidingMonths as $keyContractDates => $arrayContractMonths){
                    if(in_array(date("Y-m-d",strtotime("01-".$Month."-".$Year)),$arrayContractMonths)){
                        print_r($Year."-".$Month."-01"."<br />");
                        $totalMonthsOfContractInCurrentFinancialYear = intval($arrayContractMonths['TotalMonths']);
                        $tempArray[$Year."-".$Month]['AnnualSalary'] = intval($tempArray[$Year."-".$Month]['Salary'])*$totalMonthsOfContractInCurrentFinancialYear;
                        $tempArray[$Year."-".$Month]['Contract'] = $keyContractDates;

                        if($change['index'] === 0){
                            $change['totalRemainingMonths'] = $totalMonthsOfContractInCurrentFinancialYear;
                        }

                        foreach($taxSlabsData as $key=>$obj){

                            //Slab Income Information
                            $annualIncomeFrom = intval($obj->AnnualIncomeFrom);
                            $annualIncomeTo = intval($obj->AnnualIncomeTo);


                            //AnnualSalary
                            $AnnualSalary = $tempArray[$Year."-".$Month]['AnnualSalary'];

/*                            echo "slabFrom  = ".$annualIncomeFrom;
                            echo "slabTo = ".$annualIncomeTo;
                            echo "Annual = ".$AnnualSalary."<br /><br /><br /><br />";*/

                            if($AnnualSalary >=  intval($obj->AnnualIncomeFrom) && $AnnualSalary <= intval($annualIncomeTo)){

                                if(!array_key_exists('TaxInfo',$tempArray[$Year."-".$Month])){

                                    $TaxRate = $obj->TaxRate;
                                    $FixedAmount = (isset($obj->FixedAmount) && !empty($obj->FixedAmount) && is_numeric($obj->FixedAmount))?$obj->FixedAmount:0;

                                    $AnnualTaxToDeduct = ceil(((($AnnualSalary - $annualIncomeFrom)*$TaxRate)/100)+$FixedAmount);
                                    $MonthlyTaxToDeduct = ceil($AnnualTaxToDeduct/$totalMonthsOfContractInCurrentFinancialYear);

                                    $tempArray[$Year."-".$Month]['TaxInfo'] = array();
                                    $tempArray[$Year."-".$Month]['TaxInfo']['SlabNo'] = $obj->SlabNo;
                                    $tempArray[$Year."-".$Month]['TaxInfo']['FixedAmount'] = $FixedAmount;
                                    $tempArray[$Year."-".$Month]['TaxInfo']['TaxRate'] = $TaxRate;
                                    $tempArray[$Year."-".$Month]['TaxInfo']['AnnualTaxToDeduct'] = $AnnualTaxToDeduct;
                                    $tempArray[$Year."-".$Month]['TaxInfo']['MonthlyTaxToDeduct'] = $MonthlyTaxToDeduct;

                                    $tempArray[$Year."-".$Month]['Salary']  = $tempArray[$Year."-".$Month]['Salary'] - $MonthlyTaxToDeduct;

                                    //Just Now Check If There Is Any Deduction That Needs To Be Applied After Tax.
                                    if(!isset($tempArray[$Year."-".$Month]['Deduction']) && !empty($tempArray[$Year."-".$Month][['Deduction']] && is_array(['Deduction'])))
                                    foreach($tempArray[$Year."-".$Month]['Deduction'] as $key => $array){
                                        ////Still Work is Remaining Here that needs to Be Completed.
                                        foreach($array as $subKey => $subArray){
                                            if($subArray['DeductBeforeTax'] === false){
                                                ///If False then it must not have been deducted before tax. Must/Should Be Deducted Now.
                                                $tempArray[$Year."-".$Month]['Salary'] -= $subArray['Amount'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


            //Sum Up All the Taxable Salaries...
            if(isset($tempArray[$Year."-".$Month]['AnnualSalary']) && !empty($tempArray[$Year."-".$Month]['AnnualSalary'])){
                $taxableMonthlySum +=  intval($tempArray[$Year."-".$Month]['Salary']);
                $workingMonth = true;
            }else{
                //Need To Unset if not Exist
                unset($tempArray[$Year."-".$Month]);
                $tempArray[$Year."-".$Month] = 0;
                $workingMonth = false;
            }


            /**
             * $change['totalAppliedIncrements']
             * $change['index'] //Will Show Up the Index
             * 'incrementInCurrentFinancialYear' => 0,
             * 'totalAppliedAllowances' => 0,
             * 'allowanceInCurrentFinancialYear' => 0,
             * 'SalaryIncreasedUpTo' => 0,
             * 'tempSalary' => 0  //Simply using it for tempSalary
             */
            $arrears = false;
            if($change['index'] >= 1){
                //If temp Salary Changed, Means We Need To Work on Areas.
                if($workingMonth === true && (intval($tempArray[$Year."-".$Month]['Salary']) !== intval($change['tempSalary']))){
                    $arrears = true;
                }
/*                //check if salary has been increased in any way.
                if(!empty($tempArray[$Year."-".$Month]['IncrementDates'])){
                    //If Any More Increment Dates Added
                    //This Means Salary Has Been Changed.
                    //We Need To Calculate Areas.
                    if(count($tempArray[$Year."-".$Month]['IncrementDates'] > $change['totalAppliedIncrements'])){
                        $arrears = true;
                        $totalIncrementsAppliedMore = count($tempArray[$Year."-".$Month]['IncrementDates']) - $change['totalAppliedIncrements'];
                    }
                    $change['totalAppliedIncrements'] = count($tempArray[$Year."-".$Month]['IncrementDates']);
                }*/

                if($arrears === true){

                    //Need Remaining Months.
                    $RemainingMonths = $change['totalRemainingMonths']--;
                    $currentTotalTaxableSUM = $taxableMonthlySum + ($tempArray[$Year."-".$Month]['Salary'] * $RemainingMonths);
                    $currentTotalTaxPayableMonthlySUM = $taxPayableMonthlySum + (($tempArray[$Year."-".$Month]['PayableTax']*$RemainingMonths) + ($tempArray[$Year."-".$Month]['Arrears']*$RemainingMonths));
                    $tempArray[$Year."-".$Month]['Arrears'] = round((($currentTotalTaxableSUM - $currentTotalTaxPayableMonthlySUM)/$RemainingMonths),2);
                    echo "This Month Arrears Are ".$arrears;
                }


            }else{
                $change['tempSalary'] = $tempArray[$Year."-".$Month]['Salary'];
                //Counting Applied Total Increments
                if(!empty($tempArray[$Year."-".$Month]['IncrementDates'])){
                    $change['totalAppliedIncrements'] = count($tempArray[$Year."-".$Month]['IncrementDates']);
                }
                //Counting Applied Total Allowances
                if(!empty($tempArray[$Year."-".$Month]['AllowanceDates'])){
                    $change['totalAppliedIncrements'] = count($tempArray[$Year."-".$Month]['IncrementDates']);
                }
            }

            if($workingMonth === true){
                //Calculate Payable Tax for This Month
                $tempArray[$Year."-".$Month]['PayableTax'] = (isset($tempArray[$Year."-".$Month]['TaxInfo']['MonthlyTaxToDeduct'])?$tempArray[$Year."-".$Month]['TaxInfo']['MonthlyTaxToDeduct']:0) + $tempArray[$Year."-".$Month]['Arrears'];

                    $taxPayableMonthlySum += intval($tempArray[$Year."-".$Month]['PayableTax']);

            }

            //Need To Stay Out to Keep Updating Indexes.
            $change['index'] ++;

        }


        print_r($tempArray);


        echo $taxableMonthlySum."<br />";
        echo $taxPayableMonthlySum;



        //Get records from employee tax statements table.
        $TaxStatementsTable = "employee_tax_statement ETS";
        $TaxStatementsSelectData = array('*',false);
        $TaxStatementsJoins = array(
            array(
                'table' => 'ml_financial_year MLFY',
                'condition' => 'MLFY.FinancialYearID = ETS.FinancialYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_year FY',
                'condition' => 'FY.ml_year_id = MLFY.FromYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_year TY',
                'condition' => 'TY.ml_year_id = MLFY.ToYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month FM',
                'condition' => 'FM.ml_month_id = MLFY.FromMonthID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month TM',
                'condition' => 'TM.ml_month_id = MLFY.ToMonthID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'tax_statement_lines TSL',
                'condition' => 'TSL.TaxStatementID = ETS.TaxStatementID AND TSL.IsTrashed = 0',
                'type' => 'LEFT'
            )
        );
        $whereTaxStatement = array(
            'ETS.EmploymentID' => $employmentID,
            'ETS.ISTrashed' => 0, //This Columns Might Not Be Needed But Just Added For Incase.
            'MLFY.FinancialYearID' => $financialYearID
        );

        $taxStatements = $ci->common_model->select_fields_where_like_join($TaxStatementsTable,$TaxStatementsSelectData,$TaxStatementsJoins,$whereTaxStatement);


/*
        //We need to add information in table if empty
        $totalTaxableSalary = 0;
        $totalPayable = 0;
        $change= array(
            'index' => 0,
            'totalAppliedIncrements' => 0,
            'incrementInCurrentFinancialYear' => 0,
            'totalAppliedAllowances' => 0,
            'allowanceInCurrentFinancialYear' => 0,
            'SalaryIncreasedUpTo' => 0,
            'tempSalary' => 0
        );

        foreach($tempArray as $key=>$array){ //$key Includes MonthYear
            if(!array_key_exists('AnnualSalary',$array)){ //According to our coding style this means no contract is available for this month.
                //This Means This Month Salary Will Be ZERO.
                unset($tempArray[$key]);
            }else{
                //Means We Have Contract for This Month.
                $totalTaxableSalary += $array['Salary'];
                $arrears = false;

                if($change['index'] >= 1){

                    //If temp Salary Changed, Means We Need To Work on Areas.
                    if(intval($array['Salary']) > intval($change['tempSalary'])){
                        $arrears = true;
                    }
                    //check if salary has been increased in any way.
                    if(!empty($array['IncrementDates'])){
                        //If Any More Increment Dates Added
                        //This Means Salary Has Been Changed.
                        //We Need To Calculate Areas.
                        if(count($array['IncrementDates'] > $change['totalAppliedIncrements'])){
                            $arrears = true;
                            $totalIncrementsAppliedMore = count($array['IncrementDates']) - $change['totalAppliedIncrements'];
                        }
                        $change['totalAppliedIncrements'] = count($array['IncrementDates']);
                    }



                    //Finally Calculate the Arrears
                    if($arrears === true){
                        //Do Arrears Stuff Here.
                    }


                }else{
                    $change['tempSalary'] = $array['Salary'];
                    //Counting Applied Total Increments
                    if(!empty($array['IncrementDates'])){
                        $change['totalAppliedIncrements'] = count($array['IncrementDates']);
                    }
                    //Counting Applied Total Allowances
                    if(!empty($array['AllowanceDates'])){
                        $change['totalAppliedIncrements'] = count($array['IncrementDates']);
                    }

                }
                //Need To Check if it has salary that needs to be deducted.
                if(array_key_exists('TaxInfo',$array)){

                }
            }
            $change['index']++;
        }

        echo "<br />".$totalTaxableSalary;

        if(empty($taxStatements)){
            //Need to code to add data to tables.
            //If Empty Then Just Add Data to Table.



        }*/

    }

    //END
}