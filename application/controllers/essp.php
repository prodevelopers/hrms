<?php 
class Essp extends My_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
        $this->load->helper('essp_helper');
		//$this->load->library('database');
		$this->load->model('essp_model');
		if($this->session->userdata('logged_in') != TRUE)
		{
			redirect('login');
		}

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if(!is_module_Allowed($loggedInEmployeeIDAccessCheck,$moduleController,'view') && !is_admin($loggedInEmployeeIDAccessCheck)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }
		
	}
	
	public function index()
	{
        $data['EmployeeID']= $this->session->userdata('employee_id');
        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($data['EmployeeID'],$datein);
        $data['enable'] = $this->essp_model->attendance_out($data['EmployeeID'],$dateout);

        //var_dump($data['enable']);
	    $empid=$this->session->userdata('employee_id');
        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
	$data['employee']=$this->essp_model->employee_info($empid);
    $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
    $data['employee']=$this->essp_model->employee_info($empid);
    $data['design'] = $this->essp_model->join_for_exp($where1);

    $employment_id=get_employment_from_employeeID($employee_id);
        $where=array('employment_id'=>$employment_id);
        $data['leave_notify']=$this->essp_model->get_records_where_desc('essp_employee_notifications',$where);
//        print_r($data['design']);die;
        /// Code for bar chart project/task progress ///
        $whereTP=array('AJ.employment_id' => $employment_id);
        $data['progress']=$this->essp_model->get_projectTask_progress($whereTP);
        //echo "<pre>";
        //print_r($data['progress']); die;
       /// End
	$this->load->view('essp/index',$data);	
	}
	
	public function personal_info()
	{
        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);

        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($employee_id,$datein);
        $data['enable'] = $this->essp_model->attendance_out($employee_id,$dateout);

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
        // print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;
        //$data['complete_profile'] = $this->hr_model->view_complete_profile($where);

        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        if(!empty($data['personal_info']->nationality)){
        $nat = array('nationality_id' => $data['personal_info']->nationality);
        $data['nat'] = $this->essp_model->get_row_by_where('ml_nationality',$nat);}
        if(!empty($data['personal_info']->marital_status)){
       $martl_status = array('marital_status_id' => $data['personal_info']->marital_status);
        $data['marital'] = $this->essp_model->get_row_by_where('ml_marital_status',$martl_status);}
        if(!empty($data['personal_info']->religion)){
        $reglion = array('religion_id' => $data['personal_info']->religion);
        $data['reg'] = $this->essp_model->get_row_by_where('ml_religion',$reglion);}
        $data['cont'] = $this->essp_model->get_row_by_where('current_contacts',$where);
        $data['contact'] = $this->essp_model->join_for_contact($where1);
        $data['emr'] = $this->essp_model->get_row_by_where('emergency_contacts',$where);
        if(!empty($data['emr']->relationship)){
        $id = array('relation_id' => $data['emr']->relationship);
        $data['jn_emr'] = $this->essp_model->get_row_by_where('ml_relation',$id);}
        $data['qua'] = $this->essp_model->get_row_by_where('qualification',$where);
        $data['design'] = $this->essp_model->join_for_exp($where1);
        $data['exp'] = $this->essp_model->join_for_exp_result($where1);
       // $data['exp'] = $this->essp_model->get_row_by_where('emp_experience',$where);
        $data['per'] = $this->essp_model->get_row_by_where('permanant_contacts',$where);
        if(!empty($employmentID)){
        $data['jn_skl'] = $this->essp_model->join_for_skl($employmentID);
        $idemployment = array('employment_id' => $employmentID);
        $data['skill'] = $this->essp_model->get_row_by_where('emp_skills',$idemployment);}
        $employmentWhere = array(
            'employee_id' => $employee_id,
            'employment.current' => 1
        );
        $data['eee'] = $this->essp_model->get_row_by_where('employment',$employmentWhere);
        if(!empty($data['eee']->employment_id)){
        $data['eee1'] = $this->essp_model->get_row_by_where('position_management',array('employement_id'=>$data['eee']->employment_id));
        $data['we'] = $this->essp_model->get_row_by_where('contract',array('employment_id'=>$data['eee']->employment_id));}
        if(!empty($data['we']->employment_type)){
        $data['we2'] = $this->essp_model->get_row_by_where('ml_job_category',array('job_category_id'=>$data['we']->employment_type));}
        if(!empty($data['eee']->employment_id)){
        $data['pp'] = $this->essp_model->get_row_by_where('posting',array('employement_id'=>$data['eee']->employment_id));}
        if(!empty($data['pp']->location)){
        $data['pp2'] = $this->essp_model->get_row_by_where('ml_posting_location_list',array('posting_locations_list_id'=>$data['pp']->location));}
        if(!empty($data['pp']->shift)){
        $data['pp3'] = $this->essp_model->get_row_by_where('ml_shift',array('shift_id'=>$data['pp']->shift));}
        if(!empty($data['pp']->ml_department_id)){
        $data['dd'] = $this->essp_model->get_row_by_where('ml_department',array('department_id'=>$data['pp']->ml_department_id));}
        if(!empty($data['per']->city_village)){
        $data['per4'] = $this->essp_model->get_row_by_where('ml_city',array('city_id'=>$data['per']->city_village));}
        //var_dump($data['pp2']);die;
        $data['emplyeee'] = $this->essp_model->detl_employeeee($where1);
        $data['depend'] = $this->essp_model->join_for_depend($where1);
        $data['pay'] = $this->essp_model->join_for_pay($where1);
        $data['last'] = $this->essp_model->join_for_last($where1);



        //Code For Getting Data For Entitlements Section..

        //Increments
        $iTable = 'increments I';
        $iSelectData = array('increment_amount AS Amount, increment_type AS Type, DATE_FORMAT(date_effective,"%D-%M-%Y") AS EffectiveDate',false);
        $iJoins = array(
            array(
                'table' => 'ml_increment_type MLIT',
                'condition' => 'MLIT.increment_type_id = I.increment_type_id AND MLIT.trashed = 0',
                'type' => 'INNER'
            )
        );
        $iWhere = array(
            'I.employment_id' => $employmentID,
            'I.trashed' => 0
        );
        $data['incrementsData'] = $this->common_model->select_fields_where_like_join($iTable,$iSelectData,$iJoins,$iWhere,FALSE);


       //Benefits
        $bTable = 'benefits B';
        $bSelectData = 'MLBT.benefit_type_title AS Benefit, MLS.status_title AS Status';
        $bJoins = array(
            array(
                'table' => 'ml_benefit_type MLBT',
                'condition' => 'MLBT.benefit_type_id = B.benefit_type_id AND MLBT.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'MLS.status_id = B.status',
                'type' => 'LEFT'
            )
        );
        $bWhere = array(
            'B.employment_id' => $employmentID,
            'B.trashed' => 0
        );

        $data['benefitsData'] = $this->common_model->select_fields_where_like_join($bTable,$bSelectData,$bJoins,$bWhere,FALSE);

        //Allowances
        $aTable = 'allowance A';
        $aSelectData = 'MLAT.allowance_type AS Allowance, A.allowance_amount AS Amount, MLS.status_title AS Status';
        $aJoins = array(
            array(
                'table' => 'ml_allowance_type MLAT',
                'condition' => 'MLAT.ml_allowance_type_id = A.ml_allowance_type_id AND MLAT.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'MLS.status_id = A.status',
                'type' => 'LEFT'
            )
        );
        $aWhere = array(
            'A.employment_id' => $employmentID
        );
        $data['allowanceData'] = $this->common_model->select_fields_where_like_join($aTable,$aSelectData,$aJoins,$aWhere,FALSE);


        //Leaves
        $lTables = 'leave_entitlement LE';
        $lSelectData = '
        MLLT.leave_type AS Type,
        LE.no_of_leaves_allocated AS AllocatedLeaves,
        LE.no_of_leaves_entitled EntitledLeaves,
        MLS.status_title AS Status';
        $lJoins = array(
            array(
                'table' => 'ml_leave_type MLLT',
                'condition' => 'MLLT.ml_leave_type_id = LE.ml_leave_type_id AND MLLT.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'MLS.status_id = LE.status',
                'type' => 'LEFT'
            )
        );
        $lWhere = array(
            'LE.trashed' => 0,
            'LE.employment_id' => $employmentID
        );

        $data['leavesEntitlementsData'] = $this->common_model->select_fields_where_like_join($lTables,$lSelectData,$lJoins,$lWhere,FALSE);


        //Need To Populate the Reporting Table.
        $rTable = 'reporting_heirarchy RH';
        $rSelectData = 'RH.report_heirarchy_id AS ReportingID,
        SupE.employee_code AS SupervisorCode,
        SupE.full_name AS SupervisorName,
        SupE.enrolled AS SupervisorEnrolledStatus,
        SupE.trashed AS SupervisorTrashedStatus,
        SubE.employee_code AS SubordinateCode,
        SubE.full_name AS SubordinateName,
        SubE.enrolled AS SubordinateEnrolledStatus,
        SubE.trashed AS SubordinateTrashedStatus,
        MLRO.reporting_option AS ReportingType';
        $rJoins = array(
            array(
                'table' => 'employee SupE', //Join For Supervisor Employee
                'condition' => 'SupE.employee_id = RH.employeed_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'employee SubE',
                'condition' => 'SubE.employee_id = RH.reporting_authority_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_reporting_options MLRO',
                'condition' => 'MLRO.reporting_option_id = RH.ml_reporting_heirarchy_id',
                'type' => 'LEFT'
            )
        );
        $rWhere = array(
            'RH.trashed' => 0,
            'RH.for_employee' => $employee_id
        );

        $reportingData = $this->common_model->select_fields_where_like_join($rTable,$rSelectData,$rJoins,$rWhere,FALSE);

        if(isset($reportingData) && !empty($reportingData) && is_array($reportingData)){
            $data['reportingDetails'] = array(
                'supervisors' => array(),
                'subordinates' => array()
            );
            foreach($reportingData as $rKey=>$rVal){
                if(isset($rVal->SupervisorCode) && !empty($rVal->SupervisorCode)){


                    if($rVal->SupervisorEnrolledStatus == '1' && $rVal->SupervisorTrashedStatus == '0'){
                        $SupStatus = '<span style="color:green">Active</span>';
                    }elseif($rVal->SupervisorEnrolledStatus == '0'){
                        $SupStatus = '<span style="color:black">Terminated/Retired</span>';
                    }elseif($rVal->SupervisorEnrolledStatus == '1' && $rVal->SupervisorTrashedStatus == '1'){
                        $SupStatus = '<span style="color:red">Not Available</span>';
                    }else{
                        $SupStatus = 'Unknown';
                    }

                    $arrayToPush = array(
                        'ReportingID' => $rVal->ReportingID,
                        'SupervisorCode' => $rVal->SupervisorCode,
                        'SupervisorName'=> $rVal->SupervisorName,
                        'ReportingType' => $rVal->ReportingType,
                        'Status' => $SupStatus
                    );


                    array_push($data['reportingDetails']['supervisors'],$arrayToPush);
                }elseif(isset($rVal->SubordinateCode) && !empty($rVal->SubordinateCode)){


                    if($rVal->SubordinateEnrolledStatus == '1' && $rVal->SubordinateTrashedStatus == '0'){
                        $SubStatus = '<span style="color:green">Active</span>';
                    }elseif($rVal->SubordinateEnrolledStatus == '0'){
                        $SubStatus = '<span style="color:black">Terminated/Retired</span>';
                    }elseif($rVal->SubordinateEnrolledStatus == '1' && $rVal->SubordinateTrashedStatus == '1'){
                        $SubStatus = '<span style="color:red">Not Available</span>';
                    }else{
                        $SubStatus = 'Unknown';
                    }

                    $arrayToPush = array(
                        'ReportingID' => $rVal->ReportingID,
                        'SubordinateCode' => $rVal->SubordinateCode,
                        'SubordinateName'=> $rVal->SubordinateName,
                        'ReportingType'=> $rVal->ReportingType,
                        'Status' => $SubStatus
                    );
                    array_push($data['reportingDetails']['subordinates'],$arrayToPush);                }
            }
        }

       // echo "<pre>"; print_r($data['last']);die;
	$empid=$this->session->userdata('employee_id');
	$data['employee']=$this->essp_model->employee_info($empid);
    $this->load->view('essp/includes/header',$data);
	$this->load->view('essp/personal_info',$data);	
	}
	
	public function salary_info()
	{
	$empid=$this->session->userdata('employee_id');

        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);
	
	$data['employee']=$this->essp_model->employee_info($empid);
	$selc_month=$this->input->post('month');
	$selc_year=$this->input->post('year');
	$data['selc_m']=$this->input->post('month');
	$data['selc_y']=$this->input->post('year');
	$data['months']=
	$this->payroll_model->ml_month_datatable('ml_month','ml_month_id','month');
	$data['years']=
	$this->payroll_model->ml_year_datatable('ml_year','ml_year_id','year');
	$emp_id=$this->session->userdata('employee_id');
	$where=array('employee.employee_id'=>$emp_id);
	$data['info']=$this->essp_model->salary_info($where,$selc_month,$selc_year);

        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['design'] = $this->essp_model->join_for_exp($where1);
	$this->load->view('essp/salary_info',$data);
		
	}
	
	public function progress()
	{
    $employee_id = $this->session->userdata('employee_id');

        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($employee_id,$datein);
        $data['enable'] = $this->essp_model->attendance_out($employee_id,$dateout);

    $where1 = array('employee.employee_id' => $employee_id);
    $data['design'] = $this->essp_model->join_for_exp($where1);
    $empid=$this->session->userdata('employee_id');


        //$data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $empid,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        //$empoymentID = $data['empl']->employment_id;
    $data['name'] = $this->essp_model->view_progress($empid);
    $data['name2'] = $this->essp_model->view_progress_table($empid);
   // echo"<pre>";print_r($data['name2']);die;
	$data['employee']=$this->essp_model->employee_info($empid);
	$this->load->view('essp/progress',$data);	
	}

    ///////////////////////// Function For Apply For Leave in Controller ///////////////////////////////

    public function apply_leave()
    {
        $employee_id = $this->session->userdata('employee_id');
        $employment_id=get_employment_from_employeeID($employee_id);

        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');


        $where=array('employment.employment_id'=>$employment_id);
        $full_name = $this->input->post('full_name');
        $leave_type = $this->input->post('leave_type');
        $data = $this->essp_model->leave_attendance_count($where);
        $count = count($data);
        //echo "<pre>";print_r($config['per_page']);die;
        $config['base_url'] = base_url() . 'essp/apply_leave';
        $config['total_rows'] = $count;
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);
        $start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if(empty($start) || $start == 1){
            $start = 0;
        }
        $data['leave_attend'] = $this->essp_model->leave_attendance($limit, $start, $full_name,$leave_type,$where);
        $data['links'] = $this->pagination->create_links();
       // $data['leave_type'] = $this->site_model->drop_down3();
        //$data['full_name'] = $this->site_model->drop_down3();


        $employee_id = $this->session->userdata('employee_id');
        //print_r($data['leaves']);die;
        $data['name']=$this->essp_model->apply_for_leave($employee_id);
        $data['leave_type'] = $this->essp_model->leave_type_ml();
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        $data['employee']=$this->essp_model->employee_info($employee_id);
        //echo print_r($data['employee']); die;
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        // echo "<pre>";
        // print_r($data['personal_info']);die;
        $data['disable'] = $this->essp_model->attendance_in($employee_id,$datein);
        $data['enable'] = $this->essp_model->attendance_out($employee_id,$dateout);
        $data['design'] = $this->essp_model->join_for_exp($where1);
        $this->load->view('essp/apply_leave',$data);
    }

 // Function For ADD Apply for Leave in Controller//

    // Function For ADD Apply for Leave
    public function add_apply_for_leave()
    {
        if ($this->input->post('add')) {
            $shortFull=$this->input->post('leave');
            // If employee apply for full day leave

                $postedEmployeeID = $this->input->post('emp_id');
                $postedLeaveType = $this->input->post('leave_type');
                //$data['rec'] = $this->site_model->get_all_by_where('leave_approval', array('employee_id' => $this->input->post('emp_id')));

                //Leave Application Is Based On Employment, So We Need To Find Current Employement ID for posted Employee
                $table = 'employment ET';
                $data = ('ET.employment_id AS EmploymentID');
                $where = array(
                    'ET.employee_id' => $postedEmployeeID,
                    'current' => 1,
                    'trashed' => 0
                );
                $result = $this->common_model->select_fields_where($table, $data, $where, TRUE);
                if (!isset($result) || empty($result)) {
                    echo "FAIL::This Employee Is Not In Roll, Can Not Add Details For This Employee::error";
                    return;
                }
                $employmentID = $result->EmploymentID;
                //print_r($employmentID);die;
                //Lets Do It Different Way, The Above Whoever Done it is not right...
                $table = 'leave_approval';
                $selectData = array('COUNT(1) AS TotalApplicationsSubmitted');
                $where = array(
                    'employment_id' => $employmentID,
                    'status' => 1,
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table, $selectData, $where, TRUE);
                if (isset($countResult) && $countResult->TotalApplicationsSubmitted > 0) {
                    $msg = "One Application is Already Pending!";
                    $this->session->set_flashdata('alert', $msg);
                    redirect("essp/apply_leave");
                    return;
                }
                //No Need TO Post Entitlement ID from View. We will get Entitlement ID from Here.
                //Getting Entitlement ID
                $table = 'leave_entitlement LE';
                $selectData = 'leave_entitlement_id AS EntitlementID';
                $where = 'status = 2 AND trashed = 0 AND ml_leave_type_id = ' . $postedLeaveType . ' AND employment_id = ' . $employmentID . ' AND date_approved = (SELECT MAX(date_approved) from leave_entitlement where status = 2 AND trashed = 0 AND ml_leave_type_id = ' . $postedLeaveType . ' AND employment_id = ' . $employmentID . ')';
                $resultID = $this->common_model->select_fields_where($table, $selectData, $where, TRUE);

                if (isset($resultID) && !empty($resultID) && $resultID->EntitlementID > 0) {
                    //This Will Continue
                    $entitlementID = $resultID->EntitlementID;
                } else {
                    $msg = "You Might Not Be Entitled For This Leave or Entitle Might be In Pending,If you think you are recieving this Message in Some Error then Please Contact System Administrator For Further Assistance";
                    $this->session->set_flashdata('alert', $msg);
                    redirect("essp/apply_leave");
                    return;
                }
                $fromdate = $this->input->post('from_date');
                $todate = $this->input->post('to_date');
                if (isset($fromdate) && !empty($fromdate)) {
                    $fromdate = date('Y-m-d', strtotime($fromdate));
                }
                if (isset($todate) && !empty($todate)) {
                    $todate = date('Y-m-d', strtotime($todate));
                }

                $add_type = array(
                    'employment_id' => $employmentID,
                    'entitlement_id' => $entitlementID,
                    'ml_leave_type_id' => $postedLeaveType,
                    'from_date' => $fromdate,
                    'to_date' => $todate,
                    'total_days' => $this->input->post('total_days'),
                    'application_date' => date('Y-m-d'),
                    'note' => $this->input->post('note'),
                    'status' => 1
                );
                //echo "<pre>"; print_r($add_type); die;
                $query = $this->site_model->create_new_record('leave_application', $add_type);

                $insert = array(
                    'employment_id' => $employmentID,
                    'leave_application_id' => $this->site_model->get_last_id()

                );
                // echo "<pre>"; print_r($insert); die;
                $query1 = $this->site_model->create_new_record('leave_approval', $insert);
                if ($query || $query1) {
                    //if Attempt Was Successful, Then Also Inform The Line Managers 0000194.
                    //First Need To Find All The Line Managers For the This Employee..
                    $PTable = 'reporting_heirarchy RH'; //Employee Table
                    $selectData = array('E.full_name AS EmployeeName, ES.full_name AS SupervisorName, SCC.official_email AS SupervisorEmailAddress',false);
                    $joins= array(
                        array(
                            'table' => 'employee E',
                            'condition' => 'E.employee_id = RH.for_employee AND E.trashed = 0 AND E.enrolled = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employee ES',
                            'condition' => 'ES.employee_id = RH.employeed_id AND ES.trashed = 0 AND ES.enrolled = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'current_contacts SCC',
                            'condition' => 'SCC.employee_id = ES.employee_id AND SCC.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where=array(
                        'RH.trashed' => 0,
                        'RH.for_employee' => $postedEmployeeID
                    );
                    $availableSupervisorsInfo = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where);
                    if(isset($availableSupervisorsInfo) && !empty($availableSupervisorsInfo)){
                        //If there is Data in It. Means We Have Line Managers For This Current Employee and We Need To Inform The Line Managers For Certain Application.
                        $supervisorMails = implode(',',array_column(json_decode(json_encode($availableSupervisorsInfo),true),'SupervisorEmailAddress'));

                        $config = $this->data['mailConfiguration'];
                        $this->load->library('email',$config);
                        $this->email->set_newline("\r\n");
                        //Now Lets Send The Email..
                        $this->email->to($supervisorMails);
                        $this->email->from('phrismapp@gmail.com','Phrism Administrator');
                        $this->email->subject('Employee Applied For Leave');
                        $this->email->message('Dear Sir/Madam, <br /> You Are Getting This Message because Employee Under Your Supervision Applied For Leave. Please Do Not Reply To This Email Address.');
                        if($this->email->send()) {
                            $msg ='Applied Successfully For Leaves, Also Email Has Been Dispatched To Respective Line Managers::success';
                            $this->session->set_flashdata('msg',$msg);
                        } else {
//                        echo $this->email->print_debugger();
                            $msg ='Applied Successfully For Leave, But Some problem Occurred during Sending of Email, Please Contact System Administrator For Further Assistance::warning';
                            $this->session->set_flashdata('msg',$msg);
                            /*                        echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                                                    return;*/
                        }
                    }else{
                        $msg ='Successfully Applied For Leave, But No Line Managers Found To Send Emails To, Please Contact System Administrator For Further Assistance::warning';
                        $this->session->set_flashdata('msg',$msg);
                    }
                    redirect('essp/apply_leave');
                }else{
                    $msg ='Some Problem Occurred, System Could Not Create Leave Application, Please Contact System Administrator For Further Assistance::error';
                    $this->session->set_flashdata('msg',$msg);
                    redirect('essp/apply_leave');
                }
            // if employee apply for short leave
        }
    }//End Of Function



	public function projects()
	{
    $employee_id = $this->session->userdata('employee_id');


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($employee_id,$datein);
        $data['enable'] = $this->essp_model->attendance_out($employee_id,$dateout);

    $where = array('employee_id' => $employee_id);
    $where1 = array('employee.employee_id' => $employee_id);
        $data['design'] = $this->essp_model->join_for_exp($where1);
    $data['task_history'] = $this->essp_model->view_task_history($where1);
   //echo "<pre>"; print_r($data['task_history']); die;
	$empid=$this->session->userdata('employee_id');
	$data['employee']=$this->essp_model->employee_info($empid);	
	$this->load->view('essp/projects',$data);	
	}
	
	public function expense_claim()
	{
		$where=array('enabled'=>1,
			 		 'trashed'=>0);
	$empid=$this->session->userdata('employee_id');


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);

	$data['employee']=$this->essp_model->employee_info($empid);
	$data['expense']=$this->essp_model->html_selectbox('ml_expense_type','-- Select Expense Type --','ml_expense_type_id','expense_type_title',$where);
	$data['selc_m']=$this->input->post('month');
	$data['selc_y']=$this->input->post('year');
	$data['expense_t']=$this->input->post('expense_type');
	$data['months']=$this->payroll_model->ml_month_datatable('ml_month','ml_month_id','month');
	$data['years']=$this->payroll_model->ml_year_datatable('ml_year','ml_year_id','year');

        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        //$data['employee']=$this->essp_model->employee_info($empid);
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['design'] = $this->essp_model->join_for_exp($where1);
        ///////// code for expense claims records

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
$employmentID = $data['empl']->employment_id;
        //print_r($employmentID);die;
        $rec=$this->essp_model->expense_count($employmentID);
        //print_r($rec);die;
        $count=count($rec);
        $config['base_url']     =base_url().'essp/expense_claim';
        $config['total_rows']   = $count;
        $config['per_page']     = 10;
        $config['uri_segment']  = 3;
        $config['prev_link']='previous';
        $config['next_link']='next';
        $config['full_tag_open'] = "<div id='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links']    =round($choice);
        $limit=$config['per_page'];
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['info']=$this->essp_model->expense_view($limit,$page,$employmentID);
        $data['slct_m']=$this->input->post('month');
        $data['slct_y']=$this->input->post('year');
        $data['slct_exp']=$this->input->post('exp_type');
        $data['month']=$this->essp_model->master_list_data('ml_month','Select Month','ml_month_id','month');
        $data['year']=$this->essp_model->master_list_data('ml_year','Select Year','ml_year_id','year');
        $data['expense_type']=$this->essp_model->master_list_data('ml_expense_type','Select Expense Type','ml_expense_type_id','expense_type_title');


        ///////////// end
	$this->load->view('essp/expense_claim',$data);
		
	}
    public function advance_claim()
    {
        $where=array('enabled'=>1,
            'trashed'=>0);
        $empid=$this->session->userdata('employee_id');


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);

        $data['employee']=$this->essp_model->employee_info($empid);
        $data['selc_m']=$this->input->post('month');
        $data['selc_y']=$this->input->post('year');
        $data['payment_t']=$this->input->post('payment_type');
        $data['payment_type']=$this->essp_model->html_selectbox('ml_payment_type','-- Select Payment Type --','payment_type_id','payment_type_title',$where);
        //$data['payment_mode']=$this->essp_model->
        //html_selectbox('ml_payment_mode_types','-- Select Payment Type --','payment_mode_type_id','payment_mode_type',$where);
        $data['status']=$this->essp_model->html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where);


        $data['months']=$this->essp_model->html_selectbox('ml_month','-- Select Month --','ml_month_id','month',$where);
        $data['years']=$this->essp_model->html_selectbox('ml_year','-- Select Year --','ml_year_id','year',$where);
        ///////////////////// code for advances records


        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $empid,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
$employmentID =  $data['empl']->employment_id;
        $rec=$this->payroll_model->loan_advacne_count($employmentID);
        $count=count($rec);
        $config['base_url']     =base_url().'essp/advance_claim';
        $config['total_rows']   = $count;
        $config['per_page']     = 10;
        $config['uri_segment']  = 3;
        $config['prev_link']='previous';
        $config['next_link']='next';
        $config['full_tag_open'] = "<div id='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links']    =round($choice);
        $limit=$config['per_page'];
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['info']=$this->essp_model->loan_advacne_view($employmentID,$limit,$page);
        $data['slct_m']=$this->input->post('month');
        $data['slct_y']=$this->input->post('year');
        $data['slct_adnc']=$this->input->post('adnc_type');
        $data['month']=$this->essp_model->master_list_data('ml_month','Select Month','ml_month_id','month');
        $data['year']=$this->essp_model->master_list_data('ml_year','Select Year','ml_year_id','year');
        $data['advance_type']=$this->essp_model->master_list_data('ml_payment_type','Select Type','payment_type_id','payment_type_title');


        ///////////////////// end
        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['design'] = $this->essp_model->join_for_exp($where1);
        $this->load->view('essp/advance_claim',$data);

    }

    public function loan_advance_pay()
    {
        $emp_id=$this->input->post('emp_id');
        $advance_month=$this->input->post('month');
        $advance_year=$this->input->post('year');
        $advance_type=$this->input->post('payment_type');

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $emp_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID =  $data['empl']->employment_id;
        $check=$this->payroll_model->check_advances($employmentID,$advance_type,$advance_month,$advance_year);

        if(!empty($check)){
            $this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#F00 !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Sorry You Already Submit this Request!</span>');
            redirect('essp/advance_claim');
        }else{
            $loan_advance=array(
                'employment_id'			 =>$employmentID,
                'payment_type'			 =>$this->input->post('payment_type'),
                'amount'				 =>$this->input->post('payment_amount'),
                'month'					 =>$this->input->post('month'),
                'year'					 =>$this->input->post('year'),
                'date_created'			 =>date('Y-m-d'),
                'created_by'			 =>$this->session->userdata('employee_id'), // need to change later for session user id
            );
            //echo "<pre>";
            //print_r($loan_advance);
            $query=$this->payroll_model->loan_advance_claim($loan_advance);

            if($query)
            {
                $this->session->set_flashdata('message', '<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Advance Request Submit succesfully!</span>');
                redirect('essp/advance_claim');
            }}
    }
    // End
	
	public function schedule()
	{
	$empid=$this->session->userdata('employee_id');
	$data['employee']=$this->essp_model->employee_info($empid);

        $employee_id = $empid;


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);

        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['design'] = $this->essp_model->join_for_exp($where1);
	$this->load->view('essp/schedule',$data);	
	}

    /////////////////////////////// Function For Time Sheet In Controller /////////////////////////////////////

    public function timesheet()
    {
        $empid=$this->session->userdata('employee_id');


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);

        $data['employee']=$this->essp_model->employee_info($empid);
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $data['months'] = $this->input->post('month');
        $data['years'] =  $this->input->post('year');
        $full_name = $this->input->post('full_name');
        $leave_type = $this->input->post('leave_type');
        //$data['emp_test'] = $this->site_model->get_emp_name($full_name, $month, $year, $leave_type);
        //print_r($data['emp_test']);die;
        //echo "<pre>"; print_r($full_name); die;

        $emp = array('employee.employee_id' => $this->session->userdata('employee_id'));

        $data['time_sheet'] = $this->essp_model->time_sheet($emp);

        //$data['emp'] = $this->site_model->get_row_by_id('employee', $emp);
        //$indate = $this->input->post('in_date');


        $data['month'] = $this->essp_model->month_same();
        $data['year'] = $this->essp_model->year_same();


        $employee_id = $this->session->userdata('employee_id');
        $where = array('employee_id' => $employee_id);
        $where1 = array('employee.employee_id' => $employee_id);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['personal_info'] = $this->essp_model->get_row_by_where('employee',$where);
        $data['employee']=$this->essp_model->employee_info($empid);
        $data['design'] = $this->essp_model->join_for_exp($where1);

        $this->load->view('essp/timesheet',$data);
    }

    ///////////////////// End
	
	public function go_to_list($param = NULL)
	{
        if($param == "list")
        {
            echo $this->essp_model->goto_list();
            die;
        }
        $data['full_name'] = $this->input->post('full_name');
        $data['exp'] = $this->site_model->dropdown_wd_option('employee', '-- Select Employee Name --','employee_id','full_name');
        $data['department_name'] = $this->input->post('department_name');
        $data['status'] = $this->site_model->dropdown_wd_option('ml_department', '-- Select Department --',	'department_id','department_name');

        $empid=$this->session->userdata('employee_id');
	$data['employee']=$this->essp_model->employee_info($empid);	
	$this->load->view('essp/go_to_list',$data);	
	}
	
	public function phone_book($param = NULL)
	{
        if($param == "list")
        {
            echo $this->essp_model->phone_book();
            die;
        }
	$empid=$this->session->userdata('employee_id');
	$data['employee']=$this->essp_model->employee_info($empid);	
	$this->load->view('essp/phone_book',$data);	
	}
	
	public function downloads()
	{
        $data['download'] = $this->site_model->get_all('download');
	$empid=$this->session->userdata('employee_id');


        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $data['disable'] = $this->essp_model->attendance_in($empid,$datein);
        $data['enable'] = $this->essp_model->attendance_out($empid,$dateout);

	$data['employee']=$this->essp_model->employee_info($empid);	
	$this->load->view('essp/downloads',$data);	
	}
	
	public function slip()
	{   //error_reporting(0);
		$id=$this->uri->segment(3);
        $month=$this->uri->segment(4);
        $year=$this->uri->segment(5);
        $where=array(
            'employee.employee_id'=>$id,
            'salary_payment.month'=>$month,
            'salary_payment.year'=>$year
        );

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $employmentID = $data['empl']->employment_id;

        $query['last_salary']=$this->payroll_model->last_salary($id);
        $query['salary_info']=$this->essp_model->month_salaries($where);
        $salary_trans_date=$query['salary_info']->transaction_date;
        $salary_payment_mode_id=$query['salary_info']->payment_mode;
        $query['payment_mode']=$this->essp_model->payment_mode_details_slip($salary_payment_mode_id);
        $query['ded_trans']=$this->essp_model->payroll_regded_trans_slip($employmentID,$month,$year,$salary_trans_date);
        $ded_trans_id=$query['ded_trans']->transaction_id;
        //$query['ded_info']=$this->payroll_model->ded_info($ded_trans_id);
        $query['allw_trans']=$this->essp_model->payroll_regallw_trans_slip($employmentID,$month,$year,$salary_trans_date);
        $query['exp_trans']=$this->essp_model->payroll_regexp_trans_slip($employmentID,$month,$year,$salary_trans_date);
        $query['advance_trans']=$this->essp_model->payroll_regadvance_trans_slip($employmentID,$month,$year,$salary_trans_date);

        $query['details']=$this->essp_model->payroll_reg_details_slip($employmentID,$month,$year,$salary_trans_date);
        $where_emp=array(
            'employee.employee_id'=>$id
        );
        $query['salaries']=$this->essp_model->year_salaries_slip($where_emp,$year);
        /*echo "<pre>";
        print_r($query['salaries']); die;*/
        //$data['info']=$this->essp_model->salarypayment_slip_emp($where);
		$this->load->view('essp/salary_cheque',$query);
	}
	
 // Function for add expenses records //
	
	public function add_expense_records()
	{
       // echo "testing ";
        $ecxp_date=$this->input->post('expdate');
        $exp_month=date("m",strtotime($ecxp_date));
        $exp_year=date("Y",strtotime($ecxp_date));
        $month= intval($exp_month);
        $where=array('ml_year.year'=>$exp_year);
        $eyear=$this->essp_model->get_row_by_id('ml_year',$where);
        $year=$eyear->ml_year_id;


		//echo "<pre>";print_r($data); die;
		//$month=$this->input->post('month');
		//$year=$this->input->post('year');
        $empID = $this->input->post('emp_id');

        $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $empID,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
        $emplomentID = $data['empl']->employment_id;
        $config['upload_path'] = 'upload/expense_receipts';
        $config['allowed_types'] = 'doc|docx|gif|jpeg|pdf|jpg|png|rtf|txt|text';
        $config['max_size'] = '6000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        //echo "tet 2";
        $this->load->library('upload', $config);
        $file=$this->upload->do_upload('receipt');
        if($file) {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];

            ///// For Date format
            if(isset($ecxp_date) && !empty($ecxp_date))
            {
               $ecxp_date = date('Y-m-d',strtotime($ecxp_date));
            }

            $data=array(
                'employment_id'			=>	$emplomentID,
                'expense_type'			=>	$this->input->post('expense_type'),
                'expense_date'	   	    =>	$ecxp_date,
                'amount'			    =>	$this->input->post('expamount'),
                'month'					 =>$month,
                'year'					 =>$year,
                'file_name'              =>$file_name,
                'date_created'	        =>	date('Y-m-d'),
                'created_by'		    =>	get_employment_from_employeeID($this->session->userdata('employee_id'))// need to change for session user id
            );
            //print_r($data);die;
            $query=$this->essp_model->expense_new_record('expense_claims',$data);
            if($query)
            {
                $empid=$this->session->userdata('employee_id');
                $data['employee']=$this->essp_model->employee_info($empid);
                $this->session->set_flashdata('msg','<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Claim submit with attachment successfully !</span>');
                redirect('essp/expense_claim',$data);
            }}
            else
            {
                $data=array(
                    'employment_id'			=>	$emplomentID,
                    'expense_type'			=>	$this->input->post('expense_type'),
                    'expense_date'	   	    =>	$ecxp_date,
                    'amount'			    =>	$this->input->post('expamount'),
                    'month'					 =>$month,
                    'year'					 =>$year,
                    'date_created'	        =>	date('Y-m-d'),
                    'created_by'		    =>	$this->session->userdata('employee_id')// need to change for session user id
                );
                $query=$this->essp_model->expense_new_record('expense_claims',$data);
                if($query)
                {
                    $empid=$this->session->userdata('employee_id');
                    $data['employee']=$this->essp_model->employee_info($empid);
                    $this->session->set_flashdata('msg','<span style="float:left;width:100%;height:50px;background:#6AAD6A !important;color:#FFF !important;text-align:center;line-height:50px;font-weight:bold;">Claim submit without attachment successfully !</span>');
                    redirect('essp/expense_claim',$data);
                }
            }
	}
    function download_app($file_name = NULL)
    {
        $this->load->library('image_lib');

        $myFile = "upload/Download_Files/".$file_name;
        header("Content-Length: " . filesize($myFile));
        header('Content-Type: {$this->mime_type}');
        header('Content-Disposition: attachment; filename='.$file_name);

        readfile($myFile);
    }
	// End of Function for add expenses records //




    ////////////////////////////////////////////// Leave Attendance Function /////////////////////////////////////////////

    public function Register_attendance()
    {
        $currentDate = date('Y-m-d');
        $table = 'ml_holiday';
        $data = 'ml_holiday_id';
        $where = array(
            'from_date <=' => $currentDate,
            'to_date >=' => $currentDate
        );
        $result = $this->common_model->select_fields_where($table,$data,$where,true);
        //echo print_r($result); die;
//        var_dump($result);
        $MlHolidayID = @$result->ml_holiday_id;
        if(isset($result) && !empty($MlHolidayID) && !empty($result)){

            //$insert =  date_default_timezone_set('Asia/karachi');
            $day = date('D');
            switch ($day)
            {
                case 'Mon':
                    $day = 1;
                    break;

                case 'Tue':
                    $day = 2;
                    break;

                case 'Wed':
                    $day = 3;
                    break;

                case 'Thu':
                    $day = 4;
                    break;

                case 'Fri':
                    $day = 5;
                    break;

                case 'Sat':
                    $day = 6;
                    break;

                case 'Sun':
                    $day = 7;
                    break;
            }
            $month = date('M');
            switch ($month)
            {
                case 'Jan':
                    $month = 1;
                    break;

                case 'Feb':
                    $month = 2;
                    break;

                case 'Mar':
                    $month = 3;
                    break;

                case 'Apr':
                    $month = 4;
                    break;

                case 'May':
                    $month = 5;
                    break;

                case 'Jun':
                    $month = 6;
                    break;

                case 'Jul':
                    $month = 7;
                    break;

                case 'Aug':
                    $month = 8;
                    break;

                case 'Sep':
                    $month = 9;
                    break;

                case 'Oct':
                    $month = 10;
                    break;

                case 'Nov':
                    $month = 11;
                    break;

                case 'Dec':
                    $month = 12;
                    break;

            }

            $year = $this->input->post('year');
            $yearexp = date("Y", strtotime($year));
            $where = array('ml_year.year' => $yearexp);

            $eyear = $this->site_model->get_row_by_id('ml_year',$where);
            $year =  $eyear->ml_year_id;
            $insert = array(
                'employee_id'            => $this->session->userdata('employee_id'),
                'in_time'                => $this->input->post('in_time'),
                'in_date'                => $this->input->post('in_date'),
                'attendance_status_type' => 2,
                'work_week_id'           => $day,
                'ml_month_id'            => $month,
                'ml_year_id' 			 => $year,
                'out_time' => '00:00:00',
                'out_date' => '0000-00-00',
                'remarks' => '',
                'holiday'=> $MlHolidayID
            );

            //$data['year_s'] = $this->site_model->year_same();
            $query = $this->site_model->create_new_record('attendence', $insert);
            if($query)
            {
                redirect('dashboard_site/view_dashboard');
            }}else{

            //$insert =  date_default_timezone_set('Asia/karachi');
            $day = date('D');
            switch ($day)
            {
                case 'Mon':
                    $day = 1;
                    break;

                case 'Tue':
                    $day = 2;
                    break;

                case 'Wed':
                    $day = 3;
                    break;

                case 'Thu':
                    $day = 4;
                    break;

                case 'Fri':
                    $day = 5;
                    break;

                case 'Sat':
                    $day = 6;
                    break;

                case 'Sun':
                    $day = 7;
                    break;
            }
            $month = date('M');
            switch ($month)
            {
                case 'Jan':
                    $month = 1;
                    break;

                case 'Feb':
                    $month = 2;
                    break;

                case 'Mar':
                    $month = 3;
                    break;

                case 'Apr':
                    $month = 4;
                    break;

                case 'May':
                    $month = 5;
                    break;

                case 'Jun':
                    $month = 6;
                    break;

                case 'Jul':
                    $month = 7;
                    break;

                case 'Aug':
                    $month = 8;
                    break;

                case 'Sep':
                    $month = 9;
                    break;

                case 'Oct':
                    $month = 10;
                    break;

                case 'Nov':
                    $month = 11;
                    break;

                case 'Dec':
                    $month = 12;
                    break;

            }

            $year = $this->input->post('year');
            $yearexp = date("Y", strtotime($year));
            $where = array('ml_year.year' => $yearexp);

            $eyear = $this->site_model->get_row_by_id('ml_year',$where);
            $year =  $eyear->ml_year_id;
            $insert = array(
                'employee_id'            => $this->session->userdata('employee_id'),
                'in_time'                => $this->input->post('in_time'),
                'in_date'                => $this->input->post('in_date'),
                'attendance_status_type' => 2,
                'work_week_id'           => $day,
                'ml_month_id'            => $month,
                'ml_year_id' 			 => $year,
                'out_time' => '00:00:00',
                'out_date' => '0000-00-00',
                'remarks' => '',
                'holiday'=> 0
            );

            //$data['year_s'] = $this->site_model->year_same();
            $query = $this->site_model->create_new_record('attendence', $insert);
            if($query)
            {
                redirect('dashboard_site/view_dashboard');
            }
        }
    }
//////////////////////////////////////////// Function for Day Out Attendance ///////////////////////////////////////////////////
    public function dayout_attendance()
    {


        $insert = array(
            'out_time' => $this->input->post('out_time'),
            'out_date' => $this->input->post('out_date'),
            'attendance_status_type' => 1,
        );

        $query = $this->essp_model->update_record('attendence',array('in_date ='=>date('Y-m-d'),'employee_id'=>$this->session->userdata('employee_id')),$insert);

        if($query)
        {
            $empid=$this->session->userdata('employee_id');
            $data['employee']=$this->essp_model->employee_info($empid);
            redirect('essp/index',$data);

        }
    }

    /**
     * @return string Returns success/error messages..
     *
     */
    function update_personal_information(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $LoggedInEmployeeID = $this->data['EmployeeID'];
                $form = $this->input->post('form');
                if($form === 'personalInformation'){
                    $employeeName = $this->input->post('employeeName');
                    $fatherName = $this->input->post('FatherName');
                    $CNIC = $this->input->post('CNIC');
                    $MaritalStatus = $this->input->post('MaritalStatus');
                    $dob = $this->input->post('DOB');
                    $nationality = $this->input->post('Nationality');
                    $religion = $this->input->post('Religion');

                    //Some Fields Are Mandatory, Need To Put Check For Them.
                    if(!isset($employeeName) || empty($employeeName)){
                        echo "FAIL::You Must Enter Name to Continue:error";
                        return;
                    }

                    //To Update Or Insert, First We Need To Check If employee Record Already Exist
                    $table = 'employee E';
                    $data = array('COUNT(1) AS TotalEmployeeRecords, E.full_name AS EmployeeName, E.employee_id AS EmployeeID',false);
                    $where = array(
                      'E.employee_id' => $LoggedInEmployeeID
                    );
                    $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);

                    $updateAddData = array(
                        'full_name' => $employeeName
                    );
                    if(isset($fatherName) && !empty($fatherName)){
                        $updateAddData['father_name'] = $fatherName;
                    }
                    if(isset($CNIC) && !empty($CNIC)){
                        if(!preg_match("/[0-9]{5}-[0-9]{7}-[0-9]{1}/", $CNIC)){
                            echo 'FAIL::Please Provide CNIC In Correct Format::warning';
                            return;
                        }
                        $updateAddData['CNIC'] = $CNIC;
                    }

                    if(isset($MaritalStatus) && !empty($MaritalStatus)){
                        if (strpos($MaritalStatus,',') !== false) {
                            $MaritalStatus = explode(',',$MaritalStatus);
                            $updateAddData['marital_status'] = $MaritalStatus[0];
                        }else{
                            $updateAddData['marital_status'] = $MaritalStatus;
                        }
                    }
                    if(isset($dob) && !empty($dob)){
                        $updateAddData['date_of_birth'] = $dob;
                    }
                    if(isset($nationality) && !empty($nationality)){
                        if (strpos($nationality,',') !== false) {
                            $nationality = explode(',',$nationality);
                            $updateAddData['nationality'] = $nationality[0];
                        }else{
                            $updateAddData['nationality'] = $nationality;
                        }
                    }
                    if(isset($religion) && !empty($religion)){
                        if (strpos($religion,',') !== false) {
                            $religion = explode(',',$religion);
                            $updateAddData['religion'] = $religion[0];
                        }else{
                            $updateAddData['religion'] = $religion;
                        }
                    }


                    if($result->TotalEmployeeRecords > 0){
                        //If Employee Record Exist Then We Need To Update The Record
                        $updateWhere = array(
                            'employee_id' => $LoggedInEmployeeID
                        );
                        $updateResult = $this->common_model->update($table,$updateWhere,$updateAddData);
                        //Give User Message For Record Inserted
                        if($updateResult === true){
                            echo "OK::Record Successfully Updated::success";
                        }else{
                            echo "FAIL::Some Database Error Occurred, Record Could Not Be Updated::error";
                        }
                        return;
                    }elseif ($result->TotalEmployeeRecords === 0){
                        //If No Employee Record Found Then We Will Add The New Record For the Employee..
                                //Actually No Insertions Will Gonna Happen Here, As To Login in ESSP, There Must Be Some Employee Record.
                    }
                }elseif($form === 'contact'){
                    //Current Address Postings
                    $currentAddress = $this->input->post('currentAddress');

                    //Permanent Address Postings
                    $permanentAddress = $this->input->post('permanentAddress');

                    //Emergency Contact
                    $contactFormType = $this->input->post('contactFormType');


                    if(isset($contactFormType) && $contactFormType === 'current'){
                        $currentCity = $this->input->post('currentCity');
                        $currentDistrict = $this->input->post('currentDistrict');
                        $currentTelephone = $this->input->post('telephone');
                        $currentEmail = $this->input->post('email');
                        $currentWorkEmail = $this->input->post('workEmail');
                        $currentMobile = $this->input->post('mobile');
                        $currentWorkTelephone = $this->input->post('workPhone');

                        //Now Lets Check If Record Already Exist
                        $table = 'current_contacts';
                        $data = 'COUNT(1) AS TotalEmployeeContactRecords';
                        $where = array(
                            'employee_id' => $LoggedInEmployeeID,
                            'trashed' => 0
                        );

                        $updateAddData = array();
                        if(isset($currentAddress) && !empty($currentAddress)){
                            $updateAddData['address'] = $currentAddress;
                        }
                        if(isset($currentCity) && !empty($currentCity)){
                            if (strpos($currentCity,',') !== false) {
                                $currentCity = explode(',',$currentCity);
                                $updateAddData['city_village'] = $currentCity[0];
                            }else{
                                $updateAddData['city_village'] = $currentCity;
                            }
                        }
                        if(isset($currentDistrict) && !empty($currentDistrict)){
                            if (strpos($currentDistrict,',') !== false) {
                                $currentDistrict = explode(',',$currentDistrict);
                                $updateAddData['District'] = $currentDistrict[0];
                            }else{
                                $updateAddData['District'] = $currentDistrict;
                            }
                        }
                        if(isset($currentTelephone) && !empty($currentTelephone)){
                            $updateAddData['home_phone'] = $currentTelephone;
                        }
                        if(isset($currentEmail) && !empty($currentEmail)){
                            $updateAddData['email_address'] = $currentEmail;
                        }
                        if(isset($currentWorkEmail) && !empty($currentWorkEmail)){
                            $updateAddData['official_email'] = $currentWorkEmail;
                        }
                        if(isset($currentMobile) && !empty($currentMobile)){
                            $updateAddData['mob_num'] = $currentMobile;
                        }
                        if(isset($currentWorkTelephone) && !empty($currentWorkTelephone)){
                            $updateAddData['office_phone'] = $currentWorkTelephone;
                        }

                        $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                        if($result->TotalEmployeeContactRecords > 0){
                            //We Will Do the Update If The Record Exist.
                            $ccUpdateResult = $this->common_model->update($table,$where,$updateAddData);
                            if($ccUpdateResult === true){
                                echo "OK::Records Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }else{
                            //We Will Do the Insertion if The Record Do Not Exist.
                            $updateAddData['employee_id'] = $LoggedInEmployeeID;
                            $ccInsertResult = $this->common_model->insert_record($table,$updateAddData);
                            if($ccInsertResult > 0){
                                echo "OK::Record Successfully Added::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }

                    }elseif(isset($contactFormType) && $contactFormType === 'permanent'){
                        $permanentDistrict = $this->input->post('district');
                        $permanentCity = $this->input->post('city');
                        $permanentTelephone = $this->input->post('telephone');
                        $permanentProvince = $this->input->post('permanentProvince');

                        $updateAddData = array();
                        if(!empty($permanentAddress)){
                            $updateAddData['address'] = $permanentAddress;
                        }

                        if(isset($permanentDistrict) && !empty($permanentDistrict)){
                            if (strpos($permanentDistrict,',') !== false) {
                                $permanentDistrict = explode(',',$permanentDistrict);
                                $updateAddData['district'] = $permanentDistrict[0];
                            }else{
                                $updateAddData['district'] = $permanentDistrict;
                            }
                        }

                        if(isset($permanentCity) && !empty($permanentCity)){
                            if (strpos($permanentCity,',') !== false) {
                                $permanentCity = explode(',',$permanentCity);
                                $updateAddData['city_village'] = $permanentCity[0];
                            }else{
                                $updateAddData['city_village'] = $permanentCity;
                            }
                        }else{
                            echo "FAIL::Please Select City From DropDown::error";
                            return;
                        }

                        if(isset($permanentTelephone) && !empty($permanentTelephone)){
                            $updateAddData['phone'] = $permanentTelephone;
                        }

                        if(isset($permanentProvince) && !empty($permanentProvince)){
                            if (strpos($permanentProvince,',') !== false) {
                                $permanentProvince = explode(',',$permanentProvince);
                                $updateAddData['province'] = $permanentProvince[0];
                            }else{
                                $updateAddData['province'] = $permanentProvince;
                            }
                        }

                        //Lets Check If Record Already Exist Or Not.
                        $table = 'permanant_contacts';
                        $data = 'COUNT(1) AS TotalEmployeePermanentContacts';
                        $where = array(
                          'employee_id' => $LoggedInEmployeeID
                        );
                        $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);
//                        print_r($result->TotalEmployeePermanentContacts);
                        if($result->TotalEmployeePermanentContacts > 0){
                            //Update the Record If the Already Exist
                            echo "FAIL::Hello World::error";
                            $permanentContactUpdateResult = $this->common_model->update($table,$where,$updateAddData);
                            if($permanentContactUpdateResult === true){
                                echo "OK::Record Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::success";
                                return;
                            }
                        }elseif(intval($result->TotalEmployeePermanentContacts) === 0){
                            //Insert Record If Do Not Exist..
                            $updateAddData['employee_id'] = $LoggedInEmployeeID;
                            $permanentContactInsertResult = $this->common_model->insert_record($table,$updateAddData);

//                            echo "FAIL::Hello World::error";
                            if($permanentContactInsertResult > 0){
                                echo "OK::Record Successfully Added::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator for Further Assistance::error";
                                return;
                            }
                        }

                    }elseif(isset($contactFormType) && $contactFormType === 'emergency'){
                        $emergencyContactPersonName = $this->input->post('emergencyContactPersonName');
                        $ecMobile = $this->input->post('ecMobile');
                        $homeTelephone = $this->input->post('homeTelephone');
                        $workTelephone = $this->input->post('workTelephone');
                        $relationship = $this->input->post('relationship');
                        if(isset($relationship) && !empty($relationship)){
                            if (strpos($relationship,',') !== false) {
                                $relationship = explode(',',$relationship);
                                $relationship = $relationship[0];
                            }
                        }
                        if(!isset($emergencyContactPersonName) || empty($emergencyContactPersonName)){
                            echo "FAIL::Person Name Can Not Be Null::error";
                            return;
                        }
                        if(!isset($ecMobile) || empty($ecMobile)){
                            echo "FAIL::Mobile Field Can Not Be Null::error";
                            return;
                        }
                        if(!isset($homeTelephone) || empty($homeTelephone)){
                            echo "FAIL::Home Telephone Field Can Not Be Null::error";
                            return;
                        }
                        if(!isset($workTelephone) || empty($workTelephone)){
                            echo "FAIL::Mobile Field Can Not Be Null::error";
                            return;
                        }
                        if(!isset($relationship) || empty($relationship)){
                            echo "FAIL::Mobile Field Can Not Be Null::error";
                            return;
                        }

                        //We First Need To Check If Record Already Exist Or Not.
                        $table = 'emergency_contacts';
                        $data = array('COUNT(1) AS TotalEmployeeEmergencyContacts');
                        $where = array(
                            'employee_id' => $LoggedInEmployeeID,
                            'trashed' => 0
                        );
                        $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                        $updateAddData = array(
                            'cotact_person_name' => $emergencyContactPersonName,
                            'relationship' => $relationship,
                            'home_phone' => $homeTelephone,
                            'mobile_num' => $ecMobile,
                            'work_phone' => $workTelephone
                        );
                        if($result->TotalEmployeeEmergencyContacts > 0){
                            //Update If Record Exist
                            $emergencyUpdateResult = $this->common_model->update($table,$where,$updateAddData);
                            if($emergencyUpdateResult === true){
                                echo "OK::Record Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Record Could Not Be Updated, Please Contact System Administrator for Further Assistance::error";
                                return;
                            }
                        }elseif($result->TotalEmployeeEmergencyContacts === 0){
                            //Insert If Record Do Not Exist
                            $updateAddData['employee_id'] = $LoggedInEmployeeID;
                            $emergencyInsertResult = $this->common_model->insert_record($table,$data);
                            if($emergencyInsertResult > 0){
                                echo "OK::Record Successfully Added::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error, Please Contact System Administrator for Further Assistance::error";
                                return;
                            }
                        }

                    }
                }
            }else{
                echo "FAIL::No Data Posted, Please Contact System Administrator For Further Assistance.::error";
                return;
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_marital_statuses(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_marital_status';
                $data = 'marital_status_id AS ID, marital_status_title AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'marital_status_title';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }
    //Functions For the Selectors
    function load_all_available_nationalities(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_nationality';
                $data = 'nationality_id AS ID, nationality AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'nationality';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_religion(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_religion';
                $data = 'religion_id AS ID, religion_title AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'religion_title';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_districts(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_district';
                $data = 'district_id AS ID, district_name AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'district_name';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_cities(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_city';
                $data = 'city_id AS ID, city_name AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'city_name';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_provinces(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_province';
                $data = 'province_id AS ID, province_name AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'province_name';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_relations(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_relation';
                $data = 'relation_id AS ID, relation_name AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'relation_name';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_skills(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_skill_type MLST';
                $data = 'MLST.skill_type_id AS ID, MLST.skill_name AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'MLST.skill_name';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }
    //Functions For the Selectors
    function load_all_available_skill_level(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_skill_level MLSL';
                $data = 'MLSL.level_id AS ID, MLSL.skill_level AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'MLSL.skill_level';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_jobs(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_job_title MLJT';
                $data = 'MLJT.ml_job_title_id AS ID, MLJT.job_title AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'MLJT.job_title';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }

    //Functions For the Selectors
    function load_all_available_employment_types(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');
                $table = 'ml_employment_type MLETT';
                $data = 'MLETT.employment_type_id AS ID, MLETT.employment_type AS TEXT';
                $where = array(
                    'trashed' => 0
                );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'MLETT.employment_type';
                    $result = $this->common_model->select_fields_where_like($table,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE);
                }
                //Finally Print The Output in Json..
                print_r(json_encode($result));
            }
        }
    }


    function add_job_title(){
        if($this->input->is_ajax_request()){
            $jobTitle = $this->input->post('jobTitle');
            if(isset($jobTitle) and !empty($jobTitle)){
                //if data is posted then we will do the insertion
                $table = 'ml_job_title';
                $data = array(
                    'trashed' => 0,
                    'enabled' => 1,
                    'job_title' => $jobTitle
                );
                $result = $this->common_model->insert_record($table,$data);
                if($result > 0){
                    echo "OK::New Job Title Successfully Added To The System::success::".$result;
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For the Further Assistance::error";
                    return;
                }
            }
        }
    }


    function add_skill_name(){
        if($this->input->is_ajax_request()){
            $skillName = $this->input->post('skillName');
            if(isset($skillName) and !empty($skillName)){
                //if data is posted then we will do the insertion
                $table = 'ml_skill_type';
                $data = array(
                    'trashed' => 0,
                    'enabled' => 1,
                    'skill_name' => $skillName
                );
                $result = $this->common_model->insert_record($table,$data);
                if($result > 0){
                    echo "OK::New Skill Successfully Added To The System::success::".$result;
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For the Further Assistance::error";
                    return;
                }
            }
        }
    }

    /**
     * Getting Data For DataTables..
     */

    //Employee Skills List DataTable
    function employee_skills_DT(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                //Load The Table Now As Request is Coming Through Proper Channel..
                $PTable = 'employee E';
                $data = array(
                    'ES.skill_record_id AS SkillID, MLST.skill_name AS Skill, MLSL.skill_level AS Level',
                    false
                );
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'emp_skills ES',
                        'condition' => 'ES.employment_id = ET.employment_id AND ES.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_skill_type MLST',
                        'condition' => 'MLST.skill_type_id = ES.ml_skill_type_id AND MLST.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_skill_level MLSL',
                        'condition' => 'MLSL.level_id = ES.ml_skill_level_id AND MLSL.trashed = 0',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.enrolled' => 1,
                    'E.trashed' => 0,
                    'E.employee_id' => $loggedInEmployeeID
                );
                $group_by = '';
                $addColumn = array(
                    'EditUpdate' => '<a style="cursor:pointer;" class="editUpdatePencil"><span class="fa fa-pencil"></span></a>',
                    'Trash' => '<a style="cursor:pointer;" class="trashBox"><span class="fa fa-trash"></span></a>'
                );
                $result = $this->common_model->select_fields_joined_DT($data,$PTable,$joins,$where,'','',$group_by,$addColumn);
                print_r($result);
                return;
            }
        }
    }

    //Employee Experience List DataTable
    function employee_experiences_DT(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                //Load The Table Now As Request is Coming Through Proper Channel..
                $PTable = 'employee E';
                $data = array(
                    'EE.experience_id AS ExpID,
                    MLETT.employment_type AS EmploymentType,
                    MLJT.job_title AS EmployeeJobName,
                    EE.job_title AS EmployeeJobID,
                    EE.from_date AS EmploymentFromDate,
                    EE.to_date AS EmploymentToDate,
                    EE.organization AS Organization',
                    false
                );
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'emp_experience EE',
                        'condition' => 'EE.employee_id = E.employee_id AND EE.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_employment_type MLETT',
                        'condition' => 'MLETT.employment_type_id = EE.ml_employment_type_id AND MLETT.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_job_title MLJT',
                        'condition' => 'MLJT.ml_job_title_id = EE.job_title AND MLJT.trashed = 0',
                        'type' => 'LEFT'
                    )
                );
                $where = array(
                    'E.enrolled' => 1,
                    'E.trashed' => 0,
                    'E.employee_id' => $loggedInEmployeeID
                );
                $group_by = '';
                $addColumn = array(
                    'EditUpdate' => '<a style="cursor:pointer;" class="editUpdatePencil"><span class="fa fa-pencil"></span></a>',
                    'Trash' => '<a style="cursor:pointer;" class="trashBox"><span class="fa fa-trash"></span></a>'
                );
                $result = $this->common_model->select_fields_joined_DT($data,$PTable,$joins,$where,'','',$group_by,$addColumn);
                print_r($result);
                return;
            }
        }
    }

    //Employee Qualifications List DataTable
    function employee_qualification_DT(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                //Load The Table Now As Request is Coming Through Proper Channel..
                $PTable = 'employee E';
                $data = array(
                    'MLQT.qualification_title AS QualificationTitle,
                    Q.qualification_id AS QualificationID,
                    Q.qualification AS DegreeLevel,
                    Q.institute AS Institute,
                    Q.year AS Year,
                    Q.gpa_score AS Score',
                    false
                );
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'qualification Q',
                        'condition' => 'Q.employee_id = E.employee_id AND Q.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_qualification_type MLQT',
                        'condition' => 'MLQT.qualification_type_id = Q.qualification_type_id AND MLQT.trashed = 0',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.enrolled' => 1,
                    'E.trashed' => 0,
                    'E.employee_id' => $loggedInEmployeeID
                );
                $group_by = '';
                $addColumn = array(
                    'EditUpdate' => '<a style="cursor:pointer;" class="editUpdatePencil"><span class="fa fa-pencil"></span></a>',
                    'Trash' => '<a style="cursor:pointer;" class="trashBox"><span class="fa fa-trash"></span></a>'
                );
                $result = $this->common_model->select_fields_joined_DT($data,$PTable,$joins,$where,'','',$group_by,$addColumn);
                print_r($result);
                return;
            }
        }
    }

    function updateSkillInfo($updateType = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                $skillID = $this->input->post('skill');
                //If Request is Requested Through a Proper Channel, Then We Will Gonna Do The Require work
                $employmentID=get_employment_from_employeeID($loggedInEmployeeID);
                $table = 'emp_skills';
                $where = array(
                    'skill_record_id' => $skillID,
                    'employment_id' =>$employmentID
                );
                if($updateType === 'trash'){
                    if(!isset($skillID) || empty($skillID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $data = array(
                        'trashed' => 1
                    );
                    $result = $this->common_model->update($table,$where,$data);
                    if($result === true){
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    }else{
                        echo "FAIL::Some Database error Occurred,Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }elseif($updateType === 'edit'){
                    if(!isset($skillID) || empty($skillID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $PTable = 'emp_skills ES';
                    $joins = array(
                        array(
                            'table' => 'ml_skill_type MLST',
                            'condition' => 'ES.ml_skill_type_id = MLST.skill_type_id AND ES.trashed = 0',
                            'type' => 'INNER'
                        ),array(
                            'table' => 'ml_skill_level MLSL',
                            'condition' => 'ES.ml_skill_level_id = MLSL.level_id AND ES.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $selectData = array('ES.skill_record_id AS SkillID, MLST.skill_name AS SkillName, ES.ml_skill_type_id AS SkillTypeID, MLSL.skill_level AS Level,ES.ml_skill_level_id',false);
                    $resultSkillInfo = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where,TRUE);
                    if(isset($resultSkillInfo) && !empty($resultSkillInfo)){
                        print_r(json_encode($resultSkillInfo));
                    }

                }elseif($updateType === 'save'){
                    //We Have Two Kind Of Savings Here, Need To Check For What Kind Request Is Generated.
                    //Save for Insertion of New Date, or Save for Updation of Existing Data

                    $skillName = $this->input->post('skillName');
                    $SkillLevel = $this->input->post('SkillLevel');
                    if(!isset($skillName) ||empty($skillName)){
                        echo "FAIL::Skill Name Field Can Not Be Left Empty::error";
                        return;
                    }

                    if(!isset($SkillLevel) ||empty($SkillLevel)){
                        echo "FAIL::Please Select Skill Level::error";
                        return;
                    }

                    //As Everything Above Went Fine, We Will Do The Insertions Now.
                    $insertData = array(
                        'ml_skill_level_id' => $SkillLevel,
                        'ml_skill_type_id' => $skillName,
                        'trashed' => 0
                    );

                    if(isset($skillID) and !empty($skillID)){
                        //Do The Updation Here Against the SkillID As We Are Getting The Skill ID
                        $result = $this->common_model->update($table,$where,$insertData);
                        if($result === true){
                            echo "OK::Record Successfully Updated::success";
                            return;
                        }else{
                            echo "FAIL::Some Database Error Occurred, Record Could Not Be Updated, Please Contact System Administrator For Further Assistance::error";
                            return;
                        }

                    }else{
                        //As We Dont Have SkillID Here , SO We Will Go For Insertion of New Data
                        $insertData['employment_id'] = $employmentID;
                        $result = $this->common_model->insert_record($table,$insertData);
                        if($result > 0){
                            echo "OK:: Record Successfully Added To The System::success";
                            return;
                        }else{
                            echo "FAIL:: Some Database Error Occurred Please Contact System Administrator For Further Assistance::error";
                            return;
                        }
                    }

                }
            }
        }
    }
    function updateExperienceInfo($updateType = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                $expID = $this->input->post('exp');
                //If Request is Requested Through a Proper Channel, Then We Will Gonna Do The Require work
                $table = 'emp_experience';
                $where = array(
                    'experience_id' => $expID,
                    'employee_id' => $loggedInEmployeeID
                );
                if($updateType === 'trash'){
                    if(!isset($expID) || empty($expID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $data = array(
                        'trashed' => 1
                    );
                    $result = $this->common_model->update($table,$where,$data);
                    if($result === true){
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    }else{
                        echo "FAIL::Some Database error Occurred,Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }elseif($updateType === 'edit'){
                    if(!isset($expID) || empty($expID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $PTable = 'emp_experience EE';
                    $joins = array(
                        array(
                            'table' => 'ml_job_title MLJT',
                            'condition' => 'EE.job_title = MLJT.ml_job_title_id AND MLJT.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_employment_type MLETT',
                            'condition' => 'MLETT.employment_type_id = EE.ml_employment_type_id AND MLETT.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where['EE.trashed'] = 0;
                    $selectData = array('EE.experience_id AS ExpID, MLJT.job_title AS JobTitle, EE.job_title AS JobID, EE.organization AS Organization, EE.ml_employment_type_id AS EmploymentTypeID, MLETT.employment_type AS EmploymentTypeText, EE.from_date AS EmploymentFromDate, EE.to_date AS EmploymentToDate, DATE_FORMAT(EE.from_date,"%d-%m-%Y") AS EmploymentFromDateUsr, DATE_FORMAT(EE.to_date,"%d-%m-%Y") AS EmploymentToDateUsr',false);
                    $resultSkillInfo = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where,TRUE);
                    if(isset($resultSkillInfo) && !empty($resultSkillInfo)){
                        print_r(json_encode($resultSkillInfo));
                    }

                }elseif($updateType === 'save'){
                    //We Have Two Kind Of Savings Here, Need To Check For What Kind Request Is Generated.
                    //Save for Insertion of New Date, or Save for Updation of Existing Data

                    $employmentType = $this->input->post('employmentType');
                    $jobTitle = $this->input->post('jobTitle');
                    $empStartDate = $this->input->post('startDate');
                    $empEndDate = $this->input->post('endDate');
                    $organization = $this->input->post('organization');
                    if(!isset($jobTitle) || empty($jobTitle)){
                        echo "FAIL::Job Title Field Can Not Be Left Empty::error";
                        return;
                    }

                    if(!isset($employmentType) ||empty($employmentType)){
                        echo "FAIL::Please Fill Employment Type Field::error";
                        return;
                    }


                    //As Everything Above Went Fine, We Will Do The Insertions Now.
                    $insertData = array(
                        'job_title' => $jobTitle,
                        'ml_employment_type_id' => $employmentType,
                        'trashed' => 0
                    );

                    if(isset($organization) && !empty($organization)){
                        $insertData['organization'] = $organization;
                    }

                    if(isset($empStartDate) && !empty($empStartDate)){
                        $insertData['from_date'] = $empStartDate;
                    }

                    if(isset($empEndDate) && !empty($empEndDate)){
                        $insertData['to_date'] = $empEndDate;
                    }

                    if(isset($expID) && !empty($expID)){
                        //If ExpID has Been Provided Then We Will Do the Updation against that ID.
                        $result = $this->common_model->update($table,$where,$insertData);
                        if($result === true){
                            echo "OK::Record Successfully Updated::success";
                            return;
                        }else{
                            echo "FAIL::Some Database Error Occurred, Record Could Not Be Updated, Please Contact System Administrator For Further Assistance::error";
                            return;
                        }

                    }else{
                        //No ExpID has Been Provided, Which Tell Us To Do The Insertion, Not the Updation..
                        $insertData['employee_id'] = $loggedInEmployeeID;
                        $result = $this->common_model->insert_record($table,$insertData);
                        if($result > 0){
                            echo "OK:: Record Successfully Added To The System::success";
                            return;
                        }else{
                            echo "FAIL:: Some Database Error Occurred Please Contact System Administrator For Further Assistance::error";
                            return;
                        }
                    }
                }
            }
        }
    }

    //Function For Education Form Functions
    function updateEducationInfo($updateType = NULL){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployeeID = $this->data['EmployeeID'];
                $qualificationID = $this->input->post('qualification');
                //If Request is Requested Through a Proper Channel, Then We Will Gonna Do The Require work
                $table = 'qualification';
                $where = array(
                    'qualification_id' => $qualificationID,
                    'employee_id' => $loggedInEmployeeID
                );
                if($updateType === 'trash'){
                    if(!isset($qualificationID) || empty($qualificationID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $data = array(
                        'trashed' => 1
                    );
                    $result = $this->common_model->update($table,$where,$data);
                    if($result === true){
                        echo "OK::Record Successfully Trashed From The System::success";
                        return;
                    }else{
                        echo "FAIL::Some Database error Occurred,Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                }elseif($updateType === 'edit'){
                    if(!isset($qualificationID) || empty($qualificationID)){
                        echo "FAIL::Data Not Posted Correctly, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $PTable = 'qualification Q';
                    $joins = array(
                        array(
                            'table' => 'ml_qualification_type MLQT',
                            'condition' => 'MLQT.qualification_type_id = Q.qualification_type_id AND MLQT.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where['Q.trashed'] = 0;
                    $selectData = array('
                    Q.qualification_id AS QualificationID,
                    MLQT.qualification_title AS QualificationTitle,
                    Q.qualification AS DegreeLevel,
                    Q.institute AS Institute,
                    Q.year AS Year,
                    Q.gpa_score AS Score,
                    Q.major_specialization AS Specialization,
                    Q.duration AS Duration,
                    Q.qualification_type_id AS QualificationTypeID',false);
                    $resultSkillInfo = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where,TRUE);
                    if(isset($resultSkillInfo) && !empty($resultSkillInfo)){
                        print_r(json_encode($resultSkillInfo));
                    }

                }elseif($updateType === 'save'){
                    //We Have Two Kind Of Savings Here, Need To Check For What Kind Request Is Generated.
                    //Save for Insertion of New Date, or Save for Updation of Existing Data

                    $completionYear = $this->input->post('completionYear');
                    $degree = $this->input->post('degree');
                    $institute = $this->input->post('institute');
                    $qualificationType = $this->input->post('qualificationType');
                    $score = $this->input->post('score');
                    $specialization = $this->input->post('specialization');
                    $diploma = $this->input->post('diploma');
                    $diplomaDuration = $this->input->post('diplomaDuration');
                    $certificate = $this->input->post('certificate');

                    if(!isset($qualificationType) || empty($qualificationType)){
                        echo "FAIL::Some Error In Form Post, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }

                    if($qualificationType === 'degree'){
                        //Lets Do the Checks Now For Degree Form Data
                        if(!isset($degree) || empty($degree)){
                            echo "FAIL::Degree/Level Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($institute) || empty($institute)){
                            echo "FAIL::Institute Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($completionYear) || empty($completionYear)){
                            echo "FAIL::Year Of Completion Field Can Not Be Left Empty::error";
                            return;
                        }
                        $insertUpdateData = array(
                            'qualification' => $degree,
                            'institute' => $institute,
                            'year' => $completionYear
                        );
                        if(isset($score) && !empty($score)){
                            $insertUpdateData['gpa_score'] = $score;
                        }
                        if(isset($specialization) && !empty($specialization)){
                            $insertUpdateData['major_specialization'] = $specialization;
                        }
                        $insertUpdateData['qualification_type_id'] = 1;
                        //Now Need To Check If We Need To Do The Insertion or If We Need To Do The Updation
                        if(isset($qualificationID) && $qualificationID > 0){
                            //If Set then We Need To Do the Updation
                            $updateResult = $this->common_model->update($table,$where,$insertUpdateData);
                            if($updateResult === true){
                                echo "OK::Record Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }else{
                            //If Not Set then We Need TO Do The Insertion
                            $insertUpdateData['employee_id'] = $loggedInEmployeeID;
                            $resultInsert = $this->common_model->insert_record($table,$insertUpdateData);
                            if($resultInsert > 0){
                                echo "OK::Record Successfully Added In the System::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }
                    }elseif($qualificationType === 'diploma'){
                        //Lets Do the Checks Now For Diploma Form Data
                        if(!isset($diploma) || empty($diploma)){
                            echo "FAIL::Diploma Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($institute) || empty($institute)){
                            echo "FAIL::Institute Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($completionYear) || empty($completionYear)){
                            echo "FAIL::Year Field Can Not Be Left Empty::error";
                            return;
                        }
                        $insertUpdateData = array(
                            'qualification' => $diploma,
                            'institute' => $institute,
                            'year' => $completionYear,
                            'gpa_score' => '-'
                        );
                        if(isset($diplomaDuration) && !empty($diplomaDuration)){
                            $insertUpdateData['duration'] = $score;
                        }
                        $insertUpdateData['qualification_type_id'] = 2;
                        //Now Need To Check If We Need To Do The Insertion or If We Need To Do The Updation
                        if(isset($qualificationID) && $qualificationID > 0){
                            //If Set then We Need To Do the Updation
                            $updateResult = $this->common_model->update($table,$where,$insertUpdateData);
                            if($updateResult === true){
                                echo "OK::Record Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }else{
                            //If Not Set then We Need TO Do The Insertion
                            $insertUpdateData['employee_id'] = $loggedInEmployeeID;
                            $resultInsert = $this->common_model->insert_record($table,$insertUpdateData);
                            if($resultInsert > 0){
                                echo "OK::Record Successfully Added In the System::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }
                    }elseif($qualificationType === 'certificate'){
                        if(!isset($certificate) ||empty($certificate)){
                            echo "FAIL::Certificate Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($institute) || empty($institute)){
                            echo "FAIL::Institute Field Can Not Be Left Empty::error";
                            return;
                        }
                        if(!isset($completionYear) || empty($completionYear)){
                            echo "FAIL::Year Field Can Not Be Left Empty::error";
                            return;
                        }
                        $insertUpdateData = array(
                            'qualification' => $certificate,
                            'institute' => $institute,
                            'year' => $completionYear
                        );
                        $insertUpdateData['qualification_type_id'] = 3;
                        //Now Need To Check If We Need To Do The Insertion or If We Need To Do The Updation
                        if(isset($qualificationID) && $qualificationID > 0){
                            //If Set then We Need To Do the Updation
                            $updateResult = $this->common_model->update($table,$where,$insertUpdateData);
                            if($updateResult === true){
                                echo "OK::Record Successfully Updated::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }else{
                            //If Not Set then We Need TO Do The Insertion
                            $insertUpdateData['employee_id'] = $loggedInEmployeeID;
                            $resultInsert = $this->common_model->insert_record($table,$insertUpdateData);
                            if($resultInsert > 0){
                                echo "OK::Record Successfully Added In the System::success";
                                return;
                            }else{
                                echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                                return;
                            }
                        }
                    }
                }
            }
        }
    }


/////////////END OF FUNCTION Register Attendance //////////////////////////////

}
?>