<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dashboard_site extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('site_model');
		$this->load->model('common_model');
		$this->load->library('datatables');
        $this->load->helper('extra_helper');

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if(!is_module_Allowed($loggedInEmployeeIDAccessCheck,$moduleController,'view') && !is_admin($loggedInEmployeeIDAccessCheck)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
            exit;
        }
	}
	/// Function For Uploaded Documents / Android Apps
	public function download_app()
	{
		$data['downloads'] = $this->hr_model->get_all('download');
		$this->load->view('includes/header');
		$this->load->view('dashboard/downloads', $data);
		$this->load->view('includes/footer');
	}
	/// Function For Uploading Files and Images
	public function ins_download()
	{
		if($this->input->post('continue'))
		{
			$file 		= $this->hr_model->upload_pic('attached_file');			
			//$thumb 		= $this->hr_model->resize_image($file['upload_data']['full_path'], "upload/Thumb_Nails");
			//$thumbNail 	= $thumb['upload_data']['file_name'];
			$file_name 	= $file['upload_data']['file_name']; 
			
			$insert_files 		= array(
			'attached_file' 	=> $file_name,
			'remarks' 			=> $this->input->post('remarks'),
			'upload_date' 		=> date('Y-m-d')
			);
			$query_files = $this->hr_model->create_new_record('download', $insert_files);
			
			if($query_files)
			{
				redirect('dashboard_site/download_app');
			}
		}
		$this->load->view('includes/header');
		$this->load->view('dashboard/ins_down');
		$this->load->view('includes/footer');
	}
	/* Download Uploaded File */
    function download($file_name = NULL) 
	{
		$this->load->library('image_lib');
		
        $myFile = "upload/Download_Files/".$file_name;
        header("Content-Length: " . filesize($myFile));
        header('Content-Type: {$this->mime_type}');
        header('Content-Disposition: attachment; filename='.$file_name);

        readfile($myFile);
    }
    //////////FUNCTION FOR DASHBOARD VIEW/////////////////////
    public function view_dashboard()
    {

        $loggedInEmployee = $this->session->userdata('employee_id');
        $datein = date('Y-m-d');
        $dateout = date('Y-m-d');
        $viewData['disable'] = $this->site_model->attendance_in($loggedInEmployee,$datein);
//var_dump($viewData['disable']);
        $viewData['enable'] = $this->site_model->attendance_out($loggedInEmployee,$dateout);
        ///var_dump($viewData['enable']);
        /*===========Coding To Get Data For The Charts ==========*/
        //Districts Pie Chart.
        $PTable = 'employee E';
        $data = ('COUNT(PC.`District`) AS value, MLD.color, MLD.highlight, MLD.district_name AS label ');
        $joins = array(
            array(
                'table' => 'permanant_contacts PC',
                'condition' => 'E.employee_id=PC.employee_id AND PC.trashed=0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_district MLD',
                'condition' => 'MLD.district_id=PC.District AND MLD.enabled=1',
                'type' => 'INNER'
            )
        );
        $where = array(
            'E.trashed' => '0',
            'E.enrolled' => 1 //It will check if the Employee is still enrolled.
        );
        $group_by = 'PC.District';
        $viewData['EmpInDistricts'] = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);


        //Projects Pie Chart
        $PTable = 'employee E';
        $data = ('
        COUNT(E.employee_id) AS value, MLP.color, MLP.highlight, MLP.project_title AS label');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_project EP',
                'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0 AND EP.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'MLP.project_id = EP.project_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_project_status_type MLPST',
                'condition' => 'MLPST.pst_id = MLP.project_status_type_id AND MLPST.trashed = 0 AND MLPST.enabled = 1',
                'type' => 'INNER'
            )
        );
        $where = array(
            'E.trashed' => '0',
            'E.enrolled' => 1, //It will check if the Employee is still enrolled.
            'MLP.project_status_type_id !=' => 4
        );
        $group_by = 'EP.project_id';
        $EmpInProjects = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
//        var_dump($EmpInProjects);
        if($EmpInProjects !== FALSE){
            $viewData['EmpInProjects'] = $EmpInProjects;
        }
        //Now need to code for the bar charts..
        //Gender Bar Chart
        $pastYears = intval(5);
        $rangeYear = $pastYears - intval(1);
        $last5Years = @range(date("Y"), date("Y",strtotime("-".$rangeYear." year")));
        $PTable = 'employee E';
        $data = array('
        E.employee_id AS EmployeeID,
        E.employee_code AS EmployeeCode,
        E.full_name AS EmployeeName,
        E.gender AS GenderTypeID,
        MLGT.`gender_type_title` AS GenderType,
        DATE_FORMAT(ET.joining_date, "%Y") AS Date,
        COUNT(E.`employee_id`) AS Total'
        ,false);
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_gender_type MLGT',
                'condition' => 'E.gender = MLGT.gender_type_id',
                'type' => 'INNER'
            )
        );
        $where = array(
            'E.trashed' => 0
        );
        $group_by = 'Date, E.gender';
        $resultCurrentEmployeesGenders = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
        if($resultCurrentEmployeesGenders !== FALSE){
            $viewData['EmpGendersLast5Years'] = createBarChartData($resultCurrentEmployeesGenders,'GenderType',$pastYears);
        }

        //End of Gender BarChart Finally

        //Leaver Stats BarChart 11-12-2014
        $PTable = 'employee E';
        $data = array('
        MLST.separation_type AS SeparationType,
        SM.ml_seperation_type AS SeparationTypeID,
        COUNT(SM.ml_seperation_type) AS Total,
        SM.date AS Date
        ',false);
        $joins = array(
            array(
                'table' => 'seperation_management SM',
                'condition' => 'SM.employee_id=E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_separation_type MLST',
                'condition' => 'MLST.separation_type_id = SM.ml_seperation_type',
                'type' => 'INNER'
            )
        );
        $where = ('SM.date >= DATE_SUB(NOW(),INTERVAL '.$pastYears.' YEAR)');
        $group_by = 'SM.ml_seperation_type, YEAR(SM.date)';
        $resultLeaversData = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
        if($resultLeaversData !== FALSE){
            $viewData['LeaversData'] = createBarChartData($resultLeaversData,'SeparationType',$pastYears);
        }
//        $viewData['LeaversData'] = createBarChartData($resultLeaversData,'SeparationType',$pastYears);
        //Finally Finished with the Leavers Data with the Common Function Created Today on 11-12-2014

        //Now Need to Work on the Joiners Data.
        $PTable = 'employee E';
        $data=('
        MLGT.gender_type_title AS GenderType,
        MLGT.gender_type_id AS GenderID,
        COUNT(E.gender) AS Total,
        ET.joining_date AS Date
        ');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed=0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_gender_type MLGT',
                'condition' => 'E.gender = MLGT.gender_type_id AND MLGT.trashed=0',
                'type' => 'INNER'
            )
        );
        $where = 'E.trashed = 0 AND ET.joining_date >= DATE_SUB(NOW(),INTERVAL '.$pastYears.' YEAR)';
        $group_by = 'E.`gender`, YEAR(ET.`joining_date`)';
        $resultJoinersData = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
        if($resultJoinersData !== FALSE){
            $viewData['JoinersData'] = createBarChartData($resultJoinersData,'GenderType',$pastYears);
        }

        /**
         * Dashboard Tables
         */
        //Get the List of Alerts Which are not Needed.
        $table = 'employee_alerts EA';
        $data = ('
            EA.alert_id AS AlertID
        ');
        //Getting Projects ID's that we Don't Need In Alerts in Dashboard..
        $where = array(
            'EA.employee_id' => $loggedInEmployee,
            'EA.alert_text' => 'Project'
        );
        $ProjectsListToUnset = $this->common_model->select_fields_where($table,$data,$where);
        $ProjectsListToUnset = json_decode(json_encode($ProjectsListToUnset),true);
        if(isset($ProjectsListToUnset) and !empty($ProjectsListToUnset)){
            $project_ids = array_column($ProjectsListToUnset, 'AlertID');
            $project_ids = implode(',',$project_ids);
        }

        //Getting Exits ID's that we Don't Need In Alerts in Dashboard..
        $where = array(
            'EA.employee_id' => $loggedInEmployee,
            'EA.alert_text' => 'Exit'
        );
        $ExitsListToUnset = $this->common_model->select_fields_where($table,$data,$where);
        $ExitsListToUnset = json_decode(json_encode($ExitsListToUnset),true);
        if(isset($ExitsListToUnset) and !empty($ExitsListToUnset)){
            $separation_ids = array_column($ExitsListToUnset, 'AlertID');
            $separation_ids = implode(',',$separation_ids);
        }

        //Getting Contract Expiry that we Don't Need In Alerts in Dashboard..
        $where = array(
            'EA.employee_id' => $loggedInEmployee,
            'EA.alert_text' => 'ContractExpiry'
        );
        $contractExpiryAlerts = $this->common_model->select_fields_where($table,$data,$where);
        $contractExpiryAlerts = json_decode(json_encode($contractExpiryAlerts),true);
        if(isset($contractExpiryAlerts) and !empty($contractExpiryAlerts)){
            $expContract_ids = array_column($contractExpiryAlerts, 'AlertID');
            $expContract_ids = implode(',',$expContract_ids);
        }

        //Getting Contract Extensions that we Don't Need In Alerts in Dashboard..
        $where = array(
            'EA.employee_id' => $loggedInEmployee,
            'EA.alert_text' => 'NewContractExtensions'
        );
        $contractExtensionsAlerts = $this->common_model->select_fields_where($table,$data,$where);
        $contractExtensionsAlerts = json_decode(json_encode($contractExtensionsAlerts),true);
        if(isset($contractExtensionsAlerts) and !empty($contractExtensionsAlerts)){
            $extContract_ids = array_column($contractExtensionsAlerts, 'AlertID');
            $extContract_ids = implode(',',$extContract_ids);
        }

        //Getting Probation Alerts that we Don't Need In Alerts in Dashboard..
        $where = array(
            'EA.employee_id' => $loggedInEmployee,
            'EA.alert_text' => 'ProbationExpiry'
        );
        $alertData = ('
            EA.alert_id AS AlertID
        ');
        $probationExpiryAlerts = $this->common_model->select_fields_where($table,$alertData,$where);
        $probationExpiryAlerts = json_decode(json_encode($probationExpiryAlerts),true);
        if(isset($probationExpiryAlerts) and !empty($probationExpiryAlerts)){
            $probationExp_ids = array_column($probationExpiryAlerts, 'AlertID');
            $probationExp_ids = implode(',',$probationExp_ids);
        }
//        print_r($project_ids);

        //Get the Data for Projects.
        $table = 'employee_project EP';
        $data = array(
            'EP.project_id AS ProjectID, MLP.project_title AS ProjectName,MLPST.status_type_title AS ProjectStatus',
            false
        );
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employment_id = EP.employment_id AND ET.trashed = 0 AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'MLP.project_id = EP.project_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_project_status_type MLPST',
                'condition' => 'MLPST.pst_id = MLP.project_Status_type_id AND MLPST.trashed = 0',
                'type' => 'LEFT'
            )
        );
        if(isset($project_ids)){
            $where = 'EP.trashed = 0 AND EP.project_id NOT IN ('.$project_ids.')';
        }else{
            $where = 'EP.trashed = 0';
        }
        $group_by = ('EP.project_id');
        $viewData['ProjectsTable'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);

        //Get The Data for the Expiry Contracts..
        $table = 'employee E';
        $data = ('E.full_name AS EmployeeName,E.employee_id AS EmployeeID, C.contract_id AS ContractID, C.contract_expiry_date AS ContractExpiry');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'contract C',
                'condition' => 'C.employment_id = ET.employment_id AND C.current = 1 AND C.trashed = 0',
                'type' => 'INNER'
            )
        );
        if(isset($expContract_ids)){
            $where = 'E.enrolled = 1 AND E.trashed = 0 AND CURDATE() >= IFNULL(C.date_contract_expiry_alert,C.contract_expiry_date) AND C.contract_id NOT IN ('.$expContract_ids.')';
        }else{
            $where = 'E.enrolled = 1 AND CURDATE() >= IFNULL(C.date_contract_expiry_alert,C.contract_expiry_date) AND E.trashed = 0';
        }
        $group_by = 'E.employee_id';
        $viewData['ContractExpiryTable'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);

        //Start Of Hamid's Code
        //Get Data for Probation Period /////
        $table = 'employee E';
        $data = ('E.full_name AS EmployeeName,E.employee_id AS EmployeeID,C.contract_id AS ContractID,C.prob_alert_dat AS ProbationDate');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'contract C',
                'condition' => 'C.employment_id = ET.employment_id AND C.current = 1 AND C.trashed = 0',
                'type' => 'INNER'
            )
        );
        if(isset($probationExp_ids)){
            $where = 'E.enrolled = 1 AND E.trashed = 0 AND CURDATE() >= C.prob_alert_dat AND C.contract_id NOT IN ('.$probationExp_ids.')';
        }else{
            $where = 'E.enrolled = 1 AND CURDATE() >= C.prob_alert_dat AND E.trashed = 0';
        }
        $group_by = 'E.employee_id';
        $viewData['Probation'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
        //End Of Hamid's Code...


        //echo"<pre>";print_r($viewData['Probation']);die;
        //Get the Data for Exits Table (it shows the records for leavers)
        $table = 'employee E';
        $data = ('E.full_name AS EmployeeName,E.employee_id AS EmployeeID, SM.date AS SeparationDate, SM.seperation_id AS SeparationID');
        $joins = array(
            array(
                'table' => 'seperation_management SM',
                'condition' => 'E.employee_id = SM.employee_id AND SM.status = 2',
                'type' => 'INNER'
            )
        );
        if(isset($separation_ids)){
            $where = 'E.trashed = 0 AND SM.seperation_id NOT IN ('.$separation_ids.')';
        }else{
            $where = 'E.trashed = 0';
        }
        $group_by = '';
        $viewData['ExitsTable'] = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);

//        New Contract and Extensions Table
        $table = 'employee E';
        $selectNewContractExtensions = array(
            'E.full_name AS EmployeeName,E.employee_id AS EmployeeID, IFNULL(ECE.e_start_date,C.contract_start_date) AS ContractExtensionDate, ET.employment_id AS EmploymentID', //CASE WHEN C.extension = 1 THEN IFNULL(ECE.ece_id, C.contract_id ) ELSE C.contract_id END AS ContractExtensionID
            false
        );
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'contract C',
                'condition' => 'C.employment_id = ET.employment_id AND C.current = 1 AND C.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_contract_extensions ECE',
                'condition' => 'ECE.contract_id = C.contract_id AND ECE.trashed = 0 AND ECE.current = 1',
                'type' => 'LEFT'
            )
        );
        if(isset($extContract_ids)){
            $where = ('E.enrolled = 1 AND E.trashed = 0 AND C.employment_id NOT IN ('.$extContract_ids.')');
        }else{
            $where = ('E.enrolled = 1 AND E.trashed = 0');
        }
        $group_by = '';
        $viewData['NewContractExtensions'] = $this->common_model->select_fields_where_like_join($table,$selectNewContractExtensions,$joins,$where,FALSE,'','',$group_by);
        //--Finally End of Joiners Data..

        //Load the Views Now.
        $this->load->view('includes/header');
        $this->load->view('dashboard/index',$viewData);
        $this->load->view('includes/footer');
    }

    public function CreateBackUp()
    {
        $this->load->dbutil();

        //Setting Up Backup Preferences
        $preferences = array(
            'format'        => 'zip',                       // gzip, zip, txt
            'filename'      => 'dbBackup-'.date('d-M-Y').'.sql',              // File name - NEEDED ONLY WITH ZIP FILES
            'newline'       => "\n"                         // Newline character used in backup file
        );

        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup($preferences);

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download('dbBackup-'.date('d-M-Y').'.zip', $backup);

    }
    ////FUNCTION TO SURPRESS SELECTED ALERTS FROM DASHBOARD
    public function suppress_alerts(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $loggedInEmployee = $this->session->userdata('employee_id');
                $table = 'employee_alerts';
                $alertText = $this->input->post('alertText');
                $alertID = $this->input->post('alertID');
                if(isset($alertText) && is_numeric($alertID)){
                    //First We Need To Check If The Same Project and Employee Exist;
                    $data = array(
                        'employee_id' => $loggedInEmployee,
                        'alert_id' => $alertID,
                        'alert_text' => $alertText
                    );
                    $result = $this->common_model->insert_record($table,$data);
                    if($result>0){
                        echo "OK::Alert Successfully suppressed::success";
                    }else{
                        echo "FAIL::Sorry, Alert Cant Not Be Removed::error";
                    }
                }
            }
        }

    }
    ///////////// FUNCTION for chart //////////////////////////////
	public function organization_chart()
	{
        $PTable = 'organization_table O';
        $data = ('O.employee_id,organization_id,parent_organization_id,full_name,employee_order');
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'O.employee_id = E.employee_id',
                'type' => 'INNER'
            )
        );
        $where = array(
            'O.trashed' => '0'
        );
        $result['data'] = $this->common_model->select_fields_where_like_join($PTable, $data,$joins,$where,FALSE);
		$this->load->view('includes/header');
		$this->load->view('dashboard/organization_chart',$result);
		$this->load->view('includes/footer');
	}

    public function get_employeeData()
    {
        $tbl = 'employee';
        $data = ('employee_id,full_name');
        $field = 'full_name';
        $where = array(
            'employee.trashed' => '0'
        );
        if($this->input->post()){
            $searchValue=$this->input->post('queryString');
            $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue);
            if($result){
                $i=1;
                foreach($result as $row):
                    ?><li tabindex=<?php echo $i;?> onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->employee_id ?>')"><?php echo $row->full_name;?></li> <?php
                    //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
                endforeach;
            }
            else{
                $result='';
            }
        }
        else{
            echo "No Value Posted";
        }
    }
    public function select2_employee_get(){

        if($this->input->post()){
            $searchValue = $this->input->post('term');
            $PTable = 'employee E';
            $data = ('E.employee_ID as ID, E.full_name as Text');
            $field = 'E.full_name';
            $joins = array(
                array(
                    'table' => 'organization_table OT',
                    'condition' => 'OT.employee_id = E.employee_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'OT.`employee_id` IS NULL';
            $result = $this->common_model->get_autoCompleteJoin($PTable,$joins,$where,$data,$field,$searchValue);
            print json_encode($result);
        }
    }

    public function select2_get_available_employees(){
        if($this->input->post()){
            $searchValue = $this->input->post('term');
            $PTable = 'organization_table';
            $data = ('organization_id as ID,full_name as Text');
            $joins = array(
                array(
                    'table' => 'employee',
                    'condition' => 'organization_table.employee_id = employee.employee_id',
                    'type' => 'INNER'
                )
            );
            if(!empty($searchValue)){
                $field = 'full_name';
                $result = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,'',FALSE,$field,$searchValue);
                print_r(json_encode($result));
            }
            else{
                $result = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,'');
                print_r(json_encode($result));
            }
        }
    }

    public function update_root_employee(){
        if($this->input->post()){
            $employeeName = $this->input->post('rootEmployee');
            $table = 'organization_table';
            $data = array(
                'employee_id' => $employeeName
            );
            $where = array(
                'parent_organization_id' => '0',
                'organization_id' => '1'
            );
            $this->common_model->update($table,$where,$data);
        }
    }

    public  function insert_organization_data(){
        if($this->input->post()){
            $parentEmployee = $this->input->post('ParentID');
            $parentChildren = $this->input->post('childIDs');
            $tbl = 'organization_table';
            if(!empty($parentChildren)){
                $employees = explode(',', $parentChildren);
                foreach($employees as $employee){
                    $data = array(
                        'parent_organization_id' => $parentEmployee,
                        'employee_id' => $employee,
                        'trashed' => '0',
                        'enabled' => '1'
                    );
                    $result = $this->common_model->insert_record($tbl,$data);
                    echo "OK::Record Successfully Added::Success";
                }
            }
        }
    }

////////////////////////////////////////////////////////////////
	public function add_chart()
	{
		if($this->input->post('add'))
		{
			$add_type		= array(
			'report_to' 	=> $this->input->post('employee_id')
			);		
			$query = $this->site_model->create_new_record('organization_chart', $add_type);
			if($query)
			{
				redirect('dashboard_site/add_chart');
			}
		}
		if($this->input->post('add2'))
		{
			$sub_name =  $this->input->post('Sub_name');
			$counter = 0;
			
			foreach($sub_name as $sub):
			
			$add2 = array(
			'report_to' => $this->input->post('report_to'),
			'Sub_name' => $sub
			);
			$query = $this->site_model->create_new_record('organization_chart', $add2);
			
			$counter ++;
			endforeach;
		}		
		if($this->input->post('add3'))
		{
			$sub_name =  $this->input->post('Child_name');
			$counter = 0;
			
			foreach($sub_name as $sub):
			
			$add3 = array(
			'Sub_name' => $this->input->post('Sub_name'),
			'Sub_child_name' => $sub
			);
			$query = $this->site_model->create_new_record('organization_chart', $add3);
			
			$counter ++;		
			endforeach;
		}
		
		$data['exp'] 	= $this->site_model->dropdown_wd_option('employee', '-- Select Top Name --','employee_id','full_name');	
		$data['name'] 	= $this->site_model->get_all('employee');

        //see if record exists in organization chart for Root.
        $PTable = 'organization_table';
        $OData = ('organization_id,full_name');
        $joins = array(
            array(
                'table' => 'employee',
                'condition' => 'organization_table.employee_id = employee.employee_id',
                'type' => 'INNER'
            )
        );
        $where = array(
            'parent_organization_id' => '0'
        );

/*        $result = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','');
        if($result !== FALSE){
//            $data['OData'] = $result;
        }*/

		$this->load->view('includes/header');	
		$this->load->view('dashboard/add_chart',$data);
		$this->load->view('includes/footer');
	}
	////////////////////////////////////////////////////////
	public function goto_list($param = NULL)
	{
		if($param == "list")
		{
			echo $this->site_model->goto_list();
			die;
		}
		$data['full_name'] = $this->input->post('full_name');
		$data['exp'] = $this->site_model->dropdown_wd_option('employee', '-- Select Employee Name --','employee_id','full_name');
		$data['department_name'] = $this->input->post('department_name');
		$data['status'] = $this->site_model->dropdown_wd_option('ml_department', '-- Select Department --',	'department_id','department_name');
		
		$this->load->view('includes/header');
		$this->load->view('dashboard/goto_list',$data);
		$this->load->view('includes/footer');
	}
	public function add_goto_list()
	{
		if($this->input->post('add'))
		{
			$add_type				= array(
			'employee_id' 			=> $this->input->post('employee_id'),
			'ml_department_id' 		=> $this->input->post('dept_id'),
			'ml_desigination_id' 	=> $this->input->post('desg_id'),
			'resposible' 			=> $this->input->post('resposible')
			);
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('goto_list', $add_type);
			if($query)
			{
				redirect('dashboard_site/goto_list');
			}
		}
			
		$this->load->view('includes/header');
		$this->load->view('dashboard/add_goto_list');
		$this->load->view('includes/footer');
	}
		
	//////////////////////////////////////////////////////////
	public function delete_goto_list($id = NULL)
	{
		$table_id=$this->uri->segment(3);
		
		$data['emer_contact'] = $this->site_model->get_row_by_id('goto_list', array('goto_list_id' => $id));
		//echo "<pre>";print_r($data['emer_contact']);die;
		$query = $this->site_model->delete_row_by_where('goto_list', array('goto_list_id' => $id));
		
		redirect('dashboard_site/goto_list');
		
	}
	///////////////////////////////////////////////////////////////////
	public function edit_goto_list($goto_list_id = NULL)
	{
	 	if($this->uri->segment(3))
		{
			$data['goto_list_id'] = $this->uri->segment(3);
			$where = array('goto_list_id' => $goto_list_id);
			$data['user'] = $this->site_model->get_row_by_id('goto_list', $where);
			$id = $data['user']->employee_id;
			$where_id = array('employee.employee_id' =>$id); 
			$data['data'] = $this->site_model->get_edit_goto_list($where_id);
			//echo "<pre>";print_r($data['data']); die;
			if($this->input->post('edit'))
			{
				$insert 		= array(
				'resposible' => $this->input->post('resposible')); 
				//echo "<pre>";print_r($insert); die;
				$query = $this->site_model->update_record('goto_list', $where, $insert);
				if($query)
				{
					redirect('dashboard_site/goto_list');
				}
			}
		}
		
		$this->load->view('includes/header');
		$this->load->view('dashboard/edit_goto_list', $data);
		$this->load->view('includes/footer');	
	}	
	////////////////////////////////////////////////////////
	public function autocomplete_goto_list()
	{
		$query = $this->site_model->get_autocomplete_goto_list();
	 	// echo "<pre>"; print_r($query); die;
	  	$i=1;
		if(!empty($query)):
      	foreach($query as $row):
		//echo "<pre>"; print_r($row); die;
	  	?><li tabindex="<?php echo $i;?>" onclick="fill('<?php echo $row->full_name;?>','<?php echo $row->ml_designation_id;?>','<?php echo $row->designation_name;?>','<?php echo $row->department_id;?>','<?php echo $row->department_name;?>','<?php echo $row->employee_id;?>')"><?php echo $row->full_name;?></li> <?php
        //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
   		endforeach; 
		endif;   
	}
	////////////////////////////////////////////////////////////////
	public function staff_activity_schedule($param = NULL)
	{
		if($param == "list")
		{
			echo $this->site_model->staff_activity_schedule();
			die;
		}
		
		$data['project_title'] 	= $this->input->post('project_title');
		$data['exp'] 			= $this->site_model->dropdown_wd_option('ml_projects', '-- Select Project --', 'project_id','project_title');
		$data['department_name'] = $this->input->post('department_name');
		$data['status'] 		= $this->site_model->dropdown_wd_option('ml_department', '-- Select Department --',	'department_id','department_name');
		//echo"<pre>";print_r();die;
		$this->load->view('includes/header');
		$this->load->view('dashboard/staff_activity_schedule', $data);
		$this->load->view('includes/footer');
	}	
	//////////////// FUNCTION FOR SKILL INVENTORY ////////////	
	public function skills_inventory_old($param = NULL)
	{
		if($param == "list")
		{
			echo $this->site_model->skills_inventory();
			die;
		}
		
		$data['experience_in_years'] = $this->input->post('experience_in_years');
		$data['exp'] = $this->site_model->dropdown_wd_option('emp_skills', '-- Select Experience --', 'experience_in_years','experience_in_years');
		$data['skill_name'] = $this->input->post('skill_name');
		$data['status'] = $this->site_model->dropdown_wd_option('ml_skill_type', '-- Select Skills --', 'skill_type_id','skill_name');
		
		$this->load->view('includes/header');
		$this->load->view('dashboard/skills_inventory',$data);
		$this->load->view('includes/footer');
	}
    public function skills_inventory($param = NULL)
    {
        $data['experience_in_years'] = $this->input->post('experience_in_years');
        $data['exp'] = $this->site_model->dropdown_wd_option('emp_skills', '-- Select Experience --', 'experience_in_years','experience_in_years');
        $data['skill_name'] = $this->input->post('skill_name');
        $data['status'] = $this->site_model->dropdown_wd_option('ml_skill_type', '-- Select Skills --', 'skill_type_id','skill_name');
        $data['projects'] = $this->site_model->html_selectbox('ml_projects', '-- Select Project --', 'project_id','project_title');

        $this->load->view('includes/header');
        $this->load->view('dashboard/skills_inventory',$data);
        $this->load->view('includes/footer');
    }
    public function skills_inventory_DT(){
        if($this->input->post()){
            $project=$this->uri->segment(3);
            $PTable = 'employee E';
            $DT_Data = ('
            ET.employment_id,
            E.full_name,
            MLD.designation_name,
            GROUP_CONCAT(DISTINCT MLP.project_title) AS Projects,
            GROUP_CONCAT(DISTINCT MLST.skill_name) AS EmployeeSkills');
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'emp_skills ES',
                    'condition' => 'ET.employment_id= ES.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_skill_type MLST',
                    'condition' => 'MLST.skill_type_id = ES.ml_skill_type_id',
                    'type' => 'INNER'
                ),

                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id=ET.`employment_id` AND PM.current=1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLD',
                    'condition' => 'MLD.ml_designation_id=PM.ml_designation_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'assign_job AJ',
                    'condition' => 'AJ.employment_id = ET.employment_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_projects MLP',
                    'condition' => 'MLP.project_id = AJ.project_id',
                    'type' => 'LEFT'
                )
            );
            $group_by = 'E.employee_id';
            $addColumn = array(
                'Actions' => '<a class="view_skills" style="cursor:pointer;cursor:hand;"><span class="fa fa-eye"></span></a>',
                'CheckBoxes' => "<input type='checkbox'>"
            );
            $editColumn = '';

            if($this->input->post('skill_name') || $this->input->post('experience_in_years')){
                $skills = $this->input->post('skill_name');
                $years_of_experience = $this->input->post('experience_in_years');
                if(!empty($skills) && isset($skills) && (empty($years_of_experience) || $years_of_experience == '')){
                    //$where = 'MLST.skill_type_id IN ('.$skills.')';
                    $where = array(
                        'AJ.trashed'=>0,
                        'AJ.project_id'=>$project);
                    $where_in_data = explode(",",$skills);
                    $where_in_column = "MLST.skill_type_id";
                    $result = $this->common_model->select_fields_joined_DT($DT_Data,$PTable,$joins,$where,$where_in_column,$where_in_data,$group_by,$addColumn,$editColumn);
                } elseif(!empty($skills) && isset($skills) && !empty($years_of_experience) && isset($years_of_experience)){
                    $where = array(
                        'AJ.project_id'=>$project,
                        'experience_in_years >=' => $years_of_experience
                    );
                    $where_in_data = explode(",",$skills);
                    $where_in_column = "MLST.skill_type_id";
                    $result = $this->common_model->select_fields_joined_DT($DT_Data,$PTable,$joins,$where,$where_in_column,$where_in_data,$group_by,$addColumn,$editColumn);
                }elseif((empty($skills) || $skills == '') && !empty($years_of_experience) && isset($years_of_experience)){
                    $where = array(
                        'AJ.project_id'=>$project,
                        'experience_in_years >=' => $years_of_experience
                    );
                    $where_in_data = '';
                    $where_in_column = '';
                    $result = $this->common_model->select_fields_joined_DT($DT_Data,$PTable,$joins,$where,$where_in_column,$where_in_data,$group_by,$addColumn,$editColumn);
                }

            }else{
                $where = array(
                    'AJ.trashed'=>0,
                    'AJ.project_id'=>$project);
                $where_in = '';
                $where_in_column ='';
                $result = $this->common_model->select_fields_joined_DT($DT_Data,$PTable,$joins,$where,$where_in_column,$where_in,$group_by,$addColumn);
            }
            print_r($result);
        }
    }
    public function load_all_available_skills(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $project=$this->uri->segment(3);
                $tbl = "ml_skill_type";
                $data = array('skill_type_id AS SkillID','skill_name AS SkillName');
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'skill_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,'',FALSE,$field,$searchValue);
                }else{
                    $result = $this->common_model->select_fields($tbl,$data);
                }
                /*Print the Json Result for All the Available Skills*/
                print_r(json_encode($result));
            }
        }
    }
    public function load_all_available_projects(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $tbl = 'ml_projects';
                $data = array('project_id AS ProjectID, project_title AS ProjectName');
                $where = array(
                    'trashed' => 0
                );
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'project_title';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue);
                }
                else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where,FALSE);
                }
                /*Print the Json Result for All the Available Projects*/
                print_r(json_encode($result));
            }
        }
    }

    public function load_all_available_project_tasks(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $project_id=$this->uri->segment(3);
                $tbl = 'ml_assign_task';
                $data = array('ml_assign_task_id AS TaskID, task_name AS TaskName');
                $where = array(
                    'project_id'=>$project_id,
                    'trashed' => 0
                );
                if($this->input->post('term')){
                    $searchValue = $this->input->post('term');
                    $field = 'task_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$searchValue);
                }
                else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where,FALSE);
                }
                /*Print the Json Result for All the Available Projects*/
                print_r(json_encode($result));
            }
        }
    }

    public function assign_task_to_employees(){
        if($this->input->post()){
           $project_id=$this->uri->segment(3);
            $viewData['project_id']=$this->uri->segment(3);
            $employeeIDs = $this->input->post('employeeIDs');
            $table = 'employee E';
            $data = ('E.full_name, ET.employment_id');
            $join=array(
                array(
                    'table'=>  'employment ET',
                    'condition'=> 'ET.employee_id=E.employee_id',
                    'type'=>       'INNER'
                )
            );
            $where = ('ET.employment_id IN ('.$employeeIDs.')');
            $viewData['employeesData'] = $this->common_model->select_fields_where_like_join($table,$data,$join,$where,FALSE);
//            $viewData['employeesData'] = json_decode(json_encode($viewData['employeesData']),true);
        }/*
        echo "<pre>";
        print_r($viewData);
        echo "</pre>";*/
        $this->load->view('includes/header');
        $this->load->view('dashboard/assign_tasks_selected',$viewData);
        $this->load->view('includes/footer');
    }

    public function assign_task_to_employees_action()
    {   //echo "working";
            $this->db->trans_start();
//            echo $this->input->post('employeeIDs');
            $employment = $this->input->post('employment');
            $project_id = $this->input->post('project_id');
            $selectCurrentTask = $this->input->post('selectCurrentTask');
            $startDate = $this->input->post('start_date');

            $compDate = $this->input->post('comp_date');
            $taskDescription = $this->input->post('taskDescription');

            $milestones = $this->input->post('milestones');
            $estimateDate = $this->input->post('estimate_date');
            $actualDate = $this->input->post('actual_date');
            $remarks = $this->input->post('remarks');


            $employee_explode=explode(',',$employment);

            foreach($employee_explode as $empID) {
                if(empty($empID)){
                    echo "FAIL:: Please Select Employee First !::warning";
                    return;
                }
                if(empty($selectCurrentTask)){
                    echo "FAIL:: Please Select Task First !::warning";
                    return;
                }
                $where_check_task = array(
                    'employment_id' => $empID,
                    'project_id' => $project_id,
                    'ml_assign_task_id' => $selectCurrentTask);
                $check_task_rec = $this->site_model->get_row_by_where('assign_job', $where_check_task);
                if (count($check_task_rec) > 0) {
                    echo "FAIL:: This Task Assigned to " .get_employment_Name_from_employmentID($empID). " Already !::warning";
                    return;
                }
                else
                {

                    $data = array(
                        'employment_id' => $empID,
                        'project_id' => $project_id,
                        'ml_assign_task_id' => $selectCurrentTask,
                        'start_date' => $startDate,
                        'completion_date' => $compDate,
                        'task_description' => $taskDescription,
                        'status' => 'Assigned'
                    );
                    $table = 'assign_job';
                    $result = $this->common_model->insert_record($table, $data);
                    //print_r($data);

                    /*  }*/

                    foreach ($milestones as $mKey => $milestone) {
                        $new_array[$mKey] = array();
                        $subArrayValues = array(
                            'milestones' => $milestone,
                            'estimated_date' => $estimateDate[$mKey],
                            'actual_date' => $actualDate[$mKey],
                            'remarks' => $remarks[$mKey],
                            'employment_id' => $empID,
                            'task_id' => $selectCurrentTask

                        );
                        $table2 = 'milestone';
                        $result = $this->common_model->insert_record($table2, $subArrayValues);


                    }

                }
            }
            $this->db->trans_complete();
            if($this->db->trans_status()=== FALSE)
            {
                $this->db->trans_rollback();
                echo "FAIL:: Task Assigned Successfully ::error";
                return;
            }
            else
            {
                $this->db->trans_commit();
                echo "OK:: Task Assigned Successfully ::success";
                return;
            }

    }
	///////////////////////////////
	public function view_skills($employee_id = NULL)
	{
//		error_reporting(0);
		$data['employee_id'] = $this->uri->segment(3);
		$where = array(
		'employment.employment_id' => $employee_id
		);
		
		$data['complete_profile'] = $this->site_model->view_skills($where);
		//echo "<pre>"; print_r($data['complete_profile']);die;
		$where_emp = array('assign_job.employment_id' => $employee_id);
		$data['job_history'] = $this->site_model->assign_job_history($where_emp); 
		//echo "<pre>"; print_r($data['job_history']);die;
		$this->load->view('includes/header');
        $this->load->view('dashboard/view_skills', $data); 
        $this->load->view('includes/footer');
	}
	//////////////////END OF FUNCTION //////////////////////
	public function trash_log($param = NULL)
	{
		if($param == "list")
		{
			echo $this->site_model->all_trashed();
			die;
		}
		
		$data['record_id'] = $this->input->post('record_id');
		$data['exp'] 		= $this->site_model->dropdown_wd_option('trash_store', '-- Select Trash ID --', 'record_id','record_id');
        //echo "<pre>";print_r($data['re']);die;
		$this->load->view('includes/header');
		$this->load->view('dashboard/trash_log',$data);
		$this->load->view('includes/footer');
	}
	///////////////////////// view single record from trash table ////////////////////
	public function view($trash_store_id = NULL)
	{
		if($this->uri->segment(3))
		{
			$data['trash_store_id'] = $this->uri->segment(3);
			$where = array('trash_store_id' => $trash_store_id);
			$data['user'] = $this->site_model->get_row_by_id('trash_store', $where);
			//echo "<pre>";print_r($data['user']); die;
		}
		
		$this->load->view('includes/header');
		$this->load->view('dashboard/view_table',$data);
		$this->load->view('includes/footer');
	}
	
	//////////////////////delete permanent from trashed ///////////////////////////
	public function delete_parmanent($trash_store_id = NULL)
	{
		if($this->uri->segment(3))
		{
			$data['trash_store_id'] = $this->uri->segment(3);
			$where = array('trash_store_id' => $trash_store_id);
			$data['user'] = $this->site_model->delete_row_by_where('trash_store', $where);
			//echo "<pre>";print_r($data['user']); die;
		}
			redirect('dashboard_site/trash_log');
		
		$this->load->view('includes/header');
		$this->load->view('dashboard/trash_log',$data);
		$this->load->view('includes/footer');
	}
	////////////////////////recover from trashed ////////////////////
	public function recover_trash()
	{
//		error_reporting(0);
		$trash_id=$this->uri->segment(3);
		$table_id=$this->uri->segment(4);
		$this->db->trans_start();
		
		$data['emer_contact'] = $this->site_model->get_row_by_id('trash_store', array('trash_store_id' => $trash_id));
		//echo "<pre>";print_r($data['emer_contact']); die;
		$table = $data['emer_contact']->table_name;
		//echo "<br>";
		$field_name= $data['emer_contact']->table_id_name;
		$where = array($field_name => $table_id);
		// die;
		//$update_trash = array('trashed' => 0);
		
		$query = $this->site_model->update_trash($table,$where,$table_id);
		
		$where_trash_del = array('trash_store_id' => $trash_id);
		$delete=$this->site_model->delete_row_by_where('trash_store' ,$where_trash_del);
		
		$this->db->trans_complete();
		if($query || $delete)
		{
			 redirect('dashboard_site/trash_log');
		}
			 
	}

    ////////////////////// function for view contract extension Approval////////

    public function extend_approval($param = NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }



//        $data['extend'] = $this->site_model->get_extend_contract_app();
        //New Fashion Same Query.
        $table = 'contract C';

        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'C.employment_id = ET.employment_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee E',
                'condition' => 'ET.employee_id = E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_contract_extensions ECE',
                'condition' => 'C.contract_id = ECE.contract_id AND extension = 1',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'ET.employment_id = PM.employement_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'C.status = MLS.status_id',
                'type' => 'LEFT'
            )
        );

        $group_by = 'C.contract_id';
        $where = 'C.status = 1';

        if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
            $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
            $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
            $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';

        }

        if($param === 'list'){
            $selectData = array('
            employee_code,
            full_name,
            designation_name,
            IFNULL(DATE_FORMAT(e_start_date,"%d-%m-%Y"),DATE_FORMAT(contract_start_date,"%d-%m-%Y")) as strt_date,
            IFNULL(DATE_FORMAT(e_end_date,"%d-%m-%Y"),DATE_FORMAT(contract_expiry_date,"%d-%m-%Y")) as end_date,
            status_title,
            C.contract_id AS ContractID,
            E.employee_id AS EmployeeID',false);
            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action','<a href="dashboard_site/action_extend_approval/$1/$2"><button class= "btn green">Action</button></a>','EmployeeID,ContractID'
                )
            );

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }

        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $contractTable = 'contract';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $contractID){
                    $updateData = array(
                        'approved_date' => $currentDate,
                        'status' => 2,
                        'comments' => 'Batch Approved',
                        'approved_by' => $loggedInEmployeeID
                    );
                    $where = array(
                        'contract_id' => $contractID
                    );
                    $updateResult = $this->common_model->update($contractTable,$where,$updateData);
                    if($updateResult === true){
                        $this->common_model->update('employee_contract_extensions',array('contract_id'=> $contractID, 'current'=> 1), array('comments' => 'Batch Approved'));
                    }
                }

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $contractID){
                    $updateData = array(
                        'approved_date' => $currentDate,
                        'status' => 3,
                        'comments' => 'Batch Declined',
                        'approved_by' => $loggedInEmployeeID
                    );
                    $where = array(
                        'contract_id' => $contractID
                    );
                    $updateResult = $this->common_model->update($contractTable,$where,$updateData);
                    if($updateResult === true){
                        $this->common_model->update('employee_contract_extensions',array('contract_id'=> $contractID, 'current'=> 1), array('comments' => 'Batch Declined  '));
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }

            return;
        }


        $data = getApprovalsSideMenus();
        $data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 'status_id', 'status_title');
        $this->load->view('includes/header');
        $this->load->view('dashboard/extend_approved',$data);
        $this->load->view('includes/footer');
    }

    ////////////////////// function for Job Applicants Approvals////////

    public function applicants_approvals()
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        $data = getApprovalsSideMenus();
        //Old Fashion..
//        $data['applicants'] = $this->site_model->get_selected_applicants_app();

        //New Fashion.. Same Query
        $table = 'applicants App';
        $selectData = array('JAdv.referenceNo, App.fullName, JAdv.title,ApJob.dateAppliedOn,ShtInt.shtintr_id',false);
        $joins = array(
            array(
             'table' => 'applicant_applied_jobs ApJob',
                'condition' => 'ApJob.applicantID = App.applicantID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'job_advertisement JAdv',
                'condition' => 'JAdv.advertisementID = ApJob.jobAdvertisementID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'shortlisted_interview ShtInt',
                'condition' => 'ShtInt.applicantID = App.applicantID',
                'type' => 'LEFT'
            )
        );
        $where = 'ShtInt.selected_approval = 1';
        $data['applicants'] = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,FALSE);

        $data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id','status_title');
        $this->load->view('includes/header');
        $this->load->view('dashboard/job_applicants_approved',$data);
        $this->load->view('includes/footer');
    }

    /////////////////////action for contract extend approvals///////////////
    public function action_extend_approval($employee_id = NULL, $contract_id = NULL)
    {
        $data = getApprovalsSideMenus();
        $data['emp'] = $this->site_model->g_extend_contract_app(array('contract.contract_id'=> $contract_id));
        //var_dump($data['rec']);
        $data['data'] = $this->site_model->get_all('ml_status');

        if($this->input->post('add'))
        {
            $add_type= array(
                'approved_by' 			=> $this->session->userdata('employee_id'),
                'approved_date'			=> date('Y-m-d'),
                'comments' 			=> $this->input->post('comments'),
                'status' 			=> $this->input->post('status')
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->update_record('contract',array('contract.contract_id'=> $contract_id), $add_type);
           $update = array('comments'=>$this->input->post('comments'));
            $query2 = $this->site_model->update_record('employee_contract_extensions',array('employee_contract_extensions.contract_id'=> $contract_id), $update);
            if($query || $query2)
            {
                redirect('dashboard_site/extend_approval');
            }
        }
        $this->load->view('includes/header');
        $this->load->view('dashboard/action_extend_approved',$data);
        $this->load->view('includes/footer');
    }

    /////////////////////action for Applicants approvals///////////////
    public function action_applicants_approval($interview_id = NULL)
    {
        $data = getApprovalsSideMenus();
        $data['info'] = $this->site_model->g_selected_applicants_app(array('ShtInt.shtintr_id'=> $interview_id));
        //print_r($data['info']); die;
        $data['data'] = $this->site_model->get_all_by_where('ml_status',array('trashed'=>0));

        if($this->input->post('add'))
        {
            $status=$this->input->post('status');
            $shortlistid=$this->input->post('shortlistid');
            if(isset($status) && $status == 2 && !empty($shortlistid)){
            $applicantID=$this->input->post('applicantid');
            $empResult=$this->site_model->get_row_by_where('applicants',array('applicantID'=>$applicantID));
            $QulfResult=$this->site_model->get_row_by_where('applicant_qualification',array('applicant_id'=>$applicantID));
            $ExpResult=$this->site_model->get_row_by_where('applicant_experience',array('ApplicantID'=>$applicantID));
            $SkillResult=$this->site_model->get_row_by_where('applicant_skills',array('ApplicantID'=>$applicantID));
            //echo $result->firstName; die;

            ////////// Code for employee code /////////////////////////////
            $table = 'employee E';
            $data = array('E.employee_code', false);
            $where = 'E.employee_id = (SELECT MAX(E.employee_id) from employee E where E.trashed = 0)';
            $last_empID = $this->common_model->select_fields_where($table, $data, $where, TRUE);

            if(isset($last_empID) && !empty($last_empID)){
                $data['last_emp_code'] = $last_empID->employee_code;
                //Get the Numeric Value From Employee Code Which Will Be AutoIncremented.
                if(is_numeric($data['last_emp_code'])){
                    $EmpNumericCode = intval($data['last_emp_code']);
                    echo "is Numeric";
                }else{
                    $matches = null;
                    $returnValue = preg_match('/-(?<number>\d+)/', $data['last_emp_code'], $matches);
                    $EmpNumericCode = intval($matches['number']);
                }
                $NewEmpNumericCode = $EmpNumericCode + 1;
                $totalPad = substr_count($this->data['empCodeFormat'], 'x');
                $empCodeFormatStart = substr($data['last_emp_code'], 0, -$totalPad);
                $newEmployeeCode = str_pad($NewEmpNumericCode, $totalPad, 0, STR_PAD_LEFT);
                $data['newFullEmployeeCode'] = $empCodeFormatStart.$newEmployeeCode;
            }

            ///////////////////////////////////////////////////////////////
           $employeeData=array(
               'employee_code'=>$data['newFullEmployeeCode'],
               'first_name'=>$empResult->firstName,
               'last_name'=>$empResult->lastName,
               'full_name'=>$empResult->fullName,
               'father_name'=>$empResult->fatherName,
               'CNIC'=>$empResult->CNIC,
               'gender'=>$empResult->genderID,
               'photograph'=>$empResult->avatar,
               'thumbnail'=>$empResult->avatar
           );
            if(!empty($employeeData)){
                $this->db->trans_begin();
            $query=$this->common_model->insert_record('employee',$employeeData);
                $employee_id=$this->db->insert_id();

            $employmentData=array(
                'employee_id'=>$employee_id,
                'joining_date'=>date("Y-m-d")
            );
            if(!empty($employee_id)){
                $query=$this->common_model->insert_record('employment',$employmentData);
                $employment_id=$this->db->insert_id();
            }

            $employeeQualification=array(
                'employee_id'=>$employee_id,
                'qualification_type_id'=>$QulfResult->qualification_type_id,
                'qualification'=>$QulfResult->title,
                'institute'=>$QulfResult->institute,
                'year'=>$QulfResult->comp_year,
                'gpa_score'=>$QulfResult->gpa_division
            );
            if(!empty($employeeQualification)){
                $query=$this->common_model->insert_record('qualification',$employeeQualification);
            }
            $employeeExperience=array(
                'employee_id'=>$employee_id,
                'job_title'=>$ExpResult->Exp_position,
                'organization'=>$ExpResult->Company,
                'from_date'=>$ExpResult->FromDate,
                'to_date'=>$ExpResult->ToDate,
                'ml_employment_type_id'=>4
            );
            if(!empty($employeeExperience)){
                $query=$this->common_model->insert_record('emp_experience',$employeeExperience);
            }
            $employeeSkill=array(
                'employment_id'=>$employment_id,
                'ml_skill_type_id'=>$SkillResult->SkillID,
                'ml_skill_level_id'=>$SkillResult->skill_level
            );
            if(!empty($employeeSkill)){
                $query=$this->common_model->insert_record('emp_skills',$employeeSkill);
            }
            $employeeAccount=array(
                'employee_id'=>$employee_id,
                'user_name'=>$empResult->username,
                'employee_email'=>$empResult->officialEmailAddress,
                'password'=>$empResult->password,
                'user_group'=>4,
                'acct_creation_date'=>date("Y-m-d")
            );
            if(!empty($employeeAccount)){
                $query=$this->common_model->insert_record('user_account',$employeeAccount);
            }
                $query=$this->common_model->update('shortlisted_interview',array('shtintr_id'=>$shortlistid),array('selected_approval_status'=>$status,'selected_approval'=>2));
                if($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else{
                    $this->db->trans_commit();
                    $messageSubject="Congratulation You have Been Selected for the job You applied!";
                    $messageBody="Dear <br>".$empResult->fullName."</b> You Are Selected For the Job you have applied for furthur process you should visit our site and enter your remaining biodata click link below if link does not work then copy link and past in browser .<br>".base_url()."<br> Your new UserName:".$empResult->officialEmailAddress." <br> password:".$empResult->password." <br>  Thanks .";
                    $config=$this->data['mailConfiguration'];
                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");
                    //Now Lets Send The Email..

                    $this->email->to($empResult->officialEmailAddress);

                    $this->email->from($this->data['noReply'],$this->data['Company']);
                    $this->email->subject($messageSubject);
                    $this->email->message($messageBody);
                    if($this->email->send()) {
//                        echo 'OK::Selection Information has been sent to Applicant::success';
                        $this->db->trans_complete();
                        redirect('dashboard_site/applicants_approvals');
                    } else {
                        $this->email->print_debugger();
//                        echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                        return;
                    }
                }

            }
        }}
        $this->load->view('includes/header');
        $this->load->view('dashboard/action_applicants_approved',$data);
        $this->load->view('includes/footer');
        }


	/////////////////////function for leave entitlement //////////////////

	public function leave_entitlement($param = NULL)
	{
        $loggedInEmployeeID =$this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($this->data['EmployeeID']) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

		if($param == "list")
		{

            //Need To Change it To The New Way..
            //Only Changing the Query Way, Rest Query Will Be As Done by whomever its been Done.
            $approval = $this->input->post('status_title');
            $table = 'leave_entitlement LE';
            $selectData = array('LE.leave_entitlement_id AS ID, E.employee_code,E.full_name,MLDg.designation_name,MLS.status_title,GROUP_CONCAT(DISTINCT LE.leave_entitlement_id separator "_") AS leave_entitlement_id,E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = LE.employment_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_leave_type MLLT',
                    'condition' => 'LE.ml_leave_type_id = MLLT.ml_leave_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'LE.status = MLS.status_id',
                    'type' => 'INNER'
                )
            );
            $group_by = 'ET.employment_id';
            //$group_by = 'LE.leave_entitlement_id';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action','<a href="dashboard_site/action_entitlement_approval/$1/$2"><button class= "btn green">Action</button></a>','leave_entitlement_id,E.employee_id,LE.leave_entitlement_id'
                )
            );

            $where = array('LE.status' => 1,
            'LE.trashed' => 0);
            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id='.$approval;
            }

            //If Is not Admin But is Line Manager Then Only Show Approvals Related To His Subordinates
            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                //Only Get Records Of Employee For Whom He is Assigned AS Supervisor.
                //Get All SubOrdinates Under This Line Manager.
                $reportToTable = 'reporting_heirarchy RH';
                $reportToSelectData = array('RH.reporting_authority_id AS SubordinateID',false);
                $reportToWhere = 'RH.trashed = 0 AND RH.for_employee ='.$loggedInEmployeeID.' AND RH.reporting_authority_id IS NOT NULL';
                $employeeIDs = $this->common_model->select_fields_where($reportToTable,$reportToSelectData,$reportToWhere);
                if(isset($employeeIDs) && !empty($employeeIDs)){
                        $employmentIDs = array();
//                    $employeeIDs = json_decode(json_encode($employeeIDs),true);
                    foreach($employeeIDs as $keyEmpID=>$empID){
                        $employmentID = get_employment_from_employeeID($empID->SubordinateID);
                        $employmentIDs[$empID->SubordinateID] = $employmentID;
                    }
                    $employmentIDsTrimmed = array_map('trim', $employmentIDs);
                    $employmentIDsFiltered = array_filter($employmentIDsTrimmed);
                    $implodedEmploymentIDs = implode(',',$employmentIDsFiltered);
                    $where .= ' AND LE.employment_id IN ('.$implodedEmploymentIDs.')';
                }
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);;
            echo $result;
//            echo $this->db->last_query();
            return;
		}

        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $leaveEntitlementTable = 'leave_entitlement LE';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $LeaveEntitlementID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID
                    );
                    $where = array(
                        'leave_entitlement_id' => $LeaveEntitlementID
                    );
                    $this->common_model->update($leaveEntitlementTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $LeaveEntitlementID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID
                    );
                    $where = array(
                        'leave_entitlement_id' => $LeaveEntitlementID
                    );
                    $this->common_model->update($leaveEntitlementTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }

            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] = $this->input->post('status_title');
        $data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 'status_id', 'status_title');
        $this->load->view('includes/header');
        $this->load->view('dashboard/entitle_approval', $data);
        $this->load->view('includes/footer');
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function action_entitlement_approval($employee_id = NULL)
	{
		if($this->uri->segment(3))
		{

            $ids=$this->uri->segment(3);
            $explode=explode('_',$ids);


            //$array=array();
            if(!empty($explode))
            {
                foreach($explode as $exp){
                    $id=$exp;
                $table = 'leave_entitlement';
                $selectData = 'ml_leave_type.leave_type,leave_entitlement.leave_entitlement_id';
                $joins = array(
                    array(
                        'table' => 'ml_leave_type',
                        'condition' => 'leave_entitlement.ml_leave_type_id = ml_leave_type.ml_leave_type_id',
                        'type' => 'LEFT'
                    )
                );
                $where=array('leave_entitlement.leave_entitlement_id'=>$id);
                $result = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,true);

                    @$type=$result->leave_type;
                   @$leave_id=$result->leave_entitlement_id;
                    $datas=$type.','.$leave_id;
                    //array_push($array,$data);
                    //echo "<br>";
                $info[]=$datas;
                 }

            }
            $inf=implode(',',$info);
           // $data['test']=$inf;
            $info=json_encode($inf);
            $this->session->set_userdata('leaves_data',$info);
        //echo $data['test']; die;

           // echo $data['leave_ids']; die;

            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['employee_id'] = $this->uri->segment(3);
            $data['empl'] = $this->hr_model->get_row_by_where('employment' , array('employee_id'=>$employee_id,'current' =>1,'trashed' =>0));

            $where = array('leave_entitlement_id' => $employee_id);
			$data['emp'] = $this->site_model->get_entitlement_approvals($where);
			//echo "<pre>";print_r($data['emp']); die;
			if($this->input->post('ids'))
			{
               
                $ids=$this->input->post('ids');
                $explode_ids=explode(',',$ids);
                //print_r($explode_ids);die;
                foreach($explode_ids as $id)
                {
                    $where = array('leave_entitlement_id' => $id);

                    $add_type= array(
                        'approved_by' 			=> $this->session->userdata('employee_id'),
                        'date_approved'			=> date('Y-m-d'),
                        'note' 			=> $this->input->post('note'),
                        'status' 			=> $this->input->post('status')
                    );
                    //echo "<pre>"; print_r($add_type); die;
                    $query = $this->site_model->update_record('leave_entitlement',$where, $add_type);

                }
                if($query)
				{
					redirect('dashboard_site/leave_entitlement');
				}
			}
		}
		$data['data1'] = $this->site_model->get_all('ml_status');
		//$id['employee.employee_id'] = $this->uri->segment(4);
        //echo "<pre>";print_r($data['salary']);die;
		$this->load->view('includes/header');
		$this->load->view('dashboard/entitlement_approved',$data);
		$this->load->view('includes/footer');
	}
	/////////////////////function for loan Approval /////////////////
	public function loan_approval($param =NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

		if($param == "list")
		{
            $approval = $this->input->post('status_title');
//			echo $this->site_model->get_loan();
//			die;

            //New Fashion..
            $table = 'employee E';
            $selectData = array('E.employee_code,
            E.full_name,
            MLDg.designation_name,
            MLPT.payment_type_title,
            LAdv.amount,
            MLS.status_title,
            LAdv.loan_advance_id AS LoanAdvanceID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed= 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'loan_advances LAdv',
                    'condition' => 'ET.employment_id = LAdv.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'LAdv.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_payment_type MLPT',
                    'condition' => 'LAdv.payment_type = MLPT.payment_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                )
            );
            $where = 'LAdv.status = 1';
            $group_by = 'LAdv.loan_advance_id';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_loan_approval/$1/$2"><button class= "btn green">Action</button></a>',
                    'LoanAdvanceID,EmployeeID'
                )
            );
            if(isset($approval) && !empty($approval)){
                $where .= 'MLS.status_id ='.$approval;
            }
            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
		}

        if($param === 'batch'){
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $loadAdvanceTable = 'loan_advances';
            $currentData = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray as $loadAdvanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_of_approval'			=> $currentData,
                        'status' 			=> 2,
                        'note' 			=> 'Batch Approval'
                    );
                    $updateWhere = array(
                        'loan_advance_id' => $loadAdvanceID
                    );
                    $updateResult = $this->common_model->update($loadAdvanceTable,$updateWhere,$updateData);

                    if($updateResult === true){
                        $result = $this->common_model->select_fields_where($loadAdvanceTable,'employment_id AS EmploymentID',array('loan_advance_id'=>$loadAdvanceID),TRUE);
                        $insert_data=array(
                            'notification_text'=>'Advance Request Approved',
                            'notification_detail_link'=>'Advance Request',
                            'employment_id'=>$result->EmploymentID,
                            'created_date'=>$currentData
                        );
                        $this->common_model->insert_record('essp_employee_notifications',$insert_data);
                    }

                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray as $loadAdvanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_of_approval'			=> $currentData,
                        'status' 			=> 3,
                        'note' 			=> 'Batch Decline'
                    );
                    $updateWhere = array(
                        'loan_advance_id' => $loadAdvanceID
                    );
                    $updateResult = $this->common_model->update($loadAdvanceTable,$updateWhere,$updateData);

                    if($updateResult === true){
                        $result = $this->common_model->select_fields_where($loadAdvanceTable,'employment_id AS EmploymentID',array('loan_advance_id'=>$loadAdvanceID),TRUE);
                        $insert_data=array(
                            'notification_text'=>'Advance Request Declined',
                            'notification_detail_link'=>'Advance Request',
                            'employment_id'=>$result->EmploymentID,
                            'created_date'=>$currentData
                        );
                        $this->common_model->insert_record('essp_employee_notifications',$insert_data);
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Declining of Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();
		$data['status_title'] = $this->input->post('status_title');
		$data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
		'status_id','status_title');

		$this->load->view('includes/header');
		$this->load->view('dashboard/loan_approval', $data);
		$this->load->view('includes/footer');
	}
	//////////////////////////////////////////////////////////////////////////////////////////

	public function action_loan_approval($loan_advance_id = NULL ,$employee_id = NULL)
	{
		if($this->uri->segment(3))
		{
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['loan_advance_id'] = $this->uri->segment(3);
			$where = array('loan_advance_id' => $loan_advance_id);
			$data['emp'] = $this->site_model->get_loan_approvals($where);
			//echo "<pre>";print_r($data['emp']); die;
			if($this->input->post('add'))
			{					
				$add_type= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'date_of_approval'			=> date('Y-m-d'),
				'note' 			=> $this->input->post('note'),
				'status' 			=> $this->input->post('status')
                );
				//echo "<pre>"; print_r($add_type); die;
				$query = $this->site_model->update_record('loan_advances',$where, $add_type);

                $employment_id=get_employment_from_employeeID($this->uri->segment(4));

                if($this->input->post('status') == 2){
                    $insert_data=array(
                        'notification_text'=>'Advance Request Approved',
                        'notification_detail_link'=>'Advance Request',
                        'employment_id'=>$employment_id,
                        'created_date'=>date("Y-m-d")
                    );
                    $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);
                }
                if($this->input->post('status') == 3){
                    $insert_data=array(
                        'notification_text'=>'Advance Request Declined',
                        'notification_detail_link'=>'Advance Request',
                        'employment_id'=>$employment_id,
                        'created_date'=>date("Y-m-d")
                    );
                    $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);
                }


                if($query)
				{
					redirect('dashboard_site/loan_approval');
				}
			}
		}
		$data['data'] = $this->site_model->get_all('ml_status');
		$id['employee.employee_id'] = $this->uri->segment(4);
		$data['history'] = $this->site_model->get_loan_approvals($id);

		
		$this->load->view('includes/header');
		$this->load->view('dashboard/loan_approved', $data);
		$this->load->view('includes/footer');			
	}



	/////////////// function for posting approvals //////////////////
	
	public function posting_approval($param =NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

		if($param == "list")
		{
/*			echo $this->site_model->get_position();
			die;*/

            $approval = $this->input->post('status_title');
            $PTable = 'position_management PM';
            $selectData = array('
            E.employee_code,
            E.full_name,
            MLDg.designation_name,
            MLPG.pay_grade,
            MLS.status_title,
            PM.position_id AS PositionID,
            ET.employment_id AS EmploymentID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'PM.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_pay_grade MLPG',
                    'condition' => 'PM.ml_pay_grade_id = MLPG.ml_pay_grade_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                )
            );

            $where = 'PM.status = 1 AND PM.current = 1 AND ET.current = 1 AND ET.trashed=0';

            $group_by = 'PM.position_id';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_posting_approval/$1/$2/$3"><button class= "btn green">Action</button></a>',
                    'PositionID,EmploymentID,EmployeeID'
                )
            );


            if(isset($approval) && !empty($approval))
            {
                $where .= ' AND MLS.status_id ='.$approval;
            }

            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                //If Logged In As LineManager, Then Show Only Records From Whom He is Assigned As Line Manager.
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                if(!empty($subOrdinatesEmployeeIDsImploded)){
                    $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
                }
            }
            $result =$this->common_model->select_fields_joined_DT($selectData,$PTable,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;

		}


        if($param === 'batch'){
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $positionTable = 'position_management';
            $currentData = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray as $AllowanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'approval_date'			=> $currentData,
                        'status' 			=> 2,
                        'note' 			=> 'Batch Approval'
                    );
                    $updateWhere = array(
                        'position_id' => $AllowanceID
                    );
                    $this->common_model->update($positionTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray as $AllowanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'approval_date'			=> $currentData,
                        'status' 			=> 3,
                        'note' 			=> 'Batch Decline'
                    );
                    $updateWhere = array(
                        'position_id' => $AllowanceID
                    );
                    $this->common_model->update($positionTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Declining of Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();
		$data['status_title'] 		= $this->input->post('status_title');
		$data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 
		'status_id','status_title');
		$where = array('status' => 1);
		$this->load->view('includes/header');
		$this->load->view('dashboard/position_approval', $data);
		$this->load->view('includes/footer');
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	public function action_posting_approval($position = NULL)
	{
		if($this->uri->segment(3))
		{
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['employee_id'] = $this->uri->segment(4);
			$employment_id = $this->uri->segment(4);
			$where = array('position_id' => $position);
			$where = array('position_management.employement_id' => $employment_id);
			$data['emp'] = $this->site_model->get_position_approvals($where);
			$data['current_position'] = $this->site_model->get_current_position($where);
			//echo "<pre>";print_r($data['emp']); die;
            $where2 = array('employee_id' => $this->uri->segment(5),'employment.current'=>1);

            $data['employment'] = $this->hr_model->get_row_by_where('employment', $where2);
           // var_dump($data['employment']);die;
             $employement_id = $data['employment']->employment_id;
            $where = array('employement_id' =>$this->uri->segment(4));
            $data['ddd'] = $this->site_model->get_2nd_last_id_where($where,'position_management','position_id');
            @$snd_last_id = $data['ddd']->position_id;

			if($this->input->post('add'))
			{
				
				$add_type= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'approval_date'			=> date('Y-m-d'),
				'note' 			=> $this->input->post('note'),
				'status' 			=> $this->input->post('status')
                );
				//echo "<pre>"; print_r($add_type); die;
				$query = $this->site_model->update_record('position_management',$where, $add_type);
                if (!empty($data['employment']->employment_id) || !(NULL)) {
                    $data = array(
                        'current' => '0'
                    );
                    $table = 'position_management';
                    $where = array(
                        'employement_id' => $employement_id
                    );
                    //print_r($data);die;
                   $query2= $this->common_model->update_query_array($table, $where, $data);
                    $where3 = array(
                        'position_id' => $this->uri->segment(3)
                    );
                    $data= array('current' => 1);
                        $query3 =$this->hr_model->update_record('position_management',$where3,$data);
                    $where4 = array(
                        'position_id' => $snd_last_id
                    );
                    $query4 =$this->hr_model->update_record('position_management',$where4,array('to_date'=>date("Y-m-d")));

                }
				if($query || $query2 || $query3)
				{
					redirect('dashboard_site/posting_approval');
				}
			}
		}
		$data['data'] = $this->site_model->get_all('ml_status');
		$id['employment_id'] = $this->uri->segment(4);
		$data['history'] = $this->site_model->get_position_approvals($id);
		///$data['record'] = $this->site_model->get_transcation_history_repeat($id);
		//echo "<pre>"; print_r($data['history']); die;

		//echo "<pre>";print_r($data['salary']);die;
		$this->load->view('includes/header');
		$this->load->view('dashboard/posting_approved', $data);
		$this->load->view('includes/footer');
	}
	////////////////function for transaction approval ///////////////
	
	public function approvals($param = NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];

        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();


        //Main Query Code...
        //Old Way
//        $data['info'] = $this->site_model->get_transaction();

        //New Way..
        //Result Would Be the Same No Change, Only Moving Code to Controller, Execution will still be done in Model.
        $table = 'employee E';
        $selectData = array('
        (T.transaction_id) as trans_id,
        E.employee_code,
		E.full_name,
		MLDg.designation_name,
		MLTT.transaction_type,
		T.transaction_amount,
		MLS.status_title,
		T.transaction_id,
		ET.employment_id,
		E.employee_id,
		(T.transaction_type) as id,
		SP.month AS salary_month,
		SP.year,
		SP.salary_payment_id

        ',false);
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id = ET.employee_id  AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'transaction T',
                'condition' => 'ET.employment_id  = T.employment_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'salary_payment SP',
                'condition' => 'SP.transaction_id = T.transaction_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'ET.employment_id = PM.employement_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_transaction_types MLTT',
                'condition' => 'T.transaction_type = MLTT.ml_transaction_type_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'T.status = MLS.status_id',
                'type' => 'LEFT'
            )
        );
        $where = 'T.status = 1 AND ET.current = 1 AND E.enrolled = 1 AND E.trashed = 0';
        $transaction_type=$this->input->post('transaction_type');
        if(isset($transaction_type) && !empty($transaction_type))
        {
            $where = 'T.transaction_type = '.$transaction_type.' AND T.status = 1 AND ET.current = 1 AND E.enrolled = 1 AND E.trashed = 0';
        }
        if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
            //Means If User is Not A Administrator But is A Line Manager, Then Do Stuff For It..
           $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
            $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
            $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
        }
        $order_by = array('T.transaction_id','desc');
        $group_by = 'T.transaction_id';


/*        if($param === 'list'){
            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            $edit_column = array(
                array(  'leave_approval_id',
                    '<a href="dashboard_site/action_transcation_approval/$1/$2/$3"><button class= "btn green">Action</button></a>',
                    'trans_id,employeeID,id')
            );
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }*/


        $data['info'] = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,FALSE,'','',$group_by,$order_by,'');

        //End Of Query Code..
		$data['status_title'] 		= $this->input->post('status_title');
		$data['type'] 		= $this->input->post('transaction_type');
		$data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 'status_id','status_title');
		$data['transaction_type'] 				= $this->site_model->dropdown_wd_option('ml_transaction_types', '-- Select Type --', 'ml_transaction_type_id','transaction_type');

		$this->load->view('includes/header');
		$this->load->view('dashboard/approvals', $data);
		$this->load->view('includes/footer');
	}
    public function EosApprovals()
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();
        $data['status_title'] 	= $this->input->post('status_title');
        $data['status'] 		= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --','status_id','status_title');
        //OLD FASHION..
//        $data['info_eos'] = $this->site_model->get_eos_transcation();
//        echo $this->db->last_query();
        //New FASHION SAME OLD QUERY
        $approval = $this->input->post('status_title');
        $selectData = array('(T.transaction_id) as trans_id,E.employee_code,
		E.full_name,
		MLDg.designation_name,
		MLTT.transaction_type,
		T.transaction_amount,
		MLS.status_title,
		T.transaction_id,
		ET.employment_id,
		E.employee_id,
		(T.transaction_type) as id,SP.month as salary_month,SP.year as salary_year,eos.eos_id',false);
        $table = 'employee E';
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id = ET.employee_id  AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'eos',
                'condition' => 'eos.employment_id = ET.employment_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'transaction T',
                'condition' => 'ET.employment_id  = T.employment_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'salary_payment SP',
                'condition' => 'SP.transaction_id=T.transaction_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'ET.employment_id = PM.employement_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_transaction_types MLTT',
                'condition' => 'T.transaction_type = MLTT.ml_transaction_type_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'T.status = MLS.status_id',
                'type' => 'LEFT'
            )
        );
        $where = 'T.status = 1 AND ET.current = 0';
        $group_by = 'T.transaction_id';
        if (isset($approval) && !empty($approval)) {
            $where .= ' AND MLS.status_id ='.$approval;
        }

        if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
            $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
            $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
            $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
        }
        $data['info_eos'] = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,FALSE,'','',$group_by);

        $this->load->view('includes/header');
        $this->load->view('dashboard/eos_approvals',$data);
        $this->load->view('includes/footer');
    }
	public function action_transcation_approval($transaction_id = NULL ,$employee_id = NULL, $id = NULL)
	{
		if($this->uri->segment(3))
		{
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['transaction_id'] = $this->uri->segment(3);
			$where = array('transaction.transaction_id' => $transaction_id);
			$id = array('transaction.transaction_type' => $this->uri->segment(5),'transaction.status !='=>1);
			$data['emp'] = $this->site_model->get_transcation_approval($where);
			//echo "<pre>";print_r($data['emp']); 
			if($this->input->post('add'))
			{
				$add_type= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'approval_date'			=> date('Y-m-d'),
				'note' 			=> $this->input->post('note'),
				'status' 			=> $this->input->post('status')
                );
				//echo "<pre>"; print_r($add_type); die;
				$query = $this->site_model->update_record('transaction',$where, $add_type);
				if($query)
				{
				redirect('dashboard_site/approvals');
				}
			}
		}
		$data['data'] = $this->site_model->get_all('ml_status');
		$id['employee.employee_id'] = $this->uri->segment(4);
		$where = array('employee.employee_id' =>$this->uri->segment(4),'transaction.status !=' =>1);
		$data['history'] = $this->site_model->get_transcation_history($where);
		$data['record'] = $this->site_model->get_transcation_history_repeat($id);
		//echo "<pre>";print_r($data['history']); die;
		//echo "<pre>"; print_r($data['record']); die;

//        $data['separation'] = $this->site_model->employee_table_count('employee','seperation_management','employee.employee_id=seperation_management.employee_id',array('seperation_management.status'=>1,'employee.enrolled'=>1));
//        $data['deduction'] = $this->site_model->employee_table_count('employee','deduction','employee.employee_id=deduction.employee_id',array('deduction.status'=>1,'employee.enrolled'=>1));
//        $data['posting'] = $this->site_model->employee_table_count('employment','posting','employment.employment_id=posting.employement_id',array('posting.status'=>1,'employment.current'=>1));
//        $data['expense_claims'] = $this->site_model->employee_table_count('employee','expense_claims','employee.employee_id=expense_claims.employee_id',array('expense_claims.status'=>1,'employee.enrolled'=>1));
//        $data['entitlement'] = $this->site_model->employee_table_count('employee','leave_entitlement','employee.employee_id=leave_entitlement.employee_id',array('leave_entitlement.status'=>1,'employee.enrolled'=>1));
//        $data['position'] = $this->site_model->employee_table_count('employment','position_management','employment.employment_id=position_management.employement_id',array('position_management.status'=>1,'employment.current'=>1));
//        $data['count'] = $this->site_model->employee_table_count('employee','transaction','employee.employee_id=transaction.employee_id',array('transaction.status'=>1,'employee.enrolled'=>1));
//        $data['benefits'] = $this->site_model->employee_table_count('employee','benefits','employee.employee_id=benefits.employee_id',array('benefits.status'=>1,'employee.enrolled'=>1));
//        $data['loan'] = $this->site_model->employee_table_count('employee','loan_advances','employee.employee_id=loan_advances.employee_id',array('loan_advances.status'=>1,'employee.enrolled'=>1));
//        $data['allowance'] = $this->site_model->employee_table_count('employee','allowance','employee.employee_id=allowance.employee_id',array('allowance.status'=>1,'employee.enrolled'=>1));
//        $data['increments'] = $this->site_model->employee_table_count('employee','increments','employee.employee_id=increments.employee_id',array('increments.status'=>1,'employee.enrolled'=>1));
//        $data['leave_approval'] = $this->site_model->employee_table_count('employee','leave_approval','employee.employee_id=leave_approval.employee_id',array('leave_approval.status'=>1,'employee.enrolled'=>1));
//        $data['salary'] = $this->site_model->employee_table_count('employment','salary','employment.employment_id=salary.employement_id',array('salary.status'=>1,'employment.current'=>1));

        $this->load->view('includes/header');
		$this->load->view('dashboard/trans_approved', $data);
		$this->load->view('includes/footer');
	}
    public function action_transaction_review()
{
    //Get The Data for Left Side Menus..
    $query = getApprovalsSideMenus();
    $id=$this->uri->segment(3);
    $month=$this->uri->segment(4);
    $year=$this->uri->segment(5);
    $where=array(
        'employment.employment_id'=>$id,
        'salary_payment.month'=>$month,
        'salary_payment.year'=>$year
    );

    $query['salary_info']=$this->site_model->month_salaries($where);
    //print_r($query['salary_info']); die;
    //var_dump($query['salary_info']);
    $salary_trans_date=$query['salary_info']->trans_date;
    $query['ded_trans']=$this->site_model->payroll_regded_trans($id,$month,$year,$salary_trans_date);
    $query['allw_trans']=$this->site_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
    $query['exp_trans']=$this->site_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
    $query['advance_trans']=$this->site_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);
    $query['details']=$this->site_model->payroll_reg_details($where,$month,$year,$salary_trans_date);
    $where_enable=array('enabled'=>1,
        'trashed'=>0);
    $query['status']=$this->site_model->
    html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where_enable);
    /* $where_emp=array(
        'employee.employee_id'=>$id
     );*/
    // $query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);

    $this->load->view('includes/header');
    $this->load->view('dashboard/trans_approval_review',$query);
    $this->load->view('includes/footer');
}
    public function action_eos_transaction_review()
    {
        //Get The Data for Left Side Menus..
        $query = getApprovalsSideMenus();
        $id=$this->uri->segment(3);
        $query['employment_id']=$this->uri->segment(3);
        $month=$this->uri->segment(4);
        $year=$this->uri->segment(5);
        $where=array(
            'employment.employment_id'=>$id,
            'salary_payment.month'=>$month,
            'salary_payment.year'=>$year
        );

        $query['salary_info']=$this->site_model->month_salaries($where);
        //print_r($query['salary_info']); die;
        //var_dump($query['salary_info']);
        $salary_trans_date=$query['salary_info']->trans_date;
        $query['ded_trans']=$this->site_model->eos_ded_trans($id,$month,$year,$salary_trans_date);
        $query['ded_refund']=$this->site_model->eos_refundDed_trans($id);
        $query['allw_trans']=$this->site_model->payroll_regallw_trans($id,$month,$year,$salary_trans_date);
        $query['exp_trans']=$this->site_model->payroll_regexp_trans($id,$month,$year,$salary_trans_date);
        $query['advance_trans']=$this->site_model->payroll_regadvance_trans($id,$month,$year,$salary_trans_date);
        $query['details']=$this->site_model->eos_salary_details($where,$month,$year,$salary_trans_date);
        $where_enable=array('enabled'=>1,
            'trashed'=>0);
        $query['status']=$this->site_model->
        html_selectbox('ml_status','-- Select Status Type --','status_id','status_title',$where_enable);
        /* $where_emp=array(
            'employee.employee_id'=>$id
         );*/
        // $query['salaries']=$this->payroll_model->year_salaries($where_emp,$year);

        $this->load->view('includes/header');
        $this->load->view('dashboard/eos_trans_approval_review',$query);
        $this->load->view('includes/footer');
    }

    public function update_transaction_status()
    {
        if($this->input->post('status')==3)
        {
            $alw_trans_id=$this->input->post('alw_trans_id');
            $ded_trans_id=$this->input->post('ded_trans_id');
            $adv_trans_id=$this->input->post('adv_trans_id');
            $exp_trans_id=$this->input->post('exp_trans_id');

            $exp_id=$this->input->post('exp_ids');
            $adv_id=$this->input->post('adv_ids');


            $salary_payment_id=$this->input->post('salary_payment_id');
            $remarks=$this->input->post('remarks');
            $transaction_id=$this->uri->segment(3);
            //$where=array('transaction.transaction_id'=>$transaction_id);
            $review_data= array(
                'reviewed_by' 			=> $this->session->userdata('employee_id'),
                'review_date'			=> date('Y-m-d'),
                'salary_payment_id' 	=> $salary_payment_id,
                'remarks'               => $remarks);
            $transaction_data=array('status' => $this->input->post('status'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->fail_review($review_data,$transaction_data,$transaction_id,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id,$exp_id,$adv_id);
            if($query)
            {
                redirect('dashboard_site/approvals');
                return;
            }
        }
        elseif($this->input->post('status')==2)
        {
            $alw_trans_id=$this->input->post('alw_trans_id');
            $ded_trans_id=$this->input->post('ded_trans_id');
            $adv_trans_id=$this->input->post('adv_trans_id');
            $exp_trans_id=$this->input->post('exp_trans_id');
            $salary_tran_id=$this->uri->segment(3);
            $approval_data= array(
                'approved_by' 			=> $this->session->userdata('employee_id'),
                'approval_date'			=> date('Y-m-d'),
                'status' 			    => $this->input->post('status'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->salary_transaction_approval($salary_tran_id,$approval_data,$alw_trans_id,$ded_trans_id,$adv_trans_id,$exp_trans_id);
            if($query)
            {
                redirect('dashboard_site/approvals');
                return;
            }
        }

    }
    public function update_eos_transaction_status()
    {

        if($this->input->post('status')==3)
        {
            $salary_transaction_id=$this->uri->segment(3);
            $ded_transaction_id=$this->input->post('ded_trans_id');
            $alw_trans_id=$this->input->post('alw_trans_id');
            //$ded_trans_id=$this->input->post('ded_trans_id');
            $adv_trans_id=$this->input->post('adv_trans_id');
            $exp_trans_id=$this->input->post('exp_trans_id');

            $exp_id=$this->input->post('exp_ids');
            $adv_id=$this->input->post('adv_ids');


            $salary_payment_id=$this->input->post('salary_payment_id');
            $remarks=$this->input->post('remarks');
            //$transaction_id=$this->uri->segment(3);
            //$where=array('transaction.transaction_id'=>$transaction_id);
            $review_data= array(
                'reviewed_by' 			=> $this->session->userdata('employee_id'),
                'review_date'			=> date('Y-m-d'),
                'salary_payment_id' 	=> $salary_payment_id,
                'remarks'               => $remarks);
            $transaction_data=array('status' => $this->input->post('status'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->fail_review($review_data,$transaction_data,$salary_transaction_id,$alw_trans_id,$ded_transaction_id,$adv_trans_id,$exp_trans_id,$exp_id,$adv_id);
            if($query)
            {
                redirect('dashboard_site/approvals');
                return;
            }
        }
        elseif($this->input->post('status')== 2)
        {
            $alw_trans_id=$this->input->post('alw_trans_id');
            $ded_transaction_id=$this->input->post('ded_trans_id');
            //$ded_trans_id=$this->input->post('ded_trans_id');
            $adv_trans_id=$this->input->post('adv_trans_id');
            $exp_trans_id=$this->input->post('exp_trans_id');
            $employment_id=$this->input->post('employment_id');
            $salary_tran_id=$this->uri->segment(3);
            $approval_data= array(
                'approved_by' 			=> get_employment_from_employeeID($this->session->userdata('employee_id')),
                'approval_date'			=> date('Y-m-d'),
                'status' 			    => $this->input->post('status'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->eos_transaction_approval($employment_id,$salary_tran_id,$approval_data,$alw_trans_id,$ded_transaction_id,$adv_trans_id,$exp_trans_id);
            if($query)
            {
                redirect('dashboard_site/approvals');
                return;
            }
        }

    }
	////////////////END OF FUNCTION ///////////////////////////////////////
	////////////////function for Leave Approval//////////////////
	public function leave_approval($param= NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        if($param == "list")
		{
//			echo $this->site_model->leave_approval();
//			die;
            $approval = $this->input->post('status_title');
            //New Way. Same Query.
            $table = 'employee E';
            $selectData = array('E.employee_code AS EmployeeCode,
            E.full_name AS EmployeeName,
            MLDg.designation_name AS EmployeeDesignation,
            leave_type AS LeaveType,
            date_format(LAPP.from_date,"%d-%m-%Y") AS LeaveFromDate,
            date_format(LAPP.to_date,"%d-%m-%Y") AS LeaveToDate,
            LAPP.total_days AS TotalDays,
            MLS.status_title AS Status,
            LA.leave_approval_id AS LeaveApprovalID,
            LA.leave_approval_id AS ID,
            LAPP.application_id,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_approval LA',
                    'condition' => 'LA.employment_id = ET.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_application LAPP',
                    'condition' => 'LA.leave_application_id = LAPP.application_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_leave_type MLLT',
                    'condition' => 'MLLT.ml_leave_type_id = LAPP.ml_leave_type_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'LA.status = MLS.status_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'LA.status = 1 AND ET.current = 1 AND E.trashed = 0 AND E.enrolled = 1';
            $group_by = 'LAPP.application_id';

            $add_column = array(
                    'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            $edit_column = array(
              array(  'leave_approval_id',
                  '<a href="dashboard_site/action_leave_approval/$1/$2"><button class= "btn green">Action</button></a>',
                  'LeaveApprovalID,EmployeeID')
            );


            if(isset($approval) && !empty($approval))
            {
                $where .= ' AND MLS.status_id ='.$approval;
            }

            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                //If Logged In As LineManager, Then Show Only Records From Whom He is Assigned As Line Manager.
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                if(!empty($subOrdinatesEmployeeIDsImploded)){
                    $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
                }
            }
            $result =$this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
		}

        if($param === 'batch'){

            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            if($batchType === 'approval'){

                //As We Got The IDs Of The Applications To Approve. So Approve Them..
                $leaveApprovalTable = 'leave_approval';
                $leaveApplicationTable = 'leave_application LAPP';
                $this->db->trans_start();
                foreach($IDsArray as $leaveApprovalID){
                    //First We Need To Find Out the Application Details like From Date To Date etc.

                    $selectData = array('from_date, to_date, total_days',false);
                    $joins = array(
                        array(
                            'table' => 'leave_approval LA',
                            'condition' => 'LA.leave_application_id = LAPP.application_id',
                            'type' => 'INNER'
                        )
                    );
                    $whereApplication = array(
                        'LA.leave_approval_id' => $leaveApprovalID
                    );
                    $applicationDetails = $this->common_model->select_fields_where_like_join($leaveApplicationTable,$selectData,$joins,$whereApplication,TRUE);

                    if(isset($applicationDetails) and !empty($applicationDetails)){
                        //Now As We Get The Application Details, We Can Get the Values Needed For Approval From Application..

                        $LeaveFrom = $applicationDetails->from_date;
                        $LeaveTo = $applicationDetails->to_date;
                        $paidDays = $applicationDetails->total_days;


                        //Update the Stuff Now.
                        $updateData = array(
                            'approved_by' => $loggedInEmployeeID,
                            'approval_date' => $this->data['dbCurrentDate'],
                            'status' => 2,
                            'remarks' => 'Batch Approved',
                            'approved_from' => $LeaveFrom,
                            'approved_to' => $LeaveTo,
                            'no_of_paid_days' => $paidDays,
                            'no_of_un_paid_days' => 0
                        );
                        $whereUpdate = array(
                            'leave_approval_id' => $leaveApprovalID
                        );
                        $updateResult = $this->common_model->update($leaveApprovalTable,$whereUpdate,$updateData);
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Database Error Occurred During Approvals, If you feel its a bug or problem in system, Please Contact System Administrator::error';
                    return;
                }else{
                    echo "OK::Leave Applications Successfully Approved::success";
                    return;
                }
            }elseif($batchType === 'decline'){
                $declineReason = $this->input->post('remarks');
                //As We Got The IDs Of The Applications To Approve. So Approve Them..
                $leaveApprovalTable = 'leave_approval';
                $leaveApplicationTable = 'leave_application LAPP';
                $failReviews = 'fail_reviews';
                $dateZeroes = '0000-00-00';
                $currentDate = $this->data['dbCurrentDate'];
                $this->db->trans_start();
                foreach($IDsArray as $leaveApprovalID){

                    //Update the Stuff Now.
                    $updateData = array(
                        'approved_by' => $loggedInEmployeeID,
                        'approval_date' => $currentDate,
                        'status' => 3,
                        'remarks' => $declineReason,
                        'approved_from' => $dateZeroes,
                        'approved_to' => $dateZeroes,
                        'no_of_paid_days' => 0,
                        'no_of_un_paid_days' => 0
                    );
                    $whereUpdate = array(
                        'leave_approval_id' => $leaveApprovalID
                    );
                    $updateResult = $this->common_model->update($leaveApprovalTable,$whereUpdate,$updateData);
                    if($updateResult !== true){
                        echo 'Some problem';
                    }
                    //But we Also Need To Do Insertion in To Fail-Reviews According to Approvals Flow.
                    $insertData = array(
                      'review_date' => $currentDate,
                        'reviewed_by' =>$loggedInEmployeeID,
                        'remarks' => $declineReason,
                        'status' => 3,
                        'fail_table' => 'leave_approval',
                        'fail_id' => $leaveApprovalID
                    );
                    $this->common_model->insert_record($failReviews,$insertData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Database Error Occurred During Approvals, If you feel its a bug or problem in system, Please Contact System Administrator::error';
                    return;
                }else{
                    echo "OK::Leave Applications Successfully <b>Declined</b>::success";
                    return;
                }
            }

            echo 'batch approval';
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

		$data['status_title'] 		= $this->input->post('status_title');
		$data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 
		'status_id','status_title');

        $this->load->view('includes/header');
		$this->load->view('dashboard/leave_approvals',$data);
		$this->load->view('includes/footer');
	}
	///////////////////function for leave approval /////////////////////
	public function action_leave_approval($leave_Approval = NULL, $employee_id = NULL)
	{
		if($leave_Approval != NULL && is_numeric($leave_Approval))
		{
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();
			$data['employee_id'] = $employee_id;
			$where = array('leave_approval_id' => $leave_Approval);
            $table = 'employee E';
            $empData = array('E.employee_code,E.full_name,
		    MLLT.leave_type,
		    MLS.status_title,
		    LA.leave_approval_id,
		    LAPP.from_date AS ApplicationFromDate,
		    LAPP.to_date AS ApplicationToDate,
		    LAPP.from_time AS ApplicationFromTime,
		    LAPP.to_time AS ApplicationToTime,
		    LAPP.total_days AS TotalDays',false);
            $empJoins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_application LAPP',
                    'condition' => 'LAPP.employment_id = ET.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_approval LA',
                    'condition' => 'LA.employment_id = ET.employment_id AND LAPP.application_id = LA.leave_application_id AND LA.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_entitlement LE',
                    'condition' => 'LE.employment_id = ET.employment_id AND LE.trashed = 0 AND LE.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_leave_type MLLT',
                    'condition' => 'MLLT.ml_leave_type_id = LAPP.ml_leave_type_id AND MLLT.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'MLS.status_id = LA.status AND MLS.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $empWhere = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'LA.status' => 1,
                'E.employee_id' => $employee_id,
                'LA.leave_approval_id' => $leave_Approval
            );
            $data['emp'] = $this->common_model->select_fields_where_like_join($table,$empData,$empJoins,$empWhere,TRUE);
         //print_r($data['emp']); die;
            //If Approved Button Was Pressed..
            $post=$this->input->post("approved");

			if($post == 1)
			{


                $allowfrom = $this->input->post('allowFromDate');
                $allowto = $this->input->post('allowToDate');
                $fromDate= $this->input->post('fromDate');
               $toDate = $this->input->post('toDate');
                $totalDay = $this->input->post('totalDay');
                if(isset($allowfrom) && !empty($allowfrom)){
                    $allowfrom1 = date('Y-m-d',strtotime($allowfrom));
                }
                if(isset($allowto) && !empty($allowto)){
                    $allowto1 = date('Y-m-d',strtotime($allowto));
                }
                    if(!empty($allowfrom1) && !empty($allowto1)){
                        $add_type1 = array(
                        'approved_by' => $this->session->userdata('employee_id'),
                        'approval_date' => date('Y-m-d'),
                        'approved_from' => $allowfrom1,
                        'approved_to' => $allowto1,
                        'no_of_paid_days' => $this->input->post('paidDays'),
                        'no_of_un_paid_days' => $this->input->post('unPaidDays'),
                        'status' => 2,
                        'remarks' => $this->input->post('approvedRemarks'));
                        $query = $this->site_model->update_record('leave_approval',$where, $add_type1);
                    }else{
                        if(isset($data['emp']) && !empty($data['emp'])){
                            $add_type1 = array(
                                'approved_by' => $this->session->userdata('employee_id'),
                                'approval_date' => date('Y-m-d'),
                                'approved_from' => $data['emp']->ApplicationFromDate,
                                'approved_to' => $data['emp']->ApplicationToDate,
                                'no_of_paid_days' => $data['emp']->TotalDays,
                                'no_of_un_paid_days' => 0,
                                'status' => 2,
                                'remarks' => $this->input->post('approvedRemarks'));
                            $query = $this->site_model->update_record('leave_approval',$where, $add_type1);
                        }
                        else{
                            $msg = "Could Not Approve, Some Problem Occured In System, Please Contact System Administrator For Further Assistance.::success";
                            $this->session->set_flashdata('msg',$msg);
                            redirect('dashboard_site/leave_approval');
                        }
                    }


                $employment_id=get_employment_from_employeeID($this->uri->segment(4));

                    $insert_data=array(
                        'notification_text'=>'Leave Application Approved',
                        'notification_detail_link'=>'Leave Application',
                        'employment_id'=>$employment_id,
                        'created_date'=>date("Y-m-d")
                    );
                    $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);

                if($query)
				{
                    $msg = "Record Successfully Approved::success";
                    $this->session->set_flashdata('msg',$msg);
					redirect('dashboard_site/leave_approval');
				}
			}elseif($this->input->post('declinedRemarks')){

                $this->db->trans_start();
                //First We Need To Update The Record Inside the LeaveApproval..
                $updateTable = 'leave_approval';

                $updateData = array(
                    'approved_by' => $this->session->userdata('employee_id'),
                    'approval_date' => date('Y-m-d'),
                    'approved_from' => '0000-00-00',
                    'approved_to' => '0000-00-00',
                    'no_of_paid_days' => 0,
                    'no_of_un_paid_days' => 0,
                    'status' => 3,
                    'remarks' => $this->input->post('declinedRemarks')
                );

                $updateWhere = array(
                    'leave_approval_id' => $leave_Approval
                );
                $updateResult = $this->common_model->update($updateTable,$updateWhere,$updateData);
                //Now After Update I will Do The Insertion Query.
                $insertTable = 'fail_reviews';
                $insertData = array(
                    'review_date' => $this->data['dbCurrentDate'],
                    'reviewed_by' => $this->data['EmployeeID'],
                    'remarks' => $this->input->post('declinedRemarks'),
                    'status' => 3,
                    'fail_table' => 'leave_approval',
                    'fail_id' => $leave_Approval
                );
                $resultInsert = $this->common_model->insert_record($insertTable,$insertData);
                if($this->input->post('status') == 3){
                    $employment_id=get_employment_from_employeeID($this->uri->segment(4));
                    $insert_data=array(
                        'notification_text'=>'Leave Application Declined',
                        'notification_detail_link'=>'Leave Application',
                        'employment_id'=>$employment_id,
                        'created_date'=>date("Y-m-d")
                    );
                    $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);
                }
                $this->db->trans_complete();
                if($this->db->trans_status() === FALSE){
                    $msg = "Record Could Not Be Declined, Some Database Error::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('dashboard_site/leave_approval');
                }else{
                    $msg = "Record Successfully Declined::success";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('dashboard_site/leave_approval');
                }
            }

            $monthYear = $data['emp']->ApplicationFromDate;
            //print_r($monthYear); die;
		}

        //Copied Haider Code By Izhar...

        //Need To Get Month and Year of Employee.
        if (isset($monthYear) && !empty($monthYear)) {
            $monthYearData = explode("-", $monthYear);
            $year = $monthYearData[0];
            $month = $monthYearData[1];
            $date = $monthYearData[2];
            $data['complete_profile'] = $this->site_model->leave_enetitlement_detail($year,$employee_id);
            //print_r($data['complete_profile']); die;
        }

        //End OF Copied Code By Izhar...

		$data['data'] = $this->site_model->get_all('ml_status');
		$where_id = array('employee.employee_id' => $employee_id);
		$data['history'] = $this->site_model->get_leave_approval($where_id);

        $this->load->view('includes/header');
		$this->load->view('dashboard/approved', $data);
		$this->load->view('includes/footer');
	}
	////////////////END OF FUNCTION ///////////////////////////////////////
	////////////////function for allowance Approval//////////////////
	public function allowances_approvals($param= NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

		if($param === "list")
		{
//			echo $this->site_model->allowances_approvals();
//			die;
            $approval = $this->input->post('status_title');
            //New Way, Same Query As Before.
            $table = 'employee E';
            $selectData = array('A.allowance_id AS ID, E.employee_code AS EmployeeCode,
            E.full_name AS EmployeeName,
            MLDg.designation_name AS EmployeeDesignation,
            MLAT.allowance_type AS AllowanceType,
            A.allowance_amount AS AllowanceAmount,
            MLS.status_title AS Status,
            A.allowance_id AS AllowanceID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'allowance A',
                    'condition' => 'A.employment_id = ET.employment_id AND A.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_allowance_type MLAT',
                    'condition' => 'A.ml_allowance_type_id = MLAT.ml_allowance_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'A.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                )
            );
            $where = 'A.status = 1 AND ET.current = 1 AND E.enrolled = 1';
            $group_by = 'A.allowance_id';
            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            $edit_column = array(
                array('Action',
				'<a href="dashboard_site/action_allowances_approvals/$1/$2"><button class= "btn green">Action</button></a>',
				'AllowanceID,EmployeeID')
            );

            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id = '.$approval;
            }

            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }

            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
		}


        if($param === 'batch'){
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $allowanceTable = 'allowance';
            $currentData = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray as $AllowanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_approved'			=> $currentData,
                        'status' 			=> 2,
                        'note' 			=> 'Batch Approval'
                    );
                    $updateWhere = array(
                        'allowance_id' => $AllowanceID
                    );
                    $this->common_model->update($allowanceTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray as $AllowanceID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_approved'			=> $currentData,
                        'status' 			=> 3,
                        'note' 			=> 'Batch Decline'
                    );
                    $updateWhere = array(
                        'allowance_id' => $AllowanceID
                    );
                    $this->common_model->update($allowanceTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    echo 'FAIL::Some Problem Occurred During Declining of Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

		$data['status_title'] 		= $this->input->post('status_title');
		$data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 
		'status_id','status_title');

        $this->load->view('includes/header');
		$this->load->view('dashboard/allowances_approvals',$data);
		$this->load->view('includes/footer');
	}
	///////////////////function for leave approval /////////////////////
	public function action_allowances_approvals($employee_id = NULL)
	{
		if($this->uri->segment(3))
		{
			$data['employee_id'] = $this->uri->segment(3);
            $data['empl'] = $this->site_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
			$where = array('allowance_id' => $employee_id);

            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['emp'] = $this->site_model->get_allowances_approvals($where);
			//echo "<pre>";print_r($data['data']); die;
			if($this->input->post('add'))
			{
				$add_type= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'date_approved'			=> date('Y-m-d'),
				'status' 			=> $this->input->post('status'),
				'note' 			=> $this->input->post('note'));
				//echo "<pre>"; print_r($add_type); die;
				$query = $this->site_model->update_record('allowance',$where, $add_type);
				if($query)
				{
					redirect('dashboard_site/allowances_approvals');
				}
			}
		}
		$where_id = array('allowance.employment_id' => $this->uri->segment(4),'allowance.status !='=>1);
		$data['history'] = $this->site_model->get_allowances_approvals_repeat($where_id);
		//echo "<pre>"; print_r($data['history']); die;
		$data['data'] = $this->site_model->get_all('ml_status');

        $this->load->view('includes/header');
		$this->load->view('dashboard/allowance_approved', $data);
		$this->load->view('includes/footer');
	}
	////////////////END OF FUNCTION ///////////////////////////////////////
	////////////////////////////function for benefit approvals /////////////////////////
	public function benefits_approvals($param= NULL)
	{
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

		if($param == "list")
		{
            //OLD FASHION
//			echo $this->site_model->benefits_approvals();
//			die;
            $approval = $this->input->post('status_title');
            //NEW FASHION, SAME QUERY
            $table = 'employee E';
            $selectData = array('B.emp_benefit_id AS ID,
            E.employee_code AS EmployeeCode,
            E.full_name AS EmployeeName,
            MLDg.designation_name AS EmployeeDesignation,
		MLBT.benefit_type_title AS BenefitType,
		MLS.status_title AS Status,
		B.emp_benefit_id AS BenefitID,
		E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'benefits B',
                    'condition' => 'B.employment_id = ET.employment_id AND B.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'B.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_benefit_type MLBT',
                    'condition' => 'B.benefit_type_id = MLBT.benefit_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'B.status = 1 AND ET.current = 1 AND E.enrolled = 1';
            $group_by = 'B.emp_benefit_id';
            $addColumn = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_benefits_approvals/$1/$2"><button class= "btn green">Action</button></a>',
                    'BenefitID,EmployeeID'
                )
            );

            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id ='.$approval;
            }

            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                if(!empty($subOrdinatesEmployeeIDsImploded)){
                    $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
                }
            }

            $result = $this->common_model->select_fields_joined_DT($selectData, $table, $joins, $where, '','',$group_by,$addColumn,$edit_column);
            echo $result;
            return;
		}
        $currentDate = $this->data['dbCurrentDate'];
        if($param === 'batch'){
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $benefitsTable = 'benefits';
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray as $empBenefitID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_approved'			=> $currentDate,
                        'note' 			=> 'Batch Approval',
                        'status' 			=> 2
                    );
                    $updateWhere = array(
                        'emp_benefit_id' => $empBenefitID
                    );
                    $this->common_model->update($benefitsTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray as $empBenefitID){
                    $updateData = array(
                        'approved_by' 			=> $loggedInEmployeeID,
                        'date_approved'			=> $currentDate,
                        'note' 			=> 'Batch Declining',
                        'status' 			=> 3
                    );
                    $updateWhere = array(
                        'emp_benefit_id' => $empBenefitID
                    );
                    $this->common_model->update($benefitsTable,$updateWhere,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Request::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

		$data['status_title'] = $this->input->post('status_title');
		$data['status'] 	  = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 
			'status_id','status_title');
        $this->load->view('includes/header');
		$this->load->view('dashboard/benefits_approvals',$data);
		$this->load->view('includes/footer');
	}
	///////////////////function for benefit approval /////////////////////
	public function action_benefits_approvals($employee_id = NULL)
	{
		if($this->uri->segment(3))
		{
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

			$data['employee_id'] = $this->uri->segment(3);
			$data['id']= $this->uri->segment(4);
            $data['empl'] = $this->site_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;
			$where = array('emp_benefit_id' => $employee_id);
			$data['emp'] = $this->site_model->get_benefits_approvals($where);
			//echo "<pre>";print_r($data['emp']); die;
			if($this->input->post('add'))
			{
				$add_type= array(
				'approved_by' 			=> $this->session->userdata('employee_id'),
				'date_approved'			=> date('Y-m-d'),
				'note' 			=> $this->input->post('note'),
				'status' 			=> $this->input->post('status')
                );
				//echo "<pre>"; print_r($add_type); die;
				$query = $this->site_model->update_record('benefits',$where, $add_type);
				if($query)
				{
					redirect('dashboard_site/benefits_approvals');
				}
			}
		}
			//$where_g = array('en' => $this->uri->segment(4));
			$data['history'] = $this->site_model->get_benefits($where);
			//echo "<pre>";print_r($data['history']);die;
			$data['data'] = $this->site_model->get_all('ml_status');

//        $data['separation'] = $this->site_model->employee_table_count('employee','seperation_management','employee.employee_id=seperation_management.employee_id',array('seperation_management.status'=>1,'employee.enrolled'=>1));
//        $data['deduction'] = $this->site_model->employee_table_count('employee','deduction','employee.employee_id=deduction.employee_id',array('deduction.status'=>1,'employee.enrolled'=>1));
//        $data['posting'] = $this->site_model->employee_table_count('employment','posting','employment.employment_id=posting.employement_id',array('posting.status'=>1,'employment.current'=>1));
//        $data['expense_claims'] = $this->site_model->employee_table_count('employee','expense_claims','employee.employee_id=expense_claims.employee_id',array('expense_claims.status'=>1,'employee.enrolled'=>1));
//        $data['entitlement'] = $this->site_model->employee_table_count('employee','leave_entitlement','employee.employee_id=leave_entitlement.employee_id',array('leave_entitlement.status'=>1,'employee.enrolled'=>1));
//        $data['position'] = $this->site_model->employee_table_count('employment','position_management','employment.employment_id=position_management.employement_id',array('position_management.status'=>1,'employment.current'=>1));
//        $data['count'] = $this->site_model->employee_table_count('employee','transaction','employee.employee_id=transaction.employee_id',array('transaction.status'=>1,'employee.enrolled'=>1));
//        $data['benefits'] = $this->site_model->employee_table_count('employee','benefits','employee.employee_id=benefits.employee_id',array('benefits.status'=>1,'employee.enrolled'=>1));
//        $data['loan'] = $this->site_model->employee_table_count('employee','loan_advances','employee.employee_id=loan_advances.employee_id',array('loan_advances.status'=>1,'employee.enrolled'=>1));
//        $data['allowance'] = $this->site_model->employee_table_count('employee','allowance','employee.employee_id=allowance.employee_id',array('allowance.status'=>1,'employee.enrolled'=>1));
//        $data['increments'] = $this->site_model->employee_table_count('employee','increments','employee.employee_id=increments.employee_id',array('increments.status'=>1,'employee.enrolled'=>1));
//        $data['leave_approval'] = $this->site_model->employee_table_count('employee','leave_approval','employee.employee_id=leave_approval.employee_id',array('leave_approval.status'=>1,'employee.enrolled'=>1));
//        $data['salary'] = $this->site_model->employee_table_count('employment','salary','employment.employment_id=salary.employement_id',array('salary.status'=>1,'employment.current'=>1));

        $this->load->view('includes/header');
			$this->load->view('dashboard/benefit_approved', $data);
			$this->load->view('includes/footer');
	}
	////////////////END OF FUNCTION ///////////////////////////////////////
	////////////////////////////function for salary approvals /////////////////////////
    public function salary_approvals($param = NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if (!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)) {
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg', $msg);
            redirect(previousURL());
        }

        if ($param == "list") {
            //Old Fashion Deprecated.
//            echo $this->site_model->salary_approvals();
//            die;
            $approval = $this->input->post('status_title');
            //New Fashion, Same Query..
            $table = 'salary S';
            $selectData = array('S.salary_id AS ID, E.employee_code AS EmployeeCode,
            E.full_name AS EmployeeName,
            MLDg.designation_name AS EmployeeDesignation,
            S.base_salary AS BaseSalary,
            MLS.status_title AS Status,
            S.salary_id AS SalaryID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = S.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'S.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                )
            );
            $where = 'S.status = 1 AND E.enrolled = 1 AND ET.current = 1';
            $group_by = '';
            $addColumn = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            $edit_column = array(
                array(
                'Action',
                '<a href="dashboard_site/action_salary_approvals/$1"><button class= "btn green">Action</button></a>',
                'SalaryID'
                )
            );

            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id = '.$approval;
            }

            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table, $joins,$where,'','',$group_by,$addColumn,$edit_column);
            echo $result;
            return;
        }

        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $salaryTable = 'salary';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $salaryID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Approved'
                    );
                    $where = array(
                        'salary_id' => $salaryID
                    );
                    $this->common_model->update($salaryTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $salaryID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Decline'
                    );
                    $where = array(
                        'salary_id' => $salaryID
                    );
                    $this->common_model->update($salaryTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] = $this->input->post('status_title');
        $data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id', 'status_title');
        $where = array('status' => 1);
        $this->load->view('includes/header');
        $this->load->view('dashboard/salary_approvals', $data);
        $this->load->view('includes/footer');
    }
//////////////////////////////////////////////////////////////////////////////////////////
	public function action_salary_approvals($employee_id = NULL)
	{
		if($this->uri->segment(3))
			{
                //Get The Data for Left Side Menus..
                $data = getApprovalsSideMenus();

			$data['employee_id'] = $this->uri->segment(3);
			$where = array('salary_id' => $employee_id);
			//$where_h = "status != 1 " ;
			$data['emp'] = $this->site_model->get_salary_approvals($where);
			//echo "<pre>";print_r($data['emp']); die;
			if($this->input->post('add'))
			{
				
			$add_type= array(
			'approved_by' 			=> $this->session->userdata('employee_id'),
			'date_approved'			=> date('Y-m-d'),
			'note' 			=> $this->input->post('note'),
			'status' 			=> $this->input->post('status')
            );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->update_record('salary',$where, $add_type);
				if($query)
				{
				redirect('dashboard_site/salary_approvals');
				}
			}
			}
			$data['data'] = $this->site_model->get_all('ml_status');
			$data['history'] = $this->site_model->get_row_by_where('salary',$where);

            $this->load->view('includes/header');
			$this->load->view('dashboard/salary_approved', $data);
			$this->load->view('includes/footer');
			
	}
	
	
////////////////////////////function for Increment approvals /////////////////////////


	public function increments_approvals($param= NULL)
	{
        //Restrict Un Authorized Access..
        $loggedInEmployee = $this->data['EmployeeID'];
        if(!is_method_allowed($loggedInEmployee, 'approvals', 'view') && !is_admin($loggedInEmployee) && !is_lineManager($loggedInEmployee)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        if ($param == "list") {
            //New Fashion Same Query.
            $approval = $this->input->post('status_title');
            $table = 'increments I';
            $selectData = array('E.employee_code,
            E.full_name AS EmployeeName,
            MLDg.designation_name,
            increment_amount,
            MLIT.increment_type,
            MLS.status_title,
            I.increment_id AS IncrementID,
            E.employee_id AS EmployeeID', false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'I.employment_id = ET.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_increment_type MLIT',
                    'condition' => 'I.increment_type_id = MLIT.increment_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'I.status = MLS.status_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'I.status = 1 AND I.trashed = 0 AND ET.current = 1';
            $group_by = 'I.increment_id';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_increment_approvals/$1/$2"><button class= "btn green">Action</button></a>',
                    'IncrementID,EmployeeID'
                )
            );
            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id = '.$approval;
            }
            if(is_lineManager($loggedInEmployee) && !is_admin($loggedInEmployee)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployee);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                if(!empty($subOrdinatesEmployeeIDsImploded)){
                    $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
                }
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }

        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $incrementsTable = 'increments I';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $IncrementID){
                    $updateData = array(
                        'approval_date' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployee,
                        'note' => 'Batch Approved'
                    );
                    $where = array(
                        'increment_id' => $IncrementID
                    );
                    $this->common_model->update($incrementsTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $IncrementID){
                    $updateData = array(
                        'approval_date' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployee,
                        'note' => 'Batch Decline'
                    );
                    $where = array(
                        'increment_id' => $IncrementID
                    );
                    $this->common_model->update($incrementsTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

			$data['status_title'] 		= $this->input->post('status_title');
			$data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 
			'status_id','status_title');

        $this->load->view('includes/header');
			$this->load->view('dashboard/increments_approvals',$data);
			$this->load->view('includes/footer');
	}
//////////////////////////////////////////////////////////////////////////////////////////
 public function action_increment_approvals($increment_id = NULL , $employee_id = NULL)
	{
		if($this->uri->segment(3))
			{
                //Get The Data for Left Side Menus..
                $data = getApprovalsSideMenus();

			 $data['increment_id'] = $this->uri->segment(3);
			 $data['employee_id'] = $this->uri->segment(4);
			 $where = array('increment_id' => $increment_id);
                $data['empl'] = $this->site_model->get_row_by_where('employment',array('employee_id' => $employee_id,'current'=> 1,'trashed'=>0));
// print_r($data['empl']);die;

            $id = array('increments.employment_id' =>  $data['empl']->employment_id, 'increments.status !=' => 1);
			//$where_h = "status != 1 " ;
			$data['emp'] = $this->site_model->get_increment_approvals($where);
			$data['rec'] = $this->site_model->get_increment_repeat($id);
			//echo "<pre>";print_r($data['data']); die;
			if($this->input->post('add'))
			{
				
			$add_type= array(
			'approved_by' 			=> $this->session->userdata('employee_id'),
			'approval_date'			=> date('Y-m-d'),
			'note' 			=> $this->input->post('note'),
			'status' 			=> $this->input->post('status')
            );
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->update_record('increments',$where, $add_type);
                if ($query) {
                    redirect('dashboard_site/increments_approvals');
                }
			}
			}
			$data['data'] = $this->site_model->get_all('ml_status');
			$where_g = array('increments.employment_id' =>  $data['empl']->employment_id);
			$data['history'] = $this->site_model->get_increment_approvals_repeat($where);

            $this->load->view('includes/header');
			$this->load->view('dashboard/increment_approved', $data);
			$this->load->view('includes/footer');
	}
	
	 // Function For Phone Book By Izhar Ali Khan //
	public function phone_book($param= NULL)
    {
        if ($param == "list") {
            echo $this->site_model->phone_book();
            die;
        }

        $this->load->view('includes/header');
        $this->load->view('dashboard/phone_book');
        $this->load->view('includes/footer');
    }


    public function action_separation_retir($seperation_id = NULL , $employee_id = NULL)
    {
        if(isset($seperation_id) && $seperation_id !== NULL)
        {
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

            $data['seperation_id'] = $seperation_id;
            $where = array('seperation_management.seperation_id' => $seperation_id);
//            $data['emp'] = $this->site_model->get_separation_retir($where);

            //We Need to get employee and separation details.
            $PTable = 'seperation_management SM';
            $selectData = ('E.full_name as EmployeeName, MLDg.designation_name AS Designation,E.employee_code AS EmployeeCode, MLST.separation_type AS SeparationType, MLPRL.posting_reason AS PostingReason');
            $joins = array(
                array(
                    'table' => 'employee E',
                    'condition' => 'SM.employee_id = E.employee_id AND E.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id = ET.employment_id AND PM.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_separation_type MLST',
                    'condition' => 'SM.ml_seperation_type = MLST.separation_type_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_posting_reason_list MLPRL',
                    'condition' => 'SM.reason = MLPRL.posting_reason_list_id',
                    'type' => 'LEFT'
                )

            );
            $where = array(
                'SM.trashed' => 0,
                'SM.employee_id' => $employee_id,
                'SM.seperation_id' => $seperation_id
            );
            $data['emp'] = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where,TRUE);

            if(!isset($data['emp']) || empty($data['emp']) || $data['emp'] == false){
                $msg = "Can Not Retire Employee, Employee is Not Available For Retirement::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('dashboard_site/retirment_separation_approval','location');
                return;
            }
            if($this->input->post('updateApprovalStatus'))
            {
                $this->db->trans_start();
                $approvalStatus = $this->input->post('status');
                $SMTable = 'seperation_management';
                $SMUpdateWhere = array(
                        'seperation_id' => $seperation_id
                );
                if(isset($approvalStatus) && is_numeric($approvalStatus) && $approvalStatus == 2){
                    $updateSeparationData= array(
                        'approved_by' 			=> $this->session->userdata('employee_id'),
                        'approved_date'			=> date('Y-m-d'),
                        'status' 			=> $approvalStatus,
                        'rejoin' => 0
                    );
                }elseif(is_numeric($approvalStatus) && $approvalStatus == 1){
                    $updateSeparationData= array(
                        'approved_by' 			=> $this->session->userdata('employee_id'),
                        'approved_date'			=> date('Y-m-d'),
                        'status' 			=> $approvalStatus
                    );
                }
                $updateSeperationResult = $this->common_model->update($SMTable,$SMUpdateWhere,$updateSeparationData);
                if($approvalStatus == 2){
                    //Now if Retirement/Resignations is Approved We need to do some changes.
                    //We First Need Current EmploymentID for the Employee..

                    $ETTable = 'employment ET';
                    $selectETData = 'ET.employment_id AS EmploymentID';
                    $where = array(
                        'ET.current' => 1,
                        'ET.employee_id' => $employee_id,
                        'ET.trashed' => 0
                    );
                    $selectETResult = $this->common_model->select_fields_where($ETTable,$selectETData,$where,TRUE);
                    $employmentID = $selectETResult->EmploymentID;
                    //Updating Position_Management Table.
                    $PMTable = 'position_management';
                    $updatePositionData = array(
                        'current' => 0
                    );
                    $where = array(
                        'employement_id' => $employmentID,
                        'current' => 1,
                        'trashed' => 0,
                        'status' => 2
                    );
                    $PMResult = $this->common_model->update($PMTable,$where,$updatePositionData);

                    //Updating Posting Table
                    $PostingTable = 'posting';
                    $updatePostingData = array(
                        'current' => 0,
                    );
                    $PostingWhere = array(
                        'current' => 1,
                        'trashed' => 0,
                        'employement_id' => $employmentID,
                        'status' => 2
                    );
                    $PostingResult = $this->common_model->update($PostingTable,$PostingWhere,$updatePostingData);

                    //Updating Employee
                    $EmployeeTable = 'employee';
                    $EmployeeData = array(
                        'enrolled' => 0
                    );
                    $EmployeeWhere = array(
                        'employee_id' => $employee_id
                    );
                    $EmployeeResult = $this->common_model->update($EmployeeTable,$EmployeeWhere,$EmployeeData);

                    //Updating Employment
                    $EmploymentTable = 'employment';
                    $EmploymentData = array(
                        'current' => 0
                    );
                    $EmploymentWhere = array(
                        'employee_id' => $employee_id,
                        'current' => 1,
                        'trashed' => 0
                    );
                    $EmploymentResult = $this->common_model->update($EmploymentTable,$EmploymentWhere,$EmploymentData);
                    $this->db->trans_complete();
                    if($this->db->trans_status() !== FALSE && $PMResult == true && $PostingResult == true && $EmployeeResult == true && $EmploymentResult == true){
                        redirect('dashboard_site/retirment_separation_approval','location');
                    }else{
                        $msg = "Could Not Retire The Employee, Some Database Error, Please Contact Administrator:error";
                        $this->session->set_flashdata('msg',$msg);
                    }
                    /////////////////////Projects Dont Has the Current Column, i guess we dont need to disable employee Projects.
                }

            }
        }
        $data['statusTypes'] = $this->site_model->get_all('ml_status');
        $id['employee.employee_id'] = $employee_id;

        $this->load->view('includes/header');
        $this->load->view('dashboard/retired_action', $data);
        $this->load->view('includes/footer');
    }

    public function retirment_separation_approval($param = NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        $PTable = 'seperation_management SM';
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = SM.employee_id AND E.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'PM.employement_id = ET.employment_id AND PM.current = 1 AND PM.status = 2',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_status MLS',
                'condition' => 'MLS.status_id = SM.status',
                'type' => 'LEFT'
            )
        );

        $where = 'SM.trashed = 0';
        $group_by = 'SM.seperation_id';

        if ($param == "list") {
            $approved = $this->input->post('status_title');
            if(isset($approved) && !empty($approved)){
                $where .=' AND SM.status ='.$approved;
            }else{
                $where .=' AND SM.status = 1';
            }
            $data = array('
                E.employee_code,
                E.full_name,
                MLDg.designation_name,
                MLS.status_title AS `Status`,
                SM.seperation_id AS SeparationID,
                E.employee_id AS EmployeeID', false);

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $editColumn = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_separation_retir/$1/$2"><button class= "btn green">Action</button></a>',
                    'SeparationID,EmployeeID'
                )
            );

            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }
            $resultDT = $this->common_model->select_fields_joined_DT($data,$PTable,$joins,$where,'','',$group_by,$add_column,$editColumn);
            print_r($resultDT);
            return;
        }

        $where2 = array('trashed' => 0);

        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $separationTable = 'seperation_management SM';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $separationID){
                    $updateData = array(
                        'approved_date' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Approved  '
                    );
                    $where = array(
                        'seperation_id' => $separationID
                    );
                    $updateResult = $this->common_model->update($separationTable,$where,$updateData);

                    if($updateResult === true){
                        $result = $this->common_model->select_fields_where($separationTable,'employee_id AS EmployeeID',array('seperation_id' => $separationID),TRUE);
                        //Now if Retirement/Resignations is Approved We need to do some changes.
                        //We First Need Current EmploymentID for the Employee..
                        $employeeID = $result->EmployeeID;
                        $ETTable = 'employment ET';
                        $selectETData = 'ET.employment_id AS EmploymentID';
                        $where = array(
                            'ET.current' => 1,
                            'ET.employee_id' => $employeeID,
                            'ET.trashed' => 0
                        );
                        $selectETResult = $this->common_model->select_fields_where($ETTable,$selectETData,$where,TRUE);
                        $employmentID = $selectETResult->EmploymentID;
                        //Updating Position_Management Table.
                        $PMTable = 'position_management';
                        $updatePositionData = array(
                            'current' => 0
                        );
                        $where = array(
                            'employement_id' => $employmentID,
                            'current' => 1,
                            'trashed' => 0,
                            'status' => 2
                        );
                        $this->common_model->update($PMTable,$where,$updatePositionData);

                        //Updating Posting Table
                        $PostingTable = 'posting';
                        $updatePostingData = array(
                            'current' => 0,
                        );
                        $PostingWhere = array(
                            'current' => 1,
                            'trashed' => 0,
                            'employement_id' => $employmentID,
                            'status' => 2
                        );
                        $this->common_model->update($PostingTable,$PostingWhere,$updatePostingData);

                        //Updating Employee
                        $EmployeeTable = 'employee';
                        $EmployeeData = array(
                            'enrolled' => 0
                        );
                        $EmployeeWhere = array(
                            'employee_id' => $employeeID
                        );
                        $this->common_model->update($EmployeeTable,$EmployeeWhere,$EmployeeData);

                        //Updating Employment
                        $EmploymentTable = 'employment';
                        $EmploymentData = array(
                            'current' => 0
                        );
                        $EmploymentWhere = array(
                            'employee_id' => $employeeID,
                            'current' => 1,
                            'trashed' => 0
                        );
                        $this->common_model->update($EmploymentTable,$EmploymentWhere,$EmploymentData);
                        /////////////////////Projects Don't Has the Current Column, i guess we Don't need to disable employee Projects.
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $separationID){
                    $updateData = array(
                        'approved_date' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Declined'
                    );
                    $where = array(
                        'seperation_id' => $separationID
                    );
                    $this->common_model->update($separationTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] 		= $this->input->post('status_title');
        $data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id','status_title',$where2);

        $this->load->view('includes/header');
        $this->load->view('dashboard/retired_app', $data);
        $this->load->view('includes/footer');
    }

    public function posting_approval_new($param =NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        if($param == "list")
        {
//            echo $this->site_model->posting();
//            die;

            //New Fashion Old Query
            $approval = $this->input->post('status_title');
            $table = 'posting P';
            $selectData = array('E.employee_code,
            E.full_name,
            MLDg.designation_name,
            MLB.branch_name,
            MLD.department_name,
            MLC.city_name,
            MLPLL.posting_location,
            MLSft.shift_name,
            MLS.status_title,
            P.posting_id AS PostingID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = P.employement_id AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_branch MLB',
                    'condition' => 'P.ml_branch_id = MLB.branch_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_department MLD',
                    'condition' => 'P.ml_department_id = MLD.department_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_city MLC',
                    'condition' => 'P.ml_city_id = MLC.city_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_shift MLSft',
                    'condition' => 'P.shift = MLSft.shift_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_posting_location_list MLPLL',
                    'condition' => 'P.location = MLPLL.posting_locations_list_id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'P.status = MLS.status_id',
                    'type' => 'LEFT'
                )
            );
            $where = 'P.status = 1 AND E.enrolled = 1 AND P.trashed = 0 AND P.current = 1';
            $group_by = '';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_posting_new/$1/$2"><button class= "btn green">Action</button></a>',
                    'PostingID,EmployeeID'
                )
            );
            if(isset($approval) && !empty($approval)){
                $where .= ' AND MLS.status_id ='.$approval;
            }
            if(is_lineManager($loggedInEmployeeID) && !is_admin($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                if(!empty($subOrdinatesEmployeeIDsImploded)){
                    $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
                }
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }




        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $incrementsTable = 'posting P';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $PostingID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID,
                        'transfer_remarks' => 'Batch Approved  '
                    );
                    $where = array(
                        'posting_id' => $PostingID
                    );
                    $this->common_model->update($incrementsTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $PostingID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID,
                        'transfer_remarks' => 'Batch Declined'
                    );
                    $where = array(
                        'posting_id' => $PostingID
                    );
                    $this->common_model->update($incrementsTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] 		= $this->input->post('status_title');
        $data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id','status_title');

        $this->load->view('includes/header');
        $this->load->view('dashboard/posting', $data);
        $this->load->view('includes/footer');
    }

    public function action_posting_new($posting_id = NULL , $employee_id = NULL)
    {
        if($this->uri->segment(3))
        {
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

            $data['posting_id'] = $this->uri->segment(3);
            $where = array('posting.posting_id' => $posting_id);
            $data['emp'] = $this->site_model->get_posting_get($where);
            // echo "<pre>";print_r($data['emp']); die;
            if($this->input->post('add'))
            {
                $add_type= array(
                    'approved_by' 			=> $this->session->userdata('employee_id'),
                    'date_approved'			=> date('Y-m-d'),
                    'transfer_remarks' 			=> $this->input->post('transfer_remarks'),
                    'status' 			=> $this->input->post('status')
                );
                //echo "<pre>"; print_r($add_type); die;
                $query = $this->site_model->update_record('posting',$where, $add_type);
                if($query)
                {
                    redirect('dashboard_site/posting_approval_new');
                }
            }
        }

        $data['data'] = $this->site_model->get_all('ml_status');
        $id['employee.employee_id'] = $this->uri->segment(4);

        $this->load->view('includes/header');
        $this->load->view('dashboard/posting_action_new', $data);
        $this->load->view('includes/footer');
    }

    public function deductions($param =NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        if($param == "list")
        {

            //New FASHION SAME QUERY
            $approval = $this->input->post('status_title');
            $table = 'employee E';
            $selectData = array('E.employee_code,
            E.full_name,
            MLDg.designation_name,
            MLDT.deduction_type,
            D.deduction_amount,
            MLS.status_title,
            D.deduction_id AS DeductionID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'deduction D',
                    'condition' => 'ET.employment_id  = D.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_deduction_type MLDT',
                    'condition' => 'D.ml_deduction_type_id = MLDT.ml_deduction_type_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'D.status = MLS.status_id',
                    'type' => 'INNER'
                )
            );
            $where = 'D.status = 1';
            $group_by = 'D.deduction_id';


            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_deductions/$1/$2"><button class= "btn green">Action</button></a>',
                    'DeductionID,EmployeeID'
                )
            );
            if(isset($approval) && !empty($approval)){
                $where .=' AND MLS.status_id ='.$approval;
            }
            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }



        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $deductionTable = 'deduction D';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $DeductionID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Approved'
                    );
                    $where = array(
                        'deduction_id' => $DeductionID
                    );
                    $this->common_model->update($deductionTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $DeductionID){
                    $updateData = array(
                        'date_approved' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Decline'
                    );
                    $where = array(
                        'deduction_id' => $DeductionID
                    );
                    $this->common_model->update($deductionTable,$where,$updateData);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] 		= $this->input->post('status_title');
        $data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --', 'status_id','status_title');
        $this->load->view('includes/header');
        $this->load->view('dashboard/deduction', $data);
        $this->load->view('includes/footer');
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_deductions($deduction_id = NULL , $employee_id = NULL)
    {
        if($this->uri->segment(3))
        {
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

            $data['deduction_id'] = $this->uri->segment(3);
            $where = array('deduction.deduction_id' => $deduction_id);
            $data['emp'] = $this->site_model->get_deduction_approvals($where);
            //echo "<pre>";print_r($data['emp']); die;
            if($this->input->post('add'))
            {
                $add_type= array(
                    'approved_by' 			=> $this->session->userdata('employee_id'),
                    'date_approved'			=> date('Y-m-d'),
                    'note' 			=> $this->input->post('note'),
                    'status' 			=> $this->input->post('status')
                );
                //echo "<pre>"; print_r($add_type); die;
                $query = $this->site_model->update_record('deduction',$where, $add_type);
                if($query)
                {
                    redirect('dashboard_site/deductions');
                }
            }
        }
        $data['data'] = $this->site_model->get_all('ml_status');
        $id['employee.employee_id'] = $this->uri->segment(4);

        $this->load->view('includes/header');
        $this->load->view('dashboard/deduction_approved', $data);
        $this->load->view('includes/footer');
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function expense_claim($param =NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }

        if($param == "list")
        {
//            echo $this->site_model->expense_claim();
//            die;
            $approval = $this->input->post('status_title');
            //New Fashion, Same Query
            $table = 'employee E';
            $selectData = array('E.employee_code,
            E.full_name,
            MLDg.designation_name,
            EC.amount,
            MLM.month,
            MLY.year,
            MLS.status_title,
            EC.expense_claim_id AS ExpenseClaimID,
            E.employee_id AS EmployeeID',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'expense_claims EC',
                    'condition' => 'ET.employment_id = EC.employment_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'PM.ml_designation_id = MLDg.ml_designation_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_month MLM',
                    'condition' => 'EC.month = MLM.ml_month_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_year MLY',
                    'condition' => 'EC.year = MLY.ml_year_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_status MLS',
                    'condition' => 'EC.status = MLS.status_id',
                    'type' => 'INNER'
                )
            );
            $where = 'EC.status = 1';
            $group_by = 'EC.expense_claim_id';

            $add_column = array(
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            $edit_column = array(
                array(
                    'Action',
                    '<a href="dashboard_site/action_expense_claim/$1/$2"><button class= "btn green">Action</button></a>',
                    'ExpenseClaimID,EmployeeID'
                )
            );

            //If Filtered
            if(isset($approval) && !empty($approval)){
                $where .= 'MLS.status_id ='.$approval;
            }

            //If Not Admin But Is A Line Manager
            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                $subOrdinatesEmployeeID = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDImploded = implode(',',$subOrdinatesEmployeeID);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDImploded.')';
            }
            $result = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','',$group_by,$add_column,$edit_column);
            echo $result;
            return;
        }


        if($param === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $ExpenseClaimTable = 'expense_claims EC';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'approval'){
                $this->db->trans_start();
                foreach($IDsArray AS $ExpenseClaimID){
                    $updateData = array(
                        'approval_date' => $currentDate,
                        'status' => 2,
                        'approved_by' => $loggedInEmployeeID,
                        'note' => 'Batch Approved'
                    );
                    $where = array(
                        'expense_claim_id' => $ExpenseClaimID
                    );
                    $updateResult = $this->common_model->update($ExpenseClaimTable,$where,$updateData);

                    if($updateResult === true){
                        $result = $this->common_model->select_fields_where($ExpenseClaimTable,'employment_id AS EmploymentID',array('expense_claim_id' => $ExpenseClaimID),TRUE);
                        $insert_data=array(
                            'notification_text'=>'Expense Claim Approved',
                            'notification_detail_link'=>'Expense Claim',
                            'employment_id'=>$result->EmploymentID,
                            'created_date'=>date("Y-m-d")
                        );
                        $this->common_model->insert_record('essp_employee_notifications',$insert_data);
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }elseif($batchType === 'decline'){
                $this->db->trans_start();
                foreach($IDsArray AS $ExpenseClaimID){
                    $updateData = array(
                        'approval_date' => $currentDate,
                        'status' => 3,
                        'approved_by' => $loggedInEmployeeID,
                        'note'=> 'Batch Declined'
                    );
                    $where = array(
                        'expense_claim_id' => $ExpenseClaimID
                    );
                    $updateResult = $this->common_model->update($ExpenseClaimTable,$where,$updateData);
                    ////Need To Set Notification For ESSP User..
                    if($updateResult === true){
                        $result = $this->common_model->select_fields_where($ExpenseClaimTable,'employment_id AS EmploymentID',array('expense_claim_id' => $ExpenseClaimID),TRUE);
                        $insert_data=array(
                            'notification_text'=>'Expense Claim Declined',
                            'notification_detail_link'=>'Expense Claim',
                            'employment_id'=>$result->EmploymentID,
                            'created_date'=>date("Y-m-d")
                        );
                        $this->common_model->insert_record('essp_employee_notifications',$insert_data);
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Decline of Requests::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Declined::success';
                    return;
                }
            }
            return;
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();

        $data['status_title'] 		= $this->input->post('status_title');
        $data['status'] 				= $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id','status_title');

        $this->load->view('includes/header');
        $this->load->view('dashboard/expense_claim', $data);
        $this->load->view('includes/footer');
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_expense_claim($expense_claim_id = NULL , $employee_id = NULL)
    {
        if($this->uri->segment(3))
        {
            //Get The Data for Left Side Menus..
            $data = getApprovalsSideMenus();

            $data['expense_claim_id'] = $this->uri->segment(3);
            $where = array('expense_claims.expense_claim_id' => $expense_claim_id);
            $data['emp'] = $this->site_model->get_expense_approvals($where);
            //echo "<pre>";print_r($data['emp']); die;
            if($this->input->post('add'))
            {
               $employment_id=get_employment_from_employeeID($this->uri->segment(4));
                $add_type= array(
                    'approved_by' 			=> $this->session->userdata('employee_id'),
                    'approval_date'			=> date('Y-m-d'),
                    'note' 			=> $this->input->post('note'),
                    'status' 			=> $this->input->post('status')
                );
                //echo "<pre>"; print_r($add_type); die;
                $query = $this->site_model->update_record('expense_claims',$where, $add_type);

                if($this->input->post('status') == 1){
                $insert_data=array(
                    'notification_text'=>'Expense Claim Approved',
                    'notification_detail_link'=>'Expense Claim',
                    'employment_id'=>$employment_id,
                    'created_date'=>date("Y-m-d")
                );
                $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);
                }
                if($this->input->post('status') == 3){
                    $insert_data=array(
                        'notification_text'=>'Expense Claim Declined',
                        'notification_detail_link'=>'Expense Claim',
                        'employment_id'=>$employment_id,
                        'created_date'=>date("Y-m-d")
                    );
                    $query = $this->site_model->create_new_record('essp_employee_notifications',$insert_data);
                }
                if($query)
                {
                    redirect('dashboard_site/expense_claim');
                }
            }
        }
        $data['data'] = $this->site_model->get_all('ml_status');
        $id['employee.employee_id'] = $this->uri->segment(4);

        $this->load->view('includes/header');
        $this->load->view('dashboard/expense_approved', $data);
        $this->load->view('includes/footer');
    }
    public  function app_des(){
        $this->load->view('includes/header');
        $this->load->view('dashboard/attend_app');
        $this->load->view('includes/footer');
    }


    //////////FUNCTION FOR Register Attendance/////////////////////
    public function Register_attendance()
    {
        $currentDate = date('Y-m-d');
        $table = 'ml_holiday';
        $data = 'ml_holiday_id';
        $where = array(
            'from_date <=' => $currentDate,
            'to_date >=' => $currentDate
        );
        $result = $this->common_model->select_fields_where($table,$data,$where,true);
      //echo print_r($result); die;
       $MlHolidayID = $result->ml_holiday_id;
        //echo print_r($MlHolidayID); die;

      if(isset($MlHolidayID) && !empty($MlHolidayID)){

        //$insert =  date_default_timezone_set('Asia/karachi');
        $day = date('D');
        switch ($day)
        {
            case 'Mon':
                $day = 1;
                break;

            case 'Tue':
                $day = 2;
                break;

            case 'Wed':
                $day = 3;
                break;

            case 'Thu':
                $day = 4;
                break;

            case 'Fri':
                $day = 5;
                break;

            case 'Sat':
                $day = 6;
                break;

            case 'Sun':
                $day = 7;
                break;
        }
        $month = date('M');
        switch ($month)
        {
            case 'Jan':
                $month = 1;
                break;

            case 'Feb':
                $month = 2;
                break;

            case 'Mar':
                $month = 3;
                break;

            case 'Apr':
                $month = 4;
                break;

            case 'May':
                $month = 5;
                break;

            case 'Jun':
                $month = 6;
                break;

            case 'Jul':
                $month = 7;
                break;

            case 'Aug':
                $month = 8;
                break;

            case 'Sep':
                $month = 9;
                break;

            case 'Oct':
                $month = 10;
                break;

            case 'Nov':
                $month = 11;
                break;

            case 'Dec':
                $month = 12;
                break;

        }

        $year = $this->input->post('year');
        $yearexp = date("Y", strtotime($year));
        $where = array('ml_year.year' => $yearexp);

        $eyear = $this->site_model->get_row_by_id('ml_year',$where);
        $year =  $eyear->ml_year_id;
        $insert = array(
            'employee_id'            => $this->session->userdata('employee_id'),
            'in_time'                => $this->input->post('in_time'),
            'in_date'                => $this->input->post('in_date'),
            'attendance_status_type' => 2,
            'work_week_id'           => $day,
            'ml_month_id'            => $month,
            'ml_year_id' 			 => $year,
            'out_time' => '00:00:00',
            'out_date' => '0000-00-00',
            'remarks' => '',
            'holiday'=> $MlHolidayID
        );

        //$data['year_s'] = $this->site_model->year_same();
        $query = $this->site_model->create_new_record('attendence', $insert);
        if($query)
        {
            redirect('dashboard_site/view_dashboard');
        }}else{

          //$insert =  date_default_timezone_set('Asia/karachi');
          $day = date('D');
          switch ($day)
          {
              case 'Mon':
                  $day = 1;
                  break;

              case 'Tue':
                  $day = 2;
                  break;

              case 'Wed':
                  $day = 3;
                  break;

              case 'Thu':
                  $day = 4;
                  break;

              case 'Fri':
                  $day = 5;
                  break;

              case 'Sat':
                  $day = 6;
                  break;

              case 'Sun':
                  $day = 7;
                  break;
          }
          $month = date('M');
          switch ($month)
          {
              case 'Jan':
                  $month = 1;
                  break;

              case 'Feb':
                  $month = 2;
                  break;

              case 'Mar':
                  $month = 3;
                  break;

              case 'Apr':
                  $month = 4;
                  break;

              case 'May':
                  $month = 5;
                  break;

              case 'Jun':
                  $month = 6;
                  break;

              case 'Jul':
                  $month = 7;
                  break;

              case 'Aug':
                  $month = 8;
                  break;

              case 'Sep':
                  $month = 9;
                  break;

              case 'Oct':
                  $month = 10;
                  break;

              case 'Nov':
                  $month = 11;
                  break;

              case 'Dec':
                  $month = 12;
                  break;

          }

          $year = $this->input->post('year');
          $yearexp = date("Y", strtotime($year));
          $where = array('ml_year.year' => $yearexp);

          $eyear = $this->site_model->get_row_by_id('ml_year',$where);
          $year =  $eyear->ml_year_id;
          $insert = array(
              'employee_id'            => $this->session->userdata('employee_id'),
              'in_time'                => $this->input->post('in_time'),
              'in_date'                => $this->input->post('in_date'),
              'attendance_status_type' => 2,
              'work_week_id'           => $day,
              'ml_month_id'            => $month,
              'ml_year_id' 			 => $year,
              'out_time' => '00:00:00',
              'out_date' => '0000-00-00',
              'remarks' => '',
              'holiday'=> 0
          );

          //$data['year_s'] = $this->site_model->year_same();
          $query = $this->site_model->create_new_record('attendence', $insert);
          if($query)
          {
              redirect('dashboard_site/view_dashboard');
          }
      }



    }



//////////////////////////////////////////// Function for Day Out Attendance ///////////////////////////////////////////////////
    public function dayout_attendance()
    {

        $insert = array(
            'out_time' => $this->input->post('out_time'),
            'out_date' => $this->input->post('out_date'),
            'attendance_status_type' => 1
        );

        $query = $this->site_model->update_record('attendence',array('in_date ='=>date('Y-m-d'),'employee_id'=>$this->session->userdata('employee_id')),$insert);

        if($query)
        {
            redirect('dashboard_site/view_dashboard');
        }
    }



    //////////////////////////Time Sheet Approvals \\\\\\\\\\\\\\\\\\\\\\\\

public function manual_timesheet_approval($dataTables = NULL){
    $loggedInEmployeeID = $this->data['EmployeeID'];
    //Restrict Un Authorized Access..
    if(!is_method_allowed($loggedInEmployeeID, 'approvals', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)){
        $msg = "You Do Not Have Access To This Certain Section::error";
        $this->session->set_flashdata('msg',$msg);
        redirect(previousURL());
    }

        If($dataTables != NULL && $dataTables == 'listEmployeesWithTimeSheetApprovals'){
            $postedMonth = $this->input->post('month');
            $postedYear = $this->input->post('year');
            $postedEmployeeIDs = $this->input->post('employees');

            //Need To Show the Table in the DataTables..
            $table = 'timesheet T';
            $data = array('E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode, DATE_FORMAT(T.date_created, "%M, %Y") AS MonthOfTimeSheet',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = T.employment_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                )
            );

/*            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'T.timesheet_status_id' => 1
            );*/

            $where = 'E.enrolled = 1 AND E.trashed = 0 AND T.timesheet_status_id = 1';
            $group_by = '';
            $where_in_column = '';
            $where_in_data = '';
            if($this->input->post()){
                if(isset($postedMonth) && !empty($postedMonth)){
//                    $where['MONTH(T.date_created)'] = $postedMonth;
                    $where .= ' AND MONTH(T.date_created) ='.$postedMonth;
                }
                if(isset($postedYear) && !empty($postedYear)){
//                    $where['YEAR(T.date_created)'] = $postedYear;
                    $where .= ' AND YEAR(T.date_created) ='.$postedYear;
                }
                if(isset($postedEmployeeIDs) && !empty($postedEmployeeIDs)){
                    $where_in_column = 'E.employee_id';
                    $where_in_data = $postedEmployeeIDs;
                }
            }
            $addColumn = array(
                'Actions' => '<a class="viewEmployeeTimeSheet" style="cursor: pointer;"><span class="fa fa-eye"></span></a>',
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );
            if(!is_admin($loggedInEmployeeID) && is_lineManager($loggedInEmployeeID)){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND E.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';

            }
            $result = $this->common_model->select_fields_joined_DT($data,$table,$joins,$where,$where_in_column,$where_in_data,$group_by,$addColumn);
            print_r($result);
            return;
        }elseif($dataTables == 'listEmployeesWithTimeSheetApprovalsSelect2'){
            $table = 'timesheet T';
            $data = array('E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode, DATE_FORMAT(T.date_created, "%M, %Y") AS MonthOfTimeSheet',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = T.employment_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'T.timesheet_status_id' => 1
            );
            $value = $this->input->post('term');
            $group_by = 'E.employee_id';
            if(isset($value) && !empty($value)){
                $field = 'E.full_name';
                $employees = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,$field,$value,$group_by);
            }else{
                $employees = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }
            print_r(json_encode($employees));
//            echo $this->db->last_query();
            return;
        }elseif($dataTables == 'listYearsWithTimeSheetApprovalsSelect2'){
            $table = 'timesheet T';
            $data = array('EXTRACT(YEAR FROM T.date_created) AS ManualTimeSheetYear ',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = T.employment_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'T.timesheet_status_id' => 1
            );
            $group_by = ('ManualTimeSheetYear');
            $value = $this->input->post('term');
            if(isset($value) && !empty($value)){
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }else{
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }
            print_r(json_encode($result));
            return;
        }
        elseif($dataTables == 'listMonthsWithTimeSheetApprovalsSelect2'){
            $table = 'timesheet T';
            $data = array('EXTRACT(MONTH FROM T.date_created) AS ManualTimeSheetMonthNumeric,
            MONTHNAME(T.date_created) AS ManualTimeSheetMonth');
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = T.employment_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'T.timesheet_status_id' => 1
            );
            $group_by = 'ManualTimeSheetMonth';
            $value = $this->input->post('term');
            if(isset($value) && !empty($value)){
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }else{
                $result = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            }
            print_r(json_encode($result));
            return;
        }
        elseif($dataTables == 'showEmployeeTimeSheetToApprove'){
            //This Section Should Load the Employee TimeSheet on the Next Page When Pressed on Details Button..
            if($this->input->post()){

                //Loading Data for Left Menus of Approvals..
                $viewData = getApprovalsSideMenus();

                $employeeID = $this->input->post('employee');
                $employmentID = get_employment_from_employeeID($employeeID);
                $postedMonth = $this->input->post('selectedMonth');
                $postedYear = $this->input->post('selectedYear');
                if(isset($employeeID) && !empty($employeeID) && is_numeric($employeeID)){
                    //As We Have EmployeeID, We Need To Show The TimeSheet View :) Back To The Most Complicated Work i have done in this Project.

                    ///Getting TimeSheet Data

                    $PTable = 'employee E';
                    $data = ('
   TSD.timesheetDetails_id,
   EP.project_id AS ProjectID,
   TSD.date AS WorkDate,
   TSD.hours AS WorkHours,
   TS.date_created AS TimeSheetMonth,
   E.employee_id AS EmployeeID,
   TS.id AS TimeSheetID,
   MLP.project_title AS ProjectTitle,
   E.full_name AS EmployeeName
');
                    $joins = array(
                        array(
                            'table' => 'employment ET',
                            'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'timesheet TS',
                            'condition' => 'TS.employment_id = ET.employment_id AND DATE_FORMAT(TS.date_created,"%Y") = ' . $postedYear,
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'timesheet_details TSD',
                            'condition' => 'TS.id = TSD.timesheet_id AND DATE_FORMAT(TSD.date,"%c") = "' . $postedMonth . '" AND DATE_FORMAT(TSD.date,"%Y") = ' . $postedYear,
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_projects MLP',
                            'condition' => 'TSD.project_id = MLP.project_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employee_project EP',
                            'condition' => 'MLP.project_id = EP.project_id AND ET.employment_id = EP.employment_id',
                            'type' => 'INNER'
                        )
                    );
                    $group_by = '';
                    $where = array(
                        'E.employee_id' => $employeeID,
                        'E.enrolled' => 1,
                        'E.trashed' => 0
                    );
                    $viewData['TimeSheetViewData'] = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, FALSE, '', '', $group_by);
                    //End of TimeSheetTable

                    //Getting Employee Info Details.
                    $table = 'employee';
                    $data = ('full_name AS EmployeeName,employee_id AS EmployeeID');
                    if(isset($employeeID) && !empty($employeeID)){
                        $where = array(
                            'employee_id' => $employeeID
                        );
                    }else{
                        $where = array(
                            'employee_id' => $employeeID
                        );
                    }
                    $viewData['employeeData'] = $this->common_model->select_fields_where($table, $data, $where, TRUE);
                    //End of Employee Details Info..

                    /**
                     * Now Code For the Leaves Of Employees.. Colors and Entitlements..
                     */
                    $viewData['employeeTotalLeaves'] = $this->site_model->getTotalEmployeeLeaves($postedMonth, $postedYear, $employeeID,$employmentID);
                    //Bottom
                    $viewData['employeeTotalMonthlyAndYearly'] = $this->site_model->getTotalEmployeeLeavesNew($postedMonth, $postedYear, $employeeID,$employmentID);

                    ///Need The Colors For TimeSheet Leave Types.
                    $table = 'ml_leave_type';
                    $data = array('ml_leave_type_id AS LeaveTypeID, leave_type AS LeaveType, hex_color AS LeaveTypeColor', false);
                    $where = array(
                        'trashed' => 0
                    );
                    $result = $this->common_model->select_fields_where($table,$data,$where);
                    if(isset($result) && !empty($result)){
                        $viewData['LeaveTypes'] = $result;
                    }

                    //Load the View..
                    $this->load->view('includes/header');
                    $this->load->view('dashboard/manualTimeSheetApproval', $viewData);
                    $this->load->view('includes/footer');
                    return;
                }else{
                    //Will Add Some Message With The Redirect Later ...
                    redirect(previousURL()); //Should Redirect Back To the Previous Page As Record Was Not Posted Properly.
                }
            }else{
                //Will Add Some Message With The Redirect Later ...
                redirect(previousURL()); // Should Redirect Back To the Previous Page As No Record Was Posted.
            }
        }

    //Loading Data for Left Menus of Approvals..
    $data = getApprovalsSideMenus();

    //Loading Views
    $this->load->view('includes/header');
    $this->load->view('dashboard/manualTimeSheetApprovalsList', $data);
    $this->load->view('includes/footer');
}

    function approve_decline_manualTimeSheet()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $employeeID = $this->input->post('empID');
                $timeSheetID = $this->input->post('timeSheet');
                $approvalRemarks = $this->input->post('approvalRemarks');
                $declineRemarks = $this->input->post('declinedRemarks');
                $tableTimeSheet = 'timesheet';

                if (isset($approvalRemarks) && !empty($approvalRemarks)) {
                    //Before Declining SomeThing We Should First Check It is In Pending State or Not..
                    $checkWhere = array(
                        'id' => $timeSheetID
                    );
                    $checkData = ('timesheet_status_id AS Status');
                    $checkResult = $this->common_model->select_fields_where($tableTimeSheet,$checkData,$checkWhere,TRUE);
                    if($checkResult->Status != 1){
                        echo "FAIL::TimeSheet Must Be In Pending Status To Be Approved or Declined::warning";
                        return;
                    }
                    //If the TimeSheet Was in Pending Status, Then it will move to this below steps.. Which are updating there status Info


                    $updateData = array(
                        'timesheet_status_id' => 2
                    );
                    $updateWhere = array(
                        'id' => $timeSheetID
                    );
                    $updateResult = $this->common_model->update($tableTimeSheet, $updateWhere, $updateData);
                    if ($updateResult === true) {
                        echo "OK::TimeSheet Successfully Approved::success";
                        return;
                    }
                } elseif (isset($declineRemarks) && !empty($declineRemarks)) {

                    //Before Declining SomeThing We Should First Check It is In Pending State or Not..
                    $checkWhere = array(
                        'id' => $timeSheetID
                    );
                    $checkData = ('timesheet_status_id AS Status');
                    $checkResult = $this->common_model->select_fields_where($tableTimeSheet,$checkData,$checkWhere,TRUE);
                    if($checkResult->Status != 1){
                        echo "FAIL::TimeSheet Must Be In Pending Status To Be Approved or Declined::warning";
                        return;
                    }
                    //If the TimeSheet Was in Pending Status, Then it will move to this below steps.. Which are updating there status Info
                    // and inserting the Fail Review Data..
                    $updateData = array(
                        'timesheet_status_id' => 3
                    );
                    $updateWhere = array(
                        'id' => $timeSheetID
                    );
                    $updateResult = $this->common_model->update($tableTimeSheet, $updateWhere, $updateData);

                    $tableFailInsert = 'fail_reviews';
                    $data = array(
                        'fail_table' => 'timesheet',
                        'fail_id' => $timeSheetID,
                        'status' => 3,
                        'remarks' => $declineRemarks,
                        'reviewed_by' => $this->data['EmployeeID'],
                        'review_date' => $this->data['dbCurrentDateTime']
                    );
                    $insertResult = $this->common_model->insert_record($tableFailInsert, $data);
                    if ($updateResult === true && $insertResult > 0) {
                        echo "OK::TimeSheet Successfully Declined::success";
                        return;
                    }
                } else {
                    echo "FAIL::No Data Posted::error";
                    return;
                }
            }
        }
    }

    ////////////   //////////End Of TimeSheet Approvals////////
    function review_breached_disciplines($list = NULL)
    {
        $loggedInEmployeeID = $this->data['EmployeeID'];
        //Restrict Un Authorized Access..
        if (!is_method_allowed($this->data['EmployeeID'], 'review_breached_disciplines', 'view') && !is_admin($loggedInEmployeeID) && !is_lineManager($loggedInEmployeeID)) {
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg', $msg);
            redirect(previousURL());
        }

        //Get The Data for Left Side Menus..
        $data = getApprovalsSideMenus();
        $data['status_title'] = $this->input->post('status_title');
        $data['status'] = $this->site_model->dropdown_wd_option('ml_status', '-- Select Status --',
            'status_id', 'status_title');

//        $data['info_eos'] = $this->site_model->get_eos_transcation();

        if ($list === 'list') {
            //SHH Code
            $table = 'disciplinary_action_reports DAR';
            $selectData = array('
            DAR.DAreportID AS ReportID,
            DAR.disciplineBreached AS BreachedDiscipline,
            DATE_FORMAT(DAR.dateOfBreach,"%d-%m-%Y") AS DateOfBreach,
            RE.full_name AS ReportedBy,
            DAR.reportingDate AS DateReported,
            CASE WHEN DAR.status = 0 THEN "Pending Investigation" WHEN DAR.status = 1 THEN "Pending Review" WHEN DAR.status = 2 THEN "Reviewed Report" END AS ReportStatus,
            MLDA.action_type AS DisciplinaryAction,
            IE.full_name AS InvestigatedBy,
            AE.full_name AS AccusedEmployee
        ', false);
            $joins = array(
                array(
                    'table' => 'employee RE',
                    'condition' => 'DAR.reportedBy = RE.employee_id AND RE.trashed = 0',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'employee IE',
                    'condition' => 'DAR.investigatedBy = IE.employee_id AND IE.trashed = 0',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_disciplinary_action MLDA',
                    'condition' => 'MLDA.disciplinaryActionID = DAR.disciplinaryActionID',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'employment AET', //Join For Accused Employee
                    'condition' => 'AET.employment_id = DAR.employmentID AND AET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee AE',
                    'condition' => 'AE.employee_id = AET.employee_id AND AE.trashed = 0',
                    'type' => 'INNER'
                )
            );

            $where = 'DAR.trashed = 0 AND status = 1';
            $add_column = array(
                'ViewEditActionButtons' => '<a style="cursor:pointer;cursor:hand;" class="disciplinaryActionReview"><span class="fa fa-pencil-square-o"></span></a>',
                'checkboxes' => '<input type="checkbox" class="batchApprovalCheckBox" />'
            );

            if(!is_admin($loggedInEmployeeID) && is_lineManager($this->data['EmployeeID'])){
                $subOrdinatesEmployeeIDs = get_lineManager_SubOrdinatesEmployeeIDs($loggedInEmployeeID);
                $subOrdinatesEmployeeIDsImploded = implode(',',$subOrdinatesEmployeeIDs);
                $where .= ' AND AE.employee_id IN ('.$subOrdinatesEmployeeIDsImploded.')';
            }
            $listResult = $this->common_model->select_fields_joined_DT($selectData, $table, $joins, $where, '', '', '', $add_column);
            echo $listResult;
            return;
        }

        if($list === 'batch'){
            //If Data Posted As Batch..
            //First Get the Posted Values that Matters>
            $batchType = $this->input->post('batchType');
            $disciplinaryActionRemarks = $this->input->post('disciplinaryActionRemarks');
            $disciplinaryActionID = $this->input->post('disciplinaryAction');
            if(!isset($disciplinaryActionRemarks) || empty($disciplinaryActionRemarks)){
                $disciplinaryActionRemarks = 'Batch Reviewed';
            }
            $IDs = $this->input->post('IDs');

            if(!isset($IDs) || empty($IDs) || $IDs === '[]'){
                echo 'Some Problem Occurred, You Must Select At Least 1 Leave Application To Approve';
                return;
            }
            $IDsArray = json_decode($IDs);
            $disciplinaryActionReportsTable = 'disciplinary_action_reports';
            $currentDate = $this->data['dbCurrentDate'];
            if($batchType === 'review'){
                $this->db->trans_start();
                foreach($IDsArray AS $reportID){
                    $updateData = array(
                        'reviewDate' => $currentDate,
                        'status' => 2,
                        'reviewedBy' => $loggedInEmployeeID,
                        'disciplinaryActionID' => $disciplinaryActionID,
                        'reviewerRemarks' => $disciplinaryActionRemarks
                    );
                    $where = array(
                        'DAreportID' => $reportID
                    );
                    $this->common_model->update($disciplinaryActionReportsTable,$where,$updateData);

                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    echo 'FAIL::Some Problem Occurred During Approval::error';
                    return;
                }else{
                    echo 'OK::Records Successfully Approved::success';
                    return;
                }
            }
            return;
        }

        $this->load->view('includes/header');
        $this->load->view('dashboard/breached_disciplines_reviews', $data);
        $this->load->view('includes/footer');
    }
    function load_all_available_disciplinary_actions(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //If Request Generated Through A Proper Channel Then Below Code Should Execute.
                $table = 'ml_disciplinary_action MLDA';
                $selectData= array('disciplinaryActionID AS ID, action_type AS Text',false);
                $where = array(
                    'trashed' => 0
                );
                $result = $this->common_model->select_fields_where($table,$selectData,$where,FALSE);
                echo json_encode($result);
            }
        }
    }
    function update_breached_discipline_review(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $reportID = $this->input->post('reportID');
                $disciplinaryActionID = $this->input->post('disciplinaryActionID');
                $reviewerRemarks = $this->input->post('reviewerRemarks');

                if(!isset($reportID) || empty($reportID)){
                    echo "FAIL::Some Problem Occurred With The POST, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
                if(!isset($disciplinaryActionID) || empty($disciplinaryActionID)){
                    echo "FAIL::Please Select Disciplinary Action From Dropdown::error";
                    return;
                }
                if(!isset($reviewerRemarks) || empty($reviewerRemarks)){
                    echo  "FAIL::Please Enter At Least 10 Characters In Remarks::error";
                    return;
                }

                //If User Came Through Proper Channel. Then Lets Do The Update.
                $table = 'disciplinary_action_reports DAR';
                $updateData = array(
                    'reviewerRemarks' => $reviewerRemarks,
                    'disciplinaryActionID' => $disciplinaryActionID,
                    'status' => 2
                );
                $where =array(
                    'DAreportID' => $reportID,
                    'status' => 1
                );
                $updateResult = $this->common_model->update($table,$where,$updateData);
                if($updateResult === true){
                    echo "OK::Review Successfully Updated::success";
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Try Again or Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }


/////////////END OF FUNCTION//////////////////////////////


    /* End of Dashboard Class */
}
