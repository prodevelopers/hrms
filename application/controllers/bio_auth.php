<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bio_auth extends MY_Controller
{
	public $site_name;
	public $redirect = 'auth/login';
	public $data = array();
	function __construct()
	{
		parent::__construct();
		$this->load->library('zklib');
	}
	
    public function attendance(){
        //So Using This Function We Will Register Attendance
        //But Before Fetching Today's Results From Device, We First Need To Know If Today is Holiday Or Not.
        //If Today is Holiday or OFF for Some Reason, Then No Need To Mark Today's Attendance.

        //Today's Date
        $current_date = date("Y-m-d");

        $holidayTable = 'ml_holiday';
        $holidayWhere = array(
            'trashed'=>0,
            'enabled'=>1,
            'to_date >='=> $current_date,
            'from_date <='=> $current_date
        );
        $countResult = $this->common_model->select_fields_where($holidayTable,'COUNT(1) AS TotalRecordsFound,GROUP_CONCAT(holiday_type) AS HolidayTitles,ml_holiday_id ID',$holidayWhere,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            //If Holiday Then Only Need To Register Holiday Attendance.
            $employeeTable = 'employee E';
            $selectData = 'E.employee_id';
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'attendence ATT',
                    'condition' => 'E.employee_id = ATT.employee_id AND ATT.in_date = "'.$current_date.'"',
                    'type' => 'LEFT'
                )
            );
            $where = 'E.enrolled = 1 AND E.trashed = 0 AND ATT.employee_id IS NULL';
            $employees = $this->common_model->select_fields_where_like_join($employeeTable,$selectData,$joins,$where);
            //Need Month, Year and Week ID
            //Getting MonthID
            $monthTable = 'ml_month';
            $selectData = 'ml_month_id AS MonthID';
            $where = array(
                'trashed' => 0,
                'month' => date('M') //Current Month Short Name
            );
            $monthResult = $this->common_model->select_fields_where($monthTable,$selectData,$where,TRUE);
            //Getting YearID
            $yearTable = 'ml_year';
            $selectData = 'ml_year_id AS YearID';
            $where = array(
                'trashed' => 0,
                'year' => date('Y') //Current Year 4 digits
            );
            $yearResult = $this->common_model->select_fields_where($yearTable,$selectData,$where,TRUE);
            //WorkWeek
            $weekTable = 'work_week';
            $selectData = 'work_week_id AS workWeekID';
            $where = array(
                'trashed' => 0,
                'work_week' => date('l')
            );
            $workWeekTable = $this->common_model->select_fields_where($weekTable,$selectData,$where,TRUE);
            if(isset($employees) && is_array($employees) && !empty($employees)){
               foreach($employees as $employeeKey=>$employeeVal){
                   $employees[$employeeKey]->in_date = $current_date;
                   $employees[$employeeKey]->holiday = $countResult->ID;
                   $employees[$employeeKey]->remarks = 'Holiday';
                   $employees[$employeeKey]->attendance_status_type = 2;
                   if(isset($monthResult) && !empty($monthResult)){
                       $employees[$employeeKey]->ml_month_id = $monthResult->MonthID;
                   }
                   if(isset($yearResult) && !empty($yearResult)){
                       $employees[$employeeKey]->ml_year_id = $yearResult->YearID;
                   }
                   if(isset($workWeekTable) && !empty($workWeekTable)){
                       $employees[$employeeKey]->work_week_id = $workWeekTable->workWeekID;
                   }
               }
                //As We Have Setup The Data for Insertion.
                $employeesAttendance = json_decode(json_encode($employees),true);
                $this->common_model->insert_multiple('attendence',$employeesAttendance);
            }else{
                echo "FAIL::Nothing To Perform, Either No Employees In System Or Either All Employees Have Already Been Marked For Holiday.::error";
            }
            return;
        }

        //Continuing Further If Passed From Above Test.
        $this->zklib->can_connect("203.215.166.236", 4370);
        $connected=$this->zklib->connect();
        sleep(1);
        if(!$connected){
            //If Not Connected To Device Return Using Some Message.
            echo "FAIL::Could Not Connect To The Device, Make Sure, You have Provided Right IP and PORT.::error";
            return;
        }
        $this->zklib->disableDevice();
        sleep(1);

        $attendance =  $this->zklib->getAttendance();
        $attendanceTable = 'attendence';
        $this->db->trans_begin();
        while(list($idx, $attendanceData) = each($attendance)){
            $employeeID = $attendanceData[1];
            $date= date( "Y-m-d", strtotime($attendanceData[3]));
            $time_in= date( "H:i:s", strtotime( $attendanceData[3]));
            //First We Need To Check If Employee Has Already CheckedIn Or Not For Current Date.
            $whereCheck=array('employee_id'=>$employeeID,'in_date'=>$current_date);
            $countResult = $this->common_model->select_fields_where('attendence',array('COUNT(1) AS TotalRecordsFound'),$whereCheck,TRUE);
            if(!isset($countResult) || empty($countResult)){
                echo "FAIL::Something Went Wrong with The Query Result, Please Contact System Administrator For Further Assistance::error";
                return;
            }
            if($countResult->TotalRecordsFound > 0){
                //If Records Already Exist For Current Date For Particular Employee, Then Will Update The Data In Database.
               //But Before Update We Need To Take Out Little Device Bug Problem.
                //Device Sometimes Register Attendance 2 or more then 2 times if Finger is Kept Pressed Against The FingerPrint Mirror
                $attResult = $this->common_model->select_fields_where($attendanceTable,'in_date AS InDate, in_time AS InTime, employee_id AS EmployeeID',$whereCheck,TRUE);
                $dbInTime = new DateTime($attResult->InTime);
                $deviceInTime = new DateTime($time_in);
                $dbInTime->modify('+1 minutes'); // can be seconds, hours.. etc
                $targetTime = $dbInTime->format('H:i:s');

                //Checking If Updating For Right Employee Or Not.
                if(($employeeID == $attResult->EmployeeID) && ($date == $attResult->InDate) && ($time_in > $targetTime) ){
                    //Updating If Passed The Above Checks
                    $updateData = array(
                        'out_time' => $time_in,
                        'out_date' => $date,
                        'attendance_status_type' => 2,
                        'remarks' => 'bioMetric Update'
                    );
                    $updateWhere = array(
                        'employee_id' => $employeeID,
                        'in_date' => $date,
                        'in_time' => $attResult->InTime
                    );
                    $this->common_model->update($attendanceTable,$updateWhere,$updateData);
                }
            }elseif($countResult->TotalRecordsFound == 0){
                //Means No Record Found For Today's Date, We Will Check Him In.
                //Means Inserting New Record
                $insertData = array(
                    'employee_id' => $employeeID,
                    'in_time' => $time_in,
                    'in_date' => $date,
                    'attendance_status_type' => 1,
                    'remarks' => 'bioMetric Insert'
                );
                $lastInsertedID = $this->common_model->insert_record($attendanceTable, $insertData);
                if(!($lastInsertedID > 0)){
                    //If Fail RollBack
                    $this->db->trans_rollback();
                    echo "FAIL::Something Went Wrong In the Middle Of The Process, Could Not Insert New Record In Database::error";
                    return;
                }
            }
        }

        $this->zklib->getTime();
        $this->zklib->enableDevice();
        $this->zklib->disconnect();

        if ($this->db->trans_status() === FALSE)
        {
            echo "FAIL";
            $this->db->trans_rollback();
        }
        else
        {
            echo "Success";
            $this->db->trans_commit();
        }
    }
}
