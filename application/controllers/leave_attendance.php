<?php

/**
 * Class leave_attendance
 * @property site_model $site_model
 */
class leave_attendance extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('site_model');
        $this->load->library('datatables');
        $this->load->library('pagination');
        $this->load->library('pagination');
        $this->load->library('session');
        if ($this->session->userdata('logged_in') != true) {
            redirect('login');
        }

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if(!is_module_Allowed($loggedInEmployeeIDAccessCheck,$moduleController,'view') && !is_admin($loggedInEmployeeIDAccessCheck)){
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg',$msg);
            redirect(previousURL());
        }
    }

    /*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    /*///////////////////////////////////////////////////// Apply For leave ///////////////////////////////////////////////////////*/
    /*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

    // Function for leave Attendence view //

    public function view_leave_attandance()
    {
        $full_name = $this->input->post('full_name');
        $leave_type = $this->input->post('leave_type');
        $data = $this->site_model->leave_attendance_count();
        $count = count($data);
        //echo "<pre>";print_r($config['per_page']);die;
        $config['base_url'] = base_url() . 'leave_attendance/view_leave_attandance';
        $config['total_rows'] = $count;
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);
        $start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if(empty($start) || $start == 1){
            $start = 0;
        }
        $data['leave_attend'] = $this->site_model->leave_attendance($limit, $start, $full_name, $leave_type);
        $data['links'] = $this->pagination->create_links();
        $data['leave_type'] = $this->site_model->drop_down3();
        //$data['full_name'] = $this->site_model->drop_down3();
        $this->load->view('includes/header');
        $this->load->view('leave_attendence/leave_attendance', $data);
        $this->load->view('includes/footer');

    }

    // Function for Edit Leave Application //

    public function edit_leave_application($id = NULL,$employee = NULL,$leave = NULL)
    {
        if($this->uri->segment(3))
        {
            $data['leave_type'] = $this->site_model->drop_down3();
            //$data['leave_attend'] = $this->site_model->leave_attendance();
            $where = array('application_id' => $id);
            $data['user'] = $this->site_model->get_row_by_id('leave_application',$where);
            $data['employee_id'] = $this->site_model->get_row_by_id('employee',array('employee_id' => $employee));
            $data['user1'] = $this->site_model->get_row_by_id('leave_approval',array('leave_approval_id' => $leave));
           //echo "<pre>";print_r($data['user']); die;
            if($this->input->post('edit'))
            {
                //Leave Application Is Based On Employment, So We Need To Find Current Employement ID for posted Employee
                $table = 'employment ET';
                $data = ('ET.employment_id AS EmploymentID');
                $where = array(
                    'ET.employee_id' =>  $employee,
                    'current' => 1,
                    'trashed' => 0
                );
                $result = $this->common_model->select_fields_where($table, $data, $where, TRUE);
                if(!isset($result) || empty($result)){
                    echo "FAIL::This Employee Is Not In Roll, Can Not Add Details For This Employee::error";
                    return;
                }

                $employmentID = $result->EmploymentID;

                $table = 'leave_approval';
                $selectData = array('COUNT(1) AS TotalApplicationsSubmitted');
                $where = array(
                    'employment_id' => $employmentID,
                    'status' => 1,
                    'trashed' => 0
                );
                $countResult = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
              /*  if(isset($countResult) && $countResult->TotalApplicationsSubmitted > 0){
                    echo "this"; die;
                    $msg = "One Application is Already Pending!";
                    $this->session->set_flashdata('alert', $msg);
                    redirect("leave_attendance/view_leave_attandance");
                    return;
                }*/

                $fromdate = $this->input->post('from_date');
                $todate  = $this->input->post('to_date');
                if(isset($fromdate) && !empty($fromdate)){
                    $fromdate = date('Y-m-d',strtotime($fromdate));
                }
                if(isset($todate) && !empty($todate)){
                    $todate = date('Y-m-d',strtotime($todate));
                }

                $insert = array(
                    'from_date' => $fromdate,
                    'to_date' => $todate,
                    'total_days' => $this->input->post('total_days'),
                    'ml_leave_type_id' => $this->input->post('leave_type'),
                    'note' => $this->input->post('note')
                );


                //echo "<pre>";print_r($insert); die;
                $where_id=array('application_id'=>$id);
                $query = $this->site_model->update_record('leave_application',$where_id,$insert);
                $where = array('leave_approval_id' => $leave);
                $insert1 = array(
                    'employment_id' => $employmentID,
                    //'leave_application_id' => $this->site_model->get_last_id()
                );
                //echo "<pre>";print_r($insert1); die;
                $query1 = $this->site_model->update_record('leave_approval',$where,$insert1);
                //echo "<pre>";print_r($query1); die;
                if ($query || $query1){
                    redirect('leave_attendance/view_leave_attandance/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/edit_leave_application', $data);
        $this->load->view('includes/footer');
    }


    // Function for Apply For Leave//

    public function apply_for_leave()
    {
        //$employee_id = $this->input->post('emp_id');

       // $where = "employee.employee_id = '$employee_id'";
        //var_dump($where);
        $data['employee'] = $this->site_model->fetch_information();
       //var_dump($data['employee']);
        $data['leave_type'] = $this->site_model->drop_down3();

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/apply_for_leave', $data);
        $this->load->view('includes/footer');
    }

    // Function For ADD Apply for Leave
    public function add_apply_for_leave()
    {

        if ($this->input->post('add')) {
            $shortFull=$this->input->post('leave');

            $postedEmployeeID = $this->input->post('emp_id');
            $postedLeaveType = $this->input->post('leave_type');
            //$data['rec'] = $this->site_model->get_all_by_where('leave_approval', array('employee_id' => $this->input->post('emp_id')));


            //Leave Application Is Based On Employment, So We Need To Find Current Employement ID for posted Employee

            $table = 'employment ET';
            $data = ('ET.employment_id AS EmploymentID');
            $where = array(
                'ET.employee_id' => $postedEmployeeID,
                'current' => 1,
                'trashed' => 0
            );
            $result = $this->common_model->select_fields_where($table, $data, $where, TRUE);
            if (!isset($result) || empty($result)) {
                echo "FAIL::This Employee Is Not In Roll, Can Not Add Details For This Employee::error";
                return;
            }

            $employmentID = $result->EmploymentID;
            //print_r($employmentID);die;


            //Lets Do It Different Way, The Above Whoever Done it is not right...
            $table = 'leave_approval';
            $selectData = array('COUNT(1) AS TotalApplicationsSubmitted');
            $where = array(
                'employment_id' => $employmentID,
                'status' => 1,
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table, $selectData, $where, TRUE);
            if (isset($countResult) && $countResult->TotalApplicationsSubmitted > 0) {
                $msg = "One Application is Already Pending!";
                $this->session->set_flashdata('alert', $msg);
                redirect("leave_attendance/apply_for_leave");
                return;
            }



            //No Need TO Post Entitlement ID from View. We will get Entitlement ID from Here.
            //Getting Entitlement ID
            $table = 'leave_entitlement LE';
            $selectData = 'leave_entitlement_id AS EntitlementID';
            $where = 'status = 2 AND trashed = 0 AND ml_leave_type_id = ' . $postedLeaveType . ' AND employment_id = ' . $employmentID . ' AND date_approved = (SELECT MAX(date_approved) from leave_entitlement where status = 2 AND trashed = 0 AND ml_leave_type_id = ' . $postedLeaveType . ' AND employment_id = ' . $employmentID . ')';
            $resultID = $this->common_model->select_fields_where($table, $selectData, $where, TRUE);

            if (isset($resultID) && !empty($resultID) && $resultID->EntitlementID > 0) {
                //This Will Continue
                $entitlementID = $resultID->EntitlementID;
            } else {
                $msg = "You Might Not Be Entitled For This Leave or Entitle Might be In Pending,If you think you are recieving this Message in Some Error then Please Contact System Administrator For Further Assistance";
                $this->session->set_flashdata('alert', $msg);
                redirect("leave_attendance/apply_for_leave");
                return;
            }
            $fromdate = $this->input->post('from_date');
            $todate = $this->input->post('to_date');
            if (isset($fromdate) && !empty($fromdate)) {
                $fromdate = date('Y-m-d', strtotime($fromdate));
            }
            if (isset($todate) && !empty($todate)) {
                $todate = date('Y-m-d', strtotime($todate));
            }

            $add_type = array(
                'employment_id' => $employmentID,
                'entitlement_id' => $entitlementID,
                'ml_leave_type_id' => $postedLeaveType,
                'from_date' => $fromdate,
                'to_date' => $todate,
                'total_days' => $this->input->post('total_days'),
                'application_date' => date('Y-m-d'),
                'note' => $this->input->post('note'),
                'status' => 1
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('leave_application', $add_type);

            $insert = array(
                'employment_id' => $employmentID,
                'leave_application_id' => $this->site_model->get_last_id()

            );
            // echo "<pre>"; print_r($insert); die;
            $query1 = $this->site_model->create_new_record('leave_approval', $insert);
            if ($query || $query1) {
                //if Attempt Was Successful, Then Also Inform The Line Managers 0000194.
                //First Need To Find All The Line Managers For the This Employee..
                $PTable = 'reporting_heirarchy RH'; //Employee Table
                $selectData = array('E.full_name AS EmployeeName, ES.full_name AS SupervisorName, SCC.official_email AS SupervisorEmailAddress',false);
                $joins= array(
                    array(
                        'table' => 'employee E',
                        'condition' => 'E.employee_id = RH.for_employee AND E.trashed = 0 AND E.enrolled = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'employee ES',
                        'condition' => 'ES.employee_id = RH.employeed_id AND ES.trashed = 0 AND ES.enrolled = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'current_contacts SCC',
                        'condition' => 'SCC.employee_id = ES.employee_id AND SCC.trashed = 0',
                        'type' => 'INNER'
                    )
                );
                $where=array(
                    'RH.trashed' => 0,
                    'RH.for_employee' => $postedEmployeeID
                );
                $availableSupervisorsInfo = $this->common_model->select_fields_where_like_join($PTable,$selectData,$joins,$where);
                if(isset($availableSupervisorsInfo) && !empty($availableSupervisorsInfo)){
                    //If there is Data in It. Means We Have Line Managers For This Current Employee and We Need To Inform The Line Managers For Certain Application.
                    $supervisorMails = implode(',',array_column(json_decode(json_encode($availableSupervisorsInfo),true),'SupervisorEmailAddress'));

                    $config = $this->data['mailConfiguration'];
                    $this->load->library('email',$config);
                    $this->email->set_newline("\r\n");
                    //Now Lets Send The Email..
                    $this->email->to($supervisorMails);
                    $this->email->from('phrismapp@gmail.com','Phrism Administrator');
                    $this->email->subject('Employee Applied For Leave');
                    $this->email->message('Dear Sir/Madam, <br /> You Are Getting This Message because Employee Under Your Supervision Applied For Leave. Please Do Not Reply To This Email Address.');
                    if($this->email->send()) {
                        $msg ='Applied Successfully For Leaves, Also Email Has Been Dispatched To Respective Line Managers::success';
                        $this->session->set_flashdata('msg',$msg);
                    } else {
//                        echo $this->email->print_debugger();
                        $msg ='Applied Successfully For Leave, But Some problem Occurred during Sending of Email, Please Contact System Administrator For Further Assistance::warning';
                        $this->session->set_flashdata('msg',$msg);
/*                        echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                        return;*/
                    }
                }else{
                    $msg ='Successfully Applied For Leave, But No Line Managers Found To Send Emails To, Please Contact System Administrator For Further Assistance::warning';
                    $this->session->set_flashdata('msg',$msg);
                }
                redirect('leave_attendance/view_leave_attandance');
                return;
            }else{
                $msg ='Some Problem Occurred, System Could Not Create Leave Application, Please Contact System Administrator For Further Assistance::error';
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_attendance/view_leave_attandance');
                return;
            }
        }
    }//End Of Function


    // Function for Leave Action View //

    public function leave_action($emp_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);
            //$where = 'employee.employee_id ='.$emp_id;
            $where = 'application_id =' . $emp_id;
            $data['rec'] = $this->site_model->fetch_leave_act($where);
            $data['status'] = $this->site_model->html_selectbox('ml_status', '-- Select Status --', 'status_id', 'status_title');
            //echo "<pre>";print_r($data['rec']); die;
            $this->load->view('includes/header');
            $this->load->view('leave_attendence/leave_action', $data);
            $this->load->view('includes/footer');
        }

    }

    // Function for Add Leave Action //

    public function add_leave_action()
    {
        if ($this->input->post('add')) {
            $emp_id = $this->session->userdata('employee_id');
            //echo $emp_id; die;
            $add_type = array(
                'employment_id' => $emp_id,
                'no_of_paid_days' => $this->input->post('no_of_paid_days'),
                'no_of_un_paid_days' => $this->input->post('no_of_un_paid_days'),
                //'remarks'              =>    $this->input->post('remarks'),
                'remarks' => $this->input->post('remarks'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('leave_approval', $add_type);
            if ($query) {
                redirect('leave_attendance/view_leave_attandance');
            }
        }

    }

    // Function for Leave Status View //

    public function view_leave_status()
    {
        $full_name = $this->input->post('full_name');
        $data = $this->site_model->leave_staff_count();
        $count = count($data);
        $config['base_url'] = base_url() . 'leave_attendance/view_leave_status';
        $config['total_rows'] = $count;
        $config['per_page'] =15;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['links'] = $this->pagination->create_links();
        $data['leave_staff'] = $this->site_model->leave_staff($limit, $page, $full_name);
        $data['employee_id'] = $this->input->post('emp_id');
        $this->load->view('includes/header');
        $this->load->view('leave_attendence/leave_status', $data);
        $this->load->view('includes/footer');
    }

    // Function for Print Leave Status //

    public function print_leave_status($id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $id);
        $data['print'] = $this->site_model->print_leave_status($where);
        //echo "<pre>"; print_r($data['print']); die;
        $this->load->view('leave_attendence/print_leave_status', $data);
    }

    // Function for Print All Attendence //

    public function print_all_leave_status($id = NULL)
    {
       $data['employee_id'] = $this->uri->segment(3);
        //$where = array('employee.employee_id' => $id);

      $data['empl'] = $this->hr_model->get_row_by_where('employment',array('employee_id' => $id,'current'=> 1,'trashed'=>0));
 //print_r($data['empl']);die;
            $data['print'] = $this->site_model->print_all_leave_status();

        $this->load->view('leave_attendence/print_all_leave_status', $data);
    }


    // Function for Edit Leave Staff //

    public function edit_leave_staff($employee_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['employee_id'] = $this->uri->segment(3);
            $where = "employee.employee_id = " . $employee_id;
            $data['employee'] = $this->site_model->staff_infromation($where);
            //echo "<pre>"; print_r($data['employee']); die;
            if ($this->input->post('edit')) {
                $employee_code = $this->input->post('employee_code');
                $full_name = $this->input->post('full_name');
                $leave_type = $this->input->post('leave_type');
                $from_date = $this->input->post('from_date');
                $to_date = $this->input->post('to_date');
                $mobile_num = $this->input->post('mobile_num');
                $status = $this->input->post('status');
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_leave_staff($where, $employee_code, $full_name, $leave_type, $from_date, $to_date, $mobile_num, $status);
                if ($query) {
                    redirect('leave_attendance/view_leave_status/');
                }
            }
            $data['leave'] = $this->input->post('leave_type');
            //echo $data['group'];
            $data['leave_type'] = $this->site_model->drop_down3();
            $this->load->view('includes/header');
            $this->load->view('leave_attendence/edit_leave_staff', $data);
            $this->load->view('includes/footer');
        }

    }


    // Function for Attendance View //

    public function view_attendance()
    {
        //$Check will get all the leave approvals for the TimeSheets
        $data['check'] = $this->site_model->leave_application();

        //Get and Set All the Available Holidays

        $day = date('D');
        switch ($day) {
            case 'Mon':
                $day = 1;
                break;

            case 'Tue':
                $day = 2;
                break;

            case 'Wed':
                $day = 5;
                break;

            case 'Thu':
                $day = 4;
                break;

            case 'Fri':
                $day = 3;
                break;

            case 'Sat':
                $day = 6;
                break;

            case 'Sun':
                $day = 7;
                break;
        }
        $where = array('work_week_id' => $day);
        //Get Type of Days in Week Like HalfDay Full Day etc..
        $data['check3'] = $this->site_model->work_week($where);


        if($this->input->post()){
            //We Will Do Come Work here if Something is Posted.
            $full_name = $this->input->post('full_name');
            $data['select_assign'] = $this->input->post('project_title');
            $data['select_department'] = $this->input->post('department_name');
            $data['select_status'] = $this->input->post('status_type');
        }else{
            $full_name = '';
        }
        $data = $this->site_model->leave_attend_count();

        //Pagination For the Table..
        $count = count($data);
        $config['base_url'] = base_url() . 'leave_attendance/view_attendance';
        $config['total_rows'] = $count;
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['links'] = $this->pagination->create_links();
        $data['leave_attend'] = $this->site_model->leave_attend($limit, $page, $full_name, $where);
        //print_r($data['leave_attend']); die;
        $query=$this->db->last_query();
        $query=str_replace(", employee.employee_id", "",$query);

        //echo "<pre>";
       // print_r($data['leave_attend']); die;
        //var_dump($data['leave_attend']);
        $where = array('from_date' => date('Y-m-d'));
        $data['hhh'] = $this->site_model->holiday($where);
        $currentDate = date('Y-m-d');
        if(isset($data['hhh'])){
            $start_date  = $data['hhh'][0]->from_date;
            $end_date = $data['hhh'][0]->to_date;
            $holiday_type = $data['hhh'][0]->holiday_type;

        }else{
            $start_date ='';
            $end_date = '';
            $holiday_type = 0;
        }
        //StartDate And EndDate
        if (isset($data['hhh'])) {
            $start_date = strtotime($start_date);
            $end_date = strtotime($end_date);
        }


        $associatedArrays = json_decode(json_encode($data['leave_attend']), true);


        if (!empty($associatedArrays)) {
            foreach ($associatedArrays as $index => $value) {
                $user_date = strtotime($value['in_date']);
                if (($user_date >= $start_date) && ($user_date <= $end_date)) {
                    array_push($associatedArrays[$index], $holiday_type);
                } else {
                    array_push($associatedArrays[$index], $value['working_day']);
                }
            }
        }
        $data['leave_attend'] = $associatedArrays;
        $data['projects'] = $this->site_model->assign_projects();
        $data['status'] = $this->site_model->attendance_status_type();
        $data['select_department'] = $this->site_model->dropdown_ml_department();

        $data['employee_id'] = $this->input->post('emp_id');
        //var_dump($data['employee_id']);
        /// View For Pdf download
        $data['view']=$this->load->view('leave_attendence/pdf_attendance',$data,true);
        /// END



        // Code for CSV File
        if(!empty($data['leave_attend'])){
        //$query=$this->db->last_query();
        $this->load->dbutil();
        $this->load->helper('file');
        $newline="\r\n";
        $datas=$this->common_model->get_query_record($query);
        //$query=json_encode($query);
        $csv=$this->dbutil->csv_from_result($datas,',',$newline);
//        $csv = ltrim(strstr($this->dbutil->csv_from_result($data, ',', "\r\n"), "\r\n"));

        $this->load->helper('download');

        $uploadPath = 'systemGeneratedFiles/AttendanceCsvReports/'.$this->data['EmployeeID'].'/Attendance'.time().'.csv';
        $uploadDirectory = 'systemGeneratedFiles/AttendanceCsvReports/'.$this->data['EmployeeID'];
        //If Directories are Not Avaialable, Create The Directories On The Go..
        if(!is_dir($uploadDirectory)){
            mkdir($uploadDirectory, 0755, true);
        }

        $path= 'systemGeneratedFiles/AttendanceCsvReports/'.$this->data['EmployeeID'].'/';
        $delete_files= glob($path.'*');
        foreach($delete_files as $dFile){ // iterate files
            if(is_file($dFile))
                unlink($dFile); // delete file
            //echo $file.'file deleted';
        }

        //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
        //$fileName = ("Parexons_GeneratedReport_HR_CSV_'.time().'.csv",$csv);
        //$filePath = $uploadPath.$fileName;
        write_file($uploadPath,$csv, 'w+');
            $data['files']=$uploadPath;
        }


        //END

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/attendance', $data);
        $this->load->view('includes/footer');
    }

    // Function for PDF download Attendance
    public function downloadPdfAttendanceReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/AttendanceReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/AttendanceReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/AttendanceReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_Attendance_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }
    // END
//Function for Send pdf file of Time Sheet in email
    function SendPDFAttendanceReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/AttendanceReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/AttendanceReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'AttendanceReportEmail_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    // END
    //Sign Out All Employees If this Function is Invoked
    public function sign_out_all_employees(){
        if($this->input->is_ajax_request()){
//            $loggedInEmployee = $this->session->userdata('employee_id');

            /**
             * First We Need To Get All the Employees Who are Active and Have Projects Assigned To Them.
             */
            $table = 'employee E';
            $data = ('E.employee_id AS EmployeeID,
  E.full_name AS EmployeeNam,
  ATT.in_date AS EmployeeInDate,
  ATT.out_date AS EmployeeOutDate,
  ATT.in_time AS EmployeeInTime,
  ATT.out_time AS EmployeeOutTime,
  ATT.ml_month_id AS EmployeeMonthID,
  ATT.ml_year_id AS EmployeeYearID');

            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee_project EP',
                    'condition' => 'ET.employment_id = EP.employment_id AND EP.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'attendence ATT',
                    'condition' => 'ATT.employee_id = E.employee_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'posting P',
                    'condition' => 'P.employement_id = ET.employment_id AND P.current = 1 AND P.status = 2',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.enrolled' => 1
            );
            $group_by = 'E.employee_id';
            $availableEmployees = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);
            if(!is_array($availableEmployees) || empty($availableEmployees) || !isset($availableEmployees)){
                echo "FAIL::No Employees Exist To Sign Out::warning";
                return;
            }

            //Need AvailableEmployees in Associated Array for looping..
            $resultArray = json_decode(json_encode($availableEmployees),true);

//            $employee_ids = array_column($resultArray,'EmployeeID');
//            $employee_ids = implode(',',$employee_ids); // We Got The Employees Ids in CSV Format..

            /**
             * Now We Need To Get All Those Employees who are signed in and signed out in last two dates(it includes today's date and yesterday's date).
             */
            $checkedOutEmployees = $this->site_model->get_today_and_yesterday_checked_out_employees();

            //Now Need to Unset the Employees Who have CheckedOut..
              //First SetUp Dates In PHP for Todays and Yesterday
            $dates = array();
            $absentEmployeesData = array();
            array_push($dates,date('Y-m-d'));
            array_push($dates,date('Y-m-d',strtotime("-1 days")));
//            echo "checked Out Employees = ";print_r($checkedOutEmployees); echo "<br />";

/*            foreach ($dates as $date){
//                print_r($date);
                foreach($checkedOutEmployees as $empKey => $empValue){ //This Foreach is for Employees who did their DayIn and DayOut in last two days.
//                    print_r($AVEmpValues);
                        foreach($resultArray as $AVEmpKeys=>$AVEmpValues){ //This Foreach is For All Available Employees..
//                            print_r($AVEmpValues);
                            if($AVEmpValues['EmployeeOutDate'] !== $empValue['date'] && $AVEmpValues['EmployeeID'] !== $empValue['EmployeeID']){
                                $arrayToPush = array(
                                    'EmployeeID' => $AVEmpValues['EmployeeID'],
                                    'AbsentDate' => $date
                                );
                                array_push($absentEmployeesData,$arrayToPush);
                            }
                        }
                }
            }*/
            foreach($dates as $date){
                foreach($resultArray as $AVEmpKeys=>$AVEmpValues){ //This Foreach is For All Available Employees
                    if(isset($checkedOutEmployees) && is_array($checkedOutEmployees)){
                        foreach($checkedOutEmployees as $empKey => $empValue){ //This Foreach is For Who Did their there DayIn and DayOut in Last TwoDays
                            if($AVEmpValues['EmployeeOutDate'] == $empValue['date'] && $AVEmpValues['EmployeeID'] == $empValue['EmployeeID']){
                                unset($resultArray[$AVEmpKeys]);
                            }
                        }
                    }
                }
            }

            foreach ($dates as $date) {
                if (isset($resultArray) && !empty($resultArray) && is_array($resultArray)) {
                    foreach ($resultArray as $AVEmpKeys => $AVEmpValues) {
                        $arrayToPush = array(
                            'EmployeeID' => $AVEmpValues['EmployeeID'],
                            'AbsentDate' => $date
                        );
                        array_push($absentEmployeesData, $arrayToPush);
                    }
                }
            }

            if(isset($absentEmployeesData) && is_array($absentEmployeesData) && !empty($absentEmployeesData)){
                //Now Need To Do the insertions
                foreach($absentEmployeesData as $InsertDataValue){
                    //Check if the Record Exists.
                    $table = 'attendence';
                    $data = ('attendence_id');
                    $where = array(
                        'employee_id' => $InsertDataValue['EmployeeID'],
                        'in_date' => $InsertDataValue['AbsentDate']
                    );
                    $result = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                    if(isset($result) && !empty($result)){ //If Result is Not Empty We Need To Do Updations..
                        $data = array(
                            'remarks' => 'Auto Updated Absented',
                            'attendance_status_type' => 2
                        );
                        $this->common_model->update($table,$where,$data);
                    }else{ //Else We Will Do the Insertions....
                        $getMonthYear = explode("-",$InsertDataValue['AbsentDate']);
                        $month_id = intval($getMonthYear[1]);
                        //Need To Get Year ID
                        $year = $getMonthYear[0];
                        $table = 'ml_year';
                        $data = ('ml_year_id AS YearID');
                        $where = array(
                            'year' => $year,
                            'trashed' => 0,
                            'enabled' => 1
                        );
                        $yearResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                        $year_id = $yearResult->YearID;
                        ///Turns Out We Need to Get the Work ID Also.. :)
                        //So Now 1 more query for workID
                            $table = 'work_week';
                        $dayName = date("l",strtotime($InsertDataValue['AbsentDate']));
                        $data = ('work_week_id AS WorkWeekID, working_day AS WorkingDay');
                        $where = array(
                            'work_week' => $dayName,
                            'trashed' => 0,
                            'enabled' => 1
                        );
                        $workWeekResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                        if(!$workWeekResult){
                            echo "FAIL::No Work Weeks Found";
                            return;
                        }

                        //As We Got Both Month and Year IDs plus other Required Data, So Now We Will Do The Insertions..
                        $table = 'attendence';
                        $DataArray = array(
                            'employee_id' => $InsertDataValue['EmployeeID'],
                            'in_date' => $InsertDataValue['AbsentDate'],
                            'remarks' => 'Auto Inserted Absented',
                            'ml_month_id' => $month_id,
                            'ml_year_id' => $year_id,
                            'attendance_status_type' => 2,
                            'work_week_id' => $workWeekResult->WorkWeekID
                        );
                        $this->common_model->insert_record($table,$DataArray);
                    }
                }
                echo "OK::Employees Successfully Signed Out::success";
                return;
            }else{
                echo "OK::No Employees To Absent::error";
                return;
            }
        }
    }

    function importAttendanceFromFile(){
        if($this->input->is_ajax_request()){
            //If It Is Ajax Request and it is Post Request Then We Should Continue with the Work..
            if(!$this->input->post()){
                echo "FAIL::You Must Fill All The Required Fields To Import Data From File::error";
                return;
            }
            $importingFromDate = $this->input->post('ImportFromDate');
            $importingToDate = $this->input->post('ImportToDate');

            //To Insert Data, We First Need To Check If Data Already Exist In DB For the Dates Selected..
            $selectTable = 'attendence';
            $selectData = 'COUNT(1) AS TotalRecords';
            $selectWhere = 'in_date BETWEEN "'.$importingFromDate.'" AND "'.$importingToDate.'"';
            $result = $this->common_model->select_fields_where($selectTable,$selectData,$selectWhere,TRUE);
            if($result->TotalRecords > 0){
                echo "FAIL::Records Already Exist Against The Selected Selected Dates::error";
                return;
            }

            //Now If Dates Do Not Exist For The Dates That Are Setup In The TextBox, Then this Below Section Should Execute..
            if(!isset($importingFromDate) || empty($importingFromDate)){
                echo "FAIL::You Must Select The Start Date For Importing To Work::error";
                return;
            }
            if(!isset($importingToDate) || empty($importingToDate)){
                echo "FAIL::You Must Select The End Date For Importing To Work::error";
                return;
            }

            //Ok As We Got Data From Both Dates, We Must Move Forward To Next Step, Working With The File..
            if(isset($_FILES['file']) && $_FILES['file']['size'] > 0){
                //As The File is Selected For Importing So Lets Start The Work on Importing Of The File..
                //
                //We First Need To Check The Type Of the File.. Only CSV and Excel Files Should Be Allowed TO Import.
                //
                $allowedExt = array('csv','txt','xlsx','xls');
                $FileName = $_FILES['file']['name'];
                $ext = end(explode('.',$FileName));
                if(!in_array(strtolower(end(explode('.',$FileName))),$allowedExt))
                {
                    echo "FAIL:: Only CSV ('.csv','.txt') and Excel('xlsx','xls') Files Are Allowed For Importing::error";
                    return;
                }
                //AS File Passes The Above Security For Extension, We Need To Upload The File to its Appropriate Directory.
                $LoggedInEmployeeID = $this->data['EmployeeID'];
                $uploadPath = './upload/employeesData/Imports/'.$LoggedInEmployeeID.'/';
                $uploadDirectory = './upload/employeesData/Imports/'.$LoggedInEmployeeID;
                $FileName = "Parexons_HRMS_".$LoggedInEmployeeID."_".time().".".$ext;
                //UploadPath, UploadDirectory and FileNames Are Set, But We Need To Check If Directory Exist and It is Not Write Protected.
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Move The Uploaded Path To Its Own Directory Which We Assigned To This File..
                move_uploaded_file($_FILES['file']['tmp_name'],$uploadPath.$FileName);


                //Uploaded FilePath..
                $filePath = $uploadPath.$FileName;

                /**
                 * I have Programmed For Importing From CSV, But Still Missed The Huge Part, Importing From Excel File..
                 */

                //Checking If File is Excel Format Or Not.
                $excelExtensions = array('xlsx','xls');

                if(in_array(strtolower(end(explode('.',$FileName))),$excelExtensions)){
                    //If Excel File Is Uploaded, Then Hell of Lot of More Work Needs To Be Done To Convert It To CSV First :(
                    $this->load->library('excel');
                    $this->excel=PHPExcel_IOFactory::load(FCPATH.$filePath);
                    //$this->excel=PHPExcel_IOFactory::identify(FCPATH.$filePath);
                    //activate worksheet number 1
                    $this->excel->setActiveSheetIndex(0);

                    $objWriter = $this->excel=PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
                    //Need To Change The File Format From Xcel To CSV Also..
                    $ConvertedFileName = explode('.',$FileName);
                    $poppedExtension = array_pop($ConvertedFileName);
                    array_push($ConvertedFileName,'csv');
                    $ConvertedFileName = 'ConvertedToCSV_'.implode('.',$ConvertedFileName); //Now The File Format Will Be In CSV
                    $ConvertFolderPath = $uploadPath.'ConvertedFiles/';
                    $ConvertFolderDirectory = $uploadPath.'ConvertedFiles';
                    //If Directory Do Not Exist For Converted Files. Then Create The Directory For Them..
                    if(!is_dir($ConvertFolderDirectory)){
                        mkdir($ConvertFolderDirectory, 0755, true);
                    }
                    $outputPath = $ConvertFolderPath.$ConvertedFileName;
                    $objWriter->save($outputPath);
                    if(!file_exists($outputPath)){
                        echo "FAIL::File Could Not Be Converted To CSV Format For Import, Please Contact System Administrator For Further Assistance.::error";
                        return;
                    }
                    //If File uploaded Successfully Then We Will Gonna Make Some Hula Lala Lala Work.. :)
                    $filePath = $outputPath;
                }

                //Now As The File is Also Been Uploaded, Need To Import Data That Exists In File..
                //Loading Import Library..
                $this->load->library('csvimport');

                if ($this->csvimport->get_array($filePath)) {
                    $csv_array = $this->csvimport->get_array($filePath);
//                    print_r($csv_array);
                    $fileDataInArray =array();
                    foreach($csv_array as $key=>$value){
                        $fileDataInArray[$key]['EmployeeCode'] = $value['EmployeeCode'];
                        if(isset($value['InDateTime']) && !empty($value['InDateTime'])){
                            $dateTime = new DateTime($value['InDateTime']);
                            $fileDataInArray[$key]['InDate'] = $dateTime->format('Y-m-d');
                            $fileDataInArray[$key]['InTime'] = $dateTime->format('H:i:s');
                        }

                        if(isset($value['OutDateTime']) && !empty($value['OutDateTime'])){
                            $dateTime = new DateTime($value['OutDateTime']);
                            $fileDataInArray[$key]['OutDate'] = $dateTime->format('Y-m-d');
                            $fileDataInArray[$key]['OutTime'] = $dateTime->format('H:i:s');
                        }
                    }

                    //Now If We Need To Import The Employees ID's, Then We Will Compare The Employee ID's With Employee Codes.
                    //Getting All Employee Codes For Which We Need Employee ID's
//                    $employeeCodes In File
                    $employeeCodes = '"' . implode('","',array_unique(array_column($fileDataInArray,'EmployeeCode'))) . '"';

                    //If array is not empty and data successfully Imported From File. Then Get Employee ID's For Employee Codes.

                    $employeeTable = 'employee E';
                    $selectData = 'E.employee_id AS EmployeeID, E.full_name AS EmployeeName, E.employee_code AS EmployeeCode';
                    $where = 'E.trashed = 0 AND E.enrolled = 1 AND E.employee_code IN ('.$employeeCodes.')';
                    $employeesData = $this->common_model->select_fields_where($employeeTable, $selectData, $where);
//                    var_dump($employeesData);
                    if(isset($employeesData) and !empty($employeesData)){
                        $employeesDataToInsert = array();
                        /**
                         * As To Save Some Time From Rebuilding The Attendance View and Other Modules.
                         * We Needed To Do Changes Here In Controller
                         * To Save From Killing The Database From Alot Of Queries for Every Employee Code We Have To Do 1 Query And Compare Two Different Arrays And Create A Third Array From Two Arrays.
                         * But Also Needs To Do Kind Of Same Stuff For MonthID And YearID..
                         */

                        //Getting MonthID's And YearID's
                        //MonthData
                        $monthTable = 'ml_month';
                        $monthSelectData = 'ml_month_id AS MonthID, month AS MonthName';
                        $where = array(
                            'trashed' => 0
                        );
                        $monthTableData = $this->common_model->select_fields_where($monthTable, $monthSelectData, $where);
                        if(!isset($monthTableData) || empty($monthTableData)){
                            //Do Stuff Here if No Result Is Found.
                            echo "FAIL::Months Master List is Not Set in Database::error";
                            return;
                        }

                        //YearData
                        $yearTable = 'ml_year';
                        $yearSelectData = 'ml_year_id AS YearID, year AS YearValue';
                        $where = array(
                            'trashed' => 0
                        );
                        $yearTableData = $this->common_model->select_fields_where($yearTable, $yearSelectData, $where);
                        if(!isset($yearTableData) || empty($yearTableData)){
                            //Do Stuff Here if No Result Is Found.
                            echo "FAIL::Year Master List is Not Set in Database::error";
                            return;
                        }

                        //Need WorkWeek Data Also.. :)
                        $workWeekTable = 'work_week';
                        $workWeekSelectData = 'work_week_id AS WorkWeekID, work_week AS WorkingDayName, working_day AS WorkingDayType';
                        $where = array(
                            'trashed' => 0
                        );
                        $workWeekTableData = $this->common_model->select_fields_where($workWeekTable,$workWeekSelectData,$where);
                        if(!isset($workWeekTableData) || empty($workWeekTableData)){
                            echo "FAIL::WorkWeeks Are Not Set in Database..";
                        }


                        //Now Here Comes The Big Part, Set Up Things If All the Required Data Is Present Then Create The Array For Insertion in To Database...
                        foreach($fileDataInArray as $employeeKey=>$employeeValues){
                            foreach($employeesData as $key=>$value){
                                if($value->EmployeeCode === $employeeValues['EmployeeCode']){
                                    foreach($yearTableData as $year){
                                        if($year->YearValue === date("Y",strtotime($employeeValues['InDate']))){
                                            $yearID = $year->YearID;
                                        }
                                    }
                                    foreach($monthTableData as $month){
                                        if($month->MonthName === date("M",strtotime($employeeValues['InDate']))){
                                            $monthID = $month->MonthID;
                                        }
                                    }
                                    foreach($workWeekTableData as $workWeek){
                                        if($workWeek->WorkingDayName === date("l",strtotime($employeeValues['InDate']))){
                                            $workWeekID = $workWeek->WorkWeekID;
                                        }
                                    }
                                    $employeeData = array(
                                        'EmployeeID' => intval($value->EmployeeID),
                                        'InTime' => $employeeValues['InTime'],
                                        'InDate' => $employeeValues['InDate'],
                                        'OutTime' => $employeeValues['OutTime'],
                                        'OutDate' => $employeeValues['OutDate'],
                                        'YearID' => isset($yearID)?$yearID:'(NULL)',
                                        'MonthID' => isset($monthID)?$monthID:'(NULL)',
                                        'WorkWeekID' => isset($workWeekID)?$workWeekID:'(NULL)'
                                    );
//                                    array_push($employeesDataToInsert,$employeeData);
                                    $employeesDataToInsert[$value->EmployeeID.'::'.$employeeValues['InDate']] = $employeeData;
                                }
                            }
                        }
                        //Sort Array Ascending..
                        array_sort_by_column($employeesDataToInsert,'InDate');
                        //As We Got The Array That We Need To Insert, But Only 1 Stuff Remaining That is To Absent or Present Data..
                        //To Absent Employees We Need To Have The Dates From Which Date To Which Date User Wants To Do The Entry.
                        $begin = new DateTime($importingFromDate);
                        $end = new DateTime($importingToDate);
                        $end = $end->modify( '+1 day' ); //Adding Extra 1 Day as Difference Misses 1 Date..

                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                        $totalEmployeesToInsert = array_count_values(array_column($employeesDataToInsert,'EmployeeID')) ;
//                        print_r($totalEmployeesToInsert);
                        //Three Nested Foreach Loops.. But If i find out a better solution then will implement it later on only if that solution don't take much time.
                        //This Here are the Dates to which if employee is if Present Mark Him/Her Present or else Mark Absent.
                        foreach ($totalEmployeesToInsert as $countedKey => $CountedValues) {
                            foreach ($employeesDataToInsert as $key => $empValues) {
                                foreach ($period as $dt) {
                                    if (intval($empValues['EmployeeID']) === intval($countedKey) && date("Y-m-d",strtotime($empValues['OutDate'])) === $dt->format("Y-m-d")) {
                                        $employeesDataToInsert[$key]['AttendanceStatus'] = 1;
                                    }elseif(intval($empValues['EmployeeID']) === intval($countedKey) && date("Y-m-d",strtotime($empValues['OutDate'])) !== $dt->format("Y-m-d")){
                                        if(!array_key_exists($countedKey.'::'.$dt->format("Y-m-d"),$employeesDataToInsert)){
                                            foreach($yearTableData as $year){
                                                if($year->YearValue === $dt->format("Y")){
                                                    $yearID = $year->YearID;
                                                }
                                            }
                                            foreach($monthTableData as $month){
                                                if($month->MonthName === $dt->format("M")){
                                                    $monthID = $month->MonthID;
                                                }
                                            }
                                            foreach($workWeekTableData as $workWeek){
                                                if($workWeek->WorkingDayName === $dt->format("l")){
                                                    $workWeekID = $workWeek->WorkWeekID;
                                                }
                                            }
                                            $absentEmployeeData = array(
                                                'EmployeeID' => intval($countedKey),
                                                'InTime' => '00:00:00',
                                                'InDate' => $dt->format("Y-m-d"),
                                                'OutTime' => '00:00:00',
                                                'OutDate' => '0000-00-00',
                                                'AttendanceStatus' => 2,
                                                'YearID' => isset($yearID)?$yearID:'(NULL)',
                                                'MonthID' => isset($monthID)?$monthID:'(NULL)',
                                                'WorkWeekID' => isset($workWeekID)?$workWeekID:'(NULL)'
                                            );
                                            $employeesDataToInsert[$countedKey.'::'.$dt->format("Y-m-d")] = $absentEmployeeData;
                                        }
                                        if(!array_key_exists('AttendanceStatus',$employeesDataToInsert[$key])){
                                            $employeesDataToInsert[$key]['AttendanceStatus'] = 2;
                                        }
                                    }
                                }
                            }
                        }
                        $employeesDataToInsert = array_order_by($employeesDataToInsert, 'EmployeeID', SORT_ASC, 'InDate', SORT_ASC);
//                        print_r($employeesDataToInsert);
                        //Need To Change Key Values For Database Columns
                        $newEmployeeDataToInsert = array();
                        foreach($employeesDataToInsert as $key=>$value){
                            $arrayToInsert = array(
                                'employee_id' => $value['EmployeeID'],
                                'in_time' => $value['InTime'],
                                'in_date' => $value['InDate'],
                                'out_time' => $value['OutTime'],
                                'out_date' => $value['OutDate'],
                                'attendance_status_type' => $value['AttendanceStatus'],
                                'ml_month_id' => $value['MonthID'],
                                'ml_year_id' => $value['YearID'],
                                'work_week_id' => $value['WorkWeekID']
                            );
                            $newEmployeeDataToInsert[$key] = $arrayToInsert;
                        }
                        //Time To Do The Entry Finally, Now Will Gonna Do The Batch Insertions..
                        $insertTableAttendance = 'attendence';
                        $result = $this->common_model->insert_multiple_ignore_duplicate($insertTableAttendance,$newEmployeeDataToInsert);
                        //Better Function Would Have Been Top One, But Insert Ignore Still Gives Error On DuplicateEntry..
                        if($result !== FALSE){
                            echo "OK::Records Successfully Uploaded To The System::success";
                            return;
                        }elseif($result === FALSE){
                            echo "FAIL::Some DataBase Error, Records Could Not Be Updated, Please Contact System Administrator For Further Assistance.::error";
                            return;
                        }
                    }else{
                        //Do Stuff If It Don't Match With Any Employee..
                        echo "FAIL::No EmployeeCode in File Matched With the Employees in The System::error";
                        return;
                    }
                } else {
                    $data['error'] = "Error occured";
                    $this->load->view('csvindex', $data);
            }

            }else{
                echo "FAIL::No File Selected, You Must Select the appropriate File To Import::error";
            }
        }else{

        }
    }

    /// Function for This Day ///

    public function get_day_status($holiday = NULL, $work_week = NULL)
    {
        if ($holiday || $work_week) {
            if (!empty($holiday) && empty($work_week)) {
                $where = " ml_holiday.ml_holiday_id = '$holiday'";
                $data['present'] = $this->site_model->holiday($where);
            }
            if (empty($holiday) && !empty($work_week)) {
                $where = "work_week.work_week_id = '$work_week'";
                $data['present'] = $this->site_model->work_week($where);
            }
        }
    }

    // Function for Print Attendence //

    public function print_attendence($id = NULL)
    {
        $data['employee_id'] = $this->uri->segment(3);


        $where = array('employee.employee_id' => $id);
        $data['print'] = $this->site_model->print_attendence($where);

        //echo "<pre>"; print_r($data['print']); die;
        $this->load->view('leave_attendence/print_attendence', $data);
    }

    // Function for Print All Attendence //

    public function print_all_attendance($id = NULL)
    {
         $data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $id);
                $data['present'] = $this->site_model->print_attendence_present($where);

            $this->load->view('leave_attendence/print_all_attendence', $data);
    }


    // Function for Time Sheet view //

    public function view_timesheet()
    {
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $data['months'] = $this->input->post('month');
        $data['years'] = $this->input->post('year');
        $full_name = $this->input->post('full_name');
        $leave_type = $this->input->post('leave_type');
        //$data['emp_test'] = $this->site_model->get_emp_name($full_name, $month, $year, $leave_type);
        //print_r($data['emp_test']);die;
        //echo "<pre>"; print_r($full_name); die;
        //$emp = array('employee.employee_id' => $this->session->userdata('employee_id'));

        $data['time_sheet'] = $this->site_model->time_sheet($full_name);

        //$data['emp'] = $this->site_model->get_row_by_id('employee', $emp);
        //$indate = $this->input->post('in_date');

        //echo "<pre>"; print_r( $data['time_sheet']); die;

        $data['month'] = $this->site_model->month_same();
        $data['year'] = $this->site_model->year_same();

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/timesheet', $data);
        $this->load->view('includes/footer');
    }


    // Function for Print Time Sheet //

    public function print_timesheet($id = NULL)
    {
        //$data['employee_id'] = $this->uri->segment(3);
        $where = array('employee.employee_id' => $this->uri->segment(3));
        $data['print'] = $this->site_model->print_timesheet($where);
        //echo "<pre>"; print_r($data['print']); die;
        $this->load->view('leave_attendence/print_timesheet', $data);
    }


    // Function for Standard Time Sheet View //

    public function view_standard_timesheet($param = NULL)
    {
        $this->load->library('datatables');
        if ($param == 'list') {
            echo $this->site_model->standard_timesheet();
            die;
        }
        $data['month'] = $this->site_model->html_selectbox('ml_month', '-- Select Month --', 'ml_month_id', 'month');
        $data['year'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');

        $data['select_month'] = $this->input->post('month');
        $data['select_year'] = $this->input->post('year');

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/stan_timesheet', $data);
        $this->load->view('includes/footer');
    }


    public function print_all_standard_timesheet($month = NULL, $year = NULL)
    {
        //echo $this->input->post('in_date')."<br>adfghj".$in_id;die;
        if ($month || $year) {
            if (empty($month) && empty($year)) {
                $where = "ml_month.ml_month_id = '$month'
					AND
					ml_year.ml_year_id = '$year'";
                $data['present'] = $this->site_model->print_standard_timesheet($where);
                //echo "<pre>";print_r($data['present']); die;
            } else if (!empty($month) && !empty($year)) {
                $where = "ml_month.ml_month_id = '$month'";
                $data['present'] = $this->site_model->print_standard_timesheet($where);
                //echo "<pre>";print_r($data['present']); die;
            }
            $this->load->view('leave_attendence/print_all_standard_timesheet', $data);
        }
    }


    // Function for Accumulative Time Sheet View // 

    public function view_acumulative($emp1 = NULL)
    {
        $emp = array('employee.employee_id' => $this->session->userdata('employee_id'));
        if ($emp1 == "list") {
            echo $this->site_model->standard_timesheet($emp);
            die;
        }
        //$data['monthly_std_hrs'] = $this->input->post('monthly_std_hrs');

        $data['monthly_std_hrs'] = $this->site_model->html_select_box('ml_total_month_hour', 'monthly_std_hrs_id', 'monthly_std_hrs');
        $data['standard'] = $this->site_model->get_one('ml_total_month_hour');
        //echo "<pre>";print_r($data['monthly_std_hrs']); die;
        $data['month'] = $this->site_model->html_selectbox('ml_month', '-- Select Month --', 'ml_month_id', 'month');
        $data['year'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');
        $this->load->view('includes/header');
        $this->load->view('leave_attendence/ac_timesheet', $data);
        $this->load->view('includes/footer');
    }

    // Function for Add Leave Action //

    public function add_accumulative()
    {
        if ($this->input->post('add')) {
            //$emp_id= $this->session->userdata('employee_id');
            $emp_id = $this->input->post('emp_id');
            $postedMonth = $this->input->post('month');
            $postedYear = $this->input->post('year');
            $existingSessionArray = $this->session->all_userdata();
            if(array_key_exists("AccumulativeTimeSheet-".$emp_id."-".$postedMonth."-".$postedYear,$existingSessionArray)){
               //Update If key Exist..
                $msg = "Can Not Add Entry For Same Employee in Same Month::error";
                $this->session->set_flashdata('msg',$msg);
                //echo "AccumulativeTimeSheet-".$emp_id."-".$postedMonth."-".$postedYear,$existingSessionArray;
                redirect('leave_attendance/view_acumulative');
                return;
            }else{
                //Insert If Key Do Not Already Exist..
                $add_type = array(
                    'employee_id' => $emp_id,
                    'work_hours' => $this->input->post('work_hours'),
                    'leave_hours' => $this->input->post('leave_hours'),
                    'monthly_std_hrs_id' => $this->input->post('monthly_std_hrss'),
                    'ml_month_id' => $this->input->post('month'),
                    'ml_year_id' => $this->input->post('year')
                );
                $table = 'hourly_time_sheet';
                $insertResult = $this->common_model->insert_record($table,$add_type);
                if($insertResult > 0){
                    $TimeSheetID = $insertResult;
                }else{
                    $msg = "Some DataBase Error Occurred, Can Not Add Certain Record::error";
                    $this->session->set_flashdata('msg',$msg);
                    return;
                }
            }


            //Need Employee Details On Base Of Employee ID Given
            $table = 'employee';
            $selectData = 'full_name AS FullName';
            $where = array(
              'employee_id' => $emp_id
            );
            $employee = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);

            //Need Month And Year Details
            $monthTable = 'ml_month';
            $selectData = 'month AS MonthName';
            $where = array(
                'ml_month_id' => $postedMonth
            );
            $monthResult = $this->common_model->select_fields_where($monthTable,$selectData,$where,TRUE);

            $yearTable = 'ml_year';
            $selectData = 'year AS CompleteYear';
            $where = array(
                'ml_year_id' => $postedYear
            );
            $yearResult = $this->common_model->select_fields_where($yearTable,$selectData,$where,TRUE);

            //Need Standard Hours
            $standardHoursTable = 'ml_total_month_hour MLTMH';
            $data = 'monthly_std_hrs';
            $where = array(
                'monthly_std_hrs_id' => $this->input->post('monthly_std_hrss'),
                'trashed' => 0
            );
            $standardHoursResult = $this->common_model->select_fields_where($standardHoursTable,$data,$where,TRUE);
            //End Of Queries Finally..

            $add_arr_see = array(
                "AccumulativeTimeSheet-".$emp_id."-".$postedMonth."-".$postedYear => array(
                    'employeeID' => $emp_id,
                    'TimeSheetID' => $TimeSheetID,
                    'employeeName' => $employee->FullName,
                    'WorkHours' => $this->input->post('work_hours'),
                    'LeaveHours' => $this->input->post('leave_hours'),
                    'TotalHours' => $standardHoursResult,
                    'Month' => $monthResult->MonthName,
                    'Year' => $yearResult->CompleteYear
                )
            );
                $this->session->set_userdata($add_arr_see);
                redirect('leave_attendance/view_acumulative');
        }

    }


    // Function for Edit Leave Staff //

    public function edit_accumulative($employee_id = NULL, $hour_timeSheet_id = NULL)
    {
        if ($employee_id != NULL && is_numeric($employee_id) && $employee_id > 0) {
            $data['employee_id'] = $employee_id;
            $where = array('hourly_time_sheet.hour_time_sheet_id '=>  $this->uri->segment(4));
            $data['employee'] = $this->site_model->standard_timesheet_edt($where);
            $where_hour = "hour_time_sheet_id =" . $hour_timeSheet_id;

            if ($this->input->post('edit') and is_numeric($employee_id) and $employee_id > 0 and isset($hour_timeSheet_id) and $hour_timeSheet_id > 0) {

                //To Update We First Need The Old Month and Year ID
                $oldMonthID = $data['employee']->ml_month_id;
                $oldYearID = $data['employee']->ml_year_id;

                //Now Lets Start Getting Data For New Entries..
                $month = $this->input->post('month');
                $year = $this->input->post('year');
                $work_hours = $this->input->post('work_hours');
                $leave_hours = $this->input->post('leave_hours');

                //Setting The MultiDimensional Array

                //Need Employee Details On Base Of Employee ID Given
                $table = 'employee';
                $selectData = 'full_name AS FullName';
                $where = array(
                    'employee_id' => $employee_id
                );
                $employee = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);

                //Need Month And Year Details
                $monthTable = 'ml_month';
                $selectData = 'month AS MonthName';
                $where = array(
                    'ml_month_id' => $month
                );
                $monthResult = $this->common_model->select_fields_where($monthTable,$selectData,$where,TRUE);

                $yearTable = 'ml_year';
                $selectData = 'year AS CompleteYear';
                $where = array(
                    'ml_year_id' => $year
                );
                $yearResult = $this->common_model->select_fields_where($yearTable,$selectData,$where,TRUE);

                //Need Standard Hours
                $standardHoursTable = 'ml_total_month_hour MLTMH';
                $selectData = 'monthly_std_hrs';
                $where = array(
                    'monthly_std_hrs_id' => $this->input->post('monthly_std_hrss'),
                    'trashed' => 0
                );
                $standardHoursResult = $this->common_model->select_fields_where($standardHoursTable,$selectData,$where,TRUE);

                //End Of Queries Finally..
                $sessionKey = "AccumulativeTimeSheet-".$employee_id."-".$oldMonthID."-".$oldYearID;
                //Unset The Old Record in Session..
                $this->session->unset_userdata($sessionKey);
                //Now Create The New Record In The Session.
                $add_arr_see = array(
                    "AccumulativeTimeSheet-".$employee_id."-".$month."-".$year => array(
                        'employeeID' => $employee_id,
                        'TimeSheetID' => $hour_timeSheet_id,
                        'employeeName' => $employee->FullName,
                        'WorkHours' => $this->input->post('work_hours'),
                        'LeaveHours' => $this->input->post('leave_hours'),
                        'TotalHours' => $standardHoursResult,
                        'Month' => $monthResult->MonthName,
                        'Year' => $yearResult->CompleteYear
                    )
                );
                $this->session->set_userdata($add_arr_see);

                $query = $this->site_model->update_accumulative($where_hour, $month, $year, $work_hours, $leave_hours);
                if ($query) {
                    redirect('leave_attendance/view_acumulative/');
                }
            }
            $data['month'] = $this->site_model->html_selectbox('ml_month', '-- Select Month --', 'ml_month_id', 'month');
            $data['year'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');
            $this->load->view('includes/header');
            $this->load->view('leave_attendence/edit_accumulative', $data);
            $this->load->view('includes/footer');
        }

    }
    function clearHourlyTimeSheetSession(){
        if($this->input->is_ajax_request()){
            //If The Request Is Generated From Ajax Then Below Code Should Execute..
            $allSessionData = $this->session->all_userdata();
            $arraysUnset = 0;
            foreach($allSessionData as $key=>$value){
                if("AccumulativeTimeSheet-" === substr($key,0,22)){
                    $this->session->unset_userdata($key);
                    $arraysUnset++;
                }
            }
            if($arraysUnset > 0){
                echo "OK::Successfully Cleared The Table::success";
                return;
            }elseif($arraysUnset === 0){
                echo "FAIL::There is Nothing To Clear In The Table::warning";
                return;
            }

        }
    }


    // Function for Auto Complete Time Sheet //

    function autocomplete_time_sheet()
    {
        //$this->load->model('model','get_data');
        //echo $this->input->post('queryString');die;
        $query = $this->site_model->get_autocomplete_time_sheet();
       // echo "<pre>"; print_r($query); die;
        $i = 1;
        //var_dump($query);
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name; ?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }





    // Function for Auto Complete Time Sheet //

    function autocomplete_attendence_name()
    {
        //$this->load->model('model','get_data');
        //echo $this->input->post('queryString');die;
        $query = $this->site_model->get_autocomplete_attendence();
        // echo "<pre>"; print_r($query); die;
        $i = 1;
       ///var_dump($query);
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name; ?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }

    function autocomplete_employee_name()
    {
        //$this->load->model('model','get_data');
        //echo $this->input->post('queryString');die;
        $query = $this->site_model->get_autocomplete_name();
        // echo "<pre>"; print_r($query); die;
        $i = 1;
        ///var_dump($query);
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name; ?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }


    function autocomplete_apply_name()
    {

        $query = $this->site_model->get_autocomplete_apply_for_leave();
        // echo "<pre>"; print_r($query); die;
        $i = 1;
        ///var_dump($query);
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>" data-id="<?php echo $row->employee_id; ?>" class="employeeValue"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>',this)"><?php echo $row->full_name; ?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }
     function selectLeavesForSelectedEmployee(){
        if($this->input->is_ajax_request()){
            if($this->input->post('employeeID')){
                $employeeID = $this->input->post('employeeID');
                $PTable = "employee E";
                $data = ('MLT.leave_type AS Category, LE.no_of_leaves_allocated AS Allocated');
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'E.employee_id = ET.employee_id AND ET.current = 1 AND ET.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'leave_entitlement LE',
                        'condition' => 'ET.employment_id = LE.employment_id AND LE.status = 2 AND LE.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'ml_leave_type MLT',
                        'condition' => 'MLT.ml_leave_type_id = LE.ml_leave_type_id AND MLT.trashed = 0',
                        'type' => 'INNER'
                    )

                );
                $where = array(
                    'E.employee_id' => $employeeID
                );
                $result = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where);
//                var_dump($result);
                print_r(json_encode($result));
            }
        }
    }

    // Function for Add Time Sheet //

    public function add_time_sheet()
    {
        if ($this->input->post('add')) {
            $emp_id = $this->session->userdata('employee_id');
            $add_type = array(
                'employment_id' => $emp_id,
                'monthly_std_hrs_id' => $this->input->post('monthly_std_hrs'),
                'work_hours' => $this->input->post('work_hours'),
                'leave_hours' => $this->input->post('leave_hours'));
            //'note'           =>    $this->input->post('note')
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('hourly_time_sheet', $add_type);
            if ($query) {
                redirect('leave_attendance/view_standard_timesheet');
            }
        }

    }

    // Function for Leave Status View //

    public function view_over_time()
    {
        $year = $this->input->post('year');
        $full_name = $this->input->post('full_name');
        $month = $this->input->post('month');

        $data = $this->site_model->over_time_model_count();

        $count = count($data);
        $config['base_url'] = base_url() . 'leave_attendance/view_over_time';
        $config['total_rows'] = $count;
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data['links'] = $this->pagination->create_links();
        $data['over_time'] = $this->site_model->over_time_model($limit, $page, $full_name, $month, $year);
        $data['month'] = $this->site_model->month_same();
        $data['year'] = $this->site_model->year_same();

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/over_time', $data);
        $this->load->view('includes/footer');
    }

    // Function for add over time

    public function add_over_time()
    {
        $date = $this->input->post('indate');
        $employeeID = $this->input->post('hr_emp_id');
        if(!isset($employeeID) || empty($employeeID))
        {
            $msg ="Please Select Above Filters !::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('leave_attendance/view_timesheet');
        }
        $add_type = array(
            'employee_id' => $employeeID,
            'ml_month_id' => $this->input->post('hr_month'),
            'ml_year_id' => $this->input->post('hr_year'),
            'sanction_hours' => $this->input->post('sanction_hours')
        );
       echo "<pre>";($add_type);die;
        //print_r($add_type);
        //die;
        //echo $hours=$this->input->post('sanction_hours');

        $yearexp = date("Y", strtotime($date));

        $where = array('ml_year.year' => $yearexp);

        $eyear = $this->site_model->get_row_by_id('ml_year', $where);
        $year = $eyear->ml_year_id;
        $query = $this->site_model->create_new_record('over_time', $add_type);
        if ($query) {
            redirect('leave_attendance/view_timesheet');
        } else {
            echo "technical error !";
        }

    }

    // Function for Leave Entitlement View //

    public function view_leave_entitlement($param=null)
    {

        $year = $this->input->post('year');
        $full_name = $this->input->post('full_name');

        $table = 'leave_entitlement LE';
        $selectData = array('E.employee_id AS ID,E.employee_code, E.full_name, LE.no_of_leaves_allocated',false);
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employment_id =  LE.employment_id AND ET.trashed = 0 AND  ET.current =1',
                'type' => 'INNER'
            ),
              array(
                  'table' => 'employee E',
                  'condition' => 'E.employee_id =  ET.employee_id AND E.trashed = 0 AND  E.enrolled =1',
                  'type' => 'INNER'
              ),
            //my changes goese here..
            array(
                  'table' => 'leave_application',
                  'condition' => 'leave_application.entitlement_id=LE.leave_entitlement_id',
                  'type' => 'left'
              ),array(
                  'table' => 'leave_approval',
                  'condition' => 'leave_approval.employment_id=ET.employment_id AND leave_approval.status=2',
                  'type' => 'left'
              )
        );
        $where = array(
            'LE.trashed' => 0,
            'LE.status' => 2
        );

        //$add_column=array('ViewEditActionButtons'=>array('<a style="cursor:pointer;"
        //href="leave_attendance/view_leave_entitlement_detail/$1">
        //<span data-toggle="tooltip" data-placement="right" data-original-title="View Records" class="fa fa-pencil-square-o"></span></a>','ID'));
        $data = $this->common_model->select_fields_where_like_join($table,$selectData,$joins,$where,FALSE,'','','E.employee_id');
       //echo "<pre/>";
       //print_r($data);

        //$result = $this->Common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$add_column);
        //print $result;
       // return;
        //}
        //print_r($data);
/*        //@function leave_entitlement_cound is deprecated.
        $data = $this->site_model->leave_entitlement_count();*/

        // Can be Re Re-Use when neede...
        $count = count($data);
        $config['base_url'] = base_url() . 'leave_attendance/view_leave_entitlement';
        $config['total_rows'] = $count;
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data['links'] = $this->pagination->create_links();
        $data['leave_entitlement'] = $this->site_model->leave_enetitlement($limit, $page, $full_name, $year);

        $data['year'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');


        $this->load->view('includes/header');
        $this->load->view('leave_attendence/leave_entitlement',$data);
        $this->load->view('includes/footer');
    }

    // Function for Leave Entitlement Details //

    public function view_leave_entitlement_detail($employee_id = NULL)
    {   $monthYear = date("Y-m-d");
        $monthYearData = explode("-", $monthYear);
        $currentMonth = $monthYearData[1];
        $currentYear = $monthYearData[0];
        $loggedInEmployee = $this->session->userdata('employee_id');

        $data['complete_profile'] = $this->site_model->leave_enetitlement_detail($currentYear,$employee_id);

        /*Below: take personal information of employee*/
        $whereId=array('employee_id'=>$employee_id);
        $data['employeInfo'] = $this->site_model->GetEmployeePersonalDetails('employee',$whereId);
        //var_dump($data['employeInfo']);die;
        /*end*/
        $this->load->view('includes/header');
        $this->load->view('leave_attendence/leave_entitlement_detail', $data);
        $this->load->view('includes/footer');
    }


    public function add_leave_earned()
    {
        $date = $this->input->post('indate');
        if ($this->input->post('leav_add')) {
            $query_emp = $this->site_model->get_row_by_id('employee', array('employee_id' => $this->input->post('full_name')));


             $employmentID= $this->input->post('leave_entitle');
            if(!isset($employmentID) || empty($employmentID))
            {
                $msg ="Please Select Above Filters !::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('leave_attendance/view_timesheet');
            }
            $add_type = array(
                'employment_id' => $employmentID,
                'no_of_leaves_allocated' => $this->input->post('no_of_leaves'),
                'ml_leave_type_id' => 3
            );
            $yearexp = date("Y", strtotime($date));

            $where = array('ml_year.year' => $yearexp);

            $eyear = $this->site_model->get_row_by_id('ml_year', $where);
            $year = $eyear->ml_year_id;
            $query = $this->site_model->create_new_record('leave_entitlement', $add_type);

            if ($query) {
                redirect('leave_attendance/view_timesheet');
            }
        }

    }


    // Function for Auto Complete Leave_Entitlement //

    function autocomplete_leave_enetitlement()
    {
        $query = $this->site_model->get_auto_leave_entit();
        $i = 1;
        if (!empty($query)):
//            var_dump($qurery)
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name;?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }

    // Function for Auto Complete Leave Attendance //

    function autocomplete_leave_attendence()
    {
        $query = $this->site_model->get_auto_leave_attendence();
        $i = 1;
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name;?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }
    // Function for Auto Complete Over Time //

    function autocomplete_over_time()
    {
        $query = $this->site_model->get_auto_over_time();
        $i = 1;
        if (!empty($query)):
            foreach ($query as $row):
                ?>
                <li tabindex="<?php echo $i; ?>"
                    onclick="fill('<?php echo $row->full_name; ?>','<?php echo $row->employee_id; ?>')"><?php echo $row->full_name;?></li> <?php
                //echo "<li id='".$row->employee_id."'>".$row->full_name."</li>";
            endforeach;
        endif;
    }

///////////////////////////////////////// View Employee rocord/Details ////////////////////////////////////////////////

    public function view_employee_leave($application_id = NULL)
    {


        $where = array(
           'leave_application.application_id' => $application_id
      );

        $data['complete_profile'] = $this->site_model->view_leave_detail($where);
        //echo "<pre>"; print_r($data['complete_profile']);die;

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/view_detail_leave_application', $data);
        $this->load->view('includes/footer');
    }

    public function manual_timesheet()
    {
        $monthYear = date("Y-m-d");
        $monthYearData = explode("-", $monthYear);
        $currentMonth = $monthYearData[1];
        $currentYear = $monthYearData[0];
        $loggedInEmployee = $this->session->userdata('employee_id');
        $monthYear = $this->input->post('selectedMonthYear');
        if (isset($monthYear) && !empty($monthYear)) {
            $monthYearData = explode(",", $monthYear);
            $month = $monthYearData[0];
            $monthNumeric = date("m", strtotime($month));
            $year = $monthYearData[1];
            $viewData['filterInfo'] = $monthYearData;
            if(is_admin($loggedInEmployee)){
                if(isset($monthYearData[2]) && !empty($monthYearData[2])){
                    $employeeID = $monthYearData[2];
                }
            }
            if(isset($employeeID) && !empty($employeeID)){
                $where = array(
/*                    "DATE_FORMAT(TS.date_created,'%M')" => $monthYearData[0],
                    "DATE_FORMAT(TS.date_created,'%Y')" => $monthYearData[1],*/
                    'E.employee_id' => $employeeID
                );
            }else{
                $where = array(
/*                    "DATE_FORMAT(TS.date_created,'%M')" => $monthYearData[0],
                    "DATE_FORMAT(TS.date_created,'%Y')" => $monthYearData[1],*/
                    'E.employee_id' => $loggedInEmployee
                );
            }

            $viewData['monthYear'] = $monthYearData;
        } else {
            $currentYearMonth = date('Y-F');
            $currentMonthYearData = explode("-", $currentYearMonth);
            $month = $currentMonthYearData[1];
            $monthNumeric = date("m",strtotime($month));
//            var_dump($monthNumeric);
            $year = $currentMonthYearData[0];
            if(isset($employeeID) && !empty($employeeID)){
                $where = array(
                    'E.employee_id' => $employeeID
                );
            }else{
                $where = array(
                    'E.employee_id' => $loggedInEmployee
                );
            }

        }
        $PTable = 'employee E';
        $data = ('
   TSD.timesheetDetails_id,
   EP.project_id AS ProjectID,
   TSD.date AS WorkDate,
   TSD.hours AS WorkHours,
   TS.date_created AS TimeSheetMonth,
   TS.employment_id AS EmployeeID,
   TS.timesheet_status_id AS TimeSheetStatus,
   TS.id AS TimeSheetID,
   MLP.project_title AS ProjectTitle,
   E.full_name AS EmployeeName
');
        $joins = array(
            array(
                'table' => 'employment EMP',
                'condition' => 'EMP.employee_id = E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'timesheet TS',
                'condition' => 'TS.employment_id = EMP.employment_id AND DATE_FORMAT(TS.date_created,"%Y") = ' . $year,
                'type' => 'INNER'
            ),
            array(
                'table' => 'timesheet_details TSD',
                'condition' => 'TS.id = TSD.timesheet_id AND DATE_FORMAT(TSD.date,"%M") = "' . $month . '" AND DATE_FORMAT(TSD.date,"%Y") = ' . $year,
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'TSD.project_id = MLP.project_id',
                'type' => 'INNER'
            ),

            array(
                'table' => 'employee_project EP',
                'condition' => 'MLP.project_id = EP.project_id AND EMP.employment_id = EP.employment_id AND EP.current=1',
                'type' => 'INNER'
            )
        );
//        $group_by = 'TSD.timesheetDetails_id, TSD.project_id';
        $group_by = '';
        $viewData['TimeSheetViewData'] = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, FALSE, '', '', $group_by);

        //Employee Data
        $table = 'employee';
        $data = ('full_name AS EmployeeName,employee_id AS EmployeeID,employee_code AS EmployeeCode');
        if(isset($employeeID) && !empty($employeeID)){
            $where = array(
                'employee_id' => $employeeID
            );
        }else{
            $where = array(
                'employee_id' => $loggedInEmployee
            );
        }

        $viewData['employeeData'] = $this->common_model->select_fields_where($table, $data, $where, TRUE);

        //Need to Get Leave Details Now.
        if(isset($employeeID) && !empty($employeeID)){
            $employmentID = get_employment_from_employeeID($employeeID);
            $viewData['employeeTotalLeaves'] = $this->site_model->getTotalEmployeeLeaves($monthNumeric, $year, $employeeID,$employmentID);
            //Bottom
            $viewData['employeeTotalMonthlyAndYearly'] = $this->site_model->getTotalEmployeeLeavesNew($monthNumeric, $year, $employeeID,$employmentID);
        }else{
            $loggedInEmploymentID = get_employment_from_employeeID($loggedInEmployee);
            $viewData['employeeTotalLeaves'] = $this->site_model->getTotalEmployeeLeaves($monthNumeric, $year, $loggedInEmployee,$loggedInEmploymentID);
            //Bottom
            $viewData['employeeTotalMonthlyAndYearly'] = $this->site_model->getTotalEmployeeLeavesNew($monthNumeric, $year, $loggedInEmployee,$loggedInEmploymentID);
        }

/*        echo "<pre>";
        echo "<br />employment ID is = ".$employmentID;
        var_dump($viewData['employeeTotalLeaves']);
        echo "<br />";
        echo $this->db->last_query();
                exit;*/


        ///Need The Colors For TimeSheet Leave Types.
        $table = 'ml_leave_type';
        $data = array('ml_leave_type_id AS LeaveTypeID, leave_type AS LeaveType, hex_color AS LeaveTypeColor', false);
        $where = array(
            'trashed' => 0
        );
        $result = $this->common_model->select_fields_where($table,$data,$where);
        if(isset($result) && !empty($result)){
            $viewData['LeaveTypes'] = $result;
        }//End Of Color Getting for Leaves


        //Need Holiday Dates
        $table = "ml_holiday";
        $holidaySelectData = array('ml_holiday_id, holiday_type as HolidayType, from_date, to_date',false);
        $where = array(
            'trashed' => 0,
            'enabled' => 1
        );

        $holidaysData = $this->common_model->select_fields_where($table,$holidaySelectData,$where);
        echo "<pre>";
//        var_dump($holidaysData);
        echo "</pre>";
        if(isset($holidaysData) && !empty($holidaysData)){
            $viewData['HolidaysData'] = $holidaysData;
        }

        $viewData['pdf_view']=$this->load->view('leave_attendence/manual_timesheet_pdf',$viewData,true);
        //print_r($viewData['pdf_view']);

        $this->load->view('includes/header');
        $this->load->view('leave_attendence/manual_timesheet',$viewData);
        $this->load->view('includes/footer');
    }

    //Function for Send pdf file of Time Sheet in email
    function SendPDFEmployeeTimeSheetReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');


                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
                //echo $mailFrom;die;
                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                //print_r($viewDataForPDF);die;
                $uploadPath = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';

                $uploadDirectory = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'ManualTimeSheetReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $this->data['mailConfiguration'] = array(
                    'protocol' => 'mail',
                    'mailpath' => '/usr/sbin/sendmail',
                    'wordwrap' => 'TRUE',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
              /*  $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );*/
                $this->load->library('email',$this->data['mailConfiguration']);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..
                //print_r($mailTo);die;
                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }

                $this->email->subject($messageSubject);

                $this->email->message($messageBody);

                $this->email->attach($filePath);
                //print_r($filePath);die;
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
            }
        }
    }
    // END

    public function sendManualTimeSheetForApproval(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                //As Request is Coming through Proper Channel, So We Will Process The Request Now.
                $timeSheetID = $this->input->post('timeSheetID');
                $table = 'timesheet';
                //Lets First Check If Its Already Been Approved Or Sent For Approval?
                $selectData = 'timesheet_status_id AS TimeSheetStatus';
                $where = array(
                  'id' => $timeSheetID
                );
                $statusInfo = $this->common_model->select_fields_where($table,$selectData,$where,TRUE);
                if(isset($statusInfo) && intval($statusInfo->TimeSheetStatus) === 1){
                    echo "FAIL::TimeSheet Already Been Sent For Approval::error";
                    return;
                }elseif(isset($statusInfo) && intval($statusInfo->TimeSheetStatus) === 2){
                    echo "FAIL::TimeSheet Already Have Been Approved, Can Not Send TimeSheet Back For Approval::error";
                    return;
                }

                $data = array(
                    'timesheet_status_id'=> 1,
                    'date_updated' => $this->data['dbCurrentDate']
                );
                $where = array(
                    'id' => $timeSheetID
                );
                $result = $this->common_model->update($table,$where,$data);
                if($result === true){
                    echo "OK::TimeSheet Successfully Sent For Approval::success";
                    return;
                }else{
                    echo "FAIL::Some Database Error Occurred, Please Contact System Administrator For Further Assistance::error";
                    return;
                }
            }
        }
    }

    public function downloadPdfTimeSheetReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                /*$this->load->library('dompdf_gen');
                ini_set('memory_limit','32M');
                $dompdf = new DOMPDF();
                //$dompdf = $this->dompdf_gen->load();
                $dompdf->load_html($viewDataForPDF);
                $dompdf->render();
                $output = $dompdf->output();
                file_put_contents('systemGeneratedFiles/ManualTimeSheetReportPdf/file.pdf', $output);*/

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">

                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_ManualTimeSheet_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }

    public function load_availableMonths_in_timeSheet()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $dependent = $this->input->post('dependent');
                $table = 'timesheet T';
                $data = array(
                    "T.id AS TimeSheetID, DATE_FORMAT(T.date_created,'%M') AS MonthName",
                    false
                );
                $group_by = "MonthName";
                $where = array(
                    "DATE_FORMAT(T.date_created,'%Y')" => $dependent["text"]
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = "DATE_FORMAT(T.date_created,'%Y')";
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where, FALSE, '', '', '', $group_by);
                }
                print_r((json_encode($result)));
            }
        }
    }

    public function load_availableYears_in_timeSheet()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $dependent = $this->input->post('dependent');
                $table = 'timesheet T';
                $data = array(
                    "T.id AS TimeSheetID, DATE_FORMAT(T.date_created,'%Y') AS YearValue",
                    false
                );
                $group_by = "YearValue";
                if(isset($dependent) && !empty($dependent)){
                    $where = array(
                        'T.employment_id' => get_employment_from_employeeID($dependent['id'])
                    );
                }else{
                    $where = array(
                        'T.employment_id' => get_employment_from_employeeID($this->session->userdata('employee_id'))
                    );
                }
                if (isset($sValue) && !empty($sValue)) {
                    $field = "DATE_FORMAT(T.date_created,'%Y')";
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
//                    $result = $this->common_model->select_fields($table, $data, FALSE, $group_by);
                    $result = $this->common_model->select_fields_where($table,$data,$where,FALSE,'','','',$group_by);
                }
                print_r((json_encode($result)));
            }
        }
    }

    public function updateTimeSheetInfo()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $dateOfHoursWorked = $this->input->post('dateOfHoursWorked');
                $hoursWorked = $this->input->post('hoursWorked');
                $projectID = $this->input->post('projectID');
                $postedEmployeeID = $this->input->post('EmployeeID');
                if(isset($postedEmployeeID) && !empty($postedEmployeeID)){
                    $loggedInEmployee = $postedEmployeeID;
                }else{
                    $loggedInEmployee = $this->session->userdata('employee_id');
                }
                $employmentID = get_employment_from_employeeID($loggedInEmployee);
                if (!isset($loggedInEmployee) && !is_numeric($loggedInEmployee)) {
                    echo "FAIL::Session is No Longer Available, Please Login Again To Fix this Problem::error";
                    return;
                }
                if (!isset($dateOfHoursWorked)) {
                    echo "FAIL::Data is Not Posted Properly::error";
                    return;
                }
                if (!isset($hoursWorked)) {
                    echo "FAIL::Data is Not Posted Properly::error";
                    return;
                }
                if (!isset($projectID)) {
                    echo "FAIL::Data is Not Posted Properly::error";
                    return;
                }
                //We Need To Check If Record Already Exist for timeSheet..
                $dateInArray = explode("-", $dateOfHoursWorked);
                $postedYear = $dateInArray[0];
                $postedMonth = $dateInArray[1];
                $postedDay = $dateInArray[2];

                $PTable = 'timesheet T';
                $data = array('COUNT(1) AS TotalRecords, IFNULL(T.timesheet_status_id,0) AS Status, T.id AS TimeSheetID', false);
                $joins = array(
                    array(
                        'table' => 'timesheet_details TSD',
                        'condition' => 'TSD.timesheet_id = T.id',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'T.employment_id' => $employmentID,
                    'TSD.project_id' => $projectID,
                    'YEAR(TSD.date)' => $postedYear,
                    'MONTH(TSD.date)' => $postedMonth
                );
                $countResult = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, TRUE);
//                print_r($countResult);
                if ($countResult->TotalRecords > 0 && intval($countResult->Status) !== 2) {
                    $queryType = 'Update';
                } elseif ($countResult->TotalRecords > 0 && intval($countResult->Status) === 2) {
                    $queryType = 'Approved';
                } else {
                    $queryType = 'Insert';
                }
                if ($queryType === 'Update') {
                    $where = array(
                        'T.employment_id' => $employmentID,
                        'TSD.project_id' => $projectID,
                        'YEAR(TSD.date)' => $postedYear,
                        'MONTH(TSD.date)' => $postedMonth,
                        'DAY(TSD.date)' => $postedDay
                    );
                    $countResultWithDay = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, FALSE);
                    if ($countResultWithDay[0]->TotalRecords > 0) {
                        $tsDetailsQuery = 'tsUpdate';
                    } else {
                        $tsDetailsQuery = 'tsInsert';
                    }
                } elseif ($queryType === 'Insert') {
                    //As this is Insertion Process cuz no Data Found for the date and project in time sheet for logged-in
                    //--employee.So i would do insertion 1 by 1 to both tables.
                    //First Insertion for Table TimeSheet.
                    $tbl = 'timesheet';
                    $data = array(
                        'employment_id' => $employmentID,
                        'timesheet_status_id' => 1,
                        'date_created' => date('Y-m-d')
                    );
                    $tInsertStatus = $this->common_model->insert_record($tbl, $data);
                    if ($tInsertStatus > 0) {
                        $tbl = 'timesheet_details';
                        $data = array(
                            'project_id' => $projectID,
                            'timesheet_id' => $tInsertStatus,
                            'date' => $postedYear . '-' . $postedMonth . '-' . $postedDay,
                            'hours' => $hoursWorked
                        );
                        $tdInsertStatus = $this->common_model->insert_record($tbl, $data);
                        if ($tdInsertStatus > 0) {
                            echo "OK::Record Successfully Added::success";
                            return;
                        }

                    } else {
                        echo "FAIL::Some Database Error, Record Could Not Be Added::error";
                    }

                } elseif ($queryType === 'Approved') {
                    echo "FAIL::Can Not Update an Approved Time Sheet::error";
                    return;
                }
                if ($tsDetailsQuery === 'tsUpdate') {
                    $tbl = 'timesheet_details TSD';
                    $where = array(
                        'TSD.project_id' => $projectID,
                        'TSD.date' => $postedYear . '-' . $postedMonth . '-' . $postedDay,
                        'TSD.timesheet_id' => $countResultWithDay[0]->TimeSheetID
                    );
                    $data = array(
                        'hours' => intval($hoursWorked)
                    );
                    $updateStatus = $this->common_model->update($tbl, $where, $data);
                    if ($updateStatus === true) {
//                        print_r($this->db->last_query());
                        echo "OK::Record Successfully Updated::success";
                        return;
                    }
                } elseif ($tsDetailsQuery === 'tsInsert') {
                    //If No Record Exist in TimeSheetDetails for the Posted Date and Details Then New Record Will Be Inserted.
                    $tbl = 'timesheet_details';
                    $data = array(
                        'project_id' => intval($projectID),
                        'date' => $postedYear . '-' . $postedMonth . '-' . $postedDay,
                        'timesheet_id' => $countResult->TimeSheetID,
                        'hours' => intval($hoursWorked)
                    );
                    $insertStatus = $this->common_model->insert_record($tbl, $data);
                    if ($insertStatus > 0) {
                        echo "OK::New Record Added Successfully::success";
                        return;
                    }
                }
            }
        }
    }

    public  function loadAvailableEmployees(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $sValue = $this->input->post('term');

                $tbl = 'employee E';
                $data = ('E.full_name AS EmployeeName, E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.thumbnail AS EmployeeAvatar');
                $where = array(
                    'trashed' => 0
                 );
                if(isset($sValue) && !empty($sValue)){
                    $field = 'E.full_name';
                    $result = $this->common_model->select_fields_where_like($tbl,$data,$where,FALSE,$field,$sValue);
                }else{
                    $result = $this->common_model->select_fields_where($tbl,$data,$where);
                }

                //Need To Replace The EmployeeAvatar Key Value (Image Name To Image Path)
                foreach($result as $key=> $val){
                    $empAvatar = empAvatarExist($val->EmployeeAvatar);
                    if($empAvatar === TRUE){
                        $result[$key]->EmployeeAvatar = base_url('upload/Thumb_Nails/'.$val->EmployeeAvatar);
                    }else{
                        $result[$key]->EmployeeAvatar = $empAvatar;
                    }
                }
                print_r(json_encode($result));
            }
        }
    }
    public  function createTimeSheetForAllRemainingEmployees(){
        if($this->input->is_ajax_request()){
            $PTable = 'employee E';
            $data = ('ET.employment_id');
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee_project EP',
                    'condition' => 'ET.employment_id = EP.employment_id AND EP.enabled = 1 AND EP.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = "E.trashed = 0 AND E.enrolled = 1 AND MONTH(EP.project_assign_date) <= MONTH(NOW())";
            $group_by = 'E.employee_id';
            $result = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,FALSE,'','',$group_by);
            if(!isset($result) || empty($result)){
                echo "FAIL::Employees Do Not Have Any Projects Assigned For Current Month::error";
                return;
            }

            $resultAssociatedArray = json_decode( json_encode($result),true);
            $employeeIDs = implode(',',array_column($resultAssociatedArray,'employment_id'));

            //Now Need to Insert The Record in the TimeSheet if the Record do Not Match
            //Need To Get the Employees Whose TimeSheets Already Exist.
            $table = 'employee E';
            $data = array('ET.employment_id',false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'timesheet T',
                    'condition' => 'T.employment_id = ET.employment_id',
                    'type' => 'INNER'
                )
            );
            $where = 'ET.employment_id IN ('.$employeeIDs.')  AND DATE_FORMAT(T.date_created, "%Y-%m") = DATE_FORMAT(NOW(), "%Y-%m")';
            $group_by = 'E.employee_id';
            $employeesTimeSheetsAlreadyCreated = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,FALSE,'','',$group_by);

            $employeesTimeSheetsAlreadyCreatedArray = json_decode(json_encode($employeesTimeSheetsAlreadyCreated),true);
/*            print_r($employeesTimeSheetsAlreadyCreatedArray);
            return;*/

            /**
             * Now As We Got The Employees Whose TimeSheet Already Exist for the Current Month
             * So We will Unset those Employees From the Arrays Of Employees who Have Been Assigned Projects
             */
            $employeesWithProjects = $result;
            $existingEmployeeTimeSheets = $employeesTimeSheetsAlreadyCreated;

/*            print_r($employeesWithProjects);
            print_r($existingEmployeeTimeSheets);
            return;*/

            if ($existingEmployeeTimeSheets) {
                foreach ($existingEmployeeTimeSheets as $ETKey => $ETValue) {
                    foreach ($employeesWithProjects as $EPKey => $EPValue) {
                        if (isset($ETValue) && isset($EPValue)) {
                            if ($ETValue->employment_id == $EPValue->employment_id) {
                                unset($employeesWithProjects[$EPKey]);
                            }
                        }
                    }
                }
            }

            if(empty($employeesWithProjects)){
                echo "FAIL::Employees TimeSheets Already Been Created::warning";
                return;
            }
            $TimeSheetEmployeesToInsert = json_decode(json_encode($employeesWithProjects),true);
//            $employeeToInsertIDs = implode(",",array_column($TimeSheetEmployeesToInsert,'employee_id'));
            $employeeToInsertIDs = array_column($TimeSheetEmployeesToInsert,'employment_id');



//            Insertion of Employees
            //Creating the ArrayData for Batch Insertion
            foreach ($TimeSheetEmployeesToInsert as $key => $empIDs) {
                $TimeSheetEmployeesToInsert[$key]['date_created'] = date("Y-m-d");
                $TimeSheetEmployeesToInsert[$key]['timesheet_status_id'] = 0;
            }
            $this->db->trans_start();
             $table = 'timesheet';
            $data = array();
            $result = $this->common_model->insert_multiple($table,$TimeSheetEmployeesToInsert);

            //Now Insert A single Record to EverNew Record Created In TimeSheet.
            //First Get the ProjectIDs for Every Employee.
            foreach ($employeeToInsertIDs as $empID){
                $PTable = 'employee_project EP';
                $data = ('GROUP_CONCAT(DISTINCT EP.project_id) AS ProjectIDs');
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'EP.employment_id = ET.employment_id AND ET.current = 1',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'employee E',
                        'condition' => 'EP.employment_id = ET.employment_id AND E.enrolled = 1 AND E.trashed = 0',
                        'type' => 'INNER'
                    )
                );
                /*$where = array(
                    'ET.employment_id' => $empID,
                    'EP.trashed' => 0,
                    'EP.current' => 1
                );*/
                $where = "ET.employment_id = ".$empID." AND EP.trashed = 0 AND EP.current = 1 AND MONTH(EP.project_assign_date) <= MONTH(NOW())";
                $group_by = 'E.employee_id';
                $resultProjectID = $this->common_model->select_fields_where_like_join($PTable,$data,$joins,$where,TRUE,'','',$group_by);
                if(isset($resultProjectID) && !empty($resultProjectID)){
                    $projectIDs = $resultProjectID->ProjectIDs;

                    //We Got the ProjectIDs, but we require the TimeSheetID for which we want to create the Details of TimeSheet..
                    $table = 'timesheet T';
                    $data = ('id AS TimeSheetID');
                    $where = array(
                        'T.employment_id' => $empID,
                        'DATE(T.date_created)' => date("Y-m-d")
                    );
                    $resultTimeSheetID = $this->common_model->select_fields_where($table,$data,$where,TRUE);
                    $timeSheetID = $resultTimeSheetID->TimeSheetID;

                    if(isset($projectIDs) && !empty($projectIDs)){
                        $projectIDsArray = explode(',',$projectIDs);
                        $timeSheetsDetailsDataArray = array();
                        foreach ($projectIDsArray as $pKey=>$pVal){
                            $arrayToPush = array(
                                'timesheet_id' => $timeSheetID,
                                'project_id' => $pVal,
                                'date' => date("Y-m-").'1',
                                'hours' => 00
                            );
                            array_push($timeSheetsDetailsDataArray,$arrayToPush);
                        }
                    }
                    $timeSheetDetailsTable = 'timesheet_details';
                    $result = $this->common_model->insert_multiple($timeSheetDetailsTable,$timeSheetsDetailsDataArray);
                }
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                echo "FAIL::Some Database Error, Record Could Not Be Updated::error";
            }else{
                echo "OK::Time Sheets Successfully Created For Employees::success";
            }
            return;
        }
    }
}