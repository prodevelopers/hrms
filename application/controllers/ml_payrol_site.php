<?php 
class ml_payrol_site extends MY_Controller{
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('site_model');

		
		if($this->session->userdata('logged_in') != TRUE)
		{
			redirect('login');
		}
	}
/*////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*////////////////////////////////////////////// Payroll Master List /////////////////////////////////////////////////////////////////*/
/*////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
         
		    // Function for Pay Grade viewing //
  	public function view_payroll_ml()
	    {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
		$data{'data'} = $this->site_model->get_all_by_where('ml_pay_grade',$where);
        $this->load->view('includes/header');
		$this->load->view('payroll_ml/payroll_master_lists', $data);
		$this->load->view('includes/footer');
		}

	public function AddAjustmentHead()
	{
		if($this->input->post('add'))
		{
			$adjustmentTitle = $this->input->post('adjustmentTitle');

			$InputData=array('adjustmentTitle'=>$adjustmentTitle);
			$query=$this->common_model->insert_record('ml_adjustment_head',$InputData);
			if($query)
			{
				$alert = "Record Added Successfully!::success";
				$this->session->set_flashdata('msg',$alert);
				redirect('ml_payrol_site/AddAjustmentHead');
			}

		}
		$SelectData = "adjustmentID,adjustmentTitle,IsLive";
		$where = array('trash'=>0);
		$data['AjustmentHead']=$this->common_model->select_fields_where('ml_adjustment_head',$SelectData,$where);
		//print_r($data['AjustmentHead']);
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/adjustmenthead_master_lists',$data);
		$this->load->view('includes/footer');
	}

	public function UpdateAjustmentHead($param=null)
	{
		if($this->input->post('update'))
		{

			$adjustmentID = $this->input->post('adjustmentID');
			$adjustmentTitle = $this->input->post('adjustmentTitle');
			$IsLive = $this->input->post('IsLive');
			$InputUpdate = array(
					'adjustmentTitle'=>$adjustmentTitle,
					'IsLive'=>$IsLive
			);
			$whereID = array('adjustmentID'=>$adjustmentID);
			$query= $this->common_model->update('ml_adjustment_head',$whereID,$InputUpdate);
			if($query)
			{
				$alert = "Record Updated Successfully!::success";
				$this->session->set_flashdata('msg',$alert);
				redirect('ml_payrol_site/AddAjustmentHead');

			}

		}
		if(!empty($param))
		{
			$where = array('adjustmentID'=>$param);
			$SelectData = "adjustmentID,adjustmentTitle,IsLive";
			$data['AjustmentHead']=$this->common_model->select_fields_where('ml_adjustment_head',$SelectData,$where,true);
			//print_r($data['AjustmentHead']);
		}
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/updateadjustmenthead_master_lists',$data);
		$this->load->view('includes/footer');

	}
	public function DeleteAjustmentHead($param=null)
	{
		if(!empty($param))
		{
			$where = array('adjustmentID'=>$param);
			$UpdateInfo = array('trash'=>1);
			$query= $this->common_model->update('ml_adjustment_head',$where,$UpdateInfo);
			if($query)
			{
				$alert = "Record Deleted Successfully!::success";
				$this->session->set_flashdata('msg',$alert);
				redirect('ml_payrol_site/AddAjustmentHead');
			}
		}
	}

	public function AddAjustmentType()
	{
		if($this->input->post('add'))
		{
			$typeTitle = $this->input->post('typeTitle');
			$InputData = array('typeTitle'=>$typeTitle);
			$query = $this->common_model->insert_record('ml_adjustment_type',$InputData);
			if($query)
			{
				$alert = "Record Added Successfully!::success";
				$this->session->set_flashdata('msg',$alert);
				redirect('ml_payrol_site/AddAjustmentType');
			}
		}

		$SelectData = "adjustmentTypeID,typeTitle,IsLive ";
		$where = array('trash'=>0);
		$data['Ajustment']=$this->common_model->select_fields_where('ml_adjustment_type',$SelectData,$where);
		//print_r($data['Ajustment']);
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/adjustmenttype_master_lists',$data);
		$this->load->view('includes/footer');

	}

	public function UpdateAjustmentType($param=null)
	{
		if($this->input->post('update')) {
			$adjustmentTypeID = $this->input->post('adjustmentTypeID');
			$whereID =array('adjustmentTypeID'=>$adjustmentTypeID);
			$typeTitle = $this->input->post('typeTitle');
			$IsLive = $this->input->post('IsLive');
			$InputData = array(
					'typeTitle' => $typeTitle,
					'IsLive' => $IsLive
			);

			$query = $this->common_model->update('ml_adjustment_type', $whereID, $InputData);
			if ($query) {
				$alert = "Record Updated Successfully!::success";
				$this->session->set_flashdata('msg', $alert);
				redirect('ml_payrol_site/AddAjustmentType');
			}
		}
		if(!empty($param))
		{
			$where = array('adjustmentTypeID'=>$param);
			$SelectData = "adjustmentTypeID,typeTitle,IsLive";
			$data['Ajustment']=$this->common_model->select_fields_where('ml_adjustment_type',$SelectData,$where,true);
			//print_r($data['Ajustment']);
		}
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/updateadjustmenttype_master_lists',$data);
		$this->load->view('includes/footer');

	}

	public function DeleteAjustmentType($param=null)
	{
		if(!empty($param))
		{
			$where = array('adjustmentTypeID'=>$param);
			$UpdateInfo = array('trash'=>1);
			$query= $this->common_model->update('ml_adjustment_type',$where,$UpdateInfo);
			if($query)
			{
				$alert = "Record Deleted Successfully!::success";
				$this->session->set_flashdata('msg',$alert);
				redirect('ml_payrol_site/AddAjustmentType');
			}
		}
	}
       
	    	// Function for Leave Type Addition // 
	
	public function add_pay_grade($id = NULL)
	    {
		if($this->input->post('add'))
        {

            $paygrade = $this->input->post('pay_grade');
            if(!isset($paygrade) || empty($paygrade) || !is_numeric($paygrade)){
                $msg ="Please Enter Value In Number.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payroll_ml');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_pay_grade';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(pay_grade)' => strtolower($paygrade),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payroll_ml');
                return;
            }

			$add_type = array (
		    'pay_grade' =>  $paygrade
            );
	     $query = $this->site_model->create_new_record('ml_pay_grade', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_payroll_ml');
		}
	}
 }
    
		// Function for Edit ML Pay Grade //
	
	public function edit_paygrade_ml($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payroll_ml');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['payroll'] = $this->uri->segment(3);
		$where = array('ml_pay_grade_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_pay_grade', $where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'pay_grade' => $this->input->post('pay_grade'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_pay_grade', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_payroll_ml/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_paygrade_ml', $data);
		$this->load->view('includes/footer');	
}
	
	
	 // Function for Trash Pay Grade //

     public function trashed_pay_grade($id = NULL,$tableName = NULL)
       {

         if($tableName === NULL || !is_string($tableName))
           {
               $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('ml_payrol_site/view_payroll_ml/');
               return;
           }

           if($id === NULL || !is_numeric($id))
           {
               $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('ml_payrol_site/view_payroll_ml/');
               return;
           }



           //Position Management.
           $selectTable = 'position_management';
           $selectData = array('COUNT(1) AS TotalRecordsFound',false);
           $where = array(
               'trashed !=' => 1,
               'ml_pay_grade_id' => $id
           );
           $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

           if(isset($countResult) && $countResult->TotalRecordsFound > 0){
               $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('ml_payrol_site/view_payroll_ml/');
               return;
           }


           //Salary...
           $selectTable = 'salary';
           $selectData = array('COUNT(1) AS TotalRecordsFound',false);
           $where = array(
               'trashed !=' => 1,
               'ml_pay_grade' => $id
           );
           $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

           if(isset($countResult) && $countResult->TotalRecordsFound > 0){
               $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
               $this->session->set_flashdata('msg',$msg);
               redirect('ml_payrol_site/view_payroll_ml/');
               return;
           }


		  $this->db->trans_start();
           $table_id = $tableName;
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_pay_grade', array('ml_pay_grade_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_pay_grade', array('ml_pay_grade_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_pay_grade',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		  {
              $msg = "Record Have Been Successfully Trashed From The System::success";
              $this->session->set_flashdata('msg',$msg);
	          redirect('ml_payrol_site/view_payroll_ml/');
		  }
	
}
	
		    // Function for Currency viewing //
			
  	         public function view_currency()
	        {
				$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_currency',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/currency', $data);
			$this->load->view('includes/footer');
			}

			// Function for Currency active //

  	         public function active_currency()
	        {
				//$id=$this->input->post('id');
				$id=$this->input->post('active');

                $active=$this->input->post('active');
                $where=array('ml_currency_id'=>$id);

                $data=array('active'=>1);

                $query=$this->site_model->update_record('ml_currency',array('enabled'=>1),array('active'=>0));
                if($query){
                $query=$this->site_model->update_record('ml_currency',$where,$data);

                if($query)
                {
                    echo "OK:: Currency Active Successfully !::success";
                    return;
                }else{
                    echo "FAIL:: Error in Activation !::error";
                    return;
                }
            }
			}
	
	// Function for Currency Addition // 
	
	public function add_currency($id = NULL)
	    {
		if($this->input->post('add'))
        {

            $currency = $this->input->post('currency_name');
            if(!isset($currency) || empty($currency)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_currency');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_currency';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(currency_name)' => strtolower($currency),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_currency');
                return;
            }




            $add_type = array (
		    'currency_name' => $currency
            );
	     $query = $this->site_model->create_new_record('ml_currency', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_currency');
		}
	}
 }
	  
     	// Function for Edit ML Currency //
	
	public function edit_currency($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_currency');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['currency'] = $this->uri->segment(3);
		$where = array('ml_currency_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_currency', $where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'currency_name' => $this->input->post('currency_name'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_currency', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_currency/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_currency', $data);
		$this->load->view('includes/footer');	
}
	
        
       	 // Function for Trash Currency //

     public function trashed_currency($id = NULL,$tableName = NULL)
     {

         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_currency');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_currency');
             return;
         }


         //Salary.....
         $selectTable = 'salary';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !='  => 1,
             'ml_currency' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_currency');
             return;
         }



         $this->db->trans_start();
          $table_id = $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_currency', array('ml_currency_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_currency', array('ml_currency_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_currency',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{
                $msg = "Record Have Been Successfully Trashed From The System::success";
                $this->session->set_flashdata('msg',$msg);
			    redirect('ml_payrol_site/view_currency/');
		    }
}
           
              // Function for Deduction Head view //
			
  	         public function view_deduction_head()
	        {
				$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_deduction_type',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/deductions_heads', $data);
			$this->load->view('includes/footer');
			}

        
	// Function for Deduction Head Addition // 
	
	public function add_deduction($id = NULL)
	    {
		if($this->input->post('add')){

            $deduction = $this->input->post('deduction_type');
            if(!isset($deduction) || empty($deduction))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_deduction_head');
                return;
            }


            //Little Fix For Add.......
            $table = 'ml_deduction_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(deduction_type)' => strtolower($deduction),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_deduction_head');
                return;
            }



            $add_type = array (
		    'deduction_type' => $deduction
            );
	     $query = $this->site_model->create_new_record('ml_deduction_type', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_deduction_head');
		}
	}
 }

         // Function for Edit Head Deduction //
	
	public function edit_deduction($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_deduction_head');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_deduction_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_deduction_type', $where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'deduction_type' => $this->input->post('deduction_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_deduction_type', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_deduction_head/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_deduction', $data);
		$this->load->view('includes/footer');	
}
	
         // Function for Trash Currency //

     public function trashed_Deduction($id = NULL, $tableName = NULL)
     {

         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_deduction_head');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_deduction_head');
             return;
         }


         //Deduction.....
         $selectTable = 'deduction';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !='  => 1,
             'ml_deduction_type_id' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_deduction_head');
             return;
         }




         $this->db->trans_start();
         $table_id = $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_deduction_type', array('ml_deduction_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_deduction_type', array('ml_deduction_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	  => $id,
		  'table_name' 	  => 'ml_deduction_type',
          'table_id_name' => $table_id,
		  'trash_date' 	  => date('Y-m-d'),
		  'trashed_by'    => $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_deduction_head/');
		}
	
}


          // Function for Allowance Type view //
			
  	      public function view_allowance_type()
	        {
					$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_allowance_type',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/allowance_types', $data);
			$this->load->view('includes/footer');
			}

       // Function for Deduction Head Addition // 
	
	public function add_allowance()
	    {
		if($this->input->post('add'))
        {
            $allowance = $this->input->post('allowance_type');
            if(!isset($allowance) || empty($allowance))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_allowance_type');
                return;
            }


            //Little Fix For Add..........
            $table = 'ml_allowance_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(allowance_type)' => strtolower($allowance),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_allowance_type');
                return;
            }

            $add_type = array (
		'allowance_type' =>  $allowance
            );
	     $query = $this->site_model->create_new_record('ml_allowance_type', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_allowance_type');
		}
	}
 }

            // Function for Edit Allowance Type //
	
	public function edit_allowance($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_allowance_type');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_allowance_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_allowance_type', $where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'allowance_type' => $this->input->post('allowance_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_allowance_type', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_allowance_type/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_allowance', $data);
		$this->load->view('includes/footer');	
}

      // Function for Trash Allowance Type //

     public function trashed_allownace($id = NULL, $tableName = NULL)
     {

         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_allowance_type');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_allowance_type');
             return;
         }


         //Allowance.....
         $selectTable = 'allowance';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !='  => 1,
             'ml_allowance_type_id' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_allowance_type');
             return;
         }




		  $this->db->trans_start();
          $table_id = $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_allowance_type', array('ml_allowance_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_allowance_type', array('ml_allowance_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_allowance_type',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{
                $msg = "Record Have Been Successfully Trashed From The System::success";
                $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_allowance_type/');
	}
}

          // Function for Pay Frequecy view //
			
  	      public function view_pay_frequecy()
	        {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_pay_frequency', $where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/pay_frequency', $data);
			$this->load->view('includes/footer');
			}
        
		
		
		
       // Function for Pay Frequency Addition // 
	
	public function add_frequency($id = NULL)
	    {
		if($this->input->post('add'))
        {
            $payfrequency = $this->input->post('pay_frequency');
            if(!isset($payfrequency) || empty($payfrequency)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_frequecy');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_pay_frequency';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(pay_frequency)' => strtolower($payfrequency),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_frequecy');
                return;
            }

			$add_type = array (
		    'pay_frequency' =>  $payfrequency
             );
	     $query = $this->site_model->create_new_record('ml_pay_frequency', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_pay_frequecy');
		}
	}
 }
 
        
       // Function for Edit Pay Frequency //
		
      public function edit_Frequency($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_frequecy');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_pay_frequency_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_pay_frequency',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'pay_frequency' => $this->input->post('pay_frequency'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_pay_frequency', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_pay_frequecy/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_Frequency', $data);
		$this->load->view('includes/footer');	
}
	     
	
	
	     // Function for Trash Pay Frequency //

     public function trashed_frequency($id = NULL, $tableName = NULL)
           {

               if($tableName === NULL || !is_string($tableName)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_frequecy');
                   return;
               }

               if($id === NULL || !is_numeric($id)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_frequecy');
                   return;
               }


               //Allowance.....
               $selectTable = 'allowance';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !='  => 1,
                   'pay_frequency' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_frequecy');
                   return;
               }


               //Salary.....
               $selectTable = 'salary';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !='  => 1,
                   'ml_pay_frequency' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_frequecy');
                   return;
               }


               //Deduction.....
               $selectTable = 'deduction';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !='  => 1,
                   'deduction_frequency' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_frequecy');
                   return;
               }


		  $this->db->trans_start();
          $table_id = $tableName;

		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_pay_frequency', array('ml_pay_frequency_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_pay_frequency', array('ml_pay_frequency_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_pay_frequency',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_pay_frequecy/');
		}
	
}
	   
	      
		   // Function for Increment Type view //
			
  	      public function view_increment_type()
	        {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_increment_type',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/increments-types', $data);
			$this->load->view('includes/footer');
			}
        
	          
			     
				   
	// Function for Increment Type Addition // 
	
	public function add_increment($id = NULL)
	    {
		if($this->input->post('add')){

            $increment = $this->input->post('increment_type');
            if(!isset($increment) || empty($increment))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_increment_type');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_increment_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(increment_type)' => strtolower($increment),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_increment_type');
                return;
            }



            $add_type = array (
		    'increment_type' =>  $increment
            );
	     $query = $this->site_model->create_new_record('ml_increment_type', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_increment_type');
		}
	}
 }
 
         // Function for Edit Increment Types //
		
      public function edit_increment($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_increment_type');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('increment_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_increment_type',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'increment_type' => $this->input->post('increment_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_increment_type', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_increment_type/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_increment', $data);
		$this->load->view('includes/footer');	
}
	    
	             
		    
	  // Function for Trash Increment Types //

     public function trashed_increment($id = NULL,$tableName = NULL)
           {


               if($tableName === NULL || !is_string($tableName)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_increment_type/');
                   return;
               }

               if($id === NULL || !is_numeric($id)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_increment_type/');
                   return;
               }


               //Increment.
               $selectTable = 'increments';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !=' => 1,
                   'increment_type_id' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_increment_type/');
                   return;
               }


               $this->db->trans_start();
		  $table_id = $tableName;
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_increment_type', array('increment_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_increment_type', array('increment_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_increment_type',
          'table_id_name'=>$table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{
                $msg = "Record Have Been Successfully Trashed From The System::success";
                $this->session->set_flashdata('msg',$msg);
			    redirect('ml_payrol_site/view_increment_type/');
		}
} 
		
		 
            // Function for Expenses view //
			
  	      public function view_expense()
	        {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_expense_type',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/expenses', $data);
			$this->load->view('includes/footer');
			} 
		   
	    
	// Function for Expense Addition // 
	
	public function add_expense($id = NULL)
	    {
		if($this->input->post('add'))
        {
            $expense = $this->input->post('expense_type_title');
            if(!isset($expense) || empty($expense))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_expense');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_expense_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(expense_type_title)' => strtolower($expense),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_expense');
                return;
            }

			$add_type = array (
		    'expense_type_title' =>  $expense
            );
	     $query = $this->site_model->create_new_record('ml_expense_type', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_expense');
		}
	}
 }
		  
	// Function for Edit Expense //
		
      public function edit_expense($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_expense');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_expense_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_expense_type',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'expense_type_title' => $this->input->post('expense_type_title'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_expense_type', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_expense/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_expense', $data);
		$this->load->view('includes/footer');	
}
	
	 
	  // Function for Trash Expense //

     public function trashed_expense($id = NULL,$tableName = NULL)
     {


         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_expense/');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_expense/');
             return;
         }


         //Expense Claims.
         $selectTable = 'expense_claims';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             //'trashed !=' => 1,
             'expense_type' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_expense/');
             return;
         }




         $this->db->trans_start();
		  $table_id = $tableName;
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_expense_type', array('ml_expense_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_expense_type', array('ml_expense_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_expense_type',
          'table_id_name'=> $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
          {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_expense/');
		}
	
} 
	 
	 	    // Function for Transaction Type view //
			
  	      public function view_transaction()
	        {
			$trashed = 0;
				$where = array(
				'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_transaction_types',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/transactions_types',$data);
			$this->load->view('includes/footer');
			}     
		 



     	// Function for Expense Addition // 
	
	public function add_transaction($id = NULL)
	    {
		if($this->input->post('add'))
        {

            $transaction = $this->input->post('transaction_type');
            if(!isset($transaction) || empty($transaction))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_transaction');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_transaction_types';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(transaction_type)' => strtolower($transaction),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_transaction');
                return;
            }



            $add_type = array (
		'transaction_type' =>  $transaction
            );
	     $query = $this->site_model->create_new_record('ml_transaction_types', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_transaction');
		}
	}
 }
 
 	// Function for Edit Expense //
		
      public function edit_transaction($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_transaction');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_transaction_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_transaction_types',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'transaction_type' => $this->input->post('transaction_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_transaction_types', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_transaction/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_transaction', $data);
		$this->load->view('includes/footer');	
}
	
    	  // Function for Trash Transaction //

     public function trashed_transaction($id = NULL,$tableName = NULL)
     {
         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_transaction/');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_transaction/');
             return;
         }


         //Transaction.
         $selectTable = 'transaction';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !=' => 1,
             'transaction_type' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_transaction/');
             return;
         }





         $this->db->trans_start();
		  $table_id = $tableName;
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_transaction_types', array('ml_transaction_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_transaction_types', array('ml_transaction_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_transaction_types',
           'table_id_name' =>$table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_transaction/');
		}
	
}

            // Function for Payment mode Type view //
			
  	      public function view_payment_mode_type()
	        {
			$trashed = 0;
		    $where = array(
		    'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_payment_mode_types',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/payment_mode_type',$data);
			$this->load->view('includes/footer');
			} 
      
        
     	// Function for Payment Mode Type Addition // 
	
	public function add_payment($id = NULL)
	    {
		if($this->input->post('add'))
        {
            $paymentmode = $this->input->post('payment_mode_type');
            if(!isset($paymentmode) || empty($paymentmode)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payment_mode_type');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_payment_mode_types';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(payment_mode_type)' => strtolower($paymentmode),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payment_mode_type');
                return;
            }




		$add_type = array (
		'payment_mode_type' =>  $paymentmode
         );
	     $query = $this->site_model->create_new_record('ml_payment_mode_types', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_payment_mode_type');
		}
	}
 }
    	// Function for Edit Payment Mode Type //
		
      public function edit_payment($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payment_mode_type');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('payment_mode_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_payment_mode_types',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'payment_mode_type' => $this->input->post('payment_mode_type'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_payment_mode_types', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_payment_mode_type/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_payment',$data);
		$this->load->view('includes/footer');	
}
 
         // Function for Trash Payment Mode Type //

     public function trashed_payment_mode_type($id = NULL, $tableName = NULL)
     {

         if($tableName === NULL || !is_string($tableName)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_payment_mode_type');
             return;
         }

         if($id === NULL || !is_numeric($id)){
             $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_payment_mode_type');
             return;
         }


         //Payment Mode....
         $selectTable = 'payment_mode';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !=' => 1,
             'payment_mode_type_id' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_payment_mode_type');
             return;
         }


         //Transaction....
         $selectTable = 'transaction';
         $selectData = array('COUNT(1) AS TotalRecordsFound',false);
         $where = array(
             'trashed !=' => 1,
             'payment_mode' => $id
         );
         $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

         if(isset($countResult) && $countResult->TotalRecordsFound > 0){
             $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
             $this->session->set_flashdata('msg',$msg);
             redirect('ml_payrol_site/view_payment_mode_type');
             return;
         }




         $this->db->trans_start();
		  $table_id = $tableName;
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_payment_mode_types', array('payment_mode_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_payment_mode_types', array('payment_mode_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	  => $id,
		  'table_name' 	  => 'ml_payment_mode_types',
          'table_id_name' =>$table_id,
		  'trash_date' 	  => date('Y-m-d'),
		  'trashed_by' 	  => $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_payment_mode_type/');
		}
	
}
         // Function For payment Type //
         public function view_payment_type()
	        {
			$trashed = 0;
		    $where = array(
		    'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_payment_type',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/payment_type',$data);
			$this->load->view('includes/footer');
			}
			// Function for Payment Type Addition // 
	
	public function add_payment_type($id = NULL)
	    {
		if($this->input->post('add'))
        {
            $paymenttype = $this->input->post('payment_type_title');
            if(!isset($paymenttype) || empty($paymenttype))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payment_type');
                return;
            }


            //Little Fix For Add......
            $table = 'ml_payment_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(payment_type_title)' => strtolower($paymenttype),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_payment_type');
                return;
            }

			$add_type = array (
		'payment_type_title' =>  $paymenttype
            );
	     $query = $this->site_model->create_new_record('ml_payment_type', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_payment_type');
		}
	}
 } 
     // Function for Edit Payment type //
 
      public function edit_payment_type($id = NULL)
	    {
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('payment_type_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_payment_type',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'payment_type_title' => $this->input->post('payment_type_title'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_payment_type', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_payment_type/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_payment_type',$data);
		$this->load->view('includes/footer');	
}

         // Function for Trash Payment Type //

     public function trashed_payment_type($id = NULL,$tableName = NULL)
           {

               if($tableName === NULL || !is_string($tableName)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_payment_type/');
                   return;
               }

               if($id === NULL || !is_numeric($id)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_payment_type/');
                   return;
               }


               //Loan Advance.
               $selectTable = 'loan_advances';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !=' => 1,
                   'payment_type' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_payment_type/');
                   return;
               }



           $this->db->trans_start();
		  $table_id = $tableName;
 		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_payment_type', array('payment_type_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_payment_type', array('payment_type_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_payment_type',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
		{
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_payment_type/');
		}
	
}


         // Function for Pay Rate Viewing //
			
  	      public function view_pay_rate()
	        {
			$trashed = 0;
		    $where = array(
		    'trashed' => $trashed
				);
			$data{'data'} = $this->site_model->get_all_by_where('ml_payrate',$where);
			$this->load->view('includes/header');
			$this->load->view('payroll_ml/payrate',$data);
			$this->load->view('includes/footer');
			} 
    
	     
		// Function for Pay Rate Addition // 
	
	public function add_payrate($id = NULL)
	    {
		if($this->input->post('add'))
        {
            $payrate = $this->input->post('payrate');
            if(!isset($payrate) || empty($payrate)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_rate');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_payrate';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(payrate)' => strtolower($payrate),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_rate');
                return;
            }




            $add_type = array (
		'payrate' =>  $payrate
            );
	     $query = $this->site_model->create_new_record('ml_payrate', $add_type);
		if ($query) 
		{
		redirect('ml_payrol_site/view_pay_rate');
		}
	}
 }
 
     
	    // Function for Edit Payrate //
		
      public function edit_payrate($id = NULL)
	    {
            if(!isset($id) || empty($id) || !is_numeric($id)){
                $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('ml_payrol_site/view_pay_rate');
                return;
            }
	 	if($this->uri->segment(3))
		{
		$data['data'] = $this->uri->segment(3);
		$where = array('ml_payrate_id' => $id);
		$data['user'] = $this->site_model->get_row_by_id('ml_payrate',$where);
		//echo "<pre>";print_r($data['user']); die;
		if($this->input->post('edit'))
		{
		$insert 		= array(
		'payrate' => $this->input->post('payrate'),
		'enabled' => $this->input->post('enabled')
		); 
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_payrate', $where, $insert);
		if($query)
		{
		redirect('ml_payrol_site/view_pay_rate/');
		}
	}
}
		
		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_payrate',$data);
		$this->load->view('includes/footer');	
}
 
            // Function for Trash Pay Rate //

     public function trashed_payrate($id = NULL,$tableName = NULL)
     {

               if($tableName === NULL || !is_string($tableName)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_rate/');
                   return;
               }

               if($id === NULL || !is_numeric($id)){
                   $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_rate/');
                   return;
               }


               //Salary.
               $selectTable = 'salary';
               $selectData = array('COUNT(1) AS TotalRecordsFound',false);
               $where = array(
                   'trashed !=' => 1,
                   'ml_pay_rate_id' => $id
               );
               $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

               if(isset($countResult) && $countResult->TotalRecordsFound > 0){
                   $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                   $this->session->set_flashdata('msg',$msg);
                   redirect('ml_payrol_site/view_pay_rate/');
                   return;
               }




               $this->db->trans_start();
               $table_id = $tableName;
		  
		  $data['emer_contact'] = $this->site_model->get_row_by_id('ml_payrate', array('ml_payrate_id' => $id));
		  if($data['emer_contact']->trashed == 0)
		  {
			  $update_trash = array('trashed' => 1);
			  $query = $this->site_model->update_record('ml_payrate', array('ml_payrate_id' => $id), $update_trash);
		  }
		  
		  $insert_trash = array(
		  'record_id' 	=> $id,
		  'table_name' 	=> 'ml_payrate',
          'table_id_name' => $table_id,
		  'trash_date' 	=> date('Y-m-d'),
		  'trashed_by' 	=> $this->session->userdata('employee_id')
		  );
		  $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);
		  
		  $this->db->trans_complete();
		  
		  if($query || $query_trash)
			{
                $msg = "Record Have Been Successfully Trashed From The System::success";
                $this->session->set_flashdata('msg',$msg);
			    redirect('ml_payrol_site/view_pay_rate/');
		}
	
    }
	public function add_tax_slabs(){

		//Get Financial Years for Filters
		$financialYearsTable = 'ml_financial_year';
		$data['financialYears'] = $this->hr_model->dropdown_wd_option($financialYearsTable, '-- Select FinancialYear --','FinancialYearID', 'FinancialYear');


		$this->load->view('includes/header');
		$this->load->view('payroll_ml/add_tax_slabs_master_list',$data);
		$this->load->view('includes/footer');
	}
	public function add_taxSlab_info()
	{
		$financialYearID = $this->input->post('FinancialYear');

		$tableTaxSlab = "ml_tax_slab";
		$tableTaxYear = "ml_tax_year";

		//First Lets Check If Record Already exist for the posted financial year in the Tax Year Table.
		$selectDataSlabNo = array("MAX(SlabNo) AS SlabNo",false);
		$whereTaxYear = array(
			'FinancialYearID' => $financialYearID
		);

        $joinsTaxYear = array(
            array(
                'table' => 'ml_tax_year TY',
                'condition' => 'TY.TaxYearID = ml_tax_slab.TaxYearID',
                'type' => 'INNER'
            )
        );

		$ResultSlabNo = $this->common_model->select_fields_where_like_join($tableTaxSlab,$selectDataSlabNo,$joinsTaxYear,$whereTaxYear,TRUE);


		if(isset($ResultSlabNo) || !empty($ResultSlabNo->SlabNo)){
			//Create New Record Here.
				$slabNo = $ResultSlabNo->SlabNo+1;
		}else{
			$slabNo =$ResultSlabNo->SlabNo;
		}
		//First Lets Check If Record Already exist for the posted financial year in the Tax Year Table.
		$selectDataTaxYear = array("TaxYearID",false);
		$whereTaxYear = array(
			'FinancialYearID' => $financialYearID
		);

		$ResultTaxYear = $this->common_model->select_fields_where($tableTaxYear,$selectDataTaxYear,$whereTaxYear,TRUE);

		$taxSlabsPostedDataArray = array();


		//Getting Posted Arrays
		$AnnualIncomeFrom = $this->input->post('AnnualIncomeFrom');
		$AnnualIncomeTo = $this->input->post('AnnualIncomeTo');
		$FixedAmount = $this->input->post('FixedAmount');
		$TaxRate = $this->input->post('TaxRate');
		$Remarks = $this->input->post('Remarks');

//If Record Not Found in the Tax Year for Slected Financial Year. We Need TO Create One Record for it now.
		if(!isset($ResultTaxYear) || empty($ResultTaxYear->TaxYearID)){
			//Create New Record Here.
            $data=array(
				'FinancialYearID'=>$financialYearID,
				'FromMonthID'=>1,
				'ToMonthID'=>12,
				'DateCreated'=> date('Y-m-d'),
				'CreatedBy'=>1

			);
			$TaxYearID = $this->common_model->insert_record($tableTaxYear,$data);
		}else{
			$TaxYearID = $ResultTaxYear->TaxYearID;
		}

		foreach($AnnualIncomeFrom as $key => $val){
			if(!empty($AnnualIncomeFrom) && !empty($AnnualIncomeTo)){
				$arrayToPush = array(
					'TaxYearID' => $TaxYearID,
					'SlabNo' => $slabNo, //Slab No To Insert
					'AnnualIncomeFrom' => $val,
					'AnnualIncomeTo' => $AnnualIncomeTo[$key],
					'FixedAmount' => $FixedAmount[$key],
					'TaxRate' => $TaxRate[$key],
					'Remarks' => $Remarks[$key]
				);
				array_push($taxSlabsPostedDataArray,$arrayToPush);
			}

			$slabNo++;

		}
		$query = $this->common_model->insert_multiple($tableTaxSlab,$taxSlabsPostedDataArray);
		if ($query)
		{
			$msg = "Record Have Been Successfully Add::success";
			$this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_tax_slabs');
		}
//		echo "<pre>";
//		var_dump($taxSlabsPostedDataArray);
	}
	public function edit_tax_slabs($taxSlabID)
	{
		//Get Financial Years for Filters
		$financialYearsTable = 'ml_financial_year';
		$data['financialYears'] = $this->hr_model->dropdown_wd_option($financialYearsTable, '-- Select FinancialYear --','FinancialYearID', 'FinancialYear');

		if($taxSlabID)
		{

			//selected data
			$data['data'] = $taxSlabID;
			$where = array('TaxSlabID' =>$taxSlabID);
			$data['taxSlab'] = $this->site_model->get_row_by_id('ml_tax_slab', $where);

		}

		$this->load->view('includes/header');
		$this->load->view('payroll_ml/edit_tax_slabs_master_list',$data);
		$this->load->view('includes/footer');
	}
	public function update_taxSlab_info(){
				$ID= $this->input->post('hiddenID');
		        $financialYearID = $this->input->post('FinancialYear');
				$AnnualIncomeFrom = $this->input->post('AnnualIncomeFrom');
				$AnnualIncomeTo = $this->input->post('AnnualIncomeTo');
				$FixedAmount = $this->input->post('FixedAmount');
				$TaxRate = $this->input->post('TaxRate');
				$Remarks = $this->input->post('Remarks');
				$where= array('TaxSlabID' =>$ID);
				$data = array(
					'TaxYearID' => $financialYearID,
//					'SlabNo' => $slabNo, //Slab No To Insert
					'AnnualIncomeFrom' => $AnnualIncomeFrom,
					'AnnualIncomeTo' => $AnnualIncomeTo,
					'FixedAmount' => $FixedAmount,
					'TaxRate' => $TaxRate,
					'Remarks' => $Remarks
				);
				//echo "<pre>";print_r($insert); die;
				$query = $this->site_model->update_record('ml_tax_slab', $where, $data);
		       if ($query['code'] === 0) {
			       $msg = "Record is Same in Database, No Change Occurred::warning";
			       $this->session->set_flashdata('msg',$msg);
			       redirect('ml_payrol_site/view_tax_slabs/');
		         }


		        else{
					if ($query === true) {
						$msg = "Record Have Been Successfully Updated::success";
						$this->session->set_flashdata('msg',$msg);
						redirect('ml_payrol_site/view_tax_slabs/');
					}
				}
	}
	public function trash_tax_slabs($taxSlabID)
	{
		$where= array('TaxSlabID' =>$taxSlabID);
		$data = array(
			'Trashed' => 1
		);
		//echo "<pre>";print_r($insert); die;
		$query = $this->site_model->update_record('ml_tax_slab', $where, $data);

		if ($query) {
			$msg = "Record Have Been Successfully Trashed::success";
			$this->session->set_flashdata('msg',$msg);
			redirect('ml_payrol_site/view_tax_slabs/');
		}

	}
	public function view_tax_slabs(){

        //Get Financial Years for Filters
        $financialYearsTable = 'ml_financial_year';
        $data['financialYears'] = $this->hr_model->dropdown_wd_option($financialYearsTable, '-- Select FinancialYear --','FinancialYearID', 'FinancialYear');

        $this->load->view('includes/header');
        $this->load->view('payroll_ml/tax_slabs_master_list',$data);
        $this->load->view('includes/footer');
	}
    public function list_tax_slabs(){
        $table = "ml_tax_year TY";
        $selectData = array('TaxSlabID AS SlabID,AnnualIncomeFrom, AnnualIncomeTo, TaxRate, FixedAmount, Remarks',false);
        $joins = array(
            array(
                'table' => 'ml_tax_slab TS',
                'condition' => 'TS.TaxYearID = TY.TaxYearID',
                'type' => 'INNER'
            )
        );

        $financialYearID = $this->input->post('FinancialYear');
        $where = "TS.Trashed = 0";
        if(isset($financialYearID) && !empty($financialYearID) && is_numeric($financialYearID)){
            $where = array(
                'TY.FinancialYearID' => $financialYearID
            );
        }

        $addColumn = array(
            'ActionButtons' => array(
                '<a href="'.base_url().'ml_payrol_site/edit_tax_slabs/$1"> <span title="Edit Record" class="fa fa-pencil" id="EditTaxSlab"></span></a> &nbsp; &nbsp;|&nbsp; &nbsp;
                  <a href="'.base_url().'ml_payrol_site/trash_tax_slabs/$1"><span title="Trash Record" class="fa fa-trash"></span></a>',
                'SlabID'
            )
        );

        $taxSlabs = $this->common_model->select_fields_joined_DT($selectData,$table,$joins,$where,'','','',$addColumn);

        print $taxSlabs;

    }
}

?>