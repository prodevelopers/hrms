<?php
class Site extends CI_Controller{

		function __construct()
		{
		parent::__construct();
		$this->load->model('site_model');
		$this->load->library('datatables');
	
		}
	
	
	
	

	
//********************************* ***** *** ********** ************************************//
//																							 //
//================================= leave and attendance ====================================//
//																							 //
///******************************** ***** *** ********** ************************************//
		public function test()
		{
			$this->load->view('test');
		}

//////////function for time sheet ///////////

public function view_timesheet($param = NULL)
{
			$this->load->library('datatables');
			if($param == 'list')
			{
			echo $this->site_model->view_time();
			die;
			}
			$this->load->view('includes/header');
			$this->load->view('leave_attend/timesheet');
			$this->load->view('includes/footer');
}


///// function for view attendance /////////
public function attendance($param = NULL)
	{
		$this->load->library('datatables');
			if($param == 'list')
			{
			echo $this->site_model->view_attend();
			die;
			}
			$data['uname']				 = $this->input->post('leave_type');
			$data['leave_type']			 = $this->site_model->drop_down3();
			
			$this->load->view('includes/header');
			$this->load->view('leave_attend/attendance', $data);
			$this->load->view('includes/footer');
			}


//// function for staff on leave ///////////
	
	public function leave_status($param = NULL)
	{
		$this->load->library('datatables');
			if($param == 'list')
			{
			echo $this->site_model->leave_staff();
			die;
			}
			$data['uname'] 					= $this->input->post('leave_type');
			$data['leave_type']			 	= $this->site_model->drop_down3();

			$this->load->view('includes/header');
			$this->load->view('leave_attend/leave_status', $data);
			$this->load->view('includes/footer');
			}


///// function for  add apply for leave///////

	public function add_leave_action()
	{
		if($this->input->post('add'))
		{
			$emp_id = $this->session->userdata('employee_id');
			//echo $emp_id;die;
			$add_type			= array(
			'employee_id'		=> $emp_id,
			'entitlement_id' 	=> $this->input->post('leave_type'),
			'from' 				=> $this->input->post('from'),
			'to' 				=> $this->input->post('to'),
			'note' 				=> $this->input->post('note'));
			//echo "<pre>"; print_r($add_type); die;
			$query = $this->site_model->create_new_record('leave_application', $add_type);
			if($query)
			{
				redirect('site/view_leave_attandance');
			}
			
		}
		
		}
		
///// action function for leave /////////		
	
	public function leave_action($emp_id = NULL)
	{
		if($this->uri->segment(3))
		{
			$data['employee_id']			 = $this->uri->segment(3);
			$where							 = 'emp.employee_id ='. $emp_id;
			$data['rec']					 = $this->site_model->fetch_information($where);
			//echo "<pre>";print_r($data['rec']); die;
			$this->load->view('includes/header');
			$this->load->view('leave_attend/leave_action', $data);
			$this->load->view('includes/footer');
		}
	}
	
////// function for view apply for leave form data ///////
	
	public function apply_for_leave($employee_id = NULL)
	{
			$employee_id = $this->session->userdata('employee_id');
			$where = 'emp.employee_id='.$employee_id;
				
			$data['employee'] 				= $this->site_model->fetch_information($where);		
			$data['leave_type'] 			= $this->site_model->drop_down3();
			$this->load->view('includes/header');
			$this->load->view('leave_attend/apply_for_leave', $data);
			$this->load->view('includes/footer');
	}
	
//// function for view leave and attendance detail//////
	
	public function view_leave_attandance($param = NULL)
	{
		$this->load->library('datatables');
			if($param == 'list')
			{
			echo $this->site_model->leave_attendance();
			die;
			}
			$data['uname']			 = $this->input->post('leave_type');
			$data['leave_type']		 = $this->site_model->drop_down3();
			$this->load->view('includes/header');
			$this->load->view('leave_attend/leave_attendance', $data);
			$this->load->view('includes/footer');
			}
	}
?>