<?php
/**
 * Class dashboard_site
 * @property common_model $common_model It resides all the methods which can be used in most of the controllers.
 */
class Hr_site extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('site_model');
        $this->load->model('common_model');
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function index()
    {
        $this->load->library('form_validation');
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function module()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_module_list', $where);
        //echo"<pre>"; print_r($data['rec']); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/module', $data);
        $this->load->view('includes/footer');
    }

    public function add_module()
    {
        if ($this->input->post('add'))
        {

            $module = $this->input->post('module_name');
            if(!isset($module) || empty($module))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/module');
                return;
          }


            //Little Fix For Add........
            $table = 'ml_module_list';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(module_name)' => strtolower($module),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/module');
                return;
            }
                   $add_type = array(
                    'module_name' => $module
                    );
                    //echo "<pre>"; print_r($add_type); die;
                    $query = $this->site_model->create_new_record('ml_module_list', $add_type);

            if ($query)
            {
                redirect('hr_site/module');
            }
        }
    }




    public function edit_module($ml_module_id = NULL)
    {
        if(!isset($ml_module_id) || empty($ml_module_id) || !is_numeric($ml_module_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/module');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['ml_module_id'] = $this->uri->segment(3);
            $where = array('ml_module_id' => $ml_module_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_module_list', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'module_name' => $this->input->post('module_name'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_module_list', $where, $insert);
                if ($query) {
                    redirect('hr_site/module/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_module', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_module($id = NULL,$tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/module');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/module');
            return;
        }

        //.
        $selectTable = 'user_groups_privileges';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_module_list_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/module');
            return;
        }



        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_module_list', array('ml_module_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_module_list', array('ml_module_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_module_list',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/module');
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function benefit_list()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_benefits_list', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/benefit_list', $data);
        $this->load->view('includes/footer');
    }

    public function add_benefit_list()
    {
        if ($this->input->post('add')) {

            $rec = $this->site_model->get_all('ml_benefits_list');
            foreach($rec as $rec) {

                if (strcasecmp($rec->benefit_name,$this->input->post('benefit_name')) == 0)

                {
                    $msg ="OOps Already Inserted !";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('hr_site/benefit_list');
                    return;}}


            $add_type = array(
                'benefit_name' => $this->input->post('benefit_name'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_benefits_list', $add_type);
            if ($query) {
                redirect('hr_site/benefit_list');
            }
        }
    }


    public function edit_benefit_list($benefit_item_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['benefit_item_id'] = $this->uri->segment(3);
            $where = array('benefit_item_id' => $benefit_item_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_benefits_list', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'benefit_name' => $this->input->post('benefit_name'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_benefits_list', $where, $insert);
                if ($query) {
                    redirect('hr_site/benefit_list/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_benefit_list', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_benefit_list($id = NULL)
    {

        $this->db->trans_start();
        $table_id = $this->uri->segment(4);

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_benefits_list', array('benefit_item_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_benefits_list', array('benefit_item_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_benefits_list',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {

            redirect('hr_site/benefit_list/');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function benefit_type()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_benefit_type', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/benefit_type', $data);
        $this->load->view('includes/footer');
    }

    public function add_benefit_type()
    {
        if ($this->input->post('add')) {

            $benefit = $this->input->post('benefit_type_title');
            if(!isset($benefit) || empty($benefit)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/benefit_type');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_benefit_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(benefit_type_title)' => strtolower($benefit),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/benefit_type');
                return;
            }


            $add_type = array(
                'benefit_type_title' => $benefit
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_benefit_type', $add_type);
            if ($query) {
                redirect('hr_site/benefit_type');
            }
        }
    }


    public function edit_benefit_type($benefit_type_id = NULL)
    {
        if(!isset($benefit_type_id) || empty($benefit_type_id) || !is_numeric($benefit_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/benefit_type');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['benefit_type_id'] = $this->uri->segment(3);
            $where = array('benefit_type_id' => $benefit_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_benefit_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'benefit_type_title' => $this->input->post('benefit_type_title'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_benefit_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/benefit_type/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_benefit_type', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_benefit_type($id = NULL, $tableName= NULL)
    {


        if($tableName === NULL || !is_string($tableName))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/benefit_type');
            return;
        }

        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/benefit_type');
            return;
        }



        //Benefits
        $selectTable = 'benefits';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'benefit_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0)
        {
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/benefit_type');
            return;
        }



        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_benefit_type', array('benefit_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_benefit_type', array('benefit_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_benefit_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/benefit_type');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function kpi()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data['data'] = $this->site_model->get_all_by_where('ml_kpi', $where);
        $data['projects'] = $this->site_model->get_all_by_where('ml_projects', $where);
        $data['info']=$this->site_model->get_kpi_project();
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/kpi', $data);
        $this->load->view('includes/footer');
    }

    public function add_kpi()
    {
        if ($this->input->post('add')) {

            $project = $this->input->post('project');
            $kpi = $this->input->post('kpi');
            if(!isset($project) || empty($project))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/kpi');
                return;
            }
            if(!isset($kpi) || empty($kpi))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/kpi');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_kpi';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'project_id' => $project,
                'LOWER(kpi)' => strtolower($kpi),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/kpi');
                return;
            }



            $add_type = array(
                'project_id' => $project,
                'kpi' => $kpi
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_kpi', $add_type);
            if ($query) {
                redirect('hr_site/kpi');
            }
        }
    }


    public function edit_kpi($ml_kpi_id = NULL)
    {
        if(!isset($ml_kpi_id) || empty($ml_kpi_id) || !is_numeric($ml_kpi_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/kpi');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['ml_kpi_id'] = $this->uri->segment(3);
            $where = array('ml_kpi_id' => $ml_kpi_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_kpi', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {

                $insert = array(
                    'project_id' => $this->input->post('project'),
                    'kpi' => $this->input->post('kpi'),
                    'enabled' => $this->input->post('enabled')
                );
               //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_kpi', $where, $insert);
                if ($query) {
                    redirect('hr_site/kpi/');
                }
            }
        }
        $data['project']=$this->site_model->html_selectbox('ml_projects','Select Project','project_id','project_title');
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_kpi', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_kpi($id = NULL, $tableName = Null)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/kpi');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/kpi');
            return;
        }


        //performance Evaluation
        $selectTable = 'performance_evaluation';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_kpi_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/kpi');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_kpi', array('ml_kpi_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_kpi', array('ml_kpi_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_kpi',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/kpi');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function posting_location()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_posting_location_list', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/posting_location', $data);
        $this->load->view('includes/footer');
    }

    public function add_posting_location()
    {
        if ($this->input->post('add'))
        {


            $postinglocation = $this->input->post('posting_location');
            if(!isset($postinglocation) || empty($postinglocation))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/posting_location');
                return;
            }


            //Little Fix For Add.....

            $table = 'ml_posting_location_list';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(posting_location)' => strtolower($postinglocation),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/posting_location');
                return;
            }

            $add_type = array(
                'posting_location' => $postinglocation
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_posting_location_list', $add_type);
            if ($query)
            {
                redirect('hr_site/posting_location');
            }
        }
    }


    public function edit_posting_location($posting_locations_list_id = NULL)
    {
        if(!isset($posting_locations_list_id) || empty($posting_locations_list_id) || !is_numeric($posting_locations_list_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_location');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['posting_locations_list_id'] = $this->uri->segment(3);
            $where = array('posting_locations_list_id' => $posting_locations_list_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_posting_location_list', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'posting_location' => $this->input->post('posting_location'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_posting_location_list', $where, $insert);
                if ($query) {
                    redirect('hr_site/posting_location/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_posting_location', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_posting_location($id = NULL , $tableName = NULL)
    {



        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_location');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_location');
            return;
        }



        //Posting
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'location' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_location');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_posting_location_list', array('posting_locations_list_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_posting_location_list', array('posting_locations_list_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_posting_location_list',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_location');
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function posting_reason()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_posting_reason_list', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/posting_reason', $data);
        $this->load->view('includes/footer');
    }

    public function add_posting_reason()
    {
        if ($this->input->post('add'))
        {

            $postingreason = $this->input->post('posting_reason');
            if(!isset($postingreason) || empty($postingreason))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/posting_reason');
                return;
            }


            //Little Fix For Add.......
            $table = 'ml_posting_reason_list';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(posting_reason)' => strtolower($postingreason),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/posting_reason');
                return;
            }


            $add_type = array(
                'posting_reason' => $postingreason
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_posting_reason_list', $add_type);
            if ($query) {
                redirect('hr_site/posting_reason');
            }
        }
    }


    public function edit_posting_reason($posting_reason_list_id = NULL)
    {
        if(!isset($posting_reason_list_id) || empty($posting_reason_list_id) || !is_numeric($posting_reason_list_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['posting_reason_list_id'] = $this->uri->segment(3);
            $where = array('posting_reason_list_id' => $posting_reason_list_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_posting_reason_list', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'posting_reason' => $this->input->post('posting_reason'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_posting_reason_list', $where, $insert);
                if ($query) {
                    redirect('hr_site/posting_reason/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_posting_reason', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_posting_reason($id = NULL, $tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
            return;
        }


        //Posting Reason.
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'reason_transfer' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
            return;
        }


        //Separation Management.
        $selectTable = 'seperation_management';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_seperation_type' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_posting_reason_list', array('posting_reason_list_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_posting_reason_list', array('posting_reason_list_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_posting_reason_list',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/posting_reason');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function religion()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_religion', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/religion', $data);
        $this->load->view('includes/footer');
    }

    public function add_religion()
    {
        if ($this->input->post('add'))
        {
            $religion = $this->input->post('religion_title');
            if(!isset($religion) || empty($religion)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/religion');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_religion';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(religion_title)' => strtolower($religion),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/religion');
                return;
            }



            $add_type = array(
                'religion_title' => $religion
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_religion', $add_type);
            if ($query)
            {
                redirect('hr_site/religion');
            }
        }
    }


    public function edit_religion($religion_id = NULL)
    {
        if(!isset($religion_id) || empty($religion_id) || !is_numeric($religion_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/religion');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['religion_id'] = $this->uri->segment(3);
            $where = array('religion_id' => $religion_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_religion', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'religion_title' => $this->input->post('religion_title'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_religion', $where, $insert);
                if ($query) {
                    redirect('hr_site/religion/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_religion', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_religion($id = NULL, $tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/religion');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/religion');
            return;
        }

        //Employee..
        $selectTable = 'employee';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'religion' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/religion');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_religion', array('religion_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_religion', array('religion_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_religion',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/religion');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function month()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_month', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/month', $data);
        $this->load->view('includes/footer');
    }

    public function add_month()
    {
        if ($this->input->post('add'))
        {
            $month = $this->input->post('month');
            if(!isset($month) || empty($month))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/month');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_month';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(month)' => strtolower($month),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Nationality Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/month');
                return;
            }



            $add_type = array(
                'month' => $month
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_month', $add_type);
            if ($query)
            {
                redirect('hr_site/month');
            }
        }
    }


    public function edit_month($ml_month_id = NULL)
    {
        if(!isset($ml_month_id) || empty($ml_month_id) || !is_numeric($ml_month_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['ml_month_id'] = $this->uri->segment(3);
            $where = array('ml_month_id' => $ml_month_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_month', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'month' => $this->input->post('month'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_month', $where, $insert);
                if ($query) {
                    redirect('hr_site/month/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_month', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_month($id = NULL, $tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }



        // Attendance
        $selectTable = 'attendence';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'ml_month_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

        // Hourly Time Sheet
        $selectTable = 'hourly_time_sheet';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'ml_month_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

        // Expense Claim
        $selectTable = 'expense_claims';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

        // Expense Disbursment
        $selectTable = 'expense_disbursment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

         // Expense Payment
        $selectTable = 'expense_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

         // Deduction
        $selectTable = 'deduction';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

         // Deduction Processing
        $selectTable = 'deduction_processing';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

         // Salary Payment
        $selectTable = 'salary_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }


         // Loan Advance
        $selectTable = 'loan_advances';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
           'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

         // Allowance
        $selectTable = 'allowance';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
           'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }


         // Allowance Payment
        $selectTable = 'allowance_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
           'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }

        //Increment Processing
        $selectTable = 'increments_processing';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }



        //Loan Advance payment
        $selectTable = 'loan_advance_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }


        //Loan Advance PayBack
        $selectTable = 'loan_advance_payback';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'month' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
            return;
        }



        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_month', array('ml_month_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_month', array('ml_month_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_month',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/month');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function marital()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_marital_status', $where);
        //echo"<pre>"; print_r($data); die;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/marital_status', $data);
        $this->load->view('includes/footer');
    }

    public function add_marital()
    {
        if ($this->input->post('add')) {

            $maritalstatus = $this->input->post('marital_status_title');
            if(!isset($maritalstatus) || empty($maritalstatus)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/marital');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_nationality MLN';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(nationality)' => strtolower($maritalstatus),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/marital');
                return;
            }


            $add_type = array(
                'marital_status_title' => $maritalstatus
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_marital_status', $add_type);
            if ($query) {
                redirect('hr_site/marital');
            }
        }
    }


    public function edit_marital($marital_status_id = NULL)
    {
        if(!isset($marital_status_id) || empty($marital_status_id) || !is_numeric($marital_status_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/marital');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['marital_status_id'] = $this->uri->segment(3);
            $where = array('marital_status_id' => $marital_status_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_marital_status', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'marital_status_title' => $this->input->post('marital_status_title'),
                    'enabled' => $this->input->post('enabled')
                );
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_marital_status', $where, $insert);
                if ($query) {
                    redirect('hr_site/marital/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_marital_status', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_marital($id = NULL,$tableName= NULL)
    {
        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/marital');
            return;
        }


        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/marital');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'employee';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'marital_status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/marital');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_marital_status', array('marital_status_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_marital_status', array('marital_status_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_marital_status',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/marital');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function province()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_province', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/province', $data);
        $this->load->view('includes/footer');
    }

    public function add_province()
    {
        if ($this->input->post('add')) {

            $province = $this->input->post('province_name');
            if(!isset($province) || empty($province)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/province');
                return;
            }


            //Little Fix For Add.
            $table = 'ml_province';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(province_name)' => strtolower($province),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/province');
                return;
            }



            $add_type = array(
                'province_name' => $province
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_province', $add_type);
            if ($query) {
                redirect('hr_site/province');
            }
        }
    }


    public function edit_province($province_id = NULL)
    {
        if(!isset($province_id) || empty($province_id) || !is_numeric($province_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/province');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['province_id'] = $this->uri->segment(3);
            $where = array('province_id' => $province_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_province', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'province_name' => $this->input->post('province_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_province', $where, $insert);
                if ($query) {
                    redirect('hr_site/province/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_province', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_province($id = NULL,$tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/province');
            return;
        }

        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/province');
            return;
        }




        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'permanant_contacts';
        $selectData = array('COUNT(1) AS TotalRecords',false);
        $where = array(
           'trashed !=' => 1,
            'province' => $id

        );
         $result = $this->common_model->select_fields_where($selectTable,$selectData,$where, TRUE);

        if(isset($result)  && $result->TotalRecords > 0)
        {
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/province');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_province', array('province_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_province', array('province_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_province',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/province');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reporting()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_reporting_options', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/reporting', $data);
        $this->load->view('includes/footer');
    }

    public function add_report()
    {
        if ($this->input->post('add')) {

            $report = $this->input->post('reporting_option');
            if(!isset($report) || empty($report)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/reporting');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_reporting_options';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(reporting_option)' => strtolower($report),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Nationality Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/reporting');
                return;
            }


            $add_type = array(
                'reporting_option' => $report
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_reporting_options', $add_type);
            if ($query) {
                redirect('hr_site/reporting');
            }
        }
    }


    public function edit_report($reporting_option_id = NULL)
    {
        if(!isset($reporting_option_id) || empty($reporting_option_id) || !is_numeric($reporting_option_id)){
            $msg = "Something Went Wrong With The Link, If This Message Showed In Error than Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/reporting');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['reporting_option_id'] = $this->uri->segment(3);
            $where = array('reporting_option_id' => $reporting_option_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_reporting_options', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'reporting_option' => $this->input->post('reporting_option'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_reporting_options', $where, $insert);
                if ($query) {
                    redirect('hr_site/reporting/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_reporting', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_report($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/reporting');
            return;
        }


        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/reporting');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'reporting_heirarchy RH';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_reporting_heirarchy_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/reporting');
            return;
        }


        // if record not exist it will be trashed..
        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_reporting_options', array('reporting_option_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_reporting_options', array('reporting_option_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_reporting_options',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/reporting');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function project()
    {
        $trashed = 0;
        $where = array(
            'ml_projects.trashed' => $trashed);
        $data{'data'} = $this->site_model->join_for_project($where);
        $whered = array('trashed' => 0);
        $data['Program'] = $this->hr_model->dropdown_wd_option('ml_program_list', '-- Select Program --', 'ProgramID','Program',$whered);
        //var_dump( $data['Program']); return;
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/project', $data);
        $this->load->view('includes/footer');
    }

    public function add_project()
    {
        if ($this->input->post('add')) {

            $projecttitle = $this->input->post('project_title');
            if(!isset($projecttitle) || empty($projecttitle)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/project');
                return;
            }
            $program = $this->input->post('Program');
            if(!isset($program) || empty($program)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/project');
                return;
            }

            $abbreviation = $this->input->post('Abbreviation');
            if(!isset($abbreviation) || empty($abbreviation)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/project');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_projects';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(project_title)' => strtolower($projecttitle),
                'LOWER(Abbreviation)' => strtolower($abbreviation),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/project');
                return;
            }
            $ProjectStartDate = $this->input->post('project_start_date');
            if(isset($ProjectStartDate) && !empty($ProjectStartDate))
            {
                $ProjectStartDate = date('Y-m-d',strtotime($ProjectStartDate));
            }


            $ProjectEndDate = $this->input->post('project_end_date');
            if(isset($ProjectEndDate) && !empty($ProjectEndDate))
            {
                $ProjectEndDate = date('Y-m-d',strtotime($ProjectEndDate));
            }

            $add_type = array(
                'project_title' => $projecttitle,
                'ProgramID' => $program,
                'Abbreviation' => $abbreviation,
                'project_start_date' => $ProjectStartDate,
                'project_end_date' => $ProjectEndDate,
                'donor' => $this->input->post('donor'),
                'duration' => $this->input->post('duration'),
                'project_status_type_id' => $this->input->post('project_status')
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_projects', $add_type);
            if ($query) {
                redirect('hr_site/project');
            }
        }
    }


    public function edit_project($project_id = NULL)
    {
        if(!isset($project_id) || empty($project_id) || !is_numeric($project_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }
        if ($this->uri->segment(3)) {
            $whered = array('trashed' => 0);
            $data['Program'] = $this->hr_model->dropdown_wd_option('ml_program_list', '-- Select Program --', 'ProgramID','Program',$whered);
            //var_dump( $data['Program']); return;
            $data['project_id'] = $this->uri->segment(3);
            $where = array('project_id' => $project_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_projects', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {

                   $ProjectStartDate = $this->input->post('project_start_date');
                  if(isset($ProjectStartDate) && !empty($ProjectStartDate))
                  {
                      $ProjectStartDate = date('Y-m-d', strtotime($ProjectStartDate));
                  }

                    $ProjectEndDate = $this->input->post('project_end_date');

                    if(isset($ProjectEndDate) && !empty($ProjectEndDate))
                    {
                        $ProjectEndDate = date('Y-m-d', strtotime($ProjectEndDate));
                    }



                $insert = array(
                    'project_title' => $this->input->post('project_title'),
                    'ProgramID' => $this->input->post('Program'),
                    'Abbreviation' => $this->input->post('Abbreviation'),
                    'project_start_date' => $ProjectStartDate,
                    'project_end_date' => $ProjectEndDate,
                    'donor' => $this->input->post('donor'),
                    'duration' => $this->input->post('duration'),
                    'project_status_type_id' => $this->input->post('project_status_type_id'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_projects', $where, $insert);
                if ($query) {
                    redirect('hr_site/project/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_project', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_project($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }


        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'employee_project EP';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'project_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }

        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'timesheet_details TD';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
//            'trashed !=' => 1,
            'project_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }

        //Need TO Know If Record Exist in the alertsTable
        $selectTable = 'employee_alerts';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
                'alert_text' => 'Project',
                'alert_id' => $id
            );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
            return;
        }

        $this->db->trans_start();
        $table_id = $tableName;
        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_projects', array('project_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_projects', array('project_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_projects',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/project');
        }

    }

    //////// Function for Program View //////

    public function ProgramView()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_program_list', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/program', $data);
        $this->load->view('includes/footer');
    }
//End

    /// Function for Add Program ///

    public function AddProgram()
    {
        if ($this->input->post('add')) {

            $program = $this->input->post('program');
            if(!isset($program) || empty($program)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/ProgramView');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_program_list';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(Program)' => strtolower($program),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/ProgramView');
                return;
            }

            $add_type = array(
                'Program' => $program
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_program_list', $add_type);
            if ($query) {
                redirect('hr_site/ProgramView');
            }
        }
    }
//END

/// Function for Edit Program ////
 public function EditProgram($programID = NULL)
 {
     if(!isset($programID) || empty($programID) || !is_numeric($programID)){
         $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
         $this->session->set_flashdata('msg',$msg);
         redirect('hr_site/ProgramView');
         return;
     }
     if ($this->uri->segment(3)) {
         $data['programID'] = $this->uri->segment(3);
         $where = array('ProgramID' => $programID);
         $data['user'] = $this->site_model->get_row_by_id('ml_program_list', $where);
         //echo "<pre>";print_r($data['user']); die;
         if ($this->input->post('edit')) {
             $insert = array(
                 'Program' => $this->input->post('program'),
                 'enabled' => $this->input->post('enabled'));
             //echo "<pre>";print_r($insert); die;
             $query = $this->site_model->update_record('ml_program_list', $where, $insert);
             if ($query) {
                 redirect('hr_site/ProgramView/');
             }
         }
     }

     $this->load->view('includes/header');
     $this->load->view('hr_master_list/edit_program', $data);
     $this->load->view('includes/footer');

 }
//END

/// Function for Trashed //
    public function trashedProgram($id = NULL, $tableName = NULL)
    {

        if ($tableName === NULL || !is_string($tableName)) {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg', $msg);
            redirect('hr_site/ProgramView');
            return;
        }

        if ($id === NULL || !is_numeric($id)) {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg', $msg);
            redirect('hr_site/ProgramView');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'ml_projects';
        $selectData = array('COUNT(1) AS TotalRecords',false);
        $where = array(
            'trashed !=' => 1,
            'ProgramID' => $id

        );
        $result = $this->common_model->select_fields_where($selectTable,$selectData,$where, TRUE);

        if(isset($result)  && $result->TotalRecords > 0)
        {
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/ProgramView');
            return;
        }
        //so if no record found, its save to be trashed..

        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_program_list', array('ProgramID' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_program_list', array('ProgramID' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_program_list',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg', $msg);
            redirect('hr_site/ProgramView');
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function separation()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_separation_type', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/separation_type', $data);
        $this->load->view('includes/footer');
    }

    public function add_separation()
    {
        if ($this->input->post('add')) {

            $separationtype = $this->input->post('separation_type');
            if(!isset($separationtype) || empty($separationtype)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/separation');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_separation_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(separation_type)' => strtolower($separationtype),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/separation');
                return;
            }

            $add_type = array(
                'separation_type' => $separationtype
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_separation_type', $add_type);
            if ($query) {
                redirect('hr_site/separation');
            }
        }
    }


    public function edit_separation($separation_type_id = NULL)
    {
        if(!isset($separation_type_id) || empty($separation_type_id) || !is_numeric($separation_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/separation');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['separation_type_id'] = $this->uri->segment(3);
            $where = array('separation_type_id' => $separation_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_separation_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'separation_type' => $this->input->post('separation_type'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_separation_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/separation/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_separation_type', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_separation($id = NULL, $tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/separation');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/separation');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'seperation_management SM';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_seperation_type' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/separation');
            return;
        }


        //so if no record found, its save to be trashed..

        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_separation_type', array('separation_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_separation_type', array('separation_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_separation_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/separation');
        }

    }

    public function DontLetNumberic($param=null)
    {

    }
    public function appraisal()
    {
        if($this->input->post('add'))
        {
            $InputCheck=$this->input->post('AppraisalTitle');
            DoNotLetNumeric($InputCheck,'hr_site/appraisal');

            $InputDate=array('AppraisalTitle'=>$InputCheck);
            $query=$this->common_model->insert_record('ml_appraisal_title',$InputDate);
            if($query) {
                $AlertMessage = "Record Inserted Successfully!::success";
                $this->session->set_flashdata('msg', $AlertMessage);
                redirect('hr_site/appraisal');
                return;
            }
        }
        $select="AppraisalId,AppraisalTitle";
        $where= array('Trash'=>0);
        $data['Appraisal']=$this->common_model->select_fields_where('ml_appraisal_title',$select,$where);
        $this->load->view('includes/header');
        $this->load->view('appraisal/appraisal_master_lists',$data);
        $this->load->view('includes/footer');
    }

    public function UpdateAppraisal($param=null)
    {
        if($this->input->post('add'))
        {
            $AppraisalHiddenId=intval($this->input->post('AppraisalHiddenId'));
            $where=array('AppraisalId'=>$AppraisalHiddenId);
            //print_r($where);die;
            $InputDate=array('AppraisalTitle'=>$this->input->post('AppraisalTitle'));
            $query=$this->common_model->update('ml_appraisal_title',$where,$InputDate);
            if($query)
            {
                $Alert="Record Updated Successfully!::success";
                $this->session->set_flashdata('msg',$Alert);
                redirect('hr_site/appraisal');
            }
        }
        if(!empty($param))
        {
            $SelectData="AppraisalId AS ID,AppraisalTitle";
            $where=array('AppraisalId'=>$param);
            $data['Appraisal']=$this->common_model->select_fields_where('ml_appraisal_title',$SelectData,$where,true);
            //print_r($data['Appraisal']);
        }
        $this->load->view('includes/header');
        $this->load->view('appraisal/update_appraisal_master_lists',$data);
        $this->load->view('includes/footer');
    }

    public function DeleteAppraisal($param=null)
    {
        if(!empty($param)){
            $where=array('AppraisalId'=>$param);
            $InputDate=array('Trash'=>0);
            $query=$this->common_model->update('ml_appraisal_title',$where,$InputDate);
            if($query)
            {
                redirect('hr_site/appraisal');
            }
        }
    }

    public function FinancialYear()
    {
        if($this->input->post('add'))
        {
            $FinancialYear = $this->input->post('FinancialYear');
            $FromYearID = $this->input->post('FromYearID');
            $ToYearID = $this->input->post('ToYearID');

            $whereFromYear = array('ml_year_id'=>$FromYearID);
            $From=$this->common_model->select_fields_where('ml_year','year AS Year',$whereFromYear,true);


            $whereToYear = array('ml_year_id'=>$ToYearID);
            $To=$this->common_model->select_fields_where('ml_year','year AS Year',$whereToYear,true);

            $YearFromToConcat = $From->Year." - ".$To->Year;


            $FromMonthID = $this->input->post('FromMonthID');
            $ToMonthID = $this->input->post('ToMonthID');
            $inputData=array(
                'FinancialYear'=>$YearFromToConcat,
                'FromYearID'=>$FromYearID,
                'ToYearID'=>$ToYearID,
                'FromMonthID'=>$FromMonthID,
                'ToMonthID'=>$ToMonthID,
            );

            $query=$this->common_model->insert_record('ml_financial_year',$inputData);
            if($query){
                $Alert="Record Inserted Successfully!::success";
                $this->session->set_flashdata('msg',$Alert);
                redirect('hr_site/FinancialYear');
            }
        }

        $SelectData="FY.FinancialYearID AS MainID,FY.FinancialYear,FMY.year AS YearFrom,TMY.year AS YearTo,FMM.month AS MonthFrom,SMM.month AS MonthTo";
        $From = "ml_financial_year FY";
        $join=array(
            array(
                'table'=>'ml_year FMY',
                'condition'=>'FMY.ml_year_id=FY.FromYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_year TMY',
                'condition' => 'TMY.ml_year_id = FY.ToYearID',
                'type' => 'INNER'
            ),
        array(
                'table' => 'ml_month FMM',
                'condition' => 'FMM.ml_month_id = FY.FromMonthID',
                'type' => 'INNER'
            ),
        array(
                'table' => 'ml_month SMM',
                'condition' => 'SMM.ml_month_id = FY.ToMonthID',
                'type' => 'INNER'
            )
        );

        $data['FYDATA']=$this->common_model->select_fields_where_like_join($From,$SelectData,$join);

        //echo $this->db->last_query();
        //return;
        $data['FinancialYear'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');
        $data['FinancialMonth'] = $this->site_model->html_selectbox('ml_month', '-- Select Month --', 'ml_month_id', 'month');

        $this->load->view('includes/header');
        $this->load->view('financialyear/financialyear_master_lists',$data);
        $this->load->view('includes/footer');

    }

    public function UpdateFinancialYear($param=null)
    {
        if($this->input->post('update'))
        {
            $MainID =$this->input->post('MainID');

            $FinancialYear = $this->input->post('FinancialYear');
            $FromYearID = $this->input->post('FromYearID');
            $ToYearID = $this->input->post('ToYearID');
            $FromMonthID = $this->input->post('FromMonthID');
            $ToMonthID = $this->input->post('ToMonthID');
            $inputData=array(
                'FinancialYear'=>$FinancialYear,
                'FromYearID'=>$FromYearID,
                'ToYearID'=>$ToYearID,
                'FromMonthID'=>$FromMonthID,
                'ToMonthID'=>$ToMonthID,
            );
            $where= array('FinancialYearID'=>$MainID);
            $query=$this->common_model->update('ml_financial_year',$where,$inputData);
            if($query)
            {
                $Alert= "Record Updated Successfully!::success";
                $this->session->set_flashdata('msg',$Alert);
                redirect('hr_site/FinancialYear');
            }
            }
        $SelectData="FY.FinancialYearID AS MainID,FY.FinancialYear,FMY.year AS YearFrom,FMY.ml_year_id AS FYearID,
        TMY.ml_year_id AS ToYearID, TMY.year AS YearTo,FMM.month AS MonthFrom,FMM.ml_month_id AS FromMonthID,
        SMM.ml_month_id AS ToMonthID,SMM.month AS MonthTo";
        $From = "ml_financial_year FY";
        $join=array(
            array(
                'table'=>'ml_year FMY',
                'condition'=>'FMY.ml_year_id=FY.FromYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_year TMY',
                'condition' => 'TMY.ml_year_id = FY.ToYearID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month FMM',
                'condition' => 'FMM.ml_month_id = FY.FromMonthID',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_month SMM',
                'condition' => 'SMM.ml_month_id = FY.ToMonthID',
                'type' => 'INNER'
            )
        );
        $where= array('FY.FinancialYearID'=>$param);
        $data['FYDATA']=$this->common_model->select_fields_where_like_join($From,$SelectData,$join,$where,true);
        //print_r($data['FYDATA']);

        $data['FinancialYear'] = $this->site_model->html_selectbox('ml_year', '-- Select Year --', 'ml_year_id', 'year');
        $data['FinancialMonth'] = $this->site_model->html_selectbox('ml_month', '-- Select Month --', 'ml_month_id', 'month');
        $this->load->view('includes/header');
        $this->load->view('financialyear/update_financialyear_master_lists',$data);
        $this->load->view('includes/footer');

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function year()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_year', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/year', $data);
        $this->load->view('includes/footer');
    }

    public function add_year()
    {
        if ($this->input->post('add'))
        {
            $year = $this->input->post('year');
            if(!isset($year) || empty($year) || !is_numeric($year))
            {
                $msg ="Please Enter Year. ::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/year');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_year';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(year)' => strtolower($year),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/year');
                return;
            }




            $add_type = array(
                'year' => $year
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_year', $add_type);
            if ($query)
            {
                redirect('hr_site/year');
            }
        }
    }


    public function edit_year($ml_year_id = NULL)
    {
        if(!isset($ml_year_id) || empty($ml_year_id) || !is_numeric($ml_year_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['ml_year_id'] = $this->uri->segment(3);
            $where = array('ml_year_id' => $ml_year_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_year', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'year' => $this->input->post('year'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_year', $where, $insert);
                if ($query) {
                    redirect('hr_site/year/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_year', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_year($id = NULL ,$tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }



        // Attendance
        $selectTable = 'attendence';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'ml_year_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Hourly Time Sheet
        $selectTable = 'hourly_time_sheet';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'ml_year_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Expense Claim
        $selectTable = 'expense_claims';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Expense Disbursment
        $selectTable = 'expense_disbursment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Expense Payment
        $selectTable = 'expense_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Deduction
        $selectTable = 'deduction';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Deduction Processing
        $selectTable = 'deduction_processing';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Salary Payment
        $selectTable = 'salary_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }


        // Loan Advance
        $selectTable = 'loan_advances';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        // Allowance
        $selectTable = 'allowance';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }


        // Allowance Payment
        $selectTable = 'allowance_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }

        //Increment Processing
        $selectTable = 'increments_processing';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }



        //Loan Advance payment
        $selectTable = 'loan_advance_payment';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }


        //Loan Advance PayBack
        $selectTable = 'loan_advance_payback';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'year' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');
            return;
        }



        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_year', array('ml_year_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_year', array('ml_year_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_year',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/year');

        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function shift()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_shift', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/shift', $data);
        $this->load->view('includes/footer');
    }

    public function add_shift()
    {
        if ($this->input->post('add')) {

            $shift = $this->input->post('shift_name');
            if(!isset($shift) || empty($shift)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/shift');
                return;
            }


            //Little Fix For Add.
            $table = 'ml_shift';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(shift_name)' => strtolower($shift),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/shift');
                return;
            }

            $add_type = array(
                'shift_name' => $shift
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_shift', $add_type);
            if ($query) {
                redirect('hr_site/shift');
            }
        }
    }


    public function edit_shift($shift_id = NULL)
    {
        if(!isset($shift_id) || empty($shift_id) || !is_numeric($shift_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/shift');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['shift_id'] = $this->uri->segment(3);
            $where = array('shift_id' => $shift_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_shift', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'shift_name' => $this->input->post('shift_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_shift', $where, $insert);
                if ($query) {
                    redirect('hr_site/shift/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_shift', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_shift($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/shift');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/shift');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'shift' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/shift');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_shift', array('shift_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_shift', array('shift_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_shift',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {


            redirect('hr_site/shift');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function status()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_status', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/status', $data);
        $this->load->view('includes/footer');
    }

    public function add_status()
    {
        if ($this->input->post('add')) {
            $status = $this->input->post('status_title');
            if(!isset($status) || empty($status)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/status');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_status';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(status_title)' => strtolower($status),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/status');
                return;
            }

            $add_type = array(
                'status_title' => $status
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_status', $add_type);
            if ($query) {
                redirect('hr_site/status');
            }
        }
    }


    public function edit_status($status_id = NULL)
    {
        if(!isset($status_id) || empty($status_id) || !is_numeric($status_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['status_id'] = $this->uri->segment(3);
            $where = array('status_id' => $status_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_status', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'status_title' => $this->input->post('status_title'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_status', $where, $insert);
                if ($query) {
                    redirect('hr_site/status/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_status', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_status($id = NULL, $tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.

        //leave Application
        $selectTable = 'leave_application LA';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        //Leave Approval

        $selectTable = 'leave_approval LAP';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        //Leave Entitlement

        $selectTable = 'leave_entitlement LE';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Allowance

        $selectTable = 'allowance AL';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }


        // Benefits

        $selectTable = 'benefits B';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Contract

        $selectTable = 'contract C';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Deduction

        $selectTable = 'deduction D';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }


        // Expense Claim

        $selectTable = 'expense_claims EC';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }


        // Fail Reviews
        $selectTable = 'fail_reviews FR';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

       // Increment
        $selectTable = 'increments I';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

          // Loan Advances
        $selectTable = 'loan_advances LA';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Position Management
        $selectTable = 'position_management PM';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Posting
        $selectTable = 'posting P';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Salary
        $selectTable = 'salary s';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Separation_management
        $selectTable = 'seperation_management SM';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }

        // Transaction
        $selectTable = 'transaction T';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'status' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
            return;
        }






        //As No Record Found So, Its Save To Be Trashed..
        $this->db->trans_start();
        $table_id = $tableName;
        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_status', array('status_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);

            $query = $this->site_model->update_record('ml_status', array('status_id' => $id), $update_trash);

        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_status',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->data['EmployeeID']
        );
        //echo "<pre>" ;print_r($insert_trash);die;
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/status');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function user_group()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_user_groups', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/user_group', $data);
        $this->load->view('includes/footer');
    }

    public function add_user_group()
    {
        if ($this->input->post('add'))
        {
            $usergroup = $this->input->post('user_group_title');
            if(!isset($usergroup) || empty($usergroup))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/user_group');
                return;
            }


            //Little Fix For Add.......
            $table = 'ml_user_groups';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(user_group_title)' => strtolower($usergroup),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/user_group');
                return;
            }



            $add_type = array(
                'user_group_title' => $usergroup
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_user_groups', $add_type);
            if ($query)
            {
                redirect('hr_site/user_group');
            }
        }
    }


    public function edit_user($user_group_id = NULL)
    {
        if(!isset($user_group_id) || empty($user_group_id) || !is_numeric($user_group_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['user_group_id'] = $this->uri->segment(3);
            $where = array('user_group_id' => $user_group_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_user_groups', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'user_group_title' => $this->input->post('user_group_title'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_user_groups', $where, $insert);
                if ($query) {
                    redirect('hr_site/user_group/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_user_group', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_user($id = NULL,$tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }


        // User Account
        $selectTable = 'user_account';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'user_group' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }



        // User Group privileges..
        $selectTable = 'user_groups_privileges';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'user_group_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }


        // User Group Views privileges..
        $selectTable = 'user_groups_views_privileges';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'user_group_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }


        // User Login Logs..
        $selectTable = 'users_login_logs';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            //'trashed !=' => 1,
            'user_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_user_groups', array('user_group_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_user_groups', array('user_group_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_user_groups',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/user_group');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function district()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_district', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/district', $data);
        $this->load->view('includes/footer');
    }

    public function add_distt()
    {
        if ($this->input->post('add'))
        {

            $district = $this->input->post('district_name');
            if(!isset($district) || empty($district)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/district');
                return;
            }


            //Little Fix For Add.......
            $table = 'ml_district';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(district_name)' => strtolower($district),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/district');
                return;
            }



            $add_type = array(
                'district_name' => $district
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_district', $add_type);
            if ($query) {
                redirect('hr_site/district');
            }
        }
    }


    public function edit_distt($district_id = NULL)
    {
        if(!isset($district_id) || empty($district_id) || !is_numeric($district_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['district_id'] = $this->uri->segment(3);
            $where = array('district_id' => $district_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_district', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'district_name' => $this->input->post('district_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_district', $where, $insert);
                if ($query) {
                    redirect('hr_site/district/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_district', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_distt($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
            return;
        }


        //Permanent Contact.
        $selectTable = 'permanant_contacts';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'district' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
            return;
        }

        //Current Contact.
        $selectTable = 'current_contacts';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'District' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_district', array('district_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_district', array('district_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_district',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/district');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function city()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_city', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/city', $data);
        $this->load->view('includes/footer');
    }

    public function add_city()
    {
        if ($this->input->post('add')) {

            $city = $this->input->post('city_name');
            if(!isset($city) || empty($city)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/city');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_city';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(city_name)' => strtolower($city),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/city');
                return;
            }


            $add_type = array(
                'city_name' => $city
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_city', $add_type);
            if ($query) {
                redirect('hr_site/city');
            }
        }
    }


    public function edit_city($city_id = NULL)
    {
        if(!isset($city_id) || empty($city_id) || !is_numeric($city_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['city_id'] = $this->uri->segment(3);
            $where = array('city_id' => $city_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_city', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'city_name' => $this->input->post('city_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_city', $where, $insert);
                if ($query) {
                    redirect('hr_site/city/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_city', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_city($id = NULL,$tableName =NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
            return;
        }



        //Permanent Contacts.
        $selectTable = 'permanant_contacts';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'city_village' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
            return;
        }



        //Posting.
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_city_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
            return;
        }





        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_city', array('city_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_city', array('city_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_city',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/city');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function gender()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_gender_type', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/gender', $data);
        $this->load->view('includes/footer');
    }

    public function add_gender()
    {
        if ($this->input->post('add')) {

            $gender = $this->input->post('gender_type_title');
            if(!isset($gender) || empty($gender))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/gender');
                return;
            }


            //Little Fix For Add......
            $table = 'ml_gender_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(gender_type_title)' => strtolower($gender),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/gender');
                return;
            }


            $add_type = array(
                'gender_type_title' => $gender
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_gender_type', $add_type);
            if ($query) {
                redirect('hr_site/gender');
            }
        }
    }


    public function edit_gender($gender_type_id = NULL)
    {
        if(!isset($gender_type_id) || empty($gender_type_id) || !is_numeric($gender_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/gender');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['gender_type_id'] = $this->uri->segment(3);
            $where = array('gender_type_id' => $gender_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_gender_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'gender_type_title' => $this->input->post('gender_type_title'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_gender_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/gender/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_gender', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_gender($id = NULL,$tableName = NULL)
    {

        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/gender');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/gender');
            return;
        }



//To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'employee';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'gender' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/gender');
            return;
        }




        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_gender_type', array('gender_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_gender_type', array('gender_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_gender_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/gender');
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function task()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_assign_task', $where);
        $data{'project'} = $this->site_model->get_all('ml_projects');
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/assign_task', $data);
        $this->load->view('includes/footer');
    }

    public function add_task()
    {
        if ($this->input->post('add'))
        {

            $taskname = $this->input->post('task_name');
            if(!isset($taskname) || empty($taskname)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/task');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_assign_task';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(task_name)' => strtolower($taskname),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/task');
                return;
            }


            $add_type = array(
                'project_id'=>$this->input->post('project'),
                'task_name' => $taskname
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_assign_task', $add_type);
            if ($query)
            {
                redirect('hr_site/task');
            }
        }
    }


    public function edit_task($ml_assign_task_id = NULL)
    {
        if(!isset($ml_assign_task_id) || empty($ml_assign_task_id) || !is_numeric($ml_assign_task_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['ml_assign_task_id'] = $this->uri->segment(3);
            $where = array('ml_assign_task_id' => $ml_assign_task_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_assign_task', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'task_name' => $this->input->post('task_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_assign_task', $where, $insert);
                if ($query) {
                    redirect('hr_site/task/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_assign_task', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_task($id = NULL,$tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
            return;
        }


        // Assign Job
        $selectTable = 'assign_job';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_assign_task_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
            return;
        }

        // Milestone
        $selectTable = 'milestone';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'task_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
            return;
        }






        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_assign_task', array('ml_assign_task_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_assign_task', array('ml_assign_task_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_assign_task',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/task');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function relations()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_relation', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/relations', $data);
        $this->load->view('includes/footer');
    }

    public function add_relation()
    {
        if ($this->input->post('add'))
        {

            $relation = $this->input->post('relation_name');
            if(!isset($relation) || empty($relation))
            {
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/relations');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_relation';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(relation_name)' => strtolower($relation),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0)
            {
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/relations');
                return;
            }



            $add_type = array(
                'relation_name' => $relation
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_relation', $add_type);
            if ($query)
            {
                redirect('hr_site/relations');
            }
        }
    }


    public function edit_relation($relation_id = NULL)
    {
        if(!isset($relation_id) || empty($relation_id) || !is_numeric($relation_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['relation_id'] = $this->uri->segment(3);
            $where = array('relation_id' => $relation_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_relation', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'relation_name' => $this->input->post('relation_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_relation', $where, $insert);
                if ($query) {
                    redirect('hr_site/relations/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_relations', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_relation($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
            return;
        }


        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
            return;
        }


        //Dependents.
        $selectTable = 'dependents';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_relationship_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
            return;
        }


        // Emergency Contacts
        $selectTable = 'emergency_contacts';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'relationship' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
            return;
        }



        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_relation', array('relation_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_relation', array('relation_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_relation',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/relations');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////		
    public function branch()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_branch', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/branch', $data);
        $this->load->view('includes/footer');
    }

    public function add_branch()
    {
        if ($this->input->post('add'))
        {

            $branch = $this->input->post('branch_name');
            if(!isset($branch) || empty($branch)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/branch');
                return;
            }


            //Little Fix For Add......
            $table = 'ml_branch';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(branch_name)' => strtolower($branch),
                'trashed' => 0
            );

            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/branch');
                return;
            }
            


            $add_type = array(
                'branch_name' => $branch
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_branch', $add_type);
            if ($query)
            {
                redirect('hr_site/branch');
            }
        }
    }


    public function edit_branch($branch_id = NULL)
    {
        if(!isset($branch_id) || empty($branch_id) || !is_numeric($branch_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/branch');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['branch_id'] = $this->uri->segment(3);
            $where = array('branch_id' => $branch_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_branch', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'branch_name' => $this->input->post('branch_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_branch', $where, $insert);
                if ($query) {
                    redirect('hr_site/branch/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_branch', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_branch($id = NULL,$tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/branch');
            return;
        }


        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/branch');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_branch_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/branch');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_branch', array('branch_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_branch', array('branch_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_branch',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/branch');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////		
    public function department()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_department', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/department', $data);
        $this->load->view('includes/footer');
    }

    public function add_department()
    {
        if ($this->input->post('add')) {

            $department = $this->input->post('department_name');
            if(!isset($department) || empty($department)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/department');
                return;
            }


            //Little Fix For Add.....
            $table = 'ml_department';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(department_name)' => strtolower($department),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/department');
                return;
            }


            $add_type = array(
                'department_name' => $department
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_department', $add_type);
            if ($query) {
                redirect('hr_site/department');
            }
        }
    }


    public function edit_department($department_id = NULL)
    {
        if(!isset($department_id) || empty($department_id) || !is_numeric($department_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['department_id'] = $this->uri->segment(3);
            $where = array('department_id' => $department_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_department', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'department_name' => $this->input->post('department_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_department', $where, $insert);
                if ($query) {
                    redirect('hr_site/department/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_department', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_department($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
            return;
        }

        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'posting';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_department_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
            return;
        }

        // Go To List
        $selectTable = 'goto_list';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_department_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_department', array('department_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_department', array('department_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_department',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/department');
        }

    }


//////////////////////////////////////////////////////////////////////////////////////		
    public function skills()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_skill_type', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/skills', $data);
        $this->load->view('includes/footer');
    }

    public function add_skill()
    {
        if ($this->input->post('add')) {

            $skill = $this->input->post('skill_name');
            if(!isset($skill) || empty($skill)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/skills');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_skill_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(skill_name)' => strtolower($skill),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Skill Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/skills');
                return;
            }


            $add_type = array(
                'skill_name' => $skill
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_skill_type', $add_type);
            if ($query) {
                redirect('hr_site/skills');
            }
        }
    }


    public function edit_skill($skill_type_id = NULL)
    {
        if(!isset($skill_type_id) || empty($skill_type_id) || !is_numeric($skill_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['skill_type_id'] = $this->uri->segment(3);
            $where = array('skill_type_id' => $skill_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_skill_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'skill_name' => $this->input->post('skill_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_skill_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/skills/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_skills', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_skill($id = NULL, $tableName = NULL)
    {



        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;
        }


        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;

        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'emp_skills';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_skill_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_skill_type', array('skill_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_skill_type', array('skill_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_skill_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
        }

    }

    ///////////////////////////////////// SKILLS Levels

    public function skill_levels()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_skill_level', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/skill_levels', $data);
        $this->load->view('includes/footer');
    }

    public function add_skill_level()
    {
        if ($this->input->post('level')) {

            $skill_level = $this->input->post('level');
            if(!isset($skill_level) || empty($skill_level)){
                $msg ="Please Enter Skill Level::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/skill_levels');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_skill_level';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(skill_level)' => strtolower($skill_level),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Skill Level Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/skill_levels');
                return;
            }


            $add_type = array(
                'skill_level' => $skill_level
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_skill_level', $add_type);
            if ($query) {
                redirect('hr_site/skill_levels');
            }
        }
    }


    public function edit_skill_level($skill_level_id = NULL)
    {
        if(!isset($skill_level_id) || empty($skill_level_id) || !is_numeric($skill_level_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skill_levels');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['level_id'] = $this->uri->segment(3);
            $where = array('level_id' => $skill_level_id);
            $data['skill'] = $this->site_model->get_row_by_id('ml_skill_level', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'skill_level' => $this->input->post('skill_level'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_skill_level', $where, $insert);
                if ($query) {
                    redirect('hr_site/skill_levels/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_skill_level', $data);
        $this->load->view('includes/footer');
    }


    public function trashed_skill_level($id = NULL, $tableName = NULL)
    {



        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;
        }


        if($id === NULL || !is_numeric($id))
        {
            $msg = "<span style='color: red'>Some Error Occurred, Please Contact System Administrator For Further Assistance</span>";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skills');
            return;

        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'emp_skills';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_skill_level_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "<span style='color: red'>Record Have Already Been Used, So Cant Be Deleted</span>";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skill_levels');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_skill_level', array('level_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_skill_level', array('level_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_skill_level',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "<span style='color: #008000'>Record Have Been Successfully Trashed From The System</span>";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/skill_levels');
        }

    }
    ///////////////////////////////////// End

////////////////////////////////////////////////////////////////////////////////////////
    public function degree_level()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_qualification_type', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/degree_level', $data);
        $this->load->view('includes/footer');
    }




    public function add_degree()
    {
        if ($this->input->post('add')) {

            $qualification = $this->input->post('qualification_title');
            if(!isset($qualification) || empty($qualification)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/degree_level');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_qualification_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(qualification_title)' => strtolower($qualification),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/degree_level');
                return;
            }

            $add_type = array(
                'qualification_title' => $qualification
            );
            //echo "<pre>"; print_r($add_type); die;
           $query = $this->site_model->create_new_record('ml_qualification_type', $add_type);
            if ($query) {
                redirect('hr_site/degree_level');
            }
        }
    }


    public function edit_degree($qualification_type_id = NULL)
    {
        if(!isset($qualification_type_id) || empty($qualification_type_id) || !is_numeric($qualification_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/degree_level');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['qualification_type_id'] = $this->uri->segment(3);
            $where = array('qualification_type_id' => $qualification_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_qualification_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'qualification_title' => $this->input->post('qualification_title'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_qualification_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/degree_level/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_degree_level', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_degree($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/degree_level');
            return;
        }

        if($id === NULL || !is_numeric($id))
        {
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/degree_level');
            return;
        }

      //To Trash Record We First Need TO Check If Record Already Exist Or Not..
        $selectTable = 'qualification';
        $selectData = array('COUNT(1) AS TotalRecordsFound', false);
        $where = array(
            'trashed !=' => 1,
            'qualification_type_id' =>$id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/degree_level');
            return;
        }

        // if already exist... it will be trashed.....
        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_qualification_type', array('qualification_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_qualification_type', array('qualification_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_qualification_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/degree_level');
        }

    }


////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function nationality()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_nationality', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/nationality', $data);
        $this->load->view('includes/footer');
    }

    public function add_nationality()
    {
        if ($this->input->post('add')) {
            $postedNationality = $this->input->post('nationality');
            if(!isset($postedNationality) || empty($postedNationality)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/nationality');
                return;
            }
/*            $rec = $this->site_model->get_all('ml_nationality');
            foreach($rec as $rec) {
                //strcasecmp($rec->module_name,$this->input->post('module_name'));
                if (strcasecmp($rec->nationality,$this->input->post('nationality')) == 0)

                {
                    $msg ="OOps Already Inserted !";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('hr_site/nationality');
                    return;}
            }*/

            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_nationality MLN';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(nationality)' => strtolower($postedNationality),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Nationality Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/nationality');
                return;
            }
            $add_type = array(
                'nationality' => $postedNationality
            );
            $query = $this->site_model->create_new_record('ml_nationality', $add_type);
            if ($query) {
                $msg ="New Record Successfully Added To The System::success";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/nationality');
            }
        }
    }

    public function edit_nationality($nationality_id = NULL)
    {
        if(!isset($nationality_id) || empty($nationality_id) || !is_numeric($nationality_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/nationality');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['nationality_id'] = $this->uri->segment(3);
            $where = array('nationality_id' => $nationality_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_nationality', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'nationality' => $this->input->post('nationality'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_nationality', $where, $insert);
                if ($query) {
                    redirect('hr_site/nationality/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_nationality', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_nationality($id = NULL , $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/nationality');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/nationality');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'employee EMP';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'nationality' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/nationality');
            return;
        }




        // if record will not exist it will be trashed..
        $this->db->trans_start();
        $table_id = $tableName;
        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_nationality', array('nationality_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_nationality', array('nationality_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_nationality',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/nationality');
        }

    }

///////////////////////////////////////////////////////////////////////////////////////////////////		
    public function employment_type()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_employment_type', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/employment_type', $data);
        $this->load->view('includes/footer');
    }

    public function add_employment_type()
    {
        if ($this->input->post('add')) {
            $employment = $this->input->post('employment_type');
            if(!isset($employment) || empty($employment)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/employment_type');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_employment_type';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(employment_type)' => strtolower($employment),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Nationality Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/employment_type');
                return;
            }
            $add_type = array(
                'employment_type' => $employment
            );
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_employment_type', $add_type);
            if ($query) {
                redirect('hr_site/employment_type');
            }
        }
    }

    public function edit_employement_type($employment_type_id = NULL)
    {
        if(!isset($employment_type_id) || empty($employment_type_id) || !is_numeric($employment_type_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
            return;
        }
        if ($this->uri->segment(3)) {
            $data['employment_type_id'] = $this->uri->segment(3);
            $where = array('employment_type_id' => $employment_type_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_employment_type', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'employment_type' => $this->input->post('employment_type'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_employment_type', $where, $insert);
                if ($query) {
                    redirect('hr_site/employment_type/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_employement_type', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_employee($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'contract C';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'employment_type' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
            return;
        }

        //Check If Record Exist In Employee Experience
        $selectTable = 'emp_experience EE';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_employment_type_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);
        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
            return;
        }

        //As No Record Found So, Its Save To Be Trashed..
        $this->db->trans_start();
        $table_id = $tableName;
        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_employment_type', array('employment_type_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_employment_type', array('employment_type_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_employment_type',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/employment_type');
        }
    }


///////////////////////////////////////////////////////////////////////////////////////////////////		
    public function designation()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_designations', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/designation', $data);
        $this->load->view('includes/footer');
    }

    public function add_designation()
    {
        if ($this->input->post('add')) {
            $designation = $this->input->post('designation_name');
            if(!isset($designation) || empty($designation)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/designation');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_designations';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(designation_name)' => strtolower($designation),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/designation');
                return;
            }

            $add_type = array(
                'designation_name' => $designation);
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_designations', $add_type);
            if ($query) {
                redirect('hr_site/designation');
            }
        }
    }

    public function edit_designation($ml_designation_id = NULL)
    {
        if(!isset($ml_designation_id) || empty($ml_designation_id) || !is_numeric($ml_designation_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/designation');
            return;
        }
        if ($this->uri->segment(3)){
            $data['ml_designation_id'] = $this->uri->segment(3);
            $where = array('ml_designation_id' => $ml_designation_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_designations', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'designation_name' => $this->input->post('designation_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_designations', $where, $insert);
                if ($query) {
                    redirect('hr_site/designation/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_designation', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_designation($id = NULL, $tableName = NULL)
    {


        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/designation');
            return;
        }

        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/designation');
            return;
        }


        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'position_management PM';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'ml_designation_id' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/designation');
            return;
        }


        $this->db->trans_start();
        $table_id = $tableName;

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_designations', array('ml_designation_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_designations', array('ml_designation_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_designations',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash)
        {
            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/designation');
        }

    }

///////////////////////////////////////////////////////////////////////////////////////////////////		
    public function job_category()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_job_category', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/job_category', $data);
        $this->load->view('includes/footer');
    }

    public function add_job_category()
    {
        if ($this->input->post('add')) {
            $postedjobcategory = $this->input->post('job_category_name');
            if(!isset($postedjobcategory) || empty($postedjobcategory)){
                $msg ="Some Problem Occur with the Post, Please Contact System Administrator For Further Assistance.::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/job_category');
                return;
            }


            //Little Fix For Add, As Above Code Has a Bug it will not Add if even Trashed.
            $table = 'ml_job_category';
            $data = array('COUNT(1) As TotalRecordsFound');
            $where= array(
                'LOWER(job_category_name)' => strtolower($postedjobcategory),
                'trashed' => 0
            );
            $countResult = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            if(isset($countResult) && !empty($countResult) && $countResult->TotalRecordsFound > 0){
                $msg ="Record with Same Name Already Exist, Record Could Not Be Added::error";
                $this->session->set_flashdata('msg',$msg);
                redirect('hr_site/job_category');
                return;
            }


            $add_type = array(
                'job_category_name' => $postedjobcategory
            );
         // echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_job_category', $add_type);
            if ($query) {
                redirect('hr_site/job_category');
            }
        }
    }

    public function edit_master_list_job_category($job_category_id = NULL)
    {
        if(!isset($job_category_id) || empty($job_category_id) || !is_numeric($job_category_id)){
            $msg = "Something Went Wrong With The Link, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/job_category');
            return;
        }

        if ($this->uri->segment(3)) {
            $data['job_category_id'] = $this->uri->segment(3);
            $where = array('job_category_id' => $job_category_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_job_category', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'job_category_name' => $this->input->post('job_category_name'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_job_category', $where, $insert);
                if ($query) {
                    redirect('hr_site/job_category/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_master_list_job_category', $data);
        $this->load->view('includes/footer');
    }

    public function trashed_category($id = NULL , $tableName = NULL)
    {
        if($tableName === NULL || !is_string($tableName)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/job_category');
            return;
        }



        if($id === NULL || !is_numeric($id)){
            $msg = "Some Error Occurred, Please Contact System Administrator For Further Assistance::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/job_category');
            return;
        }

        //To Trash Record We First Need TO Check If Record Already Exist Or Not.
        $selectTable = 'contract C';
        $selectData = array('COUNT(1) AS TotalRecordsFound',false);
        $where = array(
            'trashed !=' => 1,
            'employment_category' => $id
        );
        $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

        if(isset($countResult) && $countResult->TotalRecordsFound > 0){
            $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/job_category');
            return;
        }


        //As No Record Found So, Its Save To Be Trashed..
        $this->db->trans_start();
        $table_id = $tableName;
        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_job_category', array('job_category_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_job_category', array('job_category_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_job_category',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {

            $msg = "Record Have Been Successfully Trashed From The System::success";
            $this->session->set_flashdata('msg',$msg);
            redirect('hr_site/job_category');
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////		
    public function job_title()
    {
        $trashed = 0;
        $where = array(
            'trashed' => $trashed);
        $data{'data'} = $this->site_model->get_all_by_where('ml_job_title', $where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/master_lists', $data);
        $this->load->view('includes/footer');
    }

    public function add_job_title()
    {
        if ($this->input->post('add')) {
            $add_type = array(
                'job_title' => $this->input->post('job_title'));
            //echo "<pre>"; print_r($add_type); die;
            $query = $this->site_model->create_new_record('ml_job_title', $add_type);
            if ($query) {
                redirect('hr_site/job_title');
            }
        }
    }

    public function trashed_job_title($id = NULL)
    {

        $this->db->trans_start();
        $table_id = $this->uri->segment(4);

        $data['emer_contact'] = $this->site_model->get_row_by_id('ml_job_title', array('ml_job_title_id' => $id));
        if ($data['emer_contact']->trashed == 0) {
            $update_trash = array('trashed' => 1);
            $query = $this->site_model->update_record('ml_job_title', array('ml_job_title_id' => $id), $update_trash);
        }

        $insert_trash = array(
            'record_id' => $id,
            'table_id_name' => $table_id,
            'table_name' => 'ml_job_title',
            'trash_date' => date('Y-m-d'),
            'trashed_by' => $this->session->userdata('employee_id')
        );
        $query_trash = $this->site_model->create_new_record('trash_store', $insert_trash);

        $this->db->trans_complete();

        if ($query || $query_trash) {


            redirect('hr_site/job_title');
        }

    }

////////////FUNCTION FOR EDIT JOB//////////////////////////////

    public function edit_job_title($ml_job_title_id = NULL)
    {
        if ($this->uri->segment(3)) {
            $data['ml_job_title_id'] = $this->uri->segment(3);
            $where = array('ml_job_title_id' => $ml_job_title_id);
            $data['user'] = $this->site_model->get_row_by_id('ml_job_title', $where);
            //echo "<pre>";print_r($data['user']); die;
            if ($this->input->post('edit')) {
                $insert = array(
                    'job_title' => $this->input->post('job_title'),
                    'enabled' => $this->input->post('enabled'));
                //echo "<pre>";print_r($insert); die;
                $query = $this->site_model->update_record('ml_job_title', $where, $insert);
                if ($query) {
                    redirect('hr_site/job_title/');
                }
            }
        }

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/edit_master_list_job_title', $data);
        $this->load->view('includes/footer');
    }


    /**
     * Organization Info Edit/Update Info..
     * @param null $employeeID
     */
    public function organization_info()
    {
        //Get Already Inserted Data from the Table
        $tbl = 'organization';
        $result = $this->common_model->select($tbl);
        if($result){
            $viewData['data'] = $result;
        }
        if(!isset($viewData)){
            $viewData = '';
        }
        $viewData['attendance'] = $this->site_model->html_selectbox('ml_configrationtypes', '-- Select Attendance Type --', 'ConfigID', 'ConfigTypes');

        $this->load->view('includes/header');
        $this->load->view('hr_master_list/organisation_info',$viewData);
        $this->load->view('includes/footer');
    }
    public function create_update_organization(){
        if($this->input->post()){
            $organizationName = $this->input->post('o_name');
            $organizationCity = $this->input->post('o_city');
            $organizationCountry = $this->input->post('o_country');
            $organizationPhone = $this->input->post('o_phone');
            $organizationWebsite = $this->input->post('o_website');
            $organizationEmail = $this->input->post('o_email');
            $organizationAddress = $this->input->post('o_address');
            $ConfigTypes = $this->input->post('ConfigTypes');
            $InsertData = array(
                'org_name' => $organizationName,
                'phone' => $organizationPhone,
                'city' => $organizationCity,
                'country' => $organizationCountry,
                'email' => $organizationEmail,
                'website' => $organizationWebsite,
                'address' => $organizationAddress,
                'ConfigID' => $ConfigTypes
            );
            $tbl = 'organization';
            $data = ('organization_id');
            $result = $this->common_model->select_fields($tbl,$data,TRUE);
            if(!empty($result)){
                $fields = array(
                    'organization_id' => $result->organization_id
                );
                $result = $this->common_model->update($tbl,$fields,$InsertData);
                if($result === true){
                    echo "OK::Record Successfully Updated::success";
                }
            } else{
                $result = $this->common_model->insert_record($tbl,$InsertData);
                if($result>0){
                    echo "OK::Record Successfully Added::success";
                }
            }
        }
    }
//End Of Organization Info..

    /**
     * Appointment Letter and Contract Letter Functions
     * @param null $employeeID
     */
    public function appointment($employeeID = NULL)
    {
        if ($employeeID != NULL) {

            $PTable = 'employee E';
            $employeeData = array('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        ET.joining_date AS StartDate,
        PM.job_specifications AS Position,
        IFNULL(S.base_salary,"Salary Not SET") AS BaseSalary
        ',false);
            $joins = array(
                array(
                    'table' => 'current_contacts CC',
                    'condition' => 'E.employee_id=CC.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id=ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id AND PM.current=1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'salary S',
                    'condition' => 'ET.employment_id=S.employement_id',
                    'type' => 'LEFT'
                )
            );
            $where = array(
                'E.trashed' => '0',
                'E.employee_id' => $employeeID
            );
            $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);
//        print_r($data);

            //Now Need to Get Company Information
            $tbl = 'organization O';
            $org_data = ('*');
            $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
            //Now need to get the General Format for User..
            $table = 'ml_letter_text';
            $letterData = ('LetterText');
            $condition = array(
                'LetterTextID' => '1'
            );
            $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        } else {
            $data = array();
        }
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/templates', $data);
        $this->load->view('includes/footer');
    }

    public function coverletter($employeeID = NULL)
    {
        if ($employeeID != NULL) {
            $PTable = 'employee E';
            $employeeData = ('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        ET.joining_date AS StartDate,
        PM.job_specifications AS Position,
        S.base_salary AS BaseSalary
        ');
            $joins = array(
                array(
                    'table' => 'current_contacts CC',
                    'condition' => 'E.employee_id=CC.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employment ET',
                    'condition' => 'E.employee_id=ET.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'ET.employment_id = PM.employement_id AND PM.current=0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'salary S',
                    'condition' => 'ET.employment_id=S.employement_id',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.trashed' => '0',
                'E.employee_id' => $employeeID
            );
            $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);
//        print_r($data);

            //Now Need to Get Company Information
            $tbl = 'organization O';
            $org_data = ('*');
            $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
            //Now need to get the General Format for User..
            $table = 'ml_letter_text';
            $letterData = ('LetterText');
            $condition = array(
                'LetterTextID' => '2'
            );
            $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        } else {
            $data = array();
        }

        //Now Just Display the View and Send the Above Data to that View
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/contract_letter', $data);
        $this->load->view('includes/footer');
    }

    public function appointment_letter($employeeID = NULL)
    {
        $PTable = 'employee E';
        $employeeData = ('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        ET.joining_date AS StartDate,
        MLDg.designation_name AS Position,
        S.base_salary AS BaseSalary
        ');
        $joins = array(
            array(
                'table' => 'current_contacts CC',
                'condition' => 'E.employee_id=CC.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id=ET.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'ET.employment_id = PM.employement_id AND PM.current = 1 AND PM.trashed = 0',
                'type' => 'INNER'
            ),
            array(
                'table' => 'salary S',
                'condition' => 'ET.employment_id=S.employement_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = array(
            'E.trashed' => '0',
            'E.employee_id' => $employeeID,
            'PM.status'=>2,
            'S.status'=>2
        );
        $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);
//        print_r($data);
        //Now Need to Get Company Information
        $tbl = 'organization O';
        $org_data = ('*');
        $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
        //Now need to get the General Updated Format for User..
        $table = 'ml_letter_text';
        $letterData = ('LetterText');
        $condition = array(
            'LetterTextID' => '1'
        );
        $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        //Get Current Logged In User Information..
        $loggedInEmployee = $this->session->userdata('employee_id');
        $EmployeeTable = 'employee';
        $EmployeeDetails = ('full_name AS FullName');
        $where = array(
            'employee_id' => $loggedInEmployee
        );
        $data['loggedEmployee'] = $this->common_model->select_fields_where($EmployeeTable, $EmployeeDetails, $where, TRUE);

        $this->load->view('hr_master_list/appointment-letter', $data);
    }

    public function update_appointment_letter()
    {
        if ($this->input->post()) {
            $table = 'ml_letter_text';
            $UpdatedLetterText = $this->input->post('tinyData');
            $where = array(
                'LetterTextID' => '1',
                'LetterText'=>$UpdatedLetterText
            );

            $result=$this->common_model->select_fields_where($table,'COUNT(1) AS Total',$where,TRUE);

            if($result->Total > 0){
                echo "OK::Same Format Already Exist, Format Not Updated::success";
            }else{
                $where = array(
                    'LetterTextID' => '1'
                );
                $data = array(
                    'LetterText' => addslashes($UpdatedLetterText)
                );
                $result = $this->common_model->update($table, $where, $data);
                if ($result === true) {
                    echo "OK::Record Successfully Updated::success";
                }else{
                    echo "FAIL::Some Error, Record Could Not Be Updated-::error";
                }
            }
        }
    }

    public function update_contractLetter()
    {
        if ($this->input->post()) {
            $table = 'ml_letter_text';
            $UpdatedLetterText = $this->input->post('tinyData');
            $where = array(
                'LetterTextID' => '2'
            );
            $data = array(
                'LetterText' => $UpdatedLetterText
            );
            $result = $this->common_model->update($table, $where, $data);
            if ($result === true) {
                echo "OK::Record Successfully Updated::success";
            }
        }
    }

    public function contract_letter($employeeID)
    {
        $PTable = 'employee E';
        $employeeData = ('
        E.employee_id AS EmployeeID,
        E.full_name AS FullName,
        CC.address AS Address,
        ET.joining_date AS StartDate,
        MLDg.designation_name AS Position,
        S.base_salary AS BaseSalary
        ');
        $joins = array(
            array(
                'table' => 'current_contacts CC',
                'condition' => 'E.employee_id=CC.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employment ET',
                'condition' => 'E.employee_id=ET.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'ET.employment_id = PM.employement_id AND PM.current=1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'salary S',
                'condition' => 'ET.employment_id=S.employement_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_designations MLDg',
                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = array(
            'E.trashed' => '0',
            'E.employee_id' => $employeeID
        );
        $data['employeeData'] = $this->common_model->select_fields_where_like_join($PTable, $employeeData, $joins, $where, TRUE);
/*        print_r($data);
        echo $this->db->last_query();*/
        //Now Need to Get Company Information
        $tbl = 'organization O';
        $org_data = ('*');
        $data['organizationData'] = $this->common_model->select_fields($tbl, $org_data, TRUE);
        //Now need to get the General Updated Format for User..
        $table = 'ml_letter_text';
        $letterData = ('LetterText');
        $condition = array(
            'LetterTextID' => '2'
        );
        $data['letterData'] = $this->common_model->select_fields_where($table, $letterData, $condition, TRUE);
        //Get Current Logged In User Information..
        $loggedInEmployee = $this->session->userdata('employee_id');
        $EmployeeTable = 'employee';
        $EmployeeDetails = ('full_name AS FullName');
        $where = array(
            'employee_id' => $loggedInEmployee
        );
        $data['loggedEmployee'] = $this->common_model->select_fields_where($EmployeeTable, $EmployeeDetails, $where, TRUE);

        $this->load->view('hr_master_list/contract-letter', $data);
    }

    //End Of Letter Functions

    //Function for the View of the Insurance Type.
    public function insurance(){
        $table = 'ml_insurance_type MIT';
        $data = ('MIT.insurance_type_id AS InsuranceTypeID, MIT.insurance_type_name AS InsuranceTypeName, MIT.enabled AS Status');
        $where = array(
            'MIT.trashed' => 0
        );
        $viewData['InsuranceTypes'] = $this->common_model->select_fields_where($table,$data,$where);
        $this->load->view('includes/header');
        $this->load->view('hr_master_list/insurance',$viewData);
        $this->load->view('includes/footer');
    }
//    Function for Edit Button inside the the Insurance Types Table.
    public function edit_insurance_type(){
        if($this->input->post('insuranceID')){
            $insuranceID = $this->input->post('insuranceID');
            $table = 'ml_insurance_type MIT';
            $data = 'MIT.insurance_type_id AS InsuranceTypeID, MIT.insurance_type_name AS InsuranceTypeName';
            $where = array(
                'MIT.insurance_type_id' => $insuranceID
            );
            $viewData['selectedInsurance'] = $this->common_model->select_fields_where($table,$data,$where,TRUE);
            $this->load->view('includes/header');
            $this->load->view('hr_master_list/edit_insurance',$viewData);
            $this->load->view('includes/footer');
        }else{
            var_dump($this->input->post());
            echo "nothing is posted";
        }
    }
    public function update_insurance_type(){
        if($this->input->is_ajax_request()){
            if($this->input->post()){
                $insuranceType = $this->input->post('insuranceType');
                $insuranceID = $this->input->post('insuranceID');
                $enabled = $this->input->post('enabled');
                $table = 'ml_insurance_type';
                $data = array(
                    'insurance_type_name' => $insuranceType,
                    'enabled' => $enabled
                );
                $where = array(
                    'insurance_type_id' => $insuranceID
                );
                $result = $this->common_model->update($table,$where,$data);
                if($result == true){
                    echo "OK::Record Successfully Updated::success";
                    return;
                }else{
                    echo "FAIL::Some Database Error, Record Could Not Be Updated::error";
                    return;
                }
            }
        }
    }

    public function trash_insurance_type($insuranceID = NULL)
    {

        //Function to Trash the Insurance Type.
        if ($this->input->is_ajax_request()) {
            $insuranceID = $this->input->post('insuranceID');
            if ($this->input->post()) {

                //First Need To Check If Record Already Been Used Or Not.
                $selectTable = 'employee_insurance';
                $selectData = array('COUNT(1) AS TotalRecordsFound',false);
                $where = array(
                    'trashed !=' => 1,
                    'ml_insurance_id' => $insuranceID
                );
                $countResult = $this->common_model->select_fields_where($selectTable,$selectData,$where,TRUE);

                if(isset($countResult) && $countResult->TotalRecordsFound > 0){
/*                    $msg = "Record Have Already Been Used, So Cant Be Deleted::error";
                    $this->session->set_flashdata('msg',$msg);
                    redirect('hr_site/insurance');*/
                    echo "FAIL::Record Have Already Been Used, So Cant Be Deleted::error";
                    return;
                }

                $table = "ml_insurance_type";
                $data = array(
                    'trashed' => 1
                );
                $where = array(
                    'insurance_type_id' => $insuranceID
                );
                $result = $this->common_model->update($table,$where,$data);
                if($result == true){
                    echo "OK::Record Successfully Trashed::success";
                }else{
                    echo "FAIL::Some Database Error, Record Could Not Be Trashed::error";
                }
            } else {
                echo "FAIL::No Data Posted::error";
                return;
            }
        }
    }
    public function addInsuranceType(){
        if($this->input->is_ajax_request())
        {
            $InsuranceType = $this->input->post('insuranceType');
            $tbl = 'ml_insurance_type';
            $data = 'COUNT(1) AS Total';
            $where = array(
                'insurance_type_name' => $InsuranceType,
                'trashed' => 0
            );
            $result = $this->common_model->select_fields_where($tbl,$data,$where,TRUE);
            if($result->Total>0){
                echo "FAIL::Record Already Exist for This Insurance Name::error";
                return;
            }else{
                echo "OK::Record Can Be Added::success";
                return;
            }
        }
        if($this->input->post()){
            $InsuranceType = $this->input->post('insuranceType');
            $table = 'ml_insurance_type';
            $data = array(
                'insurance_type_name' => $InsuranceType,
                'trashed' => 0,
                'enabled' => 1
            );
            $this->common_model->insert_record($table,$data);

                redirect('hr_site/insurance', 'location');
        }
    }
    public function fourzerofour()
    {
        $this->load->view('404.php');
    }
}

?>