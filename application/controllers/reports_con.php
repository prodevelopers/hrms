<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reports_Con extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('site_model');
        $this->load->library('datatables');
        $this->load->helper('hr_helper');

        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('login');
        }

        //Restrict UnAuthorized Users..
        $loggedInEmployeeIDAccessCheck = $this->session->userdata('employee_id');
        $moduleController = $this->router->fetch_class();
        if (!is_module_Allowed($loggedInEmployeeIDAccessCheck, $moduleController, 'view') && !is_admin($loggedInEmployeeIDAccessCheck)) {
            $msg = "You Do Not Have Access To This Certain Section::error";
            $this->session->set_flashdata('msg', $msg);
            redirect(previousURL());
            exit;
        }
    }

    public function index()
    {
        $this->load->helper('url');
    }

    /// Fucntion to View Absenties Reports
    public function view_absent_reports($employee_id = NULL)
    {
        $data['Absent'] = $this->site_model->absenties();
        //echo"<pre>";print_r($data['Absent']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports', $data);
        $this->load->view('includes/footer');
    }

    /// Function to View Leave Reports
    public function view_leave_reports($employee_id = NULL)
    {
        $data['leave'] = $this->site_model->leave_report();
        //echo"<pre>";print_r($data['leave']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_leave', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View Project Work Days Reports
    public function view_proj_mandays_reports()
    {
        /**
         * Need To Calculate The Employee Work Days
         * e-g if there are three employees worked 6 days on a project then that project total Man Days Would Be,
         * 3(employees) * 6(days) = 18(ManDays)
         * So We Need  ManDays, Number of Employees in to Days Worked, Kind oF Tricky, But i am Sure, With With Allah Almighty Grace i will do it on tomorrow's Date..
         */

        //We First Require The Total Number of Employees Working On Projects
        $employeeWithProjects = 'employee_project EP';
        $joins = array(
            array(
                'table' => 'employee E',
                'condition' => 'E.employee_id = EP.employee_id AND E.trashed = 0',
                'type' => 'INNER'
            )
        );
        $data = ('*');
        $where = array(
            'EP.trashed' => 0
        );
        $totalEmployeesWithProjects = $this->common_model->select_fields_where_like_join($employeeWithProjects, $data, $joins, $where, FALSE);


        /*        echo "<pre>";
                print_r($totalEmployeesWithProjects);
                echo "</pre>";*/

        $this->load->view('includes/header');
        $this->load->view('Reports/reports_workdays');
        $this->load->view('includes/footer');
    }

    /// Function to View ManualTime Sheet Reports
    public function view_timesheet_reports($employee_id = NULL)
    {
        $monthYear = date("Y-m-d");
        $monthYearData = explode("-", $monthYear);
        $currentMonth = $monthYearData[1];
        $currentYear = $monthYearData[0];
        $loggedInEmployee = $this->session->userdata('employee_id');
        $monthYear = $this->input->post('selectedMonthYear');
        if (isset($monthYear) && !empty($monthYear)) {
            $monthYearData = explode(",", $monthYear);
            $month = $monthYearData[0];
            $monthNumeric = date("m", strtotime($month));
            $year = $monthYearData[1];
            $viewData['filterInfo'] = $monthYearData;
            if (is_admin($loggedInEmployee)) {
                if (isset($monthYearData[2]) && !empty($monthYearData[2])) {
                    $employeeID = $monthYearData[2];
                }
            }
            if (isset($employeeID) && !empty($employeeID)) {
                $where = array(
                    /*                    "DATE_FORMAT(TS.date_created,'%M')" => $monthYearData[0],
                                        "DATE_FORMAT(TS.date_created,'%Y')" => $monthYearData[1],*/
                    'E.employee_id' => $employeeID
                );
            } else {
                $where = array(
                    /*                    "DATE_FORMAT(TS.date_created,'%M')" => $monthYearData[0],
                                        "DATE_FORMAT(TS.date_created,'%Y')" => $monthYearData[1],*/
                    'E.employee_id' => $loggedInEmployee
                );
            }

            $viewData['monthYear'] = $monthYearData;
        } else {
            $currentYearMonth = date('Y-F');
            $currentMonthYearData = explode("-", $currentYearMonth);
            $month = $currentMonthYearData[1];
            $monthNumeric = date("m", strtotime($month));
//            var_dump($monthNumeric);
            $year = $currentMonthYearData[0];
            if (isset($employeeID) && !empty($employeeID)) {
                $where = array(
                    'E.employee_id' => $employeeID
                );
            } else {
                $where = array(
                    'E.employee_id' => $loggedInEmployee
                );
            }

        }
        $PTable = 'employee E';
        $data = ('
   TSD.timesheetDetails_id,
   EP.project_id AS ProjectID,
   TSD.date AS WorkDate,
   TSD.hours AS WorkHours,
   TS.date_created AS TimeSheetMonth,
   E.employee_id AS EmployeeID,
   TS.id AS TimeSheetID,
   MLP.project_title AS ProjectTitle,
   E.full_name AS EmployeeName
');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'timesheet TS',
                'condition' => 'TS.employment_id = ET.employment_id AND DATE_FORMAT(TS.date_created,"%Y") = ' . $year,
                'type' => 'INNER'
            ),
            array(
                'table' => 'timesheet_details TSD',
                'condition' => 'TS.id = TSD.timesheet_id AND DATE_FORMAT(TSD.date,"%M") = "' . $month . '" AND DATE_FORMAT(TSD.date,"%Y") = ' . $year,
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'TSD.project_id = MLP.project_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'employee_project EP',
                'condition' => 'MLP.project_id = EP.project_id AND ET.employment_id = EP.employment_id',
                'type' => 'INNER'
            )
        );
        $group_by = '';
        $viewData['TimeSheetViewData'] = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, FALSE, '', '', $group_by);
//var_dump($viewData['TimeSheetViewData']);
        //Employee Data
        $table = 'employee';
        $data = ('full_name AS EmployeeName,employee_id AS EmployeeID');
        if (isset($employeeID) && !empty($employeeID)) {
            $where = array(
                'employee_id' => $employeeID
            );
        } else {
            $where = array(
                'employee_id' => $loggedInEmployee
            );
        }

        $viewData['employeeData'] = $this->common_model->select_fields_where($table, $data, $where, TRUE);

        //Need to Get Leave Details Now.
        if (isset($employeeID) && !empty($employeeID)) {
            $employmentID = get_employment_from_employeeID($employeeID);
            $viewData['employeeTotalLeaves'] = $this->site_model->getTotalEmployeeLeaves($monthNumeric, $year, $employeeID,$employmentID);
            //Bottom
            $viewData['employeeTotalMonthlyAndYearly'] = $this->site_model->getTotalEmployeeLeavesNew($monthNumeric, $year, $employeeID,$employmentID);
        } else {
            $loggedInEmploymentID = get_employment_from_employeeID($loggedInEmployee);
            $viewData['employeeTotalLeaves'] = $this->site_model->getTotalEmployeeLeaves($monthNumeric, $year, $loggedInEmployee,$loggedInEmploymentID);
            //Bottom
            $viewData['employeeTotalMonthlyAndYearly'] = $this->site_model->getTotalEmployeeLeavesNew($monthNumeric, $year, $loggedInEmployee,$loggedInEmploymentID);
        }

        $table = 'ml_leave_type';
        $data = array('ml_leave_type_id AS LeaveTypeID, leave_type AS LeaveType, hex_color AS LeaveTypeColor', false);
        $where = array(
            'trashed' => 0
        );
        $result = $this->common_model->select_fields_where($table,$data,$where);
        if(isset($result) && !empty($result)){
            $viewData['LeaveTypes'] = $result;
        }//End Of Color Getting for Leaves

        /*        var_dump($viewData['employeeTotalLeaves']);
                        exit;*/
        $viewData['viewPdf']=$this->load->view('Reports/reports_timesheet_pdf',$viewData,true);
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_timesheet', $viewData);
        $this->load->view('includes/footer');
    }
//Function for Send pdf file of Time Sheet in email
    function SendPDFEmployeeTimeSheetReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'ManualTimeSheetReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    // END
    // Function for Pdf generation
    public function downloadPdfTimeSheetReport()
    {
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/ManualTimeSheetReportPdf/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'ManualTimeSheetReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }

    // End
    /// Fucntion to View Pay Roll Expenses Reports
    public function view_pay_expense_reports($employee_id = NULL)
    {
        $data['info'] = $this->payroll_model->expense_report_model();
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_expense', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View Pay Roll Salary Reports
    public function view_pay_salary_reports($employee_id = NULL)
    {
        $data['info'] = $this->payroll_model->salary_report();
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_salary', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View Pay Roll Transactions Reports
    public function view_pay_transaction_reports($employee_id = NULL)
    {
        $data['info'] = $this->payroll_model->transactions_reports();
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_transaction', $data);
        $this->load->view('includes/footer');
    }
    /// Fucntion to View Joiners And Leave Reports
    /// Fucntion to View Joiners And Leave Reports
    public function view_joiner_leave_reports()
    {
        $data['name'] = $this->site_model->joiner_leaver_report();
        //echo"<pre>";print_r($data['name']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_joiners', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View HR District Wise Reports
    public function view_hr_district_reports()
    {

        $data['name'] = $this->site_model->district_view();
        //echo"<pre>";print_r($data['name']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_district', $data);
        $this->load->view('includes/footer');

    }

    /// Fucntion to View HR Skills Wise Reports
    public function view_hr_skill_reports()
    {
        $data['name'] = $this->site_model->skill_report_view();
        //echo"<pre>";print_r($data['name']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_skills', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View HR Qualification Wise Reports
    public function view_hr_qualification_reports()
    {
        $data['name'] = $this->site_model->qualification_report_view();
        //echo"<pre>";print_r($data['name']);die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_qualification', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View HR Experience Wise Reports
    public function view_hr_experience_reports()
    {
        $data['name'] = $this->site_model->experience_report_view();
        //echo"<pre>";print_r($data['name']); die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_experiance', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View HR Training Wise Reports
    public function view_hr_training_reports()
    {
        $data['name'] = $this->site_model->training_report_view();
        //echo"<pre>";print_r($data['name']); die;
        $this->load->view('includes/header');
        $this->load->view('Reports/reports_training', $data);
        $this->load->view('includes/footer');
    }

    /// Fucntion to View Employee Progress Wise Reports
    public function view_progress_reports()
    {
        if ($this->input->post('continue')) {
            $insert = array(
                'reviewer' => $this->input->post('reviewer'),
                'comments' => $this->input->post('comments')
            );
            $this->site_model->create_new_record('feedback', $insert);

            //echo"<pre>"; print_r($insert); die;
        }
        $data['name'] = $this->site_model->employee_progress();
        //echo"<pre>";print_r($data['name']); die;
        $this->load->view('includes/header');
        $this->load->view('Reports/progress_reports', $data);
        $this->load->view('includes/footer');
    }

    ////////////////////////////////////// Function for Staff View //////////////////////////////////////

    public function staff()
    {
        $data = $this->site_model->staff_report_count();
        $count = count($data);
        //echo "<pre>";print_r($count);die;
        $config['base_url'] = base_url() . 'reports_con/staff';
        $config['total_rows'] = $count;
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['next_link'] = '&nbsp;Next&nbsp;>';
        $config['prev_link'] = '<&nbsp;Prev&nbsp;';
        //$config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = "<div id ='pagination'>";
        $config['full_tag_close'] = "</div>";
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $limit = $config['per_page'];


        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data['links'] = $this->pagination->create_links();

        $data['employee_id'] = $this->uri->segment(3);

        $data['staff_report'] = $this->site_model->staff_report($limit, $page);
        $this->load->view('includes/header');
        $this->load->view('Reports/staff', $data);
        $this->load->view('includes/footer');
    }

    public function generate_leave_attendance_report($print = FALSE)
    {
        $data['leave'] = $this->site_model->leave_report();
        $selectedCheckBoxes = $this->input->post('selectData');
        if (isset($selectedCheckBoxes) && !empty($selectedCheckBoxes)) {
            //If CheckBoxes Are Selected Then This Section Should Execute..
            $projectID = $this->input->post('projID');
            $departmentID = $this->input->post('departmentID');
            $leaveTypeID = $this->input->post('leaveTypeID');
            $postedYear = $this->input->post('year');
            if (isset($postedYear) and !empty($postedYear)) {
                $yearFilter = intval($postedYear);
            } else {
                $yearFilter = date("Y");
            }

            //Lets Get the Data On basis Of the Search Criteria
            $PTable = 'employee E';
            $selectData = array('GROUP_CONCAT(E.employee_id) AS EmployeeIDs, COUNT(E.employee_id) AS TotalEmployees', false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                    'type' => 'INNER'
                )
            );
            $where = array(
                'E.trashed' => 0,
                'E.enrolled' => 1
            );
            if ((isset($projectID) && $projectID > 0) || (isset($departmentID) && $departmentID > 0)) {
                //If Project Or Department is Selected, Then This Section Should Execute..
                if (isset($projectID) && $projectID > 0) {
                    $joinsForProject = array(
                        array(
                            'table' => 'employee_project EP',
                            'condition' => 'EP.employee_id = E.employee_id AND EP.trashed = 0 AND EP.current = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_projects MLP',
                            'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $joins = array_merge($joins, $joinsForProject);
                    $where['EP.project_id'] = $projectID;
                }
            } else {
                //If there is AnyThing To Work On IF no Filter Has Been Selected from either of Project or department.
            }

            //Need Leaves For Both Leaves And Also Need In Absentees As We Need To Subtract Leaves...
            $array = array(
                array(
                    'table' => 'leave_application LAPP',
                    'condition' => 'LAPP.employee_id = E.employee_id',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'leave_approval LA',
                    'condition' => 'LA.leave_application_id = LAPP.application_id AND LA.trashed = 0 AND LA.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_leave_type MLLT',
                    'condition' => 'MLLT.ml_leave_type_id = LAPP.ml_leave_type_id',
                    'type' => 'INNER'
                )
            );
            $joins = array_merge($joins, $array);
            $selectDataLeaves = array(
                'approved_from AS StartDate,
                approved_to AS EndDate,
                E.employee_id AS EmployeeID',
                false
            );
            $where['YEAR(approved_from)'] = $yearFilter;
            //Little Check For Leave Filter.
            if (isset($leaveTypeID) && !empty($leaveTypeID)) {
                $where['LAPP.ml_leave_type_id'] = $leaveTypeID;
            }
            /**
             * This Has Little Problem If Leave Type is Selected Then It will Gonna Effect Both Leaves And Absenties,
             * But In View I have Allowed DropDown To Be Chosen Only For Leaves.
             * May Be Will Fix it Later When It Becomes A Critical or Major Issue..
             */
            $leaves = $this->common_model->select_fields_where_like_join($PTable, $selectDataLeaves, $joins, $where, FALSE);


            //Get Available Months in The System.
            $monthTable = 'ml_month MLM';
            $data = 'MLM.month AS MONTH';
            $where = array(
                'trashed' => 0
            );
            $months = $this->common_model->select_fields_where($monthTable, $data, $where);
            $totalLeavesTakenSum = 0;
            $leavesIf = FALSE;
            $absenteesIf = FALSE;

            if (is_array($selectedCheckBoxes) && in_array('Leaves', $selectedCheckBoxes)) {
                $leavesIf = TRUE;
            } elseif (is_string($selectedCheckBoxes) && strpos($selectedCheckBoxes, 'Leaves') !== false) {
                $leavesIf = TRUE;
            } else {
                $leavesIf = FALSE;
            }

            if (is_array($selectedCheckBoxes) && in_array('Absentees', $selectedCheckBoxes)) {
                $absenteesIf = TRUE;
            } elseif (is_string($selectedCheckBoxes) && strpos($selectedCheckBoxes, 'Absentees') !== false) {
                $absenteesIf = TRUE;
            } else {
                $absenteesIf = FALSE;
            }
            if ($leavesIf === TRUE) {
                if (!isset($leaves) || !is_array($leaves)) {
                    $approvedLeavesArray = array();
                    foreach ($months as $month) {
                        $array = array(
                            date('F', strtotime($month->MONTH)) => array(
                                'TotalLeaves' => 0
                            )
                        );
                        $approvedLeavesArray = array_merge($approvedLeavesArray, $array);
                    }
                } else {
                    $approvedLeavesArray = array();
                    foreach ($months as $month) {
                        foreach ($leaves as $approvedLeaves) {
                            $startDate = strtotime($approvedLeaves->StartDate);
                            $endDate = strtotime($approvedLeaves->EndDate);
                            $currentMonth = strtotime($yearFilter . '-' . $month->MONTH . '-1');
                            //First Check If Dates Matches With Range Dates
                            if (date('M', $startDate) === $month->MONTH || date('M', $endDate) === $month->MONTH) {
                                //Month Matched WIth Provided Dates
                                //Lets See if Starting or Ending Date Did Matched It..

                                if (date('M', $startDate) === $month->MONTH) {
                                    $startMatched = TRUE;
                                } else {
                                    $startMatched = FALSE;
                                }

                                if (date('M', $endDate) === $month->MONTH) {
                                    $endMatched = TRUE;
                                } else {
                                    $endMatched = FALSE;
                                }

                                if ($startMatched === TRUE and $endMatched === TRUE) {
                                    //If Both Start and End Dates Are From Same Month Then We Will Do The Difference.
                                    $dateDiff = $endDate - $startDate;
                                    $totalLeavesTaken = floor($dateDiff / (60 * 60 * 24));

                                } elseif ($startMatched === TRUE) {
                                    //If Start Date Matched Then Do Difference From Start Date Only
                                    //To Get Difference We First Should know The Total Days in Current Month of the Year.
                                    $numericMonthNumber = date("n", strtotime($month->MONTH));
                                    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $numericMonthNumber, $yearFilter);
                                    $startDateDay = explode('=', $startDate);
                                    $totalLeavesTaken = $daysInMonth - $startDateDay[2];
                                    $totalLeavesTaken = $totalLeavesTaken + 1;

                                } elseif ($endMatched === TRUE) {
                                    //If End Date Matched Then Do Difference From Start Date Only
                                    $endDateDay = explode('=', $endDate);
                                    $totalLeavesTaken = $endDateDay[2];
                                }
                            } elseif (($currentMonth >= $startDate) && ($currentMonth <= $endDate)) {

//                            echo 'Print Debugging'; print_r($approvedLeaves); echo 'End Debug';

                                //If Month Did Not Matched So We Needed To Check If Month Do Match in between the dates..
                                $numericMonthNumber = date("n", strtotime($month->MONTH));
                                $totalLeavesTaken = cal_days_in_month(CAL_GREGORIAN, $numericMonthNumber, $yearFilter);
                            }
                            if (isset($totalLeavesTaken) && $totalLeavesTaken > 0) {
                                $totalLeavesTakenSum += $totalLeavesTaken;
                                $totalLeavesTaken = 0;
                            }
                        }
                        if (isset($totalLeavesTakenSum) && !empty($totalLeavesTakenSum)) {
                            $array = array(
                                date('F', strtotime($month->MONTH)) => array(
                                    'TotalLeaves' => $totalLeavesTakenSum
                                )
                            );
                            $approvedLeavesArray = array_merge($approvedLeavesArray, $array);
                        } else {
                            if (!array_key_exists($month->MONTH, $approvedLeaves)) {
                                $array = array(
                                    date('F', strtotime($month->MONTH)) => array(
                                        'TotalLeaves' => 0
                                    )
                                );
                                $approvedLeavesArray = array_merge($approvedLeavesArray, $array);
                            }
                        }
                        $totalLeavesTakenSum = 0;
                    }
                } //End Of Main IF Else, To Check If $leaves is an Array. and There Are Any Leaves in The System.
            }

            if ($absenteesIf === TRUE) {
                $absenteesWhere = '';
                //Now Here We Need To Get All Those Employees Who Have Not Worked In Working Days But Also Have Not Submitted There Leaves.
                //We First Need To Get All The Weekend Days That We Need To Subtract From All Days.
                $table = 'work_week';
                $data = 'work_week_id AS WorkWeekID, work_week AS WorkWeek';
                $where = array(
                    'trashed' => 0,
                    'working_day' => 'Weekend'
                );
                $work_weeks = $this->common_model->select_fields_where($table, $data, $where);

                if (isset($work_weeks) && !empty($work_weeks)) {
                    $work_weeks = '"' . implode('","', array_column(json_decode(json_encode($work_weeks), true), 'WorkWeek')) . '"';
                    $absenteesWhere .= 'DATE_FORMAT(in_date,"%W") NOT IN (' . $work_weeks . ')';
                } else {
                    //No Need To Execute As Nothing is Set in Work Weeks Table
                    return;
                }

                //Now We Need All The Leaves In The System..
                if (isset($leaves) && !empty($leaves)) {
                    //If There Are Any Leaves In The System Then This Section Should Execute.
                    foreach ($leaves as $leave) {
                        if ($absenteesWhere === '') {
                            $absenteesWhere .= 'in_date NOT BETWEEN ' . $leave->StartDate . ' AND ' . $leave->EndDate;
                        } else {
                            $absenteesWhere .= ' AND in_date NOT BETWEEN ' . $leave->StartDate . ' AND ' . $leave->EndDate;
                        }
                    }
                }
                //Get All The Attendance Dates.
                $table = 'attendence ATT';
                $data = array('COUNT(1) AS TotalAbsentees,  DATE_FORMAT(in_date, "%M") AS AbsenteesMonth', false);
//                $where = 'DATE_FORMAT(in_date,"%W") NOT IN ('.$work_weeks.')';

                /**
                 * This Working Days Variable Will Gonna Have The Result Of the Query,
                 * in Which All Days Will Be Present.
                 * Except Weekends
                 * Except The Approved Leaves.
                 * Will Be For Particular Year
                 */

                if ($absenteesWhere === '') {
                    $absenteesWhere .= 'YEAR(in_date) = ' . $yearFilter;
                } else {
                    $absenteesWhere .= ' AND YEAR(in_date) = ' . $yearFilter;
                }
                if (isset($projectID) && $projectID > 0) {
                    $joinsForAbsentees = array(
                        array(
                            'table' => 'employee E',
                            'condition' => 'E.employee_id = ATT.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employee_project EP',
                            'condition' => 'EP.employee_id = E.employee_id AND EP.trashed = 0 AND EP.current = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_projects MLP',
                            'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $absenteesWhere .= ' AND EP.project_id = ' . $projectID;
                    $workingDays = $this->common_model->select_fields_where_like_join($table, $data, $joinsForAbsentees, $absenteesWhere);
                } elseif (isset($departmentID) && $departmentID > 0) {
                    $departmentJoinsForAbsentees = array(
                        array(
                            'table' => 'employee E',
                            'condition' => 'E.employee_id = ATT.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'employment ET',
                            'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current  = 1',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'posting P',
                            'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.current = 1 AND P.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_department MLD',
                            'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $absenteesWhere .= ' AND P.ml_department_id = ' . $departmentID;
                    $workingDays = $this->common_model->select_fields_where_like_join($table, $data, $departmentJoinsForAbsentees, $absenteesWhere);

                } else {
                    $workingDays = $this->common_model->select_fields_where($table, $data, $absenteesWhere);
                }

                $workingDays = json_decode(json_encode($workingDays), true);
//                print_r($workingDays);

                //Now As We Got All The Data, We Need TO make It Presentable to the View..


                $absentees = array();
                foreach ($months as $month) {
                    foreach ($workingDays as $key => $workingDaysMonth) {
                        if (date('F', strtotime($month->MONTH)) === $workingDaysMonth['AbsenteesMonth']) {
                            $array = array(
                                date('F', strtotime($month->MONTH)) => $workingDaysMonth
                            );
                            $absentees = array_merge($absentees, $array);
                        } else {
                            //Store The Other Months That Do Not Exist In Query Result.
                            if (!array_key_exists(date('F', strtotime($month->MONTH)), $absentees)) {
                                $array = array(
                                    date('F', strtotime($month->MONTH)) => array(
                                        'TotalAbsentees' => 0,
                                        'AbsenteesMonth' => '-'
                                    )
                                );
                                $absentees = array_merge($absentees, $array);
                            }
                        }
                    }
                }
            }

            //Need To Merge The Leaves And Absentees..
            //Finally The Absentees and Leaves Together
            if (isset($absentees) && isset($approvedLeavesArray)) {
                $bigResultArray = array();
                foreach ($months as $key => $month) {
                    $bigResultArray[date('F', strtotime($month->MONTH))] = array_merge($absentees[date('F', strtotime($month->MONTH))], $approvedLeavesArray[date('F', strtotime($month->MONTH))]);
                }
            } elseif (isset($absentees)) {
                $bigResultArray = $absentees;
            } elseif (isset($approvedLeavesArray)) {
                $bigResultArray = $approvedLeavesArray;
            }

            //If Call is From Print Page.
            if ($print === 'print') {
                if (isset($bigResultArray) && !empty($bigResultArray)) {
                    $data['bigResultArray'] = $bigResultArray;
                }
                $data['view'] = $this->load->view('Reports/customizedReportPrint', $data, true);
                $this->load->view('Reports/gen_print', $data);
                return;
            }
            //For Ajax Call
            if (isset($bigResultArray)) {
                print_r(json_encode($bigResultArray));
            }
            return;
        }


        //Lets Load The
        $this->load->view('includes/header');
        $this->load->view('Reports/generate_leave_attendance_report', $data);
        $this->load->view('includes/footer');
    }

    public function generate_employees_leave_attendance_report($page = NULL)
    {
        if ($page === 'listEmployeesLeaveAndAbsentees') {
            if ($this->input->is_ajax_request()) {
                if ($this->input->post()) {
                    $selectedCheckBoxes = $this->input->post('reportTypes');
                    if (isset($selectedCheckBoxes) && !empty($selectedCheckBoxes)) {
                        $leaves = false;
                        $absentees = false;
                        //Now True Only Ones Which Have Been Checked.
                        if (strpos($selectedCheckBoxes, 'Leaves') !== false) {
                            $leaves = true;
                        }
                        if (strpos($selectedCheckBoxes, 'Absentees') !== false) {
                            $absentees = true;
                        }
                    } else {
                        //If CheckBoxes Are Not Posted Then We Will Show All The Data..
                        $leaves = true;
                        $absentees = true;
                    }

                    $selectedYear = $this->input->post('year');
                    $selectedLeaveTypeID = $this->input->post('leaveType');
                    $selectedDepartmentID = $this->input->post('department');
                    $selectedProjectID = $this->input->post('project');
                    $selectedProgramID = $this->input->post('ProgramName');

                    if ($leaves === true) {
                        $leavesSelectData = 'empLeaves.LeavesFromDate, empLeaves.LeavesToDate,IFNULL(TotalApprovedLeaves,0) AS TotalApprovedLeaves,IFNULL(TotalLeavesTaken,0) AS TotalLeavesTaken';
                    } elseif ($leaves === false) {
                        $leavesSelectData = '';
                    }

                    if ($absentees === false) {
                        $absenteesSelectData = '';
                    } elseif ($absentees === true) {
                        $absenteesSelectData = 'IFNULL(TotalAbsentees,0) AS TotalAbsenteesTaken';
                    }
                    $PTable = 'employee E';

                    /* $selectData = array('E.employee_id AS EmployeeID,
                    E.employee_code AS EmployeeCode,
                    E.full_name AS EmployeeName,
                    empLeaves.LeavesFromDate,
                    empLeaves.LeavesToDate,
                    IFNULL(empAbsentees.TotalAbsentees,0) AS TotalAbsenteesTaken,
                    IFNULL(TotalApprovedLeaves,0) AS TotalApprovedLeaves,
                    ATT.in_date AS InDate,
                    ATT.out_date AS OutDate,
                    IFNULL(TotalLeavesTaken,0) AS TotalLeavesTaken', false); */

                    $selectData = array('E.employee_id AS EmployeeID, E.employee_code AS EmployeeCode, E.full_name AS EmployeeName, ATT.in_date AS InDate, ATT.out_date AS OutDate,' . $leavesSelectData . ',' . $absenteesSelectData, false);
                    $joins = array(
                        array(
                            'table' => 'attendence ATT',
                            'condition' => 'ATT.employee_id = E.employee_id',
                            'type' => 'LEFT'
                        ),

                        'leavesArray' => array( //Left Join For Leaves.
                            'table' => '(
SELECT
LA.leave_approval_id AS ApprovedLeaveID,
E.employee_id AS EmployeeID,
LA.approved_from AS LeavesFromDate,
LA.approved_to AS LeavesToDate,
SUM(DATEDIFF(LA.approved_to,approved_from)+1) AS TotalLeavesTaken,
COUNT(LA.leave_application_id) AS TotalApprovedLeaves
FROM leave_application APP
INNER JOIN leave_approval LA ON LA.leave_application_id = APP.application_id AND LA.trashed = 0 AND LA.status = 2
INNER JOIN employment ET ON ET.employment_id = APP.employment_id AND ET.current = 1
INNER JOIN employee E ON E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1
INNER JOIN ml_leave_type MLLT ON MLLT.ml_leave_type_id = APP.ml_leave_type_id AND MLLT.trashed = 0
WHERE 1 = 1 ' . ((isset($selectedLeaveTypeID) && !empty($selectedLeaveTypeID)) ? ' AND APP.ml_leave_type_id = ' . intval($selectedLeaveTypeID) . " " : "") . ((isset($selectedYear) && !empty($selectedYear)) ? ' AND YEAR(LA.`approved_from`) = ' . $selectedYear : '') . '
GROUP BY E.employee_id
) empLeaves',
                            'condition' => 'E.employee_id = empLeaves.EmployeeID',
                            'type' => 'LEFT'
                        ),

                        'absenteesArray' => array( //Left join For Absentees
                            'table' => '(
SELECT E.employee_id AS EmployeeID,
E.`full_name` AS EmployeeName,
empAbsentees.ApprovedFrom,
empAbsentees.ApprovedTo,
ATT.attendence_id AS AttendanceID,
COUNT(IFNULL(ATT.out_date,0)) AS TotalAbsentees,
GROUP_CONCAT(ATT.in_date) AS Dates
FROM employee E
INNER JOIN attendence ATT ON ATT.employee_id = E.employee_id
LEFT JOIN (
SELECT E.employee_id, LA.approved_from AS ApprovedFrom, LA.`approved_to` AS ApprovedTo
FROM employee E
INNER JOIN employment ET ON ET.employee_id = E.`employee_id`
INNER JOIN leave_application LAPP ON LAPP.employment_id = ET.`employment_id`
INNER JOIN leave_approval LA ON LA.`leave_application_id` = LAPP.`application_id` AND LA.`status` = 2
) empAbsentees ON E.employee_id = empAbsentees.employee_id
WHERE
(ATT.out_date IS NULL || ATT.out_date = "0000-00-00")' . ((isset($selectedYear) && !empty($selectedYear)) ? ' AND DATE_FORMAT(ATT.in_date,"%Y") = "' . $selectedYear . '"' : "") . '
AND CASE WHEN (empAbsentees.ApprovedFrom IS NOT NULL AND empAbsentees.ApprovedTo IS NOT NULL) THEN (empAbsentees.ApprovedFrom >= ATT.in_date || empAbsentees.ApprovedTo <= ATT.in_date) ELSE 1 = 1 END
GROUP BY E.employee_id
) empAbsentees',
                            'condition' => 'empAbsentees.EmployeeID = E.employee_id',
                            'type' => 'LEFT'
                        )
                    );


                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0
                    );

                    //If Project Or Department Filter Has Been Selected Then We Need To Do The Following.
                    if (isset($selectedDepartmentID) && is_numeric($selectedDepartmentID) && !empty($selectedDepartmentID)) {
                        $arrayToMerge = array(
                            array(
                                'table' => 'employment ET',
                                'condition' => 'ET.employee_id = E.employee_id AND ET.trashed = 0 AND ET.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'posting P',
                                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.status = 2',
                                'type' => 'INNER'
                            )
                        );
                        $joins = array_merge($joins, $arrayToMerge);
                        $where['P.ml_department_id'] = $selectedDepartmentID;
                    } elseif (isset($selectedProjectID) && is_numeric($selectedProjectID) && !empty($selectedProjectID)) {
                        $arrayToMerge = array(
                            array(
                                'table' => 'employment ET',
                                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'employee_project EP',
                                'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0',
                                'type' => 'INNER'
                            )
                        );
                        $joins = array_merge($joins, $arrayToMerge);
                        $where['EP.project_id'] = $selectedProjectID;
                    }

                        if(!isset($selectedProjectID) || empty($selectedProjectID)){
                            $arrayToMerge = array(
                                array(
                                    'table' => 'employment ET',
                                    'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1',
                                    'type' => 'INNER'
                                ),
                                array(
                                    'table' => 'employee_project EP',
                                    'condition' => 'EP.employment_id = ET.employment_id AND EP.trashed = 0',
                                    'type' => 'INNER'
                                )
                            );
                            $joins = array_merge($joins, $arrayToMerge);
                        }

                    if(isset($selectedProgramID) && is_numeric($selectedProgramID) && !empty($selectedProgramID)){
                        $arrayToMerge = array(
                            array(
                                'table' => 'ml_projects MLP',
                                'condition' => 'EP.project_id = MLP.project_id',
                                'type' => 'INNER'
                            )
                        );
                        $joins = array_merge($joins, $arrayToMerge);
                        $where['MLP.ProgramID'] = $selectedProgramID;
                    }

                    if ($leaves === false) {
                        unset($joins['leavesArray']);
                    } elseif ($absentees === false) {
                        unset($joins['absenteesArray']);
                    }

                    $group_by = 'E.employee_id';

                    $result = $this->common_model->select_fields_joined_DT($selectData, $PTable, $joins, $where, '', '', $group_by);
                    print_r($result);
                    return;
                } //End Of If Statement
            }
        }
        //Loading The View..
        $this->load->view('includes/header');
        $this->load->view('Reports/generate_leave_attendance_report_new');
        $this->load->view('includes/footer');
    }

    function load_all_available_projects()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_projects';
                $data = array(
                    'project_title AS ProjectName, project_id AS ProjectID',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'project_title';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }

    function load_all_available_Program()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_program_list';
                $data = array(
                    'Program AS ProgramName, ProgramID AS PROGRAMID',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'Program';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }

    function load_all_available_departments()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_department';
                $data = array(
                    'department_id AS DepartmentID, department_name AS Department',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'department_name';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }

    function load_all_available_leave_types()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_leave_type';
                $data = array(
                    'ml_leave_type_id AS LeaveTypeID, leave_type AS LeaveType',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'leave_type';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where);
                }
                print_r(json_encode($result));
            }
        }
    }

    function load_all_available_years()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_year';
                $data = array(
                    'ml_year_id AS YearID, ml_year.year AS Year',
                    false
                );
                $where = array(
                    'trashed' => 0
                );
                $order_by = array('ml_year.year', 'desc');
                if (isset($sValue) && !empty($sValue)) {
                    $field = 'year';
                    $result = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, '', $order_by);
                } else {
                    $result = $this->common_model->select_fields_where($table, $data, $where, FALSE, '', '', '', '', $order_by);
                }
                print_r(json_encode($result));
            }
        }
    }

    public function print_custom()
    {
        $this->load->view('Reports/gen_print');
    }

    function sendGeneratedPDFReport()
    {
        if ($this->input->is_ajax_request()) {
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if ($this->input->post()) {
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if (!isset($mailTo) || empty($mailTo)) {
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if (!isset($mailFrom) || empty($mailFrom)) {
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);

                    if (!isset($employeeInfo) || empty($employeeInfo)) {
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if (!isset($messageBody) || empty($messageBody)) {
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');


                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/generatedPDFReports/' . $this->data['EmployeeID'] . '/';
                $uploadDirectory = 'systemGeneratedFiles/generatedPDFReports/' . $this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if (!is_dir($uploadDirectory)) {
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_PDF_' . time() . '.pdf';
                $filePath = $uploadPath . $fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);

                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = $this->data['mailConfiguration'];
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..
                $this->email->to($mailTo);

                if (is_admin($this->data['EmployeeID'])) {
                    $this->email->from($mailFrom, 'Phrism Administrator');
                } else {
                    $this->email->from($mailFrom, 'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if ($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
            }
        }
    }

    /**
     * Customized Reports Duplicates..
     */
    //Employee Customize Report..
    public function generate_report()
    {
        $this->load->view('includes/header');
        $this->load->view('Reports/generate_HR_report');
        $this->load->view('includes/footer');
    }

    public function generate_report_print()
    {
        /**
         * Some Select Fields For Quick Info.
         * E employee
         * ET employment
         * EP employee_project
         * PC permanant_contacts
         * MLD ml_districts
         * PS posting
         * MLB ml_branch
         * MLC ml_city
         * MLDg ml_designations
         * PM position_management
         * MLPG ml_pay_grade
         * CC current_contacts
         * CN Contract
         * QF Qualification
         * ED Employee Dependents
         * MLDP ml_department
         * EI employee_insurance
         * MLIT ml_insurance_type
         */
        $selectsData = ('E.employee_code AS Employee_Code,E.full_name AS Name,E.father_name AS Father_Name');
        $PTable = 'employee E';
        $joins = array(
            array(
                'table' => 'employee_project EP',
                'condition' => 'EP.employee_id = E.employee_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'permanant_contacts PC',
                'condition' => 'PC.employee_id = E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_district MLD',
                'condition' => 'MLD.district_id = PC.district',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_projects MLP',
                'condition' => 'MLP.project_id = EP.project_id',
                'type' => 'LEFT'
            ),
            array(
                'table' => 'ml_program_list MLProg',
                'condition' => 'MLProg.ProgramID = MLP.ProgramID AND MLProg.trashed = 0',
                'type' => 'LEFT'
            )
        );
        $where = 'E.trashed = 0 AND E.enrolled = 1';
        $group_by = 'E.employee_id';

        //Now When Some Filters Are Posted..
        if ($this->input->post()) {
            $selects = $this->input->post('selects');
            $employees = $this->input->post('employees');
            $Program = $this->input->post('Program');
            $projects = $this->input->post('projects');
            $domiciles = $this->input->post('domiciles');
            $reportHeading = $this->input->post('reportHeading');
            if (isset($reportHeading) && !empty($reportHeading)) {
                $viewData['reportHeading'] = htmlentities($reportHeading);
            }

//            need to decode the Json Data to array
            if (isset($employees) && !empty($employees) && $employees !== '[]') {
                $employees = json_decode($employees, true);
                $employeeIDs = array();
                foreach ($employees as $key => $value) {
                    array_push($employeeIDs, $value['id']);
                }
                $employeeIDs = implode(",", $employeeIDs);
                //Where Query for the Employees If Employees Are Selected
                $where .= ' AND E.employee_id IN (' . $employeeIDs . ')';
            }
            if(isset($Program) && !empty($Program) && $Program !== '[]'){
                $Program = json_decode($Program,true);
                $programIDs = array();
                foreach($Program as $key=>$value){
                    array_push($programIDs,$value['id']);
                }
                $programIDs = implode(",",$programIDs);
                //Where Query for the Projects If Projects Are Selected
                $where .= ' AND MLP.ProgramID IN ('.$programIDs.')';
            }
            if (isset($projects) && !empty($projects) && $projects !== '[]') {
                $projects = json_decode($projects, true);
                $projectIDs = array();
                foreach ($projects as $key => $value) {
                    array_push($projectIDs, $value['id']);
                }
                $projectIDs = implode(",", $projectIDs);
                //Where Query for the Projects If Projects Are Selected
                $where .= ' AND EP.project_id IN (' . $projectIDs . ')';
            }
            if (isset($domiciles) && !empty($domiciles) && $projects !== '[]') {
                $domiciles = json_decode($domiciles, true);
                $districtIDs = array();
                foreach ($domiciles as $key => $value) {
                    array_push($districtIDs, $value['id']);
                }
                $districtIDs = implode(",", $districtIDs);
                //Where Query for the Districts If Districts Are Selected
                $where .= ' AND MLD.district_id IN (' . $districtIDs . ')';
            }
            //If There are Some Checkboxes Selected Then If Statement Should Populate..
            if (isset($selects) && !empty($selects)) {
                //For The Selected Fields if the Joins Are Needed Will Be Added On Demand..
                if (strpos($selects, 'MLC') !== false) {
                    $array = array(
                        'table' => 'ml_city MLC',
                        'condition' => 'PC.city_village = MLC.city_id',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'ET') !== false) {
                    $array = array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                        'type' => 'INNER'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'PM') !== false) {
                    $array = array(
                        'table' => 'position_management PM',
                        'condition' => 'PM.employement_id = ET.employment_id AND PM.current = 1 AND PM.status = 2 AND PM.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'MLDg') !== false) {
                    $array = array(
                        'table' => 'ml_designations MLDg',
                        'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND PM.current = 1',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'MLPG') !== false) {
                    $array = array(
                        'table' => 'ml_pay_grade MLPG',
                        'condition' => 'MLPG.ml_pay_grade_id = PM.ml_pay_grade_id AND PM.current = 1 AND MLPG.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'CC') !== false) {
                    $array = array(
                        'table' => 'current_contacts CC',
                        'condition' => 'CC.employee_id = E.employee_id AND CC.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'CN') !== false) {
                    $array = array(
                        'table' => 'contract CN',
                        'condition' => 'CN.employment_id = ET.employment_id AND CN.trashed = 0 AND CN.current = 1',
                        'type' => 'INNER'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'QF') !== false) {
                    $array = array(
                        'table' => '( SELECT *,MAX(Q.year) AS BigYear FROM qualification Q GROUP BY Q.employee_id ) AS QF',
                        'condition' => 'QF.employee_id = E.employee_id AND QF.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'PS') !== false) {
                    $array = array(
                        'table' => 'posting PS',
                        'condition' => 'PS.employement_id = ET.employment_id AND PS.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'MLB') !== false) {
                    $array = array(
                        'table' => 'ml_branch MLB',
                        'condition' => 'MLB.branch_id = PS.ml_branch_id AND MLB.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'ED') !== false) {
                    $array = array(
                        'table' => 'dependents ED',
                        'condition' => 'ED.employee_id = E.employee_id AND ED.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'MLDP') !== false) {
                    $array = array(
                        'table' => 'ml_department MLDP',
                        'condition' => 'MLDP.department_id = PS.ml_department_id AND MLDP.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'EI') !== false) {
                    $array = array(
                        'table' => 'employee_insurance EI',
                        'condition' => 'EI.employment_id = ET.employment_id AND EI.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'MLIT') !== false) {
                    $array = array(
                        'table' => 'ml_insurance_type MLIT',
                        'condition' => 'MLIT.insurance_type_id = EI.ml_insurance_id AND MLIT.trashed = 0',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                if (strpos($selects, 'DD') !== false) {
                    $array = array(
                        'table' => 'deduction DD',
                        'condition' => 'DD.employee_id = E.employee_id AND DD.trashed = 0 AND DD.status = 2 AND DD.eobi_no != ""',
                        'type' => 'LEFT'
                    );
                    array_push($joins, $array);
                }
                //Selected Fields Filters, If Checked will Be Added to the Selects..
                $selectedFieldsFilter = explode(",", $selects);
                if (in_array('NationalIDCardNo', $selectedFieldsFilter)) {
                    $selectsData .= ',E.CNIC AS CNIC';
                }
                if (in_array('ET.PS.MLDP.Department', $selectedFieldsFilter)) {
                    $selectsData .= ',MLDP.department_name AS Department';
                }
                if (in_array('MLC.CityVillage', $selectedFieldsFilter)) {
                    $selectsData .= ',MLC.city_name AS City_Name';
                }
                if (in_array('MLD.Domicile', $selectedFieldsFilter)) {
                    $selectsData .= ',MLD.district_name AS Domicile';
                }
                if (in_array('DOB', $selectedFieldsFilter)) {
                    $selectsData .= ',DATE_FORMAT(E.date_of_birth,"%D %M %Y") AS `D.O.B`';
                }
                if (in_array('ET.PM.MLDg.Designation', $selectedFieldsFilter)) {
                    $selectsData .= ',MLDg.designation_name AS Designation';
                }
                if (in_array('ET.PM.MLPG.Grade', $selectedFieldsFilter)) {
                    $selectsData .= ',MLPG.pay_grade AS Grade';
                }
                if (in_array('ET.JoiningDate', $selectedFieldsFilter)) {
                    $selectsData .= ',DATE_FORMAT(ET.joining_date,"%W %D %M %Y") AS Joining_Date';
                }
                if (in_array('PC.PostalAddress', $selectedFieldsFilter)) {
                    $selectsData .= ',PC.address AS Postal_Address';
                }
                if (in_array('CC.OfficialEmail', $selectedFieldsFilter)) {
                    $selectsData .= ',CC.official_email AS OfficialEmail';
                }
                if (in_array('CC.Contact', $selectedFieldsFilter)) {
                    $selectsData .= ',CC.mob_num AS Mobile_No';
                }
                if (in_array('ET.CN.ContractExpiry', $selectedFieldsFilter)) {
                    $selectsData .= ',date_format(CN.contract_expiry_date,"%d-%m-%Y") AS Contract_Expiry';
                }
                if (in_array('ET.EI.MLIT.InsuranceNo', $selectedFieldsFilter)) {
                    $selectsData .= ',IFNULL(GROUP_CONCAT(DISTINCT CONCAT(MLIT.insurance_type_name, " (", EI.insurance_no, ")") SEPARATOR ",<br />"), " No Insurance FOUND ") AS Insurance_No';
                }
                if (in_array('DD.EOBI', $selectedFieldsFilter)) {
                    $selectsData .= ',DD.eobi_no AS EOBI_No';
                }
                if (in_array('QF.Qualification', $selectedFieldsFilter)) {
                    $selectsData .= ',QF.qualification AS Qualification';
                }
                if (in_array('ET.PS.MLB.OfficeLocation', $selectedFieldsFilter)) {
                    $selectsData .= ',MLB.branch_name AS Office_Location';
                }
                if (in_array('ED.Dependents', $selectedFieldsFilter)) {
                    $selectsData .= ',IFNULL(GROUP_CONCAT(DISTINCT CONCAT(ED.dependent_name, "(", DATE_FORMAT(ED.date_of_birth,"%D %M %Y"), ")") SEPARATOR ",<br />")," No Dependents ") AS Dependents';
                }
            }
        }
        //Need to Remove Extra BackTicks Added By the CodeIgniter It Self.
        $data = array($selectsData, false);
        $viewData['EmployeeData'] = $this->common_model->select_fields_where_like_join($PTable, $data, $joins, $where, FALSE, '', '', $group_by);
        //Loading the View.
//        print_r($this->db->last_query());
        $this->load->view('Reports/HR_generated_report_print_preview', $viewData);
    }

//    Payroll/Salaries Report
    public function generateSalaryReport($view = NULL)
    {
        if ($view != NULL && $view = 'generate') {
            if ($this->input->post()) {
                $PTable = 'employee E';
                //Lets Get All The POsted Data From The View.
                $selectedCheckBoxes = $this->input->post('selectData');
                $employeeID = $this->input->post('empID');
                $ProgramID = $this->input->post('ProgID');
                $projectID = $this->input->post('proID');
                $departmentID = $this->input->post('department');
                $reportHeading = $this->input->post('reportHead');
                $postedYear = $this->input->post('selectedYear');
                if (isset($postedYear) and !empty($postedYear)) {
                    echo $postedYear;
                    $yearFilter = intval($postedYear);
                } else {
                    $yearFilter = date("Y");
                }
                $viewData['filteredYear'] = $yearFilter;
                if (isset($reportHeading) && !empty($reportHeading)) {
                    $viewData['reportHeading'] = $reportHeading;
                }
                $selectData = array('GROUP_CONCAT(E.employee_id) AS EmployeeID, COUNT(E.employee_id) AS TotalEmployeesPaid', false);
                $joins = array(
                    array(
                        'table' => 'employment ET',
                        'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                        'type' => 'INNER'
                    ),
                    array(
                        'table' => 'salary S',
                        'condition' => 'S.employement_id = ET.employment_id AND S.trashed = 0 AND S.status = 2',
                        'type' => 'INNER'
                    )
                );
                $where = array(
                    'E.trashed' => 0,
                    'E.enrolled' => 1
                );
                //If Main Filters Are Selected Except The Year Filter Then This Below If Statement Will Execute..
                if ((isset($employeeID) && $employeeID > 0) || (isset($ProgramID) && $ProgramID > 0) || (isset($projectID) && $projectID > 0) || (isset($departmentID) && $departmentID > 0)) {
                    if (isset($employeeID) && $employeeID > 0) {
                        $where['E.employee_id'] = $employeeID;
                        //If Any of the Above Three Main Conditions Are True Then.. Hu Haa Get The Employee Data..
                        $employeeTable = 'employee E';
                        $data = array('
                    E.employee_id AS EmployeeID,
                    E.full_name AS EmployeeName,
                    E.employee_code AS EmployeeCode,
                    MLDg.designation_name AS Designation,
                    MLD.department_name AS Department
                    ', false);
                        $empInfoJoins = array(
                            array(
                                'table' => 'employment ET',
                                'condition' => 'ET.employee_id = E.employee_id AND ET.current = 1 AND ET.trashed = 0',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'position_management PM',
                                'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.status = 2 AND PM.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'posting P',
                                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.status = 2 AND P.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_designations MLDg',
                                'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                                'type' => 'LEFT'
                            ),
                            array(
                                'table' => 'ml_department MLD',
                                'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                                'type' => 'LEFT'
                            )
                        );
                        $viewData['employeeInfo'] = $this->common_model->select_fields_where_like_join($employeeTable, $data, $empInfoJoins, $where, TRUE);

                    } elseif (isset($projectID) && $projectID > 0) {
                        //Do Some Stuff Here in Where if Project Is Selected
                        $where['MLP.project_id'] = $projectID;
                        $projectsData = ('MLP.project_title AS projectTitle, MLP.Abbreviation AS ProjectAbbreviation');
                        $projectJoins = array(
                            array(
                                'table' => 'employee_project EP',
                                'condition' => 'EP.employee_id = E.employee_id AND EP.trashed = 0 AND EP.current = 1',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_projects MLP',
                                'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_project_status_type MLST',
                                'condition' => 'MLST.pst_id = MLP.project_status_type_id AND MLST.trashed = 0',
                                'type' => 'LEFT'
                            )
                        );
                        $joins = array_merge($joins, $projectJoins);
                        $viewData['projectInfo'] = $this->common_model->select_fields_where_like_join($PTable, $projectsData, $joins, $where, TRUE);

                    }  elseif (isset($ProgramID) && $ProgramID > 0) {
                        //Do Some Stuff Here in Where if Project Is Selected
                        $where['MLP.ProgramID'] = $ProgramID;
                        $projectsData = ('MLPL.Program AS programName');
                        $projectJoins = array(
                            array(
                                'table' => 'ml_program_list MLPL',
                                'condition' => 'MLPL.ProgramID = MLP.ProgramID AND MLPL.trashed = 0',
                                'type' => 'INNER'
                            )

                        );
                        $joins = array_merge($joins, $projectJoins);
                        $viewData['programInfo'] = $this->common_model->select_fields_where_like_join($PTable, $projectsData, $joins, $where, TRUE);

                    }elseif (isset($departmentID) && $departmentID > 0) {
                        //Do Some Stuff Here if District Is Selected
                        //Update the Where Condition and Update The Joins For the Result of the Report...
                        $where['MLD.department_id'] = $departmentID;
                        $departmentSelectData = 'department_id AS DepartmentID, department_name AS DepartmentName';
                        $departmentJoins = array(
                            array(
                                'table' => 'posting P',
                                'condition' => 'P.employement_id = ET.employment_id AND P.trashed = 0 AND P.status = 2',
                                'type' => 'INNER'
                            ),
                            array(
                                'table' => 'ml_department MLD',
                                'condition' => 'MLD.department_id = P.ml_department_id AND MLD.trashed = 0',
                                'type' => 'INNER'
                            )
                        );
                        $joins = array_merge($joins, $departmentJoins);
                        $viewData['departmentInfo'] = $this->common_model->select_fields_where_like_join($PTable, $departmentSelectData, $joins, $where, TRUE);
                    }
                }
                if (strpos($selectedCheckBoxes, 'Salaries') !== false) {
                    $array = array(
                        array(
                            'table' => 'salary_payment SP',
                            'condition' => 'SP.salary_id = S.salary_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction ST',
                            'condition' => 'ST.transaction_id = SP.transaction_id AND ST.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLSM',
                            'condition' => 'MLSM.ml_month_id = SP.month AND MLSM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLSY',
                            'condition' => 'MLSY.ml_year_id = SP.year AND MLSY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $salariesJoins = array_merge($joins, $array);
                    $salariesSelectData = $selectData;
                    $salariesSelectData[0] .= ', SUM(SP.transaction_amount) AS Salaries,MLSM.month AS Month, MLSY.year AS `Year`';
                    $group_by = 'MLSM.Month';
                    $salariesWhere = $where;
                    $salariesWhere['MLSY.year'] = $yearFilter;
                    $salaries = $this->common_model->select_fields_where_like_join($PTable, $salariesSelectData, $salariesJoins, $salariesWhere, FALSE, '', '', $group_by);
                    $salaries = json_decode(json_encode($salaries), true);
                }
                //Now If Allowances Are Checked Then Do the Joining For Allowances
                if (strpos($selectedCheckBoxes, 'Allowances') !== false) {
                    $array = array(
                        array(
                            'table' => 'allowance A',
                            'condition' => 'A.employee_id = E.employee_id AND A.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'allowance_payment AP',
                            'condition' => 'AP.allowance_id = A.allowance_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction AT',
                            'condition' => 'AT.transaction_id = AP.transaction_id AND AT.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLAM',
                            'condition' => 'MLAM.ml_month_id = AP.month AND MLAM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLAY',
                            'condition' => 'MLAY.ml_year_id = AP.year AND MLAY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $allowancesJoins = array_merge($joins, $array);
                    $allowancesSelectData = $selectData;
                    $allowancesSelectData[0] .= ', SUM(AT.transaction_amount) AS Allowances, MLAM.month AS `Month`, MLAY.year AS `Year`';
                    $group_by = 'MLAM.Month';
                    $allowancesWhere = $where;
                    $allowancesWhere['MLAY.year'] = $yearFilter;
                    $allowances = $this->common_model->select_fields_where_like_join($PTable, $allowancesSelectData, $allowancesJoins, $allowancesWhere, FALSE, '', '', $group_by);
                    $allowances = json_decode(json_encode($allowances), true);
                }

                //Now If Expenses Are Checked Then Do the Joining For Allowances
                if (strpos($selectedCheckBoxes, 'Expenses') !== false) {
                    $array = array(
                        array(
                            'table' => 'expense_claims EXC',
                            'condition' => 'EXC.employee_id = E.employee_id AND EXC.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'expense_disbursment EXD',
                            'condition' => 'EXD.expense_id = EXC.expense_claim_id AND EXD.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction EXT',
                            'condition' => 'EXT.transaction_id = EXD.transaction_id AND EXT.trashed = 0 AND EXT.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLEM',
                            'condition' => 'MLEM.ml_month_id = EXD.month AND MLEM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLEY',
                            'condition' => 'MLEY.ml_year_id = EXD.year AND MLEY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $expensesJoins = array_merge($joins, $array);
                    $expensesSelectData = $selectData;
                    $expensesSelectData[0] .= ', SUM(EXT.transaction_amount) AS Expenses, MLEM.month AS `Month`, MLEY.year AS `Year`';
                    $group_by = 'MLEM.Month';
                    $expensesWhere = $where;
                    $expensesWhere['MLEY.year'] = $yearFilter;
                    $expenses = $this->common_model->select_fields_where_like_join($PTable, $expensesSelectData, $expensesJoins, $expensesWhere, FALSE, '', '', $group_by);
                    $expenses = json_decode(json_encode($expenses), true);
                }
                //Now If Deductions Are Checked Then Do the Joining For Allowances
                if (strpos($selectedCheckBoxes, 'Deductions') !== false) {
                    $array = array(
                        array(
                            'table' => 'deduction D',
                            'condition' => 'D.employee_id = E.employee_id AND D.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'deduction_processing DP',
                            'condition' => 'DP.deduction_id = D.deduction_id',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'transaction DT',
                            'condition' => 'DT.transaction_id = DP.transaction_id AND DT.trashed = 0 AND DT.status = 2',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_month MLDM',
                            'condition' => 'MLDM.ml_month_id = DP.month AND MLDM.trashed = 0',
                            'type' => 'INNER'
                        ),
                        array(
                            'table' => 'ml_year MLDY',
                            'condition' => 'MLDY.ml_year_id = DP.year AND MLDY.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $deductionsJoins = array_merge($joins, $array);
                    $deductionsSelectData = $selectData;
                    $deductionsSelectData[0] .= ', SUM(DT.transaction_amount) AS Deductions, MLDM.month AS `Month`, MLDY.year AS `Year`';
                    $group_by = 'MLDM.Month';
                    $expensesWhere = $where;
                    $expensesWhere['MLDY.year'] = $yearFilter;
                    $deductions = $this->common_model->select_fields_where_like_join($PTable, $deductionsSelectData, $deductionsJoins, $expensesWhere, FALSE, '', '', $group_by);
                    $deductions = json_decode(json_encode($deductions), true);
                }

                //First Lets Get All The Months In The System
                $monthTable = 'ml_month';
                $data = 'ml_month_id AS MonthID, ml_month.month AS `Month`';
                $where = array(
                    'trashed' => 0
                );
                $allMonths = $this->common_model->select_fields_where($monthTable, $data, $where, FALSE);
                if (!isset($allMonths) || empty($allMonths)) {
                    //redirect to error page or do someThing As all Months Do Not Exist..
                }

                $mergedArray = array();
                foreach ($allMonths as $month) {
                    //Salaries Are Checked Then This Below Foreach Will Execute..
                    if (isset($salaries) and !empty($salaries) and is_array($salaries)) {
                        foreach ($salaries as $monthSalaries) {
                            if ($month->Month === $monthSalaries['Month']) {
                                if (array_key_exists($month->Month, $mergedArray)) {
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month], $monthSalaries);
                                } else {
                                    $mergedArray[$month->Month] = $monthSalaries;
                                }

                            }
                        }
                    }//End of Salaries Foreach

                    if (isset($allowances) and !empty($allowances) and is_array($allowances)) {
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach ($allowances as $monthAllowances) {
                            if ($month->Month === $monthAllowances['Month']) {
                                if (array_key_exists($month->Month, $mergedArray)) {
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month], $monthAllowances);
                                } else {
                                    $mergedArray[$month->Month] = $monthAllowances;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if (isset($expenses) and !empty($expenses) and is_array($expenses)) {
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach ($expenses as $monthExpenses) {
                            if ($month->Month === $monthExpenses['Month']) {
                                if (array_key_exists($month->Month, $mergedArray)) {
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month], $monthExpenses);
                                } else {
                                    $mergedArray[$month->Month] = $monthExpenses;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if (isset($deductions) and !empty($deductions) and is_array($deductions)) {
                        //Allowances Are Checked Then This Below Foreach Will Execute..
                        foreach ($deductions as $deduction) {
                            if ($month->Month === $deduction['Month']) {
                                if (array_key_exists($month->Month, $mergedArray)) {
                                    $mergedArray[$month->Month] = array_merge($mergedArray[$month->Month], $deduction);
                                } else {
                                    $mergedArray[$month->Month] = $deduction;
                                }
                            }
                        }
                    }//End of Allowances Foreach

                    if (!array_key_exists($month->Month, $mergedArray)) {
                        $array = array();
                        if (isset($salaries) && !empty($salaries)) {
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Salaries'] = '-';
                        }
                        if (isset($allowances) && !empty($allowances)) {
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Allowances'] = '-';
                        }
                        if (isset($expenses) && !empty($expenses)) {
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Expenses'] = '-';
                        }
                        if (isset($deductions) && !empty($deductions)) {
                            $array['TotalEmployeesPaid'] = '-';
                            $array['EmployeeID'] = '-';
                            $array['Month'] = $month->Month;
                            $array['Deductions'] = '-';
                        }
                        $mergedArray[$month->Month] = $array;
                    }
                }

                //Time To Send Data To View Now..
                $viewData['ReportTableData'] = $mergedArray;
                $viewData['SelectedCheckBoxes'] = $selectedCheckBoxes;
//                return;
            } else {
                //redirect to the Un Authorized Page.
                return;
            }
            if (!isset($viewData) || empty($viewData)) {
                $viewData = '';
            }
            $viewData['view'] = $this->load->view('Reports/pdf_reports/GeneratePrintReport', $viewData, true);
            //Load The Generate View..
            $this->load->view('Reports/generatePayrollReportForPrint', $viewData);
            return;
        }

        //Load The View For The Generation of Report..
        $this->load->view('includes/header');
        $this->load->view('Reports/generatePayrollReport');
        $this->load->view('includes/footer');
    }

    //New Payroll Reports
    public function projectChargeReport($page = NULL)
    {
        if ($page !== NULL and $page === 'generatedReport') {
            if (!$this->input->post()) {
                redirect(previousURL());
            }

            //Get All The posted Values In to Variable
            $projectID = $this->input->post('ProjectID');
            $programID = $this->input->post('Program');
            $selectedMonthID = intval($this->input->post('selectedMonthID'));
            $designationID = $this->input->post('designation');
            $viewData['reportHeading'] = $this->input->post('reportHead');
            $selectedYear = $this->input->post('selectedYear');
            if (!isset($selectedYear) || empty($selectedYear)) {
                $selectedYear = date('Y');
            }

         if (!isset($programID) || empty($programID)) {
                $msg = "Please Select Program::error";
                $this->session->set_flashdata('msg', $msg);
                redirect('reports_con/projectChargeReport');
                return;
            }

            if (!isset($projectID) || empty($projectID)) {
                $msg = "Please Select Project::error";
                $this->session->set_flashdata('msg', $msg);
                redirect(prevsiousURL());
                return;
            }
            if (!isset($selectedMonthID) || empty($selectedMonthID) || !is_numeric($selectedMonthID)) {
                $msg = "Please Select Month::error";
                $this->session->set_flashdata('msg', $msg);
                redirect(prevsiousURL());
                return;
            }


            //Getting Total Number of Days
            $numberOfDaysInSelectedMonth = cal_days_in_month(CAL_GREGORIAN, $selectedMonthID, intval($selectedYear));


            //First Lets Get Small Data For View.
            $monthTable = 'ml_month MLM';
            $selectMonthData = 'MLM.month AS `Month`';
            $where = array(
                'ml_month_id' => $selectedMonthID,
                'trashed' => 0
            );
            $monthInfo = $this->common_model->select_fields_where($monthTable, $selectMonthData, $where, TRUE);
            if (!isset($monthInfo) || empty($monthInfo)) {
                $msg = 'Something went Wrong with POST, Please Contact System Administrator For Further Assistance::error';
                $this->session->set_flashdata('msg', $msg);
                redirect(previousURL());
                return;
            } else {
                $viewData['Month'] = $monthInfo->Month;
            }


            //Get Data From Database For Project Charges Report.
            $table = 'employee_project EP';
            $selectData = array('E.employee_id AS EmployeeID,ET.employment_id AS EmploymentID,
  E.employee_code AS EmployeeCode,
  E.full_name AS EmployeeName,
  EP.project_id AS ProjectID,
  MLP.project_title AS ProjectTitle,
  MLP.Abbreviation AS ProjectAbbreviation,
  MLPROG.Program AS PROGRAM,
  S.base_salary AS EmployeeBaseSalary,
  IFNULL(EP.percentCharged,0) AS PercentCharged,
  IFNULL(A.allowance_amount,0) + IFNULL(S.base_salary,0) + IFNULL(EC.amount,0) AS MonthlySalary,
  project_assign_date AS ProjectAssignDate,
  project_revoke_date AS ProjectRevokeDate,
  MLDg.designation_name AS EmployeeDesignation', false);
            $joins = array(
                array(
                    'table' => 'employment ET',
                    'condition' => 'ET.employment_id = EP.employment_id AND ET.trashed = 0 AND ET.current = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'employee E',
                    'condition' => 'E.employee_id = ET.employee_id AND E.trashed = 0 AND E.enrolled = 1',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_projects MLP',
                    'condition' => 'MLP.project_id = EP.project_id AND MLP.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'ml_program_list MLPROG',
                    'condition' => 'MLPROG.ProgramID = MLP.ProgramID AND MLPROG.trashed = 0',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'salary S',
                    'condition' => 'S.employement_id = ET.employment_id AND S.trashed = 0 AND S.status = 2',
                    'type' => 'INNER'
                ),
                array(
                    'table' => 'allowance A',
                    'condition' => 'A.employment_id = ET.employment_id AND A.trashed = 0 AND A.status = 2',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'expense_claims EC',
                    'condition' => 'EC.employment_id = ET.employment_id AND EC.status = 2 AND EC.month =' . $selectedMonthID . ' AND YEAR(EC.approval_date) =' . $selectedYear,
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'position_management PM',
                    'condition' => 'PM.employement_id = ET.employment_id AND PM.trashed = 0 AND PM.status = 2 AND PM.current = 1',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'ml_designations MLDg',
                    'condition' => 'MLDg.ml_designation_id = PM.ml_designation_id AND MLDg.trashed = 0',
                    'type' => 'LEFT'
                )
            );
            $group_by = 'E.employee_id,ET.employment_id,EP.project_id';
            $where = 'EP.trashed` = 0  AND EP.current = 1 AND EP.project_id = ' . $projectID . ' AND MLPROG.ProgramID = ' . $programID . ' AND ' . $selectedYear . ' >= YEAR(project_assign_date) AND ' . $selectedMonthID . ' >= DATE_FORMAT(project_assign_date,"%m")';
            (isset($designationID) && !empty($designationID)) ? $where .= ' AND PM.ml_designation_id =' . $designationID : '';
            $resultEmployeesProjectChargesReport = $this->common_model->select_fields_where_like_join($table, $selectData, $joins, $where, FALSE, '', '', $group_by);
//            echo $this->db->last_query();
            if (!isset($resultEmployeesProjectChargesReport) || empty($resultEmployeesProjectChargesReport)) {
                $msg = 'No Record Found, Please try Selecting Different Filters::warning';
                $this->session->set_flashdata('msg', $msg);
                redirect(previousURL());
            }
/*            echo  "<pre>";
            print_r($resultEmployeesProjectChargesReport);
            return;*/
            //We Have Employee AND Project Details But Need To Get The Salary Information..


//            echo "<pre>";
            foreach ($resultEmployeesProjectChargesReport as $reportKey => $reportValue) {
                if (intval(date('n', strtotime($reportValue->ProjectAssignDate))) === $selectedMonthID) {
                    $selectedMonthLastDate = strtotime(date('Y-' . $selectedMonthID . '-' . $numberOfDaysInSelectedMonth));
                    $startTimeStamp = strtotime($reportValue->ProjectAssignDate);
                    $timeDiff = abs($selectedMonthLastDate - $startTimeStamp);
                    $numberDays = $timeDiff / 86400;
                    $resultEmployeesProjectChargesReport[$reportKey]->NumberOfDays = floor($numberDays) + 1;
                    //Project Charge Percentage
                    $percentageSalary = $reportValue->MonthlySalary * ($reportValue->PercentCharged / 100);
                    $resultEmployeesProjectChargesReport[$reportKey]->ProjectPercentageSalary = $percentageSalary;

                    //To Use Number Of Days First We Need To Find Out TotalSalary
                    $perDayCalculatedSalaryForCurrentMonth = $resultEmployeesProjectChargesReport[$reportKey]->ProjectPercentageSalary / $numberOfDaysInSelectedMonth;
                    $totalCalculatedSalaryCurrentMonth = floor($perDayCalculatedSalaryForCurrentMonth * $resultEmployeesProjectChargesReport[$reportKey]->NumberOfDays);

                    //As We Got The Salary
                    $resultEmployeesProjectChargesReport[$reportKey]->PayableAmount = $totalCalculatedSalaryCurrentMonth;
                } else {
                    $percentageSalary = $reportValue->MonthlySalary * ($reportValue->PercentCharged / 100);
                    $resultEmployeesProjectChargesReport[$reportKey]->ProjectPercentageSalary = $percentageSalary;

                    $resultEmployeesProjectChargesReport[$reportKey]->NumberOfDays = $numberOfDaysInSelectedMonth;
                    $resultEmployeesProjectChargesReport[$reportKey]->PayableAmount = $resultEmployeesProjectChargesReport[$reportKey]->ProjectPercentageSalary;
                }
            }
/*            echo "<pre>";
            print_r($resultEmployeesProjectChargesReport);
            return;*/
            $viewData['projectChargesReportData'] = $resultEmployeesProjectChargesReport;
            ////
            $query = $this->db->last_query();
            // Code for CSV File
            $this->load->dbutil();
            $this->load->helper('file');
            $newline = "\r\n";
            $data = $this->common_model->get_query_record($query);
            ///$data=str_replace('EmployeeID'," ",$data);
            //$query=json_encode($query);
            $csv = $this->dbutil->csv_from_result($data, ',', $newline);
//        $csv = ltrim(strstr($this->dbutil->csv_from_result($data, ',', "\r\n"), "\r\n"));

            $this->load->helper('download');

            $uploadPath = 'systemGeneratedFiles/ProjectChargeCsvReports/' . $this->data['EmployeeID'] . '/ProjectCharge' . time() . '.csv';
            $uploadDirectory = 'systemGeneratedFiles/ProjectChargeCsvReports/' . $this->data['EmployeeID'];
            //If Directories are Not Avaialable, Create The Directories On The Go..
            if (!is_dir($uploadDirectory)) {
                mkdir($uploadDirectory, 0755, true);
            }

            $path = 'systemGeneratedFiles/ProjectChargeCsvReports/' . $this->data['EmployeeID'] . '/';
            $delete_files = glob($path . '*');
            foreach ($delete_files as $dFile) { // iterate files
                if (is_file($dFile))
                    unlink($dFile); // delete file
                //echo $file.'file deleted';
            }

            //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
            //$fileName = ("Parexons_GeneratedReport_HR_CSV_'.time().'.csv",$csv);
            //$filePath = $uploadPath.$fileName;
            write_file($uploadPath, $csv, 'w+');

            $viewData['files'] = $uploadPath;

            //END
            ////
//            echo "</pre>";
            $viewData['viewData'] = $this->load->view('Reports/generatedProjectChargeReportPDF', $viewData, TRUE);
            $this->load->view('Reports/generatedProjectChargeReport', $viewData);
            return;
        }

        $this->load->view('includes/header');
        $this->load->view('Reports/generateProjectChargeReport');
        $this->load->view('includes/footer');
    }

    public function employeeWiseHrReport($page = NULL)
    {
        //Loading View If No Page Is Defined(Means $page variable is NULL)
        $this->load->view('includes/header');
        $this->load->view('Reports/generateEmployeeWiseHrReport');
        $this->load->view('includes/footer');
    }

    // Function for Pdf Hr Reports
    function downloadGeneratedPDFReportEmployeeWisePayRoll(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path='systemGeneratedFiles/EmployeeWisePayrollReportPDF/'.$this->data['EmployeeID'].'/';
                $delete_files= glob($path.'*');
                foreach($delete_files as $dFile){ // iterate files
                    if(is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/EmployeeWisePayrollReportPDF/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/EmployeeWisePayrollReportPDF/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'EmployeeWisePayrollReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                $this->session->set_userdata('pdf_file',$filePath);
                redirect($filePath);
                // echo "Download::".$filePath;
                return;

            }
        }
    }
    //Function for Send pdf file in email
    function SendPDFEmployeeWisePayrollReport(){
        if($this->input->is_ajax_request()){
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if($this->input->post()){
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if(!isset($mailTo) || empty($mailTo)){
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if(!isset($mailFrom) || empty($mailFrom)){
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table,$data,$joins,$where,TRUE);

                    if(!isset($employeeInfo) || empty($employeeInfo)){
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if(!isset($messageBody) ||empty($messageBody)){
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');



                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF

                $uploadPath = 'systemGeneratedFiles/EmployeeWisePayrollReportPDF/'.$this->data['EmployeeID'].'/';
                $uploadDirectory = 'systemGeneratedFiles/EmployeeWisePayrollReportPDF/'.$this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if(!is_dir($uploadDirectory)){
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'EmployeeWisePayrollReport_PDF_'.time().'.pdf';
                $filePath = $uploadPath.$fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                /*                redirect($filePath);
                                return;*/


                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email',$config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if(is_admin($this->data['EmployeeID'])){
                    $this->email->from($mailFrom,'Phrism Administrator');
                }else{
                    $this->email->from($mailFrom,'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }

            }
        }
    }
    // END
    // Function for email HR report as pdf
    public function emailPdfProjectChargeReport()
    {
        if ($this->input->is_ajax_request()) {
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if ($this->input->post()) {
                // Email validation
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if (!isset($mailTo) || empty($mailTo)) {
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if (!isset($mailFrom) || empty($mailFrom)) {
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);

                    if (!isset($employeeInfo) || empty($employeeInfo)) {
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if (!isset($messageBody) || empty($messageBody)) {
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * // End
                 *
                 * $viewDataForPDF = $this->input->post('viewData');
                 * /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'] . '/';
                $delete_files = glob($path . '*');
                foreach ($delete_files as $dFile) { // iterate files
                    if (is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'] . '/';
                $uploadDirectory = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if (!is_dir($uploadDirectory)) {
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_ProjectCharge_PDF_' . time() . '.pdf';
                $filePath = $uploadPath . $fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);

                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'phrismapp@gmail.com',
                    'smtp_pass' => 'securePass786',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if (is_admin($this->data['EmployeeID'])) {
                    $this->email->from($mailFrom, 'Phrism Administrator');
                } else {
                    $this->email->from($mailFrom, 'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if ($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
                return;

            }
        }
    }

    // End

    // Function for email HR report as pdf
    public function PdfProjectChargeReport()
    {
        if ($this->input->is_ajax_request()) {
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if ($this->input->post()) {

                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'] . '/';
                $delete_files = glob($path . '*');
                foreach ($delete_files as $dFile) { // iterate files
                    if (is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'] . '/';
                $uploadDirectory = 'systemGeneratedFiles/ProjectChargeReportPdf/' . $this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if (!is_dir($uploadDirectory)) {
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_ProjectCharge_PDF_' . time() . '.pdf';
                $filePath = $uploadPath . $fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                redirect($filePath);
                return;

            }
        }
    }

    // End

    //Loading Select2 Selectors programs
    function loadAllAvailablePrograms()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_program_list MLPG';
                $data = array('MLPG.ProgramID AS ProgramID, MLPG.Program AS ProTitle', false);
                $group_by = '';
                $order_by = array('MLPG.ProgramID', 'DESC');
                $where = array(
                    'MLPG.trashed' => 0,
                    'MLPG.enabled' => 1
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLPG.Program';
                    $years = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by, $order_by);
                } else {
                    $years = $this->common_model->select_fields_where($table, $data, $where, FALSE, '', '', '', $group_by, $order_by);
                }
                print_r(json_encode($years));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    //Loading Select2 Selectors
    function loadAllAvailableYears()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_year MLY';
                $data = array('MLY.ml_year_id AS YearID, MLY.year AS YearTitle', false);
                $group_by = '';
                $order_by = array('MLY.year', 'DESC');
                $where = array(
                    'MLY.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLY.year';
                    $years = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by, $order_by);
                } else {
                    $years = $this->common_model->select_fields_where($table, $data, $where, FALSE, '', '', '', $group_by, $order_by);
                }
                print_r(json_encode($years));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    function loadAllAvailableMonths()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_month MLM';
                $data = array('MLM.ml_month_id AS MonthID, MLM.month AS MonthTitle', false);
                $group_by = '';
                $where = array(
                    'MLM.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLM.month';
                    $months = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by);
                } else {
                    $months = $this->common_model->select_fields_where($table, $data, $where, FALSE);
                }
                print_r(json_encode($months));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }

    function loadAllAvailableDesignations()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $sValue = $this->input->post('term');
                $table = 'ml_designations MLDg';
                $data = array('MLDg.ml_designation_id AS DesignationID, MLDg.designation_name AS Designation', false);
                $group_by = '';
                $where = array(
                    'MLDg.trashed' => 0
                );
                if (isset($sValue) and !empty($sValue)) {
                    $field = 'MLDg.designation_name';
                    $designations = $this->common_model->select_fields_where_like($table, $data, $where, FALSE, $field, $sValue, $group_by);
                } else {
                    $designations = $this->common_model->select_fields_where($table, $data, $where, FALSE);
                }
                print_r(json_encode($designations));
            } else {
                echo "FAIL::Un Authorized Access::error";
                return;
            }
        } else {
            //redirect To Unauthorized Page.
        }
    }
        function programeWiseReports(){
            $this->load->view('includes/header');
            $this->load->view('Reports/ProgramWiseReport1');
            $this->load->view('includes/footer');
        }

    function programeWiseReports2(){
       $programs= $this->input->post('ProgramIDS');
       $Employee= get_employment_from_employeeID($this->input->post('Employee'));
        $table = 'employee E';
        $data = ('
        MLPr.project_id AS ProjectID,
        E.employee_id AS EmployeeID,
        E.employee_code AS EmployeeCode,
        E.full_name AS EmployeeName,
        MLDG.designation_name,
        ML_Post_lc.posting_location,
        (CASE WHEN (Cont.extension = 0) THEN Cont.contract_start_date ElSE ContExt.e_start_date END) as Contract_Start_Date,
        (CASE WHEN (Cont.extension = 0) THEN Cont.contract_expiry_date ElSE ContExt.e_end_date END) as Contract_Expiry_date,
        MLPr.project_title AS ProjectTitle');
        $joins = array(
            array(
                'table' => 'employment ET',
                'condition' => 'ET.employee_id = E.employee_id',
                'type' => 'INNER'
            ),
            array(
                'table' => 'position_management PM',
                'condition' => 'PM.employement_id = ET.employment_id AND PM.current = 1',
                'type' => 'left'
            ),
            array(
                'table' => 'ml_designations MLDG',
                'condition' => 'MLDG.ml_designation_id = PM.ml_designation_id',
                'type' => 'left'
            ),
            array(
                'table' => 'employee_project EPROJ',
                'condition' => 'EPROJ.employment_id = ET.employment_id',
                'type' => 'left'
            ),
            array(
                'table' => 'ml_projects MLPr',
                'condition' => 'MLPr.project_id = EPROJ.project_id',
                'type' => 'left'
            ),
            array(
                'table' => 'ml_program_list MLProg',
                'condition' => 'MLProg.ProgramID = MLPr.ProgramID',
                'type' => 'left'
            ),
            array(
                'table' => 'contract Cont',
                'condition' => 'Cont.employment_id = ET.employment_id',
                'type' => 'left'
            ),
            array(
                'table' => 'employee_contract_extensions ContExt',
                'condition' => 'ContExt.contract_id = Cont.contract_id AND ContExt.current = 1 AND ContExt.trashed = 0' ,
                'type' => 'left'
            ),
            array(
                'table' => 'posting Post',
                'condition' => 'Post.employement_id = ET.employment_id AND Post.current = 1',
                'type' => 'INNER'
            ),
            array(
                'table' => 'ml_posting_location_list ML_Post_lc',
                'condition' => 'ML_Post_lc.posting_locations_list_id = Post.location',
                'type' => 'left'
            )
        );
        if(isset($Employee) && !empty($Employee)){
        $where = array(
            'E.enrolled' => 1,
            'E.trashed' => 0,
            'MLProg.ProgramID' => $programs,
            //'EPROJ.employment_id' => $Employee,
        'EPROJ.current'=>1,
        'ET.current'=>1,
        'ET.employment_id'=>$Employee,
        'ET.trashed'=>0);
        }else{
            $where = array(
                'E.enrolled' => 1,
                'E.trashed' => 0,
                'MLProg.ProgramID' => $programs,
                'EPROJ.current'=>1,
                'ET.current'=>1,
                'ET.trashed'=>0);
        }
        //$group_by='EPROJ.project_id';
        $resultProjectEmployees = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where, FALSE,$field='',$value='',$group_by='');
    //print_r($resultProjectEmployees); die;
        ///Converting Object Array to 3 Dimentional Array.
        /// Where Top Array Will Have Projects Arrays Inside
        //Inside Project Arrays Will Be Employees Array Related To That Project.
        if(isset($resultProjectEmployees) && !empty($resultProjectEmployees) && is_array($resultProjectEmployees)){

            //Now As We Have Data Inside So We Can Continue Further.
            //Need To Convert Object Array In to Associated Array.
            $resultProjectEmployeesAssociated= json_decode(json_encode($resultProjectEmployees),true);
            $finalReportArray = array();
            foreach($resultProjectEmployeesAssociated as $key => $val){
                //Need To Keep The Key Index Unique
                if(!array_key_exists($val['ProjectID'],$finalReportArray)){
                    $finalReportArray[$val['ProjectID']] = array(
                        'projectID' => $val['ProjectID'],
                        'projectTitle' => $val['ProjectTitle'],
                        'employees' => array()
                    );
                }

                $finalReportArray[$val['ProjectID']]['employees'][$val['EmployeeID']] = array(
                    'employeeID' => $val['EmployeeID'],
                    'employeeCode' => $val['EmployeeCode'],
                    'employeeName' => $val['EmployeeName'],
                    'designation' => $val['designation_name'],
                    'posting' => $val['posting_location'],
                    'contractStart' => $val['Contract_Start_Date'],
                    'contractExpiry' => $val['Contract_Expiry_date'],
                );
            }
           // echo '<pre>';
           // print_r($finalReportArray);
           // return;

            $viewData['info'] = $finalReportArray;
            $viewData['PdfView'] =$this->load->view('HR/Reports/generateEmployeeWiseReport2Pdf',$viewData,true);
        }

       // echo "test"; die;
        $this->load->view('HR/Reports/generateEmployeeWiseReport2',@$viewData);
    }

    /// PDF employee wise report
    // Function for email HR report as pdf
    public function PdfEmployeeWiseReport()
    {
        if ($this->input->is_ajax_request()) {
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if ($this->input->post()) {

                $viewDataForPDF = $this->input->post('viewData');
                /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'] . '/';
                $delete_files = glob($path . '*');
                foreach ($delete_files as $dFile) { // iterate files
                    if (is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'] . '/';
                $uploadDirectory = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if (!is_dir($uploadDirectory)) {
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_EmployeeWiseProgramPdf_' . time() . '.pdf';
                $filePath = $uploadPath . $fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);
                redirect($filePath);
                echo "download:: Pdf is Ready For Download::success";
                return;

            }
        }
    }

    // End
    // Function for email HR report as pdf
    public function emailPdfProgramWiseReport()
    {
        if ($this->input->is_ajax_request()) {
            //If Ajax Request Has Been Generated To Send Data For Email Then Lets Send Data For Email..
            if ($this->input->post()) {
                // Email validation
                $mailTo = $this->input->post('ToEmail');
                $mailFrom = $this->input->post('FromEmail');
                $messageSubject = $this->input->post('MessageSubject');
                $messageBody = $this->input->post('MessageBody');
                $viewDataForPDF = $this->input->post('viewData');

                if (!isset($mailTo) || empty($mailTo)) {
                    echo "FAIL::You Must Provide To Email Address::error";
                    return;
                }
                if (!isset($mailFrom) || empty($mailFrom)) {
                    //As mail is not sent from the view so we need to check if email is assigned to this employee..
                    $table = 'employee E';
                    $data = ('E.employee_id AS EmployeeID, U.employee_email AS EmployeeEmail');
                    $joins = array(
                        array(
                            'table' => 'user_account U',
                            'condition' => 'U.employee_id = E.employee_id AND U.trashed = 0',
                            'type' => 'INNER'
                        )
                    );
                    $where = array(
                        'E.enrolled' => 1,
                        'E.trashed' => 0,
                        'E.employee_id' => $this->data['EmployeeID'] //This is Currently Logged In Employee, Getting It from the Parent Class MyController.
                    );
                    $employeeInfo = $this->common_model->select_fields_where_like_join($table, $data, $joins, $where, TRUE);

                    if (!isset($employeeInfo) || empty($employeeInfo)) {
                        echo "FAIL:: Some Problem with The Email Posting, Please Contact System Administrator For Further Assistance::error";
                        return;
                    }
                    $mailFrom = $employeeInfo->EmployeeEmail;
                }
                if (!isset($messageBody) || empty($messageBody)) {
                    echo "FAIL:: Can Not Send Empty Email, You Must Write Something in Message::error";
                    return;
                }

                /**
                 * // End
                 *
                 * $viewDataForPDF = $this->input->post('viewData');
                 * /**
                 * Generate PDF File and Save It Under The Employee Who Generated IT.
                 */

                // Load library
                $this->load->library('pdf');
                // Convert to PDF

                // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

                ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($viewDataForPDF); // write the HTML into the PDF
                $path = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'] . '/';
                $delete_files = glob($path . '*');
                foreach ($delete_files as $dFile) { // iterate files
                    if (is_file($dFile))
                        unlink($dFile); // delete file
                    //echo $file.'file deleted';
                }
                $uploadPath = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'] . '/';
                $uploadDirectory = 'systemGeneratedFiles/EmployeeWiseProgramPdf/' . $this->data['EmployeeID'];
                //If Directories are Not Avaialable, Create The Directories On The Go..
                if (!is_dir($uploadDirectory)) {
                    mkdir($uploadDirectory, 0755, true);
                }
                //Need To Create the PDF File Which We Need TO Send later Throught Attachment..
                $fileName = 'Parexons_GeneratedReport_EmployeeWiseProgramPdf_' . time() . '.pdf';
                $filePath = $uploadPath . $fileName;
                $pdf->Output($filePath, 'F'); // save to file because we can
                //file_put_contents($uploadPath.$fileName, $output);

                //As We Have All The Requirements From the Post We Will Email To the User..
                //Need To do Little Configurations for SMTP..
                $config =$this->data['mailConfiguration'];
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                //Now Lets Send The Email..

                $this->email->to($mailTo);

                if (is_admin($this->data['EmployeeID'])) {
                    $this->email->from($mailFrom, 'Phrism Administrator');
                } else {
                    $this->email->from($mailFrom, 'Employee At Phrism');
                }
                $this->email->subject($messageSubject);
                $this->email->message($messageBody);
                $this->email->attach($filePath);
                if ($this->email->send()) {
                    echo 'OK::PDF File Successfully Emailed::success';
                } else {
                    $this->email->print_debugger();
                    echo 'FAIL::Some Problem Occurred, Please Contact System Administrator For Further Assistance::error';
                    return;
                }
                return;

            }
        }
    }

    // End

}

?>