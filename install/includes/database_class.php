<?php

class Database {

	// Function to the database and tables and fill them with the default data
	function create_database($data)
	{
		// Connect to the database
		$mysqli = new mysqli($data['hostname'],$data['username'],$data['password'],'');

		// Check for errors
		if(mysqli_connect_errno())
			return false;

		// Create the prepared statement
		$mysqli->query("CREATE DATABASE IF NOT EXISTS ".$data['databaseName']);

		// Close the connection
		$mysqli->close();

		return true;
	}

	// Function to create the tables and fill them with the default data
	function create_tables($data)
	{
		// Connect to the database
		$mysqli = new mysqli($data['hostname'],$data['username'],$data['password'],$data['databaseName']);

		// Check for errors
		if(mysqli_connect_errno())
			return false;

		// Open the default SQL file
		$query = file_get_contents('assets/install.sql');

//        var_dump($query);
		// Execute a multi query
//        $mysqli->multi_query($query);
        if ($mysqli->multi_query($query)) {
            do {
                // fetch results
//                echo $mysqli->more_results();
                if (!$mysqli->more_results()) {
                    break;
                }
                if (!$mysqli->next_result()) {
                    if($mysqli->errno){
                        if($mysqli->error === "Variable 'sql_mode' can't be set to the value of 'NULL'"){
                            return true;
                        }else{
                            die($mysqli->error);
                        }
                    }

                    break;
                }
//                echo $mysqli->next_result();
            } while (true);
        }

		// Close the connection
		$mysqli->close();

		return true;
	}
}