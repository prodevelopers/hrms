<?php $db_config_path = '../application/config/database.php';
?>
<!doctype html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>HRMS - Installation</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/gsdk-base.css" rel="stylesheet" />
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
</head>
<style type="text/css">
    .wizard-container{
        padding-bottom:20px;
        padding-top: 0px;
    }
    .f_logo{
        height:85px;
        width:85px;
        float:left;
        margin:10px 10px 10px 20px;
        border:1px solid #4D6684;
    }
    .f_container{
        background:#999;
        margin-top:90px;
    }
    .f_wizard{
        height:80px;
    }
    .f_p{
        float:left;
        color:white;
        padding-left:15px;
        padding-top:10px;
    }
    .f_b_bold{
        font-weight:bold;
    }
    .f_b_normal{
        font-weight:normal;
    }
    .image-container{
        background:#F2F4F3;
    }
    .w_h_h3{color:#6e7070;font-size:22px;}
    .w_h_b{color:#6e7070;}
    .alert_suces{background:#b6eaba;}
    .alert_suces b{padding-right:10px;}
    .alert-danger b{padding-right:10px;}
</style>
<body>
<div class="image-container set-full-height">
    <!--   Creative Tim Branding   -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="header" style="height:60px;background:#4D6684;">
                    <h4 style="line-height:60px;color:white;margin:0px;padding:0px;padding-left:20px;">HR Management System</h4>
                </div>
            </div>
        </div>
    </div>
    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php if(is_writable($db_config_path)){?>
                <!--      Wizard container        -->
                    <!--Alerts Messages Start-->
                    <div class="row" style="padding-top:30px;">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="alert alert-success alert_suces" id="success" role="alert"><b >Success:</b>The insertion done Sucessfully</div>
                        </div>
                    </div>
                    <!--Alerts Messages End-->

                    <!--Alerts Messages Start-->
                    <div class="row" style="padding-top:30px;">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="alert alert-danger" role="alert" id="error"><b >Sorry:</b>Error in Insertion !!!</div>
                        </div>
                    </div>
                    <!--Alerts Messages End-->

                    <div class="wizard-container">
                    <form action="" method="" id="installationForm">
                        <input type="hidden" id="hiddenValueUse">
                        <div class="card wizard-card ct-wizard-orange" id="wizard">
                            <!--        You can switch "ct-wizard-orange"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->

                            <div class="wizard-header">
                                <h3 class="w_h_h3">
                                    <b class="w_h_b">Database</b> Step By Step Installation <br>

                                </h3>
                            </div>
                            <ul>
                                <li><a href="#Database" data-toggle="tab">Database Details</a></li>
                                <li><a href="#company" data-toggle="tab">Company Details</a></li>
                                <li><a href="#address" data-toggle="tab">Administrator Details</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="Database">
                                    <div class="row">
                                        <h4 class="info-text"> Database Details</h4>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Host Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name ="hostname" id="hostname" value="localhost" placeholder="Please Enter Host Name..."">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">User Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="username" placeholder="Please Enter User Name...">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Database Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="databaseName" placeholder="Please Enter Company Name...">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Password</label>
                                                <div class="col-xs-8">
                                                    <input type="Password" class="form-control" id="password" placeholder="Please Enter Password...">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane" id="company">
                                    <h4 class="info-text"> Company Details </h4>
                                    <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label col-xs-3">Company Name</label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" id="companyName" placeholder="Please Enter Company Name...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label col-xs-3">Email</label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" id="companyEmail" placeholder="Email...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label col-xs-3">Phone</label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" id="companyPhone" placeholder="Phone Number...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label col-xs-3">Address</label>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control" id="companyAddress" placeholder="Address...">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="address">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text"> Administrator Details </h4>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">First Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="firstName" placeholder="Please Enter First Name...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Last Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="lastName" placeholder="Please Enter Last Name...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="cnic" class="control-label col-xs-3">CNIC</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="cnic" placeholder="Please Enter CNIC...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">User Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="adminUsername" placeholder="Please Enter User Name...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Password</label>
                                                <div class="col-xs-8">
                                                    <input type="password" class="form-control" id="userPassword" placeholder="Please Enter Password...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Confirm Password</label>
                                                <div class="col-xs-8">
                                                    <input type="password" class="form-control" id="userConfirmPassword" placeholder="Please Re Enter Above Password...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Email</label>
                                                <div class="col-xs-8">
                                                    <input type="email" class="form-control" id="userEmail" placeholder="e-g jhon@parexons.com...">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Country</label>
                                                <div class="col-xs-8">
                                                    <select class="form-control">
                                                        <option>Pakistani</option>
                                                        <option>Indian</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right:10px;margin-left:10px;margin-bottom:20px">
                                            <div class="form-group">
                                                <label for="inputEmail" class="control-label col-xs-3">Address</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="employeeAddress" placeholder="Please Enter Address...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                    <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />

                                </div>
                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div> <!-- wizard container -->
                <?php } else { ?>
                    <p class="error">Please make the application/config/database.php file writable. <strong>Example</strong>:<br /><br /><code>chmod 777 application/config/database.php</code></p>
                <?php } ?>
            </div>
        </div><!-- end row -->
    </div> <!--  big container -->

    <div class="container-fluid f_container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-wizard f_wizard">
                    <img src="../assets/images/pxn.png" class="img-responsive f_logo">
                    <p class="f_p"><b class="f_b_bold">Parexons</b><br>
                        <b class="f_b_normal">IT Solutions & Services</b><br>
                        <b class="f_b_normal">business@parexons.com</b><br>
                </div>
            </div>
        </div>
    </div>



</div>
<div id="domMessage" style="display:none;">
    <h1>We are processing your request.  Please be patient.</h1>
</div>
<?php
$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$linkArray = explode('/',$actual_link);
array_pop($linkArray);
array_pop($linkArray);
$baseURL = implode('/',$linkArray);
$baseURL = 'http://'.$baseURL.'/';
echo "<script type='text/javascript'> var baseURL= '".$baseURL."';</script>";
?>

</body>

<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.blockUI.js" type="text/javascript"></script>

<!--   plugins 	 -->
<script src="js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="js/wizard.js"></script>
<script type="text/javascript">
    $(document).ajaxStop($.unblockUI);

    $(document).ready(function(){
        //for Finish Button
        $('.btn-finish').on('click',function(e){
            var selectedTabID = 'adminDetails';
            var returnAddressValue = validateAddress(selectedTabID,baseURL);
            if(returnAddressValue === false){
                return false;
            }
        });
    });

</script>

