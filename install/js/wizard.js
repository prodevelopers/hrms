searchVisible = 0;
transparent = true;
var outputResult;
$(document).ready(function(){
    $('#error').hide();
    $('#success').hide();
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();
    
        
    $('#wizard').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',
         onInit : function(tab, navigation,index){
         
           //check number of tabs and fill the entire row
           var $total = navigation.find('li').length;
           $width = 100/$total;
           
           $display_width = $(document).width();
           
           if($display_width < 400 && $total > 3){
               $width = 50;
           }
           navigation.find('li').css('width',$width + '%');
        },
         onTabClick : function(tab, navigation, index){
            // Disable the posibility to click on tabs
            return false;
        },
        onNext: function(tab, navigation, index) {
            var numTabs    = $('#installationForm').find('.tab-pane').length;
            var previousForm = index-1;
            var selectedTab = $('#installationForm').find('.tab-pane').eq(previousForm);
            var selectedTabID = selectedTab.attr('id');
            //console.log(selectedTabID);
            if(selectedTabID === 'Database'){
                var returnDBValue = validateDatabase(selectedTabID);
                if(returnDBValue === false || typeof returnDBValue === 'undefined'){
                    return false;
                }
            }else if(selectedTabID === 'company'){
                var returnCompanyValue = validateCompany(selectedTabID);
                if(returnCompanyValue === false){
                    return false;
                }
            }

            //Need To Do Validation For Database First..

            if (index === numTabs) {
                // We are at the last tab
                // Uncomment the following line to submit the form using the defaultSubmit() method
                // $('#installationForm').formValidation('defaultSubmit');

                // For testing purpose

                $('#completeModal').modal();
            }

            return true;
        },
        onPrevious: function(tab, navigation, index) {
            var previousForm = index+1;
        },
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;

            var wizard = navigation.closest('.wizard-card');
            
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $(wizard).find('.btn-next').hide();
                $(wizard).find('.btn-finish').show();
            } else {
                $(wizard).find('.btn-next').show();
                $(wizard).find('.btn-finish').hide();
                $("#error").html('').append('<span style="color:green">Please Fill The Form Correctly !</span>');
            }
        }
    });

    // Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
    
    
    $('[data-toggle="wizard-radio"]').click(function(event){
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked','true');
    });
    
    $height = $(document).height();
    $('.set-full-height').css('height',$height);
    
    //functions for demo purpose
    

});


 //Function to show image before upload

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function validateDatabase(currentTab){
//Now First Do Need To Do The Validation..
    //Get Values First.
    var hostname = $('#Database').find('#hostname').val(),
        username = $('#Database').find('#username').val(),
        password = $('#Database').find('#password').val(),
        databaseName = $('#Database').find('#databaseName').val();

    if(hostname.length === 0){
        console.log('Please Enter Hostname');
        return false;
    } else if(username.length === 0){
        $('#error').show();
        var msg='<b>Error:</b> Please Enter Username';
        $("#error").html('').append(msg);
        return false;
    } else if(databaseName.length === 0){
        $('#error').show();
        var msg='<b>Error:</b> Please Enter Database Name';
        $("#error").html('').append(msg);
        return false;
    }

    //Now If We Reach Up To This Point, Means Our Data is Ready For Ajax Call..
    var postData = {
        'hostname' : hostname,
        'username':username,
        'password':password,
        'databaseName':databaseName,
        'currentTab' : currentTab
    };
    var ajaxReturn =
        $.ajax({
            url: 'process.php',
            data: postData,
            type: 'POST',
            async: false
        }).responseText;

    var data = ajaxReturn.split('::');
    if (data[0] === 'OK') {
        $('#error').hide();
        $('#success').show();
        $("#success").html('').append(data[1]);
        $('#hiddenValueUse').val(data[2]);
        return true;
    } else if (data[0] === 'FAIL') {
        $('#error').show();
        $('#success').hide();
        $("#error").html('').append(data[1]);
        $('#hiddenValueUse').val('false');
        return false;
    }
}

//Now Validation For Company
function validateCompany(currentTab){
    //Getting Required Values First.
    var companyName = $('#companyName').val(),
        companyEmail = $('#companyEmail').val(),
        companyPhone = $('#companyPhone').val(),
        companyAddress = $('#companyAddress').val(),
    //For Database Settings.
        hostname = $('#hostname').val(),
        username = $('#username').val(),
        password = $('#password').val(),
        databaseName = $('#databaseName').val();
    if(companyName.length === 0){
        var msg='<b>Sorry:</b> Please Enter Company Name';
        $("#error").html('').append(msg);
        return false;
    }else if(companyEmail.length === 0){
        var msg='<b>Sorry:</b> Please Enter Cmpany Email';
        $("#error").html('').append(msg);
        return false;
    }else if(companyPhone.length === 0){
        var msg='<b>Sorry:</b> Please Enter Company Phone Number';
        $("#error").html('').append(msg);
        return false;
    }else if(companyAddress.length === 0){
        var msg='<b>Sorry:</b> Please Enter Company Address';
        $("#error").html('').append(msg);
        return false;
    }
    var dbHiddenValue = $('#hiddenValueUse').val();
    var postData = {
        'company' : companyName,
        'companyMail':companyEmail,
        'companyPhone':companyPhone,
        'companyAddress':companyAddress,
        'currentTab' : currentTab,
        'databaseName':databaseName,
        'hostname' : hostname,
        'username':username,
        'password':password
    };
    //console.log(currentTab);
    $.blockUI();
    var ajaxReturn =
        $.ajax({
            url: 'process.php',
            data: postData,
            type: 'POST',
            async: false
        }).responseText;

if(ajaxReturn == "Variable 'sql_mode' can't be set to the value of 'NULL'"){
    $('#error').hide();
    $('#success').show();
    $("#success").html('').append('Database and Tables Have Been Successfully Created.');
    return true;
}
    var data = ajaxReturn.split('::');
    if (data[0] === 'OK') {
        $('#error').hide();
        $('#success').show();
        $("#success").html('').append(data[1]);
        $('#hiddenValueUse').val(data[2]);
        return true;
    } else if (data[0] === 'FAIL') {
        $('#error').show();
        $('#success').hide();
        $("#error").html('').append(data[1]);
        $('#hiddenValueUse').val('false');
        return false;
    }
}

function validateAddress(currentTab,baseURL){
    //Getting Required Values First.
    var firstName = $('#firstName').val(),
        lastName = $('#lastName').val(),
        CNIC = $('#cnic').val(),
        adminUsername = $('#adminUsername').val(),
        userPassword = $('#userPassword').val(),
        userEmail = $('#userEmail').val(),
        userConfirmPassword = $('#userConfirmPassword').val(),
        employeeAddress = $('#employeeAddress').val(),

    //For Database Settings.
        hostname = $('#hostname').val(),
        username = $('#username').val(),
        password = $('#password').val(),
        databaseName = $('#databaseName').val();


    if(firstName.length === 0){
        var msg='<b>FAIL:</b> Please Enter First Name';
        $("#error").html('').append(msg);
    }
    if(lastName.length === 0){
        var msg='<b>FAIL:</b> Please Enter Last Name';
        $("#error").html('').append(msg);
    }
    if(adminUsername.length === 0){
        var msg='<b>FAIL:</b> Please Enter Username';
        $("#error").html('').append(msg);
    }
    if(userPassword.length === 0){
        var msg='<b>FAIL:</b> Please Enter Password You Want to SET';
        $("#error").html('').append(msg);
    }
    if(userConfirmPassword.length === 0){
        var msg='<b>FAIL:</b> Please Enter The Confirm Password';
        $("#error").html('').append(msg);
    }
    if(userConfirmPassword !== userPassword){
        var msg='<b>FAIL:</b> Password and Confirm Password Do Not Match.';
        $("#error").html('').append(msg);
    }

    var postData = {
        firstName:firstName,
        lastName:lastName,
        adminUsername:adminUsername,
        adminPass:userPassword,
        userEmail:userEmail,
        CNIC:CNIC,
        employeeAddress:employeeAddress,
        hostname:hostname,
        dbUsername:username,
        dbPassword:password,
        databaseName:databaseName,
        currentTab:currentTab
    };

    //console.log(currentTab);
    var ajaxReturn =
        $.ajax({
            url: 'process.php',
            data: postData,
            type: 'POST',
            async: false
        }).responseText;


    var data = ajaxReturn.split('::');
    if (data[0] === 'OK') {
        $('#error').hide();
        $('#success').show();
        $("#success").html('').append(data[1]);
        $('#hiddenValueUse').val(data[2]);
        window.location.href=baseURL;
        return true;
    } else if (data[0] === 'FAIL') {
        $('#error').show();
        $('#success').hide();
        $("#error").html('').append(data[1]);
        $('#hiddenValueUse').val('false');
        return false;
    }
}
    












