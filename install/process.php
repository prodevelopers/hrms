<?php
/**
 * Created by PhpStorm.
 * User: sizzling
 * Date: 4/22/2015
 * Time: 9:32 AM
 */
error_reporting(0); //Setting this to E_ALL showed that that cause of not redirecting were few blank lines added in some php files.

// Only load the classes in case the user submitted the form
if($_POST) {

//Lets Get The Values First.
    $currentTab = $_POST['currentTab'];
    if($currentTab === 'Database'){
        // Load the classes and create the new objects
        require_once('includes/core_class.php');
        require_once('includes/database_class.php');

        $core = new Core();
        $database = new Database();


        // Validate the post data
        if($core->validate_post($_POST) == true)
        {
            // First create the database, then create tables, then write config file
            if($database->create_database($_POST) === false) {
                echo 'FAIL::The database could not be created, please verify your settings.::error';
                $error = TRUE;
            } else if ($database->create_tables($_POST) == false) {
                echo 'FAIL::The database tables could not be created, please verify your settings.::error';
                $error = TRUE;
            } else if ($core->write_config($_POST) == false) {
                echo 'FAIL::The database configuration file could not be written, please chmod application/config/database.php file to 777::error';
                $error = TRUE;
            }

            // If no errors, redirect to registration page
            if(!isset($error) && $error !== TRUE) {
                echo 'OK::Successfully Created Database and Tables.::'.$_POST['databaseName'];
                exit;
            }

        } else {
            echo "FAIL::Not all fields have been filled in correctly. The host, username, password, and database name are required.::error";
            exit;
//            $message = $core->show_message('error','Not all fields have been filled in correctly. The host, username, password, and database name are required.');
        }
    }elseif($currentTab === 'company'){
        //Company Information.

        $companyName = $_POST['company'];
        $companyAddress = $_POST['companyAddress'];
        $companyMail = $_POST['companyMail'];
        $companyPhone = $_POST['companyPhone'];
        $hostname = $_POST['hostname'];
        $databaseName = $_POST['databaseName'];
        $username = $_POST['username'];
        $password = $_POST['password'];

        if(!isset($companyName) || empty($companyName)){
            echo 'FAIL::Comapny Name is required';
            exit;
        }
        if(!isset($companyMail) || empty($companyMail)){
            echo 'FAIL::Company Email Address is Required';
            exit;
        }

        $mysqli = new mysqli($_POST['hostname'],$_POST['username'],$_POST['password'],$_POST['databaseName']);

        if(mysqli_connect_errno()){
            echo 'FAIL::Some Error Occurred, Could Not Connect To Database::error';
            exit;
        }
//        echo 'test';
        //Insert New Record If No Record Exist.
        $selectQuery = 'SELECT COUNT(1) AS TotalRecordsFound FROM organization where 1 = 1';
        $result = $mysqli->query($selectQuery);
        $countResult = $result->fetch_array();
//        echo 'test';
        if(isset($countResult) && $countResult['TotalRecordsFound'] > 0){
            echo 'There is Data In Table.';
            //Then Will Do The Update
            //For Now No Need To Add Update Query.. Cuz Script Will Create New Table.
        }else{
//            echo 'test';
            //Will Do The Insertion.
//            var_dump($data['databaseName']);
            $insertQuery = 'INSERT INTO organization(org_name,phone,email,address) VALUES("'.$companyName.'","'.$companyPhone.'","'.$companyMail.'","'.$companyAddress.'")';
//            echo $insertQuery;
            $mysqli->query($insertQuery);
            if($mysqli->error){
                echo "FAIL::Some Error Occurred During Insertion of Company Details::error";
                exit;
            }else{
                echo "OK::Company Details Successfully Added To The System::success";
                exit;
            }
        }
        //Now Lets Do The Insertion.//
    }elseif($currentTab === 'adminDetails'){
        //Getting The Posted Values.
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $adminUsername = $_POST['adminUsername'];
        $adminPassword = $_POST['adminPass'];
        $adminEmail = $_POST['userEmail'];
        $adminAddress = $_POST['employeeAddress'];
        $hostname = $_POST['hostname'];
        $databaseName = $_POST['databaseName'];
        $databaseUsername = $_POST['dbUsername'];
        $databasePassword = $_POST['dbPassword'];
        $CNIC = $_POST['CNIC'];

        //Now Just Test For Crucial Parts.
        if(!isset($firstName) || empty($firstName)){
            echo 'FAIL::Please Enter First Name::error';
            exit;
        }
        if(!isset($lastName) || empty($lastName)){
            echo 'FAIL::Please Enter Last Name::error';
            exit;
        }
        if(!isset($adminUsername) || empty($adminUsername)){
            echo 'FAIL::Please Enter Username::error';
            exit;
        }
        if(!isset($adminPassword) || empty($adminPassword)){
            echo 'FAIL::Please Enter Password::error';
            exit;
        }
        if(!isset($CNIC) || empty($CNIC)){
            echo 'FAIL::Please Enter CNIC::error';
            exit;
        }



        $mysqli = new mysqli($hostname,$databaseUsername,$databasePassword,$databaseName);

        if(mysqli_connect_errno()){
            echo 'FAIL::Some Error Occurred, Could Not Connect To Database::error';
            exit;
        }

        //Now Do The Insertions.
        $selectQuery = 'SELECT COUNT(1) AS TotalRecordsFound FROM employee where 1 = 1';
        $result = $mysqli->query($selectQuery);
       $countResult = $result->fetch_array();

        if(isset($countResult) && $countResult['TotalRecordsFound'] > 0){
            echo 'There is Data In Table.';
            exit;
            //Then Will Do The Update
            //For Now No Need To Add Update Query.. Cuz Script Will Create New Table.
        }else{
            $mysqli->autocommit(FALSE);
            $employeeCode = 'E-001';
            $image = 'default.png';
            $enrolled = 1;
            $trashed = 0;
            $insertQuery = 'INSERT INTO employee(employee_code,first_name,last_name,full_name,CNIC,enrolled,trashed,photograph,thumbnail) VALUES("'.$employeeCode.'","'.$firstName.'","'.$lastName.'","'.$firstName.' '.$lastName.'","'.$CNIC.'","'.$enrolled.'","'.$trashed.'","'.$image.'","'.$image.'")';
            $mysqli->query($insertQuery);
            if($mysqli->error){
                echo "FAIL::Some Error Occurred During Insertion of Admin Details::error";
                exit;
            }
            //Now Need TO Insert In The
            $newlyCreatedEmployeeID = $mysqli->insert_id;
            $userGroup = 1;
            $enabled = 1;
            $trashed = 0;
            $currentDate = date('Y-m-d');
            $insertQuery = 'INSERT INTO user_account(employee_id,user_name,employee_email,password,user_group,acct_creation_date,enabled,trashed) VALUES("'.$newlyCreatedEmployeeID.'","'.$adminUsername.'","'.$adminEmail.'","'.$adminPassword.'","'.$userGroup.'","'.$currentDate.'","'.$enabled.'","'.$trashed.'")';
            $mysqli->query($insertQuery);
            if($mysqli->error){
                echo $mysqli->error;
                echo "FAIL::Some Error Occurred During Creating Login Credentials::error";
                exit;
            }

            //Now Need TO Insert For Employement..
            $zero = 0;
            $trashed = 0;
            $current = 1;
            $joiningDate = date('Y-m-d');

            $insertQuery = 'INSERT INTO employment(employee_id,joining_date,`current`,trashed,do_eos_settlement,settlement_payment_status) VALUES("'.$newlyCreatedEmployeeID.'","'.$joiningDate.'","'.$current.'","'.$trashed.'","'.$zero.'","'.$zero.'")';
            $mysqli->query($insertQuery);
            if($mysqli->error){
                echo $mysqli->error;
                echo "FAIL::Some Error Occurred During Creating employment::error";
                exit;
            }
            
            $mysqli->commit();
            //If Everything Went Fine.
            echo 'OK::New Record Successfully Added To The System::success';
            exit;
        }
    }


}else{
    echo 'You are Not Allowed To Access This Page Directly.';
    exit;
}
?>