
/*Table structure for table `ml_allowance_type` */
DROP TABLE IF EXISTS `ml_allowance_type`;

CREATE TABLE `ml_allowance_type` (
  `ml_allowance_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `allowance_type` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_allowance_type_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_allowance_type` */

/*Table structure for table `ml_applicant_status_types` */

DROP TABLE IF EXISTS `ml_applicant_status_types`;

CREATE TABLE `ml_applicant_status_types` (
  `applicantStatusTypeID` INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `statusTitle` VARCHAR(80) COLLATE utf8_bin NOT NULL,
  `trashed` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT '0',
  PRIMARY KEY (`applicantStatusTypeID`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_applicant_status_types` */

INSERT  INTO `ml_applicant_status_types`(`applicantStatusTypeID`,`statusTitle`,`trashed`) VALUES (1,'Applicant',0),(2,'ShortListed',0),(3,'Selected',0);

/*Table structure for table `ml_approval_authorities` */

DROP TABLE IF EXISTS `ml_approval_authorities`;

CREATE TABLE `ml_approval_authorities` (
  `approval_auth_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) NOT NULL,
  `ml_approval_type_id` INT(11) NOT NULL,
  `enabled` TINYINT(1) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`approval_auth_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_approval_authorities` */

/*Table structure for table `ml_approval_types` */

DROP TABLE IF EXISTS `ml_approval_types`;

CREATE TABLE `ml_approval_types` (
  `ml_approval_types_id` INT(11) NOT NULL AUTO_INCREMENT,
  `approval_types` VARCHAR(200) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_approval_types_id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ml_approval_types` */

INSERT  INTO `ml_approval_types`(`ml_approval_types_id`,`approval_types`,`enabled`,`trashed`) VALUES (1,'Pending',1,0),(2,'Approved',1,0),(3,'Declined',1,0),(4,'ABC',1,1),(5,'abc',1,1);

/*Table structure for table `ml_assign_task` */

DROP TABLE IF EXISTS `ml_assign_task`;

CREATE TABLE `ml_assign_task` (
  `ml_assign_task_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_name` VARCHAR(45) NOT NULL,
  `project_id` INT(11) DEFAULT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_assign_task_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_assign_task` */

/*Table structure for table `ml_attendance_status_type` */

DROP TABLE IF EXISTS `ml_attendance_status_type`;

CREATE TABLE `ml_attendance_status_type` (
  `attendance_status_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_type` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`attendance_status_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ml_attendance_status_type` */

INSERT  INTO `ml_attendance_status_type`(`attendance_status_type_id`,`status_type`,`enabled`,`trashed`) VALUES (1,'Present',1,0),(2,'Absent',1,0),(3,'On Leave',1,0);

/*Table structure for table `ml_benefit_type` */

DROP TABLE IF EXISTS `ml_benefit_type`;

CREATE TABLE `ml_benefit_type` (
  `benefit_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `benefit_type_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`benefit_type_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_benefit_type` */

/*Table structure for table `ml_benefits_list` */

DROP TABLE IF EXISTS `ml_benefits_list`;

CREATE TABLE `ml_benefits_list` (
  `benefit_item_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `benefit_name` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`benefit_item_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_benefits_list` */

/*Table structure for table `ml_branch` */

DROP TABLE IF EXISTS `ml_branch`;

CREATE TABLE `ml_branch` (
  `branch_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_name` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`branch_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_branch` */

/*Table structure for table `ml_city` */

DROP TABLE IF EXISTS `ml_city`;

CREATE TABLE `ml_city` (
  `city_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_name` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`city_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_city` */

/*Table structure for table `ml_currency` */

DROP TABLE IF EXISTS `ml_currency`;

CREATE TABLE `ml_currency` (
  `ml_currency_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `currency_name` VARCHAR(256) NOT NULL,
  `active` TINYINT(1) DEFAULT '0',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_currency_id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ml_currency` */

INSERT  INTO `ml_currency`(`ml_currency_id`,`currency_name`,`active`,`enabled`,`trashed`) VALUES (1,'PKR',1,1,0),(2,'USD',0,1,0),(3,'Lira',0,1,1);

/*Table structure for table `ml_deduction_type` */

DROP TABLE IF EXISTS `ml_deduction_type`;

CREATE TABLE `ml_deduction_type` (
  `ml_deduction_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `deduction_type` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_deduction_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ml_deduction_type` */

INSERT  INTO `ml_deduction_type`(`ml_deduction_type_id`,`deduction_type`,`enabled`,`trashed`) VALUES (2,'Security',1,0),(3,'Pension Fund',1,0),(4,'Tax',1,1),(5,'EOBI Deduction',1,0),(6,'ABC',1,1),(7,'xyz',1,1);

/*Table structure for table `ml_department` */

DROP TABLE IF EXISTS `ml_department`;

CREATE TABLE `ml_department` (
  `department_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `department_name` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`department_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_department` */

/*Table structure for table `ml_designations` */

DROP TABLE IF EXISTS `ml_designations`;

CREATE TABLE `ml_designations` (
  `ml_designation_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `designation_name` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_designation_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_designations` */

/*Table structure for table `ml_disciplinary_action` */

DROP TABLE IF EXISTS `ml_disciplinary_action`;

CREATE TABLE `ml_disciplinary_action` (
  `disciplinaryActionID` INT(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Disciplinary Action',
  `action_type` VARCHAR(60) COLLATE utf8_bin NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`disciplinaryActionID`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_disciplinary_action` */

INSERT  INTO `ml_disciplinary_action`(`disciplinaryActionID`,`action_type`,`trashed`) VALUES (1,'counseling',0),(2,'verbal warning',0),(3,'written warning',0),(4,'suspension',0),(5,'termination',0);

/*Table structure for table `ml_district` */

DROP TABLE IF EXISTS `ml_district`;

CREATE TABLE `ml_district` (
  `district_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `district_name` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `color` VARCHAR(20) DEFAULT NULL,
  `highlight` VARCHAR(20) DEFAULT NULL,
  PRIMARY KEY (`district_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_district` */

/*Table structure for table `ml_employment_type` */

DROP TABLE IF EXISTS `ml_employment_type`;

CREATE TABLE `ml_employment_type` (
  `employment_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_type` VARCHAR(45) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`employment_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `ml_employment_type` */

INSERT  INTO `ml_employment_type`(`employment_type_id`,`employment_type`,`trashed`,`enabled`) VALUES (1,'Full Time',0,1),(2,'Part Time',0,1),(3,'Casual',0,1),(4,'Probationary period',0,1),(5,'Piecework employment',0,1),(6,'Temporary',0,1),(7,'Trainees ',0,1);

/*Table structure for table `ml_expense_type` */

DROP TABLE IF EXISTS `ml_expense_type`;

CREATE TABLE `ml_expense_type` (
  `ml_expense_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `expense_type_title` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_expense_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ml_expense_type` */

INSERT  INTO `ml_expense_type`(`ml_expense_type_id`,`expense_type_title`,`enabled`,`trashed`) VALUES (1,'Travel',1,0),(2,'Party',1,0),(3,'Fixed Expenses',1,0);

/*Table structure for table `ml_gender_type` */

DROP TABLE IF EXISTS `ml_gender_type`;

CREATE TABLE `ml_gender_type` (
  `gender_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gender_type_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`gender_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `ml_gender_type` */

INSERT  INTO `ml_gender_type`(`gender_type_id`,`gender_type_title`,`enabled`,`trashed`) VALUES (1,'Male',1,1),(2,'Female',1,1),(34,'Other',1,0),(35,'Male',1,0),(36,'Female',1,0),(37,'Saidrahman',1,1),(38,'Saidrahman',1,0);

/*Table structure for table `ml_holiday` */

DROP TABLE IF EXISTS `ml_holiday`;

CREATE TABLE `ml_holiday` (
  `ml_holiday_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `holiday_type` VARCHAR(255) NOT NULL,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_holiday_id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

/*Data for the table `ml_holiday` */

/*Table structure for table `ml_increment_type` */

DROP TABLE IF EXISTS `ml_increment_type`;

CREATE TABLE `ml_increment_type` (
  `increment_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `increment_type` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`increment_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ml_increment_type` */

INSERT  INTO `ml_increment_type`(`increment_type_id`,`increment_type`,`enabled`,`trashed`) VALUES (1,'Monthly',1,0);

/*Table structure for table `ml_insurance_type` */

DROP TABLE IF EXISTS `ml_insurance_type`;

CREATE TABLE `ml_insurance_type` (
  `insurance_type_id` INT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `insurance_type_name` VARCHAR(80) DEFAULT NULL,
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`insurance_type_id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

/*Data for the table `ml_insurance_type` */

/*Table structure for table `ml_interviewstatus` */

DROP TABLE IF EXISTS `ml_interviewstatus`;

CREATE TABLE `ml_interviewstatus` (
  `InterviewStatusID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `InterviewStatus` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL,
  `trashed` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`InterviewStatusID`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_interviewstatus` */

INSERT  INTO `ml_interviewstatus`(`InterviewStatusID`,`InterviewStatus`,`trashed`) VALUES (1,'Selected',0),(2,'Pending',0),(3,'Rejected',0);

/*Table structure for table `ml_job_category` */

DROP TABLE IF EXISTS `ml_job_category`;

CREATE TABLE `ml_job_category` (
  `job_category_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_category_name` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`job_category_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_job_category` */

/*Table structure for table `ml_job_title` */

DROP TABLE IF EXISTS `ml_job_title`;

CREATE TABLE `ml_job_title` (
  `ml_job_title_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_job_title_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_job_title` */

/*Table structure for table `ml_jobad_type` */

DROP TABLE IF EXISTS `ml_jobad_type`;

CREATE TABLE `ml_jobad_type` (
  `JobAD_ID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(50) COLLATE utf8_bin DEFAULT NULL,
  `Trashed` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`JobAD_ID`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_jobad_type` */

INSERT  INTO `ml_jobad_type`(`JobAD_ID`,`Title`,`Trashed`) VALUES (1,'Open',0),(2,'Closed',0);

/*Table structure for table `ml_projects` */

DROP TABLE IF EXISTS `ml_projects`;

CREATE TABLE `ml_projects` (
  `project_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProgramID` INT(11) UNSIGNED DEFAULT NULL,
  `project_status_type_id` INT(11) UNSIGNED DEFAULT NULL,
  `project_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `color` VARCHAR(20) DEFAULT NULL,
  `highlight` VARCHAR(20) DEFAULT NULL,
  `project_start_date` DATE DEFAULT NULL,
  `project_end_date` DATE DEFAULT NULL,
  `donor` VARCHAR(255) DEFAULT NULL,
  `duration` VARCHAR(255) DEFAULT NULL,
  `Abbreviation` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_projects` */

/*Table structure for table `ml_kpi` */

DROP TABLE IF EXISTS `ml_kpi`;

CREATE TABLE `ml_kpi` (
  `ml_kpi_id` INT(11) NOT NULL AUTO_INCREMENT,
  `kpi` VARCHAR(45) NOT NULL,
  `project_id` INT(11) UNSIGNED DEFAULT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_kpi_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `ml_projects` (`project_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_kpi` */

/*Table structure for table `ml_leave_type` */

DROP TABLE IF EXISTS `ml_leave_type`;

CREATE TABLE `ml_leave_type` (
  `ml_leave_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `leave_type` VARCHAR(45) NOT NULL,
  `hex_color` VARCHAR(10) DEFAULT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_leave_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `ml_leave_type` */

INSERT  INTO `ml_leave_type`(`ml_leave_type_id`,`leave_type`,`hex_color`,`enabled`,`trashed`) VALUES (1,'Sick','#00ccee',1,0),(2,'Unpaid Leaves','#cc0033',1,0),(3,'Earned Leaves','#aa3300',1,0),(4,'Casual','#00cc22',1,0),(5,'Short Leave','#00cc44',1,0);

/*Table structure for table `ml_letter_text` */

DROP TABLE IF EXISTS `ml_letter_text`;

CREATE TABLE `ml_letter_text` (
  `LetterTextID` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `LetterType` VARCHAR(20) COLLATE utf8_bin DEFAULT NULL,
  `LetterText` TEXT COLLATE utf8_bin,
  PRIMARY KEY (`LetterTextID`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_letter_text` */

INSERT  INTO `ml_letter_text`(`LetterTextID`,`LetterType`,`LetterText`) VALUES (1,'AppointmentLetter','<p>Respected gul khan.</p>\n<p>it is requested to you. that the tasks assign to you. complete it in a given period of time..</p>\n<p>gul khan...dear gul khan your appointemtn</p>'),(2,'ContractLetter','');

/*Table structure for table `ml_marital_status` */

DROP TABLE IF EXISTS `ml_marital_status`;

CREATE TABLE `ml_marital_status` (
  `marital_status_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `marital_status_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`marital_status_id`)
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ml_marital_status` */

INSERT  INTO `ml_marital_status`(`marital_status_id`,`marital_status_title`,`enabled`,`trashed`) VALUES (1,'Single',1,0),(2,'Married',1,0),(3,'Complicated',1,0);

/*Table structure for table `ml_module_list` */

DROP TABLE IF EXISTS `ml_module_list`;

CREATE TABLE `ml_module_list` (
  `ml_module_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_name` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `module_controllers` VARCHAR(255) DEFAULT NULL,
  `priority` INT(3) DEFAULT NULL,
  PRIMARY KEY (`ml_module_id`)
) ENGINE=INNODB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `ml_module_list` */

INSERT  INTO `ml_module_list`(`ml_module_id`,`module_name`,`enabled`,`trashed`,`module_controllers`,`priority`) VALUES (1,'HR Module',1,0,'human_resource',2),(2,'Payroll Module',1,0,'payroll',4),(3,'ESSP Module',1,0,'essp',100),(4,'Leave And Attendance',1,1,'leave_attendance',3),(5,'Dashboard',1,0,'dashboard_site',1);

/*Table structure for table `ml_month` */

DROP TABLE IF EXISTS `ml_month`;

CREATE TABLE `ml_month` (
  `ml_month_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `month` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_month_id`)
) ENGINE=INNODB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `ml_month` */

INSERT  INTO `ml_month`(`ml_month_id`,`month`,`enabled`,`trashed`) VALUES (1,'Jan',1,0),(2,'Feb',1,0),(3,'Mar',1,0),(4,'Apr',1,0),(5,'May',1,0),(6,'Jun',1,0),(7,'Jul',1,0),(8,'Aug',1,0),(9,'Sep',1,0),(10,'Oct',1,0),(11,'Nov',1,0),(12,'Dec',1,0);

/*Table structure for table `ml_nationality` */

DROP TABLE IF EXISTS `ml_nationality`;

CREATE TABLE `ml_nationality` (
  `nationality_id` INT(11) NOT NULL AUTO_INCREMENT,
  `nationality` VARCHAR(200) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`nationality_id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ml_nationality` */

INSERT  INTO `ml_nationality`(`nationality_id`,`nationality`,`enabled`,`trashed`) VALUES (1,'Pakistani',1,0),(2,'Canadian',1,0),(3,'American',1,0);

/*Table structure for table `ml_pay_frequency` */

DROP TABLE IF EXISTS `ml_pay_frequency`;

CREATE TABLE `ml_pay_frequency` (
  `ml_pay_frequency_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pay_frequency` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_pay_frequency_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_pay_frequency` */

INSERT  INTO `ml_pay_frequency`(`ml_pay_frequency_id`,`pay_frequency`,`enabled`,`trashed`) VALUES (1,'Monthly',1,0),(2,'Yearly',1,0),(3,'Once',1,0);

/*Table structure for table `ml_pay_grade` */

DROP TABLE IF EXISTS `ml_pay_grade`;

CREATE TABLE `ml_pay_grade` (
  `ml_pay_grade_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pay_grade` INT(5) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_pay_grade_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_pay_grade` */

INSERT  INTO `ml_pay_grade`(`ml_pay_grade_id`,`pay_grade`,`enabled`,`trashed`) VALUES (1,16,1,0),(2,17,1,0),(3,18,1,0),(4,19,1,1);

/*Table structure for table `ml_payment_mode_types` */

DROP TABLE IF EXISTS `ml_payment_mode_types`;

CREATE TABLE `ml_payment_mode_types` (
  `payment_mode_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_mode_type` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payment_mode_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_payment_mode_types` */

INSERT  INTO `ml_payment_mode_types`(`payment_mode_type_id`,`payment_mode_type`,`enabled`,`trashed`) VALUES (1,'Cash',1,0),(2,'Cheque',1,0),(3,'Account',1,0);

/*Table structure for table `ml_payment_type` */

DROP TABLE IF EXISTS `ml_payment_type`;

CREATE TABLE `ml_payment_type` (
  `payment_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `payment_type_title` VARCHAR(50) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payment_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_payment_type` */

INSERT  INTO `ml_payment_type`(`payment_type_id`,`payment_type_title`,`enabled`,`trashed`) VALUES (1,'Loan',1,0),(2,'Advance',1,0),(3,'Salary',1,0);

/*Table structure for table `ml_payrate` */

DROP TABLE IF EXISTS `ml_payrate`;

CREATE TABLE `ml_payrate` (
  `ml_payrate_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payrate` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_payrate_id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ml_payrate` */

INSERT  INTO `ml_payrate`(`ml_payrate_id`,`payrate`,`enabled`,`trashed`) VALUES (1,'Monthly',1,0),(2,'Hourly',1,0),(3,'Daily',1,0),(4,'Weekly',1,0),(5,'Yearly',1,0);

/*Table structure for table `ml_posting_location_list` */

DROP TABLE IF EXISTS `ml_posting_location_list`;

CREATE TABLE `ml_posting_location_list` (
  `posting_locations_list_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `posting_location` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`posting_locations_list_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_posting_location_list` */

/*Table structure for table `ml_posting_reason_list` */

DROP TABLE IF EXISTS `ml_posting_reason_list`;

CREATE TABLE `ml_posting_reason_list` (
  `posting_reason_list_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `posting_reason` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`posting_reason_list_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_posting_reason_list` */

/*Table structure for table `ml_program_list` */

DROP TABLE IF EXISTS `ml_program_list`;

CREATE TABLE `ml_program_list` (
  `ProgramID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Program` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProgramID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_program_list` */

/*Table structure for table `ml_project_status_type` */

DROP TABLE IF EXISTS `ml_project_status_type`;

CREATE TABLE `ml_project_status_type` (
  `pst_id` INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_type_title` VARCHAR(80) COLLATE utf8_bin NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`pst_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_project_status_type` */

INSERT  INTO `ml_project_status_type`(`pst_id`,`status_type_title`,`enabled`,`trashed`) VALUES (1,'Started',1,0),(2,'In Progress',1,0),(3,'Stopped',1,0),(4,'Completed',1,0);



/*Table structure for table `ml_province` */

DROP TABLE IF EXISTS `ml_province`;

CREATE TABLE `ml_province` (
  `province_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `province_name` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`province_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_province` */

/*Table structure for table `ml_qualification_type` */

DROP TABLE IF EXISTS `ml_qualification_type`;

CREATE TABLE `ml_qualification_type` (
  `qualification_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `qualification_title` VARCHAR(225) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`qualification_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ml_qualification_type` */

INSERT  INTO `ml_qualification_type`(`qualification_type_id`,`qualification_title`,`enabled`,`trashed`) VALUES (1,'Degree',1,0),(2,'Diploma',1,0),(3,'Certificate',1,0);

/*Table structure for table `ml_relation` */

DROP TABLE IF EXISTS `ml_relation`;

CREATE TABLE `ml_relation` (
  `relation_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `relation_name` VARCHAR(255) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`relation_id`)
) ENGINE=INNODB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `ml_relation` */

INSERT  INTO `ml_relation`(`relation_id`,`relation_name`,`trashed`,`enabled`) VALUES (1,'Friend',1,1),(2,'Brother',0,1),(3,'Uncle',0,1),(4,'Father',0,1);

/*Table structure for table `ml_religion` */

DROP TABLE IF EXISTS `ml_religion`;

CREATE TABLE `ml_religion` (
  `religion_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `religion_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`religion_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_religion` */

/*Table structure for table `ml_reporting_options` */

DROP TABLE IF EXISTS `ml_reporting_options`;

CREATE TABLE `ml_reporting_options` (
  `reporting_option_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reporting_option` VARCHAR(256) NOT NULL COMMENT 'direct, indirect, both',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`reporting_option_id`)
) ENGINE=INNODB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `ml_reporting_options` */

INSERT  INTO `ml_reporting_options`(`reporting_option_id`,`reporting_option`,`enabled`,`trashed`) VALUES (1,'Manual',1,1),(2,'Electronic',1,0),(7,'Direct Report',1,0),(8,'Indirect Report',1,0);

/*Table structure for table `ml_separation_type` */

DROP TABLE IF EXISTS `ml_separation_type`;

CREATE TABLE `ml_separation_type` (
  `separation_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `separation_type` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`separation_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ml_separation_type` */

INSERT  INTO `ml_separation_type`(`separation_type_id`,`separation_type`,`enabled`,`trashed`) VALUES (1,'Resignation',1,0),(2,'Retired',1,0);

/*Table structure for table `ml_shift` */

DROP TABLE IF EXISTS `ml_shift`;

CREATE TABLE `ml_shift` (
  `shift_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shift_name` VARCHAR(30) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`shift_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_shift` */

INSERT  INTO `ml_shift`(`shift_id`,`shift_name`,`enabled`,`trashed`) VALUES (1,'Day',1,0),(2,'Night',1,0),(3,'Mid Night',1,1);

/*Table structure for table `ml_skill_type` */

DROP TABLE IF EXISTS `ml_skill_type`;

CREATE TABLE `ml_skill_type` (
  `skill_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `skill_name` VARCHAR(45) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`skill_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ml_skill_type` */

INSERT  INTO `ml_skill_type`(`skill_type_id`,`skill_name`,`enabled`,`trashed`) VALUES (1,'php',0,0),(2,'mysql',1,0),(3,'Team Leader',1,0),(4,'Arts',1,1),(7,'AngularJS',1,1);

/*Table structure for table `ml_status` */

DROP TABLE IF EXISTS `ml_status`;

CREATE TABLE `ml_status` (
  `status_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_title` VARCHAR(256) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`status_id`)
) ENGINE=INNODB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `ml_status` */

INSERT  INTO `ml_status`(`status_id`,`status_title`,`enabled`,`trashed`) VALUES (1,'Pending',1,0),(2,'Approved',1,0),(3,'Declined',1,0);

/*Table structure for table `ml_total_month_hour` */

DROP TABLE IF EXISTS `ml_total_month_hour`;

CREATE TABLE `ml_total_month_hour` (
  `monthly_std_hrs_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `monthly_std_hrs` INT(10) UNSIGNED NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`monthly_std_hrs_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `ml_total_month_hour` */

/*Table structure for table `ml_transaction_types` */

DROP TABLE IF EXISTS `ml_transaction_types`;

CREATE TABLE `ml_transaction_types` (
  `ml_transaction_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_type` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_transaction_type_id`)
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ml_transaction_types` */

INSERT  INTO `ml_transaction_types`(`ml_transaction_type_id`,`transaction_type`,`enabled`,`trashed`) VALUES (1,'Deductions',1,0),(2,'Allowances',1,0),(3,'Advances',1,0),(4,'Expenses',1,0),(5,'Salary',1,0),(6,'Expense Disburment',1,0),(7,'Deduction Refund',1,0);

/*Table structure for table `ml_user_groups` */

DROP TABLE IF EXISTS `ml_user_groups`;

CREATE TABLE `ml_user_groups` (
  `user_group_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_group_title` VARCHAR(256) DEFAULT NULL,
  `enabled` INT(1) DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`user_group_id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ml_user_groups` */

INSERT  INTO `ml_user_groups`(`user_group_id`,`user_group_title`,`enabled`,`trashed`) VALUES (1,'Admin',1,0),(2,'HRM',1,0),(3,'Pay Roll',1,0),(4,'ESSP',1,0);

/*Table structure for table `ml_views` */

DROP TABLE IF EXISTS `ml_views`;

CREATE TABLE `ml_views` (
  `view_id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `View Name` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL,
  `view_method` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `Comments` TEXT COLLATE utf8_bin,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`view_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_views` */

/*Table structure for table `ml_year` */

DROP TABLE IF EXISTS `ml_year`;

CREATE TABLE `ml_year` (
  `ml_year_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `year` YEAR(4) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ml_year_id`)
) ENGINE=INNODB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `ml_year` */

INSERT  INTO `ml_year`(`ml_year_id`,`year`,`enabled`,`trashed`) VALUES (1,2000,1,0),(3,2001,1,0),(4,2002,1,0),(5,2003,1,0),(6,2004,1,0),(7,2005,1,0),(8,2006,1,0),(9,2007,1,0),(10,2008,1,0),(11,2009,1,0),(12,2010,1,0),(13,2011,1,0),(14,2012,1,0),(15,2013,1,0),(16,2014,1,0),(17,2015,1,0),(18,2016,1,1);

/* End Of Master Lists Tables..*/
/* Now Need To Work On the Other Big Tables */

/*Table structure for table `alerts_mailed` */

DROP TABLE IF EXISTS `alerts_mailed`;

CREATE TABLE `alerts_mailed` (
  `alerts_mailed_id` INT(26) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alert_id` INT(8) UNSIGNED NOT NULL,
  `alert_table_name` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `mailed` TINYINT(1) NOT NULL DEFAULT '0',
  `date_mailed` DATETIME NOT NULL,
  PRIMARY KEY (`alerts_mailed_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `alerts_mailed` */


/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employee_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_code` VARCHAR(256) DEFAULT NULL,
  `first_name` VARCHAR(256) DEFAULT NULL,
  `last_name` VARCHAR(256) DEFAULT NULL,
  `full_name` VARCHAR(256) NOT NULL,
  `father_name` VARCHAR(45) DEFAULT NULL,
  `CNIC` VARCHAR(15) DEFAULT NULL,
  `gender` INT(11) UNSIGNED DEFAULT NULL,
  `marital_status` INT(11) UNSIGNED DEFAULT NULL,
  `nationality` INT(11) DEFAULT NULL,
  `religion` INT(10) UNSIGNED DEFAULT NULL,
  `tax_number` VARCHAR(256) DEFAULT NULL,
  `date_of_birth` DATE DEFAULT NULL,
  `photograph` VARCHAR(255) DEFAULT NULL,
  `thumbnail` VARCHAR(500) DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `blood_group` VARCHAR(255) DEFAULT NULL,
  `disabled_people` VARCHAR(255) DEFAULT NULL,
  `eobi_no` VARCHAR(80) DEFAULT NULL,
  `insurance_no` VARCHAR(255) DEFAULT NULL COMMENT 'Insurance No',
  `enrolled` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `employee_code` (`employee_code`),
  KEY `gender` (`gender`),
  KEY `marital_status` (`marital_status`),
  KEY `nationality` (`nationality`),
  KEY `tax_number` (`tax_number`),
  KEY `tax_number_2` (`tax_number`),
  KEY `gender_2` (`gender`),
  KEY `religion` (`religion`),
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`nationality`) REFERENCES `ml_nationality` (`nationality_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`gender`) REFERENCES `ml_gender_type` (`gender_type_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_5` FOREIGN KEY (`marital_status`) REFERENCES `ml_marital_status` (`marital_status_id`) ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_6` FOREIGN KEY (`religion`) REFERENCES `ml_religion` (`religion_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `employee` */

/*Table structure for table `employment` */

DROP TABLE IF EXISTS `employment`;

CREATE TABLE `employment` (
  `employment_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `joining_date` DATE NOT NULL,
  `end_date` DATE DEFAULT NULL,
  `current` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `do_eos_settlement` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `settlement_payment_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`employment_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employment_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `employment` */



/*Table structure for table `allowance` */

DROP TABLE IF EXISTS `allowance`;

CREATE TABLE `allowance` (
  `allowance_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_allowance_type_id` INT(11) UNSIGNED NOT NULL,
  `allowance_amount` DECIMAL(10,2) NOT NULL,
  `month` INT(11) UNSIGNED DEFAULT NULL,
  `year` INT(11) UNSIGNED DEFAULT NULL,
  `pay_frequency` INT(11) UNSIGNED DEFAULT NULL,
  `effective_date` DATE NOT NULL,
  `date_rec_created` DATE NOT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `date_last_modified` DATE DEFAULT NULL,
  `note` TEXT,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`allowance_id`),
  KEY `ml_allowance_type_id` (`ml_allowance_type_id`),
  KEY `pay_frequency` (`pay_frequency`),
  KEY `approved_by` (`approved_by`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `status` (`status`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `employment` (`employment_id`),
  CONSTRAINT `allowance_ibfk_1` FOREIGN KEY (`ml_allowance_type_id`) REFERENCES `ml_allowance_type` (`ml_allowance_type_id`),
  CONSTRAINT `allowance_ibfk_3` FOREIGN KEY (`pay_frequency`) REFERENCES `ml_pay_frequency` (`ml_pay_frequency_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_4` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_7` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_8` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_ibfk_9` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE,
  CONSTRAINT `EmploymentID_allowance` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `allowance` */


/*Table structure for table `cheque_payments` */

DROP TABLE IF EXISTS `cheque_payments`;

CREATE TABLE `cheque_payments` (
  `cheque_payment_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cheque_no` VARCHAR(256) NOT NULL,
  `bank_name` VARCHAR(256) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`cheque_payment_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `cheque_payments` */


/*Table structure for table `cash_payment` */

DROP TABLE IF EXISTS `cash_payment`;

CREATE TABLE `cash_payment` (
  `cash_payment_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `received_by` VARCHAR(256) NOT NULL,
  `remarks` TEXT,
  `trashed` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`cash_payment_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `cash_payment` */


/*Table structure for table `emp_bank_account` */

DROP TABLE IF EXISTS `emp_bank_account`;

CREATE TABLE `emp_bank_account` (
  `bank_account_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) NOT NULL,
  `account_no` VARCHAR(45) NOT NULL,
  `account_title` VARCHAR(256) NOT NULL,
  `bank_name` VARCHAR(45) NOT NULL,
  `branch` VARCHAR(45) NOT NULL,
  `branch_code` VARCHAR(20) DEFAULT NULL,
  `active` TINYINT(1) DEFAULT '2',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`bank_account_id`),
  KEY `EmployeeID` (`employee_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `emp_bank_account` */


/*Table structure for table `payment_mode` */

DROP TABLE IF EXISTS `payment_mode`;

CREATE TABLE `payment_mode` (
  `payment_mode_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_mode_type_id` INT(11) UNSIGNED NOT NULL,
  `account_id` INT(11) UNSIGNED DEFAULT NULL,
  `cheque_id` INT(11) UNSIGNED DEFAULT NULL,
  `cash_id` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`payment_mode_id`),
  KEY `account_id` (`account_id`),
  KEY `cheque_id` (`cheque_id`),
  KEY `cash_id` (`cash_id`),
  KEY `payment_mode_type_id` (`payment_mode_type_id`),
  CONSTRAINT `payment_mode_ibfk_1` FOREIGN KEY (`payment_mode_type_id`) REFERENCES `ml_payment_mode_types` (`payment_mode_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_mode_ibfk_3` FOREIGN KEY (`cheque_id`) REFERENCES `cheque_payments` (`cheque_payment_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_mode_ibfk_4` FOREIGN KEY (`cash_id`) REFERENCES `cash_payment` (`cash_payment_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_mode_ibfk_5` FOREIGN KEY (`account_id`) REFERENCES `emp_bank_account` (`bank_account_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `payment_mode` */


/*Table structure for table `transaction` */

DROP TABLE IF EXISTS `transaction`;

CREATE TABLE `transaction` (
  `transaction_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `calender_year` YEAR(4) NOT NULL,
  `calender_month` VARCHAR(256) NOT NULL,
  `transaction_date` DATE NOT NULL,
  `transaction_time` TIME NOT NULL,
  `payment_mode` INT(11) UNSIGNED DEFAULT NULL,
  `transaction_type` INT(11) UNSIGNED DEFAULT NULL,
  `transaction_amount` DECIMAL(10,2) NOT NULL,
  `approval_date` DATE DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `operator_id` INT(11) DEFAULT NULL,
  `note` TEXT,
  `status` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `enabled` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_type` (`transaction_type`),
  KEY `payment_mode` (`payment_mode`),
  KEY `operator_id` (`operator_id`),
  KEY `status` (`status`),
  KEY `employment_id` (`employment_id`),
  CONSTRAINT `transaction_ibfk_3` FOREIGN KEY (`transaction_type`) REFERENCES `ml_transaction_types` (`ml_transaction_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `transaction_ibfk_5` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `transaction_ibfk_6` FOREIGN KEY (`payment_mode`) REFERENCES `payment_mode` (`payment_mode_id`) ON UPDATE CASCADE,
  CONSTRAINT `transaction_ibfk_7` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `transaction` */


/*Table structure for table `allowance_payment` */

DROP TABLE IF EXISTS `allowance_payment`;

CREATE TABLE `allowance_payment` (
  `allowance_payment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) UNSIGNED NOT NULL,
  `allowance_id` INT(11) NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`allowance_payment_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `allowance_id` (`allowance_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  CONSTRAINT `allowance_payment_ibfk_1` FOREIGN KEY (`allowance_id`) REFERENCES `allowance` (`allowance_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_payment_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_payment_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `allowance_payment_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `allowance_payment` */


/*Table structure for table `applicant_applied_jobs` */

DROP TABLE IF EXISTS `applicant_applied_jobs`;

CREATE TABLE `applicant_applied_jobs` (
  `appAppliedJobID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicantID` INT(10) UNSIGNED NOT NULL,
  `jobAdvertisementID` INT(8) UNSIGNED NOT NULL,
  `dateAppliedOn` DATETIME NOT NULL,
  `responseStatus` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appAppliedJobID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `applicant_applied_jobs` */

/*Table structure for table `applicant_experience` */

DROP TABLE IF EXISTS `applicant_experience`;

CREATE TABLE `applicant_experience` (
  `AppExpereinceID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ApplicantID` INT(11) DEFAULT NULL,
  `Exp_position` VARCHAR(250) COLLATE utf8_bin DEFAULT NULL,
  `Company` VARCHAR(250) COLLATE utf8_bin DEFAULT NULL,
  `FromDate` DATE DEFAULT NULL,
  `ToDate` DATE DEFAULT NULL,
  `DateCreated` DATE DEFAULT NULL,
  `Trashed` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`AppExpereinceID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `applicant_experience` */

/*Table structure for table `applicant_qualification` */

DROP TABLE IF EXISTS `applicant_qualification`;

CREATE TABLE `applicant_qualification` (
  `apql_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) COLLATE utf8_bin DEFAULT NULL,
  `qualification_type_id` INT(11) UNSIGNED DEFAULT NULL,
  `institute` VARCHAR(250) COLLATE utf8_bin DEFAULT NULL,
  `specialization` VARCHAR(250) COLLATE utf8_bin DEFAULT NULL,
  `comp_year` INT(6) DEFAULT NULL,
  `gpa_division` VARCHAR(100) COLLATE utf8_bin DEFAULT NULL,
  `applicant_id` INT(11) UNSIGNED NOT NULL,
  `trashed` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`apql_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `applicant_qualification` */

/*Table structure for table `applicant_skills` */

DROP TABLE IF EXISTS `applicant_skills`;

CREATE TABLE `applicant_skills` (
  `appSkillID` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicantID` INT(8) UNSIGNED NOT NULL,
  `SkillID` INT(6) UNSIGNED NOT NULL,
  `Duration` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL,
  `dateAdded` DATE NOT NULL,
  `trashed` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT '0',
  PRIMARY KEY (`appSkillID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `applicant_skills` */

/*Table structure for table `applicants` */

DROP TABLE IF EXISTS `applicants`;

CREATE TABLE `applicants` (
  `applicantID` INT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(60) COLLATE utf8_bin NOT NULL,
  `lastName` VARCHAR(60) COLLATE utf8_bin DEFAULT NULL,
  `fullName` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `fatherName` VARCHAR(120) COLLATE utf8_bin DEFAULT NULL,
  `username` VARCHAR(50) COLLATE utf8_bin NOT NULL,
  `CNIC` VARCHAR(20) COLLATE utf8_bin DEFAULT NULL,
  `password` VARCHAR(50) COLLATE utf8_bin NOT NULL,
  `contactNo` VARCHAR(20) COLLATE utf8_bin DEFAULT NULL,
  `officialEmailAddress` VARCHAR(80) COLLATE utf8_bin NOT NULL,
  `profileDateCreated` DATE DEFAULT NULL,
  `profileDateUpdated` DATE DEFAULT NULL,
  `attachment` VARCHAR(200) COLLATE utf8_bin DEFAULT NULL,
  `genderID` TINYINT(1) DEFAULT NULL,
  `applicantType` TINYINT(1) DEFAULT '1' COMMENT 'willGiveInfo That Has Applicant Been Select For Candidate or Further Employment',
  `currentAddress` VARCHAR(280) COLLATE utf8_bin DEFAULT NULL,
  `permanentAddress` TEXT COLLATE utf8_bin,
  `avatar` VARCHAR(300) COLLATE utf8_bin DEFAULT NULL,
  `residentPhone` VARCHAR(30) COLLATE utf8_bin DEFAULT NULL,
  `email_verification_code` VARCHAR(100) COLLATE utf8_bin DEFAULT NULL,
  `active_status` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`applicantID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `applicants` */

/*Table structure for table `assign_job` */

DROP TABLE IF EXISTS `assign_job`;

CREATE TABLE `assign_job` (
  `assign_job_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED NOT NULL,
  `project_id` INT(11) UNSIGNED NOT NULL,
  `ml_assign_task_id` INT(11) UNSIGNED NOT NULL,
  `start_date` DATE NOT NULL,
  `completion_date` DATE NOT NULL,
  `task_description` TEXT,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`assign_job_id`),
  KEY `ml_assign_task_id` (`ml_assign_task_id`),
  KEY `project_id` (`project_id`),
  KEY `employment_id` (`employment_id`),
  CONSTRAINT `assign_job_ibfk_1` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `assign_job_ibfk_1_employemnt_id` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `assign_job_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `ml_projects` (`project_id`) ON UPDATE CASCADE,
  CONSTRAINT `assign_job_ibfk_3` FOREIGN KEY (`ml_assign_task_id`) REFERENCES `ml_assign_task` (`ml_assign_task_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `assign_job` */

/*Table structure for table `attachment` */

DROP TABLE IF EXISTS `attachment`;

CREATE TABLE `attachment` (
  `attachment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `attached_file` VARCHAR(255) NOT NULL,
  `remarks` VARCHAR(70) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`attachment_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `attachment` */

/*Table structure for table `attendence` */

DROP TABLE IF EXISTS `attendence`;

CREATE TABLE `attendence` (
  `attendence_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `in_time` TIME DEFAULT NULL,
  `in_date` DATE DEFAULT NULL,
  `out_time` TIME DEFAULT NULL,
  `out_date` DATE DEFAULT NULL,
  `attendance_status_type` INT(11) UNSIGNED DEFAULT NULL,
  `ml_month_id` INT(10) UNSIGNED DEFAULT NULL,
  `ml_year_id` INT(10) UNSIGNED DEFAULT NULL,
  `work_week_id` INT(11) UNSIGNED DEFAULT NULL,
  `remarks` VARCHAR(256) DEFAULT NULL,
  `latitude` VARCHAR(255) DEFAULT NULL,
  `longitude` VARCHAR(255) DEFAULT NULL,
  `holiday` INT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`attendence_id`),
  UNIQUE KEY `unique_index` (`employee_id`,`in_date`,`ml_month_id`,`ml_year_id`,`attendance_status_type`),
  KEY `attendance_status_type` (`attendance_status_type`),
  KEY `employee_id` (`employee_id`),
  KEY `ml_month_id` (`ml_month_id`),
  KEY `ml_year_id` (`ml_year_id`),
  KEY `work_week_id` (`work_week_id`),
  CONSTRAINT `attendence_ibfk_1` FOREIGN KEY (`attendance_status_type`) REFERENCES `ml_attendance_status_type` (`attendance_status_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `attendence_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `attendence_ibfk_3` FOREIGN KEY (`ml_month_id`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `attendence_ibfk_4` FOREIGN KEY (`ml_year_id`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `attendence` */

/*Table structure for table `benefits` */

DROP TABLE IF EXISTS `benefits`;

CREATE TABLE `benefits` (
  `emp_benefit_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `benefit_type_id` INT(11) UNSIGNED NOT NULL,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `date_effective` DATE NOT NULL,
  `date_rec_created` DATE NOT NULL,
  `created_by` INT(11) UNSIGNED NOT NULL,
  `date_rec_modified` DATE DEFAULT NULL,
  `note` TEXT,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`emp_benefit_id`),
  KEY `status` (`status`),
  KEY `benefit_type_id` (`benefit_type_id`,`approved_by`,`created_by`,`modified_by`,`status`),
  KEY `EmploymentID` (`employment_id`),
  CONSTRAINT `benefits_ibfk_1` FOREIGN KEY (`benefit_type_id`) REFERENCES `ml_benefit_type` (`benefit_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `EmploymentID` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `benefits` */

/*Table structure for table `contract` */

DROP TABLE IF EXISTS `contract`;

CREATE TABLE `contract` (
  `contract_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED NOT NULL,
  `employment_type` INT(11) UNSIGNED NOT NULL,
  `employment_category` INT(11) UNSIGNED NOT NULL,
  `contract_start_date` DATE DEFAULT NULL,
  `contract_expiry_date` DATE DEFAULT NULL,
  `comments` VARCHAR(256) DEFAULT NULL,
  `contract_exp_alert` TINYINT(1) NOT NULL DEFAULT '1',
  `date_contract_expiry_alert` DATE DEFAULT NULL,
  `approved_date` DATE DEFAULT NULL,
  `approved_by` INT(11) DEFAULT NULL,
  `status` INT(11) DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `current` TINYINT(1) UNSIGNED DEFAULT '1',
  `prob_alert_dat` DATE DEFAULT NULL,
  `probation` VARCHAR(45) DEFAULT NULL,
  `extension` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contract_id`),
  KEY `employment_id` (`employment_id`),
  KEY `employment_type` (`employment_type`,`employment_category`),
  KEY `employment_type_2` (`employment_type`),
  KEY `employment_category` (`employment_category`),
  CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `contract_ibfk_2` FOREIGN KEY (`employment_type`) REFERENCES `ml_employment_type` (`employment_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `contract_ibfk_3` FOREIGN KEY (`employment_category`) REFERENCES `ml_job_category` (`job_category_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `contract` */

/*Table structure for table `current_contacts` */

DROP TABLE IF EXISTS `current_contacts`;

CREATE TABLE `current_contacts` (
  `contact_detail_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED DEFAULT NULL,
  `address` VARCHAR(60) NOT NULL,
  `city_village` VARCHAR(45) DEFAULT NULL,
  `District` INT(11) UNSIGNED DEFAULT NULL,
  `province` INT(11) UNSIGNED DEFAULT NULL,
  `home_phone` VARCHAR(15) NOT NULL,
  `mob_num` VARCHAR(15) DEFAULT NULL,
  `email_address` VARCHAR(256) NOT NULL,
  `office_phone` VARCHAR(15) DEFAULT NULL,
  `official_email` VARCHAR(256) DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_detail_id`),
  KEY `District` (`District`),
  KEY `province` (`province`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `current_contacts_ibfk_1` FOREIGN KEY (`District`) REFERENCES `ml_district` (`district_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `current_contacts_ibfk_2` FOREIGN KEY (`province`) REFERENCES `ml_province` (`province_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `current_contacts` */

/*Table structure for table `deduction` */

DROP TABLE IF EXISTS `deduction`;

CREATE TABLE `deduction` (
  `deduction_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_deduction_type_id` INT(11) NOT NULL,
  `deduction_amount` DECIMAL(10,2) NOT NULL,
  `month` INT(11) UNSIGNED DEFAULT NULL,
  `year` INT(11) UNSIGNED DEFAULT NULL,
  `deduction_frequency` INT(11) UNSIGNED NOT NULL,
  `eobi_no` VARCHAR(45) DEFAULT NULL,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `date_modified` DATE DEFAULT NULL,
  `note` TEXT,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `status` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`deduction_id`),
  KEY `modified_by` (`modified_by`),
  KEY `created_by` (`created_by`),
  KEY `deduction_frequency` (`deduction_frequency`),
  KEY `ml_deduction_type_id` (`ml_deduction_type_id`),
  KEY `status` (`status`),
  KEY `approved_by` (`approved_by`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `EmploymentID` (`employment_id`),
  CONSTRAINT `deduction_ibfk_10` FOREIGN KEY (`modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_2` FOREIGN KEY (`deduction_frequency`) REFERENCES `ml_pay_frequency` (`ml_pay_frequency_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_5` FOREIGN KEY (`ml_deduction_type_id`) REFERENCES `ml_deduction_type` (`ml_deduction_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_6` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_7` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_8` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_ibfk_9` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE,
  CONSTRAINT `employmentIDdeduction_ibfk_11` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `deduction` */

/*Table structure for table `deduction_processing` */

DROP TABLE IF EXISTS `deduction_processing`;

CREATE TABLE `deduction_processing` (
  `deduction_processing_id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `transaction_amount` DECIMAL(10,2) DEFAULT NULL,
  `deduction_id` INT(11) UNSIGNED DEFAULT NULL,
  `month` INT(11) UNSIGNED DEFAULT NULL,
  `year` INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`deduction_processing_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `deduction_id` (`deduction_id`),
  KEY `year` (`year`),
  KEY `month` (`month`),
  CONSTRAINT `deduction_processing_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_processing_ibfk_2` FOREIGN KEY (`deduction_id`) REFERENCES `deduction` (`deduction_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_processing_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `deduction_processing_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `deduction_processing` */

/*Table structure for table `dependents` */

DROP TABLE IF EXISTS `dependents`;

CREATE TABLE `dependents` (
  `dependent_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `dependent_name` VARCHAR(45) NOT NULL,
  `ml_relationship_id` INT(11) UNSIGNED NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `insurance_cover` TINYINT(1) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `illness` VARCHAR(255) DEFAULT NULL,
  `insurance_start_date` DATE DEFAULT NULL,
  PRIMARY KEY (`dependent_id`),
  KEY `ml_relationship_id` (`ml_relationship_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `dependents_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `dependents_ibfk_2` FOREIGN KEY (`ml_relationship_id`) REFERENCES `ml_relation` (`relation_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `dependents` */

/*Table structure for table `disciplinary_action_reports` */

DROP TABLE IF EXISTS `disciplinary_action_reports`;

CREATE TABLE `disciplinary_action_reports` (
  `DAreportID` INT(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employmentID` INT(6) UNSIGNED NOT NULL,
  `disciplineBreached` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `disciplineBreachedDescription` TEXT COLLATE utf8_bin COMMENT 'Any Description Related To Breached/Violated Discipline',
  `dateOfBreach` DATE NOT NULL,
  `reportedBy` INT(6) NOT NULL,
  `reportingDate` DATE NOT NULL,
  `investigatedBy` INT(6) DEFAULT NULL,
  `employeeFeedBack` TEXT COLLATE utf8_bin,
  `investigatorRemarks` TEXT COLLATE utf8_bin,
  `disciplinaryActionID` INT(3) DEFAULT NULL,
  `decisionDate` DATE DEFAULT NULL,
  `reviewedBy` INT(6) UNSIGNED DEFAULT NULL COMMENT 'Reviewed By',
  `reviewDate` DATE DEFAULT NULL,
  `reviewerRemarks` TEXT COLLATE utf8_bin COMMENT 'Reviewer Remarks',
  `status` TINYINT(6) UNSIGNED NOT NULL DEFAULT '0',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`DAreportID`)
) ENGINE=INNODB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `disciplinary_action_reports` */

INSERT  INTO `disciplinary_action_reports`(`DAreportID`,`employmentID`,`disciplineBreached`,`disciplineBreachedDescription`,`dateOfBreach`,`reportedBy`,`reportingDate`,`investigatedBy`,`employeeFeedBack`,`investigatorRemarks`,`disciplinaryActionID`,`decisionDate`,`reviewedBy`,`reviewDate`,`reviewerRemarks`,`status`,`trashed`) VALUES (1,28,'Fight In Office','Just Showing Description To Make Sure This Field is In Working Condition..','2015-03-25',1,'2015-03-18',1,'Employee is Sorry For His Behavious, According To Him He was having Some Hard Time Managing house Problems.','Misbehaviour Was Done By Him, Although He wants apolagize for his behaviour.',NULL,NULL,NULL,NULL,NULL,1,0),(8,15,'misbehaviour',NULL,'2015-03-17',1,'2015-03-21',1,'Employee is Sorry For His Behavious, According To Him He was having Some Hard Time Managing house Problems.','Misbehaviour Was Done By Him, Although He wants apolagize for his behaviour...\r\n',NULL,NULL,NULL,NULL,NULL,1,0);

/*Table structure for table `download` */

DROP TABLE IF EXISTS `download`;

CREATE TABLE `download` (
  `download_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attached_file` VARCHAR(1024) NOT NULL,
  `remarks` VARCHAR(1024) NOT NULL,
  `upload_date` DATE NOT NULL,
  `trahsed` VARCHAR(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`download_id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

/*Data for the table `download` */

/*Table structure for table `emergency_contacts` */

DROP TABLE IF EXISTS `emergency_contacts`;

CREATE TABLE `emergency_contacts` (
  `emergency_contact_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `cotact_person_name` VARCHAR(256) NOT NULL,
  `relationship` INT(11) UNSIGNED NOT NULL,
  `home_phone` VARCHAR(20) NOT NULL,
  `mobile_num` VARCHAR(20) NOT NULL,
  `work_phone` VARCHAR(20) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`emergency_contact_id`),
  UNIQUE KEY `employee_id` (`employee_id`),
  KEY `relationship` (`relationship`,`employee_id`),
  KEY `employee_id_2` (`employee_id`),
  CONSTRAINT `emergency_contacts_ibfk_1` FOREIGN KEY (`relationship`) REFERENCES `ml_relation` (`relation_id`) ON UPDATE CASCADE,
  CONSTRAINT `emergency_contacts_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `emergency_contacts` */

/*Table structure for table `emp_experience` */

DROP TABLE IF EXISTS `emp_experience`;

CREATE TABLE `emp_experience` (
  `experience_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `job_title` VARCHAR(256) NOT NULL,
  `ml_employment_type_id` INT(11) UNSIGNED NOT NULL,
  `from_date` DATE DEFAULT NULL,
  `to_date` DATE DEFAULT NULL,
  `total_dur` VARCHAR(256) DEFAULT NULL,
  `organization` VARCHAR(45) DEFAULT NULL,
  `comment` TEXT,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`experience_id`),
  KEY `employee_id` (`employee_id`),
  KEY `employee_id_2` (`employee_id`),
  KEY `ml_employment_type_id` (`ml_employment_type_id`),
  CONSTRAINT `emp_experience_ibfk_1` FOREIGN KEY (`ml_employment_type_id`) REFERENCES `ml_employment_type` (`employment_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `emp_experience_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `emp_experience` */

/*Table structure for table `emp_skills` */

DROP TABLE IF EXISTS `emp_skills`;

CREATE TABLE `emp_skills` (
  `skill_record_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employment_id` int(11) unsigned NOT NULL,
  `ml_skill_type_id` int(11) unsigned DEFAULT NULL,
  `experience_in_years` int(10) unsigned NOT NULL,
  `comments` text,
  `trashed` tinyint(1) DEFAULT '0',
  `ml_skill_level_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`skill_record_id`),
  KEY `ml_skill_type_id` (`ml_skill_type_id`),
  KEY `employee_id` (`employment_id`),
  CONSTRAINT `emp_skills_ibfk_1` FOREIGN KEY (`ml_skill_type_id`) REFERENCES `ml_skill_type` (`skill_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `emp_skills_ibfk_2` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

/*Data for the table `emp_skills` */

/*Table structure for table `employee_alerts` */

DROP TABLE IF EXISTS `employee_alerts`;

CREATE TABLE `employee_alerts` (
  `employee_alert_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(6) UNSIGNED NOT NULL,
  `alert_id` INT(6) UNSIGNED NOT NULL,
  `alert_text` VARCHAR(80) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`employee_alert_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `employee_alerts` */

/*Table structure for table `employee_contract_extensions` */

DROP TABLE IF EXISTS `employee_contract_extensions`;

CREATE TABLE `employee_contract_extensions` (
  `ece_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contract_id` INT(6) DEFAULT NULL,
  `e_start_date` DATE NOT NULL,
  `e_end_date` DATE NOT NULL,
  `e_exp_alert` TINYINT(1) NOT NULL DEFAULT '1',
  `e_exp_date` DATE DEFAULT NULL,
  `comments` TEXT COLLATE utf8_bin,
  `current` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ece_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `employee_contract_extensions` */

/*Table structure for table `employee_insurance` */

DROP TABLE IF EXISTS `employee_insurance`;

CREATE TABLE `employee_insurance` (
  `employee_insurance_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ml_insurance_id` INT(2) UNSIGNED DEFAULT NULL,
  `employment_id` INT(10) UNSIGNED DEFAULT NULL,
  `certificate_number` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `policy_number` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `effective_from` DATE DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`employee_insurance_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `employee_insurance` */

/*Table structure for table `employee_project` */

DROP TABLE IF EXISTS `employee_project`;

CREATE TABLE `employee_project` (
  `employee_project_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(12) UNSIGNED NOT NULL,
  `project_id` INT(11) UNSIGNED NOT NULL,
  `project_assign_date` DATE DEFAULT NULL,
  `project_revoke_date` DATE DEFAULT NULL,
  `current` TINYINT(1) DEFAULT '1',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `percentCharged` INT(3) DEFAULT NULL,
  `dateCreated` DATE DEFAULT NULL,
  PRIMARY KEY (`employee_project_id`),
  KEY `project_id` (`project_id`),
  KEY `employeeProjectEmploymentID` (`employment_id`),
  CONSTRAINT `employeeProjectEmploymentID` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `employee_project_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `ml_projects` (`project_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `employee_project` */

/*Table structure for table `employee_project_percentage` */

DROP TABLE IF EXISTS `employee_project_percentage`;

CREATE TABLE `employee_project_percentage` (
  `percentChargeID` INT(25) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employeeProjectID` INT(10) UNSIGNED NOT NULL,
  `percentCharge` INT(3) NOT NULL,
  `dateCreated` DATETIME NOT NULL,
  `dateUpdated` DATETIME DEFAULT NULL,
  `percentageToBeAppliedOn` DATE DEFAULT NULL,
  `CreatedBy` INT(8) NOT NULL,
  `UpdatedBy` INT(8) DEFAULT NULL,
  `trashed` INT(1) NOT NULL DEFAULT '0',
  `current` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`percentChargeID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `employee_project_percentage` */

/*Table structure for table `employees_total` */

DROP TABLE IF EXISTS `employees_total`;

CREATE TABLE `employees_total` (
  `TotalEmployeesTable` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `CurrentYear` YEAR(4) DEFAULT NULL,
  `Total` INT(4) UNSIGNED DEFAULT NULL,
  `GenderID` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`TotalEmployeesTable`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `employees_total` */

/*Table structure for table `eos` */

DROP TABLE IF EXISTS `eos`;

CREATE TABLE `eos` (
  `eos_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) DEFAULT NULL,
  `total_deductions` DECIMAL(10,2) DEFAULT NULL,
  `paid_amount` VARCHAR(50) COLLATE utf8_bin DEFAULT NULL,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `status` INT(11) UNSIGNED DEFAULT '1',
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approved_date` DATE DEFAULT NULL,
  `trashed` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`eos_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `created_by` (`created_by`),
  KEY `status` (`status`),
  KEY `approved_by` (`approved_by`),
  CONSTRAINT `approved_by_FK` FOREIGN KEY (`approved_by`) REFERENCES `employment` (`employment_id`),
  CONSTRAINT `created_by_FK` FOREIGN KEY (`created_by`) REFERENCES `employment` (`employment_id`),
  CONSTRAINT `status_FK` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`),
  CONSTRAINT `transaction_id_FK` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `eos` */

/*Table structure for table `essp_employee_notifications` */

DROP TABLE IF EXISTS `essp_employee_notifications`;

CREATE TABLE `essp_employee_notifications` (
  `notificationID` INT(50) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notification_text` TEXT COLLATE utf8_bin NOT NULL,
  `notification_detail_link` VARCHAR(300) COLLATE utf8_bin DEFAULT NULL,
  `employment_id` INT(11) DEFAULT NULL,
  `created_date` DATE DEFAULT NULL,
  PRIMARY KEY (`notificationID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `essp_employee_notifications` */

/*Table structure for table `expense_claims` */

DROP TABLE IF EXISTS `expense_claims`;

CREATE TABLE `expense_claims` (
  `expense_claim_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `expense_type` INT(11) UNSIGNED NOT NULL,
  `expense_date` DATE NOT NULL,
  `amount` DECIMAL(11,2) NOT NULL,
  `paid` TINYINT(1) NOT NULL DEFAULT '0',
  `month` INT(11) UNSIGNED DEFAULT NULL,
  `year` INT(11) UNSIGNED DEFAULT NULL,
  `file_name` VARCHAR(255) DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approval_date` DATE DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `note` TEXT,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `last_modify_date` DATE DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `status` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`expense_claim_id`),
  KEY `modified_by` (`modified_by`),
  KEY `created_by` (`created_by`),
  KEY `approved_by` (`approved_by`),
  KEY `expense_type` (`expense_type`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `employment` (`employment_id`),
  CONSTRAINT `expense_claims_ibfk_2` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_6` FOREIGN KEY (`expense_type`) REFERENCES `ml_expense_type` (`ml_expense_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_7` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_8` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_claims_ibfk_9` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `expense_claims` */

/*Table structure for table `expense_disbursment` */

DROP TABLE IF EXISTS `expense_disbursment`;

CREATE TABLE `expense_disbursment` (
  `expense_dis_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `expense_id` INT(11) NOT NULL,
  `transaction_id` INT(11) UNSIGNED NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `review_status` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`expense_dis_id`)
) ENGINE=MYISAM DEFAULT CHARSET=latin1;

/*Data for the table `expense_disbursment` */

/*Table structure for table `expense_payment` */

DROP TABLE IF EXISTS `expense_payment`;

CREATE TABLE `expense_payment` (
  `expense_payment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `expense_id` INT(11) NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`expense_payment_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `expense_id` (`expense_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  CONSTRAINT `expense_payment_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_payment_ibfk_2` FOREIGN KEY (`expense_id`) REFERENCES `expense_claims` (`expense_claim_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_payment_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `expense_payment_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `expense_payment` */


/*Table structure for table `salary` */

DROP TABLE IF EXISTS `salary`;

CREATE TABLE `salary` (
  `salary_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employement_id` INT(11) UNSIGNED NOT NULL,
  `ml_pay_grade` INT(11) UNSIGNED DEFAULT NULL,
  `ml_pay_frequency` INT(11) UNSIGNED NOT NULL,
  `ml_currency` INT(11) UNSIGNED NOT NULL,
  `ml_pay_rate_id` INT(11) UNSIGNED NOT NULL,
  `base_salary` DECIMAL(12,2) NOT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `last_modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `note` TEXT,
  `date_modified` DATE DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`salary_id`),
  KEY `ml_pay_rate_id` (`ml_pay_rate_id`),
  KEY `ml_currency` (`ml_currency`),
  KEY `ml_pay_frequency` (`ml_pay_frequency`),
  KEY `employee_id` (`employement_id`),
  KEY `ml_pay_grade` (`ml_pay_grade`),
  KEY `approved_by` (`approved_by`),
  KEY `created_by` (`created_by`),
  KEY `last_modified_by` (`last_modified_by`),
  KEY `status` (`status`),
  CONSTRAINT `salary_ibfk_10` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_2` FOREIGN KEY (`ml_pay_frequency`) REFERENCES `ml_pay_frequency` (`ml_pay_frequency_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_3` FOREIGN KEY (`ml_currency`) REFERENCES `ml_currency` (`ml_currency_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_4` FOREIGN KEY (`ml_pay_rate_id`) REFERENCES `ml_payrate` (`ml_payrate_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_5` FOREIGN KEY (`ml_pay_grade`) REFERENCES `ml_pay_grade` (`ml_pay_grade_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_6` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_7` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `salary_ibfk_8` FOREIGN KEY (`last_modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_ibfk_9` FOREIGN KEY (`employement_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `salary` */

/*Table structure for table `salary_payment` */

DROP TABLE IF EXISTS `salary_payment`;

CREATE TABLE `salary_payment` (
  `salary_payment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `salary_id` INT(11) UNSIGNED NOT NULL,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `transaction_amount` DECIMAL(10,2) DEFAULT NULL,
  `arears` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `paid_arears` DECIMAL(10,2) DEFAULT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  `status` INT(11) UNSIGNED NOT NULL DEFAULT '2',
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  PRIMARY KEY (`salary_payment_id`),
  KEY `salary_id` (`salary_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `status` (`status`,`approved_by`),
  CONSTRAINT `salary_payment_ibfk_1` FOREIGN KEY (`salary_id`) REFERENCES `salary` (`salary_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_payment_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_payment_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_payment_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `salary_payment` */


/*Table structure for table `fail_reviews` */

DROP TABLE IF EXISTS `fail_reviews`;

CREATE TABLE `fail_reviews` (
  `review_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salary_payment_id` INT(11) DEFAULT NULL,
  `review_date` DATE DEFAULT NULL,
  `reviewed_by` INT(11) UNSIGNED DEFAULT NULL,
  `remarks` TEXT,
  `status` TINYINT(1) DEFAULT '1',
  `fail_table` VARCHAR(80) DEFAULT NULL,
  `fail_id` INT(4) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `salary_payment_id` (`salary_payment_id`),
  KEY `reviewed_by` (`reviewed_by`),
  CONSTRAINT `reviewed_by` FOREIGN KEY (`reviewed_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `salary_payment_id` FOREIGN KEY (`salary_payment_id`) REFERENCES `salary_payment` (`salary_payment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `fail_reviews` */

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `feedback_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(10) UNSIGNED NOT NULL,
  `employer` INT(10) NOT NULL,
  `reviewer` TEXT,
  `comments` TEXT,
  `submit_date` DATE NOT NULL,
  `enabled` TINYINT(1) UNSIGNED DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`feedback_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `feedback` */

/*Table structure for table `goto_list` */

DROP TABLE IF EXISTS `goto_list`;

CREATE TABLE `goto_list` (
  `goto_list_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `ml_department_id` INT(11) UNSIGNED NOT NULL,
  `ml_desigination_id` INT(11) UNSIGNED NOT NULL,
  `resposible` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`goto_list_id`),
  KEY `employee_id` (`employee_id`),
  KEY `ml_department_id` (`ml_department_id`),
  KEY `ml_desigination_id` (`ml_desigination_id`),
  KEY `ml_desigination_id_2` (`ml_desigination_id`),
  CONSTRAINT `goto_list_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `goto_list_ibfk_2` FOREIGN KEY (`ml_department_id`) REFERENCES `ml_department` (`department_id`) ON UPDATE CASCADE,
  CONSTRAINT `goto_list_ibfk_3` FOREIGN KEY (`ml_desigination_id`) REFERENCES `ml_designations` (`ml_designation_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `goto_list` */

/*Table structure for table `hourly_time_sheet` */

DROP TABLE IF EXISTS `hourly_time_sheet`;

CREATE TABLE `hourly_time_sheet` (
  `hour_time_sheet_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `monthly_std_hrs_id` INT(11) UNSIGNED NOT NULL,
  `ml_month_id` INT(11) UNSIGNED NOT NULL,
  `ml_year_id` INT(11) UNSIGNED NOT NULL,
  `work_hours` INT(11) NOT NULL,
  `leave_hours` INT(11) NOT NULL,
  PRIMARY KEY (`hour_time_sheet_id`),
  KEY `ml_month_id` (`ml_month_id`),
  KEY `monthly_std_hrs_id` (`monthly_std_hrs_id`),
  KEY `employee_id` (`employee_id`),
  KEY `ml_year_id` (`ml_year_id`),
  CONSTRAINT `hourly_time_sheet_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `hourly_time_sheet_ibfk_2` FOREIGN KEY (`monthly_std_hrs_id`) REFERENCES `ml_total_month_hour` (`monthly_std_hrs_id`) ON UPDATE CASCADE,
  CONSTRAINT `hourly_time_sheet_ibfk_3` FOREIGN KEY (`ml_month_id`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `hourly_time_sheet_ibfk_4` FOREIGN KEY (`ml_year_id`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `hourly_time_sheet` */

/*Table structure for table `immigration` */

DROP TABLE IF EXISTS `immigration`;

CREATE TABLE `immigration` (
  `imigration_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED DEFAULT NULL,
  `passport_num` VARCHAR(25) NOT NULL,
  `visa_num` VARCHAR(25) NOT NULL,
  `visa_expiry` DATE NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`imigration_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `immigration_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `immigration` */

/*Table structure for table `increments` */

DROP TABLE IF EXISTS `increments`;

CREATE TABLE `increments` (
  `increment_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `increment_type_id` INT(11) UNSIGNED NOT NULL,
  `increment_amount` DECIMAL(10,2) NOT NULL,
  `date_effective` DATE DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approval_date` DATE DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `modify_date` DATE DEFAULT NULL,
  `note` TEXT,
  `last_modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`increment_id`),
  KEY `increment_type_id` (`increment_type_id`),
  KEY `approved_by` (`approved_by`),
  KEY `created_by` (`created_by`),
  KEY `last_modified_by` (`last_modified_by`),
  KEY `status` (`status`),
  KEY `employment_id` (`employment_id`),
  CONSTRAINT `employment_id_increment` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_ibfk_1` FOREIGN KEY (`increment_type_id`) REFERENCES `ml_increment_type` (`increment_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_ibfk_3` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_ibfk_5` FOREIGN KEY (`last_modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_ibfk_6` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `increments` */

/*Table structure for table `increments_processing` */

DROP TABLE IF EXISTS `increments_processing`;

CREATE TABLE `increments_processing` (
  `increments_processing_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) UNSIGNED NOT NULL,
  `increment_id` INT(10) UNSIGNED NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`increments_processing_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `increment_id` (`increment_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  CONSTRAINT `increments_processing_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_processing_ibfk_2` FOREIGN KEY (`increment_id`) REFERENCES `increments` (`increment_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_processing_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `increments_processing_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `increments_processing` */

/*Table structure for table `job_ad_required_skills` */

DROP TABLE IF EXISTS `job_ad_required_skills`;

CREATE TABLE `job_ad_required_skills` (
  `reqSkillID` INT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jobAdvertisementID` INT(8) UNSIGNED NOT NULL,
  `skillTypeID` INT(6) UNSIGNED NOT NULL,
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `dateCreated` DATE NOT NULL,
  `dateUpdated` DATE DEFAULT NULL,
  PRIMARY KEY (`reqSkillID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `job_ad_required_skills` */

/*Table structure for table `job_advertisement` */

DROP TABLE IF EXISTS `job_advertisement`;

CREATE TABLE `job_advertisement` (
  `advertisementID` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `JobAD_ID` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `description` TEXT COLLATE utf8_bin NOT NULL,
  `datePosted` DATE NOT NULL,
  `expiryDate` DATE DEFAULT NULL,
  `postedBy` INT(6) UNSIGNED NOT NULL,
  `availablePositions` INT(4) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `department` INT(3) DEFAULT NULL,
  `minAgeRange` INT(2) DEFAULT NULL,
  `maxAgeRange` INT(2) DEFAULT NULL,
  `salaryStartRange` INT(6) DEFAULT NULL,
  `salaryEndRange` INT(6) DEFAULT NULL,
  `minQualification` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL,
  `dateupdated` DATE DEFAULT NULL,
  `employmentTypeID` INT(3) UNSIGNED DEFAULT NULL,
  `minExperience` INT(2) DEFAULT NULL,
  `transportFacilitie` TINYINT(1) DEFAULT NULL,
  `cityID` INT(11) DEFAULT NULL,
  `referenceNo` INT(11) DEFAULT NULL,
  PRIMARY KEY (`advertisementID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `job_advertisement` */

/*Table structure for table `leave_application` */

DROP TABLE IF EXISTS `leave_application`;

CREATE TABLE `leave_application` (
  `application_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `entitlement_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_leave_type_id` INT(11) NOT NULL,
  `from_date` DATE DEFAULT NULL,
  `to_date` DATE DEFAULT NULL,
  `total_days` VARCHAR(255) DEFAULT NULL,
  `remark` TEXT,
  `status` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `note` TEXT,
  `application_date` DATE DEFAULT NULL,
  `short_leave` TINYINT(1) DEFAULT '2',
  `from_time` VARCHAR(45) DEFAULT NULL,
  `to_time` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`application_id`),
  KEY `status` (`status`),
  KEY `ml_leave_type_id` (`ml_leave_type_id`),
  KEY `employment_id` (`employment_id`),
  CONSTRAINT `employmentid_leave_application` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_application_ibfk_1` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_application_ibfk_3` FOREIGN KEY (`ml_leave_type_id`) REFERENCES `ml_leave_type` (`ml_leave_type_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `leave_application` */

/*Table structure for table `leave_approval` */

DROP TABLE IF EXISTS `leave_approval`;

CREATE TABLE `leave_approval` (
  `leave_approval_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `leave_application_id` INT(11) UNSIGNED DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approval_date` DATE DEFAULT NULL,
  `approved_from` DATE DEFAULT NULL,
  `approved_to` DATE DEFAULT NULL,
  `no_of_paid_days` INT(11) DEFAULT NULL,
  `no_of_un_paid_days` INT(11) DEFAULT NULL,
  `remarks` TEXT,
  `status` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `enabled` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `approved_from_time` VARCHAR(45) DEFAULT NULL,
  `approved_to_time` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`leave_approval_id`),
  KEY `leave_application_id` (`leave_application_id`),
  KEY `approved_by` (`approved_by`),
  KEY `status` (`status`),
  KEY `EmploymentID` (`employment_id`),
  CONSTRAINT `employment_idleaveapproval` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_approval_ibfk_2` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_approval_ibfk_3` FOREIGN KEY (`leave_application_id`) REFERENCES `leave_application` (`application_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `leave_approval` */

/*Table structure for table `leave_entitlement` */

DROP TABLE IF EXISTS `leave_entitlement`;

CREATE TABLE `leave_entitlement` (
  `leave_entitlement_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_leave_type_id` INT(11) DEFAULT NULL,
  `no_of_leaves_allocated` INT(11) NOT NULL,
  `no_of_leaves_entitled` INT(3) DEFAULT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `note` TEXT,
  `date_rec_modfied` DATE DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` INT(11) UNSIGNED DEFAULT '1',
  PRIMARY KEY (`leave_entitlement_id`),
  KEY `ml_leave_type_id` (`ml_leave_type_id`),
  KEY `approved_by` (`approved_by`,`modified_by`),
  KEY `status` (`status`),
  KEY `modified_by` (`modified_by`),
  KEY `employment_id` (`employment_id`),
  CONSTRAINT `employment_id_leave_entitlement` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_entitlement_ibfk_1` FOREIGN KEY (`ml_leave_type_id`) REFERENCES `ml_leave_type` (`ml_leave_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_entitlement_ibfk_2` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_entitlement_ibfk_3` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `leave_entitlement_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `leave_entitlement` */


/*Table structure for table `loan_advances` */

DROP TABLE IF EXISTS `loan_advances`;

CREATE TABLE `loan_advances` (
  `loan_advance_id` INT(11) NOT NULL AUTO_INCREMENT,
  `payment_type` INT(11) NOT NULL,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `amount` DECIMAL(11,2) NOT NULL,
  `paid` TINYINT(1) NOT NULL DEFAULT '0',
  `paid_back` TINYINT(1) DEFAULT '0',
  `month` INT(11) UNSIGNED DEFAULT NULL,
  `year` INT(11) UNSIGNED DEFAULT NULL,
  `balance` DECIMAL(11,2) DEFAULT '0.00',
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `date_of_approval` DATE DEFAULT NULL,
  `date_created` DATE DEFAULT NULL,
  `created_by` INT(11) UNSIGNED DEFAULT NULL,
  `modified_by` INT(11) UNSIGNED DEFAULT NULL,
  `note` TEXT,
  `last_modify_date` DATE DEFAULT NULL,
  `status` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`loan_advance_id`),
  KEY `approved_by` (`approved_by`),
  KEY `created_by` (`created_by`),
  KEY `status` (`status`),
  KEY `payment_type` (`payment_type`),
  KEY `modified_by` (`modified_by`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `employmentid` (`employment_id`),
  CONSTRAINT `employmentid_loan_advances_ibfk_10` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_2` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_5` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_6` FOREIGN KEY (`payment_type`) REFERENCES `ml_payment_type` (`payment_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_7` FOREIGN KEY (`modified_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_8` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advances_ibfk_9` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `loan_advances` */



/*Table structure for table `loan_advance_payback` */

DROP TABLE IF EXISTS `loan_advance_payback`;

CREATE TABLE `loan_advance_payback` (
  `loan_adv_payback_id` INT(11) NOT NULL AUTO_INCREMENT,
  `loan_advance_id` INT(11) NOT NULL,
  `paid_amount` DECIMAL(11,2) UNSIGNED DEFAULT NULL,
  `payment_mode_id` INT(11) UNSIGNED DEFAULT NULL,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  `review_status` TINYINT(1) DEFAULT '0',
  PRIMARY KEY (`loan_adv_payback_id`),
  KEY `payment_mode_id` (`payment_mode_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  KEY `loan_advance_id` (`loan_advance_id`),
  CONSTRAINT `loan_advance_payback_ibfk_1` FOREIGN KEY (`loan_advance_id`) REFERENCES `loan_advances` (`loan_advance_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payback_ibfk_3` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payback_ibfk_4` FOREIGN KEY (`payment_mode_id`) REFERENCES `payment_mode` (`payment_mode_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payback_ibfk_5` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payback_ibfk_6` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `loan_advance_payback` */

/*Table structure for table `loan_advance_payment` */

DROP TABLE IF EXISTS `loan_advance_payment`;

CREATE TABLE `loan_advance_payment` (
  `loan_advance_payment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` INT(11) UNSIGNED DEFAULT NULL,
  `loan_advance_id` INT(11) NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`loan_advance_payment_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `loan_advance_id` (`loan_advance_id`),
  KEY `month` (`month`),
  KEY `year` (`year`),
  CONSTRAINT `loan_advance_payment_ibfk_1` FOREIGN KEY (`loan_advance_id`) REFERENCES `loan_advances` (`loan_advance_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payment_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payment_ibfk_3` FOREIGN KEY (`month`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `loan_advance_payment_ibfk_4` FOREIGN KEY (`year`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `loan_advance_payment` */

/*Table structure for table `milestone` */

DROP TABLE IF EXISTS `milestone`;

CREATE TABLE `milestone` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `milestones` VARCHAR(250) COLLATE utf8_bin DEFAULT NULL,
  `estimated_date` DATE DEFAULT NULL,
  `actual_date` DATE DEFAULT NULL,
  `remarks` TEXT COLLATE utf8_bin,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `task_id` INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `milestone` */

/*Table structure for table `organization` */

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `organization_id` INT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ConfigID` INT(11) UNSIGNED NOT NULL,
  `org_name` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `phone` VARCHAR(30) COLLATE utf8_bin DEFAULT NULL,
  `email` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `mobile` VARCHAR(30) COLLATE utf8_bin DEFAULT NULL,
  `address` TEXT COLLATE utf8_bin,
  `website` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `city` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `country` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`organization_id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `organization_table` */

DROP TABLE IF EXISTS `organization_table`;

CREATE TABLE `organization_table` (
  `organization_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_organization_id` INT(6) DEFAULT NULL,
  `employee_id` INT(6) DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED DEFAULT NULL,
  `enabled` TINYINT(1) UNSIGNED DEFAULT NULL,
  `employee_order` INT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`organization_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `organization_table` */

/*Table structure for table `over_time` */

DROP TABLE IF EXISTS `over_time`;

CREATE TABLE `over_time` (
  `over_time_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `ml_month_id` INT(11) UNSIGNED NOT NULL,
  `ml_year_id` INT(11) UNSIGNED NOT NULL,
  `calculated_hours` VARCHAR(55) DEFAULT NULL,
  `sanction_hours` VARCHAR(55) DEFAULT NULL,
  PRIMARY KEY (`over_time_id`),
  KEY `employee_id` (`employee_id`),
  KEY `ml_month_id` (`ml_month_id`),
  KEY `ml_year_id` (`ml_year_id`),
  CONSTRAINT `over_time_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `over_time_ibfk_2` FOREIGN KEY (`ml_month_id`) REFERENCES `ml_month` (`ml_month_id`) ON UPDATE CASCADE,
  CONSTRAINT `over_time_ibfk_3` FOREIGN KEY (`ml_year_id`) REFERENCES `ml_year` (`ml_year_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `over_time` */

/*Table structure for table `parexons_hrms_sessions` */

DROP TABLE IF EXISTS `parexons_hrms_sessions`;

CREATE TABLE `parexons_hrms_sessions` (
  `session_id` VARCHAR(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` VARCHAR(45) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` VARCHAR(120) COLLATE utf8_bin NOT NULL,
  `last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` TEXT COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `parexons_hrms_sessions` */

/*Table structure for table `performance_evaluation` */

DROP TABLE IF EXISTS `performance_evaluation`;

CREATE TABLE `performance_evaluation` (
  `performance_evaluation_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_assignment_id` INT(11) UNSIGNED NOT NULL,
  `ml_kpi_type_id` INT(11) NOT NULL,
  `percent_achieved` INT(11) DEFAULT NULL,
  `evaluated_by` INT(11) UNSIGNED DEFAULT NULL,
  `date` DATE DEFAULT NULL,
  `remarks` TEXT,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`performance_evaluation_id`),
  KEY `job_assignment_id` (`job_assignment_id`),
  KEY `ml_kpi_type_id` (`ml_kpi_type_id`),
  CONSTRAINT `performance_evaluation_ibfk_1` FOREIGN KEY (`ml_kpi_type_id`) REFERENCES `ml_kpi` (`ml_kpi_id`) ON UPDATE CASCADE,
  CONSTRAINT `performance_evaluation_ibfk_2` FOREIGN KEY (`job_assignment_id`) REFERENCES `assign_job` (`assign_job_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `performance_evaluation` */

/*Table structure for table `permanant_contacts` */

DROP TABLE IF EXISTS `permanant_contacts`;

CREATE TABLE `permanant_contacts` (
  `perm_contact_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `address` VARCHAR(256) NOT NULL,
  `city_village` VARCHAR(256) NOT NULL,
  `district` INT(11) UNSIGNED NOT NULL,
  `province` INT(11) UNSIGNED NOT NULL,
  `phone` VARCHAR(256) NOT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`perm_contact_id`),
  KEY `district` (`district`,`province`,`employee_id`),
  KEY `employee_id` (`employee_id`),
  KEY `district_2` (`district`),
  KEY `province` (`province`),
  CONSTRAINT `permanant_contacts_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `permanant_contacts_ibfk_2` FOREIGN KEY (`district`) REFERENCES `ml_district` (`district_id`) ON UPDATE CASCADE,
  CONSTRAINT `permanant_contacts_ibfk_3` FOREIGN KEY (`province`) REFERENCES `ml_province` (`province_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `permanant_contacts` */

/*Table structure for table `position_management` */

DROP TABLE IF EXISTS `position_management`;

CREATE TABLE `position_management` (
  `position_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employement_id` INT(11) UNSIGNED NOT NULL,
  `ml_pay_grade_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_designation_id` INT(11) UNSIGNED DEFAULT NULL,
  `job_specifications` TEXT,
  `from_date` DATE DEFAULT NULL,
  `to_date` DATE DEFAULT NULL,
  `current` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approval_date` DATE DEFAULT NULL,
  `note` TEXT,
  `date_created` DATE DEFAULT NULL,
  `status` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`position_id`),
  KEY `ml_pay_grade_id` (`ml_pay_grade_id`),
  KEY `ml_designation_id` (`ml_designation_id`),
  KEY `approved_by` (`approved_by`),
  KEY `employement_id` (`employement_id`),
  KEY `status` (`status`),
  CONSTRAINT `position_management_ibfk_2` FOREIGN KEY (`ml_pay_grade_id`) REFERENCES `ml_pay_grade` (`ml_pay_grade_id`) ON UPDATE CASCADE,
  CONSTRAINT `position_management_ibfk_3` FOREIGN KEY (`ml_designation_id`) REFERENCES `ml_designations` (`ml_designation_id`) ON UPDATE CASCADE,
  CONSTRAINT `position_management_ibfk_4` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `position_management_ibfk_5` FOREIGN KEY (`employement_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE,
  CONSTRAINT `position_management_ibfk_6` FOREIGN KEY (`status`) REFERENCES `ml_status` (`status_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `position_management` */

/*Table structure for table `posting` */

DROP TABLE IF EXISTS `posting`;

CREATE TABLE `posting` (
  `posting_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employement_id` INT(11) UNSIGNED NOT NULL,
  `poting_start_date` DATE NOT NULL,
  `posting_end_date` DATE DEFAULT NULL,
  `ml_department_id` INT(11) UNSIGNED NOT NULL,
  `ml_branch_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_city_id` INT(11) UNSIGNED NOT NULL,
  `location` INT(11) UNSIGNED NOT NULL,
  `shift` INT(11) UNSIGNED NOT NULL,
  `approved_by` INT(10) UNSIGNED DEFAULT NULL,
  `date_approved` DATE DEFAULT NULL,
  `current` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `reason_transfer` INT(11) UNSIGNED DEFAULT NULL,
  `transfer_remarks` VARCHAR(500) DEFAULT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `status` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `duration` INT(11) DEFAULT NULL,
  PRIMARY KEY (`posting_id`),
  KEY `ml_department_id` (`ml_department_id`),
  KEY `ml_branch_id` (`ml_branch_id`),
  KEY `ml_city_id` (`ml_city_id`),
  KEY `shift` (`shift`),
  KEY `shift_2` (`shift`),
  KEY `employement_id` (`employement_id`),
  KEY `approved_by` (`approved_by`),
  KEY `reason_transfer` (`reason_transfer`),
  CONSTRAINT `posting_ibfk_10` FOREIGN KEY (`approved_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `posting_ibfk_4` FOREIGN KEY (`ml_department_id`) REFERENCES `ml_department` (`department_id`) ON UPDATE CASCADE,
  CONSTRAINT `posting_ibfk_5` FOREIGN KEY (`ml_branch_id`) REFERENCES `ml_branch` (`branch_id`) ON UPDATE CASCADE,
  CONSTRAINT `posting_ibfk_6` FOREIGN KEY (`ml_city_id`) REFERENCES `ml_city` (`city_id`) ON UPDATE CASCADE,
  CONSTRAINT `posting_ibfk_8` FOREIGN KEY (`shift`) REFERENCES `ml_shift` (`shift_id`) ON UPDATE CASCADE,
  CONSTRAINT `posting_ibfk_9` FOREIGN KEY (`employement_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `posting` */

/*Table structure for table `privileges` */

DROP TABLE IF EXISTS `privileges`;

CREATE TABLE `privileges` (
  `user_privilege_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `view_only` TINYINT(1) UNSIGNED NOT NULL,
  `modify` TINYINT(1) UNSIGNED NOT NULL,
  `create_new_record` TINYINT(1) UNSIGNED NOT NULL,
  `print` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_privilege_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `privileges` */

/*Table structure for table `qualification` */

DROP TABLE IF EXISTS `qualification`;

CREATE TABLE `qualification` (
  `qualification_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `qualification_type_id` INT(11) UNSIGNED NOT NULL,
  `qualification` VARCHAR(50) NOT NULL,
  `institute` VARCHAR(70) NOT NULL,
  `major_specialization` VARCHAR(45) DEFAULT NULL,
  `year` VARCHAR(255) NOT NULL,
  `gpa_score` VARCHAR(10) DEFAULT NULL,
  `duration` VARCHAR(50) DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`qualification_id`),
  KEY `qualification_type_id` (`qualification_type_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `qualification_ibfk_1` FOREIGN KEY (`qualification_type_id`) REFERENCES `ml_qualification_type` (`qualification_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `qualification_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `qualification` */

/*Table structure for table `reporting_heirarchy` */

DROP TABLE IF EXISTS `reporting_heirarchy`;

CREATE TABLE `reporting_heirarchy` (
  `report_heirarchy_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `for_employee` INT(10) UNSIGNED DEFAULT NULL,
  `employeed_id` INT(11) UNSIGNED DEFAULT NULL,
  `ml_reporting_heirarchy_id` INT(11) UNSIGNED NOT NULL,
  `reporting_authority_id` INT(11) UNSIGNED DEFAULT NULL,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `reporting_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`report_heirarchy_id`),
  KEY `employeed_id` (`employeed_id`,`ml_reporting_heirarchy_id`,`reporting_authority_id`),
  KEY `employeed_id_2` (`employeed_id`),
  KEY `ml_reporting_heirarchy_id` (`ml_reporting_heirarchy_id`),
  KEY `reporting_authority_id` (`reporting_authority_id`),
  CONSTRAINT `reporting_heirarchy_ibfk_1` FOREIGN KEY (`employeed_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `reporting_heirarchy_ibfk_2` FOREIGN KEY (`ml_reporting_heirarchy_id`) REFERENCES `ml_reporting_options` (`reporting_option_id`) ON UPDATE CASCADE,
  CONSTRAINT `reporting_heirarchy_ibfk_3` FOREIGN KEY (`reporting_authority_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `reporting_heirarchy` */

/*Table structure for table `seperation_management` */

DROP TABLE IF EXISTS `seperation_management`;

CREATE TABLE `seperation_management` (
  `seperation_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `ml_seperation_type` INT(11) UNSIGNED DEFAULT NULL,
  `date` DATE NOT NULL,
  `reason` INT(11) UNSIGNED NOT NULL,
  `note` TEXT NOT NULL,
  `approved_by` INT(11) UNSIGNED DEFAULT NULL,
  `approved_date` DATE DEFAULT NULL,
  `status` TINYINT(1) DEFAULT '1',
  `trashed` TINYINT(1) DEFAULT '0',
  `enabled` TINYINT(1) DEFAULT '1',
  `rejoin` TINYINT(1) DEFAULT '1',
  PRIMARY KEY (`seperation_id`),
  KEY `ml_seperation_type` (`ml_seperation_type`),
  KEY `employee_id` (`employee_id`),
  KEY `reason` (`reason`),
  CONSTRAINT `seperation_management_ibfk_1` FOREIGN KEY (`ml_seperation_type`) REFERENCES `ml_separation_type` (`separation_type_id`) ON UPDATE CASCADE,
  CONSTRAINT `seperation_management_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE,
  CONSTRAINT `seperation_management_ibfk_3` FOREIGN KEY (`reason`) REFERENCES `ml_posting_reason_list` (`posting_reason_list_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `seperation_management` */

/*Table structure for table `shortlisted_interview` */

DROP TABLE IF EXISTS `shortlisted_interview`;

CREATE TABLE `shortlisted_interview` (
  `shtintr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicantID` INT(11) UNSIGNED DEFAULT NULL,
  `AdvertisementID` INT(11) UNSIGNED DEFAULT NULL,
  `present` TINYINT(1) DEFAULT '0',
  `feedback` TEXT COLLATE utf8_bin,
  `Score` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `feedback_by` INT(11) UNSIGNED DEFAULT NULL,
  `feedback_date` DATE DEFAULT NULL,
  `trashed` TINYINT(1) DEFAULT '0',
  `status` INT(11) UNSIGNED DEFAULT NULL,
  `call_date` DATE DEFAULT NULL,
  `InterviewDate` DATE DEFAULT NULL,
  PRIMARY KEY (`shtintr_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `shortlisted_interview` */

/*Table structure for table `task_progress` */

DROP TABLE IF EXISTS `task_progress`;

CREATE TABLE `task_progress` (
  `tp_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `milestone_id` INT(11) UNSIGNED DEFAULT NULL,
  `reported_date` DATE DEFAULT NULL,
  `percent_achieved` INT(11) DEFAULT NULL,
  `reported_by` INT(11) UNSIGNED DEFAULT NULL,
  `remarks` TEXT COLLATE utf8_bin,
  PRIMARY KEY (`tp_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `task_progress` */

/*Table structure for table `timesheet` */

DROP TABLE IF EXISTS `timesheet`;

CREATE TABLE `timesheet` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employment_id` INT(11) UNSIGNED DEFAULT NULL,
  `date_created` DATETIME DEFAULT NULL,
  `date_updated` DATETIME DEFAULT NULL,
  `timesheet_status_id` TINYINT(2) DEFAULT '0',
  `month` VARCHAR(2) COLLATE utf8_bin DEFAULT NULL,
  `year` YEAR(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `EmploymentID` (`employment_id`),
  CONSTRAINT `employment_timesheet` FOREIGN KEY (`employment_id`) REFERENCES `employment` (`employment_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `timesheet` */

/*Table structure for table `timesheet_details` */

DROP TABLE IF EXISTS `timesheet_details`;

CREATE TABLE `timesheet_details` (
  `timesheetDetails_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timesheet_id` INT(11) UNSIGNED DEFAULT NULL,
  `project_id` INT(11) UNSIGNED DEFAULT NULL,
  `date` DATE DEFAULT NULL,
  `hours` DECIMAL(10,0) DEFAULT NULL,
  PRIMARY KEY (`timesheetDetails_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `timesheet_details` */

/*Table structure for table `training` */

DROP TABLE IF EXISTS `training`;

CREATE TABLE `training` (
  `training_record_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED NOT NULL,
  `training_name` VARCHAR(256) DEFAULT NULL,
  `frm_date` DATE DEFAULT NULL,
  `to_date` DATE DEFAULT NULL,
  `Institute` VARCHAR(256) DEFAULT NULL,
  `tdays` INT(11) DEFAULT NULL,
  `description` TEXT,
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`training_record_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `training_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `training` */

/*Table structure for table `training_list` */

DROP TABLE IF EXISTS `training_list`;

CREATE TABLE `training_list` (
  `training_list_id` INT(11) NOT NULL AUTO_INCREMENT,
  `training_id` INT(11) NOT NULL,
  `training_name` VARCHAR(45) NOT NULL,
  `training_description` VARCHAR(70) NOT NULL,
  `training_date` DATE NOT NULL,
  PRIMARY KEY (`training_list_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `training_list` */

/*Table structure for table `trash_store` */

DROP TABLE IF EXISTS `trash_store`;

CREATE TABLE `trash_store` (
  `trash_store_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `record_id` INT(11) UNSIGNED NOT NULL,
  `table_id_name` VARCHAR(55) DEFAULT NULL,
  `table_name` VARCHAR(256) DEFAULT NULL,
  `trash_date` DATE DEFAULT NULL,
  `trashed_by` INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`trash_store_id`),
  KEY `trashed_by` (`trashed_by`),
  CONSTRAINT `trash_store_ibfk_1` FOREIGN KEY (`trashed_by`) REFERENCES `employee` (`employee_id`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `trash_store` */

/*Table structure for table `user_account` */

DROP TABLE IF EXISTS `user_account`;

CREATE TABLE `user_account` (
  `user_account_id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) UNSIGNED DEFAULT NULL,
  `user_name` VARCHAR(256) DEFAULT NULL,
  `employee_email` VARCHAR(500) NOT NULL,
  `password` VARCHAR(33) NOT NULL,
  `user_group` INT(11) UNSIGNED DEFAULT NULL,
  `acct_creation_date` DATE NOT NULL,
  `last_modify_date` DATE DEFAULT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_account_id`),
  KEY `user_group` (`user_group`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `user_account_ibfk_2` FOREIGN KEY (`user_group`) REFERENCES `ml_user_groups` (`user_group_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `user_account` */

/*Table structure for table `user_groups_privileges` */

DROP TABLE IF EXISTS `user_groups_privileges`;

CREATE TABLE `user_groups_privileges` (
  `user_group_privilege_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_group_id` INT(11) UNSIGNED NOT NULL,
  `ml_module_list_id` INT(11) UNSIGNED DEFAULT NULL,
  `view_only` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `modify` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `create_new_record` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `print` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_group_privilege_id`),
  KEY `user_group_id_2` (`user_group_id`),
  KEY `ml_module_list_id` (`ml_module_list_id`),
  KEY `user_group_id` (`user_group_id`),
  CONSTRAINT `user_groups_privileges_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `ml_user_groups` (`user_group_id`) ON UPDATE CASCADE,
  CONSTRAINT `user_groups_privileges_ibfk_3` FOREIGN KEY (`ml_module_list_id`) REFERENCES `ml_module_list` (`ml_module_id`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user_groups_privileges` */

INSERT  INTO `user_groups_privileges`(`user_group_privilege_id`,`user_group_id`,`ml_module_list_id`,`view_only`,`modify`,`create_new_record`,`print`,`enabled`,`trashed`) VALUES (1,1,2,1,1,1,1,1,0),(2,3,2,1,1,1,1,1,0),(3,4,3,1,1,1,1,1,0),(4,1,1,1,1,1,1,1,0),(5,1,5,1,1,1,1,1,0),(6,2,3,1,1,1,1,1,0),(7,2,1,1,1,1,1,1,0),(8,3,3,1,1,1,1,1,0);

/*Table structure for table `users_login_logs` */

DROP TABLE IF EXISTS `users_login_logs`;

CREATE TABLE `users_login_logs` (
  `login_log_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(6) UNSIGNED NOT NULL,
  `employee_id` INT(6) UNSIGNED NOT NULL,
  `session_id` VARCHAR(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `loginDateTime` DATETIME DEFAULT NULL,
  `logoutDateTime` DATETIME DEFAULT NULL,
  `loginSuccessType` INT(1) UNSIGNED DEFAULT '1',
  `current` INT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`login_log_id`)
) ENGINE=INNODB AUTO_INCREMENT=717 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `users_login_logs` */

INSERT  INTO `users_login_logs`(`login_log_id`,`user_id`,`employee_id`,`session_id`,`loginDateTime`,`logoutDateTime`,`loginSuccessType`,`current`) VALUES (573,1,1,'1-1427708434','2015-03-30 14:40:34','2015-03-30 14:40:59',1,0),(574,1,1,'1-1427708746','2015-03-30 14:45:46','2015-03-30 16:04:20',1,0),(575,1,1,'1-1427713587','2015-03-30 16:06:27','2015-03-30 16:07:08',1,0),(576,1,1,'1-1427713689','2015-03-30 16:08:09','2015-03-30 17:12:06',1,0),(577,1,1,'1-1427774792','2015-03-31 09:06:32','2015-03-31 10:14:06',1,0),(578,1,1,'1-1427775163','2015-03-31 09:12:43','2015-03-31 11:32:34',1,0),(579,1,1,'1-1427777458','2015-03-31 09:50:58',NULL,1,1),(580,1,1,'1-1427779037','2015-03-31 10:17:17','2015-03-31 10:17:22',1,0),(581,1,1,'1-1427779447','2015-03-31 10:24:07','2015-03-31 10:24:10',1,0),(582,1,1,'1-1427779712','2015-03-31 10:28:32','2015-03-31 10:28:41',1,0),(583,1,1,'1-1427780942','2015-03-31 10:49:02','2015-03-31 12:08:22',1,0),(584,1,1,'1-1427780942','2015-03-31 10:49:02','2015-03-31 12:08:22',1,0),(585,1,1,'1-1427781959','2015-03-31 11:05:59','2015-03-31 11:06:09',1,0),(586,1,1,'1-1427782057','2015-03-31 11:07:37','2015-03-31 11:15:13',1,0),(587,1,1,'1-1427783224','2015-03-31 11:27:04','2015-03-31 11:27:13',1,0),(588,1,1,'1-1427783262','2015-03-31 11:27:42','2015-03-31 11:49:17',1,0),(589,1,1,'1-1427784677','2015-03-31 11:51:17',NULL,1,1),(590,1,1,'1-1427785382','2015-03-31 12:03:02','2015-03-31 12:05:25',1,0),(591,1,1,'1-1427785669','2015-03-31 12:07:49','2015-03-31 13:15:11',1,0),(592,1,1,'1-1427792317','2015-03-31 13:58:37','2015-03-31 14:36:25',1,0),(593,1,1,'1-1427794592','2015-03-31 14:36:32',NULL,1,1),(594,1,1,'1-1427800671','2015-03-31 16:17:51',NULL,1,1),(595,1,1,'1-1427805829','2015-03-31 17:43:49',NULL,1,1),(596,1,1,'1-1427861486','2015-04-01 09:11:26','2015-04-01 09:46:28',1,0),(597,1,1,'1-1427861721','2015-04-01 09:15:21','2015-04-01 09:53:51',1,0),(598,1,1,'1-1427863498','2015-04-01 09:44:58','2015-04-01 09:45:03',1,0),(599,1,1,'1-1427863524','2015-04-01 09:45:24','2015-04-01 09:54:37',1,0),(600,1,1,'1-1427863725','2015-04-01 09:48:45','2015-04-01 09:55:39',1,0),(601,1,1,'1-1427864168','2015-04-01 09:56:08','2015-04-01 15:36:00',1,0),(602,1,1,'1-1427868629','2015-04-01 11:10:29','2015-04-01 11:54:13',1,0),(603,1,1,'1-1427872761','2015-04-01 12:19:21','2015-04-01 16:06:04',1,0),(604,1,1,'1-1427881889','2015-04-01 14:51:29',NULL,1,1),(605,1,1,'1-1427881897','2015-04-01 14:51:37',NULL,1,1),(606,1,1,'1-1427885648','2015-04-01 15:54:08','2015-04-01 15:57:50',1,0),(607,1,1,'1-1427885906','2015-04-01 15:58:26','2015-04-01 16:27:29',1,0),(608,1,1,'1-1427887153','2015-04-01 16:19:13',NULL,1,1),(609,1,1,'1-1427887801','2015-04-01 16:30:01','2015-04-01 16:59:38',1,0),(610,1,1,'1-1427889739','2015-04-01 17:02:19','2015-04-01 17:02:58',1,0),(611,1,1,'1-1427889887','2015-04-01 17:04:47',NULL,1,1),(612,1,1,'1-1427948047','2015-04-02 09:14:07',NULL,1,1),(613,1,1,'1-1427948476','2015-04-02 09:21:16',NULL,1,1),(614,1,1,'1-1427949122','2015-04-02 09:32:02',NULL,1,1),(615,1,1,'1-1427952702','2015-04-02 10:31:42','2015-04-02 11:18:41',1,0),(616,1,1,'1-1427956592','2015-04-02 11:36:32',NULL,1,1),(617,1,1,'1-1427966895','2015-04-02 14:28:15','2015-04-02 15:17:16',1,0),(618,1,1,'1-1427968223','2015-04-02 14:50:23','2015-04-02 14:58:47',1,0),(619,1,1,'1-1427971464','2015-04-02 15:44:24','2015-04-02 16:35:54',1,0),(620,1,1,'1-1427975275','2015-04-02 16:47:55',NULL,1,1),(621,1,1,'1-1427975584','2015-04-02 16:53:04',NULL,1,1),(622,1,1,'1-1428033129','2015-04-03 08:52:09','2015-04-03 09:39:34',1,0),(623,1,1,'1-1428034815','2015-04-03 09:20:15','2015-04-03 09:21:22',1,0),(624,1,1,'1-1428034976','2015-04-03 09:22:56','2015-04-03 12:02:41',1,0),(625,1,1,'1-1428036040','2015-04-03 09:40:40',NULL,1,1),(626,1,1,'1-1428036069','2015-04-03 09:41:09','2015-04-03 09:51:35',1,0),(627,1,1,'1-1428037398','2015-04-03 10:03:18',NULL,1,1),(628,1,1,'1-1428053317','2015-04-03 14:28:37','2015-04-03 14:59:20',1,0),(629,1,1,'1-1428056381','2015-04-03 15:19:41',NULL,1,1),(630,1,1,'1-1428120762','2015-04-04 09:12:42','2015-04-04 09:37:23',1,0),(631,1,1,'1-1428133964','2015-04-04 12:52:44',NULL,1,1),(632,1,1,'1-1428137249','2015-04-04 13:47:29','2015-04-04 14:26:06',1,0),(633,1,1,'1-1428139885','2015-04-04 14:31:25',NULL,1,1),(634,1,1,'1-1428295185','2015-04-06 09:39:45','2015-04-06 09:40:44',1,0),(635,1,1,'1-1428295250','2015-04-06 09:40:50','2015-04-06 09:47:49',1,0),(636,1,1,'1-1428295673','2015-04-06 09:47:53',NULL,1,1),(637,1,1,'1-1428296031','2015-04-06 09:53:51',NULL,1,1),(638,1,1,'1-1428299482','2015-04-06 10:51:22',NULL,1,1),(639,1,1,'1-1428302075','2015-04-06 11:34:35',NULL,1,1),(640,1,1,'1-1428380454','2015-04-07 09:20:54','2015-04-07 11:30:30',1,0),(641,1,1,'1-1428380670','2015-04-07 09:24:30',NULL,1,1),(642,1,1,'1-1428389772','2015-04-07 11:56:12',NULL,1,1),(643,1,1,'1-1428390018','2015-04-07 12:00:18',NULL,1,1),(644,1,1,'1-1428402987','2015-04-07 15:36:27','2015-04-07 17:16:57',1,0),(645,1,1,'1-1428407566','2015-04-07 16:52:46',NULL,1,1),(646,1,1,'1-1428409746','2015-04-07 17:29:06',NULL,1,1),(647,1,1,'1-1428465517','2015-04-08 08:58:37',NULL,1,1),(648,1,1,'1-1428465696','2015-04-08 09:01:36','2015-04-08 10:37:49',1,0),(649,1,1,'1-1428471487','2015-04-08 10:38:07','2015-04-08 10:49:37',1,0),(650,1,1,'1-1428472306','2015-04-08 10:51:46','2015-04-08 15:03:44',1,0),(651,1,1,'1-1428553015','2015-04-09 09:16:55',NULL,1,1),(652,1,1,'1-1428553191','2015-04-09 09:19:51','2015-04-09 09:52:21',1,0),(653,1,1,'1-1428554052','2015-04-09 09:34:13','2015-04-09 16:50:03',1,0),(654,1,1,'1-1428555153','2015-04-09 09:52:33','2015-04-09 11:59:30',1,0),(655,1,1,'1-1428570559','2015-04-09 14:09:19','2015-04-09 15:59:54',1,0),(656,1,1,'1-1428577661','2015-04-09 16:07:41','2015-04-09 16:08:14',1,0),(657,1,1,'1-1428577857','2015-04-09 16:10:57','2015-04-09 16:12:24',1,0),(658,1,1,'1-1428580205','2015-04-09 16:50:05','2015-04-09 16:56:43',1,0),(659,1,1,'1-1428580474','2015-04-09 16:54:34','2015-04-09 16:55:27',1,0),(660,1,1,'1-1428580774','2015-04-09 16:59:34','2015-04-09 17:00:27',1,0),(661,1,1,'1-1428581207','2015-04-09 17:06:47',NULL,1,1),(662,1,1,'1-1428581871','2015-04-09 17:17:51',NULL,1,1),(663,1,1,'1-1428638323','2015-04-10 08:58:43',NULL,1,1),(664,1,1,'1-1428638559','2015-04-10 09:02:39','2015-04-10 09:03:02',1,0),(665,1,1,'1-1428638918','2015-04-10 09:08:38',NULL,1,1),(666,1,1,'1-1428639066','2015-04-10 09:11:06',NULL,1,1),(667,1,1,'1-1428646681','2015-04-10 11:18:02',NULL,1,1),(668,1,1,'1-1428664737','2015-04-10 16:18:58',NULL,1,1),(669,1,1,'1-1428670264','2015-04-10 17:51:04',NULL,1,1),(670,1,1,'1-1428725482','2015-04-11 09:11:22',NULL,1,1),(671,1,1,'1-1428725692','2015-04-11 09:14:52','2015-04-11 10:00:33',1,0),(672,1,1,'1-1428726139','2015-04-11 09:22:19','2015-04-11 14:34:06',1,0),(673,1,1,'1-1428728404','2015-04-11 10:00:04',NULL,1,1),(674,1,1,'1-1428728610','2015-04-11 10:03:30','2015-04-11 10:08:38',1,0),(675,1,1,'1-1428729138','2015-04-11 10:12:18','2015-04-11 10:12:24',1,0),(676,1,1,'1-1428730378','2015-04-11 10:32:58',NULL,1,1),(677,1,1,'1-1428744744','2015-04-11 14:32:24',NULL,1,1),(678,1,1,'1-1428906712','2015-04-13 11:31:52',NULL,1,1),(679,1,1,'1-1428906740','2015-04-13 11:32:21',NULL,1,1),(680,1,1,'1-1428908550','2015-04-13 12:02:30',NULL,1,1),(681,1,1,'1-1428908864','2015-04-13 12:07:44',NULL,1,1),(682,1,1,'1-1428927156','2015-04-13 17:12:36',NULL,1,1),(683,1,1,'1-1428984275','2015-04-14 09:04:35',NULL,1,1),(684,1,1,'1-1428984609','2015-04-14 09:10:09',NULL,1,1),(685,1,1,'1-1428984766','2015-04-14 09:12:47',NULL,1,1),(686,1,1,'1-1428984980','2015-04-14 09:16:20',NULL,1,1),(687,1,1,'1-1428987506','2015-04-14 09:58:26',NULL,1,1),(688,1,1,'1-1429012307','2015-04-14 16:51:47',NULL,1,1),(689,1,1,'1-1429015487','2015-04-14 17:44:47',NULL,1,1),(690,1,1,'1-1429070723','2015-04-15 09:05:23',NULL,1,1),(691,1,1,'1-1429071795','2015-04-15 09:23:15',NULL,1,1),(692,1,1,'1-1429072150','2015-04-15 09:29:10',NULL,1,1),(693,1,1,'1-1429088495','2015-04-15 14:01:36',NULL,1,1),(694,1,1,'1-1429088782','2015-04-15 14:06:23',NULL,1,1),(695,1,1,'1-1429091429','2015-04-15 14:50:29',NULL,1,1),(696,1,1,'1-1429154851','2015-04-16 08:27:31',NULL,1,1),(697,1,1,'1-1429157305','2015-04-16 09:08:25','2015-04-16 09:20:25',1,0),(698,1,1,'1-1429157760','2015-04-16 09:16:01',NULL,1,1),(699,1,1,'1-1429163321','2015-04-16 10:48:41','2015-04-16 10:49:03',1,0),(700,1,1,'1-1429163431','2015-04-16 10:50:31',NULL,1,1),(701,1,1,'1-1429183675','2015-04-16 16:27:55',NULL,1,1),(702,1,1,'1-1429244198','2015-04-17 09:16:38',NULL,1,1),(703,1,1,'1-1429244590','2015-04-17 09:23:11',NULL,1,1),(704,1,1,'1-1429328743','2015-04-18 08:45:43',NULL,1,1),(705,1,1,'1-1429328779','2015-04-18 08:46:19','2015-04-18 08:47:42',1,0),(706,1,1,'1-1429345050','2015-04-18 13:17:30',NULL,1,1),(707,1,1,'1-1429349413','2015-04-18 14:30:13',NULL,1,1),(708,1,1,'1-1429503070','2015-04-20 09:11:10','2015-04-20 09:18:20',1,0),(709,1,1,'1-1429504218','2015-04-20 09:30:18',NULL,1,1),(710,1,1,'1-1429589978','2015-04-21 09:19:38',NULL,1,1),(711,1,1,'1-1429590337','2015-04-21 09:25:37',NULL,1,1),(712,1,1,'1-1429596859','2015-04-21 11:14:19',NULL,1,1),(713,1,1,'1-1429603017','2015-04-21 12:56:57',NULL,1,1),(714,1,1,'1-1429604305','2015-04-21 13:18:25',NULL,1,1),(715,1,1,'1-1429676155','2015-04-22 09:15:55','2015-04-22 09:16:27',1,0),(716,1,1,'1-1429676978','2015-04-22 09:29:38',NULL,1,1);

/*Table structure for table `work_week` */

DROP TABLE IF EXISTS `work_week`;

CREATE TABLE `work_week` (
  `work_week_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `work_week` VARCHAR(255) NOT NULL,
  `working_day` VARCHAR(255) NOT NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`work_week_id`)
) ENGINE=MYISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `work_week` */

INSERT  INTO `work_week`(`work_week_id`,`work_week`,`working_day`,`enabled`,`trashed`) VALUES (1,'Monday','Full Day',1,0),(2,'Tuesday','Full Day',1,0),(3,'Wednesday','Full Day',1,0),(4,'Thursday','Full Day',1,0),(5,'Friday','Half Day',1,0),(6,'Saturday','Weekend',1,0),(7,'Sunday','Weekend',1,0);

/*Table structure for table `shortlisted_interview` */

DROP TABLE IF EXISTS `shortlisted_interview`;

CREATE TABLE `shortlisted_interview` (
  `shtintr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicantID` INT(11) UNSIGNED DEFAULT NULL,
  `AdvertisementID` INT(11) UNSIGNED DEFAULT NULL,
  `present` TINYINT(1) DEFAULT '0',
  `feedback` TEXT COLLATE utf8_bin,
  `Score` VARCHAR(80) COLLATE utf8_bin DEFAULT NULL,
  `feedback_by` INT(11) UNSIGNED DEFAULT NULL,
  `feedback_date` DATE DEFAULT NULL,
  `trashed` TINYINT(1) DEFAULT '0',
  `status` INT(11) UNSIGNED DEFAULT NULL,
  `call_date` DATE DEFAULT NULL,
  `InterviewDate` DATE DEFAULT NULL,
  `selected_approval` TINYINT(1) DEFAULT NULL,
  `selected_approval_status` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`shtintr_id`)
) ENGINE=INNODB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `ml_skill_level` */

DROP TABLE IF EXISTS `ml_skill_level`;

CREATE TABLE `ml_skill_level` (
  `level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skill_level` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `trashed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_skill_level` */

insert  into `ml_skill_level`(`level_id`,`skill_level`,`enabled`,`trashed`) values (1,'Beginner',1,0),(2,'Intermediate',1,0),(3,'Advanced',1,0);

/*Data for the table `shortlisted_interview` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*Table structure for table `ml_configrationtypes` */
DROP TABLE IF EXISTS `ml_configrationtypes`;

CREATE TABLE `ml_configrationtypes` (
  `ConfigID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ConfigTypes` VARCHAR(256) COLLATE utf8_bin NOT NULL,
  `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ConfigID`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ml_configrationtypes` */
INSERT  INTO `ml_configrationtypes`(`ConfigID`,`ConfigTypes`,`enabled`,`trashed`) VALUES (1,'Attendance Based TimeSheet',1,0),(2,'Manual Timesheet',1,0),(3,'Hourly Accumulative Time Sheet',1,0),(4,'Biometric Attendance',1,0);


create table `user_groups_views_privileges` (
	`user_privilege_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`user_group_id` int ,
	`view` tinyint ,
	`modify` tinyint ,
	`create` tinyint ,
	`print` tinyint ,
	`view_id` int ,
	`enabled` tinyint ,
	`trashed` tinyint
)ENGINE=INNODB DEFAULT CHARSET=latin1;
